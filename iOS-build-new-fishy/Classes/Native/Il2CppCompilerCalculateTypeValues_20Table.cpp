﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Rect>
struct List_1_t3050876758;
// UnityEngine.UI.Extensions.AutoCompleteComboBox
struct AutoCompleteComboBox_t2713109875;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Action
struct Action_t3226471752;
// UnityEngine.UI.Extensions.ComboBox
struct ComboBox_t414540299;
// UnityEngine.UI.Extensions.DropDownListItem
struct DropDownListItem_t1818608950;
// UnityEngine.UI.Extensions.DropDownList
struct DropDownList_t2509108757;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Extensions.ReorderableListContent
struct ReorderableListContent_t133253858;
// UnityEngine.UI.Extensions.ReorderableList
struct ReorderableList_t970849249;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Void
struct Void_t1841601450;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t420310625;
// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatFinishCallback
struct FloatFinishCallback_t26767115;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl
struct ColorPickerControl_t3621872136;
// UnityEngine.UI.Extensions.BoxSlider
struct BoxSlider_t3758521666;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// ColorChangedEvent
struct ColorChangedEvent_t2990895397;
// HSVChangedEvent
struct HSVChangedEvent_t1170297569;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent
struct SelectionTextChangedEvent_t934555890;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent
struct SelectionValidityChangedEvent_t352594559;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent
struct SelectionChangedEvent_t4042853089;
// UnityEngine.UI.Extensions.UILineRenderer
struct UILineRenderer_t3031355003;
// UnityEngine.UI.Extensions.Circle
struct Circle_t1341526964;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Extensions.UIVerticalScroller
struct UIVerticalScroller_t1840308474;
// UnityEngine.UI.Extensions.TextPic
struct TextPic_t1856600851;
// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent
struct SelectionChangedEvent_t3776471183;
// UnityEngine.UI.Extensions.RadialSlider
struct RadialSlider_t2099696204;
// UnityEngine.UI.Extensions.LetterSpacing
struct LetterSpacing_t4139949939;
// UnityEngine.UI.Extensions.CurvedText
struct CurvedText_t3665035540;
// UnityEngine.UI.Extensions.Gradient2
struct Gradient2_t998070926;
// UnityEngine.UI.Extensions.CylinderText
struct CylinderText_t2066901577;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Extensions.SoftMaskScript
struct SoftMaskScript_t947627439;
// UnityEngine.UI.LayoutGroup
struct LayoutGroup_t3962498969;
// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler
struct ReorderableListHandler_t1694188766;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.ReorderableListElement>
struct List_1_t2496814557;
// UnityEngine.UI.Extensions.ReorderableListElement
struct ReorderableListElement_t3127693425;
// UnityEngine.UI.Extensions.KnobFloatValueEvent
struct KnobFloatValueEvent_t2705095779;
// UnityEngine.UI.Extensions.IBoxSelectable[]
struct IBoxSelectableU5BU5D_t1290335761;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t3035069757;
// UnityEngine.UI.Extensions.IBoxSelectable
struct IBoxSelectable_t1633048368;
// UnityEngine.UI.Extensions.SelectionBox/SelectionEvent
struct SelectionEvent_t1038911497;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent
struct CooldownButtonEvent_t1873823474;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.UI.Extensions.RadialSlider/RadialSliderValueChangedEvent
struct RadialSliderValueChangedEvent_t1574085429;
// UnityEngine.UI.Extensions.RadialSlider/RadialSliderTextValueChangedEvent
struct RadialSliderTextValueChangedEvent_t3907405160;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListItem>
struct List_1_t1187730082;
// UnityEngine.UI.Extensions.DropDownListButton
struct DropDownListButton_t188411761;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListButton>
struct List_1_t3852500189;
// UnityEngine.UI.Extensions.DropDownList/SelectionChangedEvent
struct SelectionChangedEvent_t3626208507;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.UI.Extensions.Examples.OptionsMenu
struct OptionsMenu_t2309315281;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.UI.Extensions.Examples.PauseMenu
struct PauseMenu_t982516531;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t3529018992;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t3083107861;
// UnityEngine.UI.Extensions.SegmentedControl/SegmentSelectedEvent
struct SegmentSelectedEvent_t4190223295;
// UnityEngine.UI.Extensions.Stepper/StepperValueChangedEvent
struct StepperValueChangedEvent_t573788457;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t859513320;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t3244928895;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2665681875;
// UnityEngine.UI.Extensions.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t1768348986;
// UnityEngine.UI.Extensions.CUIBezierCurve[]
struct CUIBezierCurveU5BU5D_t2056300054;
// UnityEngine.UI.Extensions.Vector3_Array2D[]
struct Vector3_Array2DU5BU5D_t3555344525;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t1030026315;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t1896830814;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// UnityEngine.UI.Extensions.Accordion
struct Accordion_t2257195762;
// UnityEngine.UI.Extensions.Tweens.TweenRunner`1<UnityEngine.UI.Extensions.Tweens.FloatTween>
struct TweenRunner_1_t161549504;
// UnityEngine.UI.FontData
struct FontData_t2614388407;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t1411648341;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Object
struct Object_t1021602117;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// UnityEngine.UI.Extensions.TextPic/IconName[]
struct IconNameU5BU5D_t3827230366;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.TextPic/HrefInfo>
struct List_1_t1717420812;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// UnityEngine.UI.Extensions.TextPic/HrefClickEvent
struct HrefClickEvent_t1044804460;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t485497324;

struct Vector3_t2243707580 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef HREFINFO_T2348299680_H
#define HREFINFO_T2348299680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic/HrefInfo
struct  HrefInfo_t2348299680  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Extensions.TextPic/HrefInfo::startIndex
	int32_t ___startIndex_0;
	// System.Int32 UnityEngine.UI.Extensions.TextPic/HrefInfo::endIndex
	int32_t ___endIndex_1;
	// System.String UnityEngine.UI.Extensions.TextPic/HrefInfo::name
	String_t* ___name_2;
	// System.Collections.Generic.List`1<UnityEngine.Rect> UnityEngine.UI.Extensions.TextPic/HrefInfo::boxes
	List_1_t3050876758 * ___boxes_3;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(HrefInfo_t2348299680, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_endIndex_1() { return static_cast<int32_t>(offsetof(HrefInfo_t2348299680, ___endIndex_1)); }
	inline int32_t get_endIndex_1() const { return ___endIndex_1; }
	inline int32_t* get_address_of_endIndex_1() { return &___endIndex_1; }
	inline void set_endIndex_1(int32_t value)
	{
		___endIndex_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(HrefInfo_t2348299680, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_boxes_3() { return static_cast<int32_t>(offsetof(HrefInfo_t2348299680, ___boxes_3)); }
	inline List_1_t3050876758 * get_boxes_3() const { return ___boxes_3; }
	inline List_1_t3050876758 ** get_address_of_boxes_3() { return &___boxes_3; }
	inline void set_boxes_3(List_1_t3050876758 * value)
	{
		___boxes_3 = value;
		Il2CppCodeGenWriteBarrier((&___boxes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HREFINFO_T2348299680_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CREBUILDPANELU3EC__ANONSTOREY0_T3251602021_H
#define U3CREBUILDPANELU3EC__ANONSTOREY0_T3251602021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0
struct  U3CRebuildPanelU3Ec__AnonStorey0_t3251602021  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0::textOfItem
	String_t* ___textOfItem_0;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0::$this
	AutoCompleteComboBox_t2713109875 * ___U24this_1;

public:
	inline static int32_t get_offset_of_textOfItem_0() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t3251602021, ___textOfItem_0)); }
	inline String_t* get_textOfItem_0() const { return ___textOfItem_0; }
	inline String_t** get_address_of_textOfItem_0() { return &___textOfItem_0; }
	inline void set_textOfItem_0(String_t* value)
	{
		___textOfItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___textOfItem_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t3251602021, ___U24this_1)); }
	inline AutoCompleteComboBox_t2713109875 * get_U24this_1() const { return ___U24this_1; }
	inline AutoCompleteComboBox_t2713109875 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AutoCompleteComboBox_t2713109875 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDPANELU3EC__ANONSTOREY0_T3251602021_H
#ifndef DROPDOWNLISTITEM_T1818608950_H
#define DROPDOWNLISTITEM_T1818608950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownListItem
struct  DropDownListItem_t1818608950  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.DropDownListItem::_caption
	String_t* ____caption_0;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.DropDownListItem::_image
	Sprite_t309593783 * ____image_1;
	// System.Boolean UnityEngine.UI.Extensions.DropDownListItem::_isDisabled
	bool ____isDisabled_2;
	// System.String UnityEngine.UI.Extensions.DropDownListItem::_id
	String_t* ____id_3;
	// System.Action UnityEngine.UI.Extensions.DropDownListItem::OnSelect
	Action_t3226471752 * ___OnSelect_4;
	// System.Action UnityEngine.UI.Extensions.DropDownListItem::OnUpdate
	Action_t3226471752 * ___OnUpdate_5;

public:
	inline static int32_t get_offset_of__caption_0() { return static_cast<int32_t>(offsetof(DropDownListItem_t1818608950, ____caption_0)); }
	inline String_t* get__caption_0() const { return ____caption_0; }
	inline String_t** get_address_of__caption_0() { return &____caption_0; }
	inline void set__caption_0(String_t* value)
	{
		____caption_0 = value;
		Il2CppCodeGenWriteBarrier((&____caption_0), value);
	}

	inline static int32_t get_offset_of__image_1() { return static_cast<int32_t>(offsetof(DropDownListItem_t1818608950, ____image_1)); }
	inline Sprite_t309593783 * get__image_1() const { return ____image_1; }
	inline Sprite_t309593783 ** get_address_of__image_1() { return &____image_1; }
	inline void set__image_1(Sprite_t309593783 * value)
	{
		____image_1 = value;
		Il2CppCodeGenWriteBarrier((&____image_1), value);
	}

	inline static int32_t get_offset_of__isDisabled_2() { return static_cast<int32_t>(offsetof(DropDownListItem_t1818608950, ____isDisabled_2)); }
	inline bool get__isDisabled_2() const { return ____isDisabled_2; }
	inline bool* get_address_of__isDisabled_2() { return &____isDisabled_2; }
	inline void set__isDisabled_2(bool value)
	{
		____isDisabled_2 = value;
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(DropDownListItem_t1818608950, ____id_3)); }
	inline String_t* get__id_3() const { return ____id_3; }
	inline String_t** get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(String_t* value)
	{
		____id_3 = value;
		Il2CppCodeGenWriteBarrier((&____id_3), value);
	}

	inline static int32_t get_offset_of_OnSelect_4() { return static_cast<int32_t>(offsetof(DropDownListItem_t1818608950, ___OnSelect_4)); }
	inline Action_t3226471752 * get_OnSelect_4() const { return ___OnSelect_4; }
	inline Action_t3226471752 ** get_address_of_OnSelect_4() { return &___OnSelect_4; }
	inline void set_OnSelect_4(Action_t3226471752 * value)
	{
		___OnSelect_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelect_4), value);
	}

	inline static int32_t get_offset_of_OnUpdate_5() { return static_cast<int32_t>(offsetof(DropDownListItem_t1818608950, ___OnUpdate_5)); }
	inline Action_t3226471752 * get_OnUpdate_5() const { return ___OnUpdate_5; }
	inline Action_t3226471752 ** get_address_of_OnUpdate_5() { return &___OnUpdate_5; }
	inline void set_OnUpdate_5(Action_t3226471752 * value)
	{
		___OnUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdate_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNLISTITEM_T1818608950_H
#ifndef U3CREBUILDPANELU3EC__ANONSTOREY0_T2999840211_H
#define U3CREBUILDPANELU3EC__ANONSTOREY0_T2999840211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ComboBox/<RebuildPanel>c__AnonStorey0
struct  U3CRebuildPanelU3Ec__AnonStorey0_t2999840211  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.ComboBox/<RebuildPanel>c__AnonStorey0::textOfItem
	String_t* ___textOfItem_0;
	// UnityEngine.UI.Extensions.ComboBox UnityEngine.UI.Extensions.ComboBox/<RebuildPanel>c__AnonStorey0::$this
	ComboBox_t414540299 * ___U24this_1;

public:
	inline static int32_t get_offset_of_textOfItem_0() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t2999840211, ___textOfItem_0)); }
	inline String_t* get_textOfItem_0() const { return ___textOfItem_0; }
	inline String_t** get_address_of_textOfItem_0() { return &___textOfItem_0; }
	inline void set_textOfItem_0(String_t* value)
	{
		___textOfItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___textOfItem_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t2999840211, ___U24this_1)); }
	inline ComboBox_t414540299 * get_U24this_1() const { return ___U24this_1; }
	inline ComboBox_t414540299 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ComboBox_t414540299 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDPANELU3EC__ANONSTOREY0_T2999840211_H
#ifndef U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T4263748227_H
#define U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T4263748227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/<PruneItemsLinq>c__AnonStorey1
struct  U3CPruneItemsLinqU3Ec__AnonStorey1_t4263748227  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox/<PruneItemsLinq>c__AnonStorey1::currText
	String_t* ___currText_0;

public:
	inline static int32_t get_offset_of_currText_0() { return static_cast<int32_t>(offsetof(U3CPruneItemsLinqU3Ec__AnonStorey1_t4263748227, ___currText_0)); }
	inline String_t* get_currText_0() const { return ___currText_0; }
	inline String_t** get_address_of_currText_0() { return &___currText_0; }
	inline void set_currText_0(String_t* value)
	{
		___currText_0 = value;
		Il2CppCodeGenWriteBarrier((&___currText_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T4263748227_H
#ifndef U3CREBUILDPANELU3EC__ANONSTOREY0_T1624052423_H
#define U3CREBUILDPANELU3EC__ANONSTOREY0_T1624052423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0
struct  U3CRebuildPanelU3Ec__AnonStorey0_t1624052423  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0::ii
	int32_t ___ii_0;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0::item
	DropDownListItem_t1818608950 * ___item_1;
	// UnityEngine.UI.Extensions.DropDownList UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0::$this
	DropDownList_t2509108757 * ___U24this_2;

public:
	inline static int32_t get_offset_of_ii_0() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t1624052423, ___ii_0)); }
	inline int32_t get_ii_0() const { return ___ii_0; }
	inline int32_t* get_address_of_ii_0() { return &___ii_0; }
	inline void set_ii_0(int32_t value)
	{
		___ii_0 = value;
	}

	inline static int32_t get_offset_of_item_1() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t1624052423, ___item_1)); }
	inline DropDownListItem_t1818608950 * get_item_1() const { return ___item_1; }
	inline DropDownListItem_t1818608950 ** get_address_of_item_1() { return &___item_1; }
	inline void set_item_1(DropDownListItem_t1818608950 * value)
	{
		___item_1 = value;
		Il2CppCodeGenWriteBarrier((&___item_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t1624052423, ___U24this_2)); }
	inline DropDownList_t2509108757 * get_U24this_2() const { return ___U24this_2; }
	inline DropDownList_t2509108757 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(DropDownList_t2509108757 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDPANELU3EC__ANONSTOREY0_T1624052423_H
#ifndef DROPDOWNLISTBUTTON_T188411761_H
#define DROPDOWNLISTBUTTON_T188411761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownListButton
struct  DropDownListButton_t188411761  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownListButton::rectTransform
	RectTransform_t3349966182 * ___rectTransform_0;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.DropDownListButton::btn
	Button_t2872111280 * ___btn_1;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.DropDownListButton::txt
	Text_t356221433 * ___txt_2;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.DropDownListButton::btnImg
	Image_t2042527209 * ___btnImg_3;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.DropDownListButton::img
	Image_t2042527209 * ___img_4;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.DropDownListButton::gameobject
	GameObject_t1756533147 * ___gameobject_5;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(DropDownListButton_t188411761, ___rectTransform_0)); }
	inline RectTransform_t3349966182 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3349966182 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_btn_1() { return static_cast<int32_t>(offsetof(DropDownListButton_t188411761, ___btn_1)); }
	inline Button_t2872111280 * get_btn_1() const { return ___btn_1; }
	inline Button_t2872111280 ** get_address_of_btn_1() { return &___btn_1; }
	inline void set_btn_1(Button_t2872111280 * value)
	{
		___btn_1 = value;
		Il2CppCodeGenWriteBarrier((&___btn_1), value);
	}

	inline static int32_t get_offset_of_txt_2() { return static_cast<int32_t>(offsetof(DropDownListButton_t188411761, ___txt_2)); }
	inline Text_t356221433 * get_txt_2() const { return ___txt_2; }
	inline Text_t356221433 ** get_address_of_txt_2() { return &___txt_2; }
	inline void set_txt_2(Text_t356221433 * value)
	{
		___txt_2 = value;
		Il2CppCodeGenWriteBarrier((&___txt_2), value);
	}

	inline static int32_t get_offset_of_btnImg_3() { return static_cast<int32_t>(offsetof(DropDownListButton_t188411761, ___btnImg_3)); }
	inline Image_t2042527209 * get_btnImg_3() const { return ___btnImg_3; }
	inline Image_t2042527209 ** get_address_of_btnImg_3() { return &___btnImg_3; }
	inline void set_btnImg_3(Image_t2042527209 * value)
	{
		___btnImg_3 = value;
		Il2CppCodeGenWriteBarrier((&___btnImg_3), value);
	}

	inline static int32_t get_offset_of_img_4() { return static_cast<int32_t>(offsetof(DropDownListButton_t188411761, ___img_4)); }
	inline Image_t2042527209 * get_img_4() const { return ___img_4; }
	inline Image_t2042527209 ** get_address_of_img_4() { return &___img_4; }
	inline void set_img_4(Image_t2042527209 * value)
	{
		___img_4 = value;
		Il2CppCodeGenWriteBarrier((&___img_4), value);
	}

	inline static int32_t get_offset_of_gameobject_5() { return static_cast<int32_t>(offsetof(DropDownListButton_t188411761, ___gameobject_5)); }
	inline GameObject_t1756533147 * get_gameobject_5() const { return ___gameobject_5; }
	inline GameObject_t1756533147 ** get_address_of_gameobject_5() { return &___gameobject_5; }
	inline void set_gameobject_5(GameObject_t1756533147 * value)
	{
		___gameobject_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameobject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNLISTBUTTON_T188411761_H
#ifndef HSVUTIL_T960645847_H
#define HSVUTIL_T960645847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HSVUtil
struct  HSVUtil_t960645847  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T960645847_H
#ifndef U3CREFRESHCHILDRENU3EC__ITERATOR0_T2721765405_H
#define U3CREFRESHCHILDRENU3EC__ITERATOR0_T2721765405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0
struct  U3CRefreshChildrenU3Ec__Iterator0_t2721765405  : public RuntimeObject
{
public:
	// UnityEngine.UI.Extensions.ReorderableListContent UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$this
	ReorderableListContent_t133253858 * ___U24this_0;
	// System.Object UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2721765405, ___U24this_0)); }
	inline ReorderableListContent_t133253858 * get_U24this_0() const { return ___U24this_0; }
	inline ReorderableListContent_t133253858 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ReorderableListContent_t133253858 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2721765405, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2721765405, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2721765405, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREFRESHCHILDRENU3EC__ITERATOR0_T2721765405_H
#ifndef REORDERABLELISTEVENTSTRUCT_T1615631671_H
#define REORDERABLELISTEVENTSTRUCT_T1615631671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct
struct  ReorderableListEventStruct_t1615631671 
{
public:
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::DroppedObject
	GameObject_t1756533147 * ___DroppedObject_0;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::FromIndex
	int32_t ___FromIndex_1;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::FromList
	ReorderableList_t970849249 * ___FromList_2;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::IsAClone
	bool ___IsAClone_3;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::SourceObject
	GameObject_t1756533147 * ___SourceObject_4;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::ToIndex
	int32_t ___ToIndex_5;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::ToList
	ReorderableList_t970849249 * ___ToList_6;

public:
	inline static int32_t get_offset_of_DroppedObject_0() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1615631671, ___DroppedObject_0)); }
	inline GameObject_t1756533147 * get_DroppedObject_0() const { return ___DroppedObject_0; }
	inline GameObject_t1756533147 ** get_address_of_DroppedObject_0() { return &___DroppedObject_0; }
	inline void set_DroppedObject_0(GameObject_t1756533147 * value)
	{
		___DroppedObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___DroppedObject_0), value);
	}

	inline static int32_t get_offset_of_FromIndex_1() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1615631671, ___FromIndex_1)); }
	inline int32_t get_FromIndex_1() const { return ___FromIndex_1; }
	inline int32_t* get_address_of_FromIndex_1() { return &___FromIndex_1; }
	inline void set_FromIndex_1(int32_t value)
	{
		___FromIndex_1 = value;
	}

	inline static int32_t get_offset_of_FromList_2() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1615631671, ___FromList_2)); }
	inline ReorderableList_t970849249 * get_FromList_2() const { return ___FromList_2; }
	inline ReorderableList_t970849249 ** get_address_of_FromList_2() { return &___FromList_2; }
	inline void set_FromList_2(ReorderableList_t970849249 * value)
	{
		___FromList_2 = value;
		Il2CppCodeGenWriteBarrier((&___FromList_2), value);
	}

	inline static int32_t get_offset_of_IsAClone_3() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1615631671, ___IsAClone_3)); }
	inline bool get_IsAClone_3() const { return ___IsAClone_3; }
	inline bool* get_address_of_IsAClone_3() { return &___IsAClone_3; }
	inline void set_IsAClone_3(bool value)
	{
		___IsAClone_3 = value;
	}

	inline static int32_t get_offset_of_SourceObject_4() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1615631671, ___SourceObject_4)); }
	inline GameObject_t1756533147 * get_SourceObject_4() const { return ___SourceObject_4; }
	inline GameObject_t1756533147 ** get_address_of_SourceObject_4() { return &___SourceObject_4; }
	inline void set_SourceObject_4(GameObject_t1756533147 * value)
	{
		___SourceObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___SourceObject_4), value);
	}

	inline static int32_t get_offset_of_ToIndex_5() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1615631671, ___ToIndex_5)); }
	inline int32_t get_ToIndex_5() const { return ___ToIndex_5; }
	inline int32_t* get_address_of_ToIndex_5() { return &___ToIndex_5; }
	inline void set_ToIndex_5(int32_t value)
	{
		___ToIndex_5 = value;
	}

	inline static int32_t get_offset_of_ToList_6() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1615631671, ___ToList_6)); }
	inline ReorderableList_t970849249 * get_ToList_6() const { return ___ToList_6; }
	inline ReorderableList_t970849249 ** get_address_of_ToList_6() { return &___ToList_6; }
	inline void set_ToList_6(ReorderableList_t970849249 * value)
	{
		___ToList_6 = value;
		Il2CppCodeGenWriteBarrier((&___ToList_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct
struct ReorderableListEventStruct_t1615631671_marshaled_pinvoke
{
	GameObject_t1756533147 * ___DroppedObject_0;
	int32_t ___FromIndex_1;
	ReorderableList_t970849249 * ___FromList_2;
	int32_t ___IsAClone_3;
	GameObject_t1756533147 * ___SourceObject_4;
	int32_t ___ToIndex_5;
	ReorderableList_t970849249 * ___ToList_6;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct
struct ReorderableListEventStruct_t1615631671_marshaled_com
{
	GameObject_t1756533147 * ___DroppedObject_0;
	int32_t ___FromIndex_1;
	ReorderableList_t970849249 * ___FromList_2;
	int32_t ___IsAClone_3;
	GameObject_t1756533147 * ___SourceObject_4;
	int32_t ___ToIndex_5;
	ReorderableList_t970849249 * ___ToList_6;
};
#endif // REORDERABLELISTEVENTSTRUCT_T1615631671_H
#ifndef UNITYEVENT_2_T536013165_H
#define UNITYEVENT_2_T536013165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.String,System.Boolean>
struct  UnityEvent_2_t536013165  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t536013165, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T536013165_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SPRITESTATE_T1353336012_H
#define SPRITESTATE_T1353336012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1353336012 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t309593783 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t309593783 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_HighlightedSprite_0)); }
	inline Sprite_t309593783 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t309593783 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t309593783 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_PressedSprite_1)); }
	inline Sprite_t309593783 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t309593783 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t309593783 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_DisabledSprite_2)); }
	inline Sprite_t309593783 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t309593783 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t309593783 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_pinvoke
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_com
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1353336012_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef UNITYEVENT_1_T3020313056_H
#define UNITYEVENT_1_T3020313056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.PointerEventData/InputButton>
struct  UnityEvent_1_t3020313056  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3020313056, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3020313056_H
#ifndef UNITYEVENT_1_T2114859947_H
#define UNITYEVENT_1_T2114859947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2114859947  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2114859947, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2114859947_H
#ifndef UNITYEVENT_1_T2110227463_H
#define UNITYEVENT_1_T2110227463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2110227463  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2110227463, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2110227463_H
#ifndef UNITYEVENT_1_T2067570248_H
#define UNITYEVENT_1_T2067570248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2067570248  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2067570248, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2067570248_H
#ifndef UNITYEVENT_1_T3863924733_H
#define UNITYEVENT_1_T3863924733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t3863924733  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3863924733, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3863924733_H
#ifndef VECTOR3_ARRAY2D_T3054885220_H
#define VECTOR3_ARRAY2D_T3054885220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Vector3_Array2D
struct  Vector3_Array2D_t3054885220 
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.Vector3_Array2D::array
	Vector3U5BU5D_t1172311765* ___array_0;

public:
	inline static int32_t get_offset_of_array_0() { return static_cast<int32_t>(offsetof(Vector3_Array2D_t3054885220, ___array_0)); }
	inline Vector3U5BU5D_t1172311765* get_array_0() const { return ___array_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_array_0() { return &___array_0; }
	inline void set_array_0(Vector3U5BU5D_t1172311765* value)
	{
		___array_0 = value;
		Il2CppCodeGenWriteBarrier((&___array_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.Vector3_Array2D
struct Vector3_Array2D_t3054885220_marshaled_pinvoke
{
	Vector3_t2243707580 * ___array_0;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.Vector3_Array2D
struct Vector3_Array2D_t3054885220_marshaled_com
{
	Vector3_t2243707580 * ___array_0;
};
#endif // VECTOR3_ARRAY2D_T3054885220_H
#ifndef UNITYEVENT_1_T1653981686_H
#define UNITYEVENT_1_T1653981686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct>
struct  UnityEvent_1_t1653981686  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1653981686, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1653981686_H
#ifndef UNITYEVENT_T408735097_H
#define UNITYEVENT_T408735097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t408735097  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t408735097, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T408735097_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef UNITYEVENT_2_T2016657100_H
#define UNITYEVENT_2_T2016657100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t2016657100  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t2016657100, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T2016657100_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef UNITYEVENT_1_T2058742090_H
#define UNITYEVENT_1_T2058742090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t2058742090  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2058742090, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2058742090_H
#ifndef UNITYEVENT_1_T1328685776_H
#define UNITYEVENT_1_T1328685776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.UI.Extensions.IBoxSelectable[]>
struct  UnityEvent_1_t1328685776  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1328685776, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1328685776_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef UNITYEVENT_3_T4197061729_H
#define UNITYEVENT_3_T4197061729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t4197061729  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t4197061729, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T4197061729_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef HSVCOLOR_T223253130_H
#define HSVCOLOR_T223253130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HsvColor
struct  HsvColor_t223253130 
{
public:
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::H
	double ___H_0;
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::S
	double ___S_1;
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t223253130, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t223253130, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t223253130, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T223253130_H
#ifndef FLOATTWEEN_T798271509_H
#define FLOATTWEEN_T798271509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Tweens.FloatTween
struct  FloatTween_t798271509 
{
public:
	// System.Single UnityEngine.UI.Extensions.Tweens.FloatTween::m_StartFloat
	float ___m_StartFloat_0;
	// System.Single UnityEngine.UI.Extensions.Tweens.FloatTween::m_TargetFloat
	float ___m_TargetFloat_1;
	// System.Single UnityEngine.UI.Extensions.Tweens.FloatTween::m_Duration
	float ___m_Duration_2;
	// System.Boolean UnityEngine.UI.Extensions.Tweens.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_3;
	// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatTweenCallback UnityEngine.UI.Extensions.Tweens.FloatTween::m_Target
	FloatTweenCallback_t420310625 * ___m_Target_4;
	// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatFinishCallback UnityEngine.UI.Extensions.Tweens.FloatTween::m_Finish
	FloatFinishCallback_t26767115 * ___m_Finish_5;

public:
	inline static int32_t get_offset_of_m_StartFloat_0() { return static_cast<int32_t>(offsetof(FloatTween_t798271509, ___m_StartFloat_0)); }
	inline float get_m_StartFloat_0() const { return ___m_StartFloat_0; }
	inline float* get_address_of_m_StartFloat_0() { return &___m_StartFloat_0; }
	inline void set_m_StartFloat_0(float value)
	{
		___m_StartFloat_0 = value;
	}

	inline static int32_t get_offset_of_m_TargetFloat_1() { return static_cast<int32_t>(offsetof(FloatTween_t798271509, ___m_TargetFloat_1)); }
	inline float get_m_TargetFloat_1() const { return ___m_TargetFloat_1; }
	inline float* get_address_of_m_TargetFloat_1() { return &___m_TargetFloat_1; }
	inline void set_m_TargetFloat_1(float value)
	{
		___m_TargetFloat_1 = value;
	}

	inline static int32_t get_offset_of_m_Duration_2() { return static_cast<int32_t>(offsetof(FloatTween_t798271509, ___m_Duration_2)); }
	inline float get_m_Duration_2() const { return ___m_Duration_2; }
	inline float* get_address_of_m_Duration_2() { return &___m_Duration_2; }
	inline void set_m_Duration_2(float value)
	{
		___m_Duration_2 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_3() { return static_cast<int32_t>(offsetof(FloatTween_t798271509, ___m_IgnoreTimeScale_3)); }
	inline bool get_m_IgnoreTimeScale_3() const { return ___m_IgnoreTimeScale_3; }
	inline bool* get_address_of_m_IgnoreTimeScale_3() { return &___m_IgnoreTimeScale_3; }
	inline void set_m_IgnoreTimeScale_3(bool value)
	{
		___m_IgnoreTimeScale_3 = value;
	}

	inline static int32_t get_offset_of_m_Target_4() { return static_cast<int32_t>(offsetof(FloatTween_t798271509, ___m_Target_4)); }
	inline FloatTweenCallback_t420310625 * get_m_Target_4() const { return ___m_Target_4; }
	inline FloatTweenCallback_t420310625 ** get_address_of_m_Target_4() { return &___m_Target_4; }
	inline void set_m_Target_4(FloatTweenCallback_t420310625 * value)
	{
		___m_Target_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_4), value);
	}

	inline static int32_t get_offset_of_m_Finish_5() { return static_cast<int32_t>(offsetof(FloatTween_t798271509, ___m_Finish_5)); }
	inline FloatFinishCallback_t26767115 * get_m_Finish_5() const { return ___m_Finish_5; }
	inline FloatFinishCallback_t26767115 ** get_address_of_m_Finish_5() { return &___m_Finish_5; }
	inline void set_m_Finish_5(FloatFinishCallback_t26767115 * value)
	{
		___m_Finish_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Finish_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.Tweens.FloatTween
struct FloatTween_t798271509_marshaled_pinvoke
{
	float ___m_StartFloat_0;
	float ___m_TargetFloat_1;
	float ___m_Duration_2;
	int32_t ___m_IgnoreTimeScale_3;
	FloatTweenCallback_t420310625 * ___m_Target_4;
	FloatFinishCallback_t26767115 * ___m_Finish_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.Tweens.FloatTween
struct FloatTween_t798271509_marshaled_com
{
	float ___m_StartFloat_0;
	float ___m_TargetFloat_1;
	float ___m_Duration_2;
	int32_t ___m_IgnoreTimeScale_3;
	FloatTweenCallback_t420310625 * ___m_Target_4;
	FloatFinishCallback_t26767115 * ___m_Finish_5;
};
#endif // FLOATTWEEN_T798271509_H
#ifndef ICONNAME_T4224240103_H
#define ICONNAME_T4224240103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic/IconName
struct  IconName_t4224240103 
{
public:
	// System.String UnityEngine.UI.Extensions.TextPic/IconName::name
	String_t* ___name_0;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.TextPic/IconName::sprite
	Sprite_t309593783 * ___sprite_1;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TextPic/IconName::offset
	Vector2_t2243707579  ___offset_2;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TextPic/IconName::scale
	Vector2_t2243707579  ___scale_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(IconName_t4224240103, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_sprite_1() { return static_cast<int32_t>(offsetof(IconName_t4224240103, ___sprite_1)); }
	inline Sprite_t309593783 * get_sprite_1() const { return ___sprite_1; }
	inline Sprite_t309593783 ** get_address_of_sprite_1() { return &___sprite_1; }
	inline void set_sprite_1(Sprite_t309593783 * value)
	{
		___sprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_1), value);
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(IconName_t4224240103, ___offset_2)); }
	inline Vector2_t2243707579  get_offset_2() const { return ___offset_2; }
	inline Vector2_t2243707579 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Vector2_t2243707579  value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(IconName_t4224240103, ___scale_3)); }
	inline Vector2_t2243707579  get_scale_3() const { return ___scale_3; }
	inline Vector2_t2243707579 * get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(Vector2_t2243707579  value)
	{
		___scale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.TextPic/IconName
struct IconName_t4224240103_marshaled_pinvoke
{
	char* ___name_0;
	Sprite_t309593783 * ___sprite_1;
	Vector2_t2243707579  ___offset_2;
	Vector2_t2243707579  ___scale_3;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.TextPic/IconName
struct IconName_t4224240103_marshaled_com
{
	Il2CppChar* ___name_0;
	Sprite_t309593783 * ___sprite_1;
	Vector2_t2243707579  ___offset_2;
	Vector2_t2243707579  ___scale_3;
};
#endif // ICONNAME_T4224240103_H
#ifndef DIRECTION_T3408261084_H
#define DIRECTION_T3408261084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_Knob/Direction
struct  Direction_t3408261084 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UI_Knob/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t3408261084, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T3408261084_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef TRANSITION_T605142169_H
#define TRANSITION_T605142169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t605142169 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t605142169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T605142169_H
#ifndef COLORBLOCK_T2652774230_H
#define COLORBLOCK_T2652774230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2652774230 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2020392075  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2020392075  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2020392075  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2020392075  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_NormalColor_0)); }
	inline Color_t2020392075  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2020392075 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2020392075  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_HighlightedColor_1)); }
	inline Color_t2020392075  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2020392075 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2020392075  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_PressedColor_2)); }
	inline Color_t2020392075  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2020392075 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2020392075  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_DisabledColor_3)); }
	inline Color_t2020392075  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2020392075 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2020392075  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2652774230_H
#ifndef KNOBFLOATVALUEEVENT_T2705095779_H
#define KNOBFLOATVALUEEVENT_T2705095779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.KnobFloatValueEvent
struct  KnobFloatValueEvent_t2705095779  : public UnityEvent_1_t2114859947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOBFLOATVALUEEVENT_T2705095779_H
#ifndef SELECTIONEVENT_T1038911497_H
#define SELECTIONEVENT_T1038911497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SelectionBox/SelectionEvent
struct  SelectionEvent_t1038911497  : public UnityEvent_1_t1328685776
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONEVENT_T1038911497_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef SEGMENTSELECTEDEVENT_T4190223295_H
#define SEGMENTSELECTEDEVENT_T4190223295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SegmentedControl/SegmentSelectedEvent
struct  SegmentSelectedEvent_t4190223295  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTSELECTEDEVENT_T4190223295_H
#ifndef SCROLLBARVISIBILITY_T3834843475_H
#define SCROLLBARVISIBILITY_T3834843475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct  ScrollbarVisibility_t3834843475 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t3834843475, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T3834843475_H
#ifndef SELECTIONSTATE_T3187567897_H
#define SELECTIONSTATE_T3187567897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t3187567897 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t3187567897, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T3187567897_H
#ifndef MOVEMENTTYPE_T905360158_H
#define MOVEMENTTYPE_T905360158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/MovementType
struct  MovementType_t905360158 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementType_t905360158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T905360158_H
#ifndef STEPPERVALUECHANGEDEVENT_T573788457_H
#define STEPPERVALUECHANGEDEVENT_T573788457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Stepper/StepperValueChangedEvent
struct  StepperValueChangedEvent_t573788457  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPPERVALUECHANGEDEVENT_T573788457_H
#ifndef FLOATFINISHCALLBACK_T26767115_H
#define FLOATFINISHCALLBACK_T26767115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatFinishCallback
struct  FloatFinishCallback_t26767115  : public UnityEvent_t408735097
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFINISHCALLBACK_T26767115_H
#ifndef MODE_T1081683921_H
#define MODE_T1081683921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1081683921 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1081683921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1081683921_H
#ifndef DIRECTION_T1092976501_H
#define DIRECTION_T1092976501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider/Direction
struct  Direction_t1092976501 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.BoxSlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1092976501, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1092976501_H
#ifndef TRANSITION_T4120399363_H
#define TRANSITION_T4120399363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Accordion/Transition
struct  Transition_t4120399363 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Accordion/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t4120399363, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T4120399363_H
#ifndef DIRECTION_T1525323322_H
#define DIRECTION_T1525323322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t1525323322 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1525323322, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1525323322_H
#ifndef FLOATTWEENCALLBACK_T420310625_H
#define FLOATTWEENCALLBACK_T420310625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatTweenCallback
struct  FloatTweenCallback_t420310625  : public UnityEvent_1_t2114859947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATTWEENCALLBACK_T420310625_H
#ifndef BOXSLIDEREVENT_T1768348986_H
#define BOXSLIDEREVENT_T1768348986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider/BoxSliderEvent
struct  BoxSliderEvent_t1768348986  : public UnityEvent_2_t2016657100
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T1768348986_H
#ifndef GRADIENTDIR_T2268096981_H
#define GRADIENTDIR_T2268096981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.GradientDir
struct  GradientDir_t2268096981 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.GradientDir::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GradientDir_t2268096981, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTDIR_T2268096981_H
#ifndef GRADIENTMODE_T1452379243_H
#define GRADIENTMODE_T1452379243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.GradientMode
struct  GradientMode_t1452379243 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.GradientMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GradientMode_t1452379243, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTMODE_T1452379243_H
#ifndef TOGGLETRANSITION_T1114673831_H
#define TOGGLETRANSITION_T1114673831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle/ToggleTransition
struct  ToggleTransition_t1114673831 
{
public:
	// System.Int32 UnityEngine.UI.Toggle/ToggleTransition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ToggleTransition_t1114673831, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRANSITION_T1114673831_H
#ifndef AXIS_T112506491_H
#define AXIS_T112506491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider/Axis
struct  Axis_t112506491 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.BoxSlider/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t112506491, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T112506491_H
#ifndef BLEND_T3762654957_H
#define BLEND_T3762654957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient2/Blend
struct  Blend_t3762654957 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Gradient2/Blend::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Blend_t3762654957, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLEND_T3762654957_H
#ifndef TYPE_T763701076_H
#define TYPE_T763701076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient2/Type
struct  Type_t763701076 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Gradient2/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t763701076, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T763701076_H
#ifndef RADIALSLIDERVALUECHANGEDEVENT_T1574085429_H
#define RADIALSLIDERVALUECHANGEDEVENT_T1574085429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RadialSlider/RadialSliderValueChangedEvent
struct  RadialSliderValueChangedEvent_t1574085429  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALSLIDERVALUECHANGEDEVENT_T1574085429_H
#ifndef SELECTIONCHANGEDEVENT_T4042853089_H
#define SELECTIONCHANGEDEVENT_T4042853089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent
struct  SelectionChangedEvent_t4042853089  : public UnityEvent_2_t536013165
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEDEVENT_T4042853089_H
#ifndef RADIALSLIDERTEXTVALUECHANGEDEVENT_T3907405160_H
#define RADIALSLIDERTEXTVALUECHANGEDEVENT_T3907405160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RadialSlider/RadialSliderTextValueChangedEvent
struct  RadialSliderTextValueChangedEvent_t3907405160  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALSLIDERTEXTVALUECHANGEDEVENT_T3907405160_H
#ifndef COLORCHANGEDEVENT_T2990895397_H
#define COLORCHANGEDEVENT_T2990895397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t2990895397  : public UnityEvent_1_t2058742090
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T2990895397_H
#ifndef HREFCLICKEVENT_T1044804460_H
#define HREFCLICKEVENT_T1044804460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic/HrefClickEvent
struct  HrefClickEvent_t1044804460  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HREFCLICKEVENT_T1044804460_H
#ifndef SELECTIONTEXTCHANGEDEVENT_T934555890_H
#define SELECTIONTEXTCHANGEDEVENT_T934555890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent
struct  SelectionTextChangedEvent_t934555890  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONTEXTCHANGEDEVENT_T934555890_H
#ifndef SELECTIONCHANGEDEVENT_T3776471183_H
#define SELECTIONCHANGEDEVENT_T3776471183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent
struct  SelectionChangedEvent_t3776471183  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEDEVENT_T3776471183_H
#ifndef SELECTIONCHANGEDEVENT_T3626208507_H
#define SELECTIONCHANGEDEVENT_T3626208507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownList/SelectionChangedEvent
struct  SelectionChangedEvent_t3626208507  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEDEVENT_T3626208507_H
#ifndef COOLDOWNBUTTONEVENT_T1873823474_H
#define COOLDOWNBUTTONEVENT_T1873823474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent
struct  CooldownButtonEvent_t1873823474  : public UnityEvent_1_t3020313056
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOLDOWNBUTTONEVENT_T1873823474_H
#ifndef SELECTIONVALIDITYCHANGEDEVENT_T352594559_H
#define SELECTIONVALIDITYCHANGEDEVENT_T352594559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent
struct  SelectionValidityChangedEvent_t352594559  : public UnityEvent_1_t3863924733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONVALIDITYCHANGEDEVENT_T352594559_H
#ifndef AUTOCOMPLETESEARCHTYPE_T2324807822_H
#define AUTOCOMPLETESEARCHTYPE_T2324807822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteSearchType
struct  AutoCompleteSearchType_t2324807822 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.AutoCompleteSearchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AutoCompleteSearchType_t2324807822, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCOMPLETESEARCHTYPE_T2324807822_H
#ifndef COLORVALUES_T3669995107_H
#define COLORVALUES_T3669995107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorValues
struct  ColorValues_t3669995107 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.ColorPicker.ColorValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorValues_t3669995107, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T3669995107_H
#ifndef HSVCHANGEDEVENT_T1170297569_H
#define HSVCHANGEDEVENT_T1170297569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t1170297569  : public UnityEvent_3_t4197061729
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T1170297569_H
#ifndef REORDERABLELISTHANDLER_T1694188766_H
#define REORDERABLELISTHANDLER_T1694188766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler
struct  ReorderableListHandler_t1694188766  : public UnityEvent_1_t1653981686
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTHANDLER_T1694188766_H
#ifndef NAVIGATION_T1571958496_H
#define NAVIGATION_T1571958496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t1571958496 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1490392188 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnUp_1)); }
	inline Selectable_t1490392188 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1490392188 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnDown_2)); }
	inline Selectable_t1490392188 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1490392188 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnLeft_3)); }
	inline Selectable_t1490392188 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1490392188 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnRight_4)); }
	inline Selectable_t1490392188 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1490392188 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T1571958496_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef SVBOXSLIDER_T1950342511_H
#define SVBOXSLIDER_T1950342511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider
struct  SVBoxSlider_t1950342511  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::picker
	ColorPickerControl_t3621872136 * ___picker_2;
	// UnityEngine.UI.Extensions.BoxSlider UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::slider
	BoxSlider_t3758521666 * ___slider_3;
	// UnityEngine.UI.RawImage UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::image
	RawImage_t2749640213 * ___image_4;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::lastH
	float ___lastH_5;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::listen
	bool ___listen_6;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1950342511, ___picker_2)); }
	inline ColorPickerControl_t3621872136 * get_picker_2() const { return ___picker_2; }
	inline ColorPickerControl_t3621872136 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPickerControl_t3621872136 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1950342511, ___slider_3)); }
	inline BoxSlider_t3758521666 * get_slider_3() const { return ___slider_3; }
	inline BoxSlider_t3758521666 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(BoxSlider_t3758521666 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier((&___slider_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1950342511, ___image_4)); }
	inline RawImage_t2749640213 * get_image_4() const { return ___image_4; }
	inline RawImage_t2749640213 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(RawImage_t2749640213 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_lastH_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1950342511, ___lastH_5)); }
	inline float get_lastH_5() const { return ___lastH_5; }
	inline float* get_address_of_lastH_5() { return &___lastH_5; }
	inline void set_lastH_5(float value)
	{
		___lastH_5 = value;
	}

	inline static int32_t get_offset_of_listen_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1950342511, ___listen_6)); }
	inline bool get_listen_6() const { return ___listen_6; }
	inline bool* get_address_of_listen_6() { return &___listen_6; }
	inline void set_listen_6(bool value)
	{
		___listen_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T1950342511_H
#ifndef COLORLABEL_T2392041509_H
#define COLORLABEL_T2392041509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorLabel
struct  ColorLabel_t2392041509  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorLabel::picker
	ColorPickerControl_t3621872136 * ___picker_2;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorLabel::type
	int32_t ___type_3;
	// System.String UnityEngine.UI.Extensions.ColorPicker.ColorLabel::prefix
	String_t* ___prefix_4;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorLabel::minValue
	float ___minValue_5;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorLabel::maxValue
	float ___maxValue_6;
	// System.Int32 UnityEngine.UI.Extensions.ColorPicker.ColorLabel::precision
	int32_t ___precision_7;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ColorPicker.ColorLabel::label
	Text_t356221433 * ___label_8;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorLabel_t2392041509, ___picker_2)); }
	inline ColorPickerControl_t3621872136 * get_picker_2() const { return ___picker_2; }
	inline ColorPickerControl_t3621872136 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPickerControl_t3621872136 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorLabel_t2392041509, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_prefix_4() { return static_cast<int32_t>(offsetof(ColorLabel_t2392041509, ___prefix_4)); }
	inline String_t* get_prefix_4() const { return ___prefix_4; }
	inline String_t** get_address_of_prefix_4() { return &___prefix_4; }
	inline void set_prefix_4(String_t* value)
	{
		___prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_4), value);
	}

	inline static int32_t get_offset_of_minValue_5() { return static_cast<int32_t>(offsetof(ColorLabel_t2392041509, ___minValue_5)); }
	inline float get_minValue_5() const { return ___minValue_5; }
	inline float* get_address_of_minValue_5() { return &___minValue_5; }
	inline void set_minValue_5(float value)
	{
		___minValue_5 = value;
	}

	inline static int32_t get_offset_of_maxValue_6() { return static_cast<int32_t>(offsetof(ColorLabel_t2392041509, ___maxValue_6)); }
	inline float get_maxValue_6() const { return ___maxValue_6; }
	inline float* get_address_of_maxValue_6() { return &___maxValue_6; }
	inline void set_maxValue_6(float value)
	{
		___maxValue_6 = value;
	}

	inline static int32_t get_offset_of_precision_7() { return static_cast<int32_t>(offsetof(ColorLabel_t2392041509, ___precision_7)); }
	inline int32_t get_precision_7() const { return ___precision_7; }
	inline int32_t* get_address_of_precision_7() { return &___precision_7; }
	inline void set_precision_7(int32_t value)
	{
		___precision_7 = value;
	}

	inline static int32_t get_offset_of_label_8() { return static_cast<int32_t>(offsetof(ColorLabel_t2392041509, ___label_8)); }
	inline Text_t356221433 * get_label_8() const { return ___label_8; }
	inline Text_t356221433 ** get_address_of_label_8() { return &___label_8; }
	inline void set_label_8(Text_t356221433 * value)
	{
		___label_8 = value;
		Il2CppCodeGenWriteBarrier((&___label_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T2392041509_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef COLORPICKERCONTROL_T3621872136_H
#define COLORPICKERCONTROL_T3621872136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl
struct  ColorPickerControl_t3621872136  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_hue
	float ____hue_2;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_saturation
	float ____saturation_3;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_brightness
	float ____brightness_4;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_red
	float ____red_5;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_green
	float ____green_6;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_blue
	float ____blue_7;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_alpha
	float ____alpha_8;
	// ColorChangedEvent UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::onValueChanged
	ColorChangedEvent_t2990895397 * ___onValueChanged_9;
	// HSVChangedEvent UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::onHSVChanged
	HSVChangedEvent_t1170297569 * ___onHSVChanged_10;

public:
	inline static int32_t get_offset_of__hue_2() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ____hue_2)); }
	inline float get__hue_2() const { return ____hue_2; }
	inline float* get_address_of__hue_2() { return &____hue_2; }
	inline void set__hue_2(float value)
	{
		____hue_2 = value;
	}

	inline static int32_t get_offset_of__saturation_3() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ____saturation_3)); }
	inline float get__saturation_3() const { return ____saturation_3; }
	inline float* get_address_of__saturation_3() { return &____saturation_3; }
	inline void set__saturation_3(float value)
	{
		____saturation_3 = value;
	}

	inline static int32_t get_offset_of__brightness_4() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ____brightness_4)); }
	inline float get__brightness_4() const { return ____brightness_4; }
	inline float* get_address_of__brightness_4() { return &____brightness_4; }
	inline void set__brightness_4(float value)
	{
		____brightness_4 = value;
	}

	inline static int32_t get_offset_of__red_5() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ____red_5)); }
	inline float get__red_5() const { return ____red_5; }
	inline float* get_address_of__red_5() { return &____red_5; }
	inline void set__red_5(float value)
	{
		____red_5 = value;
	}

	inline static int32_t get_offset_of__green_6() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ____green_6)); }
	inline float get__green_6() const { return ____green_6; }
	inline float* get_address_of__green_6() { return &____green_6; }
	inline void set__green_6(float value)
	{
		____green_6 = value;
	}

	inline static int32_t get_offset_of__blue_7() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ____blue_7)); }
	inline float get__blue_7() const { return ____blue_7; }
	inline float* get_address_of__blue_7() { return &____blue_7; }
	inline void set__blue_7(float value)
	{
		____blue_7 = value;
	}

	inline static int32_t get_offset_of__alpha_8() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ____alpha_8)); }
	inline float get__alpha_8() const { return ____alpha_8; }
	inline float* get_address_of__alpha_8() { return &____alpha_8; }
	inline void set__alpha_8(float value)
	{
		____alpha_8 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_9() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ___onValueChanged_9)); }
	inline ColorChangedEvent_t2990895397 * get_onValueChanged_9() const { return ___onValueChanged_9; }
	inline ColorChangedEvent_t2990895397 ** get_address_of_onValueChanged_9() { return &___onValueChanged_9; }
	inline void set_onValueChanged_9(ColorChangedEvent_t2990895397 * value)
	{
		___onValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_9), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_10() { return static_cast<int32_t>(offsetof(ColorPickerControl_t3621872136, ___onHSVChanged_10)); }
	inline HSVChangedEvent_t1170297569 * get_onHSVChanged_10() const { return ___onHSVChanged_10; }
	inline HSVChangedEvent_t1170297569 ** get_address_of_onHSVChanged_10() { return &___onHSVChanged_10; }
	inline void set_onHSVChanged_10(HSVChangedEvent_t1170297569 * value)
	{
		___onHSVChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERCONTROL_T3621872136_H
#ifndef COLORIMAGE_T1119512210_H
#define COLORIMAGE_T1119512210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorImage
struct  ColorImage_t1119512210  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorImage::picker
	ColorPickerControl_t3621872136 * ___picker_2;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.ColorPicker.ColorImage::image
	Image_t2042527209 * ___image_3;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorImage_t1119512210, ___picker_2)); }
	inline ColorPickerControl_t3621872136 * get_picker_2() const { return ___picker_2; }
	inline ColorPickerControl_t3621872136 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPickerControl_t3621872136 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(ColorImage_t1119512210, ___image_3)); }
	inline Image_t2042527209 * get_image_3() const { return ___image_3; }
	inline Image_t2042527209 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t2042527209 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMAGE_T1119512210_H
#ifndef AUTOCOMPLETECOMBOBOX_T2713109875_H
#define AUTOCOMPLETECOMBOBOX_T2713109875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox
struct  AutoCompleteComboBox_t2713109875  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::disabledTextColor
	Color_t2020392075  ___disabledTextColor_2;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.AutoCompleteComboBox::<SelectedItem>k__BackingField
	DropDownListItem_t1818608950 * ___U3CSelectedItemU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::AvailableOptions
	List_1_t1398341365 * ___AvailableOptions_4;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_isPanelActive
	bool ____isPanelActive_5;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_hasDrawnOnce
	bool ____hasDrawnOnce_6;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.AutoCompleteComboBox::_mainInput
	InputField_t1631627530 * ____mainInput_7;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_inputRT
	RectTransform_t3349966182 * ____inputRT_8;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_rectTransform
	RectTransform_t3349966182 * ____rectTransform_9;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_overlayRT
	RectTransform_t3349966182 * ____overlayRT_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollPanelRT
	RectTransform_t3349966182 * ____scrollPanelRT_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollBarRT
	RectTransform_t3349966182 * ____scrollBarRT_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_slidingAreaRT
	RectTransform_t3349966182 * ____slidingAreaRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_itemsPanelRT
	RectTransform_t3349966182 * ____itemsPanelRT_14;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.AutoCompleteComboBox::_canvas
	Canvas_t209405766 * ____canvas_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_canvasRT
	RectTransform_t3349966182 * ____canvasRT_16;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_17;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::_panelItems
	List_1_t1398341365 * ____panelItems_18;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::_prunedPanelItems
	List_1_t1398341365 * ____prunedPanelItems_19;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> UnityEngine.UI.Extensions.AutoCompleteComboBox::panelObjects
	Dictionary_2_t3671312409 * ___panelObjects_20;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.AutoCompleteComboBox::itemTemplate
	GameObject_t1756533147 * ___itemTemplate_21;
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_22;
	// System.Single UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollBarWidth
	float ____scrollBarWidth_23;
	// System.Int32 UnityEngine.UI.Extensions.AutoCompleteComboBox::_itemsToDisplay
	int32_t ____itemsToDisplay_24;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::SelectFirstItemOnStart
	bool ___SelectFirstItemOnStart_25;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_ChangeInputTextColorBasedOnMatchingItems
	bool ____ChangeInputTextColorBasedOnMatchingItems_26;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::ValidSelectionTextColor
	Color_t2020392075  ___ValidSelectionTextColor_27;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::MatchingItemsRemainingTextColor
	Color_t2020392075  ___MatchingItemsRemainingTextColor_28;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::NoItemsRemainingTextColor
	Color_t2020392075  ___NoItemsRemainingTextColor_29;
	// UnityEngine.UI.Extensions.AutoCompleteSearchType UnityEngine.UI.Extensions.AutoCompleteComboBox::autocompleteSearchType
	int32_t ___autocompleteSearchType_30;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_selectionIsValid
	bool ____selectionIsValid_31;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionTextChanged
	SelectionTextChangedEvent_t934555890 * ___OnSelectionTextChanged_32;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionValidityChanged
	SelectionValidityChangedEvent_t352594559 * ___OnSelectionValidityChanged_33;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionChanged
	SelectionChangedEvent_t4042853089 * ___OnSelectionChanged_34;

public:
	inline static int32_t get_offset_of_disabledTextColor_2() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___disabledTextColor_2)); }
	inline Color_t2020392075  get_disabledTextColor_2() const { return ___disabledTextColor_2; }
	inline Color_t2020392075 * get_address_of_disabledTextColor_2() { return &___disabledTextColor_2; }
	inline void set_disabledTextColor_2(Color_t2020392075  value)
	{
		___disabledTextColor_2 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___U3CSelectedItemU3Ek__BackingField_3)); }
	inline DropDownListItem_t1818608950 * get_U3CSelectedItemU3Ek__BackingField_3() const { return ___U3CSelectedItemU3Ek__BackingField_3; }
	inline DropDownListItem_t1818608950 ** get_address_of_U3CSelectedItemU3Ek__BackingField_3() { return &___U3CSelectedItemU3Ek__BackingField_3; }
	inline void set_U3CSelectedItemU3Ek__BackingField_3(DropDownListItem_t1818608950 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_AvailableOptions_4() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___AvailableOptions_4)); }
	inline List_1_t1398341365 * get_AvailableOptions_4() const { return ___AvailableOptions_4; }
	inline List_1_t1398341365 ** get_address_of_AvailableOptions_4() { return &___AvailableOptions_4; }
	inline void set_AvailableOptions_4(List_1_t1398341365 * value)
	{
		___AvailableOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableOptions_4), value);
	}

	inline static int32_t get_offset_of__isPanelActive_5() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____isPanelActive_5)); }
	inline bool get__isPanelActive_5() const { return ____isPanelActive_5; }
	inline bool* get_address_of__isPanelActive_5() { return &____isPanelActive_5; }
	inline void set__isPanelActive_5(bool value)
	{
		____isPanelActive_5 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_6() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____hasDrawnOnce_6)); }
	inline bool get__hasDrawnOnce_6() const { return ____hasDrawnOnce_6; }
	inline bool* get_address_of__hasDrawnOnce_6() { return &____hasDrawnOnce_6; }
	inline void set__hasDrawnOnce_6(bool value)
	{
		____hasDrawnOnce_6 = value;
	}

	inline static int32_t get_offset_of__mainInput_7() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____mainInput_7)); }
	inline InputField_t1631627530 * get__mainInput_7() const { return ____mainInput_7; }
	inline InputField_t1631627530 ** get_address_of__mainInput_7() { return &____mainInput_7; }
	inline void set__mainInput_7(InputField_t1631627530 * value)
	{
		____mainInput_7 = value;
		Il2CppCodeGenWriteBarrier((&____mainInput_7), value);
	}

	inline static int32_t get_offset_of__inputRT_8() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____inputRT_8)); }
	inline RectTransform_t3349966182 * get__inputRT_8() const { return ____inputRT_8; }
	inline RectTransform_t3349966182 ** get_address_of__inputRT_8() { return &____inputRT_8; }
	inline void set__inputRT_8(RectTransform_t3349966182 * value)
	{
		____inputRT_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputRT_8), value);
	}

	inline static int32_t get_offset_of__rectTransform_9() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____rectTransform_9)); }
	inline RectTransform_t3349966182 * get__rectTransform_9() const { return ____rectTransform_9; }
	inline RectTransform_t3349966182 ** get_address_of__rectTransform_9() { return &____rectTransform_9; }
	inline void set__rectTransform_9(RectTransform_t3349966182 * value)
	{
		____rectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_9), value);
	}

	inline static int32_t get_offset_of__overlayRT_10() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____overlayRT_10)); }
	inline RectTransform_t3349966182 * get__overlayRT_10() const { return ____overlayRT_10; }
	inline RectTransform_t3349966182 ** get_address_of__overlayRT_10() { return &____overlayRT_10; }
	inline void set__overlayRT_10(RectTransform_t3349966182 * value)
	{
		____overlayRT_10 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_10), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_11() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____scrollPanelRT_11)); }
	inline RectTransform_t3349966182 * get__scrollPanelRT_11() const { return ____scrollPanelRT_11; }
	inline RectTransform_t3349966182 ** get_address_of__scrollPanelRT_11() { return &____scrollPanelRT_11; }
	inline void set__scrollPanelRT_11(RectTransform_t3349966182 * value)
	{
		____scrollPanelRT_11 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_11), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_12() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____scrollBarRT_12)); }
	inline RectTransform_t3349966182 * get__scrollBarRT_12() const { return ____scrollBarRT_12; }
	inline RectTransform_t3349966182 ** get_address_of__scrollBarRT_12() { return &____scrollBarRT_12; }
	inline void set__scrollBarRT_12(RectTransform_t3349966182 * value)
	{
		____scrollBarRT_12 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_12), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_13() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____slidingAreaRT_13)); }
	inline RectTransform_t3349966182 * get__slidingAreaRT_13() const { return ____slidingAreaRT_13; }
	inline RectTransform_t3349966182 ** get_address_of__slidingAreaRT_13() { return &____slidingAreaRT_13; }
	inline void set__slidingAreaRT_13(RectTransform_t3349966182 * value)
	{
		____slidingAreaRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_13), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_14() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____itemsPanelRT_14)); }
	inline RectTransform_t3349966182 * get__itemsPanelRT_14() const { return ____itemsPanelRT_14; }
	inline RectTransform_t3349966182 ** get_address_of__itemsPanelRT_14() { return &____itemsPanelRT_14; }
	inline void set__itemsPanelRT_14(RectTransform_t3349966182 * value)
	{
		____itemsPanelRT_14 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_14), value);
	}

	inline static int32_t get_offset_of__canvas_15() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____canvas_15)); }
	inline Canvas_t209405766 * get__canvas_15() const { return ____canvas_15; }
	inline Canvas_t209405766 ** get_address_of__canvas_15() { return &____canvas_15; }
	inline void set__canvas_15(Canvas_t209405766 * value)
	{
		____canvas_15 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_15), value);
	}

	inline static int32_t get_offset_of__canvasRT_16() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____canvasRT_16)); }
	inline RectTransform_t3349966182 * get__canvasRT_16() const { return ____canvasRT_16; }
	inline RectTransform_t3349966182 ** get_address_of__canvasRT_16() { return &____canvasRT_16; }
	inline void set__canvasRT_16(RectTransform_t3349966182 * value)
	{
		____canvasRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_16), value);
	}

	inline static int32_t get_offset_of__scrollRect_17() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____scrollRect_17)); }
	inline ScrollRect_t1199013257 * get__scrollRect_17() const { return ____scrollRect_17; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_17() { return &____scrollRect_17; }
	inline void set__scrollRect_17(ScrollRect_t1199013257 * value)
	{
		____scrollRect_17 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_17), value);
	}

	inline static int32_t get_offset_of__panelItems_18() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____panelItems_18)); }
	inline List_1_t1398341365 * get__panelItems_18() const { return ____panelItems_18; }
	inline List_1_t1398341365 ** get_address_of__panelItems_18() { return &____panelItems_18; }
	inline void set__panelItems_18(List_1_t1398341365 * value)
	{
		____panelItems_18 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_18), value);
	}

	inline static int32_t get_offset_of__prunedPanelItems_19() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____prunedPanelItems_19)); }
	inline List_1_t1398341365 * get__prunedPanelItems_19() const { return ____prunedPanelItems_19; }
	inline List_1_t1398341365 ** get_address_of__prunedPanelItems_19() { return &____prunedPanelItems_19; }
	inline void set__prunedPanelItems_19(List_1_t1398341365 * value)
	{
		____prunedPanelItems_19 = value;
		Il2CppCodeGenWriteBarrier((&____prunedPanelItems_19), value);
	}

	inline static int32_t get_offset_of_panelObjects_20() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___panelObjects_20)); }
	inline Dictionary_2_t3671312409 * get_panelObjects_20() const { return ___panelObjects_20; }
	inline Dictionary_2_t3671312409 ** get_address_of_panelObjects_20() { return &___panelObjects_20; }
	inline void set_panelObjects_20(Dictionary_2_t3671312409 * value)
	{
		___panelObjects_20 = value;
		Il2CppCodeGenWriteBarrier((&___panelObjects_20), value);
	}

	inline static int32_t get_offset_of_itemTemplate_21() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___itemTemplate_21)); }
	inline GameObject_t1756533147 * get_itemTemplate_21() const { return ___itemTemplate_21; }
	inline GameObject_t1756533147 ** get_address_of_itemTemplate_21() { return &___itemTemplate_21; }
	inline void set_itemTemplate_21(GameObject_t1756533147 * value)
	{
		___itemTemplate_21 = value;
		Il2CppCodeGenWriteBarrier((&___itemTemplate_21), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___U3CTextU3Ek__BackingField_22)); }
	inline String_t* get_U3CTextU3Ek__BackingField_22() const { return ___U3CTextU3Ek__BackingField_22; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_22() { return &___U3CTextU3Ek__BackingField_22; }
	inline void set_U3CTextU3Ek__BackingField_22(String_t* value)
	{
		___U3CTextU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_23() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____scrollBarWidth_23)); }
	inline float get__scrollBarWidth_23() const { return ____scrollBarWidth_23; }
	inline float* get_address_of__scrollBarWidth_23() { return &____scrollBarWidth_23; }
	inline void set__scrollBarWidth_23(float value)
	{
		____scrollBarWidth_23 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_24() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____itemsToDisplay_24)); }
	inline int32_t get__itemsToDisplay_24() const { return ____itemsToDisplay_24; }
	inline int32_t* get_address_of__itemsToDisplay_24() { return &____itemsToDisplay_24; }
	inline void set__itemsToDisplay_24(int32_t value)
	{
		____itemsToDisplay_24 = value;
	}

	inline static int32_t get_offset_of_SelectFirstItemOnStart_25() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___SelectFirstItemOnStart_25)); }
	inline bool get_SelectFirstItemOnStart_25() const { return ___SelectFirstItemOnStart_25; }
	inline bool* get_address_of_SelectFirstItemOnStart_25() { return &___SelectFirstItemOnStart_25; }
	inline void set_SelectFirstItemOnStart_25(bool value)
	{
		___SelectFirstItemOnStart_25 = value;
	}

	inline static int32_t get_offset_of__ChangeInputTextColorBasedOnMatchingItems_26() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____ChangeInputTextColorBasedOnMatchingItems_26)); }
	inline bool get__ChangeInputTextColorBasedOnMatchingItems_26() const { return ____ChangeInputTextColorBasedOnMatchingItems_26; }
	inline bool* get_address_of__ChangeInputTextColorBasedOnMatchingItems_26() { return &____ChangeInputTextColorBasedOnMatchingItems_26; }
	inline void set__ChangeInputTextColorBasedOnMatchingItems_26(bool value)
	{
		____ChangeInputTextColorBasedOnMatchingItems_26 = value;
	}

	inline static int32_t get_offset_of_ValidSelectionTextColor_27() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___ValidSelectionTextColor_27)); }
	inline Color_t2020392075  get_ValidSelectionTextColor_27() const { return ___ValidSelectionTextColor_27; }
	inline Color_t2020392075 * get_address_of_ValidSelectionTextColor_27() { return &___ValidSelectionTextColor_27; }
	inline void set_ValidSelectionTextColor_27(Color_t2020392075  value)
	{
		___ValidSelectionTextColor_27 = value;
	}

	inline static int32_t get_offset_of_MatchingItemsRemainingTextColor_28() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___MatchingItemsRemainingTextColor_28)); }
	inline Color_t2020392075  get_MatchingItemsRemainingTextColor_28() const { return ___MatchingItemsRemainingTextColor_28; }
	inline Color_t2020392075 * get_address_of_MatchingItemsRemainingTextColor_28() { return &___MatchingItemsRemainingTextColor_28; }
	inline void set_MatchingItemsRemainingTextColor_28(Color_t2020392075  value)
	{
		___MatchingItemsRemainingTextColor_28 = value;
	}

	inline static int32_t get_offset_of_NoItemsRemainingTextColor_29() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___NoItemsRemainingTextColor_29)); }
	inline Color_t2020392075  get_NoItemsRemainingTextColor_29() const { return ___NoItemsRemainingTextColor_29; }
	inline Color_t2020392075 * get_address_of_NoItemsRemainingTextColor_29() { return &___NoItemsRemainingTextColor_29; }
	inline void set_NoItemsRemainingTextColor_29(Color_t2020392075  value)
	{
		___NoItemsRemainingTextColor_29 = value;
	}

	inline static int32_t get_offset_of_autocompleteSearchType_30() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___autocompleteSearchType_30)); }
	inline int32_t get_autocompleteSearchType_30() const { return ___autocompleteSearchType_30; }
	inline int32_t* get_address_of_autocompleteSearchType_30() { return &___autocompleteSearchType_30; }
	inline void set_autocompleteSearchType_30(int32_t value)
	{
		___autocompleteSearchType_30 = value;
	}

	inline static int32_t get_offset_of__selectionIsValid_31() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ____selectionIsValid_31)); }
	inline bool get__selectionIsValid_31() const { return ____selectionIsValid_31; }
	inline bool* get_address_of__selectionIsValid_31() { return &____selectionIsValid_31; }
	inline void set__selectionIsValid_31(bool value)
	{
		____selectionIsValid_31 = value;
	}

	inline static int32_t get_offset_of_OnSelectionTextChanged_32() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___OnSelectionTextChanged_32)); }
	inline SelectionTextChangedEvent_t934555890 * get_OnSelectionTextChanged_32() const { return ___OnSelectionTextChanged_32; }
	inline SelectionTextChangedEvent_t934555890 ** get_address_of_OnSelectionTextChanged_32() { return &___OnSelectionTextChanged_32; }
	inline void set_OnSelectionTextChanged_32(SelectionTextChangedEvent_t934555890 * value)
	{
		___OnSelectionTextChanged_32 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionTextChanged_32), value);
	}

	inline static int32_t get_offset_of_OnSelectionValidityChanged_33() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___OnSelectionValidityChanged_33)); }
	inline SelectionValidityChangedEvent_t352594559 * get_OnSelectionValidityChanged_33() const { return ___OnSelectionValidityChanged_33; }
	inline SelectionValidityChangedEvent_t352594559 ** get_address_of_OnSelectionValidityChanged_33() { return &___OnSelectionValidityChanged_33; }
	inline void set_OnSelectionValidityChanged_33(SelectionValidityChangedEvent_t352594559 * value)
	{
		___OnSelectionValidityChanged_33 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionValidityChanged_33), value);
	}

	inline static int32_t get_offset_of_OnSelectionChanged_34() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2713109875, ___OnSelectionChanged_34)); }
	inline SelectionChangedEvent_t4042853089 * get_OnSelectionChanged_34() const { return ___OnSelectionChanged_34; }
	inline SelectionChangedEvent_t4042853089 ** get_address_of_OnSelectionChanged_34() { return &___OnSelectionChanged_34; }
	inline void set_OnSelectionChanged_34(SelectionChangedEvent_t4042853089 * value)
	{
		___OnSelectionChanged_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCOMPLETECOMBOBOX_T2713109875_H
#ifndef TESTADDINGPOINTS_T2423951840_H
#define TESTADDINGPOINTS_T2423951840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.TestAddingPoints
struct  TestAddingPoints_t2423951840  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.UILineRenderer UnityEngine.UI.Extensions.Examples.TestAddingPoints::LineRenderer
	UILineRenderer_t3031355003 * ___LineRenderer_2;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.TestAddingPoints::XValue
	Text_t356221433 * ___XValue_3;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.TestAddingPoints::YValue
	Text_t356221433 * ___YValue_4;

public:
	inline static int32_t get_offset_of_LineRenderer_2() { return static_cast<int32_t>(offsetof(TestAddingPoints_t2423951840, ___LineRenderer_2)); }
	inline UILineRenderer_t3031355003 * get_LineRenderer_2() const { return ___LineRenderer_2; }
	inline UILineRenderer_t3031355003 ** get_address_of_LineRenderer_2() { return &___LineRenderer_2; }
	inline void set_LineRenderer_2(UILineRenderer_t3031355003 * value)
	{
		___LineRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___LineRenderer_2), value);
	}

	inline static int32_t get_offset_of_XValue_3() { return static_cast<int32_t>(offsetof(TestAddingPoints_t2423951840, ___XValue_3)); }
	inline Text_t356221433 * get_XValue_3() const { return ___XValue_3; }
	inline Text_t356221433 ** get_address_of_XValue_3() { return &___XValue_3; }
	inline void set_XValue_3(Text_t356221433 * value)
	{
		___XValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___XValue_3), value);
	}

	inline static int32_t get_offset_of_YValue_4() { return static_cast<int32_t>(offsetof(TestAddingPoints_t2423951840, ___YValue_4)); }
	inline Text_t356221433 * get_YValue_4() const { return ___YValue_4; }
	inline Text_t356221433 ** get_address_of_YValue_4() { return &___YValue_4; }
	inline void set_YValue_4(Text_t356221433 * value)
	{
		___YValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___YValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTADDINGPOINTS_T2423951840_H
#ifndef LINERENDERERORBIT_T3939876451_H
#define LINERENDERERORBIT_T3939876451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.LineRendererOrbit
struct  LineRendererOrbit_t3939876451  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.UILineRenderer UnityEngine.UI.Extensions.Examples.LineRendererOrbit::lr
	UILineRenderer_t3031355003 * ___lr_2;
	// UnityEngine.UI.Extensions.Circle UnityEngine.UI.Extensions.Examples.LineRendererOrbit::circle
	Circle_t1341526964 * ___circle_3;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.LineRendererOrbit::OrbitGO
	GameObject_t1756533147 * ___OrbitGO_4;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.LineRendererOrbit::orbitGOrt
	RectTransform_t3349966182 * ___orbitGOrt_5;
	// System.Single UnityEngine.UI.Extensions.Examples.LineRendererOrbit::orbitTime
	float ___orbitTime_6;
	// System.Single UnityEngine.UI.Extensions.Examples.LineRendererOrbit::_xAxis
	float ____xAxis_7;
	// System.Single UnityEngine.UI.Extensions.Examples.LineRendererOrbit::_yAxis
	float ____yAxis_8;
	// System.Int32 UnityEngine.UI.Extensions.Examples.LineRendererOrbit::_steps
	int32_t ____steps_9;

public:
	inline static int32_t get_offset_of_lr_2() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ___lr_2)); }
	inline UILineRenderer_t3031355003 * get_lr_2() const { return ___lr_2; }
	inline UILineRenderer_t3031355003 ** get_address_of_lr_2() { return &___lr_2; }
	inline void set_lr_2(UILineRenderer_t3031355003 * value)
	{
		___lr_2 = value;
		Il2CppCodeGenWriteBarrier((&___lr_2), value);
	}

	inline static int32_t get_offset_of_circle_3() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ___circle_3)); }
	inline Circle_t1341526964 * get_circle_3() const { return ___circle_3; }
	inline Circle_t1341526964 ** get_address_of_circle_3() { return &___circle_3; }
	inline void set_circle_3(Circle_t1341526964 * value)
	{
		___circle_3 = value;
		Il2CppCodeGenWriteBarrier((&___circle_3), value);
	}

	inline static int32_t get_offset_of_OrbitGO_4() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ___OrbitGO_4)); }
	inline GameObject_t1756533147 * get_OrbitGO_4() const { return ___OrbitGO_4; }
	inline GameObject_t1756533147 ** get_address_of_OrbitGO_4() { return &___OrbitGO_4; }
	inline void set_OrbitGO_4(GameObject_t1756533147 * value)
	{
		___OrbitGO_4 = value;
		Il2CppCodeGenWriteBarrier((&___OrbitGO_4), value);
	}

	inline static int32_t get_offset_of_orbitGOrt_5() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ___orbitGOrt_5)); }
	inline RectTransform_t3349966182 * get_orbitGOrt_5() const { return ___orbitGOrt_5; }
	inline RectTransform_t3349966182 ** get_address_of_orbitGOrt_5() { return &___orbitGOrt_5; }
	inline void set_orbitGOrt_5(RectTransform_t3349966182 * value)
	{
		___orbitGOrt_5 = value;
		Il2CppCodeGenWriteBarrier((&___orbitGOrt_5), value);
	}

	inline static int32_t get_offset_of_orbitTime_6() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ___orbitTime_6)); }
	inline float get_orbitTime_6() const { return ___orbitTime_6; }
	inline float* get_address_of_orbitTime_6() { return &___orbitTime_6; }
	inline void set_orbitTime_6(float value)
	{
		___orbitTime_6 = value;
	}

	inline static int32_t get_offset_of__xAxis_7() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ____xAxis_7)); }
	inline float get__xAxis_7() const { return ____xAxis_7; }
	inline float* get_address_of__xAxis_7() { return &____xAxis_7; }
	inline void set__xAxis_7(float value)
	{
		____xAxis_7 = value;
	}

	inline static int32_t get_offset_of__yAxis_8() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ____yAxis_8)); }
	inline float get__yAxis_8() const { return ____yAxis_8; }
	inline float* get_address_of__yAxis_8() { return &____yAxis_8; }
	inline void set__yAxis_8(float value)
	{
		____yAxis_8 = value;
	}

	inline static int32_t get_offset_of__steps_9() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3939876451, ____steps_9)); }
	inline int32_t get__steps_9() const { return ____steps_9; }
	inline int32_t* get_address_of__steps_9() { return &____steps_9; }
	inline void set__steps_9(int32_t value)
	{
		____steps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERERORBIT_T3939876451_H
#ifndef ACCORDION_T2257195762_H
#define ACCORDION_T2257195762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Accordion
struct  Accordion_t2257195762  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.Accordion/Transition UnityEngine.UI.Extensions.Accordion::m_Transition
	int32_t ___m_Transition_2;
	// System.Single UnityEngine.UI.Extensions.Accordion::m_TransitionDuration
	float ___m_TransitionDuration_3;

public:
	inline static int32_t get_offset_of_m_Transition_2() { return static_cast<int32_t>(offsetof(Accordion_t2257195762, ___m_Transition_2)); }
	inline int32_t get_m_Transition_2() const { return ___m_Transition_2; }
	inline int32_t* get_address_of_m_Transition_2() { return &___m_Transition_2; }
	inline void set_m_Transition_2(int32_t value)
	{
		___m_Transition_2 = value;
	}

	inline static int32_t get_offset_of_m_TransitionDuration_3() { return static_cast<int32_t>(offsetof(Accordion_t2257195762, ___m_TransitionDuration_3)); }
	inline float get_m_TransitionDuration_3() const { return ___m_TransitionDuration_3; }
	inline float* get_address_of_m_TransitionDuration_3() { return &___m_TransitionDuration_3; }
	inline void set_m_TransitionDuration_3(float value)
	{
		___m_TransitionDuration_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCORDION_T2257195762_H
#ifndef SCROLLINGCALENDAR_T891181101_H
#define SCROLLINGCALENDAR_T891181101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.ScrollingCalendar
struct  ScrollingCalendar_t891181101  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsScrollingPanel
	RectTransform_t3349966182 * ___monthsScrollingPanel_2;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsScrollingPanel
	RectTransform_t3349966182 * ___yearsScrollingPanel_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysScrollingPanel
	RectTransform_t3349966182 * ___daysScrollingPanel_4;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsButtonPrefab
	GameObject_t1756533147 * ___yearsButtonPrefab_5;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsButtonPrefab
	GameObject_t1756533147 * ___monthsButtonPrefab_6;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysButtonPrefab
	GameObject_t1756533147 * ___daysButtonPrefab_7;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsButtons
	GameObjectU5BU5D_t3057952154* ___monthsButtons_8;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsButtons
	GameObjectU5BU5D_t3057952154* ___yearsButtons_9;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysButtons
	GameObjectU5BU5D_t3057952154* ___daysButtons_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthCenter
	RectTransform_t3349966182 * ___monthCenter_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsCenter
	RectTransform_t3349966182 * ___yearsCenter_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysCenter
	RectTransform_t3349966182 * ___daysCenter_13;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsVerticalScroller
	UIVerticalScroller_t1840308474 * ___yearsVerticalScroller_14;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsVerticalScroller
	UIVerticalScroller_t1840308474 * ___monthsVerticalScroller_15;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysVerticalScroller
	UIVerticalScroller_t1840308474 * ___daysVerticalScroller_16;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.ScrollingCalendar::inputFieldDays
	InputField_t1631627530 * ___inputFieldDays_17;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.ScrollingCalendar::inputFieldMonths
	InputField_t1631627530 * ___inputFieldMonths_18;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.ScrollingCalendar::inputFieldYears
	InputField_t1631627530 * ___inputFieldYears_19;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.ScrollingCalendar::dateText
	Text_t356221433 * ___dateText_20;
	// System.Int32 UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysSet
	int32_t ___daysSet_21;
	// System.Int32 UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsSet
	int32_t ___monthsSet_22;
	// System.Int32 UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsSet
	int32_t ___yearsSet_23;

public:
	inline static int32_t get_offset_of_monthsScrollingPanel_2() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___monthsScrollingPanel_2)); }
	inline RectTransform_t3349966182 * get_monthsScrollingPanel_2() const { return ___monthsScrollingPanel_2; }
	inline RectTransform_t3349966182 ** get_address_of_monthsScrollingPanel_2() { return &___monthsScrollingPanel_2; }
	inline void set_monthsScrollingPanel_2(RectTransform_t3349966182 * value)
	{
		___monthsScrollingPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___monthsScrollingPanel_2), value);
	}

	inline static int32_t get_offset_of_yearsScrollingPanel_3() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___yearsScrollingPanel_3)); }
	inline RectTransform_t3349966182 * get_yearsScrollingPanel_3() const { return ___yearsScrollingPanel_3; }
	inline RectTransform_t3349966182 ** get_address_of_yearsScrollingPanel_3() { return &___yearsScrollingPanel_3; }
	inline void set_yearsScrollingPanel_3(RectTransform_t3349966182 * value)
	{
		___yearsScrollingPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___yearsScrollingPanel_3), value);
	}

	inline static int32_t get_offset_of_daysScrollingPanel_4() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___daysScrollingPanel_4)); }
	inline RectTransform_t3349966182 * get_daysScrollingPanel_4() const { return ___daysScrollingPanel_4; }
	inline RectTransform_t3349966182 ** get_address_of_daysScrollingPanel_4() { return &___daysScrollingPanel_4; }
	inline void set_daysScrollingPanel_4(RectTransform_t3349966182 * value)
	{
		___daysScrollingPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___daysScrollingPanel_4), value);
	}

	inline static int32_t get_offset_of_yearsButtonPrefab_5() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___yearsButtonPrefab_5)); }
	inline GameObject_t1756533147 * get_yearsButtonPrefab_5() const { return ___yearsButtonPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_yearsButtonPrefab_5() { return &___yearsButtonPrefab_5; }
	inline void set_yearsButtonPrefab_5(GameObject_t1756533147 * value)
	{
		___yearsButtonPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___yearsButtonPrefab_5), value);
	}

	inline static int32_t get_offset_of_monthsButtonPrefab_6() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___monthsButtonPrefab_6)); }
	inline GameObject_t1756533147 * get_monthsButtonPrefab_6() const { return ___monthsButtonPrefab_6; }
	inline GameObject_t1756533147 ** get_address_of_monthsButtonPrefab_6() { return &___monthsButtonPrefab_6; }
	inline void set_monthsButtonPrefab_6(GameObject_t1756533147 * value)
	{
		___monthsButtonPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___monthsButtonPrefab_6), value);
	}

	inline static int32_t get_offset_of_daysButtonPrefab_7() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___daysButtonPrefab_7)); }
	inline GameObject_t1756533147 * get_daysButtonPrefab_7() const { return ___daysButtonPrefab_7; }
	inline GameObject_t1756533147 ** get_address_of_daysButtonPrefab_7() { return &___daysButtonPrefab_7; }
	inline void set_daysButtonPrefab_7(GameObject_t1756533147 * value)
	{
		___daysButtonPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___daysButtonPrefab_7), value);
	}

	inline static int32_t get_offset_of_monthsButtons_8() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___monthsButtons_8)); }
	inline GameObjectU5BU5D_t3057952154* get_monthsButtons_8() const { return ___monthsButtons_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_monthsButtons_8() { return &___monthsButtons_8; }
	inline void set_monthsButtons_8(GameObjectU5BU5D_t3057952154* value)
	{
		___monthsButtons_8 = value;
		Il2CppCodeGenWriteBarrier((&___monthsButtons_8), value);
	}

	inline static int32_t get_offset_of_yearsButtons_9() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___yearsButtons_9)); }
	inline GameObjectU5BU5D_t3057952154* get_yearsButtons_9() const { return ___yearsButtons_9; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_yearsButtons_9() { return &___yearsButtons_9; }
	inline void set_yearsButtons_9(GameObjectU5BU5D_t3057952154* value)
	{
		___yearsButtons_9 = value;
		Il2CppCodeGenWriteBarrier((&___yearsButtons_9), value);
	}

	inline static int32_t get_offset_of_daysButtons_10() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___daysButtons_10)); }
	inline GameObjectU5BU5D_t3057952154* get_daysButtons_10() const { return ___daysButtons_10; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_daysButtons_10() { return &___daysButtons_10; }
	inline void set_daysButtons_10(GameObjectU5BU5D_t3057952154* value)
	{
		___daysButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___daysButtons_10), value);
	}

	inline static int32_t get_offset_of_monthCenter_11() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___monthCenter_11)); }
	inline RectTransform_t3349966182 * get_monthCenter_11() const { return ___monthCenter_11; }
	inline RectTransform_t3349966182 ** get_address_of_monthCenter_11() { return &___monthCenter_11; }
	inline void set_monthCenter_11(RectTransform_t3349966182 * value)
	{
		___monthCenter_11 = value;
		Il2CppCodeGenWriteBarrier((&___monthCenter_11), value);
	}

	inline static int32_t get_offset_of_yearsCenter_12() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___yearsCenter_12)); }
	inline RectTransform_t3349966182 * get_yearsCenter_12() const { return ___yearsCenter_12; }
	inline RectTransform_t3349966182 ** get_address_of_yearsCenter_12() { return &___yearsCenter_12; }
	inline void set_yearsCenter_12(RectTransform_t3349966182 * value)
	{
		___yearsCenter_12 = value;
		Il2CppCodeGenWriteBarrier((&___yearsCenter_12), value);
	}

	inline static int32_t get_offset_of_daysCenter_13() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___daysCenter_13)); }
	inline RectTransform_t3349966182 * get_daysCenter_13() const { return ___daysCenter_13; }
	inline RectTransform_t3349966182 ** get_address_of_daysCenter_13() { return &___daysCenter_13; }
	inline void set_daysCenter_13(RectTransform_t3349966182 * value)
	{
		___daysCenter_13 = value;
		Il2CppCodeGenWriteBarrier((&___daysCenter_13), value);
	}

	inline static int32_t get_offset_of_yearsVerticalScroller_14() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___yearsVerticalScroller_14)); }
	inline UIVerticalScroller_t1840308474 * get_yearsVerticalScroller_14() const { return ___yearsVerticalScroller_14; }
	inline UIVerticalScroller_t1840308474 ** get_address_of_yearsVerticalScroller_14() { return &___yearsVerticalScroller_14; }
	inline void set_yearsVerticalScroller_14(UIVerticalScroller_t1840308474 * value)
	{
		___yearsVerticalScroller_14 = value;
		Il2CppCodeGenWriteBarrier((&___yearsVerticalScroller_14), value);
	}

	inline static int32_t get_offset_of_monthsVerticalScroller_15() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___monthsVerticalScroller_15)); }
	inline UIVerticalScroller_t1840308474 * get_monthsVerticalScroller_15() const { return ___monthsVerticalScroller_15; }
	inline UIVerticalScroller_t1840308474 ** get_address_of_monthsVerticalScroller_15() { return &___monthsVerticalScroller_15; }
	inline void set_monthsVerticalScroller_15(UIVerticalScroller_t1840308474 * value)
	{
		___monthsVerticalScroller_15 = value;
		Il2CppCodeGenWriteBarrier((&___monthsVerticalScroller_15), value);
	}

	inline static int32_t get_offset_of_daysVerticalScroller_16() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___daysVerticalScroller_16)); }
	inline UIVerticalScroller_t1840308474 * get_daysVerticalScroller_16() const { return ___daysVerticalScroller_16; }
	inline UIVerticalScroller_t1840308474 ** get_address_of_daysVerticalScroller_16() { return &___daysVerticalScroller_16; }
	inline void set_daysVerticalScroller_16(UIVerticalScroller_t1840308474 * value)
	{
		___daysVerticalScroller_16 = value;
		Il2CppCodeGenWriteBarrier((&___daysVerticalScroller_16), value);
	}

	inline static int32_t get_offset_of_inputFieldDays_17() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___inputFieldDays_17)); }
	inline InputField_t1631627530 * get_inputFieldDays_17() const { return ___inputFieldDays_17; }
	inline InputField_t1631627530 ** get_address_of_inputFieldDays_17() { return &___inputFieldDays_17; }
	inline void set_inputFieldDays_17(InputField_t1631627530 * value)
	{
		___inputFieldDays_17 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldDays_17), value);
	}

	inline static int32_t get_offset_of_inputFieldMonths_18() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___inputFieldMonths_18)); }
	inline InputField_t1631627530 * get_inputFieldMonths_18() const { return ___inputFieldMonths_18; }
	inline InputField_t1631627530 ** get_address_of_inputFieldMonths_18() { return &___inputFieldMonths_18; }
	inline void set_inputFieldMonths_18(InputField_t1631627530 * value)
	{
		___inputFieldMonths_18 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldMonths_18), value);
	}

	inline static int32_t get_offset_of_inputFieldYears_19() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___inputFieldYears_19)); }
	inline InputField_t1631627530 * get_inputFieldYears_19() const { return ___inputFieldYears_19; }
	inline InputField_t1631627530 ** get_address_of_inputFieldYears_19() { return &___inputFieldYears_19; }
	inline void set_inputFieldYears_19(InputField_t1631627530 * value)
	{
		___inputFieldYears_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldYears_19), value);
	}

	inline static int32_t get_offset_of_dateText_20() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___dateText_20)); }
	inline Text_t356221433 * get_dateText_20() const { return ___dateText_20; }
	inline Text_t356221433 ** get_address_of_dateText_20() { return &___dateText_20; }
	inline void set_dateText_20(Text_t356221433 * value)
	{
		___dateText_20 = value;
		Il2CppCodeGenWriteBarrier((&___dateText_20), value);
	}

	inline static int32_t get_offset_of_daysSet_21() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___daysSet_21)); }
	inline int32_t get_daysSet_21() const { return ___daysSet_21; }
	inline int32_t* get_address_of_daysSet_21() { return &___daysSet_21; }
	inline void set_daysSet_21(int32_t value)
	{
		___daysSet_21 = value;
	}

	inline static int32_t get_offset_of_monthsSet_22() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___monthsSet_22)); }
	inline int32_t get_monthsSet_22() const { return ___monthsSet_22; }
	inline int32_t* get_address_of_monthsSet_22() { return &___monthsSet_22; }
	inline void set_monthsSet_22(int32_t value)
	{
		___monthsSet_22 = value;
	}

	inline static int32_t get_offset_of_yearsSet_23() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t891181101, ___yearsSet_23)); }
	inline int32_t get_yearsSet_23() const { return ___yearsSet_23; }
	inline int32_t* get_address_of_yearsSet_23() { return &___yearsSet_23; }
	inline void set_yearsSet_23(int32_t value)
	{
		___yearsSet_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINGCALENDAR_T891181101_H
#ifndef TESTHREF_T3167327857_H
#define TESTHREF_T3167327857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.testHref
struct  testHref_t3167327857  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.TextPic UnityEngine.UI.Extensions.Examples.testHref::textPic
	TextPic_t1856600851 * ___textPic_2;

public:
	inline static int32_t get_offset_of_textPic_2() { return static_cast<int32_t>(offsetof(testHref_t3167327857, ___textPic_2)); }
	inline TextPic_t1856600851 * get_textPic_2() const { return ___textPic_2; }
	inline TextPic_t1856600851 ** get_address_of_textPic_2() { return &___textPic_2; }
	inline void set_textPic_2(TextPic_t1856600851 * value)
	{
		___textPic_2 = value;
		Il2CppCodeGenWriteBarrier((&___textPic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTHREF_T3167327857_H
#ifndef COMBOBOX_T414540299_H
#define COMBOBOX_T414540299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ComboBox
struct  ComboBox_t414540299  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.ComboBox::disabledTextColor
	Color_t2020392075  ___disabledTextColor_2;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.ComboBox::<SelectedItem>k__BackingField
	DropDownListItem_t1818608950 * ___U3CSelectedItemU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.ComboBox::AvailableOptions
	List_1_t1398341365 * ___AvailableOptions_4;
	// System.Single UnityEngine.UI.Extensions.ComboBox::_scrollBarWidth
	float ____scrollBarWidth_5;
	// System.Int32 UnityEngine.UI.Extensions.ComboBox::_itemsToDisplay
	int32_t ____itemsToDisplay_6;
	// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent UnityEngine.UI.Extensions.ComboBox::OnSelectionChanged
	SelectionChangedEvent_t3776471183 * ___OnSelectionChanged_7;
	// System.Boolean UnityEngine.UI.Extensions.ComboBox::_isPanelActive
	bool ____isPanelActive_8;
	// System.Boolean UnityEngine.UI.Extensions.ComboBox::_hasDrawnOnce
	bool ____hasDrawnOnce_9;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.ComboBox::_mainInput
	InputField_t1631627530 * ____mainInput_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_inputRT
	RectTransform_t3349966182 * ____inputRT_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_rectTransform
	RectTransform_t3349966182 * ____rectTransform_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_overlayRT
	RectTransform_t3349966182 * ____overlayRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_scrollPanelRT
	RectTransform_t3349966182 * ____scrollPanelRT_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_scrollBarRT
	RectTransform_t3349966182 * ____scrollBarRT_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_slidingAreaRT
	RectTransform_t3349966182 * ____slidingAreaRT_16;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_itemsPanelRT
	RectTransform_t3349966182 * ____itemsPanelRT_17;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.ComboBox::_canvas
	Canvas_t209405766 * ____canvas_18;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_canvasRT
	RectTransform_t3349966182 * ____canvasRT_19;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ComboBox::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_20;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.ComboBox::_panelItems
	List_1_t1398341365 * ____panelItems_21;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> UnityEngine.UI.Extensions.ComboBox::panelObjects
	Dictionary_2_t3671312409 * ___panelObjects_22;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ComboBox::itemTemplate
	GameObject_t1756533147 * ___itemTemplate_23;
	// System.String UnityEngine.UI.Extensions.ComboBox::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_disabledTextColor_2() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ___disabledTextColor_2)); }
	inline Color_t2020392075  get_disabledTextColor_2() const { return ___disabledTextColor_2; }
	inline Color_t2020392075 * get_address_of_disabledTextColor_2() { return &___disabledTextColor_2; }
	inline void set_disabledTextColor_2(Color_t2020392075  value)
	{
		___disabledTextColor_2 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ___U3CSelectedItemU3Ek__BackingField_3)); }
	inline DropDownListItem_t1818608950 * get_U3CSelectedItemU3Ek__BackingField_3() const { return ___U3CSelectedItemU3Ek__BackingField_3; }
	inline DropDownListItem_t1818608950 ** get_address_of_U3CSelectedItemU3Ek__BackingField_3() { return &___U3CSelectedItemU3Ek__BackingField_3; }
	inline void set_U3CSelectedItemU3Ek__BackingField_3(DropDownListItem_t1818608950 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_AvailableOptions_4() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ___AvailableOptions_4)); }
	inline List_1_t1398341365 * get_AvailableOptions_4() const { return ___AvailableOptions_4; }
	inline List_1_t1398341365 ** get_address_of_AvailableOptions_4() { return &___AvailableOptions_4; }
	inline void set_AvailableOptions_4(List_1_t1398341365 * value)
	{
		___AvailableOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableOptions_4), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_5() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____scrollBarWidth_5)); }
	inline float get__scrollBarWidth_5() const { return ____scrollBarWidth_5; }
	inline float* get_address_of__scrollBarWidth_5() { return &____scrollBarWidth_5; }
	inline void set__scrollBarWidth_5(float value)
	{
		____scrollBarWidth_5 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_6() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____itemsToDisplay_6)); }
	inline int32_t get__itemsToDisplay_6() const { return ____itemsToDisplay_6; }
	inline int32_t* get_address_of__itemsToDisplay_6() { return &____itemsToDisplay_6; }
	inline void set__itemsToDisplay_6(int32_t value)
	{
		____itemsToDisplay_6 = value;
	}

	inline static int32_t get_offset_of_OnSelectionChanged_7() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ___OnSelectionChanged_7)); }
	inline SelectionChangedEvent_t3776471183 * get_OnSelectionChanged_7() const { return ___OnSelectionChanged_7; }
	inline SelectionChangedEvent_t3776471183 ** get_address_of_OnSelectionChanged_7() { return &___OnSelectionChanged_7; }
	inline void set_OnSelectionChanged_7(SelectionChangedEvent_t3776471183 * value)
	{
		___OnSelectionChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_7), value);
	}

	inline static int32_t get_offset_of__isPanelActive_8() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____isPanelActive_8)); }
	inline bool get__isPanelActive_8() const { return ____isPanelActive_8; }
	inline bool* get_address_of__isPanelActive_8() { return &____isPanelActive_8; }
	inline void set__isPanelActive_8(bool value)
	{
		____isPanelActive_8 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_9() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____hasDrawnOnce_9)); }
	inline bool get__hasDrawnOnce_9() const { return ____hasDrawnOnce_9; }
	inline bool* get_address_of__hasDrawnOnce_9() { return &____hasDrawnOnce_9; }
	inline void set__hasDrawnOnce_9(bool value)
	{
		____hasDrawnOnce_9 = value;
	}

	inline static int32_t get_offset_of__mainInput_10() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____mainInput_10)); }
	inline InputField_t1631627530 * get__mainInput_10() const { return ____mainInput_10; }
	inline InputField_t1631627530 ** get_address_of__mainInput_10() { return &____mainInput_10; }
	inline void set__mainInput_10(InputField_t1631627530 * value)
	{
		____mainInput_10 = value;
		Il2CppCodeGenWriteBarrier((&____mainInput_10), value);
	}

	inline static int32_t get_offset_of__inputRT_11() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____inputRT_11)); }
	inline RectTransform_t3349966182 * get__inputRT_11() const { return ____inputRT_11; }
	inline RectTransform_t3349966182 ** get_address_of__inputRT_11() { return &____inputRT_11; }
	inline void set__inputRT_11(RectTransform_t3349966182 * value)
	{
		____inputRT_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputRT_11), value);
	}

	inline static int32_t get_offset_of__rectTransform_12() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____rectTransform_12)); }
	inline RectTransform_t3349966182 * get__rectTransform_12() const { return ____rectTransform_12; }
	inline RectTransform_t3349966182 ** get_address_of__rectTransform_12() { return &____rectTransform_12; }
	inline void set__rectTransform_12(RectTransform_t3349966182 * value)
	{
		____rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_12), value);
	}

	inline static int32_t get_offset_of__overlayRT_13() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____overlayRT_13)); }
	inline RectTransform_t3349966182 * get__overlayRT_13() const { return ____overlayRT_13; }
	inline RectTransform_t3349966182 ** get_address_of__overlayRT_13() { return &____overlayRT_13; }
	inline void set__overlayRT_13(RectTransform_t3349966182 * value)
	{
		____overlayRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_13), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_14() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____scrollPanelRT_14)); }
	inline RectTransform_t3349966182 * get__scrollPanelRT_14() const { return ____scrollPanelRT_14; }
	inline RectTransform_t3349966182 ** get_address_of__scrollPanelRT_14() { return &____scrollPanelRT_14; }
	inline void set__scrollPanelRT_14(RectTransform_t3349966182 * value)
	{
		____scrollPanelRT_14 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_14), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_15() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____scrollBarRT_15)); }
	inline RectTransform_t3349966182 * get__scrollBarRT_15() const { return ____scrollBarRT_15; }
	inline RectTransform_t3349966182 ** get_address_of__scrollBarRT_15() { return &____scrollBarRT_15; }
	inline void set__scrollBarRT_15(RectTransform_t3349966182 * value)
	{
		____scrollBarRT_15 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_15), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_16() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____slidingAreaRT_16)); }
	inline RectTransform_t3349966182 * get__slidingAreaRT_16() const { return ____slidingAreaRT_16; }
	inline RectTransform_t3349966182 ** get_address_of__slidingAreaRT_16() { return &____slidingAreaRT_16; }
	inline void set__slidingAreaRT_16(RectTransform_t3349966182 * value)
	{
		____slidingAreaRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_16), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_17() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____itemsPanelRT_17)); }
	inline RectTransform_t3349966182 * get__itemsPanelRT_17() const { return ____itemsPanelRT_17; }
	inline RectTransform_t3349966182 ** get_address_of__itemsPanelRT_17() { return &____itemsPanelRT_17; }
	inline void set__itemsPanelRT_17(RectTransform_t3349966182 * value)
	{
		____itemsPanelRT_17 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_17), value);
	}

	inline static int32_t get_offset_of__canvas_18() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____canvas_18)); }
	inline Canvas_t209405766 * get__canvas_18() const { return ____canvas_18; }
	inline Canvas_t209405766 ** get_address_of__canvas_18() { return &____canvas_18; }
	inline void set__canvas_18(Canvas_t209405766 * value)
	{
		____canvas_18 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_18), value);
	}

	inline static int32_t get_offset_of__canvasRT_19() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____canvasRT_19)); }
	inline RectTransform_t3349966182 * get__canvasRT_19() const { return ____canvasRT_19; }
	inline RectTransform_t3349966182 ** get_address_of__canvasRT_19() { return &____canvasRT_19; }
	inline void set__canvasRT_19(RectTransform_t3349966182 * value)
	{
		____canvasRT_19 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_19), value);
	}

	inline static int32_t get_offset_of__scrollRect_20() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____scrollRect_20)); }
	inline ScrollRect_t1199013257 * get__scrollRect_20() const { return ____scrollRect_20; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_20() { return &____scrollRect_20; }
	inline void set__scrollRect_20(ScrollRect_t1199013257 * value)
	{
		____scrollRect_20 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_20), value);
	}

	inline static int32_t get_offset_of__panelItems_21() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ____panelItems_21)); }
	inline List_1_t1398341365 * get__panelItems_21() const { return ____panelItems_21; }
	inline List_1_t1398341365 ** get_address_of__panelItems_21() { return &____panelItems_21; }
	inline void set__panelItems_21(List_1_t1398341365 * value)
	{
		____panelItems_21 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_21), value);
	}

	inline static int32_t get_offset_of_panelObjects_22() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ___panelObjects_22)); }
	inline Dictionary_2_t3671312409 * get_panelObjects_22() const { return ___panelObjects_22; }
	inline Dictionary_2_t3671312409 ** get_address_of_panelObjects_22() { return &___panelObjects_22; }
	inline void set_panelObjects_22(Dictionary_2_t3671312409 * value)
	{
		___panelObjects_22 = value;
		Il2CppCodeGenWriteBarrier((&___panelObjects_22), value);
	}

	inline static int32_t get_offset_of_itemTemplate_23() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ___itemTemplate_23)); }
	inline GameObject_t1756533147 * get_itemTemplate_23() const { return ___itemTemplate_23; }
	inline GameObject_t1756533147 ** get_address_of_itemTemplate_23() { return &___itemTemplate_23; }
	inline void set_itemTemplate_23(GameObject_t1756533147 * value)
	{
		___itemTemplate_23 = value;
		Il2CppCodeGenWriteBarrier((&___itemTemplate_23), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(ComboBox_t414540299, ___U3CTextU3Ek__BackingField_24)); }
	inline String_t* get_U3CTextU3Ek__BackingField_24() const { return ___U3CTextU3Ek__BackingField_24; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_24() { return &___U3CTextU3Ek__BackingField_24; }
	inline void set_U3CTextU3Ek__BackingField_24(String_t* value)
	{
		___U3CTextU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBOBOX_T414540299_H
#ifndef UPDATERADIALVALUE_T566688927_H
#define UPDATERADIALVALUE_T566688927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.UpdateRadialValue
struct  UpdateRadialValue_t566688927  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.UpdateRadialValue::input
	InputField_t1631627530 * ___input_2;
	// UnityEngine.UI.Extensions.RadialSlider UnityEngine.UI.Extensions.Examples.UpdateRadialValue::slider
	RadialSlider_t2099696204 * ___slider_3;

public:
	inline static int32_t get_offset_of_input_2() { return static_cast<int32_t>(offsetof(UpdateRadialValue_t566688927, ___input_2)); }
	inline InputField_t1631627530 * get_input_2() const { return ___input_2; }
	inline InputField_t1631627530 ** get_address_of_input_2() { return &___input_2; }
	inline void set_input_2(InputField_t1631627530 * value)
	{
		___input_2 = value;
		Il2CppCodeGenWriteBarrier((&___input_2), value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(UpdateRadialValue_t566688927, ___slider_3)); }
	inline RadialSlider_t2099696204 * get_slider_3() const { return ___slider_3; }
	inline RadialSlider_t2099696204 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(RadialSlider_t2099696204 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier((&___slider_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATERADIALVALUE_T566688927_H
#ifndef ANIMATEEFFECTS_T1556533669_H
#define ANIMATEEFFECTS_T1556533669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.AnimateEffects
struct  AnimateEffects_t1556533669  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.LetterSpacing UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacing
	LetterSpacing_t4139949939 * ___letterSpacing_2;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacingMax
	float ___letterSpacingMax_3;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacingMin
	float ___letterSpacingMin_4;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacingModifier
	float ___letterSpacingModifier_5;
	// UnityEngine.UI.Extensions.CurvedText UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedText
	CurvedText_t3665035540 * ___curvedText_6;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedTextMax
	float ___curvedTextMax_7;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedTextMin
	float ___curvedTextMin_8;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedTextModifier
	float ___curvedTextModifier_9;
	// UnityEngine.UI.Extensions.Gradient2 UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2
	Gradient2_t998070926 * ___gradient2_10;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2Max
	float ___gradient2Max_11;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2Min
	float ___gradient2Min_12;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2Modifier
	float ___gradient2Modifier_13;
	// UnityEngine.UI.Extensions.CylinderText UnityEngine.UI.Extensions.Examples.AnimateEffects::cylinderText
	CylinderText_t2066901577 * ___cylinderText_14;
	// UnityEngine.Transform UnityEngine.UI.Extensions.Examples.AnimateEffects::cylinderTextRT
	Transform_t3275118058 * ___cylinderTextRT_15;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.Examples.AnimateEffects::cylinderRotation
	Vector3_t2243707580  ___cylinderRotation_16;
	// UnityEngine.UI.Extensions.SoftMaskScript UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIM
	SoftMaskScript_t947627439 * ___SAUIM_17;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIMMax
	float ___SAUIMMax_18;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIMMin
	float ___SAUIMMin_19;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIMModifier
	float ___SAUIMModifier_20;

public:
	inline static int32_t get_offset_of_letterSpacing_2() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___letterSpacing_2)); }
	inline LetterSpacing_t4139949939 * get_letterSpacing_2() const { return ___letterSpacing_2; }
	inline LetterSpacing_t4139949939 ** get_address_of_letterSpacing_2() { return &___letterSpacing_2; }
	inline void set_letterSpacing_2(LetterSpacing_t4139949939 * value)
	{
		___letterSpacing_2 = value;
		Il2CppCodeGenWriteBarrier((&___letterSpacing_2), value);
	}

	inline static int32_t get_offset_of_letterSpacingMax_3() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___letterSpacingMax_3)); }
	inline float get_letterSpacingMax_3() const { return ___letterSpacingMax_3; }
	inline float* get_address_of_letterSpacingMax_3() { return &___letterSpacingMax_3; }
	inline void set_letterSpacingMax_3(float value)
	{
		___letterSpacingMax_3 = value;
	}

	inline static int32_t get_offset_of_letterSpacingMin_4() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___letterSpacingMin_4)); }
	inline float get_letterSpacingMin_4() const { return ___letterSpacingMin_4; }
	inline float* get_address_of_letterSpacingMin_4() { return &___letterSpacingMin_4; }
	inline void set_letterSpacingMin_4(float value)
	{
		___letterSpacingMin_4 = value;
	}

	inline static int32_t get_offset_of_letterSpacingModifier_5() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___letterSpacingModifier_5)); }
	inline float get_letterSpacingModifier_5() const { return ___letterSpacingModifier_5; }
	inline float* get_address_of_letterSpacingModifier_5() { return &___letterSpacingModifier_5; }
	inline void set_letterSpacingModifier_5(float value)
	{
		___letterSpacingModifier_5 = value;
	}

	inline static int32_t get_offset_of_curvedText_6() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___curvedText_6)); }
	inline CurvedText_t3665035540 * get_curvedText_6() const { return ___curvedText_6; }
	inline CurvedText_t3665035540 ** get_address_of_curvedText_6() { return &___curvedText_6; }
	inline void set_curvedText_6(CurvedText_t3665035540 * value)
	{
		___curvedText_6 = value;
		Il2CppCodeGenWriteBarrier((&___curvedText_6), value);
	}

	inline static int32_t get_offset_of_curvedTextMax_7() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___curvedTextMax_7)); }
	inline float get_curvedTextMax_7() const { return ___curvedTextMax_7; }
	inline float* get_address_of_curvedTextMax_7() { return &___curvedTextMax_7; }
	inline void set_curvedTextMax_7(float value)
	{
		___curvedTextMax_7 = value;
	}

	inline static int32_t get_offset_of_curvedTextMin_8() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___curvedTextMin_8)); }
	inline float get_curvedTextMin_8() const { return ___curvedTextMin_8; }
	inline float* get_address_of_curvedTextMin_8() { return &___curvedTextMin_8; }
	inline void set_curvedTextMin_8(float value)
	{
		___curvedTextMin_8 = value;
	}

	inline static int32_t get_offset_of_curvedTextModifier_9() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___curvedTextModifier_9)); }
	inline float get_curvedTextModifier_9() const { return ___curvedTextModifier_9; }
	inline float* get_address_of_curvedTextModifier_9() { return &___curvedTextModifier_9; }
	inline void set_curvedTextModifier_9(float value)
	{
		___curvedTextModifier_9 = value;
	}

	inline static int32_t get_offset_of_gradient2_10() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___gradient2_10)); }
	inline Gradient2_t998070926 * get_gradient2_10() const { return ___gradient2_10; }
	inline Gradient2_t998070926 ** get_address_of_gradient2_10() { return &___gradient2_10; }
	inline void set_gradient2_10(Gradient2_t998070926 * value)
	{
		___gradient2_10 = value;
		Il2CppCodeGenWriteBarrier((&___gradient2_10), value);
	}

	inline static int32_t get_offset_of_gradient2Max_11() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___gradient2Max_11)); }
	inline float get_gradient2Max_11() const { return ___gradient2Max_11; }
	inline float* get_address_of_gradient2Max_11() { return &___gradient2Max_11; }
	inline void set_gradient2Max_11(float value)
	{
		___gradient2Max_11 = value;
	}

	inline static int32_t get_offset_of_gradient2Min_12() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___gradient2Min_12)); }
	inline float get_gradient2Min_12() const { return ___gradient2Min_12; }
	inline float* get_address_of_gradient2Min_12() { return &___gradient2Min_12; }
	inline void set_gradient2Min_12(float value)
	{
		___gradient2Min_12 = value;
	}

	inline static int32_t get_offset_of_gradient2Modifier_13() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___gradient2Modifier_13)); }
	inline float get_gradient2Modifier_13() const { return ___gradient2Modifier_13; }
	inline float* get_address_of_gradient2Modifier_13() { return &___gradient2Modifier_13; }
	inline void set_gradient2Modifier_13(float value)
	{
		___gradient2Modifier_13 = value;
	}

	inline static int32_t get_offset_of_cylinderText_14() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___cylinderText_14)); }
	inline CylinderText_t2066901577 * get_cylinderText_14() const { return ___cylinderText_14; }
	inline CylinderText_t2066901577 ** get_address_of_cylinderText_14() { return &___cylinderText_14; }
	inline void set_cylinderText_14(CylinderText_t2066901577 * value)
	{
		___cylinderText_14 = value;
		Il2CppCodeGenWriteBarrier((&___cylinderText_14), value);
	}

	inline static int32_t get_offset_of_cylinderTextRT_15() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___cylinderTextRT_15)); }
	inline Transform_t3275118058 * get_cylinderTextRT_15() const { return ___cylinderTextRT_15; }
	inline Transform_t3275118058 ** get_address_of_cylinderTextRT_15() { return &___cylinderTextRT_15; }
	inline void set_cylinderTextRT_15(Transform_t3275118058 * value)
	{
		___cylinderTextRT_15 = value;
		Il2CppCodeGenWriteBarrier((&___cylinderTextRT_15), value);
	}

	inline static int32_t get_offset_of_cylinderRotation_16() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___cylinderRotation_16)); }
	inline Vector3_t2243707580  get_cylinderRotation_16() const { return ___cylinderRotation_16; }
	inline Vector3_t2243707580 * get_address_of_cylinderRotation_16() { return &___cylinderRotation_16; }
	inline void set_cylinderRotation_16(Vector3_t2243707580  value)
	{
		___cylinderRotation_16 = value;
	}

	inline static int32_t get_offset_of_SAUIM_17() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___SAUIM_17)); }
	inline SoftMaskScript_t947627439 * get_SAUIM_17() const { return ___SAUIM_17; }
	inline SoftMaskScript_t947627439 ** get_address_of_SAUIM_17() { return &___SAUIM_17; }
	inline void set_SAUIM_17(SoftMaskScript_t947627439 * value)
	{
		___SAUIM_17 = value;
		Il2CppCodeGenWriteBarrier((&___SAUIM_17), value);
	}

	inline static int32_t get_offset_of_SAUIMMax_18() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___SAUIMMax_18)); }
	inline float get_SAUIMMax_18() const { return ___SAUIMMax_18; }
	inline float* get_address_of_SAUIMMax_18() { return &___SAUIMMax_18; }
	inline void set_SAUIMMax_18(float value)
	{
		___SAUIMMax_18 = value;
	}

	inline static int32_t get_offset_of_SAUIMMin_19() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___SAUIMMin_19)); }
	inline float get_SAUIMMin_19() const { return ___SAUIMMin_19; }
	inline float* get_address_of_SAUIMMin_19() { return &___SAUIMMin_19; }
	inline void set_SAUIMMin_19(float value)
	{
		___SAUIMMin_19 = value;
	}

	inline static int32_t get_offset_of_SAUIMModifier_20() { return static_cast<int32_t>(offsetof(AnimateEffects_t1556533669, ___SAUIMModifier_20)); }
	inline float get_SAUIMModifier_20() const { return ___SAUIMModifier_20; }
	inline float* get_address_of_SAUIMModifier_20() { return &___SAUIMModifier_20; }
	inline void set_SAUIMModifier_20(float value)
	{
		___SAUIMModifier_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEEFFECTS_T1556533669_H
#ifndef TILTWINDOW_T2642812283_H
#define TILTWINDOW_T2642812283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TiltWindow
struct  TiltWindow_t2642812283  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TiltWindow::range
	Vector2_t2243707579  ___range_2;
	// UnityEngine.Transform UnityEngine.UI.Extensions.TiltWindow::mTrans
	Transform_t3275118058 * ___mTrans_3;
	// UnityEngine.Quaternion UnityEngine.UI.Extensions.TiltWindow::mStart
	Quaternion_t4030073918  ___mStart_4;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TiltWindow::mRot
	Vector2_t2243707579  ___mRot_5;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(TiltWindow_t2642812283, ___range_2)); }
	inline Vector2_t2243707579  get_range_2() const { return ___range_2; }
	inline Vector2_t2243707579 * get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(Vector2_t2243707579  value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(TiltWindow_t2642812283, ___mTrans_3)); }
	inline Transform_t3275118058 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3275118058 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3275118058 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mStart_4() { return static_cast<int32_t>(offsetof(TiltWindow_t2642812283, ___mStart_4)); }
	inline Quaternion_t4030073918  get_mStart_4() const { return ___mStart_4; }
	inline Quaternion_t4030073918 * get_address_of_mStart_4() { return &___mStart_4; }
	inline void set_mStart_4(Quaternion_t4030073918  value)
	{
		___mStart_4 = value;
	}

	inline static int32_t get_offset_of_mRot_5() { return static_cast<int32_t>(offsetof(TiltWindow_t2642812283, ___mRot_5)); }
	inline Vector2_t2243707579  get_mRot_5() const { return ___mRot_5; }
	inline Vector2_t2243707579 * get_address_of_mRot_5() { return &___mRot_5; }
	inline void set_mRot_5(Vector2_t2243707579  value)
	{
		___mRot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T2642812283_H
#ifndef REORDERABLELIST_T970849249_H
#define REORDERABLELIST_T970849249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableList
struct  ReorderableList_t970849249  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.LayoutGroup UnityEngine.UI.Extensions.ReorderableList::ContentLayout
	LayoutGroup_t3962498969 * ___ContentLayout_2;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableList::DraggableArea
	RectTransform_t3349966182 * ___DraggableArea_3;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList::IsDraggable
	bool ___IsDraggable_4;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList::CloneDraggedObject
	bool ___CloneDraggedObject_5;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList::IsDropable
	bool ___IsDropable_6;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementDropped
	ReorderableListHandler_t1694188766 * ___OnElementDropped_7;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementGrabbed
	ReorderableListHandler_t1694188766 * ___OnElementGrabbed_8;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementRemoved
	ReorderableListHandler_t1694188766 * ___OnElementRemoved_9;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementAdded
	ReorderableListHandler_t1694188766 * ___OnElementAdded_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableList::_content
	RectTransform_t3349966182 * ____content_11;
	// UnityEngine.UI.Extensions.ReorderableListContent UnityEngine.UI.Extensions.ReorderableList::_listContent
	ReorderableListContent_t133253858 * ____listContent_12;

public:
	inline static int32_t get_offset_of_ContentLayout_2() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___ContentLayout_2)); }
	inline LayoutGroup_t3962498969 * get_ContentLayout_2() const { return ___ContentLayout_2; }
	inline LayoutGroup_t3962498969 ** get_address_of_ContentLayout_2() { return &___ContentLayout_2; }
	inline void set_ContentLayout_2(LayoutGroup_t3962498969 * value)
	{
		___ContentLayout_2 = value;
		Il2CppCodeGenWriteBarrier((&___ContentLayout_2), value);
	}

	inline static int32_t get_offset_of_DraggableArea_3() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___DraggableArea_3)); }
	inline RectTransform_t3349966182 * get_DraggableArea_3() const { return ___DraggableArea_3; }
	inline RectTransform_t3349966182 ** get_address_of_DraggableArea_3() { return &___DraggableArea_3; }
	inline void set_DraggableArea_3(RectTransform_t3349966182 * value)
	{
		___DraggableArea_3 = value;
		Il2CppCodeGenWriteBarrier((&___DraggableArea_3), value);
	}

	inline static int32_t get_offset_of_IsDraggable_4() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___IsDraggable_4)); }
	inline bool get_IsDraggable_4() const { return ___IsDraggable_4; }
	inline bool* get_address_of_IsDraggable_4() { return &___IsDraggable_4; }
	inline void set_IsDraggable_4(bool value)
	{
		___IsDraggable_4 = value;
	}

	inline static int32_t get_offset_of_CloneDraggedObject_5() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___CloneDraggedObject_5)); }
	inline bool get_CloneDraggedObject_5() const { return ___CloneDraggedObject_5; }
	inline bool* get_address_of_CloneDraggedObject_5() { return &___CloneDraggedObject_5; }
	inline void set_CloneDraggedObject_5(bool value)
	{
		___CloneDraggedObject_5 = value;
	}

	inline static int32_t get_offset_of_IsDropable_6() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___IsDropable_6)); }
	inline bool get_IsDropable_6() const { return ___IsDropable_6; }
	inline bool* get_address_of_IsDropable_6() { return &___IsDropable_6; }
	inline void set_IsDropable_6(bool value)
	{
		___IsDropable_6 = value;
	}

	inline static int32_t get_offset_of_OnElementDropped_7() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___OnElementDropped_7)); }
	inline ReorderableListHandler_t1694188766 * get_OnElementDropped_7() const { return ___OnElementDropped_7; }
	inline ReorderableListHandler_t1694188766 ** get_address_of_OnElementDropped_7() { return &___OnElementDropped_7; }
	inline void set_OnElementDropped_7(ReorderableListHandler_t1694188766 * value)
	{
		___OnElementDropped_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementDropped_7), value);
	}

	inline static int32_t get_offset_of_OnElementGrabbed_8() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___OnElementGrabbed_8)); }
	inline ReorderableListHandler_t1694188766 * get_OnElementGrabbed_8() const { return ___OnElementGrabbed_8; }
	inline ReorderableListHandler_t1694188766 ** get_address_of_OnElementGrabbed_8() { return &___OnElementGrabbed_8; }
	inline void set_OnElementGrabbed_8(ReorderableListHandler_t1694188766 * value)
	{
		___OnElementGrabbed_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementGrabbed_8), value);
	}

	inline static int32_t get_offset_of_OnElementRemoved_9() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___OnElementRemoved_9)); }
	inline ReorderableListHandler_t1694188766 * get_OnElementRemoved_9() const { return ___OnElementRemoved_9; }
	inline ReorderableListHandler_t1694188766 ** get_address_of_OnElementRemoved_9() { return &___OnElementRemoved_9; }
	inline void set_OnElementRemoved_9(ReorderableListHandler_t1694188766 * value)
	{
		___OnElementRemoved_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementRemoved_9), value);
	}

	inline static int32_t get_offset_of_OnElementAdded_10() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ___OnElementAdded_10)); }
	inline ReorderableListHandler_t1694188766 * get_OnElementAdded_10() const { return ___OnElementAdded_10; }
	inline ReorderableListHandler_t1694188766 ** get_address_of_OnElementAdded_10() { return &___OnElementAdded_10; }
	inline void set_OnElementAdded_10(ReorderableListHandler_t1694188766 * value)
	{
		___OnElementAdded_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementAdded_10), value);
	}

	inline static int32_t get_offset_of__content_11() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ____content_11)); }
	inline RectTransform_t3349966182 * get__content_11() const { return ____content_11; }
	inline RectTransform_t3349966182 ** get_address_of__content_11() { return &____content_11; }
	inline void set__content_11(RectTransform_t3349966182 * value)
	{
		____content_11 = value;
		Il2CppCodeGenWriteBarrier((&____content_11), value);
	}

	inline static int32_t get_offset_of__listContent_12() { return static_cast<int32_t>(offsetof(ReorderableList_t970849249, ____listContent_12)); }
	inline ReorderableListContent_t133253858 * get__listContent_12() const { return ____listContent_12; }
	inline ReorderableListContent_t133253858 ** get_address_of__listContent_12() { return &____listContent_12; }
	inline void set__listContent_12(ReorderableListContent_t133253858 * value)
	{
		____listContent_12 = value;
		Il2CppCodeGenWriteBarrier((&____listContent_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELIST_T970849249_H
#ifndef REORDERABLELISTCONTENT_T133253858_H
#define REORDERABLELISTCONTENT_T133253858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListContent
struct  ReorderableListContent_t133253858  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.UI.Extensions.ReorderableListContent::_cachedChildren
	List_1_t2644239190 * ____cachedChildren_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.ReorderableListElement> UnityEngine.UI.Extensions.ReorderableListContent::_cachedListElement
	List_1_t2496814557 * ____cachedListElement_3;
	// UnityEngine.UI.Extensions.ReorderableListElement UnityEngine.UI.Extensions.ReorderableListContent::_ele
	ReorderableListElement_t3127693425 * ____ele_4;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableListContent::_extList
	ReorderableList_t970849249 * ____extList_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListContent::_rect
	RectTransform_t3349966182 * ____rect_6;

public:
	inline static int32_t get_offset_of__cachedChildren_2() { return static_cast<int32_t>(offsetof(ReorderableListContent_t133253858, ____cachedChildren_2)); }
	inline List_1_t2644239190 * get__cachedChildren_2() const { return ____cachedChildren_2; }
	inline List_1_t2644239190 ** get_address_of__cachedChildren_2() { return &____cachedChildren_2; }
	inline void set__cachedChildren_2(List_1_t2644239190 * value)
	{
		____cachedChildren_2 = value;
		Il2CppCodeGenWriteBarrier((&____cachedChildren_2), value);
	}

	inline static int32_t get_offset_of__cachedListElement_3() { return static_cast<int32_t>(offsetof(ReorderableListContent_t133253858, ____cachedListElement_3)); }
	inline List_1_t2496814557 * get__cachedListElement_3() const { return ____cachedListElement_3; }
	inline List_1_t2496814557 ** get_address_of__cachedListElement_3() { return &____cachedListElement_3; }
	inline void set__cachedListElement_3(List_1_t2496814557 * value)
	{
		____cachedListElement_3 = value;
		Il2CppCodeGenWriteBarrier((&____cachedListElement_3), value);
	}

	inline static int32_t get_offset_of__ele_4() { return static_cast<int32_t>(offsetof(ReorderableListContent_t133253858, ____ele_4)); }
	inline ReorderableListElement_t3127693425 * get__ele_4() const { return ____ele_4; }
	inline ReorderableListElement_t3127693425 ** get_address_of__ele_4() { return &____ele_4; }
	inline void set__ele_4(ReorderableListElement_t3127693425 * value)
	{
		____ele_4 = value;
		Il2CppCodeGenWriteBarrier((&____ele_4), value);
	}

	inline static int32_t get_offset_of__extList_5() { return static_cast<int32_t>(offsetof(ReorderableListContent_t133253858, ____extList_5)); }
	inline ReorderableList_t970849249 * get__extList_5() const { return ____extList_5; }
	inline ReorderableList_t970849249 ** get_address_of__extList_5() { return &____extList_5; }
	inline void set__extList_5(ReorderableList_t970849249 * value)
	{
		____extList_5 = value;
		Il2CppCodeGenWriteBarrier((&____extList_5), value);
	}

	inline static int32_t get_offset_of__rect_6() { return static_cast<int32_t>(offsetof(ReorderableListContent_t133253858, ____rect_6)); }
	inline RectTransform_t3349966182 * get__rect_6() const { return ____rect_6; }
	inline RectTransform_t3349966182 ** get_address_of__rect_6() { return &____rect_6; }
	inline void set__rect_6(RectTransform_t3349966182 * value)
	{
		____rect_6 = value;
		Il2CppCodeGenWriteBarrier((&____rect_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTCONTENT_T133253858_H
#ifndef COLORSLIDERIMAGE_T4154176545_H
#define COLORSLIDERIMAGE_T4154176545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage
struct  ColorSliderImage_t4154176545  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::picker
	ColorPickerControl_t3621872136 * ___picker_2;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::direction
	int32_t ___direction_4;
	// UnityEngine.UI.RawImage UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::image
	RawImage_t2749640213 * ___image_5;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorSliderImage_t4154176545, ___picker_2)); }
	inline ColorPickerControl_t3621872136 * get_picker_2() const { return ___picker_2; }
	inline ColorPickerControl_t3621872136 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPickerControl_t3621872136 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSliderImage_t4154176545, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t4154176545, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t4154176545, ___image_5)); }
	inline RawImage_t2749640213 * get_image_5() const { return ___image_5; }
	inline RawImage_t2749640213 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t2749640213 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T4154176545_H
#ifndef RESCALEPANEL_T1108500937_H
#define RESCALEPANEL_T1108500937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RescalePanel
struct  RescalePanel_t1108500937  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::minSize
	Vector2_t2243707579  ___minSize_2;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::maxSize
	Vector2_t2243707579  ___maxSize_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescalePanel::rectTransform
	RectTransform_t3349966182 * ___rectTransform_4;
	// UnityEngine.Transform UnityEngine.UI.Extensions.RescalePanel::goTransform
	Transform_t3275118058 * ___goTransform_5;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::currentPointerPosition
	Vector2_t2243707579  ___currentPointerPosition_6;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::previousPointerPosition
	Vector2_t2243707579  ___previousPointerPosition_7;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescalePanel::thisRectTransform
	RectTransform_t3349966182 * ___thisRectTransform_8;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::sizeDelta
	Vector2_t2243707579  ___sizeDelta_9;

public:
	inline static int32_t get_offset_of_minSize_2() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___minSize_2)); }
	inline Vector2_t2243707579  get_minSize_2() const { return ___minSize_2; }
	inline Vector2_t2243707579 * get_address_of_minSize_2() { return &___minSize_2; }
	inline void set_minSize_2(Vector2_t2243707579  value)
	{
		___minSize_2 = value;
	}

	inline static int32_t get_offset_of_maxSize_3() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___maxSize_3)); }
	inline Vector2_t2243707579  get_maxSize_3() const { return ___maxSize_3; }
	inline Vector2_t2243707579 * get_address_of_maxSize_3() { return &___maxSize_3; }
	inline void set_maxSize_3(Vector2_t2243707579  value)
	{
		___maxSize_3 = value;
	}

	inline static int32_t get_offset_of_rectTransform_4() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___rectTransform_4)); }
	inline RectTransform_t3349966182 * get_rectTransform_4() const { return ___rectTransform_4; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_4() { return &___rectTransform_4; }
	inline void set_rectTransform_4(RectTransform_t3349966182 * value)
	{
		___rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_4), value);
	}

	inline static int32_t get_offset_of_goTransform_5() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___goTransform_5)); }
	inline Transform_t3275118058 * get_goTransform_5() const { return ___goTransform_5; }
	inline Transform_t3275118058 ** get_address_of_goTransform_5() { return &___goTransform_5; }
	inline void set_goTransform_5(Transform_t3275118058 * value)
	{
		___goTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___goTransform_5), value);
	}

	inline static int32_t get_offset_of_currentPointerPosition_6() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___currentPointerPosition_6)); }
	inline Vector2_t2243707579  get_currentPointerPosition_6() const { return ___currentPointerPosition_6; }
	inline Vector2_t2243707579 * get_address_of_currentPointerPosition_6() { return &___currentPointerPosition_6; }
	inline void set_currentPointerPosition_6(Vector2_t2243707579  value)
	{
		___currentPointerPosition_6 = value;
	}

	inline static int32_t get_offset_of_previousPointerPosition_7() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___previousPointerPosition_7)); }
	inline Vector2_t2243707579  get_previousPointerPosition_7() const { return ___previousPointerPosition_7; }
	inline Vector2_t2243707579 * get_address_of_previousPointerPosition_7() { return &___previousPointerPosition_7; }
	inline void set_previousPointerPosition_7(Vector2_t2243707579  value)
	{
		___previousPointerPosition_7 = value;
	}

	inline static int32_t get_offset_of_thisRectTransform_8() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___thisRectTransform_8)); }
	inline RectTransform_t3349966182 * get_thisRectTransform_8() const { return ___thisRectTransform_8; }
	inline RectTransform_t3349966182 ** get_address_of_thisRectTransform_8() { return &___thisRectTransform_8; }
	inline void set_thisRectTransform_8(RectTransform_t3349966182 * value)
	{
		___thisRectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___thisRectTransform_8), value);
	}

	inline static int32_t get_offset_of_sizeDelta_9() { return static_cast<int32_t>(offsetof(RescalePanel_t1108500937, ___sizeDelta_9)); }
	inline Vector2_t2243707579  get_sizeDelta_9() const { return ___sizeDelta_9; }
	inline Vector2_t2243707579 * get_address_of_sizeDelta_9() { return &___sizeDelta_9; }
	inline void set_sizeDelta_9(Vector2_t2243707579  value)
	{
		___sizeDelta_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESCALEPANEL_T1108500937_H
#ifndef UI_KNOB_T1007033269_H
#define UI_KNOB_T1007033269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_Knob
struct  UI_Knob_t1007033269  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.UI_Knob/Direction UnityEngine.UI.Extensions.UI_Knob::direction
	int32_t ___direction_2;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::knobValue
	float ___knobValue_3;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::maxValue
	float ___maxValue_4;
	// System.Int32 UnityEngine.UI.Extensions.UI_Knob::loops
	int32_t ___loops_5;
	// System.Boolean UnityEngine.UI.Extensions.UI_Knob::clampOutput01
	bool ___clampOutput01_6;
	// System.Boolean UnityEngine.UI.Extensions.UI_Knob::snapToPosition
	bool ___snapToPosition_7;
	// System.Int32 UnityEngine.UI.Extensions.UI_Knob::snapStepsPerLoop
	int32_t ___snapStepsPerLoop_8;
	// UnityEngine.UI.Extensions.KnobFloatValueEvent UnityEngine.UI.Extensions.UI_Knob::OnValueChanged
	KnobFloatValueEvent_t2705095779 * ___OnValueChanged_9;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_currentLoops
	float ____currentLoops_10;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_previousValue
	float ____previousValue_11;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_initAngle
	float ____initAngle_12;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_currentAngle
	float ____currentAngle_13;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UI_Knob::_currentVector
	Vector2_t2243707579  ____currentVector_14;
	// UnityEngine.Quaternion UnityEngine.UI.Extensions.UI_Knob::_initRotation
	Quaternion_t4030073918  ____initRotation_15;
	// System.Boolean UnityEngine.UI.Extensions.UI_Knob::_canDrag
	bool ____canDrag_16;

public:
	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___direction_2)); }
	inline int32_t get_direction_2() const { return ___direction_2; }
	inline int32_t* get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(int32_t value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_knobValue_3() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___knobValue_3)); }
	inline float get_knobValue_3() const { return ___knobValue_3; }
	inline float* get_address_of_knobValue_3() { return &___knobValue_3; }
	inline void set_knobValue_3(float value)
	{
		___knobValue_3 = value;
	}

	inline static int32_t get_offset_of_maxValue_4() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___maxValue_4)); }
	inline float get_maxValue_4() const { return ___maxValue_4; }
	inline float* get_address_of_maxValue_4() { return &___maxValue_4; }
	inline void set_maxValue_4(float value)
	{
		___maxValue_4 = value;
	}

	inline static int32_t get_offset_of_loops_5() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___loops_5)); }
	inline int32_t get_loops_5() const { return ___loops_5; }
	inline int32_t* get_address_of_loops_5() { return &___loops_5; }
	inline void set_loops_5(int32_t value)
	{
		___loops_5 = value;
	}

	inline static int32_t get_offset_of_clampOutput01_6() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___clampOutput01_6)); }
	inline bool get_clampOutput01_6() const { return ___clampOutput01_6; }
	inline bool* get_address_of_clampOutput01_6() { return &___clampOutput01_6; }
	inline void set_clampOutput01_6(bool value)
	{
		___clampOutput01_6 = value;
	}

	inline static int32_t get_offset_of_snapToPosition_7() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___snapToPosition_7)); }
	inline bool get_snapToPosition_7() const { return ___snapToPosition_7; }
	inline bool* get_address_of_snapToPosition_7() { return &___snapToPosition_7; }
	inline void set_snapToPosition_7(bool value)
	{
		___snapToPosition_7 = value;
	}

	inline static int32_t get_offset_of_snapStepsPerLoop_8() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___snapStepsPerLoop_8)); }
	inline int32_t get_snapStepsPerLoop_8() const { return ___snapStepsPerLoop_8; }
	inline int32_t* get_address_of_snapStepsPerLoop_8() { return &___snapStepsPerLoop_8; }
	inline void set_snapStepsPerLoop_8(int32_t value)
	{
		___snapStepsPerLoop_8 = value;
	}

	inline static int32_t get_offset_of_OnValueChanged_9() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ___OnValueChanged_9)); }
	inline KnobFloatValueEvent_t2705095779 * get_OnValueChanged_9() const { return ___OnValueChanged_9; }
	inline KnobFloatValueEvent_t2705095779 ** get_address_of_OnValueChanged_9() { return &___OnValueChanged_9; }
	inline void set_OnValueChanged_9(KnobFloatValueEvent_t2705095779 * value)
	{
		___OnValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_9), value);
	}

	inline static int32_t get_offset_of__currentLoops_10() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ____currentLoops_10)); }
	inline float get__currentLoops_10() const { return ____currentLoops_10; }
	inline float* get_address_of__currentLoops_10() { return &____currentLoops_10; }
	inline void set__currentLoops_10(float value)
	{
		____currentLoops_10 = value;
	}

	inline static int32_t get_offset_of__previousValue_11() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ____previousValue_11)); }
	inline float get__previousValue_11() const { return ____previousValue_11; }
	inline float* get_address_of__previousValue_11() { return &____previousValue_11; }
	inline void set__previousValue_11(float value)
	{
		____previousValue_11 = value;
	}

	inline static int32_t get_offset_of__initAngle_12() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ____initAngle_12)); }
	inline float get__initAngle_12() const { return ____initAngle_12; }
	inline float* get_address_of__initAngle_12() { return &____initAngle_12; }
	inline void set__initAngle_12(float value)
	{
		____initAngle_12 = value;
	}

	inline static int32_t get_offset_of__currentAngle_13() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ____currentAngle_13)); }
	inline float get__currentAngle_13() const { return ____currentAngle_13; }
	inline float* get_address_of__currentAngle_13() { return &____currentAngle_13; }
	inline void set__currentAngle_13(float value)
	{
		____currentAngle_13 = value;
	}

	inline static int32_t get_offset_of__currentVector_14() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ____currentVector_14)); }
	inline Vector2_t2243707579  get__currentVector_14() const { return ____currentVector_14; }
	inline Vector2_t2243707579 * get_address_of__currentVector_14() { return &____currentVector_14; }
	inline void set__currentVector_14(Vector2_t2243707579  value)
	{
		____currentVector_14 = value;
	}

	inline static int32_t get_offset_of__initRotation_15() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ____initRotation_15)); }
	inline Quaternion_t4030073918  get__initRotation_15() const { return ____initRotation_15; }
	inline Quaternion_t4030073918 * get_address_of__initRotation_15() { return &____initRotation_15; }
	inline void set__initRotation_15(Quaternion_t4030073918  value)
	{
		____initRotation_15 = value;
	}

	inline static int32_t get_offset_of__canDrag_16() { return static_cast<int32_t>(offsetof(UI_Knob_t1007033269, ____canDrag_16)); }
	inline bool get__canDrag_16() const { return ____canDrag_16; }
	inline bool* get_address_of__canDrag_16() { return &____canDrag_16; }
	inline void set__canDrag_16(bool value)
	{
		____canDrag_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_KNOB_T1007033269_H
#ifndef SELECTIONBOX_T3294027723_H
#define SELECTIONBOX_T3294027723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SelectionBox
struct  SelectionBox_t3294027723  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.SelectionBox::color
	Color_t2020392075  ___color_2;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.SelectionBox::art
	Sprite_t309593783 * ___art_3;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.SelectionBox::origin
	Vector2_t2243707579  ___origin_4;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.SelectionBox::selectionMask
	RectTransform_t3349966182 * ___selectionMask_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.SelectionBox::boxRect
	RectTransform_t3349966182 * ___boxRect_6;
	// UnityEngine.UI.Extensions.IBoxSelectable[] UnityEngine.UI.Extensions.SelectionBox::selectables
	IBoxSelectableU5BU5D_t1290335761* ___selectables_7;
	// UnityEngine.MonoBehaviour[] UnityEngine.UI.Extensions.SelectionBox::selectableGroup
	MonoBehaviourU5BU5D_t3035069757* ___selectableGroup_8;
	// UnityEngine.UI.Extensions.IBoxSelectable UnityEngine.UI.Extensions.SelectionBox::clickedBeforeDrag
	RuntimeObject* ___clickedBeforeDrag_9;
	// UnityEngine.UI.Extensions.IBoxSelectable UnityEngine.UI.Extensions.SelectionBox::clickedAfterDrag
	RuntimeObject* ___clickedAfterDrag_10;
	// UnityEngine.UI.Extensions.SelectionBox/SelectionEvent UnityEngine.UI.Extensions.SelectionBox::onSelectionChange
	SelectionEvent_t1038911497 * ___onSelectionChange_11;

public:
	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___color_2)); }
	inline Color_t2020392075  get_color_2() const { return ___color_2; }
	inline Color_t2020392075 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color_t2020392075  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_art_3() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___art_3)); }
	inline Sprite_t309593783 * get_art_3() const { return ___art_3; }
	inline Sprite_t309593783 ** get_address_of_art_3() { return &___art_3; }
	inline void set_art_3(Sprite_t309593783 * value)
	{
		___art_3 = value;
		Il2CppCodeGenWriteBarrier((&___art_3), value);
	}

	inline static int32_t get_offset_of_origin_4() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___origin_4)); }
	inline Vector2_t2243707579  get_origin_4() const { return ___origin_4; }
	inline Vector2_t2243707579 * get_address_of_origin_4() { return &___origin_4; }
	inline void set_origin_4(Vector2_t2243707579  value)
	{
		___origin_4 = value;
	}

	inline static int32_t get_offset_of_selectionMask_5() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___selectionMask_5)); }
	inline RectTransform_t3349966182 * get_selectionMask_5() const { return ___selectionMask_5; }
	inline RectTransform_t3349966182 ** get_address_of_selectionMask_5() { return &___selectionMask_5; }
	inline void set_selectionMask_5(RectTransform_t3349966182 * value)
	{
		___selectionMask_5 = value;
		Il2CppCodeGenWriteBarrier((&___selectionMask_5), value);
	}

	inline static int32_t get_offset_of_boxRect_6() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___boxRect_6)); }
	inline RectTransform_t3349966182 * get_boxRect_6() const { return ___boxRect_6; }
	inline RectTransform_t3349966182 ** get_address_of_boxRect_6() { return &___boxRect_6; }
	inline void set_boxRect_6(RectTransform_t3349966182 * value)
	{
		___boxRect_6 = value;
		Il2CppCodeGenWriteBarrier((&___boxRect_6), value);
	}

	inline static int32_t get_offset_of_selectables_7() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___selectables_7)); }
	inline IBoxSelectableU5BU5D_t1290335761* get_selectables_7() const { return ___selectables_7; }
	inline IBoxSelectableU5BU5D_t1290335761** get_address_of_selectables_7() { return &___selectables_7; }
	inline void set_selectables_7(IBoxSelectableU5BU5D_t1290335761* value)
	{
		___selectables_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectables_7), value);
	}

	inline static int32_t get_offset_of_selectableGroup_8() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___selectableGroup_8)); }
	inline MonoBehaviourU5BU5D_t3035069757* get_selectableGroup_8() const { return ___selectableGroup_8; }
	inline MonoBehaviourU5BU5D_t3035069757** get_address_of_selectableGroup_8() { return &___selectableGroup_8; }
	inline void set_selectableGroup_8(MonoBehaviourU5BU5D_t3035069757* value)
	{
		___selectableGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&___selectableGroup_8), value);
	}

	inline static int32_t get_offset_of_clickedBeforeDrag_9() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___clickedBeforeDrag_9)); }
	inline RuntimeObject* get_clickedBeforeDrag_9() const { return ___clickedBeforeDrag_9; }
	inline RuntimeObject** get_address_of_clickedBeforeDrag_9() { return &___clickedBeforeDrag_9; }
	inline void set_clickedBeforeDrag_9(RuntimeObject* value)
	{
		___clickedBeforeDrag_9 = value;
		Il2CppCodeGenWriteBarrier((&___clickedBeforeDrag_9), value);
	}

	inline static int32_t get_offset_of_clickedAfterDrag_10() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___clickedAfterDrag_10)); }
	inline RuntimeObject* get_clickedAfterDrag_10() const { return ___clickedAfterDrag_10; }
	inline RuntimeObject** get_address_of_clickedAfterDrag_10() { return &___clickedAfterDrag_10; }
	inline void set_clickedAfterDrag_10(RuntimeObject* value)
	{
		___clickedAfterDrag_10 = value;
		Il2CppCodeGenWriteBarrier((&___clickedAfterDrag_10), value);
	}

	inline static int32_t get_offset_of_onSelectionChange_11() { return static_cast<int32_t>(offsetof(SelectionBox_t3294027723, ___onSelectionChange_11)); }
	inline SelectionEvent_t1038911497 * get_onSelectionChange_11() const { return ___onSelectionChange_11; }
	inline SelectionEvent_t1038911497 ** get_address_of_onSelectionChange_11() { return &___onSelectionChange_11; }
	inline void set_onSelectionChange_11(SelectionEvent_t1038911497 * value)
	{
		___onSelectionChange_11 = value;
		Il2CppCodeGenWriteBarrier((&___onSelectionChange_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONBOX_T3294027723_H
#ifndef MENU_T1209672415_H
#define MENU_T1209672415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu
struct  Menu_t1209672415  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.Menu::DestroyWhenClosed
	bool ___DestroyWhenClosed_2;
	// System.Boolean UnityEngine.UI.Extensions.Menu::DisableMenusUnderneath
	bool ___DisableMenusUnderneath_3;

public:
	inline static int32_t get_offset_of_DestroyWhenClosed_2() { return static_cast<int32_t>(offsetof(Menu_t1209672415, ___DestroyWhenClosed_2)); }
	inline bool get_DestroyWhenClosed_2() const { return ___DestroyWhenClosed_2; }
	inline bool* get_address_of_DestroyWhenClosed_2() { return &___DestroyWhenClosed_2; }
	inline void set_DestroyWhenClosed_2(bool value)
	{
		___DestroyWhenClosed_2 = value;
	}

	inline static int32_t get_offset_of_DisableMenusUnderneath_3() { return static_cast<int32_t>(offsetof(Menu_t1209672415, ___DisableMenusUnderneath_3)); }
	inline bool get_DisableMenusUnderneath_3() const { return ___DisableMenusUnderneath_3; }
	inline bool* get_address_of_DisableMenusUnderneath_3() { return &___DisableMenusUnderneath_3; }
	inline void set_DisableMenusUnderneath_3(bool value)
	{
		___DisableMenusUnderneath_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_T1209672415_H
#ifndef RESIZEPANEL_T2063095658_H
#define RESIZEPANEL_T2063095658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ResizePanel
struct  ResizePanel_t2063095658  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::minSize
	Vector2_t2243707579  ___minSize_2;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::maxSize
	Vector2_t2243707579  ___maxSize_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ResizePanel::rectTransform
	RectTransform_t3349966182 * ___rectTransform_4;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::currentPointerPosition
	Vector2_t2243707579  ___currentPointerPosition_5;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::previousPointerPosition
	Vector2_t2243707579  ___previousPointerPosition_6;
	// System.Single UnityEngine.UI.Extensions.ResizePanel::ratio
	float ___ratio_7;

public:
	inline static int32_t get_offset_of_minSize_2() { return static_cast<int32_t>(offsetof(ResizePanel_t2063095658, ___minSize_2)); }
	inline Vector2_t2243707579  get_minSize_2() const { return ___minSize_2; }
	inline Vector2_t2243707579 * get_address_of_minSize_2() { return &___minSize_2; }
	inline void set_minSize_2(Vector2_t2243707579  value)
	{
		___minSize_2 = value;
	}

	inline static int32_t get_offset_of_maxSize_3() { return static_cast<int32_t>(offsetof(ResizePanel_t2063095658, ___maxSize_3)); }
	inline Vector2_t2243707579  get_maxSize_3() const { return ___maxSize_3; }
	inline Vector2_t2243707579 * get_address_of_maxSize_3() { return &___maxSize_3; }
	inline void set_maxSize_3(Vector2_t2243707579  value)
	{
		___maxSize_3 = value;
	}

	inline static int32_t get_offset_of_rectTransform_4() { return static_cast<int32_t>(offsetof(ResizePanel_t2063095658, ___rectTransform_4)); }
	inline RectTransform_t3349966182 * get_rectTransform_4() const { return ___rectTransform_4; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_4() { return &___rectTransform_4; }
	inline void set_rectTransform_4(RectTransform_t3349966182 * value)
	{
		___rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_4), value);
	}

	inline static int32_t get_offset_of_currentPointerPosition_5() { return static_cast<int32_t>(offsetof(ResizePanel_t2063095658, ___currentPointerPosition_5)); }
	inline Vector2_t2243707579  get_currentPointerPosition_5() const { return ___currentPointerPosition_5; }
	inline Vector2_t2243707579 * get_address_of_currentPointerPosition_5() { return &___currentPointerPosition_5; }
	inline void set_currentPointerPosition_5(Vector2_t2243707579  value)
	{
		___currentPointerPosition_5 = value;
	}

	inline static int32_t get_offset_of_previousPointerPosition_6() { return static_cast<int32_t>(offsetof(ResizePanel_t2063095658, ___previousPointerPosition_6)); }
	inline Vector2_t2243707579  get_previousPointerPosition_6() const { return ___previousPointerPosition_6; }
	inline Vector2_t2243707579 * get_address_of_previousPointerPosition_6() { return &___previousPointerPosition_6; }
	inline void set_previousPointerPosition_6(Vector2_t2243707579  value)
	{
		___previousPointerPosition_6 = value;
	}

	inline static int32_t get_offset_of_ratio_7() { return static_cast<int32_t>(offsetof(ResizePanel_t2063095658, ___ratio_7)); }
	inline float get_ratio_7() const { return ___ratio_7; }
	inline float* get_address_of_ratio_7() { return &___ratio_7; }
	inline void set_ratio_7(float value)
	{
		___ratio_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIZEPANEL_T2063095658_H
#ifndef REORDERABLELISTELEMENT_T3127693425_H
#define REORDERABLELISTELEMENT_T3127693425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListElement
struct  ReorderableListElement_t3127693425  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::IsGrabbable
	bool ___IsGrabbable_2;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::IsTransferable
	bool ___IsTransferable_3;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::isDroppableInSpace
	bool ___isDroppableInSpace_4;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.UI.Extensions.ReorderableListElement::_raycastResults
	List_1_t3685274804 * ____raycastResults_5;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableListElement::_currentReorderableListRaycasted
	ReorderableList_t970849249 * ____currentReorderableListRaycasted_6;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListElement::_draggingObject
	RectTransform_t3349966182 * ____draggingObject_7;
	// UnityEngine.UI.LayoutElement UnityEngine.UI.Extensions.ReorderableListElement::_draggingObjectLE
	LayoutElement_t2808691390 * ____draggingObjectLE_8;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ReorderableListElement::_draggingObjectOriginalSize
	Vector2_t2243707579  ____draggingObjectOriginalSize_9;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListElement::_fakeElement
	RectTransform_t3349966182 * ____fakeElement_10;
	// UnityEngine.UI.LayoutElement UnityEngine.UI.Extensions.ReorderableListElement::_fakeElementLE
	LayoutElement_t2808691390 * ____fakeElementLE_11;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableListElement::_fromIndex
	int32_t ____fromIndex_12;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::_isDragging
	bool ____isDragging_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListElement::_rect
	RectTransform_t3349966182 * ____rect_14;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableListElement::_reorderableList
	ReorderableList_t970849249 * ____reorderableList_15;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::isValid
	bool ___isValid_16;

public:
	inline static int32_t get_offset_of_IsGrabbable_2() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ___IsGrabbable_2)); }
	inline bool get_IsGrabbable_2() const { return ___IsGrabbable_2; }
	inline bool* get_address_of_IsGrabbable_2() { return &___IsGrabbable_2; }
	inline void set_IsGrabbable_2(bool value)
	{
		___IsGrabbable_2 = value;
	}

	inline static int32_t get_offset_of_IsTransferable_3() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ___IsTransferable_3)); }
	inline bool get_IsTransferable_3() const { return ___IsTransferable_3; }
	inline bool* get_address_of_IsTransferable_3() { return &___IsTransferable_3; }
	inline void set_IsTransferable_3(bool value)
	{
		___IsTransferable_3 = value;
	}

	inline static int32_t get_offset_of_isDroppableInSpace_4() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ___isDroppableInSpace_4)); }
	inline bool get_isDroppableInSpace_4() const { return ___isDroppableInSpace_4; }
	inline bool* get_address_of_isDroppableInSpace_4() { return &___isDroppableInSpace_4; }
	inline void set_isDroppableInSpace_4(bool value)
	{
		___isDroppableInSpace_4 = value;
	}

	inline static int32_t get_offset_of__raycastResults_5() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____raycastResults_5)); }
	inline List_1_t3685274804 * get__raycastResults_5() const { return ____raycastResults_5; }
	inline List_1_t3685274804 ** get_address_of__raycastResults_5() { return &____raycastResults_5; }
	inline void set__raycastResults_5(List_1_t3685274804 * value)
	{
		____raycastResults_5 = value;
		Il2CppCodeGenWriteBarrier((&____raycastResults_5), value);
	}

	inline static int32_t get_offset_of__currentReorderableListRaycasted_6() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____currentReorderableListRaycasted_6)); }
	inline ReorderableList_t970849249 * get__currentReorderableListRaycasted_6() const { return ____currentReorderableListRaycasted_6; }
	inline ReorderableList_t970849249 ** get_address_of__currentReorderableListRaycasted_6() { return &____currentReorderableListRaycasted_6; }
	inline void set__currentReorderableListRaycasted_6(ReorderableList_t970849249 * value)
	{
		____currentReorderableListRaycasted_6 = value;
		Il2CppCodeGenWriteBarrier((&____currentReorderableListRaycasted_6), value);
	}

	inline static int32_t get_offset_of__draggingObject_7() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____draggingObject_7)); }
	inline RectTransform_t3349966182 * get__draggingObject_7() const { return ____draggingObject_7; }
	inline RectTransform_t3349966182 ** get_address_of__draggingObject_7() { return &____draggingObject_7; }
	inline void set__draggingObject_7(RectTransform_t3349966182 * value)
	{
		____draggingObject_7 = value;
		Il2CppCodeGenWriteBarrier((&____draggingObject_7), value);
	}

	inline static int32_t get_offset_of__draggingObjectLE_8() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____draggingObjectLE_8)); }
	inline LayoutElement_t2808691390 * get__draggingObjectLE_8() const { return ____draggingObjectLE_8; }
	inline LayoutElement_t2808691390 ** get_address_of__draggingObjectLE_8() { return &____draggingObjectLE_8; }
	inline void set__draggingObjectLE_8(LayoutElement_t2808691390 * value)
	{
		____draggingObjectLE_8 = value;
		Il2CppCodeGenWriteBarrier((&____draggingObjectLE_8), value);
	}

	inline static int32_t get_offset_of__draggingObjectOriginalSize_9() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____draggingObjectOriginalSize_9)); }
	inline Vector2_t2243707579  get__draggingObjectOriginalSize_9() const { return ____draggingObjectOriginalSize_9; }
	inline Vector2_t2243707579 * get_address_of__draggingObjectOriginalSize_9() { return &____draggingObjectOriginalSize_9; }
	inline void set__draggingObjectOriginalSize_9(Vector2_t2243707579  value)
	{
		____draggingObjectOriginalSize_9 = value;
	}

	inline static int32_t get_offset_of__fakeElement_10() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____fakeElement_10)); }
	inline RectTransform_t3349966182 * get__fakeElement_10() const { return ____fakeElement_10; }
	inline RectTransform_t3349966182 ** get_address_of__fakeElement_10() { return &____fakeElement_10; }
	inline void set__fakeElement_10(RectTransform_t3349966182 * value)
	{
		____fakeElement_10 = value;
		Il2CppCodeGenWriteBarrier((&____fakeElement_10), value);
	}

	inline static int32_t get_offset_of__fakeElementLE_11() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____fakeElementLE_11)); }
	inline LayoutElement_t2808691390 * get__fakeElementLE_11() const { return ____fakeElementLE_11; }
	inline LayoutElement_t2808691390 ** get_address_of__fakeElementLE_11() { return &____fakeElementLE_11; }
	inline void set__fakeElementLE_11(LayoutElement_t2808691390 * value)
	{
		____fakeElementLE_11 = value;
		Il2CppCodeGenWriteBarrier((&____fakeElementLE_11), value);
	}

	inline static int32_t get_offset_of__fromIndex_12() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____fromIndex_12)); }
	inline int32_t get__fromIndex_12() const { return ____fromIndex_12; }
	inline int32_t* get_address_of__fromIndex_12() { return &____fromIndex_12; }
	inline void set__fromIndex_12(int32_t value)
	{
		____fromIndex_12 = value;
	}

	inline static int32_t get_offset_of__isDragging_13() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____isDragging_13)); }
	inline bool get__isDragging_13() const { return ____isDragging_13; }
	inline bool* get_address_of__isDragging_13() { return &____isDragging_13; }
	inline void set__isDragging_13(bool value)
	{
		____isDragging_13 = value;
	}

	inline static int32_t get_offset_of__rect_14() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____rect_14)); }
	inline RectTransform_t3349966182 * get__rect_14() const { return ____rect_14; }
	inline RectTransform_t3349966182 ** get_address_of__rect_14() { return &____rect_14; }
	inline void set__rect_14(RectTransform_t3349966182 * value)
	{
		____rect_14 = value;
		Il2CppCodeGenWriteBarrier((&____rect_14), value);
	}

	inline static int32_t get_offset_of__reorderableList_15() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ____reorderableList_15)); }
	inline ReorderableList_t970849249 * get__reorderableList_15() const { return ____reorderableList_15; }
	inline ReorderableList_t970849249 ** get_address_of__reorderableList_15() { return &____reorderableList_15; }
	inline void set__reorderableList_15(ReorderableList_t970849249 * value)
	{
		____reorderableList_15 = value;
		Il2CppCodeGenWriteBarrier((&____reorderableList_15), value);
	}

	inline static int32_t get_offset_of_isValid_16() { return static_cast<int32_t>(offsetof(ReorderableListElement_t3127693425, ___isValid_16)); }
	inline bool get_isValid_16() const { return ___isValid_16; }
	inline bool* get_address_of_isValid_16() { return &___isValid_16; }
	inline void set_isValid_16(bool value)
	{
		___isValid_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTELEMENT_T3127693425_H
#ifndef EXAMPLESELECTABLE_T2153456088_H
#define EXAMPLESELECTABLE_T2153456088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ExampleSelectable
struct  ExampleSelectable_t2153456088  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.ExampleSelectable::_selected
	bool ____selected_2;
	// System.Boolean UnityEngine.UI.Extensions.ExampleSelectable::_preSelected
	bool ____preSelected_3;
	// UnityEngine.SpriteRenderer UnityEngine.UI.Extensions.ExampleSelectable::spriteRenderer
	SpriteRenderer_t1209076198 * ___spriteRenderer_4;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.ExampleSelectable::image
	Image_t2042527209 * ___image_5;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ExampleSelectable::text
	Text_t356221433 * ___text_6;

public:
	inline static int32_t get_offset_of__selected_2() { return static_cast<int32_t>(offsetof(ExampleSelectable_t2153456088, ____selected_2)); }
	inline bool get__selected_2() const { return ____selected_2; }
	inline bool* get_address_of__selected_2() { return &____selected_2; }
	inline void set__selected_2(bool value)
	{
		____selected_2 = value;
	}

	inline static int32_t get_offset_of__preSelected_3() { return static_cast<int32_t>(offsetof(ExampleSelectable_t2153456088, ____preSelected_3)); }
	inline bool get__preSelected_3() const { return ____preSelected_3; }
	inline bool* get_address_of__preSelected_3() { return &____preSelected_3; }
	inline void set__preSelected_3(bool value)
	{
		____preSelected_3 = value;
	}

	inline static int32_t get_offset_of_spriteRenderer_4() { return static_cast<int32_t>(offsetof(ExampleSelectable_t2153456088, ___spriteRenderer_4)); }
	inline SpriteRenderer_t1209076198 * get_spriteRenderer_4() const { return ___spriteRenderer_4; }
	inline SpriteRenderer_t1209076198 ** get_address_of_spriteRenderer_4() { return &___spriteRenderer_4; }
	inline void set_spriteRenderer_4(SpriteRenderer_t1209076198 * value)
	{
		___spriteRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_4), value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ExampleSelectable_t2153456088, ___image_5)); }
	inline Image_t2042527209 * get_image_5() const { return ___image_5; }
	inline Image_t2042527209 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Image_t2042527209 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}

	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(ExampleSelectable_t2153456088, ___text_6)); }
	inline Text_t356221433 * get_text_6() const { return ___text_6; }
	inline Text_t356221433 ** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(Text_t356221433 * value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier((&___text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESELECTABLE_T2153456088_H
#ifndef REORDERABLELISTDEBUG_T400625494_H
#define REORDERABLELISTDEBUG_T400625494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListDebug
struct  ReorderableListDebug_t400625494  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ReorderableListDebug::DebugLabel
	Text_t356221433 * ___DebugLabel_2;

public:
	inline static int32_t get_offset_of_DebugLabel_2() { return static_cast<int32_t>(offsetof(ReorderableListDebug_t400625494, ___DebugLabel_2)); }
	inline Text_t356221433 * get_DebugLabel_2() const { return ___DebugLabel_2; }
	inline Text_t356221433 ** get_address_of_DebugLabel_2() { return &___DebugLabel_2; }
	inline void set_DebugLabel_2(Text_t356221433 * value)
	{
		___DebugLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___DebugLabel_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTDEBUG_T400625494_H
#ifndef COLORSLIDER_T1323400_H
#define COLORSLIDER_T1323400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorSlider
struct  ColorSlider_t1323400  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorSlider::ColorPicker
	ColorPickerControl_t3621872136 * ___ColorPicker_2;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorSlider::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider UnityEngine.UI.Extensions.ColorPicker.ColorSlider::slider
	Slider_t297367283 * ___slider_4;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.ColorSlider::listen
	bool ___listen_5;

public:
	inline static int32_t get_offset_of_ColorPicker_2() { return static_cast<int32_t>(offsetof(ColorSlider_t1323400, ___ColorPicker_2)); }
	inline ColorPickerControl_t3621872136 * get_ColorPicker_2() const { return ___ColorPicker_2; }
	inline ColorPickerControl_t3621872136 ** get_address_of_ColorPicker_2() { return &___ColorPicker_2; }
	inline void set_ColorPicker_2(ColorPickerControl_t3621872136 * value)
	{
		___ColorPicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPicker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSlider_t1323400, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(ColorSlider_t1323400, ___slider_4)); }
	inline Slider_t297367283 * get_slider_4() const { return ___slider_4; }
	inline Slider_t297367283 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t297367283 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_listen_5() { return static_cast<int32_t>(offsetof(ColorSlider_t1323400, ___listen_5)); }
	inline bool get_listen_5() const { return ___listen_5; }
	inline bool* get_address_of_listen_5() { return &___listen_5; }
	inline void set_listen_5(bool value)
	{
		___listen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T1323400_H
#ifndef RESCALEDRAGPANEL_T3607722583_H
#define RESCALEDRAGPANEL_T3607722583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RescaleDragPanel
struct  RescaleDragPanel_t3607722583  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescaleDragPanel::pointerOffset
	Vector2_t2243707579  ___pointerOffset_2;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescaleDragPanel::canvasRectTransform
	RectTransform_t3349966182 * ___canvasRectTransform_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescaleDragPanel::panelRectTransform
	RectTransform_t3349966182 * ___panelRectTransform_4;
	// UnityEngine.Transform UnityEngine.UI.Extensions.RescaleDragPanel::goTransform
	Transform_t3275118058 * ___goTransform_5;

public:
	inline static int32_t get_offset_of_pointerOffset_2() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t3607722583, ___pointerOffset_2)); }
	inline Vector2_t2243707579  get_pointerOffset_2() const { return ___pointerOffset_2; }
	inline Vector2_t2243707579 * get_address_of_pointerOffset_2() { return &___pointerOffset_2; }
	inline void set_pointerOffset_2(Vector2_t2243707579  value)
	{
		___pointerOffset_2 = value;
	}

	inline static int32_t get_offset_of_canvasRectTransform_3() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t3607722583, ___canvasRectTransform_3)); }
	inline RectTransform_t3349966182 * get_canvasRectTransform_3() const { return ___canvasRectTransform_3; }
	inline RectTransform_t3349966182 ** get_address_of_canvasRectTransform_3() { return &___canvasRectTransform_3; }
	inline void set_canvasRectTransform_3(RectTransform_t3349966182 * value)
	{
		___canvasRectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvasRectTransform_3), value);
	}

	inline static int32_t get_offset_of_panelRectTransform_4() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t3607722583, ___panelRectTransform_4)); }
	inline RectTransform_t3349966182 * get_panelRectTransform_4() const { return ___panelRectTransform_4; }
	inline RectTransform_t3349966182 ** get_address_of_panelRectTransform_4() { return &___panelRectTransform_4; }
	inline void set_panelRectTransform_4(RectTransform_t3349966182 * value)
	{
		___panelRectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___panelRectTransform_4), value);
	}

	inline static int32_t get_offset_of_goTransform_5() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t3607722583, ___goTransform_5)); }
	inline Transform_t3275118058 * get_goTransform_5() const { return ___goTransform_5; }
	inline Transform_t3275118058 ** get_address_of_goTransform_5() { return &___goTransform_5; }
	inline void set_goTransform_5(Transform_t3275118058 * value)
	{
		___goTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___goTransform_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESCALEDRAGPANEL_T3607722583_H
#ifndef COOLDOWNBUTTON_T2132617717_H
#define COOLDOWNBUTTON_T2132617717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CooldownButton
struct  CooldownButton_t2132617717  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownTimeout
	float ___cooldownTimeout_2;
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownSpeed
	float ___cooldownSpeed_3;
	// System.Boolean UnityEngine.UI.Extensions.CooldownButton::cooldownActive
	bool ___cooldownActive_4;
	// System.Boolean UnityEngine.UI.Extensions.CooldownButton::cooldownInEffect
	bool ___cooldownInEffect_5;
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownTimeElapsed
	float ___cooldownTimeElapsed_6;
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownTimeRemaining
	float ___cooldownTimeRemaining_7;
	// System.Int32 UnityEngine.UI.Extensions.CooldownButton::cooldownPercentRemaining
	int32_t ___cooldownPercentRemaining_8;
	// System.Int32 UnityEngine.UI.Extensions.CooldownButton::cooldownPercentComplete
	int32_t ___cooldownPercentComplete_9;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.CooldownButton::buttonSource
	PointerEventData_t1599784723 * ___buttonSource_10;
	// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent UnityEngine.UI.Extensions.CooldownButton::OnCooldownStart
	CooldownButtonEvent_t1873823474 * ___OnCooldownStart_11;
	// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent UnityEngine.UI.Extensions.CooldownButton::OnButtonClickDuringCooldown
	CooldownButtonEvent_t1873823474 * ___OnButtonClickDuringCooldown_12;
	// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent UnityEngine.UI.Extensions.CooldownButton::OnCoolDownFinish
	CooldownButtonEvent_t1873823474 * ___OnCoolDownFinish_13;

public:
	inline static int32_t get_offset_of_cooldownTimeout_2() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownTimeout_2)); }
	inline float get_cooldownTimeout_2() const { return ___cooldownTimeout_2; }
	inline float* get_address_of_cooldownTimeout_2() { return &___cooldownTimeout_2; }
	inline void set_cooldownTimeout_2(float value)
	{
		___cooldownTimeout_2 = value;
	}

	inline static int32_t get_offset_of_cooldownSpeed_3() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownSpeed_3)); }
	inline float get_cooldownSpeed_3() const { return ___cooldownSpeed_3; }
	inline float* get_address_of_cooldownSpeed_3() { return &___cooldownSpeed_3; }
	inline void set_cooldownSpeed_3(float value)
	{
		___cooldownSpeed_3 = value;
	}

	inline static int32_t get_offset_of_cooldownActive_4() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownActive_4)); }
	inline bool get_cooldownActive_4() const { return ___cooldownActive_4; }
	inline bool* get_address_of_cooldownActive_4() { return &___cooldownActive_4; }
	inline void set_cooldownActive_4(bool value)
	{
		___cooldownActive_4 = value;
	}

	inline static int32_t get_offset_of_cooldownInEffect_5() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownInEffect_5)); }
	inline bool get_cooldownInEffect_5() const { return ___cooldownInEffect_5; }
	inline bool* get_address_of_cooldownInEffect_5() { return &___cooldownInEffect_5; }
	inline void set_cooldownInEffect_5(bool value)
	{
		___cooldownInEffect_5 = value;
	}

	inline static int32_t get_offset_of_cooldownTimeElapsed_6() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownTimeElapsed_6)); }
	inline float get_cooldownTimeElapsed_6() const { return ___cooldownTimeElapsed_6; }
	inline float* get_address_of_cooldownTimeElapsed_6() { return &___cooldownTimeElapsed_6; }
	inline void set_cooldownTimeElapsed_6(float value)
	{
		___cooldownTimeElapsed_6 = value;
	}

	inline static int32_t get_offset_of_cooldownTimeRemaining_7() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownTimeRemaining_7)); }
	inline float get_cooldownTimeRemaining_7() const { return ___cooldownTimeRemaining_7; }
	inline float* get_address_of_cooldownTimeRemaining_7() { return &___cooldownTimeRemaining_7; }
	inline void set_cooldownTimeRemaining_7(float value)
	{
		___cooldownTimeRemaining_7 = value;
	}

	inline static int32_t get_offset_of_cooldownPercentRemaining_8() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownPercentRemaining_8)); }
	inline int32_t get_cooldownPercentRemaining_8() const { return ___cooldownPercentRemaining_8; }
	inline int32_t* get_address_of_cooldownPercentRemaining_8() { return &___cooldownPercentRemaining_8; }
	inline void set_cooldownPercentRemaining_8(int32_t value)
	{
		___cooldownPercentRemaining_8 = value;
	}

	inline static int32_t get_offset_of_cooldownPercentComplete_9() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___cooldownPercentComplete_9)); }
	inline int32_t get_cooldownPercentComplete_9() const { return ___cooldownPercentComplete_9; }
	inline int32_t* get_address_of_cooldownPercentComplete_9() { return &___cooldownPercentComplete_9; }
	inline void set_cooldownPercentComplete_9(int32_t value)
	{
		___cooldownPercentComplete_9 = value;
	}

	inline static int32_t get_offset_of_buttonSource_10() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___buttonSource_10)); }
	inline PointerEventData_t1599784723 * get_buttonSource_10() const { return ___buttonSource_10; }
	inline PointerEventData_t1599784723 ** get_address_of_buttonSource_10() { return &___buttonSource_10; }
	inline void set_buttonSource_10(PointerEventData_t1599784723 * value)
	{
		___buttonSource_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonSource_10), value);
	}

	inline static int32_t get_offset_of_OnCooldownStart_11() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___OnCooldownStart_11)); }
	inline CooldownButtonEvent_t1873823474 * get_OnCooldownStart_11() const { return ___OnCooldownStart_11; }
	inline CooldownButtonEvent_t1873823474 ** get_address_of_OnCooldownStart_11() { return &___OnCooldownStart_11; }
	inline void set_OnCooldownStart_11(CooldownButtonEvent_t1873823474 * value)
	{
		___OnCooldownStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnCooldownStart_11), value);
	}

	inline static int32_t get_offset_of_OnButtonClickDuringCooldown_12() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___OnButtonClickDuringCooldown_12)); }
	inline CooldownButtonEvent_t1873823474 * get_OnButtonClickDuringCooldown_12() const { return ___OnButtonClickDuringCooldown_12; }
	inline CooldownButtonEvent_t1873823474 ** get_address_of_OnButtonClickDuringCooldown_12() { return &___OnButtonClickDuringCooldown_12; }
	inline void set_OnButtonClickDuringCooldown_12(CooldownButtonEvent_t1873823474 * value)
	{
		___OnButtonClickDuringCooldown_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonClickDuringCooldown_12), value);
	}

	inline static int32_t get_offset_of_OnCoolDownFinish_13() { return static_cast<int32_t>(offsetof(CooldownButton_t2132617717, ___OnCoolDownFinish_13)); }
	inline CooldownButtonEvent_t1873823474 * get_OnCoolDownFinish_13() const { return ___OnCoolDownFinish_13; }
	inline CooldownButtonEvent_t1873823474 ** get_address_of_OnCoolDownFinish_13() { return &___OnCoolDownFinish_13; }
	inline void set_OnCoolDownFinish_13(CooldownButtonEvent_t1873823474 * value)
	{
		___OnCoolDownFinish_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnCoolDownFinish_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOLDOWNBUTTON_T2132617717_H
#ifndef HEXCOLORFIELD_T1408385506_H
#define HEXCOLORFIELD_T1408385506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HexColorField
struct  HexColorField_t1408385506  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.HexColorField::ColorPicker
	ColorPickerControl_t3621872136 * ___ColorPicker_2;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.HexColorField::displayAlpha
	bool ___displayAlpha_3;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.ColorPicker.HexColorField::hexInputField
	InputField_t1631627530 * ___hexInputField_4;

public:
	inline static int32_t get_offset_of_ColorPicker_2() { return static_cast<int32_t>(offsetof(HexColorField_t1408385506, ___ColorPicker_2)); }
	inline ColorPickerControl_t3621872136 * get_ColorPicker_2() const { return ___ColorPicker_2; }
	inline ColorPickerControl_t3621872136 ** get_address_of_ColorPicker_2() { return &___ColorPicker_2; }
	inline void set_ColorPicker_2(ColorPickerControl_t3621872136 * value)
	{
		___ColorPicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPicker_2), value);
	}

	inline static int32_t get_offset_of_displayAlpha_3() { return static_cast<int32_t>(offsetof(HexColorField_t1408385506, ___displayAlpha_3)); }
	inline bool get_displayAlpha_3() const { return ___displayAlpha_3; }
	inline bool* get_address_of_displayAlpha_3() { return &___displayAlpha_3; }
	inline void set_displayAlpha_3(bool value)
	{
		___displayAlpha_3 = value;
	}

	inline static int32_t get_offset_of_hexInputField_4() { return static_cast<int32_t>(offsetof(HexColorField_t1408385506, ___hexInputField_4)); }
	inline InputField_t1631627530 * get_hexInputField_4() const { return ___hexInputField_4; }
	inline InputField_t1631627530 ** get_address_of_hexInputField_4() { return &___hexInputField_4; }
	inline void set_hexInputField_4(InputField_t1631627530 * value)
	{
		___hexInputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T1408385506_H
#ifndef RADIALSLIDER_T2099696204_H
#define RADIALSLIDER_T2099696204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RadialSlider
struct  RadialSlider_t2099696204  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::isPointerDown
	bool ___isPointerDown_2;
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::isPointerReleased
	bool ___isPointerReleased_3;
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::lerpInProgress
	bool ___lerpInProgress_4;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RadialSlider::m_localPos
	Vector2_t2243707579  ___m_localPos_5;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_targetAngle
	float ___m_targetAngle_6;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_lerpTargetAngle
	float ___m_lerpTargetAngle_7;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_startAngle
	float ___m_startAngle_8;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_currentLerpTime
	float ___m_currentLerpTime_9;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_lerpTime
	float ___m_lerpTime_10;
	// UnityEngine.Camera UnityEngine.UI.Extensions.RadialSlider::m_eventCamera
	Camera_t189460977 * ___m_eventCamera_11;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.RadialSlider::m_image
	Image_t2042527209 * ___m_image_12;
	// UnityEngine.Color UnityEngine.UI.Extensions.RadialSlider::m_startColor
	Color_t2020392075  ___m_startColor_13;
	// UnityEngine.Color UnityEngine.UI.Extensions.RadialSlider::m_endColor
	Color_t2020392075  ___m_endColor_14;
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::m_lerpToTarget
	bool ___m_lerpToTarget_15;
	// UnityEngine.AnimationCurve UnityEngine.UI.Extensions.RadialSlider::m_lerpCurve
	AnimationCurve_t3306541151 * ___m_lerpCurve_16;
	// UnityEngine.UI.Extensions.RadialSlider/RadialSliderValueChangedEvent UnityEngine.UI.Extensions.RadialSlider::_onValueChanged
	RadialSliderValueChangedEvent_t1574085429 * ____onValueChanged_17;
	// UnityEngine.UI.Extensions.RadialSlider/RadialSliderTextValueChangedEvent UnityEngine.UI.Extensions.RadialSlider::_onTextValueChanged
	RadialSliderTextValueChangedEvent_t3907405160 * ____onTextValueChanged_18;

public:
	inline static int32_t get_offset_of_isPointerDown_2() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___isPointerDown_2)); }
	inline bool get_isPointerDown_2() const { return ___isPointerDown_2; }
	inline bool* get_address_of_isPointerDown_2() { return &___isPointerDown_2; }
	inline void set_isPointerDown_2(bool value)
	{
		___isPointerDown_2 = value;
	}

	inline static int32_t get_offset_of_isPointerReleased_3() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___isPointerReleased_3)); }
	inline bool get_isPointerReleased_3() const { return ___isPointerReleased_3; }
	inline bool* get_address_of_isPointerReleased_3() { return &___isPointerReleased_3; }
	inline void set_isPointerReleased_3(bool value)
	{
		___isPointerReleased_3 = value;
	}

	inline static int32_t get_offset_of_lerpInProgress_4() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___lerpInProgress_4)); }
	inline bool get_lerpInProgress_4() const { return ___lerpInProgress_4; }
	inline bool* get_address_of_lerpInProgress_4() { return &___lerpInProgress_4; }
	inline void set_lerpInProgress_4(bool value)
	{
		___lerpInProgress_4 = value;
	}

	inline static int32_t get_offset_of_m_localPos_5() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_localPos_5)); }
	inline Vector2_t2243707579  get_m_localPos_5() const { return ___m_localPos_5; }
	inline Vector2_t2243707579 * get_address_of_m_localPos_5() { return &___m_localPos_5; }
	inline void set_m_localPos_5(Vector2_t2243707579  value)
	{
		___m_localPos_5 = value;
	}

	inline static int32_t get_offset_of_m_targetAngle_6() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_targetAngle_6)); }
	inline float get_m_targetAngle_6() const { return ___m_targetAngle_6; }
	inline float* get_address_of_m_targetAngle_6() { return &___m_targetAngle_6; }
	inline void set_m_targetAngle_6(float value)
	{
		___m_targetAngle_6 = value;
	}

	inline static int32_t get_offset_of_m_lerpTargetAngle_7() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_lerpTargetAngle_7)); }
	inline float get_m_lerpTargetAngle_7() const { return ___m_lerpTargetAngle_7; }
	inline float* get_address_of_m_lerpTargetAngle_7() { return &___m_lerpTargetAngle_7; }
	inline void set_m_lerpTargetAngle_7(float value)
	{
		___m_lerpTargetAngle_7 = value;
	}

	inline static int32_t get_offset_of_m_startAngle_8() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_startAngle_8)); }
	inline float get_m_startAngle_8() const { return ___m_startAngle_8; }
	inline float* get_address_of_m_startAngle_8() { return &___m_startAngle_8; }
	inline void set_m_startAngle_8(float value)
	{
		___m_startAngle_8 = value;
	}

	inline static int32_t get_offset_of_m_currentLerpTime_9() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_currentLerpTime_9)); }
	inline float get_m_currentLerpTime_9() const { return ___m_currentLerpTime_9; }
	inline float* get_address_of_m_currentLerpTime_9() { return &___m_currentLerpTime_9; }
	inline void set_m_currentLerpTime_9(float value)
	{
		___m_currentLerpTime_9 = value;
	}

	inline static int32_t get_offset_of_m_lerpTime_10() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_lerpTime_10)); }
	inline float get_m_lerpTime_10() const { return ___m_lerpTime_10; }
	inline float* get_address_of_m_lerpTime_10() { return &___m_lerpTime_10; }
	inline void set_m_lerpTime_10(float value)
	{
		___m_lerpTime_10 = value;
	}

	inline static int32_t get_offset_of_m_eventCamera_11() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_eventCamera_11)); }
	inline Camera_t189460977 * get_m_eventCamera_11() const { return ___m_eventCamera_11; }
	inline Camera_t189460977 ** get_address_of_m_eventCamera_11() { return &___m_eventCamera_11; }
	inline void set_m_eventCamera_11(Camera_t189460977 * value)
	{
		___m_eventCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_eventCamera_11), value);
	}

	inline static int32_t get_offset_of_m_image_12() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_image_12)); }
	inline Image_t2042527209 * get_m_image_12() const { return ___m_image_12; }
	inline Image_t2042527209 ** get_address_of_m_image_12() { return &___m_image_12; }
	inline void set_m_image_12(Image_t2042527209 * value)
	{
		___m_image_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_image_12), value);
	}

	inline static int32_t get_offset_of_m_startColor_13() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_startColor_13)); }
	inline Color_t2020392075  get_m_startColor_13() const { return ___m_startColor_13; }
	inline Color_t2020392075 * get_address_of_m_startColor_13() { return &___m_startColor_13; }
	inline void set_m_startColor_13(Color_t2020392075  value)
	{
		___m_startColor_13 = value;
	}

	inline static int32_t get_offset_of_m_endColor_14() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_endColor_14)); }
	inline Color_t2020392075  get_m_endColor_14() const { return ___m_endColor_14; }
	inline Color_t2020392075 * get_address_of_m_endColor_14() { return &___m_endColor_14; }
	inline void set_m_endColor_14(Color_t2020392075  value)
	{
		___m_endColor_14 = value;
	}

	inline static int32_t get_offset_of_m_lerpToTarget_15() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_lerpToTarget_15)); }
	inline bool get_m_lerpToTarget_15() const { return ___m_lerpToTarget_15; }
	inline bool* get_address_of_m_lerpToTarget_15() { return &___m_lerpToTarget_15; }
	inline void set_m_lerpToTarget_15(bool value)
	{
		___m_lerpToTarget_15 = value;
	}

	inline static int32_t get_offset_of_m_lerpCurve_16() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ___m_lerpCurve_16)); }
	inline AnimationCurve_t3306541151 * get_m_lerpCurve_16() const { return ___m_lerpCurve_16; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_lerpCurve_16() { return &___m_lerpCurve_16; }
	inline void set_m_lerpCurve_16(AnimationCurve_t3306541151 * value)
	{
		___m_lerpCurve_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_lerpCurve_16), value);
	}

	inline static int32_t get_offset_of__onValueChanged_17() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ____onValueChanged_17)); }
	inline RadialSliderValueChangedEvent_t1574085429 * get__onValueChanged_17() const { return ____onValueChanged_17; }
	inline RadialSliderValueChangedEvent_t1574085429 ** get_address_of__onValueChanged_17() { return &____onValueChanged_17; }
	inline void set__onValueChanged_17(RadialSliderValueChangedEvent_t1574085429 * value)
	{
		____onValueChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChanged_17), value);
	}

	inline static int32_t get_offset_of__onTextValueChanged_18() { return static_cast<int32_t>(offsetof(RadialSlider_t2099696204, ____onTextValueChanged_18)); }
	inline RadialSliderTextValueChangedEvent_t3907405160 * get__onTextValueChanged_18() const { return ____onTextValueChanged_18; }
	inline RadialSliderTextValueChangedEvent_t3907405160 ** get_address_of__onTextValueChanged_18() { return &____onTextValueChanged_18; }
	inline void set__onTextValueChanged_18(RadialSliderTextValueChangedEvent_t3907405160 * value)
	{
		____onTextValueChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&____onTextValueChanged_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALSLIDER_T2099696204_H
#ifndef DROPDOWNLIST_T2509108757_H
#define DROPDOWNLIST_T2509108757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownList
struct  DropDownList_t2509108757  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.DropDownList::disabledTextColor
	Color_t2020392075  ___disabledTextColor_2;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.DropDownList::<SelectedItem>k__BackingField
	DropDownListItem_t1818608950 * ___U3CSelectedItemU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListItem> UnityEngine.UI.Extensions.DropDownList::Items
	List_1_t1187730082 * ___Items_4;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::OverrideHighlighted
	bool ___OverrideHighlighted_5;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::_isPanelActive
	bool ____isPanelActive_6;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::_hasDrawnOnce
	bool ____hasDrawnOnce_7;
	// UnityEngine.UI.Extensions.DropDownListButton UnityEngine.UI.Extensions.DropDownList::_mainButton
	DropDownListButton_t188411761 * ____mainButton_8;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_rectTransform
	RectTransform_t3349966182 * ____rectTransform_9;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_overlayRT
	RectTransform_t3349966182 * ____overlayRT_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_scrollPanelRT
	RectTransform_t3349966182 * ____scrollPanelRT_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_scrollBarRT
	RectTransform_t3349966182 * ____scrollBarRT_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_slidingAreaRT
	RectTransform_t3349966182 * ____slidingAreaRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_itemsPanelRT
	RectTransform_t3349966182 * ____itemsPanelRT_14;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.DropDownList::_canvas
	Canvas_t209405766 * ____canvas_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_canvasRT
	RectTransform_t3349966182 * ____canvasRT_16;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.DropDownList::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_17;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListButton> UnityEngine.UI.Extensions.DropDownList::_panelItems
	List_1_t3852500189 * ____panelItems_18;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.DropDownList::_itemTemplate
	GameObject_t1756533147 * ____itemTemplate_19;
	// System.Single UnityEngine.UI.Extensions.DropDownList::_scrollBarWidth
	float ____scrollBarWidth_20;
	// System.Int32 UnityEngine.UI.Extensions.DropDownList::_selectedIndex
	int32_t ____selectedIndex_21;
	// System.Int32 UnityEngine.UI.Extensions.DropDownList::_itemsToDisplay
	int32_t ____itemsToDisplay_22;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::SelectFirstItemOnStart
	bool ___SelectFirstItemOnStart_23;
	// UnityEngine.UI.Extensions.DropDownList/SelectionChangedEvent UnityEngine.UI.Extensions.DropDownList::OnSelectionChanged
	SelectionChangedEvent_t3626208507 * ___OnSelectionChanged_24;

public:
	inline static int32_t get_offset_of_disabledTextColor_2() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ___disabledTextColor_2)); }
	inline Color_t2020392075  get_disabledTextColor_2() const { return ___disabledTextColor_2; }
	inline Color_t2020392075 * get_address_of_disabledTextColor_2() { return &___disabledTextColor_2; }
	inline void set_disabledTextColor_2(Color_t2020392075  value)
	{
		___disabledTextColor_2 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ___U3CSelectedItemU3Ek__BackingField_3)); }
	inline DropDownListItem_t1818608950 * get_U3CSelectedItemU3Ek__BackingField_3() const { return ___U3CSelectedItemU3Ek__BackingField_3; }
	inline DropDownListItem_t1818608950 ** get_address_of_U3CSelectedItemU3Ek__BackingField_3() { return &___U3CSelectedItemU3Ek__BackingField_3; }
	inline void set_U3CSelectedItemU3Ek__BackingField_3(DropDownListItem_t1818608950 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_Items_4() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ___Items_4)); }
	inline List_1_t1187730082 * get_Items_4() const { return ___Items_4; }
	inline List_1_t1187730082 ** get_address_of_Items_4() { return &___Items_4; }
	inline void set_Items_4(List_1_t1187730082 * value)
	{
		___Items_4 = value;
		Il2CppCodeGenWriteBarrier((&___Items_4), value);
	}

	inline static int32_t get_offset_of_OverrideHighlighted_5() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ___OverrideHighlighted_5)); }
	inline bool get_OverrideHighlighted_5() const { return ___OverrideHighlighted_5; }
	inline bool* get_address_of_OverrideHighlighted_5() { return &___OverrideHighlighted_5; }
	inline void set_OverrideHighlighted_5(bool value)
	{
		___OverrideHighlighted_5 = value;
	}

	inline static int32_t get_offset_of__isPanelActive_6() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____isPanelActive_6)); }
	inline bool get__isPanelActive_6() const { return ____isPanelActive_6; }
	inline bool* get_address_of__isPanelActive_6() { return &____isPanelActive_6; }
	inline void set__isPanelActive_6(bool value)
	{
		____isPanelActive_6 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_7() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____hasDrawnOnce_7)); }
	inline bool get__hasDrawnOnce_7() const { return ____hasDrawnOnce_7; }
	inline bool* get_address_of__hasDrawnOnce_7() { return &____hasDrawnOnce_7; }
	inline void set__hasDrawnOnce_7(bool value)
	{
		____hasDrawnOnce_7 = value;
	}

	inline static int32_t get_offset_of__mainButton_8() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____mainButton_8)); }
	inline DropDownListButton_t188411761 * get__mainButton_8() const { return ____mainButton_8; }
	inline DropDownListButton_t188411761 ** get_address_of__mainButton_8() { return &____mainButton_8; }
	inline void set__mainButton_8(DropDownListButton_t188411761 * value)
	{
		____mainButton_8 = value;
		Il2CppCodeGenWriteBarrier((&____mainButton_8), value);
	}

	inline static int32_t get_offset_of__rectTransform_9() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____rectTransform_9)); }
	inline RectTransform_t3349966182 * get__rectTransform_9() const { return ____rectTransform_9; }
	inline RectTransform_t3349966182 ** get_address_of__rectTransform_9() { return &____rectTransform_9; }
	inline void set__rectTransform_9(RectTransform_t3349966182 * value)
	{
		____rectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_9), value);
	}

	inline static int32_t get_offset_of__overlayRT_10() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____overlayRT_10)); }
	inline RectTransform_t3349966182 * get__overlayRT_10() const { return ____overlayRT_10; }
	inline RectTransform_t3349966182 ** get_address_of__overlayRT_10() { return &____overlayRT_10; }
	inline void set__overlayRT_10(RectTransform_t3349966182 * value)
	{
		____overlayRT_10 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_10), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_11() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____scrollPanelRT_11)); }
	inline RectTransform_t3349966182 * get__scrollPanelRT_11() const { return ____scrollPanelRT_11; }
	inline RectTransform_t3349966182 ** get_address_of__scrollPanelRT_11() { return &____scrollPanelRT_11; }
	inline void set__scrollPanelRT_11(RectTransform_t3349966182 * value)
	{
		____scrollPanelRT_11 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_11), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_12() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____scrollBarRT_12)); }
	inline RectTransform_t3349966182 * get__scrollBarRT_12() const { return ____scrollBarRT_12; }
	inline RectTransform_t3349966182 ** get_address_of__scrollBarRT_12() { return &____scrollBarRT_12; }
	inline void set__scrollBarRT_12(RectTransform_t3349966182 * value)
	{
		____scrollBarRT_12 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_12), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_13() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____slidingAreaRT_13)); }
	inline RectTransform_t3349966182 * get__slidingAreaRT_13() const { return ____slidingAreaRT_13; }
	inline RectTransform_t3349966182 ** get_address_of__slidingAreaRT_13() { return &____slidingAreaRT_13; }
	inline void set__slidingAreaRT_13(RectTransform_t3349966182 * value)
	{
		____slidingAreaRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_13), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_14() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____itemsPanelRT_14)); }
	inline RectTransform_t3349966182 * get__itemsPanelRT_14() const { return ____itemsPanelRT_14; }
	inline RectTransform_t3349966182 ** get_address_of__itemsPanelRT_14() { return &____itemsPanelRT_14; }
	inline void set__itemsPanelRT_14(RectTransform_t3349966182 * value)
	{
		____itemsPanelRT_14 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_14), value);
	}

	inline static int32_t get_offset_of__canvas_15() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____canvas_15)); }
	inline Canvas_t209405766 * get__canvas_15() const { return ____canvas_15; }
	inline Canvas_t209405766 ** get_address_of__canvas_15() { return &____canvas_15; }
	inline void set__canvas_15(Canvas_t209405766 * value)
	{
		____canvas_15 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_15), value);
	}

	inline static int32_t get_offset_of__canvasRT_16() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____canvasRT_16)); }
	inline RectTransform_t3349966182 * get__canvasRT_16() const { return ____canvasRT_16; }
	inline RectTransform_t3349966182 ** get_address_of__canvasRT_16() { return &____canvasRT_16; }
	inline void set__canvasRT_16(RectTransform_t3349966182 * value)
	{
		____canvasRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_16), value);
	}

	inline static int32_t get_offset_of__scrollRect_17() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____scrollRect_17)); }
	inline ScrollRect_t1199013257 * get__scrollRect_17() const { return ____scrollRect_17; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_17() { return &____scrollRect_17; }
	inline void set__scrollRect_17(ScrollRect_t1199013257 * value)
	{
		____scrollRect_17 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_17), value);
	}

	inline static int32_t get_offset_of__panelItems_18() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____panelItems_18)); }
	inline List_1_t3852500189 * get__panelItems_18() const { return ____panelItems_18; }
	inline List_1_t3852500189 ** get_address_of__panelItems_18() { return &____panelItems_18; }
	inline void set__panelItems_18(List_1_t3852500189 * value)
	{
		____panelItems_18 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_18), value);
	}

	inline static int32_t get_offset_of__itemTemplate_19() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____itemTemplate_19)); }
	inline GameObject_t1756533147 * get__itemTemplate_19() const { return ____itemTemplate_19; }
	inline GameObject_t1756533147 ** get_address_of__itemTemplate_19() { return &____itemTemplate_19; }
	inline void set__itemTemplate_19(GameObject_t1756533147 * value)
	{
		____itemTemplate_19 = value;
		Il2CppCodeGenWriteBarrier((&____itemTemplate_19), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_20() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____scrollBarWidth_20)); }
	inline float get__scrollBarWidth_20() const { return ____scrollBarWidth_20; }
	inline float* get_address_of__scrollBarWidth_20() { return &____scrollBarWidth_20; }
	inline void set__scrollBarWidth_20(float value)
	{
		____scrollBarWidth_20 = value;
	}

	inline static int32_t get_offset_of__selectedIndex_21() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____selectedIndex_21)); }
	inline int32_t get__selectedIndex_21() const { return ____selectedIndex_21; }
	inline int32_t* get_address_of__selectedIndex_21() { return &____selectedIndex_21; }
	inline void set__selectedIndex_21(int32_t value)
	{
		____selectedIndex_21 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_22() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ____itemsToDisplay_22)); }
	inline int32_t get__itemsToDisplay_22() const { return ____itemsToDisplay_22; }
	inline int32_t* get_address_of__itemsToDisplay_22() { return &____itemsToDisplay_22; }
	inline void set__itemsToDisplay_22(int32_t value)
	{
		____itemsToDisplay_22 = value;
	}

	inline static int32_t get_offset_of_SelectFirstItemOnStart_23() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ___SelectFirstItemOnStart_23)); }
	inline bool get_SelectFirstItemOnStart_23() const { return ___SelectFirstItemOnStart_23; }
	inline bool* get_address_of_SelectFirstItemOnStart_23() { return &___SelectFirstItemOnStart_23; }
	inline void set_SelectFirstItemOnStart_23(bool value)
	{
		___SelectFirstItemOnStart_23 = value;
	}

	inline static int32_t get_offset_of_OnSelectionChanged_24() { return static_cast<int32_t>(offsetof(DropDownList_t2509108757, ___OnSelectionChanged_24)); }
	inline SelectionChangedEvent_t3626208507 * get_OnSelectionChanged_24() const { return ___OnSelectionChanged_24; }
	inline SelectionChangedEvent_t3626208507 ** get_address_of_OnSelectionChanged_24() { return &___OnSelectionChanged_24; }
	inline void set_OnSelectionChanged_24(SelectionChangedEvent_t3626208507 * value)
	{
		___OnSelectionChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNLIST_T2509108757_H
#ifndef CUIBEZIERCURVE_T3289454031_H
#define CUIBEZIERCURVE_T3289454031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIBezierCurve
struct  CUIBezierCurve_t3289454031  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.CUIBezierCurve::controlPoints
	Vector3U5BU5D_t1172311765* ___controlPoints_3;
	// System.Action UnityEngine.UI.Extensions.CUIBezierCurve::OnRefresh
	Action_t3226471752 * ___OnRefresh_4;

public:
	inline static int32_t get_offset_of_controlPoints_3() { return static_cast<int32_t>(offsetof(CUIBezierCurve_t3289454031, ___controlPoints_3)); }
	inline Vector3U5BU5D_t1172311765* get_controlPoints_3() const { return ___controlPoints_3; }
	inline Vector3U5BU5D_t1172311765** get_address_of_controlPoints_3() { return &___controlPoints_3; }
	inline void set_controlPoints_3(Vector3U5BU5D_t1172311765* value)
	{
		___controlPoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoints_3), value);
	}

	inline static int32_t get_offset_of_OnRefresh_4() { return static_cast<int32_t>(offsetof(CUIBezierCurve_t3289454031, ___OnRefresh_4)); }
	inline Action_t3226471752 * get_OnRefresh_4() const { return ___OnRefresh_4; }
	inline Action_t3226471752 ** get_address_of_OnRefresh_4() { return &___OnRefresh_4; }
	inline void set_OnRefresh_4(Action_t3226471752 * value)
	{
		___OnRefresh_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnRefresh_4), value);
	}
};

struct CUIBezierCurve_t3289454031_StaticFields
{
public:
	// System.Int32 UnityEngine.UI.Extensions.CUIBezierCurve::CubicBezierCurvePtNum
	int32_t ___CubicBezierCurvePtNum_2;

public:
	inline static int32_t get_offset_of_CubicBezierCurvePtNum_2() { return static_cast<int32_t>(offsetof(CUIBezierCurve_t3289454031_StaticFields, ___CubicBezierCurvePtNum_2)); }
	inline int32_t get_CubicBezierCurvePtNum_2() const { return ___CubicBezierCurvePtNum_2; }
	inline int32_t* get_address_of_CubicBezierCurvePtNum_2() { return &___CubicBezierCurvePtNum_2; }
	inline void set_CubicBezierCurvePtNum_2(int32_t value)
	{
		___CubicBezierCurvePtNum_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUIBEZIERCURVE_T3289454031_H
#ifndef INPUTFOCUS_T2433219748_H
#define INPUTFOCUS_T2433219748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.InputFocus
struct  InputFocus_t2433219748  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.InputFocus::_inputField
	InputField_t1631627530 * ____inputField_2;
	// System.Boolean UnityEngine.UI.Extensions.InputFocus::_ignoreNextActivation
	bool ____ignoreNextActivation_3;

public:
	inline static int32_t get_offset_of__inputField_2() { return static_cast<int32_t>(offsetof(InputFocus_t2433219748, ____inputField_2)); }
	inline InputField_t1631627530 * get__inputField_2() const { return ____inputField_2; }
	inline InputField_t1631627530 ** get_address_of__inputField_2() { return &____inputField_2; }
	inline void set__inputField_2(InputField_t1631627530 * value)
	{
		____inputField_2 = value;
		Il2CppCodeGenWriteBarrier((&____inputField_2), value);
	}

	inline static int32_t get_offset_of__ignoreNextActivation_3() { return static_cast<int32_t>(offsetof(InputFocus_t2433219748, ____ignoreNextActivation_3)); }
	inline bool get__ignoreNextActivation_3() const { return ____ignoreNextActivation_3; }
	inline bool* get_address_of__ignoreNextActivation_3() { return &____ignoreNextActivation_3; }
	inline void set__ignoreNextActivation_3(bool value)
	{
		____ignoreNextActivation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFOCUS_T2433219748_H
#ifndef COLORPICKERTESTER_T2575572892_H
#define COLORPICKERTESTER_T2575572892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester
struct  ColorPickerTester_t2575572892  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Renderer UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester::pickerRenderer
	Renderer_t257310565 * ___pickerRenderer_2;
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester::picker
	ColorPickerControl_t3621872136 * ___picker_3;

public:
	inline static int32_t get_offset_of_pickerRenderer_2() { return static_cast<int32_t>(offsetof(ColorPickerTester_t2575572892, ___pickerRenderer_2)); }
	inline Renderer_t257310565 * get_pickerRenderer_2() const { return ___pickerRenderer_2; }
	inline Renderer_t257310565 ** get_address_of_pickerRenderer_2() { return &___pickerRenderer_2; }
	inline void set_pickerRenderer_2(Renderer_t257310565 * value)
	{
		___pickerRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___pickerRenderer_2), value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(ColorPickerTester_t2575572892, ___picker_3)); }
	inline ColorPickerControl_t3621872136 * get_picker_3() const { return ___picker_3; }
	inline ColorPickerControl_t3621872136 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(ColorPickerControl_t3621872136 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier((&___picker_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T2575572892_H
#ifndef COLORPICKERPRESETS_T3539431511_H
#define COLORPICKERPRESETS_T3539431511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets
struct  ColorPickerPresets_t3539431511  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::picker
	ColorPickerControl_t3621872136 * ___picker_2;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::presets
	GameObjectU5BU5D_t3057952154* ___presets_3;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::createPresetImage
	Image_t2042527209 * ___createPresetImage_4;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t3539431511, ___picker_2)); }
	inline ColorPickerControl_t3621872136 * get_picker_2() const { return ___picker_2; }
	inline ColorPickerControl_t3621872136 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPickerControl_t3621872136 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t3539431511, ___presets_3)); }
	inline GameObjectU5BU5D_t3057952154* get_presets_3() const { return ___presets_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(GameObjectU5BU5D_t3057952154* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}

	inline static int32_t get_offset_of_createPresetImage_4() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t3539431511, ___createPresetImage_4)); }
	inline Image_t2042527209 * get_createPresetImage_4() const { return ___createPresetImage_4; }
	inline Image_t2042527209 ** get_address_of_createPresetImage_4() { return &___createPresetImage_4; }
	inline void set_createPresetImage_4(Image_t2042527209 * value)
	{
		___createPresetImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERPRESETS_T3539431511_H
#ifndef MENU_1_T2503545450_H
#define MENU_1_T2503545450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu`1<UnityEngine.UI.Extensions.Examples.OptionsMenu>
struct  Menu_1_t2503545450  : public Menu_t1209672415
{
public:

public:
};

struct Menu_1_t2503545450_StaticFields
{
public:
	// T UnityEngine.UI.Extensions.Menu`1::<Instance>k__BackingField
	OptionsMenu_t2309315281 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Menu_1_t2503545450_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline OptionsMenu_t2309315281 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline OptionsMenu_t2309315281 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(OptionsMenu_t2309315281 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_1_T2503545450_H
#ifndef BASEMESHEFFECT_T1728560551_H
#define BASEMESHEFFECT_T1728560551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t1728560551  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t1728560551, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T1728560551_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef MENU_1_T1176746700_H
#define MENU_1_T1176746700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu`1<UnityEngine.UI.Extensions.Examples.PauseMenu>
struct  Menu_1_t1176746700  : public Menu_t1209672415
{
public:

public:
};

struct Menu_1_t1176746700_StaticFields
{
public:
	// T UnityEngine.UI.Extensions.Menu`1::<Instance>k__BackingField
	PauseMenu_t982516531 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Menu_1_t1176746700_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline PauseMenu_t982516531 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline PauseMenu_t982516531 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(PauseMenu_t982516531 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_1_T1176746700_H
#ifndef SCROLLRECT_T1199013257_H
#define SCROLLRECT_T1199013257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_t1199013257  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t3349966182 * ___m_Content_2;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_3;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_4;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_5;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_6;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_7;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_8;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_9;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t3349966182 * ___m_Viewport_10;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t3248359358 * ___m_HorizontalScrollbar_11;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t3248359358 * ___m_VerticalScrollbar_12;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_13;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_14;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_15;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_16;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t3529018992 * ___m_OnValueChanged_17;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_t2243707579  ___m_PointerStartLocalCursor_18;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_t2243707579  ___m_ContentStartPosition_19;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t3349966182 * ___m_ViewRect_20;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t3033363703  ___m_ContentBounds_21;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t3033363703  ___m_ViewBounds_22;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_t2243707579  ___m_Velocity_23;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_t2243707579  ___m_PrevPosition_25;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t3033363703  ___m_PrevContentBounds_26;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t3033363703  ___m_PrevViewBounds_27;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_28;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_29;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_30;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_31;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_32;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t3349966182 * ___m_Rect_33;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t3349966182 * ___m_HorizontalScrollbarRect_34;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t3349966182 * ___m_VerticalScrollbarRect_35;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_36;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_37;

public:
	inline static int32_t get_offset_of_m_Content_2() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Content_2)); }
	inline RectTransform_t3349966182 * get_m_Content_2() const { return ___m_Content_2; }
	inline RectTransform_t3349966182 ** get_address_of_m_Content_2() { return &___m_Content_2; }
	inline void set_m_Content_2(RectTransform_t3349966182 * value)
	{
		___m_Content_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_2), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_3() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Horizontal_3)); }
	inline bool get_m_Horizontal_3() const { return ___m_Horizontal_3; }
	inline bool* get_address_of_m_Horizontal_3() { return &___m_Horizontal_3; }
	inline void set_m_Horizontal_3(bool value)
	{
		___m_Horizontal_3 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_4() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Vertical_4)); }
	inline bool get_m_Vertical_4() const { return ___m_Vertical_4; }
	inline bool* get_address_of_m_Vertical_4() { return &___m_Vertical_4; }
	inline void set_m_Vertical_4(bool value)
	{
		___m_Vertical_4 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_5() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_MovementType_5)); }
	inline int32_t get_m_MovementType_5() const { return ___m_MovementType_5; }
	inline int32_t* get_address_of_m_MovementType_5() { return &___m_MovementType_5; }
	inline void set_m_MovementType_5(int32_t value)
	{
		___m_MovementType_5 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_6() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Elasticity_6)); }
	inline float get_m_Elasticity_6() const { return ___m_Elasticity_6; }
	inline float* get_address_of_m_Elasticity_6() { return &___m_Elasticity_6; }
	inline void set_m_Elasticity_6(float value)
	{
		___m_Elasticity_6 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_7() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Inertia_7)); }
	inline bool get_m_Inertia_7() const { return ___m_Inertia_7; }
	inline bool* get_address_of_m_Inertia_7() { return &___m_Inertia_7; }
	inline void set_m_Inertia_7(bool value)
	{
		___m_Inertia_7 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_8() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_DecelerationRate_8)); }
	inline float get_m_DecelerationRate_8() const { return ___m_DecelerationRate_8; }
	inline float* get_address_of_m_DecelerationRate_8() { return &___m_DecelerationRate_8; }
	inline void set_m_DecelerationRate_8(float value)
	{
		___m_DecelerationRate_8 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_9() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ScrollSensitivity_9)); }
	inline float get_m_ScrollSensitivity_9() const { return ___m_ScrollSensitivity_9; }
	inline float* get_address_of_m_ScrollSensitivity_9() { return &___m_ScrollSensitivity_9; }
	inline void set_m_ScrollSensitivity_9(float value)
	{
		___m_ScrollSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_10() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Viewport_10)); }
	inline RectTransform_t3349966182 * get_m_Viewport_10() const { return ___m_Viewport_10; }
	inline RectTransform_t3349966182 ** get_address_of_m_Viewport_10() { return &___m_Viewport_10; }
	inline void set_m_Viewport_10(RectTransform_t3349966182 * value)
	{
		___m_Viewport_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_10), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_11() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbar_11)); }
	inline Scrollbar_t3248359358 * get_m_HorizontalScrollbar_11() const { return ___m_HorizontalScrollbar_11; }
	inline Scrollbar_t3248359358 ** get_address_of_m_HorizontalScrollbar_11() { return &___m_HorizontalScrollbar_11; }
	inline void set_m_HorizontalScrollbar_11(Scrollbar_t3248359358 * value)
	{
		___m_HorizontalScrollbar_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_12() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbar_12)); }
	inline Scrollbar_t3248359358 * get_m_VerticalScrollbar_12() const { return ___m_VerticalScrollbar_12; }
	inline Scrollbar_t3248359358 ** get_address_of_m_VerticalScrollbar_12() { return &___m_VerticalScrollbar_12; }
	inline void set_m_VerticalScrollbar_12(Scrollbar_t3248359358 * value)
	{
		___m_VerticalScrollbar_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_13() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarVisibility_13)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_13() const { return ___m_HorizontalScrollbarVisibility_13; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_13() { return &___m_HorizontalScrollbarVisibility_13; }
	inline void set_m_HorizontalScrollbarVisibility_13(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_13 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_14() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarVisibility_14)); }
	inline int32_t get_m_VerticalScrollbarVisibility_14() const { return ___m_VerticalScrollbarVisibility_14; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_14() { return &___m_VerticalScrollbarVisibility_14; }
	inline void set_m_VerticalScrollbarVisibility_14(int32_t value)
	{
		___m_VerticalScrollbarVisibility_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_15() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarSpacing_15)); }
	inline float get_m_HorizontalScrollbarSpacing_15() const { return ___m_HorizontalScrollbarSpacing_15; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_15() { return &___m_HorizontalScrollbarSpacing_15; }
	inline void set_m_HorizontalScrollbarSpacing_15(float value)
	{
		___m_HorizontalScrollbarSpacing_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_16() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarSpacing_16)); }
	inline float get_m_VerticalScrollbarSpacing_16() const { return ___m_VerticalScrollbarSpacing_16; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_16() { return &___m_VerticalScrollbarSpacing_16; }
	inline void set_m_VerticalScrollbarSpacing_16(float value)
	{
		___m_VerticalScrollbarSpacing_16 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_17() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_OnValueChanged_17)); }
	inline ScrollRectEvent_t3529018992 * get_m_OnValueChanged_17() const { return ___m_OnValueChanged_17; }
	inline ScrollRectEvent_t3529018992 ** get_address_of_m_OnValueChanged_17() { return &___m_OnValueChanged_17; }
	inline void set_m_OnValueChanged_17(ScrollRectEvent_t3529018992 * value)
	{
		___m_OnValueChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_17), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_18() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PointerStartLocalCursor_18)); }
	inline Vector2_t2243707579  get_m_PointerStartLocalCursor_18() const { return ___m_PointerStartLocalCursor_18; }
	inline Vector2_t2243707579 * get_address_of_m_PointerStartLocalCursor_18() { return &___m_PointerStartLocalCursor_18; }
	inline void set_m_PointerStartLocalCursor_18(Vector2_t2243707579  value)
	{
		___m_PointerStartLocalCursor_18 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_19() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ContentStartPosition_19)); }
	inline Vector2_t2243707579  get_m_ContentStartPosition_19() const { return ___m_ContentStartPosition_19; }
	inline Vector2_t2243707579 * get_address_of_m_ContentStartPosition_19() { return &___m_ContentStartPosition_19; }
	inline void set_m_ContentStartPosition_19(Vector2_t2243707579  value)
	{
		___m_ContentStartPosition_19 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_20() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ViewRect_20)); }
	inline RectTransform_t3349966182 * get_m_ViewRect_20() const { return ___m_ViewRect_20; }
	inline RectTransform_t3349966182 ** get_address_of_m_ViewRect_20() { return &___m_ViewRect_20; }
	inline void set_m_ViewRect_20(RectTransform_t3349966182 * value)
	{
		___m_ViewRect_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_20), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_21() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ContentBounds_21)); }
	inline Bounds_t3033363703  get_m_ContentBounds_21() const { return ___m_ContentBounds_21; }
	inline Bounds_t3033363703 * get_address_of_m_ContentBounds_21() { return &___m_ContentBounds_21; }
	inline void set_m_ContentBounds_21(Bounds_t3033363703  value)
	{
		___m_ContentBounds_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_22() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ViewBounds_22)); }
	inline Bounds_t3033363703  get_m_ViewBounds_22() const { return ___m_ViewBounds_22; }
	inline Bounds_t3033363703 * get_address_of_m_ViewBounds_22() { return &___m_ViewBounds_22; }
	inline void set_m_ViewBounds_22(Bounds_t3033363703  value)
	{
		___m_ViewBounds_22 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_23() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Velocity_23)); }
	inline Vector2_t2243707579  get_m_Velocity_23() const { return ___m_Velocity_23; }
	inline Vector2_t2243707579 * get_address_of_m_Velocity_23() { return &___m_Velocity_23; }
	inline void set_m_Velocity_23(Vector2_t2243707579  value)
	{
		___m_Velocity_23 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_24() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Dragging_24)); }
	inline bool get_m_Dragging_24() const { return ___m_Dragging_24; }
	inline bool* get_address_of_m_Dragging_24() { return &___m_Dragging_24; }
	inline void set_m_Dragging_24(bool value)
	{
		___m_Dragging_24 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_25() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevPosition_25)); }
	inline Vector2_t2243707579  get_m_PrevPosition_25() const { return ___m_PrevPosition_25; }
	inline Vector2_t2243707579 * get_address_of_m_PrevPosition_25() { return &___m_PrevPosition_25; }
	inline void set_m_PrevPosition_25(Vector2_t2243707579  value)
	{
		___m_PrevPosition_25 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_26() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevContentBounds_26)); }
	inline Bounds_t3033363703  get_m_PrevContentBounds_26() const { return ___m_PrevContentBounds_26; }
	inline Bounds_t3033363703 * get_address_of_m_PrevContentBounds_26() { return &___m_PrevContentBounds_26; }
	inline void set_m_PrevContentBounds_26(Bounds_t3033363703  value)
	{
		___m_PrevContentBounds_26 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_27() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevViewBounds_27)); }
	inline Bounds_t3033363703  get_m_PrevViewBounds_27() const { return ___m_PrevViewBounds_27; }
	inline Bounds_t3033363703 * get_address_of_m_PrevViewBounds_27() { return &___m_PrevViewBounds_27; }
	inline void set_m_PrevViewBounds_27(Bounds_t3033363703  value)
	{
		___m_PrevViewBounds_27 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_28() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HasRebuiltLayout_28)); }
	inline bool get_m_HasRebuiltLayout_28() const { return ___m_HasRebuiltLayout_28; }
	inline bool* get_address_of_m_HasRebuiltLayout_28() { return &___m_HasRebuiltLayout_28; }
	inline void set_m_HasRebuiltLayout_28(bool value)
	{
		___m_HasRebuiltLayout_28 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_29() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HSliderExpand_29)); }
	inline bool get_m_HSliderExpand_29() const { return ___m_HSliderExpand_29; }
	inline bool* get_address_of_m_HSliderExpand_29() { return &___m_HSliderExpand_29; }
	inline void set_m_HSliderExpand_29(bool value)
	{
		___m_HSliderExpand_29 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_30() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VSliderExpand_30)); }
	inline bool get_m_VSliderExpand_30() const { return ___m_VSliderExpand_30; }
	inline bool* get_address_of_m_VSliderExpand_30() { return &___m_VSliderExpand_30; }
	inline void set_m_VSliderExpand_30(bool value)
	{
		___m_VSliderExpand_30 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_31() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HSliderHeight_31)); }
	inline float get_m_HSliderHeight_31() const { return ___m_HSliderHeight_31; }
	inline float* get_address_of_m_HSliderHeight_31() { return &___m_HSliderHeight_31; }
	inline void set_m_HSliderHeight_31(float value)
	{
		___m_HSliderHeight_31 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_32() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VSliderWidth_32)); }
	inline float get_m_VSliderWidth_32() const { return ___m_VSliderWidth_32; }
	inline float* get_address_of_m_VSliderWidth_32() { return &___m_VSliderWidth_32; }
	inline void set_m_VSliderWidth_32(float value)
	{
		___m_VSliderWidth_32 = value;
	}

	inline static int32_t get_offset_of_m_Rect_33() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Rect_33)); }
	inline RectTransform_t3349966182 * get_m_Rect_33() const { return ___m_Rect_33; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_33() { return &___m_Rect_33; }
	inline void set_m_Rect_33(RectTransform_t3349966182 * value)
	{
		___m_Rect_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_33), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_34() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarRect_34)); }
	inline RectTransform_t3349966182 * get_m_HorizontalScrollbarRect_34() const { return ___m_HorizontalScrollbarRect_34; }
	inline RectTransform_t3349966182 ** get_address_of_m_HorizontalScrollbarRect_34() { return &___m_HorizontalScrollbarRect_34; }
	inline void set_m_HorizontalScrollbarRect_34(RectTransform_t3349966182 * value)
	{
		___m_HorizontalScrollbarRect_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_34), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_35() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarRect_35)); }
	inline RectTransform_t3349966182 * get_m_VerticalScrollbarRect_35() const { return ___m_VerticalScrollbarRect_35; }
	inline RectTransform_t3349966182 ** get_address_of_m_VerticalScrollbarRect_35() { return &___m_VerticalScrollbarRect_35; }
	inline void set_m_VerticalScrollbarRect_35(RectTransform_t3349966182 * value)
	{
		___m_VerticalScrollbarRect_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_35), value);
	}

	inline static int32_t get_offset_of_m_Tracker_36() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Tracker_36)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_36() const { return ___m_Tracker_36; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_36() { return &___m_Tracker_36; }
	inline void set_m_Tracker_36(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_36 = value;
	}

	inline static int32_t get_offset_of_m_Corners_37() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Corners_37)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_37() const { return ___m_Corners_37; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_37() { return &___m_Corners_37; }
	inline void set_m_Corners_37(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_T1199013257_H
#ifndef STEPPERSIDE_T2103236692_H
#define STEPPERSIDE_T2103236692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.StepperSide
struct  StepperSide_t2103236692  : public UIBehaviour_t3960014691
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPPERSIDE_T2103236692_H
#ifndef SEGMENTEDCONTROL_T4115597461_H
#define SEGMENTEDCONTROL_T4115597461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SegmentedControl
struct  SegmentedControl_t4115597461  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Extensions.SegmentedControl::m_segments
	SelectableU5BU5D_t3083107861* ___m_segments_2;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.SegmentedControl::m_separator
	Graphic_t2426225576 * ___m_separator_3;
	// System.Single UnityEngine.UI.Extensions.SegmentedControl::m_separatorWidth
	float ___m_separatorWidth_4;
	// System.Boolean UnityEngine.UI.Extensions.SegmentedControl::m_allowSwitchingOff
	bool ___m_allowSwitchingOff_5;
	// System.Int32 UnityEngine.UI.Extensions.SegmentedControl::m_selectedSegmentIndex
	int32_t ___m_selectedSegmentIndex_6;
	// UnityEngine.UI.Extensions.SegmentedControl/SegmentSelectedEvent UnityEngine.UI.Extensions.SegmentedControl::m_onValueChanged
	SegmentSelectedEvent_t4190223295 * ___m_onValueChanged_7;
	// UnityEngine.UI.Selectable UnityEngine.UI.Extensions.SegmentedControl::selectedSegment
	Selectable_t1490392188 * ___selectedSegment_8;
	// UnityEngine.Color UnityEngine.UI.Extensions.SegmentedControl::selectedColor
	Color_t2020392075  ___selectedColor_9;

public:
	inline static int32_t get_offset_of_m_segments_2() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___m_segments_2)); }
	inline SelectableU5BU5D_t3083107861* get_m_segments_2() const { return ___m_segments_2; }
	inline SelectableU5BU5D_t3083107861** get_address_of_m_segments_2() { return &___m_segments_2; }
	inline void set_m_segments_2(SelectableU5BU5D_t3083107861* value)
	{
		___m_segments_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_segments_2), value);
	}

	inline static int32_t get_offset_of_m_separator_3() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___m_separator_3)); }
	inline Graphic_t2426225576 * get_m_separator_3() const { return ___m_separator_3; }
	inline Graphic_t2426225576 ** get_address_of_m_separator_3() { return &___m_separator_3; }
	inline void set_m_separator_3(Graphic_t2426225576 * value)
	{
		___m_separator_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_separator_3), value);
	}

	inline static int32_t get_offset_of_m_separatorWidth_4() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___m_separatorWidth_4)); }
	inline float get_m_separatorWidth_4() const { return ___m_separatorWidth_4; }
	inline float* get_address_of_m_separatorWidth_4() { return &___m_separatorWidth_4; }
	inline void set_m_separatorWidth_4(float value)
	{
		___m_separatorWidth_4 = value;
	}

	inline static int32_t get_offset_of_m_allowSwitchingOff_5() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___m_allowSwitchingOff_5)); }
	inline bool get_m_allowSwitchingOff_5() const { return ___m_allowSwitchingOff_5; }
	inline bool* get_address_of_m_allowSwitchingOff_5() { return &___m_allowSwitchingOff_5; }
	inline void set_m_allowSwitchingOff_5(bool value)
	{
		___m_allowSwitchingOff_5 = value;
	}

	inline static int32_t get_offset_of_m_selectedSegmentIndex_6() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___m_selectedSegmentIndex_6)); }
	inline int32_t get_m_selectedSegmentIndex_6() const { return ___m_selectedSegmentIndex_6; }
	inline int32_t* get_address_of_m_selectedSegmentIndex_6() { return &___m_selectedSegmentIndex_6; }
	inline void set_m_selectedSegmentIndex_6(int32_t value)
	{
		___m_selectedSegmentIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_onValueChanged_7() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___m_onValueChanged_7)); }
	inline SegmentSelectedEvent_t4190223295 * get_m_onValueChanged_7() const { return ___m_onValueChanged_7; }
	inline SegmentSelectedEvent_t4190223295 ** get_address_of_m_onValueChanged_7() { return &___m_onValueChanged_7; }
	inline void set_m_onValueChanged_7(SegmentSelectedEvent_t4190223295 * value)
	{
		___m_onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_onValueChanged_7), value);
	}

	inline static int32_t get_offset_of_selectedSegment_8() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___selectedSegment_8)); }
	inline Selectable_t1490392188 * get_selectedSegment_8() const { return ___selectedSegment_8; }
	inline Selectable_t1490392188 ** get_address_of_selectedSegment_8() { return &___selectedSegment_8; }
	inline void set_selectedSegment_8(Selectable_t1490392188 * value)
	{
		___selectedSegment_8 = value;
		Il2CppCodeGenWriteBarrier((&___selectedSegment_8), value);
	}

	inline static int32_t get_offset_of_selectedColor_9() { return static_cast<int32_t>(offsetof(SegmentedControl_t4115597461, ___selectedColor_9)); }
	inline Color_t2020392075  get_selectedColor_9() const { return ___selectedColor_9; }
	inline Color_t2020392075 * get_address_of_selectedColor_9() { return &___selectedColor_9; }
	inline void set_selectedColor_9(Color_t2020392075  value)
	{
		___selectedColor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTEDCONTROL_T4115597461_H
#ifndef SEGMENT_T3146723167_H
#define SEGMENT_T3146723167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Segment
struct  Segment_t3146723167  : public UIBehaviour_t3960014691
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Segment::index
	int32_t ___index_2;
	// UnityEngine.Color UnityEngine.UI.Extensions.Segment::textColor
	Color_t2020392075  ___textColor_3;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Segment_t3146723167, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_textColor_3() { return static_cast<int32_t>(offsetof(Segment_t3146723167, ___textColor_3)); }
	inline Color_t2020392075  get_textColor_3() const { return ___textColor_3; }
	inline Color_t2020392075 * get_address_of_textColor_3() { return &___textColor_3; }
	inline void set_textColor_3(Color_t2020392075  value)
	{
		___textColor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENT_T3146723167_H
#ifndef STEPPER_T1023215691_H
#define STEPPER_T1023215691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Stepper
struct  Stepper_t1023215691  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Extensions.Stepper::_sides
	SelectableU5BU5D_t3083107861* ____sides_2;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_value
	int32_t ____value_3;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_minimum
	int32_t ____minimum_4;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_maximum
	int32_t ____maximum_5;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_step
	int32_t ____step_6;
	// System.Boolean UnityEngine.UI.Extensions.Stepper::_wrap
	bool ____wrap_7;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.Stepper::_separator
	Graphic_t2426225576 * ____separator_8;
	// System.Single UnityEngine.UI.Extensions.Stepper::_separatorWidth
	float ____separatorWidth_9;
	// UnityEngine.UI.Extensions.Stepper/StepperValueChangedEvent UnityEngine.UI.Extensions.Stepper::_onValueChanged
	StepperValueChangedEvent_t573788457 * ____onValueChanged_10;

public:
	inline static int32_t get_offset_of__sides_2() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____sides_2)); }
	inline SelectableU5BU5D_t3083107861* get__sides_2() const { return ____sides_2; }
	inline SelectableU5BU5D_t3083107861** get_address_of__sides_2() { return &____sides_2; }
	inline void set__sides_2(SelectableU5BU5D_t3083107861* value)
	{
		____sides_2 = value;
		Il2CppCodeGenWriteBarrier((&____sides_2), value);
	}

	inline static int32_t get_offset_of__value_3() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____value_3)); }
	inline int32_t get__value_3() const { return ____value_3; }
	inline int32_t* get_address_of__value_3() { return &____value_3; }
	inline void set__value_3(int32_t value)
	{
		____value_3 = value;
	}

	inline static int32_t get_offset_of__minimum_4() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____minimum_4)); }
	inline int32_t get__minimum_4() const { return ____minimum_4; }
	inline int32_t* get_address_of__minimum_4() { return &____minimum_4; }
	inline void set__minimum_4(int32_t value)
	{
		____minimum_4 = value;
	}

	inline static int32_t get_offset_of__maximum_5() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____maximum_5)); }
	inline int32_t get__maximum_5() const { return ____maximum_5; }
	inline int32_t* get_address_of__maximum_5() { return &____maximum_5; }
	inline void set__maximum_5(int32_t value)
	{
		____maximum_5 = value;
	}

	inline static int32_t get_offset_of__step_6() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____step_6)); }
	inline int32_t get__step_6() const { return ____step_6; }
	inline int32_t* get_address_of__step_6() { return &____step_6; }
	inline void set__step_6(int32_t value)
	{
		____step_6 = value;
	}

	inline static int32_t get_offset_of__wrap_7() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____wrap_7)); }
	inline bool get__wrap_7() const { return ____wrap_7; }
	inline bool* get_address_of__wrap_7() { return &____wrap_7; }
	inline void set__wrap_7(bool value)
	{
		____wrap_7 = value;
	}

	inline static int32_t get_offset_of__separator_8() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____separator_8)); }
	inline Graphic_t2426225576 * get__separator_8() const { return ____separator_8; }
	inline Graphic_t2426225576 ** get_address_of__separator_8() { return &____separator_8; }
	inline void set__separator_8(Graphic_t2426225576 * value)
	{
		____separator_8 = value;
		Il2CppCodeGenWriteBarrier((&____separator_8), value);
	}

	inline static int32_t get_offset_of__separatorWidth_9() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____separatorWidth_9)); }
	inline float get__separatorWidth_9() const { return ____separatorWidth_9; }
	inline float* get_address_of__separatorWidth_9() { return &____separatorWidth_9; }
	inline void set__separatorWidth_9(float value)
	{
		____separatorWidth_9 = value;
	}

	inline static int32_t get_offset_of__onValueChanged_10() { return static_cast<int32_t>(offsetof(Stepper_t1023215691, ____onValueChanged_10)); }
	inline StepperValueChangedEvent_t573788457 * get__onValueChanged_10() const { return ____onValueChanged_10; }
	inline StepperValueChangedEvent_t573788457 ** get_address_of__onValueChanged_10() { return &____onValueChanged_10; }
	inline void set__onValueChanged_10(StepperValueChangedEvent_t573788457 * value)
	{
		____onValueChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPPER_T1023215691_H
#ifndef SELECTABLE_T1490392188_H
#define SELECTABLE_T1490392188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t1490392188  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1571958496  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2652774230  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1353336012  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t3244928895 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t2426225576 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2665681875 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Navigation_3)); }
	inline Navigation_t1571958496  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t1571958496 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t1571958496  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Colors_5)); }
	inline ColorBlock_t2652774230  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2652774230 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2652774230  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_SpriteState_6)); }
	inline SpriteState_t1353336012  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1353336012 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1353336012  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t3244928895 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t3244928895 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t3244928895 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_TargetGraphic_9)); }
	inline Graphic_t2426225576 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t2426225576 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t2426225576 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CanvasGroupCache_15)); }
	inline List_1_t2665681875 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t2665681875 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t2665681875 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t1490392188_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t859513320 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t1490392188_StaticFields, ___s_List_2)); }
	inline List_1_t859513320 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t859513320 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t859513320 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T1490392188_H
#ifndef BOXSLIDER_T3758521666_H
#define BOXSLIDER_T3758521666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider
struct  BoxSlider_t3758521666  : public Selectable_t1490392188
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.BoxSlider::m_HandleRect
	RectTransform_t3349966182 * ___m_HandleRect_16;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_MinValue
	float ___m_MinValue_17;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_MaxValue
	float ___m_MaxValue_18;
	// System.Boolean UnityEngine.UI.Extensions.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_19;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_ValueX
	float ___m_ValueX_20;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_ValueY
	float ___m_ValueY_21;
	// UnityEngine.UI.Extensions.BoxSlider/BoxSliderEvent UnityEngine.UI.Extensions.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t1768348986 * ___m_OnValueChanged_22;
	// UnityEngine.Transform UnityEngine.UI.Extensions.BoxSlider::m_HandleTransform
	Transform_t3275118058 * ___m_HandleTransform_23;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.BoxSlider::m_HandleContainerRect
	RectTransform_t3349966182 * ___m_HandleContainerRect_24;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.BoxSlider::m_Offset
	Vector2_t2243707579  ___m_Offset_25;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Extensions.BoxSlider::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_HandleRect_16)); }
	inline RectTransform_t3349966182 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t3349966182 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t3349966182 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_MinValue_17() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_MinValue_17)); }
	inline float get_m_MinValue_17() const { return ___m_MinValue_17; }
	inline float* get_address_of_m_MinValue_17() { return &___m_MinValue_17; }
	inline void set_m_MinValue_17(float value)
	{
		___m_MinValue_17 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_18() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_MaxValue_18)); }
	inline float get_m_MaxValue_18() const { return ___m_MaxValue_18; }
	inline float* get_address_of_m_MaxValue_18() { return &___m_MaxValue_18; }
	inline void set_m_MaxValue_18(float value)
	{
		___m_MaxValue_18 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_19() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_WholeNumbers_19)); }
	inline bool get_m_WholeNumbers_19() const { return ___m_WholeNumbers_19; }
	inline bool* get_address_of_m_WholeNumbers_19() { return &___m_WholeNumbers_19; }
	inline void set_m_WholeNumbers_19(bool value)
	{
		___m_WholeNumbers_19 = value;
	}

	inline static int32_t get_offset_of_m_ValueX_20() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_ValueX_20)); }
	inline float get_m_ValueX_20() const { return ___m_ValueX_20; }
	inline float* get_address_of_m_ValueX_20() { return &___m_ValueX_20; }
	inline void set_m_ValueX_20(float value)
	{
		___m_ValueX_20 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_21() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_ValueY_21)); }
	inline float get_m_ValueY_21() const { return ___m_ValueY_21; }
	inline float* get_address_of_m_ValueY_21() { return &___m_ValueY_21; }
	inline void set_m_ValueY_21(float value)
	{
		___m_ValueY_21 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_22() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_OnValueChanged_22)); }
	inline BoxSliderEvent_t1768348986 * get_m_OnValueChanged_22() const { return ___m_OnValueChanged_22; }
	inline BoxSliderEvent_t1768348986 ** get_address_of_m_OnValueChanged_22() { return &___m_OnValueChanged_22; }
	inline void set_m_OnValueChanged_22(BoxSliderEvent_t1768348986 * value)
	{
		___m_OnValueChanged_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_22), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_23() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_HandleTransform_23)); }
	inline Transform_t3275118058 * get_m_HandleTransform_23() const { return ___m_HandleTransform_23; }
	inline Transform_t3275118058 ** get_address_of_m_HandleTransform_23() { return &___m_HandleTransform_23; }
	inline void set_m_HandleTransform_23(Transform_t3275118058 * value)
	{
		___m_HandleTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_23), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_24() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_HandleContainerRect_24)); }
	inline RectTransform_t3349966182 * get_m_HandleContainerRect_24() const { return ___m_HandleContainerRect_24; }
	inline RectTransform_t3349966182 ** get_address_of_m_HandleContainerRect_24() { return &___m_HandleContainerRect_24; }
	inline void set_m_HandleContainerRect_24(RectTransform_t3349966182 * value)
	{
		___m_HandleContainerRect_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_24), value);
	}

	inline static int32_t get_offset_of_m_Offset_25() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_Offset_25)); }
	inline Vector2_t2243707579  get_m_Offset_25() const { return ___m_Offset_25; }
	inline Vector2_t2243707579 * get_address_of_m_Offset_25() { return &___m_Offset_25; }
	inline void set_m_Offset_25(Vector2_t2243707579  value)
	{
		___m_Offset_25 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_26() { return static_cast<int32_t>(offsetof(BoxSlider_t3758521666, ___m_Tracker_26)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_26() const { return ___m_Tracker_26; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_26() { return &___m_Tracker_26; }
	inline void set_m_Tracker_26(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_T3758521666_H
#ifndef CUIGRAPHIC_T893279881_H
#define CUIGRAPHIC_T893279881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIGraphic
struct  CUIGraphic_t893279881  : public BaseMeshEffect_t1728560551
{
public:
	// System.Boolean UnityEngine.UI.Extensions.CUIGraphic::isCurved
	bool ___isCurved_5;
	// System.Boolean UnityEngine.UI.Extensions.CUIGraphic::isLockWithRatio
	bool ___isLockWithRatio_6;
	// System.Single UnityEngine.UI.Extensions.CUIGraphic::resolution
	float ___resolution_7;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.CUIGraphic::rectTrans
	RectTransform_t3349966182 * ___rectTrans_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.CUIGraphic::uiGraphic
	Graphic_t2426225576 * ___uiGraphic_9;
	// UnityEngine.UI.Extensions.CUIGraphic UnityEngine.UI.Extensions.CUIGraphic::refCUIGraphic
	CUIGraphic_t893279881 * ___refCUIGraphic_10;
	// UnityEngine.UI.Extensions.CUIBezierCurve[] UnityEngine.UI.Extensions.CUIGraphic::refCurves
	CUIBezierCurveU5BU5D_t2056300054* ___refCurves_11;
	// UnityEngine.UI.Extensions.Vector3_Array2D[] UnityEngine.UI.Extensions.CUIGraphic::refCurvesControlRatioPoints
	Vector3_Array2DU5BU5D_t3555344525* ___refCurvesControlRatioPoints_12;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.UI.Extensions.CUIGraphic::reuse_quads
	List_1_t573379950 * ___reuse_quads_13;

public:
	inline static int32_t get_offset_of_isCurved_5() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___isCurved_5)); }
	inline bool get_isCurved_5() const { return ___isCurved_5; }
	inline bool* get_address_of_isCurved_5() { return &___isCurved_5; }
	inline void set_isCurved_5(bool value)
	{
		___isCurved_5 = value;
	}

	inline static int32_t get_offset_of_isLockWithRatio_6() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___isLockWithRatio_6)); }
	inline bool get_isLockWithRatio_6() const { return ___isLockWithRatio_6; }
	inline bool* get_address_of_isLockWithRatio_6() { return &___isLockWithRatio_6; }
	inline void set_isLockWithRatio_6(bool value)
	{
		___isLockWithRatio_6 = value;
	}

	inline static int32_t get_offset_of_resolution_7() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___resolution_7)); }
	inline float get_resolution_7() const { return ___resolution_7; }
	inline float* get_address_of_resolution_7() { return &___resolution_7; }
	inline void set_resolution_7(float value)
	{
		___resolution_7 = value;
	}

	inline static int32_t get_offset_of_rectTrans_8() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___rectTrans_8)); }
	inline RectTransform_t3349966182 * get_rectTrans_8() const { return ___rectTrans_8; }
	inline RectTransform_t3349966182 ** get_address_of_rectTrans_8() { return &___rectTrans_8; }
	inline void set_rectTrans_8(RectTransform_t3349966182 * value)
	{
		___rectTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectTrans_8), value);
	}

	inline static int32_t get_offset_of_uiGraphic_9() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___uiGraphic_9)); }
	inline Graphic_t2426225576 * get_uiGraphic_9() const { return ___uiGraphic_9; }
	inline Graphic_t2426225576 ** get_address_of_uiGraphic_9() { return &___uiGraphic_9; }
	inline void set_uiGraphic_9(Graphic_t2426225576 * value)
	{
		___uiGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___uiGraphic_9), value);
	}

	inline static int32_t get_offset_of_refCUIGraphic_10() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___refCUIGraphic_10)); }
	inline CUIGraphic_t893279881 * get_refCUIGraphic_10() const { return ___refCUIGraphic_10; }
	inline CUIGraphic_t893279881 ** get_address_of_refCUIGraphic_10() { return &___refCUIGraphic_10; }
	inline void set_refCUIGraphic_10(CUIGraphic_t893279881 * value)
	{
		___refCUIGraphic_10 = value;
		Il2CppCodeGenWriteBarrier((&___refCUIGraphic_10), value);
	}

	inline static int32_t get_offset_of_refCurves_11() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___refCurves_11)); }
	inline CUIBezierCurveU5BU5D_t2056300054* get_refCurves_11() const { return ___refCurves_11; }
	inline CUIBezierCurveU5BU5D_t2056300054** get_address_of_refCurves_11() { return &___refCurves_11; }
	inline void set_refCurves_11(CUIBezierCurveU5BU5D_t2056300054* value)
	{
		___refCurves_11 = value;
		Il2CppCodeGenWriteBarrier((&___refCurves_11), value);
	}

	inline static int32_t get_offset_of_refCurvesControlRatioPoints_12() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___refCurvesControlRatioPoints_12)); }
	inline Vector3_Array2DU5BU5D_t3555344525* get_refCurvesControlRatioPoints_12() const { return ___refCurvesControlRatioPoints_12; }
	inline Vector3_Array2DU5BU5D_t3555344525** get_address_of_refCurvesControlRatioPoints_12() { return &___refCurvesControlRatioPoints_12; }
	inline void set_refCurvesControlRatioPoints_12(Vector3_Array2DU5BU5D_t3555344525* value)
	{
		___refCurvesControlRatioPoints_12 = value;
		Il2CppCodeGenWriteBarrier((&___refCurvesControlRatioPoints_12), value);
	}

	inline static int32_t get_offset_of_reuse_quads_13() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881, ___reuse_quads_13)); }
	inline List_1_t573379950 * get_reuse_quads_13() const { return ___reuse_quads_13; }
	inline List_1_t573379950 ** get_address_of_reuse_quads_13() { return &___reuse_quads_13; }
	inline void set_reuse_quads_13(List_1_t573379950 * value)
	{
		___reuse_quads_13 = value;
		Il2CppCodeGenWriteBarrier((&___reuse_quads_13), value);
	}
};

struct CUIGraphic_t893279881_StaticFields
{
public:
	// System.Int32 UnityEngine.UI.Extensions.CUIGraphic::bottomCurveIdx
	int32_t ___bottomCurveIdx_3;
	// System.Int32 UnityEngine.UI.Extensions.CUIGraphic::topCurveIdx
	int32_t ___topCurveIdx_4;

public:
	inline static int32_t get_offset_of_bottomCurveIdx_3() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881_StaticFields, ___bottomCurveIdx_3)); }
	inline int32_t get_bottomCurveIdx_3() const { return ___bottomCurveIdx_3; }
	inline int32_t* get_address_of_bottomCurveIdx_3() { return &___bottomCurveIdx_3; }
	inline void set_bottomCurveIdx_3(int32_t value)
	{
		___bottomCurveIdx_3 = value;
	}

	inline static int32_t get_offset_of_topCurveIdx_4() { return static_cast<int32_t>(offsetof(CUIGraphic_t893279881_StaticFields, ___topCurveIdx_4)); }
	inline int32_t get_topCurveIdx_4() const { return ___topCurveIdx_4; }
	inline int32_t* get_address_of_topCurveIdx_4() { return &___topCurveIdx_4; }
	inline void set_topCurveIdx_4(int32_t value)
	{
		___topCurveIdx_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUIGRAPHIC_T893279881_H
#ifndef CURVEDTEXT_T3665035540_H
#define CURVEDTEXT_T3665035540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CurvedText
struct  CurvedText_t3665035540  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.AnimationCurve UnityEngine.UI.Extensions.CurvedText::_curveForText
	AnimationCurve_t3306541151 * ____curveForText_3;
	// System.Single UnityEngine.UI.Extensions.CurvedText::_curveMultiplier
	float ____curveMultiplier_4;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.CurvedText::rectTrans
	RectTransform_t3349966182 * ___rectTrans_5;

public:
	inline static int32_t get_offset_of__curveForText_3() { return static_cast<int32_t>(offsetof(CurvedText_t3665035540, ____curveForText_3)); }
	inline AnimationCurve_t3306541151 * get__curveForText_3() const { return ____curveForText_3; }
	inline AnimationCurve_t3306541151 ** get_address_of__curveForText_3() { return &____curveForText_3; }
	inline void set__curveForText_3(AnimationCurve_t3306541151 * value)
	{
		____curveForText_3 = value;
		Il2CppCodeGenWriteBarrier((&____curveForText_3), value);
	}

	inline static int32_t get_offset_of__curveMultiplier_4() { return static_cast<int32_t>(offsetof(CurvedText_t3665035540, ____curveMultiplier_4)); }
	inline float get__curveMultiplier_4() const { return ____curveMultiplier_4; }
	inline float* get_address_of__curveMultiplier_4() { return &____curveMultiplier_4; }
	inline void set__curveMultiplier_4(float value)
	{
		____curveMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_rectTrans_5() { return static_cast<int32_t>(offsetof(CurvedText_t3665035540, ___rectTrans_5)); }
	inline RectTransform_t3349966182 * get_rectTrans_5() const { return ___rectTrans_5; }
	inline RectTransform_t3349966182 ** get_address_of_rectTrans_5() { return &___rectTrans_5; }
	inline void set_rectTrans_5(RectTransform_t3349966182 * value)
	{
		___rectTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___rectTrans_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEDTEXT_T3665035540_H
#ifndef MULTITOUCHSCROLLRECT_T3929901975_H
#define MULTITOUCHSCROLLRECT_T3929901975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.MultiTouchScrollRect
struct  MultiTouchScrollRect_t3929901975  : public ScrollRect_t1199013257
{
public:
	// System.Int32 UnityEngine.UI.Extensions.MultiTouchScrollRect::pid
	int32_t ___pid_38;

public:
	inline static int32_t get_offset_of_pid_38() { return static_cast<int32_t>(offsetof(MultiTouchScrollRect_t3929901975, ___pid_38)); }
	inline int32_t get_pid_38() const { return ___pid_38; }
	inline int32_t* get_address_of_pid_38() { return &___pid_38; }
	inline void set_pid_38(int32_t value)
	{
		___pid_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITOUCHSCROLLRECT_T3929901975_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef TOGGLE_T3976754468_H
#define TOGGLE_T3976754468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle
struct  Toggle_t3976754468  : public Selectable_t1490392188
{
public:
	// UnityEngine.UI.Toggle/ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_16;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_t2426225576 * ___graphic_17;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t1030026315 * ___m_Group_18;
	// UnityEngine.UI.Toggle/ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t1896830814 * ___onValueChanged_19;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_20;

public:
	inline static int32_t get_offset_of_toggleTransition_16() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___toggleTransition_16)); }
	inline int32_t get_toggleTransition_16() const { return ___toggleTransition_16; }
	inline int32_t* get_address_of_toggleTransition_16() { return &___toggleTransition_16; }
	inline void set_toggleTransition_16(int32_t value)
	{
		___toggleTransition_16 = value;
	}

	inline static int32_t get_offset_of_graphic_17() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___graphic_17)); }
	inline Graphic_t2426225576 * get_graphic_17() const { return ___graphic_17; }
	inline Graphic_t2426225576 ** get_address_of_graphic_17() { return &___graphic_17; }
	inline void set_graphic_17(Graphic_t2426225576 * value)
	{
		___graphic_17 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_17), value);
	}

	inline static int32_t get_offset_of_m_Group_18() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___m_Group_18)); }
	inline ToggleGroup_t1030026315 * get_m_Group_18() const { return ___m_Group_18; }
	inline ToggleGroup_t1030026315 ** get_address_of_m_Group_18() { return &___m_Group_18; }
	inline void set_m_Group_18(ToggleGroup_t1030026315 * value)
	{
		___m_Group_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Group_18), value);
	}

	inline static int32_t get_offset_of_onValueChanged_19() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___onValueChanged_19)); }
	inline ToggleEvent_t1896830814 * get_onValueChanged_19() const { return ___onValueChanged_19; }
	inline ToggleEvent_t1896830814 ** get_address_of_onValueChanged_19() { return &___onValueChanged_19; }
	inline void set_onValueChanged_19(ToggleEvent_t1896830814 * value)
	{
		___onValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_IsOn_20() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___m_IsOn_20)); }
	inline bool get_m_IsOn_20() const { return ___m_IsOn_20; }
	inline bool* get_address_of_m_IsOn_20() { return &___m_IsOn_20; }
	inline void set_m_IsOn_20(bool value)
	{
		___m_IsOn_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLE_T3976754468_H
#ifndef SIMPLEMENU_1_T3410598426_H
#define SIMPLEMENU_1_T3410598426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SimpleMenu`1<UnityEngine.UI.Extensions.Examples.PauseMenu>
struct  SimpleMenu_1_t3410598426  : public Menu_1_t1176746700
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMENU_1_T3410598426_H
#ifndef SHADOW_T4269599528_H
#define SHADOW_T4269599528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t4269599528  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2020392075  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2243707579  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectColor_3)); }
	inline Color_t2020392075  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2020392075 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2020392075  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectDistance_4)); }
	inline Vector2_t2243707579  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2243707579 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2243707579  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T4269599528_H
#ifndef LETTERSPACING_T4139949939_H
#define LETTERSPACING_T4139949939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.LetterSpacing
struct  LetterSpacing_t4139949939  : public BaseMeshEffect_t1728560551
{
public:
	// System.Single UnityEngine.UI.Extensions.LetterSpacing::m_spacing
	float ___m_spacing_3;

public:
	inline static int32_t get_offset_of_m_spacing_3() { return static_cast<int32_t>(offsetof(LetterSpacing_t4139949939, ___m_spacing_3)); }
	inline float get_m_spacing_3() const { return ___m_spacing_3; }
	inline float* get_address_of_m_spacing_3() { return &___m_spacing_3; }
	inline void set_m_spacing_3(float value)
	{
		___m_spacing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERSPACING_T4139949939_H
#ifndef SIMPLEMENU_1_T442429880_H
#define SIMPLEMENU_1_T442429880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SimpleMenu`1<UnityEngine.UI.Extensions.Examples.OptionsMenu>
struct  SimpleMenu_1_t442429880  : public Menu_1_t2503545450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMENU_1_T442429880_H
#ifndef GRADIENT_T585968992_H
#define GRADIENT_T585968992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient
struct  Gradient_t585968992  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.UI.Extensions.GradientMode UnityEngine.UI.Extensions.Gradient::_gradientMode
	int32_t ____gradientMode_3;
	// UnityEngine.UI.Extensions.GradientDir UnityEngine.UI.Extensions.Gradient::_gradientDir
	int32_t ____gradientDir_4;
	// System.Boolean UnityEngine.UI.Extensions.Gradient::_overwriteAllColor
	bool ____overwriteAllColor_5;
	// UnityEngine.Color UnityEngine.UI.Extensions.Gradient::_vertex1
	Color_t2020392075  ____vertex1_6;
	// UnityEngine.Color UnityEngine.UI.Extensions.Gradient::_vertex2
	Color_t2020392075  ____vertex2_7;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.Gradient::targetGraphic
	Graphic_t2426225576 * ___targetGraphic_8;

public:
	inline static int32_t get_offset_of__gradientMode_3() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____gradientMode_3)); }
	inline int32_t get__gradientMode_3() const { return ____gradientMode_3; }
	inline int32_t* get_address_of__gradientMode_3() { return &____gradientMode_3; }
	inline void set__gradientMode_3(int32_t value)
	{
		____gradientMode_3 = value;
	}

	inline static int32_t get_offset_of__gradientDir_4() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____gradientDir_4)); }
	inline int32_t get__gradientDir_4() const { return ____gradientDir_4; }
	inline int32_t* get_address_of__gradientDir_4() { return &____gradientDir_4; }
	inline void set__gradientDir_4(int32_t value)
	{
		____gradientDir_4 = value;
	}

	inline static int32_t get_offset_of__overwriteAllColor_5() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____overwriteAllColor_5)); }
	inline bool get__overwriteAllColor_5() const { return ____overwriteAllColor_5; }
	inline bool* get_address_of__overwriteAllColor_5() { return &____overwriteAllColor_5; }
	inline void set__overwriteAllColor_5(bool value)
	{
		____overwriteAllColor_5 = value;
	}

	inline static int32_t get_offset_of__vertex1_6() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____vertex1_6)); }
	inline Color_t2020392075  get__vertex1_6() const { return ____vertex1_6; }
	inline Color_t2020392075 * get_address_of__vertex1_6() { return &____vertex1_6; }
	inline void set__vertex1_6(Color_t2020392075  value)
	{
		____vertex1_6 = value;
	}

	inline static int32_t get_offset_of__vertex2_7() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____vertex2_7)); }
	inline Color_t2020392075  get__vertex2_7() const { return ____vertex2_7; }
	inline Color_t2020392075 * get_address_of__vertex2_7() { return &____vertex2_7; }
	inline void set__vertex2_7(Color_t2020392075  value)
	{
		____vertex2_7 = value;
	}

	inline static int32_t get_offset_of_targetGraphic_8() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ___targetGraphic_8)); }
	inline Graphic_t2426225576 * get_targetGraphic_8() const { return ___targetGraphic_8; }
	inline Graphic_t2426225576 ** get_address_of_targetGraphic_8() { return &___targetGraphic_8; }
	inline void set_targetGraphic_8(Graphic_t2426225576 * value)
	{
		___targetGraphic_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetGraphic_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENT_T585968992_H
#ifndef GRADIENT2_T998070926_H
#define GRADIENT2_T998070926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient2
struct  Gradient2_t998070926  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.UI.Extensions.Gradient2/Type UnityEngine.UI.Extensions.Gradient2::_gradientType
	int32_t ____gradientType_3;
	// UnityEngine.UI.Extensions.Gradient2/Blend UnityEngine.UI.Extensions.Gradient2::_blendMode
	int32_t ____blendMode_4;
	// System.Single UnityEngine.UI.Extensions.Gradient2::_offset
	float ____offset_5;
	// UnityEngine.Gradient UnityEngine.UI.Extensions.Gradient2::_effectGradient
	Gradient_t3600583008 * ____effectGradient_6;

public:
	inline static int32_t get_offset_of__gradientType_3() { return static_cast<int32_t>(offsetof(Gradient2_t998070926, ____gradientType_3)); }
	inline int32_t get__gradientType_3() const { return ____gradientType_3; }
	inline int32_t* get_address_of__gradientType_3() { return &____gradientType_3; }
	inline void set__gradientType_3(int32_t value)
	{
		____gradientType_3 = value;
	}

	inline static int32_t get_offset_of__blendMode_4() { return static_cast<int32_t>(offsetof(Gradient2_t998070926, ____blendMode_4)); }
	inline int32_t get__blendMode_4() const { return ____blendMode_4; }
	inline int32_t* get_address_of__blendMode_4() { return &____blendMode_4; }
	inline void set__blendMode_4(int32_t value)
	{
		____blendMode_4 = value;
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(Gradient2_t998070926, ____offset_5)); }
	inline float get__offset_5() const { return ____offset_5; }
	inline float* get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(float value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__effectGradient_6() { return static_cast<int32_t>(offsetof(Gradient2_t998070926, ____effectGradient_6)); }
	inline Gradient_t3600583008 * get__effectGradient_6() const { return ____effectGradient_6; }
	inline Gradient_t3600583008 ** get_address_of__effectGradient_6() { return &____effectGradient_6; }
	inline void set__effectGradient_6(Gradient_t3600583008 * value)
	{
		____effectGradient_6 = value;
		Il2CppCodeGenWriteBarrier((&____effectGradient_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENT2_T998070926_H
#ifndef CYLINDERTEXT_T2066901577_H
#define CYLINDERTEXT_T2066901577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CylinderText
struct  CylinderText_t2066901577  : public BaseMeshEffect_t1728560551
{
public:
	// System.Single UnityEngine.UI.Extensions.CylinderText::radius
	float ___radius_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.CylinderText::rectTrans
	RectTransform_t3349966182 * ___rectTrans_4;

public:
	inline static int32_t get_offset_of_radius_3() { return static_cast<int32_t>(offsetof(CylinderText_t2066901577, ___radius_3)); }
	inline float get_radius_3() const { return ___radius_3; }
	inline float* get_address_of_radius_3() { return &___radius_3; }
	inline void set_radius_3(float value)
	{
		___radius_3 = value;
	}

	inline static int32_t get_offset_of_rectTrans_4() { return static_cast<int32_t>(offsetof(CylinderText_t2066901577, ___rectTrans_4)); }
	inline RectTransform_t3349966182 * get_rectTrans_4() const { return ___rectTrans_4; }
	inline RectTransform_t3349966182 ** get_address_of_rectTrans_4() { return &___rectTrans_4; }
	inline void set_rectTrans_4(RectTransform_t3349966182 * value)
	{
		___rectTrans_4 = value;
		Il2CppCodeGenWriteBarrier((&___rectTrans_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTEXT_T2066901577_H
#ifndef ACCORDIONELEMENT_T287868148_H
#define ACCORDIONELEMENT_T287868148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AccordionElement
struct  AccordionElement_t287868148  : public Toggle_t3976754468
{
public:
	// System.Single UnityEngine.UI.Extensions.AccordionElement::m_MinHeight
	float ___m_MinHeight_21;
	// UnityEngine.UI.Extensions.Accordion UnityEngine.UI.Extensions.AccordionElement::m_Accordion
	Accordion_t2257195762 * ___m_Accordion_22;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AccordionElement::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_23;
	// UnityEngine.UI.LayoutElement UnityEngine.UI.Extensions.AccordionElement::m_LayoutElement
	LayoutElement_t2808691390 * ___m_LayoutElement_24;
	// UnityEngine.UI.Extensions.Tweens.TweenRunner`1<UnityEngine.UI.Extensions.Tweens.FloatTween> UnityEngine.UI.Extensions.AccordionElement::m_FloatTweenRunner
	TweenRunner_1_t161549504 * ___m_FloatTweenRunner_25;

public:
	inline static int32_t get_offset_of_m_MinHeight_21() { return static_cast<int32_t>(offsetof(AccordionElement_t287868148, ___m_MinHeight_21)); }
	inline float get_m_MinHeight_21() const { return ___m_MinHeight_21; }
	inline float* get_address_of_m_MinHeight_21() { return &___m_MinHeight_21; }
	inline void set_m_MinHeight_21(float value)
	{
		___m_MinHeight_21 = value;
	}

	inline static int32_t get_offset_of_m_Accordion_22() { return static_cast<int32_t>(offsetof(AccordionElement_t287868148, ___m_Accordion_22)); }
	inline Accordion_t2257195762 * get_m_Accordion_22() const { return ___m_Accordion_22; }
	inline Accordion_t2257195762 ** get_address_of_m_Accordion_22() { return &___m_Accordion_22; }
	inline void set_m_Accordion_22(Accordion_t2257195762 * value)
	{
		___m_Accordion_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Accordion_22), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_23() { return static_cast<int32_t>(offsetof(AccordionElement_t287868148, ___m_RectTransform_23)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_23() const { return ___m_RectTransform_23; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_23() { return &___m_RectTransform_23; }
	inline void set_m_RectTransform_23(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_23), value);
	}

	inline static int32_t get_offset_of_m_LayoutElement_24() { return static_cast<int32_t>(offsetof(AccordionElement_t287868148, ___m_LayoutElement_24)); }
	inline LayoutElement_t2808691390 * get_m_LayoutElement_24() const { return ___m_LayoutElement_24; }
	inline LayoutElement_t2808691390 ** get_address_of_m_LayoutElement_24() { return &___m_LayoutElement_24; }
	inline void set_m_LayoutElement_24(LayoutElement_t2808691390 * value)
	{
		___m_LayoutElement_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_24), value);
	}

	inline static int32_t get_offset_of_m_FloatTweenRunner_25() { return static_cast<int32_t>(offsetof(AccordionElement_t287868148, ___m_FloatTweenRunner_25)); }
	inline TweenRunner_1_t161549504 * get_m_FloatTweenRunner_25() const { return ___m_FloatTweenRunner_25; }
	inline TweenRunner_1_t161549504 ** get_address_of_m_FloatTweenRunner_25() { return &___m_FloatTweenRunner_25; }
	inline void set_m_FloatTweenRunner_25(TweenRunner_1_t161549504 * value)
	{
		___m_FloatTweenRunner_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_FloatTweenRunner_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCORDIONELEMENT_T287868148_H
#ifndef OPTIONSMENU_T2309315281_H
#define OPTIONSMENU_T2309315281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.OptionsMenu
struct  OptionsMenu_t2309315281  : public SimpleMenu_1_t442429880
{
public:
	// UnityEngine.UI.Slider UnityEngine.UI.Extensions.Examples.OptionsMenu::Slider
	Slider_t297367283 * ___Slider_5;

public:
	inline static int32_t get_offset_of_Slider_5() { return static_cast<int32_t>(offsetof(OptionsMenu_t2309315281, ___Slider_5)); }
	inline Slider_t297367283 * get_Slider_5() const { return ___Slider_5; }
	inline Slider_t297367283 ** get_address_of_Slider_5() { return &___Slider_5; }
	inline void set_Slider_5(Slider_t297367283 * value)
	{
		___Slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___Slider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONSMENU_T2309315281_H
#ifndef PAUSEMENU_T982516531_H
#define PAUSEMENU_T982516531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.PauseMenu
struct  PauseMenu_t982516531  : public SimpleMenu_1_t3410598426
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_T982516531_H
#ifndef TEXT_T356221433_H
#define TEXT_T356221433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t356221433  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t2614388407 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t647235000 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t647235000 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_FontData_28)); }
	inline FontData_t2614388407 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t2614388407 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t2614388407 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCache_30)); }
	inline TextGenerator_t647235000 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t647235000 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t647235000 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t647235000 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t356221433_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t193706927 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t356221433_StaticFields, ___s_DefaultText_32)); }
	inline Material_t193706927 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t193706927 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t193706927 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T356221433_H
#ifndef CUITEXT_T2456700724_H
#define CUITEXT_T2456700724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIText
struct  CUIText_t2456700724  : public CUIGraphic_t893279881
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUITEXT_T2456700724_H
#ifndef CUIIMAGE_T1001140650_H
#define CUIIMAGE_T1001140650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIImage
struct  CUIImage_t1001140650  : public CUIGraphic_t893279881
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.CUIImage::cornerPosRatio
	Vector2_t2243707579  ___cornerPosRatio_16;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.CUIImage::oriCornerPosRatio
	Vector2_t2243707579  ___oriCornerPosRatio_17;

public:
	inline static int32_t get_offset_of_cornerPosRatio_16() { return static_cast<int32_t>(offsetof(CUIImage_t1001140650, ___cornerPosRatio_16)); }
	inline Vector2_t2243707579  get_cornerPosRatio_16() const { return ___cornerPosRatio_16; }
	inline Vector2_t2243707579 * get_address_of_cornerPosRatio_16() { return &___cornerPosRatio_16; }
	inline void set_cornerPosRatio_16(Vector2_t2243707579  value)
	{
		___cornerPosRatio_16 = value;
	}

	inline static int32_t get_offset_of_oriCornerPosRatio_17() { return static_cast<int32_t>(offsetof(CUIImage_t1001140650, ___oriCornerPosRatio_17)); }
	inline Vector2_t2243707579  get_oriCornerPosRatio_17() const { return ___oriCornerPosRatio_17; }
	inline Vector2_t2243707579 * get_address_of_oriCornerPosRatio_17() { return &___oriCornerPosRatio_17; }
	inline void set_oriCornerPosRatio_17(Vector2_t2243707579  value)
	{
		___oriCornerPosRatio_17 = value;
	}
};

struct CUIImage_t1001140650_StaticFields
{
public:
	// System.Int32 UnityEngine.UI.Extensions.CUIImage::SlicedImageCornerRefVertexIdx
	int32_t ___SlicedImageCornerRefVertexIdx_14;
	// System.Int32 UnityEngine.UI.Extensions.CUIImage::FilledImageCornerRefVertexIdx
	int32_t ___FilledImageCornerRefVertexIdx_15;

public:
	inline static int32_t get_offset_of_SlicedImageCornerRefVertexIdx_14() { return static_cast<int32_t>(offsetof(CUIImage_t1001140650_StaticFields, ___SlicedImageCornerRefVertexIdx_14)); }
	inline int32_t get_SlicedImageCornerRefVertexIdx_14() const { return ___SlicedImageCornerRefVertexIdx_14; }
	inline int32_t* get_address_of_SlicedImageCornerRefVertexIdx_14() { return &___SlicedImageCornerRefVertexIdx_14; }
	inline void set_SlicedImageCornerRefVertexIdx_14(int32_t value)
	{
		___SlicedImageCornerRefVertexIdx_14 = value;
	}

	inline static int32_t get_offset_of_FilledImageCornerRefVertexIdx_15() { return static_cast<int32_t>(offsetof(CUIImage_t1001140650_StaticFields, ___FilledImageCornerRefVertexIdx_15)); }
	inline int32_t get_FilledImageCornerRefVertexIdx_15() const { return ___FilledImageCornerRefVertexIdx_15; }
	inline int32_t* get_address_of_FilledImageCornerRefVertexIdx_15() { return &___FilledImageCornerRefVertexIdx_15; }
	inline void set_FilledImageCornerRefVertexIdx_15(int32_t value)
	{
		___FilledImageCornerRefVertexIdx_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUIIMAGE_T1001140650_H
#ifndef BESTFITOUTLINE_T2386741245_H
#define BESTFITOUTLINE_T2386741245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BestFitOutline
struct  BestFitOutline_t2386741245  : public Shadow_t4269599528
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BESTFITOUTLINE_T2386741245_H
#ifndef TEXTPIC_T1856600851_H
#define TEXTPIC_T1856600851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic
struct  TextPic_t1856600851  : public Text_t356221433
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Extensions.TextPic::m_ImagesPool
	List_1_t1411648341 * ___m_ImagesPool_35;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.UI.Extensions.TextPic::culled_ImagesPool
	List_1_t1125654279 * ___culled_ImagesPool_36;
	// System.Boolean UnityEngine.UI.Extensions.TextPic::clearImages
	bool ___clearImages_37;
	// UnityEngine.Object UnityEngine.UI.Extensions.TextPic::thisLock
	Object_t1021602117 * ___thisLock_38;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.Extensions.TextPic::m_ImagesVertexIndex
	List_1_t1440998580 * ___m_ImagesVertexIndex_39;
	// System.String UnityEngine.UI.Extensions.TextPic::fixedString
	String_t* ___fixedString_41;
	// System.Boolean UnityEngine.UI.Extensions.TextPic::m_ClickParents
	bool ___m_ClickParents_42;
	// System.String UnityEngine.UI.Extensions.TextPic::m_OutputText
	String_t* ___m_OutputText_43;
	// UnityEngine.UI.Extensions.TextPic/IconName[] UnityEngine.UI.Extensions.TextPic::inspectorIconList
	IconNameU5BU5D_t3827230366* ___inspectorIconList_44;
	// System.Single UnityEngine.UI.Extensions.TextPic::ImageScalingFactor
	float ___ImageScalingFactor_45;
	// System.String UnityEngine.UI.Extensions.TextPic::hyperlinkColor
	String_t* ___hyperlinkColor_46;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TextPic::imageOffset
	Vector2_t2243707579  ___imageOffset_47;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.TextPic::button
	Button_t2872111280 * ___button_48;
	// UnityEngine.UI.Selectable UnityEngine.UI.Extensions.TextPic::highlightselectable
	Selectable_t1490392188 * ___highlightselectable_49;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.Extensions.TextPic::positions
	List_1_t1612828711 * ___positions_50;
	// System.String UnityEngine.UI.Extensions.TextPic::previousText
	String_t* ___previousText_51;
	// System.Boolean UnityEngine.UI.Extensions.TextPic::isCreating_m_HrefInfos
	bool ___isCreating_m_HrefInfos_52;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.TextPic/HrefInfo> UnityEngine.UI.Extensions.TextPic::m_HrefInfos
	List_1_t1717420812 * ___m_HrefInfos_53;
	// UnityEngine.UI.Extensions.TextPic/HrefClickEvent UnityEngine.UI.Extensions.TextPic::m_OnHrefClick
	HrefClickEvent_t1044804460 * ___m_OnHrefClick_56;

public:
	inline static int32_t get_offset_of_m_ImagesPool_35() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___m_ImagesPool_35)); }
	inline List_1_t1411648341 * get_m_ImagesPool_35() const { return ___m_ImagesPool_35; }
	inline List_1_t1411648341 ** get_address_of_m_ImagesPool_35() { return &___m_ImagesPool_35; }
	inline void set_m_ImagesPool_35(List_1_t1411648341 * value)
	{
		___m_ImagesPool_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImagesPool_35), value);
	}

	inline static int32_t get_offset_of_culled_ImagesPool_36() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___culled_ImagesPool_36)); }
	inline List_1_t1125654279 * get_culled_ImagesPool_36() const { return ___culled_ImagesPool_36; }
	inline List_1_t1125654279 ** get_address_of_culled_ImagesPool_36() { return &___culled_ImagesPool_36; }
	inline void set_culled_ImagesPool_36(List_1_t1125654279 * value)
	{
		___culled_ImagesPool_36 = value;
		Il2CppCodeGenWriteBarrier((&___culled_ImagesPool_36), value);
	}

	inline static int32_t get_offset_of_clearImages_37() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___clearImages_37)); }
	inline bool get_clearImages_37() const { return ___clearImages_37; }
	inline bool* get_address_of_clearImages_37() { return &___clearImages_37; }
	inline void set_clearImages_37(bool value)
	{
		___clearImages_37 = value;
	}

	inline static int32_t get_offset_of_thisLock_38() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___thisLock_38)); }
	inline Object_t1021602117 * get_thisLock_38() const { return ___thisLock_38; }
	inline Object_t1021602117 ** get_address_of_thisLock_38() { return &___thisLock_38; }
	inline void set_thisLock_38(Object_t1021602117 * value)
	{
		___thisLock_38 = value;
		Il2CppCodeGenWriteBarrier((&___thisLock_38), value);
	}

	inline static int32_t get_offset_of_m_ImagesVertexIndex_39() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___m_ImagesVertexIndex_39)); }
	inline List_1_t1440998580 * get_m_ImagesVertexIndex_39() const { return ___m_ImagesVertexIndex_39; }
	inline List_1_t1440998580 ** get_address_of_m_ImagesVertexIndex_39() { return &___m_ImagesVertexIndex_39; }
	inline void set_m_ImagesVertexIndex_39(List_1_t1440998580 * value)
	{
		___m_ImagesVertexIndex_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImagesVertexIndex_39), value);
	}

	inline static int32_t get_offset_of_fixedString_41() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___fixedString_41)); }
	inline String_t* get_fixedString_41() const { return ___fixedString_41; }
	inline String_t** get_address_of_fixedString_41() { return &___fixedString_41; }
	inline void set_fixedString_41(String_t* value)
	{
		___fixedString_41 = value;
		Il2CppCodeGenWriteBarrier((&___fixedString_41), value);
	}

	inline static int32_t get_offset_of_m_ClickParents_42() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___m_ClickParents_42)); }
	inline bool get_m_ClickParents_42() const { return ___m_ClickParents_42; }
	inline bool* get_address_of_m_ClickParents_42() { return &___m_ClickParents_42; }
	inline void set_m_ClickParents_42(bool value)
	{
		___m_ClickParents_42 = value;
	}

	inline static int32_t get_offset_of_m_OutputText_43() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___m_OutputText_43)); }
	inline String_t* get_m_OutputText_43() const { return ___m_OutputText_43; }
	inline String_t** get_address_of_m_OutputText_43() { return &___m_OutputText_43; }
	inline void set_m_OutputText_43(String_t* value)
	{
		___m_OutputText_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_OutputText_43), value);
	}

	inline static int32_t get_offset_of_inspectorIconList_44() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___inspectorIconList_44)); }
	inline IconNameU5BU5D_t3827230366* get_inspectorIconList_44() const { return ___inspectorIconList_44; }
	inline IconNameU5BU5D_t3827230366** get_address_of_inspectorIconList_44() { return &___inspectorIconList_44; }
	inline void set_inspectorIconList_44(IconNameU5BU5D_t3827230366* value)
	{
		___inspectorIconList_44 = value;
		Il2CppCodeGenWriteBarrier((&___inspectorIconList_44), value);
	}

	inline static int32_t get_offset_of_ImageScalingFactor_45() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___ImageScalingFactor_45)); }
	inline float get_ImageScalingFactor_45() const { return ___ImageScalingFactor_45; }
	inline float* get_address_of_ImageScalingFactor_45() { return &___ImageScalingFactor_45; }
	inline void set_ImageScalingFactor_45(float value)
	{
		___ImageScalingFactor_45 = value;
	}

	inline static int32_t get_offset_of_hyperlinkColor_46() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___hyperlinkColor_46)); }
	inline String_t* get_hyperlinkColor_46() const { return ___hyperlinkColor_46; }
	inline String_t** get_address_of_hyperlinkColor_46() { return &___hyperlinkColor_46; }
	inline void set_hyperlinkColor_46(String_t* value)
	{
		___hyperlinkColor_46 = value;
		Il2CppCodeGenWriteBarrier((&___hyperlinkColor_46), value);
	}

	inline static int32_t get_offset_of_imageOffset_47() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___imageOffset_47)); }
	inline Vector2_t2243707579  get_imageOffset_47() const { return ___imageOffset_47; }
	inline Vector2_t2243707579 * get_address_of_imageOffset_47() { return &___imageOffset_47; }
	inline void set_imageOffset_47(Vector2_t2243707579  value)
	{
		___imageOffset_47 = value;
	}

	inline static int32_t get_offset_of_button_48() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___button_48)); }
	inline Button_t2872111280 * get_button_48() const { return ___button_48; }
	inline Button_t2872111280 ** get_address_of_button_48() { return &___button_48; }
	inline void set_button_48(Button_t2872111280 * value)
	{
		___button_48 = value;
		Il2CppCodeGenWriteBarrier((&___button_48), value);
	}

	inline static int32_t get_offset_of_highlightselectable_49() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___highlightselectable_49)); }
	inline Selectable_t1490392188 * get_highlightselectable_49() const { return ___highlightselectable_49; }
	inline Selectable_t1490392188 ** get_address_of_highlightselectable_49() { return &___highlightselectable_49; }
	inline void set_highlightselectable_49(Selectable_t1490392188 * value)
	{
		___highlightselectable_49 = value;
		Il2CppCodeGenWriteBarrier((&___highlightselectable_49), value);
	}

	inline static int32_t get_offset_of_positions_50() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___positions_50)); }
	inline List_1_t1612828711 * get_positions_50() const { return ___positions_50; }
	inline List_1_t1612828711 ** get_address_of_positions_50() { return &___positions_50; }
	inline void set_positions_50(List_1_t1612828711 * value)
	{
		___positions_50 = value;
		Il2CppCodeGenWriteBarrier((&___positions_50), value);
	}

	inline static int32_t get_offset_of_previousText_51() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___previousText_51)); }
	inline String_t* get_previousText_51() const { return ___previousText_51; }
	inline String_t** get_address_of_previousText_51() { return &___previousText_51; }
	inline void set_previousText_51(String_t* value)
	{
		___previousText_51 = value;
		Il2CppCodeGenWriteBarrier((&___previousText_51), value);
	}

	inline static int32_t get_offset_of_isCreating_m_HrefInfos_52() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___isCreating_m_HrefInfos_52)); }
	inline bool get_isCreating_m_HrefInfos_52() const { return ___isCreating_m_HrefInfos_52; }
	inline bool* get_address_of_isCreating_m_HrefInfos_52() { return &___isCreating_m_HrefInfos_52; }
	inline void set_isCreating_m_HrefInfos_52(bool value)
	{
		___isCreating_m_HrefInfos_52 = value;
	}

	inline static int32_t get_offset_of_m_HrefInfos_53() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___m_HrefInfos_53)); }
	inline List_1_t1717420812 * get_m_HrefInfos_53() const { return ___m_HrefInfos_53; }
	inline List_1_t1717420812 ** get_address_of_m_HrefInfos_53() { return &___m_HrefInfos_53; }
	inline void set_m_HrefInfos_53(List_1_t1717420812 * value)
	{
		___m_HrefInfos_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_HrefInfos_53), value);
	}

	inline static int32_t get_offset_of_m_OnHrefClick_56() { return static_cast<int32_t>(offsetof(TextPic_t1856600851, ___m_OnHrefClick_56)); }
	inline HrefClickEvent_t1044804460 * get_m_OnHrefClick_56() const { return ___m_OnHrefClick_56; }
	inline HrefClickEvent_t1044804460 ** get_address_of_m_OnHrefClick_56() { return &___m_OnHrefClick_56; }
	inline void set_m_OnHrefClick_56(HrefClickEvent_t1044804460 * value)
	{
		___m_OnHrefClick_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHrefClick_56), value);
	}
};

struct TextPic_t1856600851_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngine.UI.Extensions.TextPic::s_Regex
	Regex_t1803876613 * ___s_Regex_40;
	// System.Text.StringBuilder UnityEngine.UI.Extensions.TextPic::s_TextBuilder
	StringBuilder_t1221177846 * ___s_TextBuilder_54;
	// System.Text.RegularExpressions.Regex UnityEngine.UI.Extensions.TextPic::s_HrefRegex
	Regex_t1803876613 * ___s_HrefRegex_55;
	// System.Predicate`1<UnityEngine.UI.Image> UnityEngine.UI.Extensions.TextPic::<>f__am$cache0
	Predicate_1_t485497324 * ___U3CU3Ef__amU24cache0_57;

public:
	inline static int32_t get_offset_of_s_Regex_40() { return static_cast<int32_t>(offsetof(TextPic_t1856600851_StaticFields, ___s_Regex_40)); }
	inline Regex_t1803876613 * get_s_Regex_40() const { return ___s_Regex_40; }
	inline Regex_t1803876613 ** get_address_of_s_Regex_40() { return &___s_Regex_40; }
	inline void set_s_Regex_40(Regex_t1803876613 * value)
	{
		___s_Regex_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_Regex_40), value);
	}

	inline static int32_t get_offset_of_s_TextBuilder_54() { return static_cast<int32_t>(offsetof(TextPic_t1856600851_StaticFields, ___s_TextBuilder_54)); }
	inline StringBuilder_t1221177846 * get_s_TextBuilder_54() const { return ___s_TextBuilder_54; }
	inline StringBuilder_t1221177846 ** get_address_of_s_TextBuilder_54() { return &___s_TextBuilder_54; }
	inline void set_s_TextBuilder_54(StringBuilder_t1221177846 * value)
	{
		___s_TextBuilder_54 = value;
		Il2CppCodeGenWriteBarrier((&___s_TextBuilder_54), value);
	}

	inline static int32_t get_offset_of_s_HrefRegex_55() { return static_cast<int32_t>(offsetof(TextPic_t1856600851_StaticFields, ___s_HrefRegex_55)); }
	inline Regex_t1803876613 * get_s_HrefRegex_55() const { return ___s_HrefRegex_55; }
	inline Regex_t1803876613 ** get_address_of_s_HrefRegex_55() { return &___s_HrefRegex_55; }
	inline void set_s_HrefRegex_55(Regex_t1803876613 * value)
	{
		___s_HrefRegex_55 = value;
		Il2CppCodeGenWriteBarrier((&___s_HrefRegex_55), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_57() { return static_cast<int32_t>(offsetof(TextPic_t1856600851_StaticFields, ___U3CU3Ef__amU24cache0_57)); }
	inline Predicate_1_t485497324 * get_U3CU3Ef__amU24cache0_57() const { return ___U3CU3Ef__amU24cache0_57; }
	inline Predicate_1_t485497324 ** get_address_of_U3CU3Ef__amU24cache0_57() { return &___U3CU3Ef__amU24cache0_57; }
	inline void set_U3CU3Ef__amU24cache0_57(Predicate_1_t485497324 * value)
	{
		___U3CU3Ef__amU24cache0_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_57), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTPIC_T1856600851_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (OptionsMenu_t2309315281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[1] = 
{
	OptionsMenu_t2309315281::get_offset_of_Slider_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (PauseMenu_t982516531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (UpdateRadialValue_t566688927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[2] = 
{
	UpdateRadialValue_t566688927::get_offset_of_input_2(),
	UpdateRadialValue_t566688927::get_offset_of_slider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (AnimateEffects_t1556533669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[19] = 
{
	AnimateEffects_t1556533669::get_offset_of_letterSpacing_2(),
	AnimateEffects_t1556533669::get_offset_of_letterSpacingMax_3(),
	AnimateEffects_t1556533669::get_offset_of_letterSpacingMin_4(),
	AnimateEffects_t1556533669::get_offset_of_letterSpacingModifier_5(),
	AnimateEffects_t1556533669::get_offset_of_curvedText_6(),
	AnimateEffects_t1556533669::get_offset_of_curvedTextMax_7(),
	AnimateEffects_t1556533669::get_offset_of_curvedTextMin_8(),
	AnimateEffects_t1556533669::get_offset_of_curvedTextModifier_9(),
	AnimateEffects_t1556533669::get_offset_of_gradient2_10(),
	AnimateEffects_t1556533669::get_offset_of_gradient2Max_11(),
	AnimateEffects_t1556533669::get_offset_of_gradient2Min_12(),
	AnimateEffects_t1556533669::get_offset_of_gradient2Modifier_13(),
	AnimateEffects_t1556533669::get_offset_of_cylinderText_14(),
	AnimateEffects_t1556533669::get_offset_of_cylinderTextRT_15(),
	AnimateEffects_t1556533669::get_offset_of_cylinderRotation_16(),
	AnimateEffects_t1556533669::get_offset_of_SAUIM_17(),
	AnimateEffects_t1556533669::get_offset_of_SAUIMMax_18(),
	AnimateEffects_t1556533669::get_offset_of_SAUIMMin_19(),
	AnimateEffects_t1556533669::get_offset_of_SAUIMModifier_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (testHref_t3167327857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[1] = 
{
	testHref_t3167327857::get_offset_of_textPic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (LineRendererOrbit_t3939876451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[8] = 
{
	LineRendererOrbit_t3939876451::get_offset_of_lr_2(),
	LineRendererOrbit_t3939876451::get_offset_of_circle_3(),
	LineRendererOrbit_t3939876451::get_offset_of_OrbitGO_4(),
	LineRendererOrbit_t3939876451::get_offset_of_orbitGOrt_5(),
	LineRendererOrbit_t3939876451::get_offset_of_orbitTime_6(),
	LineRendererOrbit_t3939876451::get_offset_of__xAxis_7(),
	LineRendererOrbit_t3939876451::get_offset_of__yAxis_8(),
	LineRendererOrbit_t3939876451::get_offset_of__steps_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (TestAddingPoints_t2423951840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[3] = 
{
	TestAddingPoints_t2423951840::get_offset_of_LineRenderer_2(),
	TestAddingPoints_t2423951840::get_offset_of_XValue_3(),
	TestAddingPoints_t2423951840::get_offset_of_YValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (ScrollingCalendar_t891181101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[22] = 
{
	ScrollingCalendar_t891181101::get_offset_of_monthsScrollingPanel_2(),
	ScrollingCalendar_t891181101::get_offset_of_yearsScrollingPanel_3(),
	ScrollingCalendar_t891181101::get_offset_of_daysScrollingPanel_4(),
	ScrollingCalendar_t891181101::get_offset_of_yearsButtonPrefab_5(),
	ScrollingCalendar_t891181101::get_offset_of_monthsButtonPrefab_6(),
	ScrollingCalendar_t891181101::get_offset_of_daysButtonPrefab_7(),
	ScrollingCalendar_t891181101::get_offset_of_monthsButtons_8(),
	ScrollingCalendar_t891181101::get_offset_of_yearsButtons_9(),
	ScrollingCalendar_t891181101::get_offset_of_daysButtons_10(),
	ScrollingCalendar_t891181101::get_offset_of_monthCenter_11(),
	ScrollingCalendar_t891181101::get_offset_of_yearsCenter_12(),
	ScrollingCalendar_t891181101::get_offset_of_daysCenter_13(),
	ScrollingCalendar_t891181101::get_offset_of_yearsVerticalScroller_14(),
	ScrollingCalendar_t891181101::get_offset_of_monthsVerticalScroller_15(),
	ScrollingCalendar_t891181101::get_offset_of_daysVerticalScroller_16(),
	ScrollingCalendar_t891181101::get_offset_of_inputFieldDays_17(),
	ScrollingCalendar_t891181101::get_offset_of_inputFieldMonths_18(),
	ScrollingCalendar_t891181101::get_offset_of_inputFieldYears_19(),
	ScrollingCalendar_t891181101::get_offset_of_dateText_20(),
	ScrollingCalendar_t891181101::get_offset_of_daysSet_21(),
	ScrollingCalendar_t891181101::get_offset_of_monthsSet_22(),
	ScrollingCalendar_t891181101::get_offset_of_yearsSet_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (Accordion_t2257195762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	Accordion_t2257195762::get_offset_of_m_Transition_2(),
	Accordion_t2257195762::get_offset_of_m_TransitionDuration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (Transition_t4120399363)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2009[3] = 
{
	Transition_t4120399363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (AccordionElement_t287868148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[5] = 
{
	AccordionElement_t287868148::get_offset_of_m_MinHeight_21(),
	AccordionElement_t287868148::get_offset_of_m_Accordion_22(),
	AccordionElement_t287868148::get_offset_of_m_RectTransform_23(),
	AccordionElement_t287868148::get_offset_of_m_LayoutElement_24(),
	AccordionElement_t287868148::get_offset_of_m_FloatTweenRunner_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (FloatTween_t798271509)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[6] = 
{
	FloatTween_t798271509::get_offset_of_m_StartFloat_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t798271509::get_offset_of_m_TargetFloat_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t798271509::get_offset_of_m_Duration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t798271509::get_offset_of_m_IgnoreTimeScale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t798271509::get_offset_of_m_Target_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t798271509::get_offset_of_m_Finish_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (FloatTweenCallback_t420310625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (FloatFinishCallback_t26767115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (BoxSlider_t3758521666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[11] = 
{
	BoxSlider_t3758521666::get_offset_of_m_HandleRect_16(),
	BoxSlider_t3758521666::get_offset_of_m_MinValue_17(),
	BoxSlider_t3758521666::get_offset_of_m_MaxValue_18(),
	BoxSlider_t3758521666::get_offset_of_m_WholeNumbers_19(),
	BoxSlider_t3758521666::get_offset_of_m_ValueX_20(),
	BoxSlider_t3758521666::get_offset_of_m_ValueY_21(),
	BoxSlider_t3758521666::get_offset_of_m_OnValueChanged_22(),
	BoxSlider_t3758521666::get_offset_of_m_HandleTransform_23(),
	BoxSlider_t3758521666::get_offset_of_m_HandleContainerRect_24(),
	BoxSlider_t3758521666::get_offset_of_m_Offset_25(),
	BoxSlider_t3758521666::get_offset_of_m_Tracker_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Direction_t1092976501)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2018[5] = 
{
	Direction_t1092976501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (BoxSliderEvent_t1768348986), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (Axis_t112506491)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2020[3] = 
{
	Axis_t112506491::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (ColorImage_t1119512210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[2] = 
{
	ColorImage_t1119512210::get_offset_of_picker_2(),
	ColorImage_t1119512210::get_offset_of_image_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (ColorLabel_t2392041509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[7] = 
{
	ColorLabel_t2392041509::get_offset_of_picker_2(),
	ColorLabel_t2392041509::get_offset_of_type_3(),
	ColorLabel_t2392041509::get_offset_of_prefix_4(),
	ColorLabel_t2392041509::get_offset_of_minValue_5(),
	ColorLabel_t2392041509::get_offset_of_maxValue_6(),
	ColorLabel_t2392041509::get_offset_of_precision_7(),
	ColorLabel_t2392041509::get_offset_of_label_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (ColorPickerControl_t3621872136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[9] = 
{
	ColorPickerControl_t3621872136::get_offset_of__hue_2(),
	ColorPickerControl_t3621872136::get_offset_of__saturation_3(),
	ColorPickerControl_t3621872136::get_offset_of__brightness_4(),
	ColorPickerControl_t3621872136::get_offset_of__red_5(),
	ColorPickerControl_t3621872136::get_offset_of__green_6(),
	ColorPickerControl_t3621872136::get_offset_of__blue_7(),
	ColorPickerControl_t3621872136::get_offset_of__alpha_8(),
	ColorPickerControl_t3621872136::get_offset_of_onValueChanged_9(),
	ColorPickerControl_t3621872136::get_offset_of_onHSVChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (ColorPickerPresets_t3539431511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[3] = 
{
	ColorPickerPresets_t3539431511::get_offset_of_picker_2(),
	ColorPickerPresets_t3539431511::get_offset_of_presets_3(),
	ColorPickerPresets_t3539431511::get_offset_of_createPresetImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (ColorPickerTester_t2575572892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[2] = 
{
	ColorPickerTester_t2575572892::get_offset_of_pickerRenderer_2(),
	ColorPickerTester_t2575572892::get_offset_of_picker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (ColorSlider_t1323400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[4] = 
{
	ColorSlider_t1323400::get_offset_of_ColorPicker_2(),
	ColorSlider_t1323400::get_offset_of_type_3(),
	ColorSlider_t1323400::get_offset_of_slider_4(),
	ColorSlider_t1323400::get_offset_of_listen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (ColorSliderImage_t4154176545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[4] = 
{
	ColorSliderImage_t4154176545::get_offset_of_picker_2(),
	ColorSliderImage_t4154176545::get_offset_of_type_3(),
	ColorSliderImage_t4154176545::get_offset_of_direction_4(),
	ColorSliderImage_t4154176545::get_offset_of_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ColorValues_t3669995107)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[8] = 
{
	ColorValues_t3669995107::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (ColorChangedEvent_t2990895397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (HSVChangedEvent_t1170297569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (HexColorField_t1408385506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[4] = 
{
	HexColorField_t1408385506::get_offset_of_ColorPicker_2(),
	HexColorField_t1408385506::get_offset_of_displayAlpha_3(),
	HexColorField_t1408385506::get_offset_of_hexInputField_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (HSVUtil_t960645847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (HsvColor_t223253130)+ sizeof (RuntimeObject), sizeof(HsvColor_t223253130 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[3] = 
{
	HsvColor_t223253130::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t223253130::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t223253130::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (SVBoxSlider_t1950342511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[5] = 
{
	SVBoxSlider_t1950342511::get_offset_of_picker_2(),
	SVBoxSlider_t1950342511::get_offset_of_slider_3(),
	SVBoxSlider_t1950342511::get_offset_of_image_4(),
	SVBoxSlider_t1950342511::get_offset_of_lastH_5(),
	SVBoxSlider_t1950342511::get_offset_of_listen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (TiltWindow_t2642812283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[4] = 
{
	TiltWindow_t2642812283::get_offset_of_range_2(),
	TiltWindow_t2642812283::get_offset_of_mTrans_3(),
	TiltWindow_t2642812283::get_offset_of_mStart_4(),
	TiltWindow_t2642812283::get_offset_of_mRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (AutoCompleteSearchType_t2324807822)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2036[3] = 
{
	AutoCompleteSearchType_t2324807822::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (AutoCompleteComboBox_t2713109875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[33] = 
{
	AutoCompleteComboBox_t2713109875::get_offset_of_disabledTextColor_2(),
	AutoCompleteComboBox_t2713109875::get_offset_of_U3CSelectedItemU3Ek__BackingField_3(),
	AutoCompleteComboBox_t2713109875::get_offset_of_AvailableOptions_4(),
	AutoCompleteComboBox_t2713109875::get_offset_of__isPanelActive_5(),
	AutoCompleteComboBox_t2713109875::get_offset_of__hasDrawnOnce_6(),
	AutoCompleteComboBox_t2713109875::get_offset_of__mainInput_7(),
	AutoCompleteComboBox_t2713109875::get_offset_of__inputRT_8(),
	AutoCompleteComboBox_t2713109875::get_offset_of__rectTransform_9(),
	AutoCompleteComboBox_t2713109875::get_offset_of__overlayRT_10(),
	AutoCompleteComboBox_t2713109875::get_offset_of__scrollPanelRT_11(),
	AutoCompleteComboBox_t2713109875::get_offset_of__scrollBarRT_12(),
	AutoCompleteComboBox_t2713109875::get_offset_of__slidingAreaRT_13(),
	AutoCompleteComboBox_t2713109875::get_offset_of__itemsPanelRT_14(),
	AutoCompleteComboBox_t2713109875::get_offset_of__canvas_15(),
	AutoCompleteComboBox_t2713109875::get_offset_of__canvasRT_16(),
	AutoCompleteComboBox_t2713109875::get_offset_of__scrollRect_17(),
	AutoCompleteComboBox_t2713109875::get_offset_of__panelItems_18(),
	AutoCompleteComboBox_t2713109875::get_offset_of__prunedPanelItems_19(),
	AutoCompleteComboBox_t2713109875::get_offset_of_panelObjects_20(),
	AutoCompleteComboBox_t2713109875::get_offset_of_itemTemplate_21(),
	AutoCompleteComboBox_t2713109875::get_offset_of_U3CTextU3Ek__BackingField_22(),
	AutoCompleteComboBox_t2713109875::get_offset_of__scrollBarWidth_23(),
	AutoCompleteComboBox_t2713109875::get_offset_of__itemsToDisplay_24(),
	AutoCompleteComboBox_t2713109875::get_offset_of_SelectFirstItemOnStart_25(),
	AutoCompleteComboBox_t2713109875::get_offset_of__ChangeInputTextColorBasedOnMatchingItems_26(),
	AutoCompleteComboBox_t2713109875::get_offset_of_ValidSelectionTextColor_27(),
	AutoCompleteComboBox_t2713109875::get_offset_of_MatchingItemsRemainingTextColor_28(),
	AutoCompleteComboBox_t2713109875::get_offset_of_NoItemsRemainingTextColor_29(),
	AutoCompleteComboBox_t2713109875::get_offset_of_autocompleteSearchType_30(),
	AutoCompleteComboBox_t2713109875::get_offset_of__selectionIsValid_31(),
	AutoCompleteComboBox_t2713109875::get_offset_of_OnSelectionTextChanged_32(),
	AutoCompleteComboBox_t2713109875::get_offset_of_OnSelectionValidityChanged_33(),
	AutoCompleteComboBox_t2713109875::get_offset_of_OnSelectionChanged_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (SelectionChangedEvent_t4042853089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (SelectionTextChangedEvent_t934555890), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (SelectionValidityChangedEvent_t352594559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (U3CRebuildPanelU3Ec__AnonStorey0_t3251602021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[2] = 
{
	U3CRebuildPanelU3Ec__AnonStorey0_t3251602021::get_offset_of_textOfItem_0(),
	U3CRebuildPanelU3Ec__AnonStorey0_t3251602021::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (U3CPruneItemsLinqU3Ec__AnonStorey1_t4263748227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	U3CPruneItemsLinqU3Ec__AnonStorey1_t4263748227::get_offset_of_currText_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (ComboBox_t414540299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[23] = 
{
	ComboBox_t414540299::get_offset_of_disabledTextColor_2(),
	ComboBox_t414540299::get_offset_of_U3CSelectedItemU3Ek__BackingField_3(),
	ComboBox_t414540299::get_offset_of_AvailableOptions_4(),
	ComboBox_t414540299::get_offset_of__scrollBarWidth_5(),
	ComboBox_t414540299::get_offset_of__itemsToDisplay_6(),
	ComboBox_t414540299::get_offset_of_OnSelectionChanged_7(),
	ComboBox_t414540299::get_offset_of__isPanelActive_8(),
	ComboBox_t414540299::get_offset_of__hasDrawnOnce_9(),
	ComboBox_t414540299::get_offset_of__mainInput_10(),
	ComboBox_t414540299::get_offset_of__inputRT_11(),
	ComboBox_t414540299::get_offset_of__rectTransform_12(),
	ComboBox_t414540299::get_offset_of__overlayRT_13(),
	ComboBox_t414540299::get_offset_of__scrollPanelRT_14(),
	ComboBox_t414540299::get_offset_of__scrollBarRT_15(),
	ComboBox_t414540299::get_offset_of__slidingAreaRT_16(),
	ComboBox_t414540299::get_offset_of__itemsPanelRT_17(),
	ComboBox_t414540299::get_offset_of__canvas_18(),
	ComboBox_t414540299::get_offset_of__canvasRT_19(),
	ComboBox_t414540299::get_offset_of__scrollRect_20(),
	ComboBox_t414540299::get_offset_of__panelItems_21(),
	ComboBox_t414540299::get_offset_of_panelObjects_22(),
	ComboBox_t414540299::get_offset_of_itemTemplate_23(),
	ComboBox_t414540299::get_offset_of_U3CTextU3Ek__BackingField_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (SelectionChangedEvent_t3776471183), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (U3CRebuildPanelU3Ec__AnonStorey0_t2999840211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[2] = 
{
	U3CRebuildPanelU3Ec__AnonStorey0_t2999840211::get_offset_of_textOfItem_0(),
	U3CRebuildPanelU3Ec__AnonStorey0_t2999840211::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (DropDownList_t2509108757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[23] = 
{
	DropDownList_t2509108757::get_offset_of_disabledTextColor_2(),
	DropDownList_t2509108757::get_offset_of_U3CSelectedItemU3Ek__BackingField_3(),
	DropDownList_t2509108757::get_offset_of_Items_4(),
	DropDownList_t2509108757::get_offset_of_OverrideHighlighted_5(),
	DropDownList_t2509108757::get_offset_of__isPanelActive_6(),
	DropDownList_t2509108757::get_offset_of__hasDrawnOnce_7(),
	DropDownList_t2509108757::get_offset_of__mainButton_8(),
	DropDownList_t2509108757::get_offset_of__rectTransform_9(),
	DropDownList_t2509108757::get_offset_of__overlayRT_10(),
	DropDownList_t2509108757::get_offset_of__scrollPanelRT_11(),
	DropDownList_t2509108757::get_offset_of__scrollBarRT_12(),
	DropDownList_t2509108757::get_offset_of__slidingAreaRT_13(),
	DropDownList_t2509108757::get_offset_of__itemsPanelRT_14(),
	DropDownList_t2509108757::get_offset_of__canvas_15(),
	DropDownList_t2509108757::get_offset_of__canvasRT_16(),
	DropDownList_t2509108757::get_offset_of__scrollRect_17(),
	DropDownList_t2509108757::get_offset_of__panelItems_18(),
	DropDownList_t2509108757::get_offset_of__itemTemplate_19(),
	DropDownList_t2509108757::get_offset_of__scrollBarWidth_20(),
	DropDownList_t2509108757::get_offset_of__selectedIndex_21(),
	DropDownList_t2509108757::get_offset_of__itemsToDisplay_22(),
	DropDownList_t2509108757::get_offset_of_SelectFirstItemOnStart_23(),
	DropDownList_t2509108757::get_offset_of_OnSelectionChanged_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (SelectionChangedEvent_t3626208507), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (U3CRebuildPanelU3Ec__AnonStorey0_t1624052423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[3] = 
{
	U3CRebuildPanelU3Ec__AnonStorey0_t1624052423::get_offset_of_ii_0(),
	U3CRebuildPanelU3Ec__AnonStorey0_t1624052423::get_offset_of_item_1(),
	U3CRebuildPanelU3Ec__AnonStorey0_t1624052423::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (DropDownListButton_t188411761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[6] = 
{
	DropDownListButton_t188411761::get_offset_of_rectTransform_0(),
	DropDownListButton_t188411761::get_offset_of_btn_1(),
	DropDownListButton_t188411761::get_offset_of_txt_2(),
	DropDownListButton_t188411761::get_offset_of_btnImg_3(),
	DropDownListButton_t188411761::get_offset_of_img_4(),
	DropDownListButton_t188411761::get_offset_of_gameobject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (DropDownListItem_t1818608950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[6] = 
{
	DropDownListItem_t1818608950::get_offset_of__caption_0(),
	DropDownListItem_t1818608950::get_offset_of__image_1(),
	DropDownListItem_t1818608950::get_offset_of__isDisabled_2(),
	DropDownListItem_t1818608950::get_offset_of__id_3(),
	DropDownListItem_t1818608950::get_offset_of_OnSelect_4(),
	DropDownListItem_t1818608950::get_offset_of_OnUpdate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (CooldownButton_t2132617717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[12] = 
{
	CooldownButton_t2132617717::get_offset_of_cooldownTimeout_2(),
	CooldownButton_t2132617717::get_offset_of_cooldownSpeed_3(),
	CooldownButton_t2132617717::get_offset_of_cooldownActive_4(),
	CooldownButton_t2132617717::get_offset_of_cooldownInEffect_5(),
	CooldownButton_t2132617717::get_offset_of_cooldownTimeElapsed_6(),
	CooldownButton_t2132617717::get_offset_of_cooldownTimeRemaining_7(),
	CooldownButton_t2132617717::get_offset_of_cooldownPercentRemaining_8(),
	CooldownButton_t2132617717::get_offset_of_cooldownPercentComplete_9(),
	CooldownButton_t2132617717::get_offset_of_buttonSource_10(),
	CooldownButton_t2132617717::get_offset_of_OnCooldownStart_11(),
	CooldownButton_t2132617717::get_offset_of_OnButtonClickDuringCooldown_12(),
	CooldownButton_t2132617717::get_offset_of_OnCoolDownFinish_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (CooldownButtonEvent_t1873823474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (InputFocus_t2433219748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[2] = 
{
	InputFocus_t2433219748::get_offset_of__inputField_2(),
	InputFocus_t2433219748::get_offset_of__ignoreNextActivation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (MultiTouchScrollRect_t3929901975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[1] = 
{
	MultiTouchScrollRect_t3929901975::get_offset_of_pid_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (RadialSlider_t2099696204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[17] = 
{
	RadialSlider_t2099696204::get_offset_of_isPointerDown_2(),
	RadialSlider_t2099696204::get_offset_of_isPointerReleased_3(),
	RadialSlider_t2099696204::get_offset_of_lerpInProgress_4(),
	RadialSlider_t2099696204::get_offset_of_m_localPos_5(),
	RadialSlider_t2099696204::get_offset_of_m_targetAngle_6(),
	RadialSlider_t2099696204::get_offset_of_m_lerpTargetAngle_7(),
	RadialSlider_t2099696204::get_offset_of_m_startAngle_8(),
	RadialSlider_t2099696204::get_offset_of_m_currentLerpTime_9(),
	RadialSlider_t2099696204::get_offset_of_m_lerpTime_10(),
	RadialSlider_t2099696204::get_offset_of_m_eventCamera_11(),
	RadialSlider_t2099696204::get_offset_of_m_image_12(),
	RadialSlider_t2099696204::get_offset_of_m_startColor_13(),
	RadialSlider_t2099696204::get_offset_of_m_endColor_14(),
	RadialSlider_t2099696204::get_offset_of_m_lerpToTarget_15(),
	RadialSlider_t2099696204::get_offset_of_m_lerpCurve_16(),
	RadialSlider_t2099696204::get_offset_of__onValueChanged_17(),
	RadialSlider_t2099696204::get_offset_of__onTextValueChanged_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (RadialSliderValueChangedEvent_t1574085429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (RadialSliderTextValueChangedEvent_t3907405160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (ReorderableList_t970849249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[11] = 
{
	ReorderableList_t970849249::get_offset_of_ContentLayout_2(),
	ReorderableList_t970849249::get_offset_of_DraggableArea_3(),
	ReorderableList_t970849249::get_offset_of_IsDraggable_4(),
	ReorderableList_t970849249::get_offset_of_CloneDraggedObject_5(),
	ReorderableList_t970849249::get_offset_of_IsDropable_6(),
	ReorderableList_t970849249::get_offset_of_OnElementDropped_7(),
	ReorderableList_t970849249::get_offset_of_OnElementGrabbed_8(),
	ReorderableList_t970849249::get_offset_of_OnElementRemoved_9(),
	ReorderableList_t970849249::get_offset_of_OnElementAdded_10(),
	ReorderableList_t970849249::get_offset_of__content_11(),
	ReorderableList_t970849249::get_offset_of__listContent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (ReorderableListEventStruct_t1615631671)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[7] = 
{
	ReorderableListEventStruct_t1615631671::get_offset_of_DroppedObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1615631671::get_offset_of_FromIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1615631671::get_offset_of_FromList_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1615631671::get_offset_of_IsAClone_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1615631671::get_offset_of_SourceObject_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1615631671::get_offset_of_ToIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1615631671::get_offset_of_ToList_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (ReorderableListHandler_t1694188766), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (ReorderableListContent_t133253858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[5] = 
{
	ReorderableListContent_t133253858::get_offset_of__cachedChildren_2(),
	ReorderableListContent_t133253858::get_offset_of__cachedListElement_3(),
	ReorderableListContent_t133253858::get_offset_of__ele_4(),
	ReorderableListContent_t133253858::get_offset_of__extList_5(),
	ReorderableListContent_t133253858::get_offset_of__rect_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (U3CRefreshChildrenU3Ec__Iterator0_t2721765405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[4] = 
{
	U3CRefreshChildrenU3Ec__Iterator0_t2721765405::get_offset_of_U24this_0(),
	U3CRefreshChildrenU3Ec__Iterator0_t2721765405::get_offset_of_U24current_1(),
	U3CRefreshChildrenU3Ec__Iterator0_t2721765405::get_offset_of_U24disposing_2(),
	U3CRefreshChildrenU3Ec__Iterator0_t2721765405::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (ReorderableListDebug_t400625494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[1] = 
{
	ReorderableListDebug_t400625494::get_offset_of_DebugLabel_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (ReorderableListElement_t3127693425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[15] = 
{
	ReorderableListElement_t3127693425::get_offset_of_IsGrabbable_2(),
	ReorderableListElement_t3127693425::get_offset_of_IsTransferable_3(),
	ReorderableListElement_t3127693425::get_offset_of_isDroppableInSpace_4(),
	ReorderableListElement_t3127693425::get_offset_of__raycastResults_5(),
	ReorderableListElement_t3127693425::get_offset_of__currentReorderableListRaycasted_6(),
	ReorderableListElement_t3127693425::get_offset_of__draggingObject_7(),
	ReorderableListElement_t3127693425::get_offset_of__draggingObjectLE_8(),
	ReorderableListElement_t3127693425::get_offset_of__draggingObjectOriginalSize_9(),
	ReorderableListElement_t3127693425::get_offset_of__fakeElement_10(),
	ReorderableListElement_t3127693425::get_offset_of__fakeElementLE_11(),
	ReorderableListElement_t3127693425::get_offset_of__fromIndex_12(),
	ReorderableListElement_t3127693425::get_offset_of__isDragging_13(),
	ReorderableListElement_t3127693425::get_offset_of__rect_14(),
	ReorderableListElement_t3127693425::get_offset_of__reorderableList_15(),
	ReorderableListElement_t3127693425::get_offset_of_isValid_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (RescaleDragPanel_t3607722583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[4] = 
{
	RescaleDragPanel_t3607722583::get_offset_of_pointerOffset_2(),
	RescaleDragPanel_t3607722583::get_offset_of_canvasRectTransform_3(),
	RescaleDragPanel_t3607722583::get_offset_of_panelRectTransform_4(),
	RescaleDragPanel_t3607722583::get_offset_of_goTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (RescalePanel_t1108500937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[8] = 
{
	RescalePanel_t1108500937::get_offset_of_minSize_2(),
	RescalePanel_t1108500937::get_offset_of_maxSize_3(),
	RescalePanel_t1108500937::get_offset_of_rectTransform_4(),
	RescalePanel_t1108500937::get_offset_of_goTransform_5(),
	RescalePanel_t1108500937::get_offset_of_currentPointerPosition_6(),
	RescalePanel_t1108500937::get_offset_of_previousPointerPosition_7(),
	RescalePanel_t1108500937::get_offset_of_thisRectTransform_8(),
	RescalePanel_t1108500937::get_offset_of_sizeDelta_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (ResizePanel_t2063095658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[6] = 
{
	ResizePanel_t2063095658::get_offset_of_minSize_2(),
	ResizePanel_t2063095658::get_offset_of_maxSize_3(),
	ResizePanel_t2063095658::get_offset_of_rectTransform_4(),
	ResizePanel_t2063095658::get_offset_of_currentPointerPosition_5(),
	ResizePanel_t2063095658::get_offset_of_previousPointerPosition_6(),
	ResizePanel_t2063095658::get_offset_of_ratio_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (SegmentedControl_t4115597461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[8] = 
{
	SegmentedControl_t4115597461::get_offset_of_m_segments_2(),
	SegmentedControl_t4115597461::get_offset_of_m_separator_3(),
	SegmentedControl_t4115597461::get_offset_of_m_separatorWidth_4(),
	SegmentedControl_t4115597461::get_offset_of_m_allowSwitchingOff_5(),
	SegmentedControl_t4115597461::get_offset_of_m_selectedSegmentIndex_6(),
	SegmentedControl_t4115597461::get_offset_of_m_onValueChanged_7(),
	SegmentedControl_t4115597461::get_offset_of_selectedSegment_8(),
	SegmentedControl_t4115597461::get_offset_of_selectedColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (SegmentSelectedEvent_t4190223295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (Segment_t3146723167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[2] = 
{
	Segment_t3146723167::get_offset_of_index_2(),
	Segment_t3146723167::get_offset_of_textColor_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (ExampleSelectable_t2153456088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[5] = 
{
	ExampleSelectable_t2153456088::get_offset_of__selected_2(),
	ExampleSelectable_t2153456088::get_offset_of__preSelected_3(),
	ExampleSelectable_t2153456088::get_offset_of_spriteRenderer_4(),
	ExampleSelectable_t2153456088::get_offset_of_image_5(),
	ExampleSelectable_t2153456088::get_offset_of_text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (SelectionBox_t3294027723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[10] = 
{
	SelectionBox_t3294027723::get_offset_of_color_2(),
	SelectionBox_t3294027723::get_offset_of_art_3(),
	SelectionBox_t3294027723::get_offset_of_origin_4(),
	SelectionBox_t3294027723::get_offset_of_selectionMask_5(),
	SelectionBox_t3294027723::get_offset_of_boxRect_6(),
	SelectionBox_t3294027723::get_offset_of_selectables_7(),
	SelectionBox_t3294027723::get_offset_of_selectableGroup_8(),
	SelectionBox_t3294027723::get_offset_of_clickedBeforeDrag_9(),
	SelectionBox_t3294027723::get_offset_of_clickedAfterDrag_10(),
	SelectionBox_t3294027723::get_offset_of_onSelectionChange_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (SelectionEvent_t1038911497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (Stepper_t1023215691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[9] = 
{
	Stepper_t1023215691::get_offset_of__sides_2(),
	Stepper_t1023215691::get_offset_of__value_3(),
	Stepper_t1023215691::get_offset_of__minimum_4(),
	Stepper_t1023215691::get_offset_of__maximum_5(),
	Stepper_t1023215691::get_offset_of__step_6(),
	Stepper_t1023215691::get_offset_of__wrap_7(),
	Stepper_t1023215691::get_offset_of__separator_8(),
	Stepper_t1023215691::get_offset_of__separatorWidth_9(),
	Stepper_t1023215691::get_offset_of__onValueChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (StepperValueChangedEvent_t573788457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (StepperSide_t2103236692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (TextPic_t1856600851), -1, sizeof(TextPic_t1856600851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2078[23] = 
{
	TextPic_t1856600851::get_offset_of_m_ImagesPool_35(),
	TextPic_t1856600851::get_offset_of_culled_ImagesPool_36(),
	TextPic_t1856600851::get_offset_of_clearImages_37(),
	TextPic_t1856600851::get_offset_of_thisLock_38(),
	TextPic_t1856600851::get_offset_of_m_ImagesVertexIndex_39(),
	TextPic_t1856600851_StaticFields::get_offset_of_s_Regex_40(),
	TextPic_t1856600851::get_offset_of_fixedString_41(),
	TextPic_t1856600851::get_offset_of_m_ClickParents_42(),
	TextPic_t1856600851::get_offset_of_m_OutputText_43(),
	TextPic_t1856600851::get_offset_of_inspectorIconList_44(),
	TextPic_t1856600851::get_offset_of_ImageScalingFactor_45(),
	TextPic_t1856600851::get_offset_of_hyperlinkColor_46(),
	TextPic_t1856600851::get_offset_of_imageOffset_47(),
	TextPic_t1856600851::get_offset_of_button_48(),
	TextPic_t1856600851::get_offset_of_highlightselectable_49(),
	TextPic_t1856600851::get_offset_of_positions_50(),
	TextPic_t1856600851::get_offset_of_previousText_51(),
	TextPic_t1856600851::get_offset_of_isCreating_m_HrefInfos_52(),
	TextPic_t1856600851::get_offset_of_m_HrefInfos_53(),
	TextPic_t1856600851_StaticFields::get_offset_of_s_TextBuilder_54(),
	TextPic_t1856600851_StaticFields::get_offset_of_s_HrefRegex_55(),
	TextPic_t1856600851::get_offset_of_m_OnHrefClick_56(),
	TextPic_t1856600851_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (IconName_t4224240103)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[4] = 
{
	IconName_t4224240103::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IconName_t4224240103::get_offset_of_sprite_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IconName_t4224240103::get_offset_of_offset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IconName_t4224240103::get_offset_of_scale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (HrefClickEvent_t1044804460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (HrefInfo_t2348299680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[4] = 
{
	HrefInfo_t2348299680::get_offset_of_startIndex_0(),
	HrefInfo_t2348299680::get_offset_of_endIndex_1(),
	HrefInfo_t2348299680::get_offset_of_name_2(),
	HrefInfo_t2348299680::get_offset_of_boxes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (UI_Knob_t1007033269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[15] = 
{
	UI_Knob_t1007033269::get_offset_of_direction_2(),
	UI_Knob_t1007033269::get_offset_of_knobValue_3(),
	UI_Knob_t1007033269::get_offset_of_maxValue_4(),
	UI_Knob_t1007033269::get_offset_of_loops_5(),
	UI_Knob_t1007033269::get_offset_of_clampOutput01_6(),
	UI_Knob_t1007033269::get_offset_of_snapToPosition_7(),
	UI_Knob_t1007033269::get_offset_of_snapStepsPerLoop_8(),
	UI_Knob_t1007033269::get_offset_of_OnValueChanged_9(),
	UI_Knob_t1007033269::get_offset_of__currentLoops_10(),
	UI_Knob_t1007033269::get_offset_of__previousValue_11(),
	UI_Knob_t1007033269::get_offset_of__initAngle_12(),
	UI_Knob_t1007033269::get_offset_of__currentAngle_13(),
	UI_Knob_t1007033269::get_offset_of__currentVector_14(),
	UI_Knob_t1007033269::get_offset_of__initRotation_15(),
	UI_Knob_t1007033269::get_offset_of__canDrag_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (Direction_t3408261084)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[3] = 
{
	Direction_t3408261084::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (KnobFloatValueEvent_t2705095779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (BestFitOutline_t2386741245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (CUIBezierCurve_t3289454031), -1, sizeof(CUIBezierCurve_t3289454031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2086[3] = 
{
	CUIBezierCurve_t3289454031_StaticFields::get_offset_of_CubicBezierCurvePtNum_2(),
	CUIBezierCurve_t3289454031::get_offset_of_controlPoints_3(),
	CUIBezierCurve_t3289454031::get_offset_of_OnRefresh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (CUIGraphic_t893279881), -1, sizeof(CUIGraphic_t893279881_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2087[11] = 
{
	CUIGraphic_t893279881_StaticFields::get_offset_of_bottomCurveIdx_3(),
	CUIGraphic_t893279881_StaticFields::get_offset_of_topCurveIdx_4(),
	CUIGraphic_t893279881::get_offset_of_isCurved_5(),
	CUIGraphic_t893279881::get_offset_of_isLockWithRatio_6(),
	CUIGraphic_t893279881::get_offset_of_resolution_7(),
	CUIGraphic_t893279881::get_offset_of_rectTrans_8(),
	CUIGraphic_t893279881::get_offset_of_uiGraphic_9(),
	CUIGraphic_t893279881::get_offset_of_refCUIGraphic_10(),
	CUIGraphic_t893279881::get_offset_of_refCurves_11(),
	CUIGraphic_t893279881::get_offset_of_refCurvesControlRatioPoints_12(),
	CUIGraphic_t893279881::get_offset_of_reuse_quads_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (CUIImage_t1001140650), -1, sizeof(CUIImage_t1001140650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2088[4] = 
{
	CUIImage_t1001140650_StaticFields::get_offset_of_SlicedImageCornerRefVertexIdx_14(),
	CUIImage_t1001140650_StaticFields::get_offset_of_FilledImageCornerRefVertexIdx_15(),
	CUIImage_t1001140650::get_offset_of_cornerPosRatio_16(),
	CUIImage_t1001140650::get_offset_of_oriCornerPosRatio_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (Vector3_Array2D_t3054885220)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[1] = 
{
	Vector3_Array2D_t3054885220::get_offset_of_array_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (CUIText_t2456700724), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (CurvedText_t3665035540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[3] = 
{
	CurvedText_t3665035540::get_offset_of__curveForText_3(),
	CurvedText_t3665035540::get_offset_of__curveMultiplier_4(),
	CurvedText_t3665035540::get_offset_of_rectTrans_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (CylinderText_t2066901577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[2] = 
{
	CylinderText_t2066901577::get_offset_of_radius_3(),
	CylinderText_t2066901577::get_offset_of_rectTrans_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (Gradient_t585968992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[6] = 
{
	Gradient_t585968992::get_offset_of__gradientMode_3(),
	Gradient_t585968992::get_offset_of__gradientDir_4(),
	Gradient_t585968992::get_offset_of__overwriteAllColor_5(),
	Gradient_t585968992::get_offset_of__vertex1_6(),
	Gradient_t585968992::get_offset_of__vertex2_7(),
	Gradient_t585968992::get_offset_of_targetGraphic_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (GradientMode_t1452379243)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2094[3] = 
{
	GradientMode_t1452379243::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (GradientDir_t2268096981)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[5] = 
{
	GradientDir_t2268096981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (Gradient2_t998070926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[4] = 
{
	Gradient2_t998070926::get_offset_of__gradientType_3(),
	Gradient2_t998070926::get_offset_of__blendMode_4(),
	Gradient2_t998070926::get_offset_of__offset_5(),
	Gradient2_t998070926::get_offset_of__effectGradient_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (Type_t763701076)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	Type_t763701076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (Blend_t3762654957)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[4] = 
{
	Blend_t3762654957::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (LetterSpacing_t4139949939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[1] = 
{
	LetterSpacing_t4139949939::get_offset_of_m_spacing_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
