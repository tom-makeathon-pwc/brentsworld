﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.UI.Extensions.UIVerticalScroller
struct UIVerticalScroller_t1840308474;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.UI.Extensions.SelectableScaler
struct SelectableScaler_t1409550150;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// System.Void
struct Void_t1841601450;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.UI.Extensions.ScrollRectTweener
struct ScrollRectTweener_t3873690283;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t540192618;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.UI.Extensions.ShineEffect
struct ShineEffect_t1749864756;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Extensions.Menu[]
struct MenuU5BU5D_t2855465670;
// System.Collections.Generic.Stack`1<UnityEngine.UI.Extensions.Menu>
struct Stack_1_t2297400569;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t3083107861;
// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t2875670365;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2110227463;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange
struct PageSnapChange_t737912504;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeStartEvent
struct SelectionChangeStartEvent_t1331424750;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent
struct SelectionPageChangedEvent_t3967268665;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent
struct SelectionChangeEndEvent_t3994187929;
// UnityEngine.UI.Extensions.IScrollSnap
struct IScrollSnap_t1585037130;
// UnityEngine.UI.Extensions.InputFieldEnterSubmit/EnterSubmitEvent
struct EnterSubmitEvent_t3688949694;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t3529018992;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t3345875600;
// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_t2419724583;
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t2318645467;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t859513320;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t3244928895;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2665681875;
// UnityEngine.UI.Extensions.ScrollPositionController/UpdatePositionEvent
struct UpdatePositionEvent_t4030317844;
// UnityEngine.UI.Extensions.ScrollPositionController/ItemSelectedEvent
struct ItemSelectedEvent_t2604723240;
// System.Collections.Generic.List`1<UnityEngine.UI.ExtensionsToggle>
struct List_1_t2009014410;
// UnityEngine.UI.ExtensionsToggleGroup/ToggleGroupEvent
struct ToggleGroupEvent_t1021581118;
// UnityEngine.UI.ExtensionsToggle
struct ExtensionsToggle_t2639893278;
// System.Predicate`1<UnityEngine.UI.ExtensionsToggle>
struct Predicate_1_t1082863393;
// System.Func`2<UnityEngine.UI.ExtensionsToggle,System.Boolean>
struct Func_2_t1387565497;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t1524870173;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t621514313;
// System.Collections.Generic.IList`1<UnityEngine.RectTransform>
struct IList_1_t3890906783;
// UnityEngine.UI.Extensions.ScrollSnapBase
struct ScrollSnapBase_t805675194;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.UI.ExtensionsToggleGroup
struct ExtensionsToggleGroup_t1289496689;
// UnityEngine.UI.ExtensionsToggle/ToggleEvent
struct ToggleEvent_t2514518788;
// UnityEngine.UI.ExtensionsToggle/ToggleEventObject
struct ToggleEventObject_t2759065715;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t607610358;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t3572864619;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t892265570;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CADDLISTENERU3EC__ANONSTOREY0_T2102329242_H
#define U3CADDLISTENERU3EC__ANONSTOREY0_T2102329242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStorey0
struct  U3CAddListenerU3Ec__AnonStorey0_t2102329242  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStorey0::index
	int32_t ___index_0;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStorey0::$this
	UIVerticalScroller_t1840308474 * ___U24this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CAddListenerU3Ec__AnonStorey0_t2102329242, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAddListenerU3Ec__AnonStorey0_t2102329242, ___U24this_1)); }
	inline UIVerticalScroller_t1840308474 * get_U24this_1() const { return ___U24this_1; }
	inline UIVerticalScroller_t1840308474 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UIVerticalScroller_t1840308474 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDLISTENERU3EC__ANONSTOREY0_T2102329242_H
#ifndef U3CONENDDRAGU3EC__ANONSTOREY3_T2954120386_H
#define U3CONENDDRAGU3EC__ANONSTOREY3_T2954120386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectEx/<OnEndDrag>c__AnonStorey3
struct  U3COnEndDragU3Ec__AnonStorey3_t2954120386  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.ScrollRectEx/<OnEndDrag>c__AnonStorey3::eventData
	PointerEventData_t1599784723 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnEndDragU3Ec__AnonStorey3_t2954120386, ___eventData_0)); }
	inline PointerEventData_t1599784723 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1599784723 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1599784723 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONENDDRAGU3EC__ANONSTOREY3_T2954120386_H
#ifndef U3CSCALEINU3EC__ITERATOR0_T463378026_H
#define U3CSCALEINU3EC__ITERATOR0_T463378026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SelectableScaler/<ScaleIN>c__Iterator0
struct  U3CScaleINU3Ec__Iterator0_t463378026  : public RuntimeObject
{
public:
	// System.Single UnityEngine.UI.Extensions.SelectableScaler/<ScaleIN>c__Iterator0::<t>__1
	float ___U3CtU3E__1_0;
	// System.Single UnityEngine.UI.Extensions.SelectableScaler/<ScaleIN>c__Iterator0::<maxT>__1
	float ___U3CmaxTU3E__1_1;
	// UnityEngine.UI.Extensions.SelectableScaler UnityEngine.UI.Extensions.SelectableScaler/<ScaleIN>c__Iterator0::$this
	SelectableScaler_t1409550150 * ___U24this_2;
	// System.Object UnityEngine.UI.Extensions.SelectableScaler/<ScaleIN>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.Extensions.SelectableScaler/<ScaleIN>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.Extensions.SelectableScaler/<ScaleIN>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__1_0() { return static_cast<int32_t>(offsetof(U3CScaleINU3Ec__Iterator0_t463378026, ___U3CtU3E__1_0)); }
	inline float get_U3CtU3E__1_0() const { return ___U3CtU3E__1_0; }
	inline float* get_address_of_U3CtU3E__1_0() { return &___U3CtU3E__1_0; }
	inline void set_U3CtU3E__1_0(float value)
	{
		___U3CtU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CmaxTU3E__1_1() { return static_cast<int32_t>(offsetof(U3CScaleINU3Ec__Iterator0_t463378026, ___U3CmaxTU3E__1_1)); }
	inline float get_U3CmaxTU3E__1_1() const { return ___U3CmaxTU3E__1_1; }
	inline float* get_address_of_U3CmaxTU3E__1_1() { return &___U3CmaxTU3E__1_1; }
	inline void set_U3CmaxTU3E__1_1(float value)
	{
		___U3CmaxTU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CScaleINU3Ec__Iterator0_t463378026, ___U24this_2)); }
	inline SelectableScaler_t1409550150 * get_U24this_2() const { return ___U24this_2; }
	inline SelectableScaler_t1409550150 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SelectableScaler_t1409550150 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CScaleINU3Ec__Iterator0_t463378026, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CScaleINU3Ec__Iterator0_t463378026, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CScaleINU3Ec__Iterator0_t463378026, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCALEINU3EC__ITERATOR0_T463378026_H
#ifndef U3CONINITIALIZEPOTENTIALDRAGU3EC__ANONSTOREY0_T4251053400_H
#define U3CONINITIALIZEPOTENTIALDRAGU3EC__ANONSTOREY0_T4251053400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectEx/<OnInitializePotentialDrag>c__AnonStorey0
struct  U3COnInitializePotentialDragU3Ec__AnonStorey0_t4251053400  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.ScrollRectEx/<OnInitializePotentialDrag>c__AnonStorey0::eventData
	PointerEventData_t1599784723 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnInitializePotentialDragU3Ec__AnonStorey0_t4251053400, ___eventData_0)); }
	inline PointerEventData_t1599784723 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1599784723 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1599784723 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONINITIALIZEPOTENTIALDRAGU3EC__ANONSTOREY0_T4251053400_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef FANCYSCROLLVIEWNULLCONTEXT_T4203696347_H
#define FANCYSCROLLVIEWNULLCONTEXT_T4203696347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollViewNullContext
struct  FancyScrollViewNullContext_t4203696347  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEWNULLCONTEXT_T4203696347_H
#ifndef U3CONDRAGU3EC__ANONSTOREY1_T2404555749_H
#define U3CONDRAGU3EC__ANONSTOREY1_T2404555749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectEx/<OnDrag>c__AnonStorey1
struct  U3COnDragU3Ec__AnonStorey1_t2404555749  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.ScrollRectEx/<OnDrag>c__AnonStorey1::eventData
	PointerEventData_t1599784723 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnDragU3Ec__AnonStorey1_t2404555749, ___eventData_0)); }
	inline PointerEventData_t1599784723 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1599784723 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1599784723 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONDRAGU3EC__ANONSTOREY1_T2404555749_H
#ifndef U3CONBEGINDRAGU3EC__ANONSTOREY2_T3035827229_H
#define U3CONBEGINDRAGU3EC__ANONSTOREY2_T3035827229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectEx/<OnBeginDrag>c__AnonStorey2
struct  U3COnBeginDragU3Ec__AnonStorey2_t3035827229  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.ScrollRectEx/<OnBeginDrag>c__AnonStorey2::eventData
	PointerEventData_t1599784723 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnBeginDragU3Ec__AnonStorey2_t3035827229, ___eventData_0)); }
	inline PointerEventData_t1599784723 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1599784723 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1599784723 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBEGINDRAGU3EC__ANONSTOREY2_T3035827229_H
#ifndef U3CONSCROLLU3EC__ANONSTOREY4_T4166373289_H
#define U3CONSCROLLU3EC__ANONSTOREY4_T4166373289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectEx/<OnScroll>c__AnonStorey4
struct  U3COnScrollU3Ec__AnonStorey4_t4166373289  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.ScrollRectEx/<OnScroll>c__AnonStorey4::eventData
	PointerEventData_t1599784723 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnScrollU3Ec__AnonStorey4_t4166373289, ___eventData_0)); }
	inline PointerEventData_t1599784723 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1599784723 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1599784723 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSCROLLU3EC__ANONSTOREY4_T4166373289_H
#ifndef BEZIERPATH_T875060952_H
#define BEZIERPATH_T875060952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BezierPath
struct  BezierPath_t875060952  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Extensions.BezierPath::SegmentsPerCurve
	int32_t ___SegmentsPerCurve_0;
	// System.Single UnityEngine.UI.Extensions.BezierPath::MINIMUM_SQR_DISTANCE
	float ___MINIMUM_SQR_DISTANCE_1;
	// System.Single UnityEngine.UI.Extensions.BezierPath::DIVISION_THRESHOLD
	float ___DIVISION_THRESHOLD_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.Extensions.BezierPath::controlPoints
	List_1_t1612828711 * ___controlPoints_3;
	// System.Int32 UnityEngine.UI.Extensions.BezierPath::curveCount
	int32_t ___curveCount_4;

public:
	inline static int32_t get_offset_of_SegmentsPerCurve_0() { return static_cast<int32_t>(offsetof(BezierPath_t875060952, ___SegmentsPerCurve_0)); }
	inline int32_t get_SegmentsPerCurve_0() const { return ___SegmentsPerCurve_0; }
	inline int32_t* get_address_of_SegmentsPerCurve_0() { return &___SegmentsPerCurve_0; }
	inline void set_SegmentsPerCurve_0(int32_t value)
	{
		___SegmentsPerCurve_0 = value;
	}

	inline static int32_t get_offset_of_MINIMUM_SQR_DISTANCE_1() { return static_cast<int32_t>(offsetof(BezierPath_t875060952, ___MINIMUM_SQR_DISTANCE_1)); }
	inline float get_MINIMUM_SQR_DISTANCE_1() const { return ___MINIMUM_SQR_DISTANCE_1; }
	inline float* get_address_of_MINIMUM_SQR_DISTANCE_1() { return &___MINIMUM_SQR_DISTANCE_1; }
	inline void set_MINIMUM_SQR_DISTANCE_1(float value)
	{
		___MINIMUM_SQR_DISTANCE_1 = value;
	}

	inline static int32_t get_offset_of_DIVISION_THRESHOLD_2() { return static_cast<int32_t>(offsetof(BezierPath_t875060952, ___DIVISION_THRESHOLD_2)); }
	inline float get_DIVISION_THRESHOLD_2() const { return ___DIVISION_THRESHOLD_2; }
	inline float* get_address_of_DIVISION_THRESHOLD_2() { return &___DIVISION_THRESHOLD_2; }
	inline void set_DIVISION_THRESHOLD_2(float value)
	{
		___DIVISION_THRESHOLD_2 = value;
	}

	inline static int32_t get_offset_of_controlPoints_3() { return static_cast<int32_t>(offsetof(BezierPath_t875060952, ___controlPoints_3)); }
	inline List_1_t1612828711 * get_controlPoints_3() const { return ___controlPoints_3; }
	inline List_1_t1612828711 ** get_address_of_controlPoints_3() { return &___controlPoints_3; }
	inline void set_controlPoints_3(List_1_t1612828711 * value)
	{
		___controlPoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoints_3), value);
	}

	inline static int32_t get_offset_of_curveCount_4() { return static_cast<int32_t>(offsetof(BezierPath_t875060952, ___curveCount_4)); }
	inline int32_t get_curveCount_4() const { return ___curveCount_4; }
	inline int32_t* get_address_of_curveCount_4() { return &___curveCount_4; }
	inline void set_curveCount_4(int32_t value)
	{
		___curveCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERPATH_T875060952_H
#ifndef SCROLLRECTEXTENSIONS_T449593713_H
#define SCROLLRECTEXTENSIONS_T449593713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectExtensions
struct  ScrollRectExtensions_t449593713  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECTEXTENSIONS_T449593713_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef CLZF2_T824178053_H
#define CLZF2_T824178053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CLZF2
struct  CLZF2_t824178053  : public RuntimeObject
{
public:

public:
};

struct CLZF2_t824178053_StaticFields
{
public:
	// System.UInt32 UnityEngine.UI.Extensions.CLZF2::HLOG
	uint32_t ___HLOG_0;
	// System.UInt32 UnityEngine.UI.Extensions.CLZF2::HSIZE
	uint32_t ___HSIZE_1;
	// System.UInt32 UnityEngine.UI.Extensions.CLZF2::MAX_LIT
	uint32_t ___MAX_LIT_2;
	// System.UInt32 UnityEngine.UI.Extensions.CLZF2::MAX_OFF
	uint32_t ___MAX_OFF_3;
	// System.UInt32 UnityEngine.UI.Extensions.CLZF2::MAX_REF
	uint32_t ___MAX_REF_4;
	// System.Int64[] UnityEngine.UI.Extensions.CLZF2::HashTable
	Int64U5BU5D_t717125112* ___HashTable_5;

public:
	inline static int32_t get_offset_of_HLOG_0() { return static_cast<int32_t>(offsetof(CLZF2_t824178053_StaticFields, ___HLOG_0)); }
	inline uint32_t get_HLOG_0() const { return ___HLOG_0; }
	inline uint32_t* get_address_of_HLOG_0() { return &___HLOG_0; }
	inline void set_HLOG_0(uint32_t value)
	{
		___HLOG_0 = value;
	}

	inline static int32_t get_offset_of_HSIZE_1() { return static_cast<int32_t>(offsetof(CLZF2_t824178053_StaticFields, ___HSIZE_1)); }
	inline uint32_t get_HSIZE_1() const { return ___HSIZE_1; }
	inline uint32_t* get_address_of_HSIZE_1() { return &___HSIZE_1; }
	inline void set_HSIZE_1(uint32_t value)
	{
		___HSIZE_1 = value;
	}

	inline static int32_t get_offset_of_MAX_LIT_2() { return static_cast<int32_t>(offsetof(CLZF2_t824178053_StaticFields, ___MAX_LIT_2)); }
	inline uint32_t get_MAX_LIT_2() const { return ___MAX_LIT_2; }
	inline uint32_t* get_address_of_MAX_LIT_2() { return &___MAX_LIT_2; }
	inline void set_MAX_LIT_2(uint32_t value)
	{
		___MAX_LIT_2 = value;
	}

	inline static int32_t get_offset_of_MAX_OFF_3() { return static_cast<int32_t>(offsetof(CLZF2_t824178053_StaticFields, ___MAX_OFF_3)); }
	inline uint32_t get_MAX_OFF_3() const { return ___MAX_OFF_3; }
	inline uint32_t* get_address_of_MAX_OFF_3() { return &___MAX_OFF_3; }
	inline void set_MAX_OFF_3(uint32_t value)
	{
		___MAX_OFF_3 = value;
	}

	inline static int32_t get_offset_of_MAX_REF_4() { return static_cast<int32_t>(offsetof(CLZF2_t824178053_StaticFields, ___MAX_REF_4)); }
	inline uint32_t get_MAX_REF_4() const { return ___MAX_REF_4; }
	inline uint32_t* get_address_of_MAX_REF_4() { return &___MAX_REF_4; }
	inline void set_MAX_REF_4(uint32_t value)
	{
		___MAX_REF_4 = value;
	}

	inline static int32_t get_offset_of_HashTable_5() { return static_cast<int32_t>(offsetof(CLZF2_t824178053_StaticFields, ___HashTable_5)); }
	inline Int64U5BU5D_t717125112* get_HashTable_5() const { return ___HashTable_5; }
	inline Int64U5BU5D_t717125112** get_address_of_HashTable_5() { return &___HashTable_5; }
	inline void set_HashTable_5(Int64U5BU5D_t717125112* value)
	{
		___HashTable_5 = value;
		Il2CppCodeGenWriteBarrier((&___HashTable_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLZF2_T824178053_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef CIRCLE_T1341526964_H
#define CIRCLE_T1341526964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Circle
struct  Circle_t1341526964  : public RuntimeObject
{
public:
	// System.Single UnityEngine.UI.Extensions.Circle::xAxis
	float ___xAxis_0;
	// System.Single UnityEngine.UI.Extensions.Circle::yAxis
	float ___yAxis_1;
	// System.Int32 UnityEngine.UI.Extensions.Circle::steps
	int32_t ___steps_2;

public:
	inline static int32_t get_offset_of_xAxis_0() { return static_cast<int32_t>(offsetof(Circle_t1341526964, ___xAxis_0)); }
	inline float get_xAxis_0() const { return ___xAxis_0; }
	inline float* get_address_of_xAxis_0() { return &___xAxis_0; }
	inline void set_xAxis_0(float value)
	{
		___xAxis_0 = value;
	}

	inline static int32_t get_offset_of_yAxis_1() { return static_cast<int32_t>(offsetof(Circle_t1341526964, ___yAxis_1)); }
	inline float get_yAxis_1() const { return ___yAxis_1; }
	inline float* get_address_of_yAxis_1() { return &___yAxis_1; }
	inline void set_yAxis_1(float value)
	{
		___yAxis_1 = value;
	}

	inline static int32_t get_offset_of_steps_2() { return static_cast<int32_t>(offsetof(Circle_t1341526964, ___steps_2)); }
	inline int32_t get_steps_2() const { return ___steps_2; }
	inline int32_t* get_address_of_steps_2() { return &___steps_2; }
	inline void set_steps_2(int32_t value)
	{
		___steps_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLE_T1341526964_H
#ifndef SNAP_T1413478282_H
#define SNAP_T1413478282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollPositionController/Snap
struct  Snap_t1413478282 
{
public:
	// System.Boolean UnityEngine.UI.Extensions.ScrollPositionController/Snap::Enable
	bool ___Enable_0;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController/Snap::VelocityThreshold
	float ___VelocityThreshold_1;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController/Snap::Duration
	float ___Duration_2;

public:
	inline static int32_t get_offset_of_Enable_0() { return static_cast<int32_t>(offsetof(Snap_t1413478282, ___Enable_0)); }
	inline bool get_Enable_0() const { return ___Enable_0; }
	inline bool* get_address_of_Enable_0() { return &___Enable_0; }
	inline void set_Enable_0(bool value)
	{
		___Enable_0 = value;
	}

	inline static int32_t get_offset_of_VelocityThreshold_1() { return static_cast<int32_t>(offsetof(Snap_t1413478282, ___VelocityThreshold_1)); }
	inline float get_VelocityThreshold_1() const { return ___VelocityThreshold_1; }
	inline float* get_address_of_VelocityThreshold_1() { return &___VelocityThreshold_1; }
	inline void set_VelocityThreshold_1(float value)
	{
		___VelocityThreshold_1 = value;
	}

	inline static int32_t get_offset_of_Duration_2() { return static_cast<int32_t>(offsetof(Snap_t1413478282, ___Duration_2)); }
	inline float get_Duration_2() const { return ___Duration_2; }
	inline float* get_address_of_Duration_2() { return &___Duration_2; }
	inline void set_Duration_2(float value)
	{
		___Duration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.ScrollPositionController/Snap
struct Snap_t1413478282_marshaled_pinvoke
{
	int32_t ___Enable_0;
	float ___VelocityThreshold_1;
	float ___Duration_2;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.ScrollPositionController/Snap
struct Snap_t1413478282_marshaled_com
{
	int32_t ___Enable_0;
	float ___VelocityThreshold_1;
	float ___Duration_2;
};
#endif // SNAP_T1413478282_H
#ifndef UNITYEVENT_1_T2067570248_H
#define UNITYEVENT_1_T2067570248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2067570248  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2067570248, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2067570248_H
#ifndef UNITYEVENT_1_T2678243293_H
#define UNITYEVENT_1_T2678243293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.UI.ExtensionsToggle>
struct  UnityEvent_1_t2678243293  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2678243293, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2678243293_H
#ifndef PROPERTYATTRIBUTE_T2606999759_H
#define PROPERTYATTRIBUTE_T2606999759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t2606999759  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T2606999759_H
#ifndef UNITYEVENT_1_T3863924733_H
#define UNITYEVENT_1_T3863924733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t3863924733  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3863924733, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3863924733_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef UNITYEVENT_1_T2110227463_H
#define UNITYEVENT_1_T2110227463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2110227463  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2110227463, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2110227463_H
#ifndef UNITYEVENT_1_T2114859947_H
#define UNITYEVENT_1_T2114859947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2114859947  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2114859947, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2114859947_H
#ifndef TEXTURESHEETANIMATIONMODULE_T4262561859_H
#define TEXTURESHEETANIMATIONMODULE_T4262561859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct  TextureSheetAnimationModule_t4262561859 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/TextureSheetAnimationModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(TextureSheetAnimationModule_t4262561859, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t4262561859_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t4262561859_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // TEXTURESHEETANIMATIONMODULE_T4262561859_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef MAINMODULE_T6751348_H
#define MAINMODULE_T6751348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t6751348 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t6751348, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T6751348_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SPRITESTATE_T1353336012_H
#define SPRITESTATE_T1353336012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1353336012 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t309593783 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t309593783 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_HighlightedSprite_0)); }
	inline Sprite_t309593783 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t309593783 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t309593783 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_PressedSprite_1)); }
	inline Sprite_t309593783 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t309593783 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t309593783 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_DisabledSprite_2)); }
	inline Sprite_t309593783 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t309593783 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t309593783 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_pinvoke
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_com
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1353336012_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef UNITYEVENT_T408735097_H
#define UNITYEVENT_T408735097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t408735097  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t408735097, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T408735097_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef RESOLUTIONMODE_T1915409729_H
#define RESOLUTIONMODE_T1915409729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ResolutionMode
struct  ResolutionMode_t1915409729 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.ResolutionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResolutionMode_t1915409729, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONMODE_T1915409729_H
#ifndef NAVIGATIONMODE_T1772877217_H
#define NAVIGATIONMODE_T1772877217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.NavigationMode
struct  NavigationMode_t1772877217 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.NavigationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NavigationMode_t1772877217, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVIGATIONMODE_T1772877217_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef CABLECURVE_T3653995216_H
#define CABLECURVE_T3653995216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CableCurve
struct  CableCurve_t3653995216  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.CableCurve::m_start
	Vector2_t2243707579  ___m_start_0;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.CableCurve::m_end
	Vector2_t2243707579  ___m_end_1;
	// System.Single UnityEngine.UI.Extensions.CableCurve::m_slack
	float ___m_slack_2;
	// System.Int32 UnityEngine.UI.Extensions.CableCurve::m_steps
	int32_t ___m_steps_3;
	// System.Boolean UnityEngine.UI.Extensions.CableCurve::m_regen
	bool ___m_regen_4;
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.CableCurve::points
	Vector2U5BU5D_t686124026* ___points_6;

public:
	inline static int32_t get_offset_of_m_start_0() { return static_cast<int32_t>(offsetof(CableCurve_t3653995216, ___m_start_0)); }
	inline Vector2_t2243707579  get_m_start_0() const { return ___m_start_0; }
	inline Vector2_t2243707579 * get_address_of_m_start_0() { return &___m_start_0; }
	inline void set_m_start_0(Vector2_t2243707579  value)
	{
		___m_start_0 = value;
	}

	inline static int32_t get_offset_of_m_end_1() { return static_cast<int32_t>(offsetof(CableCurve_t3653995216, ___m_end_1)); }
	inline Vector2_t2243707579  get_m_end_1() const { return ___m_end_1; }
	inline Vector2_t2243707579 * get_address_of_m_end_1() { return &___m_end_1; }
	inline void set_m_end_1(Vector2_t2243707579  value)
	{
		___m_end_1 = value;
	}

	inline static int32_t get_offset_of_m_slack_2() { return static_cast<int32_t>(offsetof(CableCurve_t3653995216, ___m_slack_2)); }
	inline float get_m_slack_2() const { return ___m_slack_2; }
	inline float* get_address_of_m_slack_2() { return &___m_slack_2; }
	inline void set_m_slack_2(float value)
	{
		___m_slack_2 = value;
	}

	inline static int32_t get_offset_of_m_steps_3() { return static_cast<int32_t>(offsetof(CableCurve_t3653995216, ___m_steps_3)); }
	inline int32_t get_m_steps_3() const { return ___m_steps_3; }
	inline int32_t* get_address_of_m_steps_3() { return &___m_steps_3; }
	inline void set_m_steps_3(int32_t value)
	{
		___m_steps_3 = value;
	}

	inline static int32_t get_offset_of_m_regen_4() { return static_cast<int32_t>(offsetof(CableCurve_t3653995216, ___m_regen_4)); }
	inline bool get_m_regen_4() const { return ___m_regen_4; }
	inline bool* get_address_of_m_regen_4() { return &___m_regen_4; }
	inline void set_m_regen_4(bool value)
	{
		___m_regen_4 = value;
	}

	inline static int32_t get_offset_of_points_6() { return static_cast<int32_t>(offsetof(CableCurve_t3653995216, ___points_6)); }
	inline Vector2U5BU5D_t686124026* get_points_6() const { return ___points_6; }
	inline Vector2U5BU5D_t686124026** get_address_of_points_6() { return &___points_6; }
	inline void set_points_6(Vector2U5BU5D_t686124026* value)
	{
		___points_6 = value;
		Il2CppCodeGenWriteBarrier((&___points_6), value);
	}
};

struct CableCurve_t3653995216_StaticFields
{
public:
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.CableCurve::emptyCurve
	Vector2U5BU5D_t686124026* ___emptyCurve_5;

public:
	inline static int32_t get_offset_of_emptyCurve_5() { return static_cast<int32_t>(offsetof(CableCurve_t3653995216_StaticFields, ___emptyCurve_5)); }
	inline Vector2U5BU5D_t686124026* get_emptyCurve_5() const { return ___emptyCurve_5; }
	inline Vector2U5BU5D_t686124026** get_address_of_emptyCurve_5() { return &___emptyCurve_5; }
	inline void set_emptyCurve_5(Vector2U5BU5D_t686124026* value)
	{
		___emptyCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___emptyCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CABLECURVE_T3653995216_H
#ifndef U3CDOMOVEU3EC__ITERATOR0_T945211788_H
#define U3CDOMOVEU3EC__ITERATOR0_T945211788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0
struct  U3CDoMoveU3Ec__Iterator0_t945211788  : public RuntimeObject
{
public:
	// System.Single UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0::duration
	float ___duration_0;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0::<posOffset>__0
	Vector2_t2243707579  ___U3CposOffsetU3E__0_1;
	// System.Single UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0::<currentTime>__0
	float ___U3CcurrentTimeU3E__0_2;
	// UnityEngine.UI.Extensions.ScrollRectTweener UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0::$this
	ScrollRectTweener_t3873690283 * ___U24this_3;
	// System.Object UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityEngine.UI.Extensions.ScrollRectTweener/<DoMove>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(U3CDoMoveU3Ec__Iterator0_t945211788, ___duration_0)); }
	inline float get_duration_0() const { return ___duration_0; }
	inline float* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(float value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of_U3CposOffsetU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoMoveU3Ec__Iterator0_t945211788, ___U3CposOffsetU3E__0_1)); }
	inline Vector2_t2243707579  get_U3CposOffsetU3E__0_1() const { return ___U3CposOffsetU3E__0_1; }
	inline Vector2_t2243707579 * get_address_of_U3CposOffsetU3E__0_1() { return &___U3CposOffsetU3E__0_1; }
	inline void set_U3CposOffsetU3E__0_1(Vector2_t2243707579  value)
	{
		___U3CposOffsetU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDoMoveU3Ec__Iterator0_t945211788, ___U3CcurrentTimeU3E__0_2)); }
	inline float get_U3CcurrentTimeU3E__0_2() const { return ___U3CcurrentTimeU3E__0_2; }
	inline float* get_address_of_U3CcurrentTimeU3E__0_2() { return &___U3CcurrentTimeU3E__0_2; }
	inline void set_U3CcurrentTimeU3E__0_2(float value)
	{
		___U3CcurrentTimeU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDoMoveU3Ec__Iterator0_t945211788, ___U24this_3)); }
	inline ScrollRectTweener_t3873690283 * get_U24this_3() const { return ___U24this_3; }
	inline ScrollRectTweener_t3873690283 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ScrollRectTweener_t3873690283 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDoMoveU3Ec__Iterator0_t945211788, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDoMoveU3Ec__Iterator0_t945211788, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDoMoveU3Ec__Iterator0_t945211788, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEU3EC__ITERATOR0_T945211788_H
#ifndef ENTERSUBMITEVENT_T3688949694_H
#define ENTERSUBMITEVENT_T3688949694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.InputFieldEnterSubmit/EnterSubmitEvent
struct  EnterSubmitEvent_t3688949694  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTERSUBMITEVENT_T3688949694_H
#ifndef RENDERMODE_T4280533217_H
#define RENDERMODE_T4280533217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4280533217 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4280533217, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4280533217_H
#ifndef READONLYATTRIBUTE_T2117389128_H
#define READONLYATTRIBUTE_T2117389128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReadOnlyAttribute
struct  ReadOnlyAttribute_t2117389128  : public PropertyAttribute_t2606999759
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T2117389128_H
#ifndef TOGGLEGROUPEVENT_T1021581118_H
#define TOGGLEGROUPEVENT_T1021581118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ExtensionsToggleGroup/ToggleGroupEvent
struct  ToggleGroupEvent_t1021581118  : public UnityEvent_1_t3863924733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEGROUPEVENT_T1021581118_H
#ifndef TOGGLETRANSITION_T2798030653_H
#define TOGGLETRANSITION_T2798030653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ExtensionsToggle/ToggleTransition
struct  ToggleTransition_t2798030653 
{
public:
	// System.Int32 UnityEngine.UI.ExtensionsToggle/ToggleTransition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ToggleTransition_t2798030653, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRANSITION_T2798030653_H
#ifndef TOGGLEEVENT_T2514518788_H
#define TOGGLEEVENT_T2514518788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ExtensionsToggle/ToggleEvent
struct  ToggleEvent_t2514518788  : public UnityEvent_1_t3863924733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEEVENT_T2514518788_H
#ifndef TOGGLEEVENTOBJECT_T2759065715_H
#define TOGGLEEVENTOBJECT_T2759065715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ExtensionsToggle/ToggleEventObject
struct  ToggleEventObject_t2759065715  : public UnityEvent_1_t2678243293
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEEVENTOBJECT_T2759065715_H
#ifndef BEZIERTYPE_T670022450_H
#define BEZIERTYPE_T670022450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILineRenderer/BezierType
struct  BezierType_t670022450 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UILineRenderer/BezierType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BezierType_t670022450, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERTYPE_T670022450_H
#ifndef SELECTIONPAGECHANGEDEVENT_T3967268665_H
#define SELECTIONPAGECHANGEDEVENT_T3967268665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent
struct  SelectionPageChangedEvent_t3967268665  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONPAGECHANGEDEVENT_T3967268665_H
#ifndef SELECTIONCHANGEENDEVENT_T3994187929_H
#define SELECTIONCHANGEENDEVENT_T3994187929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent
struct  SelectionChangeEndEvent_t3994187929  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEENDEVENT_T3994187929_H
#ifndef MOVEMENTTYPE_T905360158_H
#define MOVEMENTTYPE_T905360158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/MovementType
struct  MovementType_t905360158 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementType_t905360158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T905360158_H
#ifndef SELECTIONCHANGESTARTEVENT_T1331424750_H
#define SELECTIONCHANGESTARTEVENT_T1331424750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeStartEvent
struct  SelectionChangeStartEvent_t1331424750  : public UnityEvent_t408735097
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGESTARTEVENT_T1331424750_H
#ifndef CORNER_T3586515314_H
#define CORNER_T3586515314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TableLayoutGroup/Corner
struct  Corner_t3586515314 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.TableLayoutGroup/Corner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Corner_t3586515314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T3586515314_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef SCROLLBARVISIBILITY_T3834843475_H
#define SCROLLBARVISIBILITY_T3834843475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct  ScrollbarVisibility_t3834843475 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t3834843475, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T3834843475_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef SCROLLDIRECTION_T1610447880_H
#define SCROLLDIRECTION_T1610447880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollPositionController/ScrollDirection
struct  ScrollDirection_t1610447880 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.ScrollPositionController/ScrollDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollDirection_t1610447880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLDIRECTION_T1610447880_H
#ifndef MOVEMENTTYPE_T3465211187_H
#define MOVEMENTTYPE_T3465211187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollPositionController/MovementType
struct  MovementType_t3465211187 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.ScrollPositionController/MovementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementType_t3465211187, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T3465211187_H
#ifndef UPDATEPOSITIONEVENT_T4030317844_H
#define UPDATEPOSITIONEVENT_T4030317844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollPositionController/UpdatePositionEvent
struct  UpdatePositionEvent_t4030317844  : public UnityEvent_1_t2114859947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEPOSITIONEVENT_T4030317844_H
#ifndef ITEMSELECTEDEVENT_T2604723240_H
#define ITEMSELECTEDEVENT_T2604723240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollPositionController/ItemSelectedEvent
struct  ItemSelectedEvent_t2604723240  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSELECTEDEVENT_T2604723240_H
#ifndef COLORBLOCK_T2652774230_H
#define COLORBLOCK_T2652774230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2652774230 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2020392075  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2020392075  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2020392075  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2020392075  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_NormalColor_0)); }
	inline Color_t2020392075  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2020392075 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2020392075  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_HighlightedColor_1)); }
	inline Color_t2020392075  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2020392075 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2020392075  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_PressedColor_2)); }
	inline Color_t2020392075  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2020392075 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2020392075  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_DisabledColor_3)); }
	inline Color_t2020392075  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2020392075 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2020392075  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2652774230_H
#ifndef SELECTIONSTATE_T3187567897_H
#define SELECTIONSTATE_T3187567897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t3187567897 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t3187567897, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T3187567897_H
#ifndef TRANSITION_T605142169_H
#define TRANSITION_T605142169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t605142169 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t605142169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T605142169_H
#ifndef SCROLLDIRECTION_T778749221_H
#define SCROLLDIRECTION_T778749221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnap/ScrollDirection
struct  ScrollDirection_t778749221 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap/ScrollDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollDirection_t778749221, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLDIRECTION_T778749221_H
#ifndef SEGMENTTYPE_T3639054610_H
#define SEGMENTTYPE_T3639054610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILineRenderer/SegmentType
struct  SegmentType_t3639054610 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UILineRenderer/SegmentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SegmentType_t3639054610, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTTYPE_T3639054610_H
#ifndef JOINTYPE_T3583171457_H
#define JOINTYPE_T3583171457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILineRenderer/JoinType
struct  JoinType_t3583171457 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UILineRenderer/JoinType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoinType_t3583171457, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTYPE_T3583171457_H
#ifndef MODE_T1081683921_H
#define MODE_T1081683921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1081683921 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1081683921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1081683921_H
#ifndef TEXTANCHOR_T112990806_H
#define TEXTANCHOR_T112990806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t112990806 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t112990806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T112990806_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef NAVIGATION_T1571958496_H
#define NAVIGATION_T1571958496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t1571958496 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1490392188 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnUp_1)); }
	inline Selectable_t1490392188 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1490392188 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnDown_2)); }
	inline Selectable_t1490392188 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1490392188 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnLeft_3)); }
	inline Selectable_t1490392188 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1490392188 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnRight_4)); }
	inline Selectable_t1490392188 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1490392188 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T1571958496_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef PAGESNAPCHANGE_T737912504_H
#define PAGESNAPCHANGE_T737912504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange
struct  PageSnapChange_t737912504  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGESNAPCHANGE_T737912504_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef RAYCASTMASK_T2005723581_H
#define RAYCASTMASK_T2005723581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RaycastMask
struct  RaycastMask_t2005723581  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.RaycastMask::_image
	Image_t2042527209 * ____image_2;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.RaycastMask::_sprite
	Sprite_t309593783 * ____sprite_3;

public:
	inline static int32_t get_offset_of__image_2() { return static_cast<int32_t>(offsetof(RaycastMask_t2005723581, ____image_2)); }
	inline Image_t2042527209 * get__image_2() const { return ____image_2; }
	inline Image_t2042527209 ** get_address_of__image_2() { return &____image_2; }
	inline void set__image_2(Image_t2042527209 * value)
	{
		____image_2 = value;
		Il2CppCodeGenWriteBarrier((&____image_2), value);
	}

	inline static int32_t get_offset_of__sprite_3() { return static_cast<int32_t>(offsetof(RaycastMask_t2005723581, ____sprite_3)); }
	inline Sprite_t309593783 * get__sprite_3() const { return ____sprite_3; }
	inline Sprite_t309593783 ** get_address_of__sprite_3() { return &____sprite_3; }
	inline void set__sprite_3(Sprite_t309593783 * value)
	{
		____sprite_3 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMASK_T2005723581_H
#ifndef UIADDITIVEEFFECT_T2331962695_H
#define UIADDITIVEEFFECT_T2331962695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIAdditiveEffect
struct  UIAdditiveEffect_t2331962695  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIAdditiveEffect::mGraphic
	MaskableGraphic_t540192618 * ___mGraphic_2;

public:
	inline static int32_t get_offset_of_mGraphic_2() { return static_cast<int32_t>(offsetof(UIAdditiveEffect_t2331962695, ___mGraphic_2)); }
	inline MaskableGraphic_t540192618 * get_mGraphic_2() const { return ___mGraphic_2; }
	inline MaskableGraphic_t540192618 ** get_address_of_mGraphic_2() { return &___mGraphic_2; }
	inline void set_mGraphic_2(MaskableGraphic_t540192618 * value)
	{
		___mGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADDITIVEEFFECT_T2331962695_H
#ifndef SCROLLRECTTWEENER_T3873690283_H
#define SCROLLRECTTWEENER_T3873690283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectTweener
struct  ScrollRectTweener_t3873690283  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollRectTweener::scrollRect
	ScrollRect_t1199013257 * ___scrollRect_2;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ScrollRectTweener::startPos
	Vector2_t2243707579  ___startPos_3;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ScrollRectTweener::targetPos
	Vector2_t2243707579  ___targetPos_4;
	// System.Boolean UnityEngine.UI.Extensions.ScrollRectTweener::wasHorizontal
	bool ___wasHorizontal_5;
	// System.Boolean UnityEngine.UI.Extensions.ScrollRectTweener::wasVertical
	bool ___wasVertical_6;
	// System.Single UnityEngine.UI.Extensions.ScrollRectTweener::moveSpeed
	float ___moveSpeed_7;
	// System.Boolean UnityEngine.UI.Extensions.ScrollRectTweener::disableDragWhileTweening
	bool ___disableDragWhileTweening_8;

public:
	inline static int32_t get_offset_of_scrollRect_2() { return static_cast<int32_t>(offsetof(ScrollRectTweener_t3873690283, ___scrollRect_2)); }
	inline ScrollRect_t1199013257 * get_scrollRect_2() const { return ___scrollRect_2; }
	inline ScrollRect_t1199013257 ** get_address_of_scrollRect_2() { return &___scrollRect_2; }
	inline void set_scrollRect_2(ScrollRect_t1199013257 * value)
	{
		___scrollRect_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_2), value);
	}

	inline static int32_t get_offset_of_startPos_3() { return static_cast<int32_t>(offsetof(ScrollRectTweener_t3873690283, ___startPos_3)); }
	inline Vector2_t2243707579  get_startPos_3() const { return ___startPos_3; }
	inline Vector2_t2243707579 * get_address_of_startPos_3() { return &___startPos_3; }
	inline void set_startPos_3(Vector2_t2243707579  value)
	{
		___startPos_3 = value;
	}

	inline static int32_t get_offset_of_targetPos_4() { return static_cast<int32_t>(offsetof(ScrollRectTweener_t3873690283, ___targetPos_4)); }
	inline Vector2_t2243707579  get_targetPos_4() const { return ___targetPos_4; }
	inline Vector2_t2243707579 * get_address_of_targetPos_4() { return &___targetPos_4; }
	inline void set_targetPos_4(Vector2_t2243707579  value)
	{
		___targetPos_4 = value;
	}

	inline static int32_t get_offset_of_wasHorizontal_5() { return static_cast<int32_t>(offsetof(ScrollRectTweener_t3873690283, ___wasHorizontal_5)); }
	inline bool get_wasHorizontal_5() const { return ___wasHorizontal_5; }
	inline bool* get_address_of_wasHorizontal_5() { return &___wasHorizontal_5; }
	inline void set_wasHorizontal_5(bool value)
	{
		___wasHorizontal_5 = value;
	}

	inline static int32_t get_offset_of_wasVertical_6() { return static_cast<int32_t>(offsetof(ScrollRectTweener_t3873690283, ___wasVertical_6)); }
	inline bool get_wasVertical_6() const { return ___wasVertical_6; }
	inline bool* get_address_of_wasVertical_6() { return &___wasVertical_6; }
	inline void set_wasVertical_6(bool value)
	{
		___wasVertical_6 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_7() { return static_cast<int32_t>(offsetof(ScrollRectTweener_t3873690283, ___moveSpeed_7)); }
	inline float get_moveSpeed_7() const { return ___moveSpeed_7; }
	inline float* get_address_of_moveSpeed_7() { return &___moveSpeed_7; }
	inline void set_moveSpeed_7(float value)
	{
		___moveSpeed_7 = value;
	}

	inline static int32_t get_offset_of_disableDragWhileTweening_8() { return static_cast<int32_t>(offsetof(ScrollRectTweener_t3873690283, ___disableDragWhileTweening_8)); }
	inline bool get_disableDragWhileTweening_8() const { return ___disableDragWhileTweening_8; }
	inline bool* get_address_of_disableDragWhileTweening_8() { return &___disableDragWhileTweening_8; }
	inline void set_disableDragWhileTweening_8(bool value)
	{
		___disableDragWhileTweening_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECTTWEENER_T3873690283_H
#ifndef SELECTABLESCALER_T1409550150_H
#define SELECTABLESCALER_T1409550150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SelectableScaler
struct  SelectableScaler_t1409550150  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationCurve UnityEngine.UI.Extensions.SelectableScaler::animCurve
	AnimationCurve_t3306541151 * ___animCurve_2;
	// System.Single UnityEngine.UI.Extensions.SelectableScaler::speed
	float ___speed_3;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.SelectableScaler::initScale
	Vector3_t2243707580  ___initScale_4;
	// UnityEngine.Transform UnityEngine.UI.Extensions.SelectableScaler::target
	Transform_t3275118058 * ___target_5;
	// UnityEngine.UI.Selectable UnityEngine.UI.Extensions.SelectableScaler::selectable
	Selectable_t1490392188 * ___selectable_6;

public:
	inline static int32_t get_offset_of_animCurve_2() { return static_cast<int32_t>(offsetof(SelectableScaler_t1409550150, ___animCurve_2)); }
	inline AnimationCurve_t3306541151 * get_animCurve_2() const { return ___animCurve_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_animCurve_2() { return &___animCurve_2; }
	inline void set_animCurve_2(AnimationCurve_t3306541151 * value)
	{
		___animCurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___animCurve_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(SelectableScaler_t1409550150, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_initScale_4() { return static_cast<int32_t>(offsetof(SelectableScaler_t1409550150, ___initScale_4)); }
	inline Vector3_t2243707580  get_initScale_4() const { return ___initScale_4; }
	inline Vector3_t2243707580 * get_address_of_initScale_4() { return &___initScale_4; }
	inline void set_initScale_4(Vector3_t2243707580  value)
	{
		___initScale_4 = value;
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(SelectableScaler_t1409550150, ___target_5)); }
	inline Transform_t3275118058 * get_target_5() const { return ___target_5; }
	inline Transform_t3275118058 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Transform_t3275118058 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_selectable_6() { return static_cast<int32_t>(offsetof(SelectableScaler_t1409550150, ___selectable_6)); }
	inline Selectable_t1490392188 * get_selectable_6() const { return ___selectable_6; }
	inline Selectable_t1490392188 ** get_address_of_selectable_6() { return &___selectable_6; }
	inline void set_selectable_6(Selectable_t1490392188 * value)
	{
		___selectable_6 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLESCALER_T1409550150_H
#ifndef UISOFTADDITIVEEFFECT_T1806829255_H
#define UISOFTADDITIVEEFFECT_T1806829255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UISoftAdditiveEffect
struct  UISoftAdditiveEffect_t1806829255  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UISoftAdditiveEffect::mGraphic
	MaskableGraphic_t540192618 * ___mGraphic_2;

public:
	inline static int32_t get_offset_of_mGraphic_2() { return static_cast<int32_t>(offsetof(UISoftAdditiveEffect_t1806829255, ___mGraphic_2)); }
	inline MaskableGraphic_t540192618 * get_mGraphic_2() const { return ___mGraphic_2; }
	inline MaskableGraphic_t540192618 ** get_address_of_mGraphic_2() { return &___mGraphic_2; }
	inline void set_mGraphic_2(MaskableGraphic_t540192618 * value)
	{
		___mGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISOFTADDITIVEEFFECT_T1806829255_H
#ifndef UISCREENEFFECT_T688938299_H
#define UISCREENEFFECT_T688938299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIScreenEffect
struct  UIScreenEffect_t688938299  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIScreenEffect::mGraphic
	MaskableGraphic_t540192618 * ___mGraphic_2;

public:
	inline static int32_t get_offset_of_mGraphic_2() { return static_cast<int32_t>(offsetof(UIScreenEffect_t688938299, ___mGraphic_2)); }
	inline MaskableGraphic_t540192618 * get_mGraphic_2() const { return ___mGraphic_2; }
	inline MaskableGraphic_t540192618 ** get_address_of_mGraphic_2() { return &___mGraphic_2; }
	inline void set_mGraphic_2(MaskableGraphic_t540192618 * value)
	{
		___mGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCREENEFFECT_T688938299_H
#ifndef SOFTMASKSCRIPT_T947627439_H
#define SOFTMASKSCRIPT_T947627439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SoftMaskScript
struct  SoftMaskScript_t947627439  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material UnityEngine.UI.Extensions.SoftMaskScript::mat
	Material_t193706927 * ___mat_2;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.SoftMaskScript::cachedCanvas
	Canvas_t209405766 * ___cachedCanvas_3;
	// UnityEngine.Transform UnityEngine.UI.Extensions.SoftMaskScript::cachedCanvasTransform
	Transform_t3275118058 * ___cachedCanvasTransform_4;
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.SoftMaskScript::m_WorldCorners
	Vector3U5BU5D_t1172311765* ___m_WorldCorners_5;
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.SoftMaskScript::m_CanvasCorners
	Vector3U5BU5D_t1172311765* ___m_CanvasCorners_6;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.SoftMaskScript::MaskArea
	RectTransform_t3349966182 * ___MaskArea_7;
	// UnityEngine.Texture UnityEngine.UI.Extensions.SoftMaskScript::AlphaMask
	Texture_t2243626319 * ___AlphaMask_8;
	// System.Single UnityEngine.UI.Extensions.SoftMaskScript::CutOff
	float ___CutOff_9;
	// System.Boolean UnityEngine.UI.Extensions.SoftMaskScript::HardBlend
	bool ___HardBlend_10;
	// System.Boolean UnityEngine.UI.Extensions.SoftMaskScript::FlipAlphaMask
	bool ___FlipAlphaMask_11;
	// System.Boolean UnityEngine.UI.Extensions.SoftMaskScript::DontClipMaskScalingRect
	bool ___DontClipMaskScalingRect_12;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.SoftMaskScript::maskOffset
	Vector2_t2243707579  ___maskOffset_13;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.SoftMaskScript::maskScale
	Vector2_t2243707579  ___maskScale_14;

public:
	inline static int32_t get_offset_of_mat_2() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___mat_2)); }
	inline Material_t193706927 * get_mat_2() const { return ___mat_2; }
	inline Material_t193706927 ** get_address_of_mat_2() { return &___mat_2; }
	inline void set_mat_2(Material_t193706927 * value)
	{
		___mat_2 = value;
		Il2CppCodeGenWriteBarrier((&___mat_2), value);
	}

	inline static int32_t get_offset_of_cachedCanvas_3() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___cachedCanvas_3)); }
	inline Canvas_t209405766 * get_cachedCanvas_3() const { return ___cachedCanvas_3; }
	inline Canvas_t209405766 ** get_address_of_cachedCanvas_3() { return &___cachedCanvas_3; }
	inline void set_cachedCanvas_3(Canvas_t209405766 * value)
	{
		___cachedCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCanvas_3), value);
	}

	inline static int32_t get_offset_of_cachedCanvasTransform_4() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___cachedCanvasTransform_4)); }
	inline Transform_t3275118058 * get_cachedCanvasTransform_4() const { return ___cachedCanvasTransform_4; }
	inline Transform_t3275118058 ** get_address_of_cachedCanvasTransform_4() { return &___cachedCanvasTransform_4; }
	inline void set_cachedCanvasTransform_4(Transform_t3275118058 * value)
	{
		___cachedCanvasTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCanvasTransform_4), value);
	}

	inline static int32_t get_offset_of_m_WorldCorners_5() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___m_WorldCorners_5)); }
	inline Vector3U5BU5D_t1172311765* get_m_WorldCorners_5() const { return ___m_WorldCorners_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_WorldCorners_5() { return &___m_WorldCorners_5; }
	inline void set_m_WorldCorners_5(Vector3U5BU5D_t1172311765* value)
	{
		___m_WorldCorners_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_5), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_6() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___m_CanvasCorners_6)); }
	inline Vector3U5BU5D_t1172311765* get_m_CanvasCorners_6() const { return ___m_CanvasCorners_6; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_CanvasCorners_6() { return &___m_CanvasCorners_6; }
	inline void set_m_CanvasCorners_6(Vector3U5BU5D_t1172311765* value)
	{
		___m_CanvasCorners_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_6), value);
	}

	inline static int32_t get_offset_of_MaskArea_7() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___MaskArea_7)); }
	inline RectTransform_t3349966182 * get_MaskArea_7() const { return ___MaskArea_7; }
	inline RectTransform_t3349966182 ** get_address_of_MaskArea_7() { return &___MaskArea_7; }
	inline void set_MaskArea_7(RectTransform_t3349966182 * value)
	{
		___MaskArea_7 = value;
		Il2CppCodeGenWriteBarrier((&___MaskArea_7), value);
	}

	inline static int32_t get_offset_of_AlphaMask_8() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___AlphaMask_8)); }
	inline Texture_t2243626319 * get_AlphaMask_8() const { return ___AlphaMask_8; }
	inline Texture_t2243626319 ** get_address_of_AlphaMask_8() { return &___AlphaMask_8; }
	inline void set_AlphaMask_8(Texture_t2243626319 * value)
	{
		___AlphaMask_8 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaMask_8), value);
	}

	inline static int32_t get_offset_of_CutOff_9() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___CutOff_9)); }
	inline float get_CutOff_9() const { return ___CutOff_9; }
	inline float* get_address_of_CutOff_9() { return &___CutOff_9; }
	inline void set_CutOff_9(float value)
	{
		___CutOff_9 = value;
	}

	inline static int32_t get_offset_of_HardBlend_10() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___HardBlend_10)); }
	inline bool get_HardBlend_10() const { return ___HardBlend_10; }
	inline bool* get_address_of_HardBlend_10() { return &___HardBlend_10; }
	inline void set_HardBlend_10(bool value)
	{
		___HardBlend_10 = value;
	}

	inline static int32_t get_offset_of_FlipAlphaMask_11() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___FlipAlphaMask_11)); }
	inline bool get_FlipAlphaMask_11() const { return ___FlipAlphaMask_11; }
	inline bool* get_address_of_FlipAlphaMask_11() { return &___FlipAlphaMask_11; }
	inline void set_FlipAlphaMask_11(bool value)
	{
		___FlipAlphaMask_11 = value;
	}

	inline static int32_t get_offset_of_DontClipMaskScalingRect_12() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___DontClipMaskScalingRect_12)); }
	inline bool get_DontClipMaskScalingRect_12() const { return ___DontClipMaskScalingRect_12; }
	inline bool* get_address_of_DontClipMaskScalingRect_12() { return &___DontClipMaskScalingRect_12; }
	inline void set_DontClipMaskScalingRect_12(bool value)
	{
		___DontClipMaskScalingRect_12 = value;
	}

	inline static int32_t get_offset_of_maskOffset_13() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___maskOffset_13)); }
	inline Vector2_t2243707579  get_maskOffset_13() const { return ___maskOffset_13; }
	inline Vector2_t2243707579 * get_address_of_maskOffset_13() { return &___maskOffset_13; }
	inline void set_maskOffset_13(Vector2_t2243707579  value)
	{
		___maskOffset_13 = value;
	}

	inline static int32_t get_offset_of_maskScale_14() { return static_cast<int32_t>(offsetof(SoftMaskScript_t947627439, ___maskScale_14)); }
	inline Vector2_t2243707579  get_maskScale_14() const { return ___maskScale_14; }
	inline Vector2_t2243707579 * get_address_of_maskScale_14() { return &___maskScale_14; }
	inline void set_maskScale_14(Vector2_t2243707579  value)
	{
		___maskScale_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOFTMASKSCRIPT_T947627439_H
#ifndef SHINEEFFECTOR_T4197735589_H
#define SHINEEFFECTOR_T4197735589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ShineEffector
struct  ShineEffector_t4197735589  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.ShineEffect UnityEngine.UI.Extensions.ShineEffector::effector
	ShineEffect_t1749864756 * ___effector_2;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ShineEffector::effectRoot
	GameObject_t1756533147 * ___effectRoot_3;
	// System.Single UnityEngine.UI.Extensions.ShineEffector::yOffset
	float ___yOffset_4;
	// System.Single UnityEngine.UI.Extensions.ShineEffector::width
	float ___width_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ShineEffector::effectorRect
	RectTransform_t3349966182 * ___effectorRect_6;

public:
	inline static int32_t get_offset_of_effector_2() { return static_cast<int32_t>(offsetof(ShineEffector_t4197735589, ___effector_2)); }
	inline ShineEffect_t1749864756 * get_effector_2() const { return ___effector_2; }
	inline ShineEffect_t1749864756 ** get_address_of_effector_2() { return &___effector_2; }
	inline void set_effector_2(ShineEffect_t1749864756 * value)
	{
		___effector_2 = value;
		Il2CppCodeGenWriteBarrier((&___effector_2), value);
	}

	inline static int32_t get_offset_of_effectRoot_3() { return static_cast<int32_t>(offsetof(ShineEffector_t4197735589, ___effectRoot_3)); }
	inline GameObject_t1756533147 * get_effectRoot_3() const { return ___effectRoot_3; }
	inline GameObject_t1756533147 ** get_address_of_effectRoot_3() { return &___effectRoot_3; }
	inline void set_effectRoot_3(GameObject_t1756533147 * value)
	{
		___effectRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___effectRoot_3), value);
	}

	inline static int32_t get_offset_of_yOffset_4() { return static_cast<int32_t>(offsetof(ShineEffector_t4197735589, ___yOffset_4)); }
	inline float get_yOffset_4() const { return ___yOffset_4; }
	inline float* get_address_of_yOffset_4() { return &___yOffset_4; }
	inline void set_yOffset_4(float value)
	{
		___yOffset_4 = value;
	}

	inline static int32_t get_offset_of_width_5() { return static_cast<int32_t>(offsetof(ShineEffector_t4197735589, ___width_5)); }
	inline float get_width_5() const { return ___width_5; }
	inline float* get_address_of_width_5() { return &___width_5; }
	inline void set_width_5(float value)
	{
		___width_5 = value;
	}

	inline static int32_t get_offset_of_effectorRect_6() { return static_cast<int32_t>(offsetof(ShineEffector_t4197735589, ___effectorRect_6)); }
	inline RectTransform_t3349966182 * get_effectorRect_6() const { return ___effectorRect_6; }
	inline RectTransform_t3349966182 ** get_address_of_effectorRect_6() { return &___effectorRect_6; }
	inline void set_effectorRect_6(RectTransform_t3349966182 * value)
	{
		___effectorRect_6 = value;
		Il2CppCodeGenWriteBarrier((&___effectorRect_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHINEEFFECTOR_T4197735589_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef UIIMAGECROP_T3014564719_H
#define UIIMAGECROP_T3014564719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIImageCrop
struct  UIImageCrop_t3014564719  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIImageCrop::mGraphic
	MaskableGraphic_t540192618 * ___mGraphic_2;
	// UnityEngine.Material UnityEngine.UI.Extensions.UIImageCrop::mat
	Material_t193706927 * ___mat_3;
	// System.Int32 UnityEngine.UI.Extensions.UIImageCrop::XCropProperty
	int32_t ___XCropProperty_4;
	// System.Int32 UnityEngine.UI.Extensions.UIImageCrop::YCropProperty
	int32_t ___YCropProperty_5;
	// System.Single UnityEngine.UI.Extensions.UIImageCrop::XCrop
	float ___XCrop_6;
	// System.Single UnityEngine.UI.Extensions.UIImageCrop::YCrop
	float ___YCrop_7;

public:
	inline static int32_t get_offset_of_mGraphic_2() { return static_cast<int32_t>(offsetof(UIImageCrop_t3014564719, ___mGraphic_2)); }
	inline MaskableGraphic_t540192618 * get_mGraphic_2() const { return ___mGraphic_2; }
	inline MaskableGraphic_t540192618 ** get_address_of_mGraphic_2() { return &___mGraphic_2; }
	inline void set_mGraphic_2(MaskableGraphic_t540192618 * value)
	{
		___mGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_2), value);
	}

	inline static int32_t get_offset_of_mat_3() { return static_cast<int32_t>(offsetof(UIImageCrop_t3014564719, ___mat_3)); }
	inline Material_t193706927 * get_mat_3() const { return ___mat_3; }
	inline Material_t193706927 ** get_address_of_mat_3() { return &___mat_3; }
	inline void set_mat_3(Material_t193706927 * value)
	{
		___mat_3 = value;
		Il2CppCodeGenWriteBarrier((&___mat_3), value);
	}

	inline static int32_t get_offset_of_XCropProperty_4() { return static_cast<int32_t>(offsetof(UIImageCrop_t3014564719, ___XCropProperty_4)); }
	inline int32_t get_XCropProperty_4() const { return ___XCropProperty_4; }
	inline int32_t* get_address_of_XCropProperty_4() { return &___XCropProperty_4; }
	inline void set_XCropProperty_4(int32_t value)
	{
		___XCropProperty_4 = value;
	}

	inline static int32_t get_offset_of_YCropProperty_5() { return static_cast<int32_t>(offsetof(UIImageCrop_t3014564719, ___YCropProperty_5)); }
	inline int32_t get_YCropProperty_5() const { return ___YCropProperty_5; }
	inline int32_t* get_address_of_YCropProperty_5() { return &___YCropProperty_5; }
	inline void set_YCropProperty_5(int32_t value)
	{
		___YCropProperty_5 = value;
	}

	inline static int32_t get_offset_of_XCrop_6() { return static_cast<int32_t>(offsetof(UIImageCrop_t3014564719, ___XCrop_6)); }
	inline float get_XCrop_6() const { return ___XCrop_6; }
	inline float* get_address_of_XCrop_6() { return &___XCrop_6; }
	inline void set_XCrop_6(float value)
	{
		___XCrop_6 = value;
	}

	inline static int32_t get_offset_of_YCrop_7() { return static_cast<int32_t>(offsetof(UIImageCrop_t3014564719, ___YCrop_7)); }
	inline float get_YCrop_7() const { return ___YCrop_7; }
	inline float* get_address_of_YCrop_7() { return &___YCrop_7; }
	inline void set_YCrop_7(float value)
	{
		___YCrop_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGECROP_T3014564719_H
#ifndef UIMULTIPLYEFFECT_T1139553371_H
#define UIMULTIPLYEFFECT_T1139553371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIMultiplyEffect
struct  UIMultiplyEffect_t1139553371  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIMultiplyEffect::mGraphic
	MaskableGraphic_t540192618 * ___mGraphic_2;

public:
	inline static int32_t get_offset_of_mGraphic_2() { return static_cast<int32_t>(offsetof(UIMultiplyEffect_t1139553371, ___mGraphic_2)); }
	inline MaskableGraphic_t540192618 * get_mGraphic_2() const { return ___mGraphic_2; }
	inline MaskableGraphic_t540192618 ** get_address_of_mGraphic_2() { return &___mGraphic_2; }
	inline void set_mGraphic_2(MaskableGraphic_t540192618 * value)
	{
		___mGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMULTIPLYEFFECT_T1139553371_H
#ifndef UILINEARDODGEEFFECT_T1469920893_H
#define UILINEARDODGEEFFECT_T1469920893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILinearDodgeEffect
struct  UILinearDodgeEffect_t1469920893  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UILinearDodgeEffect::mGraphic
	MaskableGraphic_t540192618 * ___mGraphic_2;

public:
	inline static int32_t get_offset_of_mGraphic_2() { return static_cast<int32_t>(offsetof(UILinearDodgeEffect_t1469920893, ___mGraphic_2)); }
	inline MaskableGraphic_t540192618 * get_mGraphic_2() const { return ___mGraphic_2; }
	inline MaskableGraphic_t540192618 ** get_address_of_mGraphic_2() { return &___mGraphic_2; }
	inline void set_mGraphic_2(MaskableGraphic_t540192618 * value)
	{
		___mGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEARDODGEEFFECT_T1469920893_H
#ifndef MENUMANAGER_T3446170766_H
#define MENUMANAGER_T3446170766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.MenuManager
struct  MenuManager_t3446170766  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.Menu[] UnityEngine.UI.Extensions.MenuManager::MenuScreens
	MenuU5BU5D_t2855465670* ___MenuScreens_2;
	// System.Int32 UnityEngine.UI.Extensions.MenuManager::StartScreen
	int32_t ___StartScreen_3;
	// System.Collections.Generic.Stack`1<UnityEngine.UI.Extensions.Menu> UnityEngine.UI.Extensions.MenuManager::menuStack
	Stack_1_t2297400569 * ___menuStack_4;

public:
	inline static int32_t get_offset_of_MenuScreens_2() { return static_cast<int32_t>(offsetof(MenuManager_t3446170766, ___MenuScreens_2)); }
	inline MenuU5BU5D_t2855465670* get_MenuScreens_2() const { return ___MenuScreens_2; }
	inline MenuU5BU5D_t2855465670** get_address_of_MenuScreens_2() { return &___MenuScreens_2; }
	inline void set_MenuScreens_2(MenuU5BU5D_t2855465670* value)
	{
		___MenuScreens_2 = value;
		Il2CppCodeGenWriteBarrier((&___MenuScreens_2), value);
	}

	inline static int32_t get_offset_of_StartScreen_3() { return static_cast<int32_t>(offsetof(MenuManager_t3446170766, ___StartScreen_3)); }
	inline int32_t get_StartScreen_3() const { return ___StartScreen_3; }
	inline int32_t* get_address_of_StartScreen_3() { return &___StartScreen_3; }
	inline void set_StartScreen_3(int32_t value)
	{
		___StartScreen_3 = value;
	}

	inline static int32_t get_offset_of_menuStack_4() { return static_cast<int32_t>(offsetof(MenuManager_t3446170766, ___menuStack_4)); }
	inline Stack_1_t2297400569 * get_menuStack_4() const { return ___menuStack_4; }
	inline Stack_1_t2297400569 ** get_address_of_menuStack_4() { return &___menuStack_4; }
	inline void set_menuStack_4(Stack_1_t2297400569 * value)
	{
		___menuStack_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuStack_4), value);
	}
};

struct MenuManager_t3446170766_StaticFields
{
public:
	// UnityEngine.UI.Extensions.MenuManager UnityEngine.UI.Extensions.MenuManager::<Instance>k__BackingField
	MenuManager_t3446170766 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MenuManager_t3446170766_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline MenuManager_t3446170766 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline MenuManager_t3446170766 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(MenuManager_t3446170766 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUMANAGER_T3446170766_H
#ifndef UIWINDOWBASE_T1223857951_H
#define UIWINDOWBASE_T1223857951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIWindowBase
struct  UIWindowBase_t1223857951  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIWindowBase::m_transform
	RectTransform_t3349966182 * ___m_transform_2;
	// System.Boolean UnityEngine.UI.Extensions.UIWindowBase::_isDragging
	bool ____isDragging_3;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.UIWindowBase::m_originalCoods
	Vector3_t2243707580  ___m_originalCoods_5;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.UIWindowBase::m_canvas
	Canvas_t209405766 * ___m_canvas_6;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIWindowBase::m_canvasRectTransform
	RectTransform_t3349966182 * ___m_canvasRectTransform_7;
	// System.Int32 UnityEngine.UI.Extensions.UIWindowBase::KeepWindowInCanvas
	int32_t ___KeepWindowInCanvas_8;

public:
	inline static int32_t get_offset_of_m_transform_2() { return static_cast<int32_t>(offsetof(UIWindowBase_t1223857951, ___m_transform_2)); }
	inline RectTransform_t3349966182 * get_m_transform_2() const { return ___m_transform_2; }
	inline RectTransform_t3349966182 ** get_address_of_m_transform_2() { return &___m_transform_2; }
	inline void set_m_transform_2(RectTransform_t3349966182 * value)
	{
		___m_transform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_2), value);
	}

	inline static int32_t get_offset_of__isDragging_3() { return static_cast<int32_t>(offsetof(UIWindowBase_t1223857951, ____isDragging_3)); }
	inline bool get__isDragging_3() const { return ____isDragging_3; }
	inline bool* get_address_of__isDragging_3() { return &____isDragging_3; }
	inline void set__isDragging_3(bool value)
	{
		____isDragging_3 = value;
	}

	inline static int32_t get_offset_of_m_originalCoods_5() { return static_cast<int32_t>(offsetof(UIWindowBase_t1223857951, ___m_originalCoods_5)); }
	inline Vector3_t2243707580  get_m_originalCoods_5() const { return ___m_originalCoods_5; }
	inline Vector3_t2243707580 * get_address_of_m_originalCoods_5() { return &___m_originalCoods_5; }
	inline void set_m_originalCoods_5(Vector3_t2243707580  value)
	{
		___m_originalCoods_5 = value;
	}

	inline static int32_t get_offset_of_m_canvas_6() { return static_cast<int32_t>(offsetof(UIWindowBase_t1223857951, ___m_canvas_6)); }
	inline Canvas_t209405766 * get_m_canvas_6() const { return ___m_canvas_6; }
	inline Canvas_t209405766 ** get_address_of_m_canvas_6() { return &___m_canvas_6; }
	inline void set_m_canvas_6(Canvas_t209405766 * value)
	{
		___m_canvas_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_6), value);
	}

	inline static int32_t get_offset_of_m_canvasRectTransform_7() { return static_cast<int32_t>(offsetof(UIWindowBase_t1223857951, ___m_canvasRectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_canvasRectTransform_7() const { return ___m_canvasRectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_canvasRectTransform_7() { return &___m_canvasRectTransform_7; }
	inline void set_m_canvasRectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_canvasRectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRectTransform_7), value);
	}

	inline static int32_t get_offset_of_KeepWindowInCanvas_8() { return static_cast<int32_t>(offsetof(UIWindowBase_t1223857951, ___KeepWindowInCanvas_8)); }
	inline int32_t get_KeepWindowInCanvas_8() const { return ___KeepWindowInCanvas_8; }
	inline int32_t* get_address_of_KeepWindowInCanvas_8() { return &___KeepWindowInCanvas_8; }
	inline void set_KeepWindowInCanvas_8(int32_t value)
	{
		___KeepWindowInCanvas_8 = value;
	}
};

struct UIWindowBase_t1223857951_StaticFields
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UIWindowBase::ResetCoords
	bool ___ResetCoords_4;

public:
	inline static int32_t get_offset_of_ResetCoords_4() { return static_cast<int32_t>(offsetof(UIWindowBase_t1223857951_StaticFields, ___ResetCoords_4)); }
	inline bool get_ResetCoords_4() const { return ___ResetCoords_4; }
	inline bool* get_address_of_ResetCoords_4() { return &___ResetCoords_4; }
	inline void set_ResetCoords_4(bool value)
	{
		___ResetCoords_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWINDOWBASE_T1223857951_H
#ifndef DRAGCORRECTOR_T1392823991_H
#define DRAGCORRECTOR_T1392823991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DragCorrector
struct  DragCorrector_t1392823991  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 UnityEngine.UI.Extensions.DragCorrector::baseTH
	int32_t ___baseTH_2;
	// System.Int32 UnityEngine.UI.Extensions.DragCorrector::basePPI
	int32_t ___basePPI_3;
	// System.Int32 UnityEngine.UI.Extensions.DragCorrector::dragTH
	int32_t ___dragTH_4;

public:
	inline static int32_t get_offset_of_baseTH_2() { return static_cast<int32_t>(offsetof(DragCorrector_t1392823991, ___baseTH_2)); }
	inline int32_t get_baseTH_2() const { return ___baseTH_2; }
	inline int32_t* get_address_of_baseTH_2() { return &___baseTH_2; }
	inline void set_baseTH_2(int32_t value)
	{
		___baseTH_2 = value;
	}

	inline static int32_t get_offset_of_basePPI_3() { return static_cast<int32_t>(offsetof(DragCorrector_t1392823991, ___basePPI_3)); }
	inline int32_t get_basePPI_3() const { return ___basePPI_3; }
	inline int32_t* get_address_of_basePPI_3() { return &___basePPI_3; }
	inline void set_basePPI_3(int32_t value)
	{
		___basePPI_3 = value;
	}

	inline static int32_t get_offset_of_dragTH_4() { return static_cast<int32_t>(offsetof(DragCorrector_t1392823991, ___dragTH_4)); }
	inline int32_t get_dragTH_4() const { return ___dragTH_4; }
	inline int32_t* get_address_of_dragTH_4() { return &___dragTH_4; }
	inline void set_dragTH_4(int32_t value)
	{
		___dragTH_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGCORRECTOR_T1392823991_H
#ifndef MENU_T1209672415_H
#define MENU_T1209672415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu
struct  Menu_t1209672415  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.Menu::DestroyWhenClosed
	bool ___DestroyWhenClosed_2;
	// System.Boolean UnityEngine.UI.Extensions.Menu::DisableMenusUnderneath
	bool ___DisableMenusUnderneath_3;

public:
	inline static int32_t get_offset_of_DestroyWhenClosed_2() { return static_cast<int32_t>(offsetof(Menu_t1209672415, ___DestroyWhenClosed_2)); }
	inline bool get_DestroyWhenClosed_2() const { return ___DestroyWhenClosed_2; }
	inline bool* get_address_of_DestroyWhenClosed_2() { return &___DestroyWhenClosed_2; }
	inline void set_DestroyWhenClosed_2(bool value)
	{
		___DestroyWhenClosed_2 = value;
	}

	inline static int32_t get_offset_of_DisableMenusUnderneath_3() { return static_cast<int32_t>(offsetof(Menu_t1209672415, ___DisableMenusUnderneath_3)); }
	inline bool get_DisableMenusUnderneath_3() const { return ___DisableMenusUnderneath_3; }
	inline bool* get_address_of_DisableMenusUnderneath_3() { return &___DisableMenusUnderneath_3; }
	inline void set_DisableMenusUnderneath_3(bool value)
	{
		___DisableMenusUnderneath_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_T1209672415_H
#ifndef TOOLTIP_T1538012217_H
#define TOOLTIP_T1538012217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ToolTip
struct  ToolTip_t1538012217  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ToolTip::_text
	Text_t356221433 * ____text_2;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ToolTip::_rectTransform
	RectTransform_t3349966182 * ____rectTransform_3;
	// System.Boolean UnityEngine.UI.Extensions.ToolTip::_inside
	bool ____inside_4;
	// System.Single UnityEngine.UI.Extensions.ToolTip::width
	float ___width_5;
	// System.Single UnityEngine.UI.Extensions.ToolTip::height
	float ___height_6;
	// System.Single UnityEngine.UI.Extensions.ToolTip::YShift
	float ___YShift_7;
	// System.Single UnityEngine.UI.Extensions.ToolTip::xShift
	float ___xShift_8;
	// UnityEngine.RenderMode UnityEngine.UI.Extensions.ToolTip::_guiMode
	int32_t ____guiMode_9;
	// UnityEngine.Camera UnityEngine.UI.Extensions.ToolTip::_guiCamera
	Camera_t189460977 * ____guiCamera_10;

public:
	inline static int32_t get_offset_of__text_2() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ____text_2)); }
	inline Text_t356221433 * get__text_2() const { return ____text_2; }
	inline Text_t356221433 ** get_address_of__text_2() { return &____text_2; }
	inline void set__text_2(Text_t356221433 * value)
	{
		____text_2 = value;
		Il2CppCodeGenWriteBarrier((&____text_2), value);
	}

	inline static int32_t get_offset_of__rectTransform_3() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ____rectTransform_3)); }
	inline RectTransform_t3349966182 * get__rectTransform_3() const { return ____rectTransform_3; }
	inline RectTransform_t3349966182 ** get_address_of__rectTransform_3() { return &____rectTransform_3; }
	inline void set__rectTransform_3(RectTransform_t3349966182 * value)
	{
		____rectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_3), value);
	}

	inline static int32_t get_offset_of__inside_4() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ____inside_4)); }
	inline bool get__inside_4() const { return ____inside_4; }
	inline bool* get_address_of__inside_4() { return &____inside_4; }
	inline void set__inside_4(bool value)
	{
		____inside_4 = value;
	}

	inline static int32_t get_offset_of_width_5() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ___width_5)); }
	inline float get_width_5() const { return ___width_5; }
	inline float* get_address_of_width_5() { return &___width_5; }
	inline void set_width_5(float value)
	{
		___width_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_YShift_7() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ___YShift_7)); }
	inline float get_YShift_7() const { return ___YShift_7; }
	inline float* get_address_of_YShift_7() { return &___YShift_7; }
	inline void set_YShift_7(float value)
	{
		___YShift_7 = value;
	}

	inline static int32_t get_offset_of_xShift_8() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ___xShift_8)); }
	inline float get_xShift_8() const { return ___xShift_8; }
	inline float* get_address_of_xShift_8() { return &___xShift_8; }
	inline void set_xShift_8(float value)
	{
		___xShift_8 = value;
	}

	inline static int32_t get_offset_of__guiMode_9() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ____guiMode_9)); }
	inline int32_t get__guiMode_9() const { return ____guiMode_9; }
	inline int32_t* get_address_of__guiMode_9() { return &____guiMode_9; }
	inline void set__guiMode_9(int32_t value)
	{
		____guiMode_9 = value;
	}

	inline static int32_t get_offset_of__guiCamera_10() { return static_cast<int32_t>(offsetof(ToolTip_t1538012217, ____guiCamera_10)); }
	inline Camera_t189460977 * get__guiCamera_10() const { return ____guiCamera_10; }
	inline Camera_t189460977 ** get_address_of__guiCamera_10() { return &____guiCamera_10; }
	inline void set__guiCamera_10(Camera_t189460977 * value)
	{
		____guiCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&____guiCamera_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIP_T1538012217_H
#ifndef BOUNDTOOLTIPITEM_T3449975380_H
#define BOUNDTOOLTIPITEM_T3449975380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoundTooltipItem
struct  BoundTooltipItem_t3449975380  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.BoundTooltipItem::TooltipText
	Text_t356221433 * ___TooltipText_2;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.BoundTooltipItem::ToolTipOffset
	Vector3_t2243707580  ___ToolTipOffset_3;

public:
	inline static int32_t get_offset_of_TooltipText_2() { return static_cast<int32_t>(offsetof(BoundTooltipItem_t3449975380, ___TooltipText_2)); }
	inline Text_t356221433 * get_TooltipText_2() const { return ___TooltipText_2; }
	inline Text_t356221433 ** get_address_of_TooltipText_2() { return &___TooltipText_2; }
	inline void set_TooltipText_2(Text_t356221433 * value)
	{
		___TooltipText_2 = value;
		Il2CppCodeGenWriteBarrier((&___TooltipText_2), value);
	}

	inline static int32_t get_offset_of_ToolTipOffset_3() { return static_cast<int32_t>(offsetof(BoundTooltipItem_t3449975380, ___ToolTipOffset_3)); }
	inline Vector3_t2243707580  get_ToolTipOffset_3() const { return ___ToolTipOffset_3; }
	inline Vector3_t2243707580 * get_address_of_ToolTipOffset_3() { return &___ToolTipOffset_3; }
	inline void set_ToolTipOffset_3(Vector3_t2243707580  value)
	{
		___ToolTipOffset_3 = value;
	}
};

struct BoundTooltipItem_t3449975380_StaticFields
{
public:
	// UnityEngine.UI.Extensions.BoundTooltipItem UnityEngine.UI.Extensions.BoundTooltipItem::instance
	BoundTooltipItem_t3449975380 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(BoundTooltipItem_t3449975380_StaticFields, ___instance_4)); }
	inline BoundTooltipItem_t3449975380 * get_instance_4() const { return ___instance_4; }
	inline BoundTooltipItem_t3449975380 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(BoundTooltipItem_t3449975380 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDTOOLTIPITEM_T3449975380_H
#ifndef TABNAVIGATIONHELPER_T538601999_H
#define TABNAVIGATIONHELPER_T538601999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TabNavigationHelper
struct  TabNavigationHelper_t538601999  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.UI.Extensions.TabNavigationHelper::_system
	EventSystem_t3466835263 * ____system_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Extensions.TabNavigationHelper::StartingObject
	Selectable_t1490392188 * ___StartingObject_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Extensions.TabNavigationHelper::LastObject
	Selectable_t1490392188 * ___LastObject_4;
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Extensions.TabNavigationHelper::NavigationPath
	SelectableU5BU5D_t3083107861* ___NavigationPath_5;
	// UnityEngine.UI.Extensions.NavigationMode UnityEngine.UI.Extensions.TabNavigationHelper::NavigationMode
	int32_t ___NavigationMode_6;
	// System.Boolean UnityEngine.UI.Extensions.TabNavigationHelper::CircularNavigation
	bool ___CircularNavigation_7;

public:
	inline static int32_t get_offset_of__system_2() { return static_cast<int32_t>(offsetof(TabNavigationHelper_t538601999, ____system_2)); }
	inline EventSystem_t3466835263 * get__system_2() const { return ____system_2; }
	inline EventSystem_t3466835263 ** get_address_of__system_2() { return &____system_2; }
	inline void set__system_2(EventSystem_t3466835263 * value)
	{
		____system_2 = value;
		Il2CppCodeGenWriteBarrier((&____system_2), value);
	}

	inline static int32_t get_offset_of_StartingObject_3() { return static_cast<int32_t>(offsetof(TabNavigationHelper_t538601999, ___StartingObject_3)); }
	inline Selectable_t1490392188 * get_StartingObject_3() const { return ___StartingObject_3; }
	inline Selectable_t1490392188 ** get_address_of_StartingObject_3() { return &___StartingObject_3; }
	inline void set_StartingObject_3(Selectable_t1490392188 * value)
	{
		___StartingObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___StartingObject_3), value);
	}

	inline static int32_t get_offset_of_LastObject_4() { return static_cast<int32_t>(offsetof(TabNavigationHelper_t538601999, ___LastObject_4)); }
	inline Selectable_t1490392188 * get_LastObject_4() const { return ___LastObject_4; }
	inline Selectable_t1490392188 ** get_address_of_LastObject_4() { return &___LastObject_4; }
	inline void set_LastObject_4(Selectable_t1490392188 * value)
	{
		___LastObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___LastObject_4), value);
	}

	inline static int32_t get_offset_of_NavigationPath_5() { return static_cast<int32_t>(offsetof(TabNavigationHelper_t538601999, ___NavigationPath_5)); }
	inline SelectableU5BU5D_t3083107861* get_NavigationPath_5() const { return ___NavigationPath_5; }
	inline SelectableU5BU5D_t3083107861** get_address_of_NavigationPath_5() { return &___NavigationPath_5; }
	inline void set_NavigationPath_5(SelectableU5BU5D_t3083107861* value)
	{
		___NavigationPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___NavigationPath_5), value);
	}

	inline static int32_t get_offset_of_NavigationMode_6() { return static_cast<int32_t>(offsetof(TabNavigationHelper_t538601999, ___NavigationMode_6)); }
	inline int32_t get_NavigationMode_6() const { return ___NavigationMode_6; }
	inline int32_t* get_address_of_NavigationMode_6() { return &___NavigationMode_6; }
	inline void set_NavigationMode_6(int32_t value)
	{
		___NavigationMode_6 = value;
	}

	inline static int32_t get_offset_of_CircularNavigation_7() { return static_cast<int32_t>(offsetof(TabNavigationHelper_t538601999, ___CircularNavigation_7)); }
	inline bool get_CircularNavigation_7() const { return ___CircularNavigation_7; }
	inline bool* get_address_of_CircularNavigation_7() { return &___CircularNavigation_7; }
	inline void set_CircularNavigation_7(bool value)
	{
		___CircularNavigation_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABNAVIGATIONHELPER_T538601999_H
#ifndef HOVERTOOLTIP_T3001491549_H
#define HOVERTOOLTIP_T3001491549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.HoverTooltip
struct  HoverTooltip_t3001491549  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 UnityEngine.UI.Extensions.HoverTooltip::horizontalPadding
	int32_t ___horizontalPadding_2;
	// System.Int32 UnityEngine.UI.Extensions.HoverTooltip::verticalPadding
	int32_t ___verticalPadding_3;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.HoverTooltip::thisText
	Text_t356221433 * ___thisText_4;
	// UnityEngine.UI.HorizontalLayoutGroup UnityEngine.UI.Extensions.HoverTooltip::hlG
	HorizontalLayoutGroup_t2875670365 * ___hlG_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.HoverTooltip::bgImage
	RectTransform_t3349966182 * ___bgImage_6;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.HoverTooltip::bgImageSource
	Image_t2042527209 * ___bgImageSource_7;
	// System.Boolean UnityEngine.UI.Extensions.HoverTooltip::firstUpdate
	bool ___firstUpdate_8;
	// System.Boolean UnityEngine.UI.Extensions.HoverTooltip::inside
	bool ___inside_9;
	// UnityEngine.RenderMode UnityEngine.UI.Extensions.HoverTooltip::GUIMode
	int32_t ___GUIMode_10;
	// UnityEngine.Camera UnityEngine.UI.Extensions.HoverTooltip::GUICamera
	Camera_t189460977 * ___GUICamera_11;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.HoverTooltip::lowerLeft
	Vector3_t2243707580  ___lowerLeft_12;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.HoverTooltip::upperRight
	Vector3_t2243707580  ___upperRight_13;
	// System.Single UnityEngine.UI.Extensions.HoverTooltip::currentYScaleFactor
	float ___currentYScaleFactor_14;
	// System.Single UnityEngine.UI.Extensions.HoverTooltip::currentXScaleFactor
	float ___currentXScaleFactor_15;
	// System.Single UnityEngine.UI.Extensions.HoverTooltip::defaultYOffset
	float ___defaultYOffset_16;
	// System.Single UnityEngine.UI.Extensions.HoverTooltip::defaultXOffset
	float ___defaultXOffset_17;
	// System.Single UnityEngine.UI.Extensions.HoverTooltip::tooltipRealHeight
	float ___tooltipRealHeight_18;
	// System.Single UnityEngine.UI.Extensions.HoverTooltip::tooltipRealWidth
	float ___tooltipRealWidth_19;

public:
	inline static int32_t get_offset_of_horizontalPadding_2() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___horizontalPadding_2)); }
	inline int32_t get_horizontalPadding_2() const { return ___horizontalPadding_2; }
	inline int32_t* get_address_of_horizontalPadding_2() { return &___horizontalPadding_2; }
	inline void set_horizontalPadding_2(int32_t value)
	{
		___horizontalPadding_2 = value;
	}

	inline static int32_t get_offset_of_verticalPadding_3() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___verticalPadding_3)); }
	inline int32_t get_verticalPadding_3() const { return ___verticalPadding_3; }
	inline int32_t* get_address_of_verticalPadding_3() { return &___verticalPadding_3; }
	inline void set_verticalPadding_3(int32_t value)
	{
		___verticalPadding_3 = value;
	}

	inline static int32_t get_offset_of_thisText_4() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___thisText_4)); }
	inline Text_t356221433 * get_thisText_4() const { return ___thisText_4; }
	inline Text_t356221433 ** get_address_of_thisText_4() { return &___thisText_4; }
	inline void set_thisText_4(Text_t356221433 * value)
	{
		___thisText_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisText_4), value);
	}

	inline static int32_t get_offset_of_hlG_5() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___hlG_5)); }
	inline HorizontalLayoutGroup_t2875670365 * get_hlG_5() const { return ___hlG_5; }
	inline HorizontalLayoutGroup_t2875670365 ** get_address_of_hlG_5() { return &___hlG_5; }
	inline void set_hlG_5(HorizontalLayoutGroup_t2875670365 * value)
	{
		___hlG_5 = value;
		Il2CppCodeGenWriteBarrier((&___hlG_5), value);
	}

	inline static int32_t get_offset_of_bgImage_6() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___bgImage_6)); }
	inline RectTransform_t3349966182 * get_bgImage_6() const { return ___bgImage_6; }
	inline RectTransform_t3349966182 ** get_address_of_bgImage_6() { return &___bgImage_6; }
	inline void set_bgImage_6(RectTransform_t3349966182 * value)
	{
		___bgImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___bgImage_6), value);
	}

	inline static int32_t get_offset_of_bgImageSource_7() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___bgImageSource_7)); }
	inline Image_t2042527209 * get_bgImageSource_7() const { return ___bgImageSource_7; }
	inline Image_t2042527209 ** get_address_of_bgImageSource_7() { return &___bgImageSource_7; }
	inline void set_bgImageSource_7(Image_t2042527209 * value)
	{
		___bgImageSource_7 = value;
		Il2CppCodeGenWriteBarrier((&___bgImageSource_7), value);
	}

	inline static int32_t get_offset_of_firstUpdate_8() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___firstUpdate_8)); }
	inline bool get_firstUpdate_8() const { return ___firstUpdate_8; }
	inline bool* get_address_of_firstUpdate_8() { return &___firstUpdate_8; }
	inline void set_firstUpdate_8(bool value)
	{
		___firstUpdate_8 = value;
	}

	inline static int32_t get_offset_of_inside_9() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___inside_9)); }
	inline bool get_inside_9() const { return ___inside_9; }
	inline bool* get_address_of_inside_9() { return &___inside_9; }
	inline void set_inside_9(bool value)
	{
		___inside_9 = value;
	}

	inline static int32_t get_offset_of_GUIMode_10() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___GUIMode_10)); }
	inline int32_t get_GUIMode_10() const { return ___GUIMode_10; }
	inline int32_t* get_address_of_GUIMode_10() { return &___GUIMode_10; }
	inline void set_GUIMode_10(int32_t value)
	{
		___GUIMode_10 = value;
	}

	inline static int32_t get_offset_of_GUICamera_11() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___GUICamera_11)); }
	inline Camera_t189460977 * get_GUICamera_11() const { return ___GUICamera_11; }
	inline Camera_t189460977 ** get_address_of_GUICamera_11() { return &___GUICamera_11; }
	inline void set_GUICamera_11(Camera_t189460977 * value)
	{
		___GUICamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___GUICamera_11), value);
	}

	inline static int32_t get_offset_of_lowerLeft_12() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___lowerLeft_12)); }
	inline Vector3_t2243707580  get_lowerLeft_12() const { return ___lowerLeft_12; }
	inline Vector3_t2243707580 * get_address_of_lowerLeft_12() { return &___lowerLeft_12; }
	inline void set_lowerLeft_12(Vector3_t2243707580  value)
	{
		___lowerLeft_12 = value;
	}

	inline static int32_t get_offset_of_upperRight_13() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___upperRight_13)); }
	inline Vector3_t2243707580  get_upperRight_13() const { return ___upperRight_13; }
	inline Vector3_t2243707580 * get_address_of_upperRight_13() { return &___upperRight_13; }
	inline void set_upperRight_13(Vector3_t2243707580  value)
	{
		___upperRight_13 = value;
	}

	inline static int32_t get_offset_of_currentYScaleFactor_14() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___currentYScaleFactor_14)); }
	inline float get_currentYScaleFactor_14() const { return ___currentYScaleFactor_14; }
	inline float* get_address_of_currentYScaleFactor_14() { return &___currentYScaleFactor_14; }
	inline void set_currentYScaleFactor_14(float value)
	{
		___currentYScaleFactor_14 = value;
	}

	inline static int32_t get_offset_of_currentXScaleFactor_15() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___currentXScaleFactor_15)); }
	inline float get_currentXScaleFactor_15() const { return ___currentXScaleFactor_15; }
	inline float* get_address_of_currentXScaleFactor_15() { return &___currentXScaleFactor_15; }
	inline void set_currentXScaleFactor_15(float value)
	{
		___currentXScaleFactor_15 = value;
	}

	inline static int32_t get_offset_of_defaultYOffset_16() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___defaultYOffset_16)); }
	inline float get_defaultYOffset_16() const { return ___defaultYOffset_16; }
	inline float* get_address_of_defaultYOffset_16() { return &___defaultYOffset_16; }
	inline void set_defaultYOffset_16(float value)
	{
		___defaultYOffset_16 = value;
	}

	inline static int32_t get_offset_of_defaultXOffset_17() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___defaultXOffset_17)); }
	inline float get_defaultXOffset_17() const { return ___defaultXOffset_17; }
	inline float* get_address_of_defaultXOffset_17() { return &___defaultXOffset_17; }
	inline void set_defaultXOffset_17(float value)
	{
		___defaultXOffset_17 = value;
	}

	inline static int32_t get_offset_of_tooltipRealHeight_18() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___tooltipRealHeight_18)); }
	inline float get_tooltipRealHeight_18() const { return ___tooltipRealHeight_18; }
	inline float* get_address_of_tooltipRealHeight_18() { return &___tooltipRealHeight_18; }
	inline void set_tooltipRealHeight_18(float value)
	{
		___tooltipRealHeight_18 = value;
	}

	inline static int32_t get_offset_of_tooltipRealWidth_19() { return static_cast<int32_t>(offsetof(HoverTooltip_t3001491549, ___tooltipRealWidth_19)); }
	inline float get_tooltipRealWidth_19() const { return ___tooltipRealWidth_19; }
	inline float* get_address_of_tooltipRealWidth_19() { return &___tooltipRealWidth_19; }
	inline void set_tooltipRealWidth_19(float value)
	{
		___tooltipRealWidth_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOVERTOOLTIP_T3001491549_H
#ifndef BOUNDTOOLTIPTRIGGER_T709210963_H
#define BOUNDTOOLTIPTRIGGER_T709210963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoundTooltipTrigger
struct  BoundTooltipTrigger_t709210963  : public MonoBehaviour_t1158329972
{
public:
	// System.String UnityEngine.UI.Extensions.BoundTooltipTrigger::text
	String_t* ___text_2;
	// System.Boolean UnityEngine.UI.Extensions.BoundTooltipTrigger::useMousePosition
	bool ___useMousePosition_3;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.BoundTooltipTrigger::offset
	Vector3_t2243707580  ___offset_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(BoundTooltipTrigger_t709210963, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_useMousePosition_3() { return static_cast<int32_t>(offsetof(BoundTooltipTrigger_t709210963, ___useMousePosition_3)); }
	inline bool get_useMousePosition_3() const { return ___useMousePosition_3; }
	inline bool* get_address_of_useMousePosition_3() { return &___useMousePosition_3; }
	inline void set_useMousePosition_3(bool value)
	{
		___useMousePosition_3 = value;
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(BoundTooltipTrigger_t709210963, ___offset_4)); }
	inline Vector3_t2243707580  get_offset_4() const { return ___offset_4; }
	inline Vector3_t2243707580 * get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(Vector3_t2243707580  value)
	{
		___offset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDTOOLTIPTRIGGER_T709210963_H
#ifndef UIVERTICALSCROLLER_T1840308474_H
#define UIVERTICALSCROLLER_T1840308474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIVerticalScroller
struct  UIVerticalScroller_t1840308474  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIVerticalScroller::_scrollingPanel
	RectTransform_t3349966182 * ____scrollingPanel_2;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.UIVerticalScroller::_arrayOfElements
	GameObjectU5BU5D_t3057952154* ____arrayOfElements_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIVerticalScroller::_center
	RectTransform_t3349966182 * ____center_4;
	// System.Int32 UnityEngine.UI.Extensions.UIVerticalScroller::StartingIndex
	int32_t ___StartingIndex_5;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.UIVerticalScroller::ScrollUpButton
	GameObject_t1756533147 * ___ScrollUpButton_6;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.UIVerticalScroller::ScrollDownButton
	GameObject_t1756533147 * ___ScrollDownButton_7;
	// UnityEngine.Events.UnityEvent`1<System.Int32> UnityEngine.UI.Extensions.UIVerticalScroller::ButtonClicked
	UnityEvent_1_t2110227463 * ___ButtonClicked_8;
	// System.Single[] UnityEngine.UI.Extensions.UIVerticalScroller::distReposition
	SingleU5BU5D_t577127397* ___distReposition_9;
	// System.Single[] UnityEngine.UI.Extensions.UIVerticalScroller::distance
	SingleU5BU5D_t577127397* ___distance_10;
	// System.Int32 UnityEngine.UI.Extensions.UIVerticalScroller::minElementsNum
	int32_t ___minElementsNum_11;
	// System.Int32 UnityEngine.UI.Extensions.UIVerticalScroller::elementLength
	int32_t ___elementLength_12;
	// System.Single UnityEngine.UI.Extensions.UIVerticalScroller::deltaY
	float ___deltaY_13;
	// System.String UnityEngine.UI.Extensions.UIVerticalScroller::result
	String_t* ___result_14;

public:
	inline static int32_t get_offset_of__scrollingPanel_2() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ____scrollingPanel_2)); }
	inline RectTransform_t3349966182 * get__scrollingPanel_2() const { return ____scrollingPanel_2; }
	inline RectTransform_t3349966182 ** get_address_of__scrollingPanel_2() { return &____scrollingPanel_2; }
	inline void set__scrollingPanel_2(RectTransform_t3349966182 * value)
	{
		____scrollingPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&____scrollingPanel_2), value);
	}

	inline static int32_t get_offset_of__arrayOfElements_3() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ____arrayOfElements_3)); }
	inline GameObjectU5BU5D_t3057952154* get__arrayOfElements_3() const { return ____arrayOfElements_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__arrayOfElements_3() { return &____arrayOfElements_3; }
	inline void set__arrayOfElements_3(GameObjectU5BU5D_t3057952154* value)
	{
		____arrayOfElements_3 = value;
		Il2CppCodeGenWriteBarrier((&____arrayOfElements_3), value);
	}

	inline static int32_t get_offset_of__center_4() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ____center_4)); }
	inline RectTransform_t3349966182 * get__center_4() const { return ____center_4; }
	inline RectTransform_t3349966182 ** get_address_of__center_4() { return &____center_4; }
	inline void set__center_4(RectTransform_t3349966182 * value)
	{
		____center_4 = value;
		Il2CppCodeGenWriteBarrier((&____center_4), value);
	}

	inline static int32_t get_offset_of_StartingIndex_5() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___StartingIndex_5)); }
	inline int32_t get_StartingIndex_5() const { return ___StartingIndex_5; }
	inline int32_t* get_address_of_StartingIndex_5() { return &___StartingIndex_5; }
	inline void set_StartingIndex_5(int32_t value)
	{
		___StartingIndex_5 = value;
	}

	inline static int32_t get_offset_of_ScrollUpButton_6() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___ScrollUpButton_6)); }
	inline GameObject_t1756533147 * get_ScrollUpButton_6() const { return ___ScrollUpButton_6; }
	inline GameObject_t1756533147 ** get_address_of_ScrollUpButton_6() { return &___ScrollUpButton_6; }
	inline void set_ScrollUpButton_6(GameObject_t1756533147 * value)
	{
		___ScrollUpButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___ScrollUpButton_6), value);
	}

	inline static int32_t get_offset_of_ScrollDownButton_7() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___ScrollDownButton_7)); }
	inline GameObject_t1756533147 * get_ScrollDownButton_7() const { return ___ScrollDownButton_7; }
	inline GameObject_t1756533147 ** get_address_of_ScrollDownButton_7() { return &___ScrollDownButton_7; }
	inline void set_ScrollDownButton_7(GameObject_t1756533147 * value)
	{
		___ScrollDownButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___ScrollDownButton_7), value);
	}

	inline static int32_t get_offset_of_ButtonClicked_8() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___ButtonClicked_8)); }
	inline UnityEvent_1_t2110227463 * get_ButtonClicked_8() const { return ___ButtonClicked_8; }
	inline UnityEvent_1_t2110227463 ** get_address_of_ButtonClicked_8() { return &___ButtonClicked_8; }
	inline void set_ButtonClicked_8(UnityEvent_1_t2110227463 * value)
	{
		___ButtonClicked_8 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonClicked_8), value);
	}

	inline static int32_t get_offset_of_distReposition_9() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___distReposition_9)); }
	inline SingleU5BU5D_t577127397* get_distReposition_9() const { return ___distReposition_9; }
	inline SingleU5BU5D_t577127397** get_address_of_distReposition_9() { return &___distReposition_9; }
	inline void set_distReposition_9(SingleU5BU5D_t577127397* value)
	{
		___distReposition_9 = value;
		Il2CppCodeGenWriteBarrier((&___distReposition_9), value);
	}

	inline static int32_t get_offset_of_distance_10() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___distance_10)); }
	inline SingleU5BU5D_t577127397* get_distance_10() const { return ___distance_10; }
	inline SingleU5BU5D_t577127397** get_address_of_distance_10() { return &___distance_10; }
	inline void set_distance_10(SingleU5BU5D_t577127397* value)
	{
		___distance_10 = value;
		Il2CppCodeGenWriteBarrier((&___distance_10), value);
	}

	inline static int32_t get_offset_of_minElementsNum_11() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___minElementsNum_11)); }
	inline int32_t get_minElementsNum_11() const { return ___minElementsNum_11; }
	inline int32_t* get_address_of_minElementsNum_11() { return &___minElementsNum_11; }
	inline void set_minElementsNum_11(int32_t value)
	{
		___minElementsNum_11 = value;
	}

	inline static int32_t get_offset_of_elementLength_12() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___elementLength_12)); }
	inline int32_t get_elementLength_12() const { return ___elementLength_12; }
	inline int32_t* get_address_of_elementLength_12() { return &___elementLength_12; }
	inline void set_elementLength_12(int32_t value)
	{
		___elementLength_12 = value;
	}

	inline static int32_t get_offset_of_deltaY_13() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___deltaY_13)); }
	inline float get_deltaY_13() const { return ___deltaY_13; }
	inline float* get_address_of_deltaY_13() { return &___deltaY_13; }
	inline void set_deltaY_13(float value)
	{
		___deltaY_13 = value;
	}

	inline static int32_t get_offset_of_result_14() { return static_cast<int32_t>(offsetof(UIVerticalScroller_t1840308474, ___result_14)); }
	inline String_t* get_result_14() const { return ___result_14; }
	inline String_t** get_address_of_result_14() { return &___result_14; }
	inline void set_result_14(String_t* value)
	{
		___result_14 = value;
		Il2CppCodeGenWriteBarrier((&___result_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTICALSCROLLER_T1840308474_H
#ifndef SCROLLCONFLICTMANAGER_T4227891694_H
#define SCROLLCONFLICTMANAGER_T4227891694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollConflictManager
struct  ScrollConflictManager_t4227891694  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollConflictManager::ParentScrollRect
	ScrollRect_t1199013257 * ___ParentScrollRect_2;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollConflictManager::_myScrollRect
	ScrollRect_t1199013257 * ____myScrollRect_3;
	// System.Boolean UnityEngine.UI.Extensions.ScrollConflictManager::scrollOther
	bool ___scrollOther_4;
	// System.Boolean UnityEngine.UI.Extensions.ScrollConflictManager::scrollOtherHorizontally
	bool ___scrollOtherHorizontally_5;

public:
	inline static int32_t get_offset_of_ParentScrollRect_2() { return static_cast<int32_t>(offsetof(ScrollConflictManager_t4227891694, ___ParentScrollRect_2)); }
	inline ScrollRect_t1199013257 * get_ParentScrollRect_2() const { return ___ParentScrollRect_2; }
	inline ScrollRect_t1199013257 ** get_address_of_ParentScrollRect_2() { return &___ParentScrollRect_2; }
	inline void set_ParentScrollRect_2(ScrollRect_t1199013257 * value)
	{
		___ParentScrollRect_2 = value;
		Il2CppCodeGenWriteBarrier((&___ParentScrollRect_2), value);
	}

	inline static int32_t get_offset_of__myScrollRect_3() { return static_cast<int32_t>(offsetof(ScrollConflictManager_t4227891694, ____myScrollRect_3)); }
	inline ScrollRect_t1199013257 * get__myScrollRect_3() const { return ____myScrollRect_3; }
	inline ScrollRect_t1199013257 ** get_address_of__myScrollRect_3() { return &____myScrollRect_3; }
	inline void set__myScrollRect_3(ScrollRect_t1199013257 * value)
	{
		____myScrollRect_3 = value;
		Il2CppCodeGenWriteBarrier((&____myScrollRect_3), value);
	}

	inline static int32_t get_offset_of_scrollOther_4() { return static_cast<int32_t>(offsetof(ScrollConflictManager_t4227891694, ___scrollOther_4)); }
	inline bool get_scrollOther_4() const { return ___scrollOther_4; }
	inline bool* get_address_of_scrollOther_4() { return &___scrollOther_4; }
	inline void set_scrollOther_4(bool value)
	{
		___scrollOther_4 = value;
	}

	inline static int32_t get_offset_of_scrollOtherHorizontally_5() { return static_cast<int32_t>(offsetof(ScrollConflictManager_t4227891694, ___scrollOtherHorizontally_5)); }
	inline bool get_scrollOtherHorizontally_5() const { return ___scrollOtherHorizontally_5; }
	inline bool* get_address_of_scrollOtherHorizontally_5() { return &___scrollOtherHorizontally_5; }
	inline void set_scrollOtherHorizontally_5(bool value)
	{
		___scrollOtherHorizontally_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLCONFLICTMANAGER_T4227891694_H
#ifndef RETURNKEYTRIGGERSBUTTON_T1668641634_H
#define RETURNKEYTRIGGERSBUTTON_T1668641634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReturnKeyTriggersButton
struct  ReturnKeyTriggersButton_t1668641634  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.UI.ReturnKeyTriggersButton::_system
	EventSystem_t3466835263 * ____system_2;
	// UnityEngine.UI.Button UnityEngine.UI.ReturnKeyTriggersButton::button
	Button_t2872111280 * ___button_3;
	// System.Boolean UnityEngine.UI.ReturnKeyTriggersButton::highlight
	bool ___highlight_4;
	// System.Single UnityEngine.UI.ReturnKeyTriggersButton::highlightDuration
	float ___highlightDuration_5;

public:
	inline static int32_t get_offset_of__system_2() { return static_cast<int32_t>(offsetof(ReturnKeyTriggersButton_t1668641634, ____system_2)); }
	inline EventSystem_t3466835263 * get__system_2() const { return ____system_2; }
	inline EventSystem_t3466835263 ** get_address_of__system_2() { return &____system_2; }
	inline void set__system_2(EventSystem_t3466835263 * value)
	{
		____system_2 = value;
		Il2CppCodeGenWriteBarrier((&____system_2), value);
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(ReturnKeyTriggersButton_t1668641634, ___button_3)); }
	inline Button_t2872111280 * get_button_3() const { return ___button_3; }
	inline Button_t2872111280 ** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(Button_t2872111280 * value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier((&___button_3), value);
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(ReturnKeyTriggersButton_t1668641634, ___highlight_4)); }
	inline bool get_highlight_4() const { return ___highlight_4; }
	inline bool* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(bool value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_highlightDuration_5() { return static_cast<int32_t>(offsetof(ReturnKeyTriggersButton_t1668641634, ___highlightDuration_5)); }
	inline float get_highlightDuration_5() const { return ___highlightDuration_5; }
	inline float* get_address_of_highlightDuration_5() { return &___highlightDuration_5; }
	inline void set_highlightDuration_5(float value)
	{
		___highlightDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETURNKEYTRIGGERSBUTTON_T1668641634_H
#ifndef SCROLLRECTLINKER_T3928490044_H
#define SCROLLRECTLINKER_T3928490044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectLinker
struct  ScrollRectLinker_t3928490044  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.ScrollRectLinker::clamp
	bool ___clamp_2;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollRectLinker::controllingScrollRect
	ScrollRect_t1199013257 * ___controllingScrollRect_3;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollRectLinker::scrollRect
	ScrollRect_t1199013257 * ___scrollRect_4;

public:
	inline static int32_t get_offset_of_clamp_2() { return static_cast<int32_t>(offsetof(ScrollRectLinker_t3928490044, ___clamp_2)); }
	inline bool get_clamp_2() const { return ___clamp_2; }
	inline bool* get_address_of_clamp_2() { return &___clamp_2; }
	inline void set_clamp_2(bool value)
	{
		___clamp_2 = value;
	}

	inline static int32_t get_offset_of_controllingScrollRect_3() { return static_cast<int32_t>(offsetof(ScrollRectLinker_t3928490044, ___controllingScrollRect_3)); }
	inline ScrollRect_t1199013257 * get_controllingScrollRect_3() const { return ___controllingScrollRect_3; }
	inline ScrollRect_t1199013257 ** get_address_of_controllingScrollRect_3() { return &___controllingScrollRect_3; }
	inline void set_controllingScrollRect_3(ScrollRect_t1199013257 * value)
	{
		___controllingScrollRect_3 = value;
		Il2CppCodeGenWriteBarrier((&___controllingScrollRect_3), value);
	}

	inline static int32_t get_offset_of_scrollRect_4() { return static_cast<int32_t>(offsetof(ScrollRectLinker_t3928490044, ___scrollRect_4)); }
	inline ScrollRect_t1199013257 * get_scrollRect_4() const { return ___scrollRect_4; }
	inline ScrollRect_t1199013257 ** get_address_of_scrollRect_4() { return &___scrollRect_4; }
	inline void set_scrollRect_4(ScrollRect_t1199013257 * value)
	{
		___scrollRect_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECTLINKER_T3928490044_H
#ifndef SCROLLSNAP_T2261190493_H
#define SCROLLSNAP_T2261190493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnap
struct  ScrollSnap_t2261190493  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollSnap::_scroll_rect
	ScrollRect_t1199013257 * ____scroll_rect_2;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollSnap::_scrollRectTransform
	RectTransform_t3349966182 * ____scrollRectTransform_3;
	// UnityEngine.Transform UnityEngine.UI.Extensions.ScrollSnap::_listContainerTransform
	Transform_t3275118058 * ____listContainerTransform_4;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::_pages
	int32_t ____pages_5;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::_startingPage
	int32_t ____startingPage_6;
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.ScrollSnap::_pageAnchorPositions
	Vector3U5BU5D_t1172311765* ____pageAnchorPositions_7;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.ScrollSnap::_lerpTarget
	Vector3_t2243707580  ____lerpTarget_8;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::_lerp
	bool ____lerp_9;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::_listContainerMinPosition
	float ____listContainerMinPosition_10;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::_listContainerMaxPosition
	float ____listContainerMaxPosition_11;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::_listContainerSize
	float ____listContainerSize_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollSnap::_listContainerRectTransform
	RectTransform_t3349966182 * ____listContainerRectTransform_13;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ScrollSnap::_listContainerCachedSize
	Vector2_t2243707579  ____listContainerCachedSize_14;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::_itemSize
	float ____itemSize_15;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::_itemsCount
	int32_t ____itemsCount_16;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::_startDrag
	bool ____startDrag_17;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.ScrollSnap::_positionOnDragStart
	Vector3_t2243707580  ____positionOnDragStart_18;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::_pageOnDragStart
	int32_t ____pageOnDragStart_19;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::_fastSwipeTimer
	bool ____fastSwipeTimer_20;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::_fastSwipeCounter
	int32_t ____fastSwipeCounter_21;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::_fastSwipeTarget
	int32_t ____fastSwipeTarget_22;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.ScrollSnap::NextButton
	Button_t2872111280 * ___NextButton_23;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.ScrollSnap::PrevButton
	Button_t2872111280 * ___PrevButton_24;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::ItemsVisibleAtOnce
	int32_t ___ItemsVisibleAtOnce_25;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::AutoLayoutItems
	bool ___AutoLayoutItems_26;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::LinkScrolbarSteps
	bool ___LinkScrolbarSteps_27;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::LinkScrolrectScrollSensitivity
	bool ___LinkScrolrectScrollSensitivity_28;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::UseFastSwipe
	bool ___UseFastSwipe_29;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::FastSwipeThreshold
	int32_t ___FastSwipeThreshold_30;
	// UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange UnityEngine.UI.Extensions.ScrollSnap::onPageChange
	PageSnapChange_t737912504 * ___onPageChange_31;
	// UnityEngine.UI.Extensions.ScrollSnap/ScrollDirection UnityEngine.UI.Extensions.ScrollSnap::direction
	int32_t ___direction_32;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::fastSwipe
	bool ___fastSwipe_33;

public:
	inline static int32_t get_offset_of__scroll_rect_2() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____scroll_rect_2)); }
	inline ScrollRect_t1199013257 * get__scroll_rect_2() const { return ____scroll_rect_2; }
	inline ScrollRect_t1199013257 ** get_address_of__scroll_rect_2() { return &____scroll_rect_2; }
	inline void set__scroll_rect_2(ScrollRect_t1199013257 * value)
	{
		____scroll_rect_2 = value;
		Il2CppCodeGenWriteBarrier((&____scroll_rect_2), value);
	}

	inline static int32_t get_offset_of__scrollRectTransform_3() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____scrollRectTransform_3)); }
	inline RectTransform_t3349966182 * get__scrollRectTransform_3() const { return ____scrollRectTransform_3; }
	inline RectTransform_t3349966182 ** get_address_of__scrollRectTransform_3() { return &____scrollRectTransform_3; }
	inline void set__scrollRectTransform_3(RectTransform_t3349966182 * value)
	{
		____scrollRectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRectTransform_3), value);
	}

	inline static int32_t get_offset_of__listContainerTransform_4() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____listContainerTransform_4)); }
	inline Transform_t3275118058 * get__listContainerTransform_4() const { return ____listContainerTransform_4; }
	inline Transform_t3275118058 ** get_address_of__listContainerTransform_4() { return &____listContainerTransform_4; }
	inline void set__listContainerTransform_4(Transform_t3275118058 * value)
	{
		____listContainerTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&____listContainerTransform_4), value);
	}

	inline static int32_t get_offset_of__pages_5() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____pages_5)); }
	inline int32_t get__pages_5() const { return ____pages_5; }
	inline int32_t* get_address_of__pages_5() { return &____pages_5; }
	inline void set__pages_5(int32_t value)
	{
		____pages_5 = value;
	}

	inline static int32_t get_offset_of__startingPage_6() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____startingPage_6)); }
	inline int32_t get__startingPage_6() const { return ____startingPage_6; }
	inline int32_t* get_address_of__startingPage_6() { return &____startingPage_6; }
	inline void set__startingPage_6(int32_t value)
	{
		____startingPage_6 = value;
	}

	inline static int32_t get_offset_of__pageAnchorPositions_7() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____pageAnchorPositions_7)); }
	inline Vector3U5BU5D_t1172311765* get__pageAnchorPositions_7() const { return ____pageAnchorPositions_7; }
	inline Vector3U5BU5D_t1172311765** get_address_of__pageAnchorPositions_7() { return &____pageAnchorPositions_7; }
	inline void set__pageAnchorPositions_7(Vector3U5BU5D_t1172311765* value)
	{
		____pageAnchorPositions_7 = value;
		Il2CppCodeGenWriteBarrier((&____pageAnchorPositions_7), value);
	}

	inline static int32_t get_offset_of__lerpTarget_8() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____lerpTarget_8)); }
	inline Vector3_t2243707580  get__lerpTarget_8() const { return ____lerpTarget_8; }
	inline Vector3_t2243707580 * get_address_of__lerpTarget_8() { return &____lerpTarget_8; }
	inline void set__lerpTarget_8(Vector3_t2243707580  value)
	{
		____lerpTarget_8 = value;
	}

	inline static int32_t get_offset_of__lerp_9() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____lerp_9)); }
	inline bool get__lerp_9() const { return ____lerp_9; }
	inline bool* get_address_of__lerp_9() { return &____lerp_9; }
	inline void set__lerp_9(bool value)
	{
		____lerp_9 = value;
	}

	inline static int32_t get_offset_of__listContainerMinPosition_10() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____listContainerMinPosition_10)); }
	inline float get__listContainerMinPosition_10() const { return ____listContainerMinPosition_10; }
	inline float* get_address_of__listContainerMinPosition_10() { return &____listContainerMinPosition_10; }
	inline void set__listContainerMinPosition_10(float value)
	{
		____listContainerMinPosition_10 = value;
	}

	inline static int32_t get_offset_of__listContainerMaxPosition_11() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____listContainerMaxPosition_11)); }
	inline float get__listContainerMaxPosition_11() const { return ____listContainerMaxPosition_11; }
	inline float* get_address_of__listContainerMaxPosition_11() { return &____listContainerMaxPosition_11; }
	inline void set__listContainerMaxPosition_11(float value)
	{
		____listContainerMaxPosition_11 = value;
	}

	inline static int32_t get_offset_of__listContainerSize_12() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____listContainerSize_12)); }
	inline float get__listContainerSize_12() const { return ____listContainerSize_12; }
	inline float* get_address_of__listContainerSize_12() { return &____listContainerSize_12; }
	inline void set__listContainerSize_12(float value)
	{
		____listContainerSize_12 = value;
	}

	inline static int32_t get_offset_of__listContainerRectTransform_13() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____listContainerRectTransform_13)); }
	inline RectTransform_t3349966182 * get__listContainerRectTransform_13() const { return ____listContainerRectTransform_13; }
	inline RectTransform_t3349966182 ** get_address_of__listContainerRectTransform_13() { return &____listContainerRectTransform_13; }
	inline void set__listContainerRectTransform_13(RectTransform_t3349966182 * value)
	{
		____listContainerRectTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&____listContainerRectTransform_13), value);
	}

	inline static int32_t get_offset_of__listContainerCachedSize_14() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____listContainerCachedSize_14)); }
	inline Vector2_t2243707579  get__listContainerCachedSize_14() const { return ____listContainerCachedSize_14; }
	inline Vector2_t2243707579 * get_address_of__listContainerCachedSize_14() { return &____listContainerCachedSize_14; }
	inline void set__listContainerCachedSize_14(Vector2_t2243707579  value)
	{
		____listContainerCachedSize_14 = value;
	}

	inline static int32_t get_offset_of__itemSize_15() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____itemSize_15)); }
	inline float get__itemSize_15() const { return ____itemSize_15; }
	inline float* get_address_of__itemSize_15() { return &____itemSize_15; }
	inline void set__itemSize_15(float value)
	{
		____itemSize_15 = value;
	}

	inline static int32_t get_offset_of__itemsCount_16() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____itemsCount_16)); }
	inline int32_t get__itemsCount_16() const { return ____itemsCount_16; }
	inline int32_t* get_address_of__itemsCount_16() { return &____itemsCount_16; }
	inline void set__itemsCount_16(int32_t value)
	{
		____itemsCount_16 = value;
	}

	inline static int32_t get_offset_of__startDrag_17() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____startDrag_17)); }
	inline bool get__startDrag_17() const { return ____startDrag_17; }
	inline bool* get_address_of__startDrag_17() { return &____startDrag_17; }
	inline void set__startDrag_17(bool value)
	{
		____startDrag_17 = value;
	}

	inline static int32_t get_offset_of__positionOnDragStart_18() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____positionOnDragStart_18)); }
	inline Vector3_t2243707580  get__positionOnDragStart_18() const { return ____positionOnDragStart_18; }
	inline Vector3_t2243707580 * get_address_of__positionOnDragStart_18() { return &____positionOnDragStart_18; }
	inline void set__positionOnDragStart_18(Vector3_t2243707580  value)
	{
		____positionOnDragStart_18 = value;
	}

	inline static int32_t get_offset_of__pageOnDragStart_19() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____pageOnDragStart_19)); }
	inline int32_t get__pageOnDragStart_19() const { return ____pageOnDragStart_19; }
	inline int32_t* get_address_of__pageOnDragStart_19() { return &____pageOnDragStart_19; }
	inline void set__pageOnDragStart_19(int32_t value)
	{
		____pageOnDragStart_19 = value;
	}

	inline static int32_t get_offset_of__fastSwipeTimer_20() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____fastSwipeTimer_20)); }
	inline bool get__fastSwipeTimer_20() const { return ____fastSwipeTimer_20; }
	inline bool* get_address_of__fastSwipeTimer_20() { return &____fastSwipeTimer_20; }
	inline void set__fastSwipeTimer_20(bool value)
	{
		____fastSwipeTimer_20 = value;
	}

	inline static int32_t get_offset_of__fastSwipeCounter_21() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____fastSwipeCounter_21)); }
	inline int32_t get__fastSwipeCounter_21() const { return ____fastSwipeCounter_21; }
	inline int32_t* get_address_of__fastSwipeCounter_21() { return &____fastSwipeCounter_21; }
	inline void set__fastSwipeCounter_21(int32_t value)
	{
		____fastSwipeCounter_21 = value;
	}

	inline static int32_t get_offset_of__fastSwipeTarget_22() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ____fastSwipeTarget_22)); }
	inline int32_t get__fastSwipeTarget_22() const { return ____fastSwipeTarget_22; }
	inline int32_t* get_address_of__fastSwipeTarget_22() { return &____fastSwipeTarget_22; }
	inline void set__fastSwipeTarget_22(int32_t value)
	{
		____fastSwipeTarget_22 = value;
	}

	inline static int32_t get_offset_of_NextButton_23() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___NextButton_23)); }
	inline Button_t2872111280 * get_NextButton_23() const { return ___NextButton_23; }
	inline Button_t2872111280 ** get_address_of_NextButton_23() { return &___NextButton_23; }
	inline void set_NextButton_23(Button_t2872111280 * value)
	{
		___NextButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___NextButton_23), value);
	}

	inline static int32_t get_offset_of_PrevButton_24() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___PrevButton_24)); }
	inline Button_t2872111280 * get_PrevButton_24() const { return ___PrevButton_24; }
	inline Button_t2872111280 ** get_address_of_PrevButton_24() { return &___PrevButton_24; }
	inline void set_PrevButton_24(Button_t2872111280 * value)
	{
		___PrevButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___PrevButton_24), value);
	}

	inline static int32_t get_offset_of_ItemsVisibleAtOnce_25() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___ItemsVisibleAtOnce_25)); }
	inline int32_t get_ItemsVisibleAtOnce_25() const { return ___ItemsVisibleAtOnce_25; }
	inline int32_t* get_address_of_ItemsVisibleAtOnce_25() { return &___ItemsVisibleAtOnce_25; }
	inline void set_ItemsVisibleAtOnce_25(int32_t value)
	{
		___ItemsVisibleAtOnce_25 = value;
	}

	inline static int32_t get_offset_of_AutoLayoutItems_26() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___AutoLayoutItems_26)); }
	inline bool get_AutoLayoutItems_26() const { return ___AutoLayoutItems_26; }
	inline bool* get_address_of_AutoLayoutItems_26() { return &___AutoLayoutItems_26; }
	inline void set_AutoLayoutItems_26(bool value)
	{
		___AutoLayoutItems_26 = value;
	}

	inline static int32_t get_offset_of_LinkScrolbarSteps_27() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___LinkScrolbarSteps_27)); }
	inline bool get_LinkScrolbarSteps_27() const { return ___LinkScrolbarSteps_27; }
	inline bool* get_address_of_LinkScrolbarSteps_27() { return &___LinkScrolbarSteps_27; }
	inline void set_LinkScrolbarSteps_27(bool value)
	{
		___LinkScrolbarSteps_27 = value;
	}

	inline static int32_t get_offset_of_LinkScrolrectScrollSensitivity_28() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___LinkScrolrectScrollSensitivity_28)); }
	inline bool get_LinkScrolrectScrollSensitivity_28() const { return ___LinkScrolrectScrollSensitivity_28; }
	inline bool* get_address_of_LinkScrolrectScrollSensitivity_28() { return &___LinkScrolrectScrollSensitivity_28; }
	inline void set_LinkScrolrectScrollSensitivity_28(bool value)
	{
		___LinkScrolrectScrollSensitivity_28 = value;
	}

	inline static int32_t get_offset_of_UseFastSwipe_29() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___UseFastSwipe_29)); }
	inline bool get_UseFastSwipe_29() const { return ___UseFastSwipe_29; }
	inline bool* get_address_of_UseFastSwipe_29() { return &___UseFastSwipe_29; }
	inline void set_UseFastSwipe_29(bool value)
	{
		___UseFastSwipe_29 = value;
	}

	inline static int32_t get_offset_of_FastSwipeThreshold_30() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___FastSwipeThreshold_30)); }
	inline int32_t get_FastSwipeThreshold_30() const { return ___FastSwipeThreshold_30; }
	inline int32_t* get_address_of_FastSwipeThreshold_30() { return &___FastSwipeThreshold_30; }
	inline void set_FastSwipeThreshold_30(int32_t value)
	{
		___FastSwipeThreshold_30 = value;
	}

	inline static int32_t get_offset_of_onPageChange_31() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___onPageChange_31)); }
	inline PageSnapChange_t737912504 * get_onPageChange_31() const { return ___onPageChange_31; }
	inline PageSnapChange_t737912504 ** get_address_of_onPageChange_31() { return &___onPageChange_31; }
	inline void set_onPageChange_31(PageSnapChange_t737912504 * value)
	{
		___onPageChange_31 = value;
		Il2CppCodeGenWriteBarrier((&___onPageChange_31), value);
	}

	inline static int32_t get_offset_of_direction_32() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___direction_32)); }
	inline int32_t get_direction_32() const { return ___direction_32; }
	inline int32_t* get_address_of_direction_32() { return &___direction_32; }
	inline void set_direction_32(int32_t value)
	{
		___direction_32 = value;
	}

	inline static int32_t get_offset_of_fastSwipe_33() { return static_cast<int32_t>(offsetof(ScrollSnap_t2261190493, ___fastSwipe_33)); }
	inline bool get_fastSwipe_33() const { return ___fastSwipe_33; }
	inline bool* get_address_of_fastSwipe_33() { return &___fastSwipe_33; }
	inline void set_fastSwipe_33(bool value)
	{
		___fastSwipe_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLSNAP_T2261190493_H
#ifndef PPIVIEWER_T479698991_H
#define PPIVIEWER_T479698991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.PPIViewer
struct  PPIViewer_t479698991  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.PPIViewer::label
	Text_t356221433 * ___label_2;

public:
	inline static int32_t get_offset_of_label_2() { return static_cast<int32_t>(offsetof(PPIViewer_t479698991, ___label_2)); }
	inline Text_t356221433 * get_label_2() const { return ___label_2; }
	inline Text_t356221433 ** get_address_of_label_2() { return &___label_2; }
	inline void set_label_2(Text_t356221433 * value)
	{
		___label_2 = value;
		Il2CppCodeGenWriteBarrier((&___label_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PPIVIEWER_T479698991_H
#ifndef SCROLLSNAPBASE_T805675194_H
#define SCROLLSNAPBASE_T805675194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnapBase
struct  ScrollSnapBase_t805675194  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rect UnityEngine.UI.Extensions.ScrollSnapBase::panelDimensions
	Rect_t3681755626  ___panelDimensions_2;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollSnapBase::_screensContainer
	RectTransform_t3349966182 * ____screensContainer_3;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::_isVertical
	bool ____isVertical_4;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::_screens
	int32_t ____screens_5;
	// System.Single UnityEngine.UI.Extensions.ScrollSnapBase::_scrollStartPosition
	float ____scrollStartPosition_6;
	// System.Single UnityEngine.UI.Extensions.ScrollSnapBase::_childSize
	float ____childSize_7;
	// System.Single UnityEngine.UI.Extensions.ScrollSnapBase::_childPos
	float ____childPos_8;
	// System.Single UnityEngine.UI.Extensions.ScrollSnapBase::_maskSize
	float ____maskSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ScrollSnapBase::_childAnchorPoint
	Vector2_t2243707579  ____childAnchorPoint_10;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollSnapBase::_scroll_rect
	ScrollRect_t1199013257 * ____scroll_rect_11;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.ScrollSnapBase::_lerp_target
	Vector3_t2243707580  ____lerp_target_12;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::_lerp
	bool ____lerp_13;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::_pointerDown
	bool ____pointerDown_14;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::_settled
	bool ____settled_15;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.ScrollSnapBase::_startPosition
	Vector3_t2243707580  ____startPosition_16;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::_currentPage
	int32_t ____currentPage_17;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::_previousPage
	int32_t ____previousPage_18;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::_halfNoVisibleItems
	int32_t ____halfNoVisibleItems_19;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::_moveStarted
	bool ____moveStarted_20;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::_bottomItem
	int32_t ____bottomItem_21;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::_topItem
	int32_t ____topItem_22;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::StartingScreen
	int32_t ___StartingScreen_23;
	// System.Single UnityEngine.UI.Extensions.ScrollSnapBase::PageStep
	float ___PageStep_24;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ScrollSnapBase::Pagination
	GameObject_t1756533147 * ___Pagination_25;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ScrollSnapBase::PrevButton
	GameObject_t1756533147 * ___PrevButton_26;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ScrollSnapBase::NextButton
	GameObject_t1756533147 * ___NextButton_27;
	// System.Single UnityEngine.UI.Extensions.ScrollSnapBase::transitionSpeed
	float ___transitionSpeed_28;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::UseFastSwipe
	bool ___UseFastSwipe_29;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::FastSwipeThreshold
	int32_t ___FastSwipeThreshold_30;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::SwipeVelocityThreshold
	int32_t ___SwipeVelocityThreshold_31;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollSnapBase::MaskArea
	RectTransform_t3349966182 * ___MaskArea_32;
	// System.Single UnityEngine.UI.Extensions.ScrollSnapBase::MaskBuffer
	float ___MaskBuffer_33;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::JumpOnEnable
	bool ___JumpOnEnable_34;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::RestartOnEnable
	bool ___RestartOnEnable_35;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::UseParentTransform
	bool ___UseParentTransform_36;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.ScrollSnapBase::ChildObjects
	GameObjectU5BU5D_t3057952154* ___ChildObjects_37;
	// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeStartEvent UnityEngine.UI.Extensions.ScrollSnapBase::m_OnSelectionChangeStartEvent
	SelectionChangeStartEvent_t1331424750 * ___m_OnSelectionChangeStartEvent_38;
	// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent UnityEngine.UI.Extensions.ScrollSnapBase::m_OnSelectionPageChangedEvent
	SelectionPageChangedEvent_t3967268665 * ___m_OnSelectionPageChangedEvent_39;
	// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent UnityEngine.UI.Extensions.ScrollSnapBase::m_OnSelectionChangeEndEvent
	SelectionChangeEndEvent_t3994187929 * ___m_OnSelectionChangeEndEvent_40;

public:
	inline static int32_t get_offset_of_panelDimensions_2() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___panelDimensions_2)); }
	inline Rect_t3681755626  get_panelDimensions_2() const { return ___panelDimensions_2; }
	inline Rect_t3681755626 * get_address_of_panelDimensions_2() { return &___panelDimensions_2; }
	inline void set_panelDimensions_2(Rect_t3681755626  value)
	{
		___panelDimensions_2 = value;
	}

	inline static int32_t get_offset_of__screensContainer_3() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____screensContainer_3)); }
	inline RectTransform_t3349966182 * get__screensContainer_3() const { return ____screensContainer_3; }
	inline RectTransform_t3349966182 ** get_address_of__screensContainer_3() { return &____screensContainer_3; }
	inline void set__screensContainer_3(RectTransform_t3349966182 * value)
	{
		____screensContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____screensContainer_3), value);
	}

	inline static int32_t get_offset_of__isVertical_4() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____isVertical_4)); }
	inline bool get__isVertical_4() const { return ____isVertical_4; }
	inline bool* get_address_of__isVertical_4() { return &____isVertical_4; }
	inline void set__isVertical_4(bool value)
	{
		____isVertical_4 = value;
	}

	inline static int32_t get_offset_of__screens_5() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____screens_5)); }
	inline int32_t get__screens_5() const { return ____screens_5; }
	inline int32_t* get_address_of__screens_5() { return &____screens_5; }
	inline void set__screens_5(int32_t value)
	{
		____screens_5 = value;
	}

	inline static int32_t get_offset_of__scrollStartPosition_6() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____scrollStartPosition_6)); }
	inline float get__scrollStartPosition_6() const { return ____scrollStartPosition_6; }
	inline float* get_address_of__scrollStartPosition_6() { return &____scrollStartPosition_6; }
	inline void set__scrollStartPosition_6(float value)
	{
		____scrollStartPosition_6 = value;
	}

	inline static int32_t get_offset_of__childSize_7() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____childSize_7)); }
	inline float get__childSize_7() const { return ____childSize_7; }
	inline float* get_address_of__childSize_7() { return &____childSize_7; }
	inline void set__childSize_7(float value)
	{
		____childSize_7 = value;
	}

	inline static int32_t get_offset_of__childPos_8() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____childPos_8)); }
	inline float get__childPos_8() const { return ____childPos_8; }
	inline float* get_address_of__childPos_8() { return &____childPos_8; }
	inline void set__childPos_8(float value)
	{
		____childPos_8 = value;
	}

	inline static int32_t get_offset_of__maskSize_9() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____maskSize_9)); }
	inline float get__maskSize_9() const { return ____maskSize_9; }
	inline float* get_address_of__maskSize_9() { return &____maskSize_9; }
	inline void set__maskSize_9(float value)
	{
		____maskSize_9 = value;
	}

	inline static int32_t get_offset_of__childAnchorPoint_10() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____childAnchorPoint_10)); }
	inline Vector2_t2243707579  get__childAnchorPoint_10() const { return ____childAnchorPoint_10; }
	inline Vector2_t2243707579 * get_address_of__childAnchorPoint_10() { return &____childAnchorPoint_10; }
	inline void set__childAnchorPoint_10(Vector2_t2243707579  value)
	{
		____childAnchorPoint_10 = value;
	}

	inline static int32_t get_offset_of__scroll_rect_11() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____scroll_rect_11)); }
	inline ScrollRect_t1199013257 * get__scroll_rect_11() const { return ____scroll_rect_11; }
	inline ScrollRect_t1199013257 ** get_address_of__scroll_rect_11() { return &____scroll_rect_11; }
	inline void set__scroll_rect_11(ScrollRect_t1199013257 * value)
	{
		____scroll_rect_11 = value;
		Il2CppCodeGenWriteBarrier((&____scroll_rect_11), value);
	}

	inline static int32_t get_offset_of__lerp_target_12() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____lerp_target_12)); }
	inline Vector3_t2243707580  get__lerp_target_12() const { return ____lerp_target_12; }
	inline Vector3_t2243707580 * get_address_of__lerp_target_12() { return &____lerp_target_12; }
	inline void set__lerp_target_12(Vector3_t2243707580  value)
	{
		____lerp_target_12 = value;
	}

	inline static int32_t get_offset_of__lerp_13() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____lerp_13)); }
	inline bool get__lerp_13() const { return ____lerp_13; }
	inline bool* get_address_of__lerp_13() { return &____lerp_13; }
	inline void set__lerp_13(bool value)
	{
		____lerp_13 = value;
	}

	inline static int32_t get_offset_of__pointerDown_14() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____pointerDown_14)); }
	inline bool get__pointerDown_14() const { return ____pointerDown_14; }
	inline bool* get_address_of__pointerDown_14() { return &____pointerDown_14; }
	inline void set__pointerDown_14(bool value)
	{
		____pointerDown_14 = value;
	}

	inline static int32_t get_offset_of__settled_15() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____settled_15)); }
	inline bool get__settled_15() const { return ____settled_15; }
	inline bool* get_address_of__settled_15() { return &____settled_15; }
	inline void set__settled_15(bool value)
	{
		____settled_15 = value;
	}

	inline static int32_t get_offset_of__startPosition_16() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____startPosition_16)); }
	inline Vector3_t2243707580  get__startPosition_16() const { return ____startPosition_16; }
	inline Vector3_t2243707580 * get_address_of__startPosition_16() { return &____startPosition_16; }
	inline void set__startPosition_16(Vector3_t2243707580  value)
	{
		____startPosition_16 = value;
	}

	inline static int32_t get_offset_of__currentPage_17() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____currentPage_17)); }
	inline int32_t get__currentPage_17() const { return ____currentPage_17; }
	inline int32_t* get_address_of__currentPage_17() { return &____currentPage_17; }
	inline void set__currentPage_17(int32_t value)
	{
		____currentPage_17 = value;
	}

	inline static int32_t get_offset_of__previousPage_18() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____previousPage_18)); }
	inline int32_t get__previousPage_18() const { return ____previousPage_18; }
	inline int32_t* get_address_of__previousPage_18() { return &____previousPage_18; }
	inline void set__previousPage_18(int32_t value)
	{
		____previousPage_18 = value;
	}

	inline static int32_t get_offset_of__halfNoVisibleItems_19() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____halfNoVisibleItems_19)); }
	inline int32_t get__halfNoVisibleItems_19() const { return ____halfNoVisibleItems_19; }
	inline int32_t* get_address_of__halfNoVisibleItems_19() { return &____halfNoVisibleItems_19; }
	inline void set__halfNoVisibleItems_19(int32_t value)
	{
		____halfNoVisibleItems_19 = value;
	}

	inline static int32_t get_offset_of__moveStarted_20() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____moveStarted_20)); }
	inline bool get__moveStarted_20() const { return ____moveStarted_20; }
	inline bool* get_address_of__moveStarted_20() { return &____moveStarted_20; }
	inline void set__moveStarted_20(bool value)
	{
		____moveStarted_20 = value;
	}

	inline static int32_t get_offset_of__bottomItem_21() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____bottomItem_21)); }
	inline int32_t get__bottomItem_21() const { return ____bottomItem_21; }
	inline int32_t* get_address_of__bottomItem_21() { return &____bottomItem_21; }
	inline void set__bottomItem_21(int32_t value)
	{
		____bottomItem_21 = value;
	}

	inline static int32_t get_offset_of__topItem_22() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ____topItem_22)); }
	inline int32_t get__topItem_22() const { return ____topItem_22; }
	inline int32_t* get_address_of__topItem_22() { return &____topItem_22; }
	inline void set__topItem_22(int32_t value)
	{
		____topItem_22 = value;
	}

	inline static int32_t get_offset_of_StartingScreen_23() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___StartingScreen_23)); }
	inline int32_t get_StartingScreen_23() const { return ___StartingScreen_23; }
	inline int32_t* get_address_of_StartingScreen_23() { return &___StartingScreen_23; }
	inline void set_StartingScreen_23(int32_t value)
	{
		___StartingScreen_23 = value;
	}

	inline static int32_t get_offset_of_PageStep_24() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___PageStep_24)); }
	inline float get_PageStep_24() const { return ___PageStep_24; }
	inline float* get_address_of_PageStep_24() { return &___PageStep_24; }
	inline void set_PageStep_24(float value)
	{
		___PageStep_24 = value;
	}

	inline static int32_t get_offset_of_Pagination_25() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___Pagination_25)); }
	inline GameObject_t1756533147 * get_Pagination_25() const { return ___Pagination_25; }
	inline GameObject_t1756533147 ** get_address_of_Pagination_25() { return &___Pagination_25; }
	inline void set_Pagination_25(GameObject_t1756533147 * value)
	{
		___Pagination_25 = value;
		Il2CppCodeGenWriteBarrier((&___Pagination_25), value);
	}

	inline static int32_t get_offset_of_PrevButton_26() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___PrevButton_26)); }
	inline GameObject_t1756533147 * get_PrevButton_26() const { return ___PrevButton_26; }
	inline GameObject_t1756533147 ** get_address_of_PrevButton_26() { return &___PrevButton_26; }
	inline void set_PrevButton_26(GameObject_t1756533147 * value)
	{
		___PrevButton_26 = value;
		Il2CppCodeGenWriteBarrier((&___PrevButton_26), value);
	}

	inline static int32_t get_offset_of_NextButton_27() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___NextButton_27)); }
	inline GameObject_t1756533147 * get_NextButton_27() const { return ___NextButton_27; }
	inline GameObject_t1756533147 ** get_address_of_NextButton_27() { return &___NextButton_27; }
	inline void set_NextButton_27(GameObject_t1756533147 * value)
	{
		___NextButton_27 = value;
		Il2CppCodeGenWriteBarrier((&___NextButton_27), value);
	}

	inline static int32_t get_offset_of_transitionSpeed_28() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___transitionSpeed_28)); }
	inline float get_transitionSpeed_28() const { return ___transitionSpeed_28; }
	inline float* get_address_of_transitionSpeed_28() { return &___transitionSpeed_28; }
	inline void set_transitionSpeed_28(float value)
	{
		___transitionSpeed_28 = value;
	}

	inline static int32_t get_offset_of_UseFastSwipe_29() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___UseFastSwipe_29)); }
	inline bool get_UseFastSwipe_29() const { return ___UseFastSwipe_29; }
	inline bool* get_address_of_UseFastSwipe_29() { return &___UseFastSwipe_29; }
	inline void set_UseFastSwipe_29(bool value)
	{
		___UseFastSwipe_29 = value;
	}

	inline static int32_t get_offset_of_FastSwipeThreshold_30() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___FastSwipeThreshold_30)); }
	inline int32_t get_FastSwipeThreshold_30() const { return ___FastSwipeThreshold_30; }
	inline int32_t* get_address_of_FastSwipeThreshold_30() { return &___FastSwipeThreshold_30; }
	inline void set_FastSwipeThreshold_30(int32_t value)
	{
		___FastSwipeThreshold_30 = value;
	}

	inline static int32_t get_offset_of_SwipeVelocityThreshold_31() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___SwipeVelocityThreshold_31)); }
	inline int32_t get_SwipeVelocityThreshold_31() const { return ___SwipeVelocityThreshold_31; }
	inline int32_t* get_address_of_SwipeVelocityThreshold_31() { return &___SwipeVelocityThreshold_31; }
	inline void set_SwipeVelocityThreshold_31(int32_t value)
	{
		___SwipeVelocityThreshold_31 = value;
	}

	inline static int32_t get_offset_of_MaskArea_32() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___MaskArea_32)); }
	inline RectTransform_t3349966182 * get_MaskArea_32() const { return ___MaskArea_32; }
	inline RectTransform_t3349966182 ** get_address_of_MaskArea_32() { return &___MaskArea_32; }
	inline void set_MaskArea_32(RectTransform_t3349966182 * value)
	{
		___MaskArea_32 = value;
		Il2CppCodeGenWriteBarrier((&___MaskArea_32), value);
	}

	inline static int32_t get_offset_of_MaskBuffer_33() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___MaskBuffer_33)); }
	inline float get_MaskBuffer_33() const { return ___MaskBuffer_33; }
	inline float* get_address_of_MaskBuffer_33() { return &___MaskBuffer_33; }
	inline void set_MaskBuffer_33(float value)
	{
		___MaskBuffer_33 = value;
	}

	inline static int32_t get_offset_of_JumpOnEnable_34() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___JumpOnEnable_34)); }
	inline bool get_JumpOnEnable_34() const { return ___JumpOnEnable_34; }
	inline bool* get_address_of_JumpOnEnable_34() { return &___JumpOnEnable_34; }
	inline void set_JumpOnEnable_34(bool value)
	{
		___JumpOnEnable_34 = value;
	}

	inline static int32_t get_offset_of_RestartOnEnable_35() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___RestartOnEnable_35)); }
	inline bool get_RestartOnEnable_35() const { return ___RestartOnEnable_35; }
	inline bool* get_address_of_RestartOnEnable_35() { return &___RestartOnEnable_35; }
	inline void set_RestartOnEnable_35(bool value)
	{
		___RestartOnEnable_35 = value;
	}

	inline static int32_t get_offset_of_UseParentTransform_36() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___UseParentTransform_36)); }
	inline bool get_UseParentTransform_36() const { return ___UseParentTransform_36; }
	inline bool* get_address_of_UseParentTransform_36() { return &___UseParentTransform_36; }
	inline void set_UseParentTransform_36(bool value)
	{
		___UseParentTransform_36 = value;
	}

	inline static int32_t get_offset_of_ChildObjects_37() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___ChildObjects_37)); }
	inline GameObjectU5BU5D_t3057952154* get_ChildObjects_37() const { return ___ChildObjects_37; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ChildObjects_37() { return &___ChildObjects_37; }
	inline void set_ChildObjects_37(GameObjectU5BU5D_t3057952154* value)
	{
		___ChildObjects_37 = value;
		Il2CppCodeGenWriteBarrier((&___ChildObjects_37), value);
	}

	inline static int32_t get_offset_of_m_OnSelectionChangeStartEvent_38() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___m_OnSelectionChangeStartEvent_38)); }
	inline SelectionChangeStartEvent_t1331424750 * get_m_OnSelectionChangeStartEvent_38() const { return ___m_OnSelectionChangeStartEvent_38; }
	inline SelectionChangeStartEvent_t1331424750 ** get_address_of_m_OnSelectionChangeStartEvent_38() { return &___m_OnSelectionChangeStartEvent_38; }
	inline void set_m_OnSelectionChangeStartEvent_38(SelectionChangeStartEvent_t1331424750 * value)
	{
		___m_OnSelectionChangeStartEvent_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSelectionChangeStartEvent_38), value);
	}

	inline static int32_t get_offset_of_m_OnSelectionPageChangedEvent_39() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___m_OnSelectionPageChangedEvent_39)); }
	inline SelectionPageChangedEvent_t3967268665 * get_m_OnSelectionPageChangedEvent_39() const { return ___m_OnSelectionPageChangedEvent_39; }
	inline SelectionPageChangedEvent_t3967268665 ** get_address_of_m_OnSelectionPageChangedEvent_39() { return &___m_OnSelectionPageChangedEvent_39; }
	inline void set_m_OnSelectionPageChangedEvent_39(SelectionPageChangedEvent_t3967268665 * value)
	{
		___m_OnSelectionPageChangedEvent_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSelectionPageChangedEvent_39), value);
	}

	inline static int32_t get_offset_of_m_OnSelectionChangeEndEvent_40() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t805675194, ___m_OnSelectionChangeEndEvent_40)); }
	inline SelectionChangeEndEvent_t3994187929 * get_m_OnSelectionChangeEndEvent_40() const { return ___m_OnSelectionChangeEndEvent_40; }
	inline SelectionChangeEndEvent_t3994187929 ** get_address_of_m_OnSelectionChangeEndEvent_40() { return &___m_OnSelectionChangeEndEvent_40; }
	inline void set_m_OnSelectionChangeEndEvent_40(SelectionChangeEndEvent_t3994187929 * value)
	{
		___m_OnSelectionChangeEndEvent_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSelectionChangeEndEvent_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLSNAPBASE_T805675194_H
#ifndef SCROLLSNAPSCROLLBARHELPER_T403895447_H
#define SCROLLSNAPSCROLLBARHELPER_T403895447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnapScrollbarHelper
struct  ScrollSnapScrollbarHelper_t403895447  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.IScrollSnap UnityEngine.UI.Extensions.ScrollSnapScrollbarHelper::ss
	RuntimeObject* ___ss_2;

public:
	inline static int32_t get_offset_of_ss_2() { return static_cast<int32_t>(offsetof(ScrollSnapScrollbarHelper_t403895447, ___ss_2)); }
	inline RuntimeObject* get_ss_2() const { return ___ss_2; }
	inline RuntimeObject** get_address_of_ss_2() { return &___ss_2; }
	inline void set_ss_2(RuntimeObject* value)
	{
		___ss_2 = value;
		Il2CppCodeGenWriteBarrier((&___ss_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLSNAPSCROLLBARHELPER_T403895447_H
#ifndef INPUTFIELDENTERSUBMIT_T1520170634_H
#define INPUTFIELDENTERSUBMIT_T1520170634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.InputFieldEnterSubmit
struct  InputFieldEnterSubmit_t1520170634  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.InputFieldEnterSubmit/EnterSubmitEvent UnityEngine.UI.Extensions.InputFieldEnterSubmit::EnterSubmit
	EnterSubmitEvent_t3688949694 * ___EnterSubmit_2;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.InputFieldEnterSubmit::_input
	InputField_t1631627530 * ____input_3;

public:
	inline static int32_t get_offset_of_EnterSubmit_2() { return static_cast<int32_t>(offsetof(InputFieldEnterSubmit_t1520170634, ___EnterSubmit_2)); }
	inline EnterSubmitEvent_t3688949694 * get_EnterSubmit_2() const { return ___EnterSubmit_2; }
	inline EnterSubmitEvent_t3688949694 ** get_address_of_EnterSubmit_2() { return &___EnterSubmit_2; }
	inline void set_EnterSubmit_2(EnterSubmitEvent_t3688949694 * value)
	{
		___EnterSubmit_2 = value;
		Il2CppCodeGenWriteBarrier((&___EnterSubmit_2), value);
	}

	inline static int32_t get_offset_of__input_3() { return static_cast<int32_t>(offsetof(InputFieldEnterSubmit_t1520170634, ____input_3)); }
	inline InputField_t1631627530 * get__input_3() const { return ____input_3; }
	inline InputField_t1631627530 ** get_address_of__input_3() { return &____input_3; }
	inline void set__input_3(InputField_t1631627530 * value)
	{
		____input_3 = value;
		Il2CppCodeGenWriteBarrier((&____input_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELDENTERSUBMIT_T1520170634_H
#ifndef SCROLLRECT_T1199013257_H
#define SCROLLRECT_T1199013257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_t1199013257  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t3349966182 * ___m_Content_2;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_3;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_4;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_5;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_6;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_7;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_8;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_9;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t3349966182 * ___m_Viewport_10;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t3248359358 * ___m_HorizontalScrollbar_11;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t3248359358 * ___m_VerticalScrollbar_12;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_13;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_14;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_15;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_16;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t3529018992 * ___m_OnValueChanged_17;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_t2243707579  ___m_PointerStartLocalCursor_18;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_t2243707579  ___m_ContentStartPosition_19;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t3349966182 * ___m_ViewRect_20;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t3033363703  ___m_ContentBounds_21;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t3033363703  ___m_ViewBounds_22;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_t2243707579  ___m_Velocity_23;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_t2243707579  ___m_PrevPosition_25;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t3033363703  ___m_PrevContentBounds_26;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t3033363703  ___m_PrevViewBounds_27;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_28;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_29;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_30;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_31;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_32;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t3349966182 * ___m_Rect_33;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t3349966182 * ___m_HorizontalScrollbarRect_34;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t3349966182 * ___m_VerticalScrollbarRect_35;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_36;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_37;

public:
	inline static int32_t get_offset_of_m_Content_2() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Content_2)); }
	inline RectTransform_t3349966182 * get_m_Content_2() const { return ___m_Content_2; }
	inline RectTransform_t3349966182 ** get_address_of_m_Content_2() { return &___m_Content_2; }
	inline void set_m_Content_2(RectTransform_t3349966182 * value)
	{
		___m_Content_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_2), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_3() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Horizontal_3)); }
	inline bool get_m_Horizontal_3() const { return ___m_Horizontal_3; }
	inline bool* get_address_of_m_Horizontal_3() { return &___m_Horizontal_3; }
	inline void set_m_Horizontal_3(bool value)
	{
		___m_Horizontal_3 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_4() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Vertical_4)); }
	inline bool get_m_Vertical_4() const { return ___m_Vertical_4; }
	inline bool* get_address_of_m_Vertical_4() { return &___m_Vertical_4; }
	inline void set_m_Vertical_4(bool value)
	{
		___m_Vertical_4 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_5() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_MovementType_5)); }
	inline int32_t get_m_MovementType_5() const { return ___m_MovementType_5; }
	inline int32_t* get_address_of_m_MovementType_5() { return &___m_MovementType_5; }
	inline void set_m_MovementType_5(int32_t value)
	{
		___m_MovementType_5 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_6() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Elasticity_6)); }
	inline float get_m_Elasticity_6() const { return ___m_Elasticity_6; }
	inline float* get_address_of_m_Elasticity_6() { return &___m_Elasticity_6; }
	inline void set_m_Elasticity_6(float value)
	{
		___m_Elasticity_6 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_7() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Inertia_7)); }
	inline bool get_m_Inertia_7() const { return ___m_Inertia_7; }
	inline bool* get_address_of_m_Inertia_7() { return &___m_Inertia_7; }
	inline void set_m_Inertia_7(bool value)
	{
		___m_Inertia_7 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_8() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_DecelerationRate_8)); }
	inline float get_m_DecelerationRate_8() const { return ___m_DecelerationRate_8; }
	inline float* get_address_of_m_DecelerationRate_8() { return &___m_DecelerationRate_8; }
	inline void set_m_DecelerationRate_8(float value)
	{
		___m_DecelerationRate_8 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_9() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ScrollSensitivity_9)); }
	inline float get_m_ScrollSensitivity_9() const { return ___m_ScrollSensitivity_9; }
	inline float* get_address_of_m_ScrollSensitivity_9() { return &___m_ScrollSensitivity_9; }
	inline void set_m_ScrollSensitivity_9(float value)
	{
		___m_ScrollSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_10() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Viewport_10)); }
	inline RectTransform_t3349966182 * get_m_Viewport_10() const { return ___m_Viewport_10; }
	inline RectTransform_t3349966182 ** get_address_of_m_Viewport_10() { return &___m_Viewport_10; }
	inline void set_m_Viewport_10(RectTransform_t3349966182 * value)
	{
		___m_Viewport_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_10), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_11() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbar_11)); }
	inline Scrollbar_t3248359358 * get_m_HorizontalScrollbar_11() const { return ___m_HorizontalScrollbar_11; }
	inline Scrollbar_t3248359358 ** get_address_of_m_HorizontalScrollbar_11() { return &___m_HorizontalScrollbar_11; }
	inline void set_m_HorizontalScrollbar_11(Scrollbar_t3248359358 * value)
	{
		___m_HorizontalScrollbar_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_12() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbar_12)); }
	inline Scrollbar_t3248359358 * get_m_VerticalScrollbar_12() const { return ___m_VerticalScrollbar_12; }
	inline Scrollbar_t3248359358 ** get_address_of_m_VerticalScrollbar_12() { return &___m_VerticalScrollbar_12; }
	inline void set_m_VerticalScrollbar_12(Scrollbar_t3248359358 * value)
	{
		___m_VerticalScrollbar_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_13() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarVisibility_13)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_13() const { return ___m_HorizontalScrollbarVisibility_13; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_13() { return &___m_HorizontalScrollbarVisibility_13; }
	inline void set_m_HorizontalScrollbarVisibility_13(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_13 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_14() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarVisibility_14)); }
	inline int32_t get_m_VerticalScrollbarVisibility_14() const { return ___m_VerticalScrollbarVisibility_14; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_14() { return &___m_VerticalScrollbarVisibility_14; }
	inline void set_m_VerticalScrollbarVisibility_14(int32_t value)
	{
		___m_VerticalScrollbarVisibility_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_15() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarSpacing_15)); }
	inline float get_m_HorizontalScrollbarSpacing_15() const { return ___m_HorizontalScrollbarSpacing_15; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_15() { return &___m_HorizontalScrollbarSpacing_15; }
	inline void set_m_HorizontalScrollbarSpacing_15(float value)
	{
		___m_HorizontalScrollbarSpacing_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_16() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarSpacing_16)); }
	inline float get_m_VerticalScrollbarSpacing_16() const { return ___m_VerticalScrollbarSpacing_16; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_16() { return &___m_VerticalScrollbarSpacing_16; }
	inline void set_m_VerticalScrollbarSpacing_16(float value)
	{
		___m_VerticalScrollbarSpacing_16 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_17() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_OnValueChanged_17)); }
	inline ScrollRectEvent_t3529018992 * get_m_OnValueChanged_17() const { return ___m_OnValueChanged_17; }
	inline ScrollRectEvent_t3529018992 ** get_address_of_m_OnValueChanged_17() { return &___m_OnValueChanged_17; }
	inline void set_m_OnValueChanged_17(ScrollRectEvent_t3529018992 * value)
	{
		___m_OnValueChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_17), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_18() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PointerStartLocalCursor_18)); }
	inline Vector2_t2243707579  get_m_PointerStartLocalCursor_18() const { return ___m_PointerStartLocalCursor_18; }
	inline Vector2_t2243707579 * get_address_of_m_PointerStartLocalCursor_18() { return &___m_PointerStartLocalCursor_18; }
	inline void set_m_PointerStartLocalCursor_18(Vector2_t2243707579  value)
	{
		___m_PointerStartLocalCursor_18 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_19() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ContentStartPosition_19)); }
	inline Vector2_t2243707579  get_m_ContentStartPosition_19() const { return ___m_ContentStartPosition_19; }
	inline Vector2_t2243707579 * get_address_of_m_ContentStartPosition_19() { return &___m_ContentStartPosition_19; }
	inline void set_m_ContentStartPosition_19(Vector2_t2243707579  value)
	{
		___m_ContentStartPosition_19 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_20() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ViewRect_20)); }
	inline RectTransform_t3349966182 * get_m_ViewRect_20() const { return ___m_ViewRect_20; }
	inline RectTransform_t3349966182 ** get_address_of_m_ViewRect_20() { return &___m_ViewRect_20; }
	inline void set_m_ViewRect_20(RectTransform_t3349966182 * value)
	{
		___m_ViewRect_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_20), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_21() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ContentBounds_21)); }
	inline Bounds_t3033363703  get_m_ContentBounds_21() const { return ___m_ContentBounds_21; }
	inline Bounds_t3033363703 * get_address_of_m_ContentBounds_21() { return &___m_ContentBounds_21; }
	inline void set_m_ContentBounds_21(Bounds_t3033363703  value)
	{
		___m_ContentBounds_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_22() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ViewBounds_22)); }
	inline Bounds_t3033363703  get_m_ViewBounds_22() const { return ___m_ViewBounds_22; }
	inline Bounds_t3033363703 * get_address_of_m_ViewBounds_22() { return &___m_ViewBounds_22; }
	inline void set_m_ViewBounds_22(Bounds_t3033363703  value)
	{
		___m_ViewBounds_22 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_23() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Velocity_23)); }
	inline Vector2_t2243707579  get_m_Velocity_23() const { return ___m_Velocity_23; }
	inline Vector2_t2243707579 * get_address_of_m_Velocity_23() { return &___m_Velocity_23; }
	inline void set_m_Velocity_23(Vector2_t2243707579  value)
	{
		___m_Velocity_23 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_24() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Dragging_24)); }
	inline bool get_m_Dragging_24() const { return ___m_Dragging_24; }
	inline bool* get_address_of_m_Dragging_24() { return &___m_Dragging_24; }
	inline void set_m_Dragging_24(bool value)
	{
		___m_Dragging_24 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_25() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevPosition_25)); }
	inline Vector2_t2243707579  get_m_PrevPosition_25() const { return ___m_PrevPosition_25; }
	inline Vector2_t2243707579 * get_address_of_m_PrevPosition_25() { return &___m_PrevPosition_25; }
	inline void set_m_PrevPosition_25(Vector2_t2243707579  value)
	{
		___m_PrevPosition_25 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_26() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevContentBounds_26)); }
	inline Bounds_t3033363703  get_m_PrevContentBounds_26() const { return ___m_PrevContentBounds_26; }
	inline Bounds_t3033363703 * get_address_of_m_PrevContentBounds_26() { return &___m_PrevContentBounds_26; }
	inline void set_m_PrevContentBounds_26(Bounds_t3033363703  value)
	{
		___m_PrevContentBounds_26 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_27() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevViewBounds_27)); }
	inline Bounds_t3033363703  get_m_PrevViewBounds_27() const { return ___m_PrevViewBounds_27; }
	inline Bounds_t3033363703 * get_address_of_m_PrevViewBounds_27() { return &___m_PrevViewBounds_27; }
	inline void set_m_PrevViewBounds_27(Bounds_t3033363703  value)
	{
		___m_PrevViewBounds_27 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_28() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HasRebuiltLayout_28)); }
	inline bool get_m_HasRebuiltLayout_28() const { return ___m_HasRebuiltLayout_28; }
	inline bool* get_address_of_m_HasRebuiltLayout_28() { return &___m_HasRebuiltLayout_28; }
	inline void set_m_HasRebuiltLayout_28(bool value)
	{
		___m_HasRebuiltLayout_28 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_29() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HSliderExpand_29)); }
	inline bool get_m_HSliderExpand_29() const { return ___m_HSliderExpand_29; }
	inline bool* get_address_of_m_HSliderExpand_29() { return &___m_HSliderExpand_29; }
	inline void set_m_HSliderExpand_29(bool value)
	{
		___m_HSliderExpand_29 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_30() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VSliderExpand_30)); }
	inline bool get_m_VSliderExpand_30() const { return ___m_VSliderExpand_30; }
	inline bool* get_address_of_m_VSliderExpand_30() { return &___m_VSliderExpand_30; }
	inline void set_m_VSliderExpand_30(bool value)
	{
		___m_VSliderExpand_30 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_31() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HSliderHeight_31)); }
	inline float get_m_HSliderHeight_31() const { return ___m_HSliderHeight_31; }
	inline float* get_address_of_m_HSliderHeight_31() { return &___m_HSliderHeight_31; }
	inline void set_m_HSliderHeight_31(float value)
	{
		___m_HSliderHeight_31 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_32() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VSliderWidth_32)); }
	inline float get_m_VSliderWidth_32() const { return ___m_VSliderWidth_32; }
	inline float* get_address_of_m_VSliderWidth_32() { return &___m_VSliderWidth_32; }
	inline void set_m_VSliderWidth_32(float value)
	{
		___m_VSliderWidth_32 = value;
	}

	inline static int32_t get_offset_of_m_Rect_33() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Rect_33)); }
	inline RectTransform_t3349966182 * get_m_Rect_33() const { return ___m_Rect_33; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_33() { return &___m_Rect_33; }
	inline void set_m_Rect_33(RectTransform_t3349966182 * value)
	{
		___m_Rect_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_33), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_34() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarRect_34)); }
	inline RectTransform_t3349966182 * get_m_HorizontalScrollbarRect_34() const { return ___m_HorizontalScrollbarRect_34; }
	inline RectTransform_t3349966182 ** get_address_of_m_HorizontalScrollbarRect_34() { return &___m_HorizontalScrollbarRect_34; }
	inline void set_m_HorizontalScrollbarRect_34(RectTransform_t3349966182 * value)
	{
		___m_HorizontalScrollbarRect_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_34), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_35() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarRect_35)); }
	inline RectTransform_t3349966182 * get_m_VerticalScrollbarRect_35() const { return ___m_VerticalScrollbarRect_35; }
	inline RectTransform_t3349966182 ** get_address_of_m_VerticalScrollbarRect_35() { return &___m_VerticalScrollbarRect_35; }
	inline void set_m_VerticalScrollbarRect_35(RectTransform_t3349966182 * value)
	{
		___m_VerticalScrollbarRect_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_35), value);
	}

	inline static int32_t get_offset_of_m_Tracker_36() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Tracker_36)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_36() const { return ___m_Tracker_36; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_36() { return &___m_Tracker_36; }
	inline void set_m_Tracker_36(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_36 = value;
	}

	inline static int32_t get_offset_of_m_Corners_37() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Corners_37)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_37() const { return ___m_Corners_37; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_37() { return &___m_Corners_37; }
	inline void set_m_Corners_37(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_T1199013257_H
#ifndef TOGGLEGROUP_T1030026315_H
#define TOGGLEGROUP_T1030026315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ToggleGroup
struct  ToggleGroup_t1030026315  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean UnityEngine.UI.ToggleGroup::m_AllowSwitchOff
	bool ___m_AllowSwitchOff_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::m_Toggles
	List_1_t3345875600 * ___m_Toggles_3;

public:
	inline static int32_t get_offset_of_m_AllowSwitchOff_2() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315, ___m_AllowSwitchOff_2)); }
	inline bool get_m_AllowSwitchOff_2() const { return ___m_AllowSwitchOff_2; }
	inline bool* get_address_of_m_AllowSwitchOff_2() { return &___m_AllowSwitchOff_2; }
	inline void set_m_AllowSwitchOff_2(bool value)
	{
		___m_AllowSwitchOff_2 = value;
	}

	inline static int32_t get_offset_of_m_Toggles_3() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315, ___m_Toggles_3)); }
	inline List_1_t3345875600 * get_m_Toggles_3() const { return ___m_Toggles_3; }
	inline List_1_t3345875600 ** get_address_of_m_Toggles_3() { return &___m_Toggles_3; }
	inline void set_m_Toggles_3(List_1_t3345875600 * value)
	{
		___m_Toggles_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Toggles_3), value);
	}
};

struct ToggleGroup_t1030026315_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::<>f__am$cache0
	Predicate_1_t2419724583 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<UnityEngine.UI.Toggle,System.Boolean> UnityEngine.UI.ToggleGroup::<>f__am$cache1
	Func_2_t2318645467 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2419724583 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2419724583 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2419724583 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t2318645467 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t2318645467 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t2318645467 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEGROUP_T1030026315_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef SELECTABLE_T1490392188_H
#define SELECTABLE_T1490392188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t1490392188  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1571958496  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2652774230  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1353336012  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t3244928895 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t2426225576 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2665681875 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Navigation_3)); }
	inline Navigation_t1571958496  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t1571958496 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t1571958496  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Colors_5)); }
	inline ColorBlock_t2652774230  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2652774230 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2652774230  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_SpriteState_6)); }
	inline SpriteState_t1353336012  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1353336012 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1353336012  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t3244928895 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t3244928895 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t3244928895 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_TargetGraphic_9)); }
	inline Graphic_t2426225576 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t2426225576 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t2426225576 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CanvasGroupCache_15)); }
	inline List_1_t2665681875 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t2665681875 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t2665681875 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t1490392188_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t859513320 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t1490392188_StaticFields, ___s_List_2)); }
	inline List_1_t859513320 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t859513320 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t859513320 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T1490392188_H
#ifndef SCROLLPOSITIONCONTROLLER_T563713522_H
#define SCROLLPOSITIONCONTROLLER_T563713522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollPositionController
struct  ScrollPositionController_t563713522  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollPositionController::viewport
	RectTransform_t3349966182 * ___viewport_2;
	// UnityEngine.UI.Extensions.ScrollPositionController/ScrollDirection UnityEngine.UI.Extensions.ScrollPositionController::directionOfRecognize
	int32_t ___directionOfRecognize_3;
	// UnityEngine.UI.Extensions.ScrollPositionController/MovementType UnityEngine.UI.Extensions.ScrollPositionController::movementType
	int32_t ___movementType_4;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::elasticity
	float ___elasticity_5;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::scrollSensitivity
	float ___scrollSensitivity_6;
	// System.Boolean UnityEngine.UI.Extensions.ScrollPositionController::inertia
	bool ___inertia_7;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::decelerationRate
	float ___decelerationRate_8;
	// UnityEngine.UI.Extensions.ScrollPositionController/Snap UnityEngine.UI.Extensions.ScrollPositionController::snap
	Snap_t1413478282  ___snap_9;
	// System.Int32 UnityEngine.UI.Extensions.ScrollPositionController::dataCount
	int32_t ___dataCount_10;
	// UnityEngine.UI.Extensions.ScrollPositionController/UpdatePositionEvent UnityEngine.UI.Extensions.ScrollPositionController::OnUpdatePosition
	UpdatePositionEvent_t4030317844 * ___OnUpdatePosition_11;
	// UnityEngine.UI.Extensions.ScrollPositionController/ItemSelectedEvent UnityEngine.UI.Extensions.ScrollPositionController::OnItemSelected
	ItemSelectedEvent_t2604723240 * ___OnItemSelected_12;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ScrollPositionController::pointerStartLocalPosition
	Vector2_t2243707579  ___pointerStartLocalPosition_13;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::dragStartScrollPosition
	float ___dragStartScrollPosition_14;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::currentScrollPosition
	float ___currentScrollPosition_15;
	// System.Boolean UnityEngine.UI.Extensions.ScrollPositionController::dragging
	bool ___dragging_16;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::velocity
	float ___velocity_17;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::prevScrollPosition
	float ___prevScrollPosition_18;
	// System.Boolean UnityEngine.UI.Extensions.ScrollPositionController::autoScrolling
	bool ___autoScrolling_19;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::autoScrollDuration
	float ___autoScrollDuration_20;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::autoScrollStartTime
	float ___autoScrollStartTime_21;
	// System.Single UnityEngine.UI.Extensions.ScrollPositionController::autoScrollPosition
	float ___autoScrollPosition_22;

public:
	inline static int32_t get_offset_of_viewport_2() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___viewport_2)); }
	inline RectTransform_t3349966182 * get_viewport_2() const { return ___viewport_2; }
	inline RectTransform_t3349966182 ** get_address_of_viewport_2() { return &___viewport_2; }
	inline void set_viewport_2(RectTransform_t3349966182 * value)
	{
		___viewport_2 = value;
		Il2CppCodeGenWriteBarrier((&___viewport_2), value);
	}

	inline static int32_t get_offset_of_directionOfRecognize_3() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___directionOfRecognize_3)); }
	inline int32_t get_directionOfRecognize_3() const { return ___directionOfRecognize_3; }
	inline int32_t* get_address_of_directionOfRecognize_3() { return &___directionOfRecognize_3; }
	inline void set_directionOfRecognize_3(int32_t value)
	{
		___directionOfRecognize_3 = value;
	}

	inline static int32_t get_offset_of_movementType_4() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___movementType_4)); }
	inline int32_t get_movementType_4() const { return ___movementType_4; }
	inline int32_t* get_address_of_movementType_4() { return &___movementType_4; }
	inline void set_movementType_4(int32_t value)
	{
		___movementType_4 = value;
	}

	inline static int32_t get_offset_of_elasticity_5() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___elasticity_5)); }
	inline float get_elasticity_5() const { return ___elasticity_5; }
	inline float* get_address_of_elasticity_5() { return &___elasticity_5; }
	inline void set_elasticity_5(float value)
	{
		___elasticity_5 = value;
	}

	inline static int32_t get_offset_of_scrollSensitivity_6() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___scrollSensitivity_6)); }
	inline float get_scrollSensitivity_6() const { return ___scrollSensitivity_6; }
	inline float* get_address_of_scrollSensitivity_6() { return &___scrollSensitivity_6; }
	inline void set_scrollSensitivity_6(float value)
	{
		___scrollSensitivity_6 = value;
	}

	inline static int32_t get_offset_of_inertia_7() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___inertia_7)); }
	inline bool get_inertia_7() const { return ___inertia_7; }
	inline bool* get_address_of_inertia_7() { return &___inertia_7; }
	inline void set_inertia_7(bool value)
	{
		___inertia_7 = value;
	}

	inline static int32_t get_offset_of_decelerationRate_8() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___decelerationRate_8)); }
	inline float get_decelerationRate_8() const { return ___decelerationRate_8; }
	inline float* get_address_of_decelerationRate_8() { return &___decelerationRate_8; }
	inline void set_decelerationRate_8(float value)
	{
		___decelerationRate_8 = value;
	}

	inline static int32_t get_offset_of_snap_9() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___snap_9)); }
	inline Snap_t1413478282  get_snap_9() const { return ___snap_9; }
	inline Snap_t1413478282 * get_address_of_snap_9() { return &___snap_9; }
	inline void set_snap_9(Snap_t1413478282  value)
	{
		___snap_9 = value;
	}

	inline static int32_t get_offset_of_dataCount_10() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___dataCount_10)); }
	inline int32_t get_dataCount_10() const { return ___dataCount_10; }
	inline int32_t* get_address_of_dataCount_10() { return &___dataCount_10; }
	inline void set_dataCount_10(int32_t value)
	{
		___dataCount_10 = value;
	}

	inline static int32_t get_offset_of_OnUpdatePosition_11() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___OnUpdatePosition_11)); }
	inline UpdatePositionEvent_t4030317844 * get_OnUpdatePosition_11() const { return ___OnUpdatePosition_11; }
	inline UpdatePositionEvent_t4030317844 ** get_address_of_OnUpdatePosition_11() { return &___OnUpdatePosition_11; }
	inline void set_OnUpdatePosition_11(UpdatePositionEvent_t4030317844 * value)
	{
		___OnUpdatePosition_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdatePosition_11), value);
	}

	inline static int32_t get_offset_of_OnItemSelected_12() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___OnItemSelected_12)); }
	inline ItemSelectedEvent_t2604723240 * get_OnItemSelected_12() const { return ___OnItemSelected_12; }
	inline ItemSelectedEvent_t2604723240 ** get_address_of_OnItemSelected_12() { return &___OnItemSelected_12; }
	inline void set_OnItemSelected_12(ItemSelectedEvent_t2604723240 * value)
	{
		___OnItemSelected_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnItemSelected_12), value);
	}

	inline static int32_t get_offset_of_pointerStartLocalPosition_13() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___pointerStartLocalPosition_13)); }
	inline Vector2_t2243707579  get_pointerStartLocalPosition_13() const { return ___pointerStartLocalPosition_13; }
	inline Vector2_t2243707579 * get_address_of_pointerStartLocalPosition_13() { return &___pointerStartLocalPosition_13; }
	inline void set_pointerStartLocalPosition_13(Vector2_t2243707579  value)
	{
		___pointerStartLocalPosition_13 = value;
	}

	inline static int32_t get_offset_of_dragStartScrollPosition_14() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___dragStartScrollPosition_14)); }
	inline float get_dragStartScrollPosition_14() const { return ___dragStartScrollPosition_14; }
	inline float* get_address_of_dragStartScrollPosition_14() { return &___dragStartScrollPosition_14; }
	inline void set_dragStartScrollPosition_14(float value)
	{
		___dragStartScrollPosition_14 = value;
	}

	inline static int32_t get_offset_of_currentScrollPosition_15() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___currentScrollPosition_15)); }
	inline float get_currentScrollPosition_15() const { return ___currentScrollPosition_15; }
	inline float* get_address_of_currentScrollPosition_15() { return &___currentScrollPosition_15; }
	inline void set_currentScrollPosition_15(float value)
	{
		___currentScrollPosition_15 = value;
	}

	inline static int32_t get_offset_of_dragging_16() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___dragging_16)); }
	inline bool get_dragging_16() const { return ___dragging_16; }
	inline bool* get_address_of_dragging_16() { return &___dragging_16; }
	inline void set_dragging_16(bool value)
	{
		___dragging_16 = value;
	}

	inline static int32_t get_offset_of_velocity_17() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___velocity_17)); }
	inline float get_velocity_17() const { return ___velocity_17; }
	inline float* get_address_of_velocity_17() { return &___velocity_17; }
	inline void set_velocity_17(float value)
	{
		___velocity_17 = value;
	}

	inline static int32_t get_offset_of_prevScrollPosition_18() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___prevScrollPosition_18)); }
	inline float get_prevScrollPosition_18() const { return ___prevScrollPosition_18; }
	inline float* get_address_of_prevScrollPosition_18() { return &___prevScrollPosition_18; }
	inline void set_prevScrollPosition_18(float value)
	{
		___prevScrollPosition_18 = value;
	}

	inline static int32_t get_offset_of_autoScrolling_19() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___autoScrolling_19)); }
	inline bool get_autoScrolling_19() const { return ___autoScrolling_19; }
	inline bool* get_address_of_autoScrolling_19() { return &___autoScrolling_19; }
	inline void set_autoScrolling_19(bool value)
	{
		___autoScrolling_19 = value;
	}

	inline static int32_t get_offset_of_autoScrollDuration_20() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___autoScrollDuration_20)); }
	inline float get_autoScrollDuration_20() const { return ___autoScrollDuration_20; }
	inline float* get_address_of_autoScrollDuration_20() { return &___autoScrollDuration_20; }
	inline void set_autoScrollDuration_20(float value)
	{
		___autoScrollDuration_20 = value;
	}

	inline static int32_t get_offset_of_autoScrollStartTime_21() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___autoScrollStartTime_21)); }
	inline float get_autoScrollStartTime_21() const { return ___autoScrollStartTime_21; }
	inline float* get_address_of_autoScrollStartTime_21() { return &___autoScrollStartTime_21; }
	inline void set_autoScrollStartTime_21(float value)
	{
		___autoScrollStartTime_21 = value;
	}

	inline static int32_t get_offset_of_autoScrollPosition_22() { return static_cast<int32_t>(offsetof(ScrollPositionController_t563713522, ___autoScrollPosition_22)); }
	inline float get_autoScrollPosition_22() const { return ___autoScrollPosition_22; }
	inline float* get_address_of_autoScrollPosition_22() { return &___autoScrollPosition_22; }
	inline void set_autoScrollPosition_22(float value)
	{
		___autoScrollPosition_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLPOSITIONCONTROLLER_T563713522_H
#ifndef TILESIZEFITTER_T2558755467_H
#define TILESIZEFITTER_T2558755467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TileSizeFitter
struct  TileSizeFitter_t2558755467  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TileSizeFitter::m_Border
	Vector2_t2243707579  ___m_Border_2;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TileSizeFitter::m_TileSize
	Vector2_t2243707579  ___m_TileSize_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.TileSizeFitter::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Extensions.TileSizeFitter::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_Border_2() { return static_cast<int32_t>(offsetof(TileSizeFitter_t2558755467, ___m_Border_2)); }
	inline Vector2_t2243707579  get_m_Border_2() const { return ___m_Border_2; }
	inline Vector2_t2243707579 * get_address_of_m_Border_2() { return &___m_Border_2; }
	inline void set_m_Border_2(Vector2_t2243707579  value)
	{
		___m_Border_2 = value;
	}

	inline static int32_t get_offset_of_m_TileSize_3() { return static_cast<int32_t>(offsetof(TileSizeFitter_t2558755467, ___m_TileSize_3)); }
	inline Vector2_t2243707579  get_m_TileSize_3() const { return ___m_TileSize_3; }
	inline Vector2_t2243707579 * get_address_of_m_TileSize_3() { return &___m_TileSize_3; }
	inline void set_m_TileSize_3(Vector2_t2243707579  value)
	{
		___m_TileSize_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(TileSizeFitter_t2558755467, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(TileSizeFitter_t2558755467, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILESIZEFITTER_T2558755467_H
#ifndef HORIZONTALSCROLLSNAP_T3431010905_H
#define HORIZONTALSCROLLSNAP_T3431010905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct  HorizontalScrollSnap_t3431010905  : public ScrollSnapBase_t805675194
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALSCROLLSNAP_T3431010905_H
#ifndef EXTENSIONSTOGGLEGROUP_T1289496689_H
#define EXTENSIONSTOGGLEGROUP_T1289496689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ExtensionsToggleGroup
struct  ExtensionsToggleGroup_t1289496689  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean UnityEngine.UI.ExtensionsToggleGroup::m_AllowSwitchOff
	bool ___m_AllowSwitchOff_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ExtensionsToggle> UnityEngine.UI.ExtensionsToggleGroup::m_Toggles
	List_1_t2009014410 * ___m_Toggles_3;
	// UnityEngine.UI.ExtensionsToggleGroup/ToggleGroupEvent UnityEngine.UI.ExtensionsToggleGroup::onToggleGroupChanged
	ToggleGroupEvent_t1021581118 * ___onToggleGroupChanged_4;
	// UnityEngine.UI.ExtensionsToggleGroup/ToggleGroupEvent UnityEngine.UI.ExtensionsToggleGroup::onToggleGroupToggleChanged
	ToggleGroupEvent_t1021581118 * ___onToggleGroupToggleChanged_5;
	// UnityEngine.UI.ExtensionsToggle UnityEngine.UI.ExtensionsToggleGroup::<SelectedToggle>k__BackingField
	ExtensionsToggle_t2639893278 * ___U3CSelectedToggleU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_AllowSwitchOff_2() { return static_cast<int32_t>(offsetof(ExtensionsToggleGroup_t1289496689, ___m_AllowSwitchOff_2)); }
	inline bool get_m_AllowSwitchOff_2() const { return ___m_AllowSwitchOff_2; }
	inline bool* get_address_of_m_AllowSwitchOff_2() { return &___m_AllowSwitchOff_2; }
	inline void set_m_AllowSwitchOff_2(bool value)
	{
		___m_AllowSwitchOff_2 = value;
	}

	inline static int32_t get_offset_of_m_Toggles_3() { return static_cast<int32_t>(offsetof(ExtensionsToggleGroup_t1289496689, ___m_Toggles_3)); }
	inline List_1_t2009014410 * get_m_Toggles_3() const { return ___m_Toggles_3; }
	inline List_1_t2009014410 ** get_address_of_m_Toggles_3() { return &___m_Toggles_3; }
	inline void set_m_Toggles_3(List_1_t2009014410 * value)
	{
		___m_Toggles_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Toggles_3), value);
	}

	inline static int32_t get_offset_of_onToggleGroupChanged_4() { return static_cast<int32_t>(offsetof(ExtensionsToggleGroup_t1289496689, ___onToggleGroupChanged_4)); }
	inline ToggleGroupEvent_t1021581118 * get_onToggleGroupChanged_4() const { return ___onToggleGroupChanged_4; }
	inline ToggleGroupEvent_t1021581118 ** get_address_of_onToggleGroupChanged_4() { return &___onToggleGroupChanged_4; }
	inline void set_onToggleGroupChanged_4(ToggleGroupEvent_t1021581118 * value)
	{
		___onToggleGroupChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___onToggleGroupChanged_4), value);
	}

	inline static int32_t get_offset_of_onToggleGroupToggleChanged_5() { return static_cast<int32_t>(offsetof(ExtensionsToggleGroup_t1289496689, ___onToggleGroupToggleChanged_5)); }
	inline ToggleGroupEvent_t1021581118 * get_onToggleGroupToggleChanged_5() const { return ___onToggleGroupToggleChanged_5; }
	inline ToggleGroupEvent_t1021581118 ** get_address_of_onToggleGroupToggleChanged_5() { return &___onToggleGroupToggleChanged_5; }
	inline void set_onToggleGroupToggleChanged_5(ToggleGroupEvent_t1021581118 * value)
	{
		___onToggleGroupToggleChanged_5 = value;
		Il2CppCodeGenWriteBarrier((&___onToggleGroupToggleChanged_5), value);
	}

	inline static int32_t get_offset_of_U3CSelectedToggleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ExtensionsToggleGroup_t1289496689, ___U3CSelectedToggleU3Ek__BackingField_6)); }
	inline ExtensionsToggle_t2639893278 * get_U3CSelectedToggleU3Ek__BackingField_6() const { return ___U3CSelectedToggleU3Ek__BackingField_6; }
	inline ExtensionsToggle_t2639893278 ** get_address_of_U3CSelectedToggleU3Ek__BackingField_6() { return &___U3CSelectedToggleU3Ek__BackingField_6; }
	inline void set_U3CSelectedToggleU3Ek__BackingField_6(ExtensionsToggle_t2639893278 * value)
	{
		___U3CSelectedToggleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedToggleU3Ek__BackingField_6), value);
	}
};

struct ExtensionsToggleGroup_t1289496689_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.ExtensionsToggle> UnityEngine.UI.ExtensionsToggleGroup::<>f__am$cache0
	Predicate_1_t1082863393 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<UnityEngine.UI.ExtensionsToggle,System.Boolean> UnityEngine.UI.ExtensionsToggleGroup::<>f__am$cache1
	Func_2_t1387565497 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(ExtensionsToggleGroup_t1289496689_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Predicate_1_t1082863393 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Predicate_1_t1082863393 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Predicate_1_t1082863393 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(ExtensionsToggleGroup_t1289496689_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t1387565497 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t1387565497 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t1387565497 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONSTOGGLEGROUP_T1289496689_H
#ifndef BASEMESHEFFECT_T1728560551_H
#define BASEMESHEFFECT_T1728560551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t1728560551  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t1728560551, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T1728560551_H
#ifndef LAYOUTGROUP_T3962498969_H
#define LAYOUTGROUP_T3962498969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t3962498969  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t3387826427 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2243707579  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2243707579  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2243707579  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t2719087314 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Padding_2)); }
	inline RectOffset_t3387826427 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t3387826427 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t3387826427 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalMinSize_6)); }
	inline Vector2_t2243707579  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2243707579 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2243707579  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2243707579  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2243707579 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2243707579  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2243707579  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2243707579 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2243707579  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_RectChildren_9)); }
	inline List_1_t2719087314 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t2719087314 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t2719087314 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T3962498969_H
#ifndef BASEINPUTMODULE_T1295781545_H
#define BASEINPUTMODULE_T1295781545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t1295781545  : public UIBehaviour_t3960014691
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t3685274804 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t1524870173 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t3466835263 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t2681005625 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t621514313 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t621514313 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_RaycastResultCache_2)); }
	inline List_1_t3685274804 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t3685274804 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t3685274804 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_AxisEventData_3)); }
	inline AxisEventData_t1524870173 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t1524870173 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t1524870173 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_EventSystem_4)); }
	inline EventSystem_t3466835263 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t3466835263 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t3466835263 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_BaseEventData_5)); }
	inline BaseEventData_t2681005625 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t2681005625 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t2681005625 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_InputOverride_6)); }
	inline BaseInput_t621514313 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t621514313 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t621514313 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_DefaultInput_7)); }
	inline BaseInput_t621514313 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t621514313 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t621514313 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T1295781545_H
#ifndef VERTICALSCROLLSNAP_T4045580255_H
#define VERTICALSCROLLSNAP_T4045580255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.VerticalScrollSnap
struct  VerticalScrollSnap_t4045580255  : public ScrollSnapBase_t805675194
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALSCROLLSNAP_T4045580255_H
#ifndef MONOSPACING_T901932972_H
#define MONOSPACING_T901932972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.MonoSpacing
struct  MonoSpacing_t901932972  : public BaseMeshEffect_t1728560551
{
public:
	// System.Single UnityEngine.UI.Extensions.MonoSpacing::m_spacing
	float ___m_spacing_3;
	// System.Single UnityEngine.UI.Extensions.MonoSpacing::HalfCharWidth
	float ___HalfCharWidth_4;
	// System.Boolean UnityEngine.UI.Extensions.MonoSpacing::UseHalfCharWidth
	bool ___UseHalfCharWidth_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.MonoSpacing::rectTransform
	RectTransform_t3349966182 * ___rectTransform_6;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.MonoSpacing::text
	Text_t356221433 * ___text_7;

public:
	inline static int32_t get_offset_of_m_spacing_3() { return static_cast<int32_t>(offsetof(MonoSpacing_t901932972, ___m_spacing_3)); }
	inline float get_m_spacing_3() const { return ___m_spacing_3; }
	inline float* get_address_of_m_spacing_3() { return &___m_spacing_3; }
	inline void set_m_spacing_3(float value)
	{
		___m_spacing_3 = value;
	}

	inline static int32_t get_offset_of_HalfCharWidth_4() { return static_cast<int32_t>(offsetof(MonoSpacing_t901932972, ___HalfCharWidth_4)); }
	inline float get_HalfCharWidth_4() const { return ___HalfCharWidth_4; }
	inline float* get_address_of_HalfCharWidth_4() { return &___HalfCharWidth_4; }
	inline void set_HalfCharWidth_4(float value)
	{
		___HalfCharWidth_4 = value;
	}

	inline static int32_t get_offset_of_UseHalfCharWidth_5() { return static_cast<int32_t>(offsetof(MonoSpacing_t901932972, ___UseHalfCharWidth_5)); }
	inline bool get_UseHalfCharWidth_5() const { return ___UseHalfCharWidth_5; }
	inline bool* get_address_of_UseHalfCharWidth_5() { return &___UseHalfCharWidth_5; }
	inline void set_UseHalfCharWidth_5(bool value)
	{
		___UseHalfCharWidth_5 = value;
	}

	inline static int32_t get_offset_of_rectTransform_6() { return static_cast<int32_t>(offsetof(MonoSpacing_t901932972, ___rectTransform_6)); }
	inline RectTransform_t3349966182 * get_rectTransform_6() const { return ___rectTransform_6; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_6() { return &___rectTransform_6; }
	inline void set_rectTransform_6(RectTransform_t3349966182 * value)
	{
		___rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_6), value);
	}

	inline static int32_t get_offset_of_text_7() { return static_cast<int32_t>(offsetof(MonoSpacing_t901932972, ___text_7)); }
	inline Text_t356221433 * get_text_7() const { return ___text_7; }
	inline Text_t356221433 ** get_address_of_text_7() { return &___text_7; }
	inline void set_text_7(Text_t356221433 * value)
	{
		___text_7 = value;
		Il2CppCodeGenWriteBarrier((&___text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSPACING_T901932972_H
#ifndef TABLELAYOUTGROUP_T2874982773_H
#define TABLELAYOUTGROUP_T2874982773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TableLayoutGroup
struct  TableLayoutGroup_t2874982773  : public LayoutGroup_t3962498969
{
public:
	// UnityEngine.UI.Extensions.TableLayoutGroup/Corner UnityEngine.UI.Extensions.TableLayoutGroup::startCorner
	int32_t ___startCorner_10;
	// System.Single[] UnityEngine.UI.Extensions.TableLayoutGroup::columnWidths
	SingleU5BU5D_t577127397* ___columnWidths_11;
	// System.Single UnityEngine.UI.Extensions.TableLayoutGroup::minimumRowHeight
	float ___minimumRowHeight_12;
	// System.Boolean UnityEngine.UI.Extensions.TableLayoutGroup::flexibleRowHeight
	bool ___flexibleRowHeight_13;
	// System.Single UnityEngine.UI.Extensions.TableLayoutGroup::columnSpacing
	float ___columnSpacing_14;
	// System.Single UnityEngine.UI.Extensions.TableLayoutGroup::rowSpacing
	float ___rowSpacing_15;
	// System.Single[] UnityEngine.UI.Extensions.TableLayoutGroup::preferredRowHeights
	SingleU5BU5D_t577127397* ___preferredRowHeights_16;

public:
	inline static int32_t get_offset_of_startCorner_10() { return static_cast<int32_t>(offsetof(TableLayoutGroup_t2874982773, ___startCorner_10)); }
	inline int32_t get_startCorner_10() const { return ___startCorner_10; }
	inline int32_t* get_address_of_startCorner_10() { return &___startCorner_10; }
	inline void set_startCorner_10(int32_t value)
	{
		___startCorner_10 = value;
	}

	inline static int32_t get_offset_of_columnWidths_11() { return static_cast<int32_t>(offsetof(TableLayoutGroup_t2874982773, ___columnWidths_11)); }
	inline SingleU5BU5D_t577127397* get_columnWidths_11() const { return ___columnWidths_11; }
	inline SingleU5BU5D_t577127397** get_address_of_columnWidths_11() { return &___columnWidths_11; }
	inline void set_columnWidths_11(SingleU5BU5D_t577127397* value)
	{
		___columnWidths_11 = value;
		Il2CppCodeGenWriteBarrier((&___columnWidths_11), value);
	}

	inline static int32_t get_offset_of_minimumRowHeight_12() { return static_cast<int32_t>(offsetof(TableLayoutGroup_t2874982773, ___minimumRowHeight_12)); }
	inline float get_minimumRowHeight_12() const { return ___minimumRowHeight_12; }
	inline float* get_address_of_minimumRowHeight_12() { return &___minimumRowHeight_12; }
	inline void set_minimumRowHeight_12(float value)
	{
		___minimumRowHeight_12 = value;
	}

	inline static int32_t get_offset_of_flexibleRowHeight_13() { return static_cast<int32_t>(offsetof(TableLayoutGroup_t2874982773, ___flexibleRowHeight_13)); }
	inline bool get_flexibleRowHeight_13() const { return ___flexibleRowHeight_13; }
	inline bool* get_address_of_flexibleRowHeight_13() { return &___flexibleRowHeight_13; }
	inline void set_flexibleRowHeight_13(bool value)
	{
		___flexibleRowHeight_13 = value;
	}

	inline static int32_t get_offset_of_columnSpacing_14() { return static_cast<int32_t>(offsetof(TableLayoutGroup_t2874982773, ___columnSpacing_14)); }
	inline float get_columnSpacing_14() const { return ___columnSpacing_14; }
	inline float* get_address_of_columnSpacing_14() { return &___columnSpacing_14; }
	inline void set_columnSpacing_14(float value)
	{
		___columnSpacing_14 = value;
	}

	inline static int32_t get_offset_of_rowSpacing_15() { return static_cast<int32_t>(offsetof(TableLayoutGroup_t2874982773, ___rowSpacing_15)); }
	inline float get_rowSpacing_15() const { return ___rowSpacing_15; }
	inline float* get_address_of_rowSpacing_15() { return &___rowSpacing_15; }
	inline void set_rowSpacing_15(float value)
	{
		___rowSpacing_15 = value;
	}

	inline static int32_t get_offset_of_preferredRowHeights_16() { return static_cast<int32_t>(offsetof(TableLayoutGroup_t2874982773, ___preferredRowHeights_16)); }
	inline SingleU5BU5D_t577127397* get_preferredRowHeights_16() const { return ___preferredRowHeights_16; }
	inline SingleU5BU5D_t577127397** get_address_of_preferredRowHeights_16() { return &___preferredRowHeights_16; }
	inline void set_preferredRowHeights_16(SingleU5BU5D_t577127397* value)
	{
		___preferredRowHeights_16 = value;
		Il2CppCodeGenWriteBarrier((&___preferredRowHeights_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLELAYOUTGROUP_T2874982773_H
#ifndef RADIALLAYOUT_T988531717_H
#define RADIALLAYOUT_T988531717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RadialLayout
struct  RadialLayout_t988531717  : public LayoutGroup_t3962498969
{
public:
	// System.Single UnityEngine.UI.Extensions.RadialLayout::fDistance
	float ___fDistance_10;
	// System.Single UnityEngine.UI.Extensions.RadialLayout::MinAngle
	float ___MinAngle_11;
	// System.Single UnityEngine.UI.Extensions.RadialLayout::MaxAngle
	float ___MaxAngle_12;
	// System.Single UnityEngine.UI.Extensions.RadialLayout::StartAngle
	float ___StartAngle_13;

public:
	inline static int32_t get_offset_of_fDistance_10() { return static_cast<int32_t>(offsetof(RadialLayout_t988531717, ___fDistance_10)); }
	inline float get_fDistance_10() const { return ___fDistance_10; }
	inline float* get_address_of_fDistance_10() { return &___fDistance_10; }
	inline void set_fDistance_10(float value)
	{
		___fDistance_10 = value;
	}

	inline static int32_t get_offset_of_MinAngle_11() { return static_cast<int32_t>(offsetof(RadialLayout_t988531717, ___MinAngle_11)); }
	inline float get_MinAngle_11() const { return ___MinAngle_11; }
	inline float* get_address_of_MinAngle_11() { return &___MinAngle_11; }
	inline void set_MinAngle_11(float value)
	{
		___MinAngle_11 = value;
	}

	inline static int32_t get_offset_of_MaxAngle_12() { return static_cast<int32_t>(offsetof(RadialLayout_t988531717, ___MaxAngle_12)); }
	inline float get_MaxAngle_12() const { return ___MaxAngle_12; }
	inline float* get_address_of_MaxAngle_12() { return &___MaxAngle_12; }
	inline void set_MaxAngle_12(float value)
	{
		___MaxAngle_12 = value;
	}

	inline static int32_t get_offset_of_StartAngle_13() { return static_cast<int32_t>(offsetof(RadialLayout_t988531717, ___StartAngle_13)); }
	inline float get_StartAngle_13() const { return ___StartAngle_13; }
	inline float* get_address_of_StartAngle_13() { return &___StartAngle_13; }
	inline void set_StartAngle_13(float value)
	{
		___StartAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALLAYOUT_T988531717_H
#ifndef UIFLIPPABLE_T725564421_H
#define UIFLIPPABLE_T725564421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIFlippable
struct  UIFlippable_t725564421  : public BaseMeshEffect_t1728560551
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UIFlippable::m_Horizontal
	bool ___m_Horizontal_3;
	// System.Boolean UnityEngine.UI.Extensions.UIFlippable::m_Veritical
	bool ___m_Veritical_4;

public:
	inline static int32_t get_offset_of_m_Horizontal_3() { return static_cast<int32_t>(offsetof(UIFlippable_t725564421, ___m_Horizontal_3)); }
	inline bool get_m_Horizontal_3() const { return ___m_Horizontal_3; }
	inline bool* get_address_of_m_Horizontal_3() { return &___m_Horizontal_3; }
	inline void set_m_Horizontal_3(bool value)
	{
		___m_Horizontal_3 = value;
	}

	inline static int32_t get_offset_of_m_Veritical_4() { return static_cast<int32_t>(offsetof(UIFlippable_t725564421, ___m_Veritical_4)); }
	inline bool get_m_Veritical_4() const { return ___m_Veritical_4; }
	inline bool* get_address_of_m_Veritical_4() { return &___m_Veritical_4; }
	inline void set_m_Veritical_4(bool value)
	{
		___m_Veritical_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFLIPPABLE_T725564421_H
#ifndef NICEROUTLINE_T710191709_H
#define NICEROUTLINE_T710191709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.NicerOutline
struct  NicerOutline_t710191709  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.NicerOutline::m_EffectColor
	Color_t2020392075  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.NicerOutline::m_EffectDistance
	Vector2_t2243707579  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Extensions.NicerOutline::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(NicerOutline_t710191709, ___m_EffectColor_3)); }
	inline Color_t2020392075  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2020392075 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2020392075  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(NicerOutline_t710191709, ___m_EffectDistance_4)); }
	inline Vector2_t2243707579  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2243707579 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2243707579  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(NicerOutline_t710191709, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NICEROUTLINE_T710191709_H
#ifndef GAMEPADINPUTMODULE_T1983499109_H
#define GAMEPADINPUTMODULE_T1983499109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.GamePadInputModule
struct  GamePadInputModule_t1983499109  : public BaseInputModule_t1295781545
{
public:
	// System.Single UnityEngine.EventSystems.GamePadInputModule::m_PrevActionTime
	float ___m_PrevActionTime_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.GamePadInputModule::m_LastMoveVector
	Vector2_t2243707579  ___m_LastMoveVector_9;
	// System.Int32 UnityEngine.EventSystems.GamePadInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_10;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_11;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_12;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_13;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_CancelButton
	String_t* ___m_CancelButton_14;
	// System.Single UnityEngine.EventSystems.GamePadInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_15;
	// System.Single UnityEngine.EventSystems.GamePadInputModule::m_RepeatDelay
	float ___m_RepeatDelay_16;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_8() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_PrevActionTime_8)); }
	inline float get_m_PrevActionTime_8() const { return ___m_PrevActionTime_8; }
	inline float* get_address_of_m_PrevActionTime_8() { return &___m_PrevActionTime_8; }
	inline void set_m_PrevActionTime_8(float value)
	{
		___m_PrevActionTime_8 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_9() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_LastMoveVector_9)); }
	inline Vector2_t2243707579  get_m_LastMoveVector_9() const { return ___m_LastMoveVector_9; }
	inline Vector2_t2243707579 * get_address_of_m_LastMoveVector_9() { return &___m_LastMoveVector_9; }
	inline void set_m_LastMoveVector_9(Vector2_t2243707579  value)
	{
		___m_LastMoveVector_9 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_10() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_ConsecutiveMoveCount_10)); }
	inline int32_t get_m_ConsecutiveMoveCount_10() const { return ___m_ConsecutiveMoveCount_10; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_10() { return &___m_ConsecutiveMoveCount_10; }
	inline void set_m_ConsecutiveMoveCount_10(int32_t value)
	{
		___m_ConsecutiveMoveCount_10 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_11() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_HorizontalAxis_11)); }
	inline String_t* get_m_HorizontalAxis_11() const { return ___m_HorizontalAxis_11; }
	inline String_t** get_address_of_m_HorizontalAxis_11() { return &___m_HorizontalAxis_11; }
	inline void set_m_HorizontalAxis_11(String_t* value)
	{
		___m_HorizontalAxis_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_12() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_VerticalAxis_12)); }
	inline String_t* get_m_VerticalAxis_12() const { return ___m_VerticalAxis_12; }
	inline String_t** get_address_of_m_VerticalAxis_12() { return &___m_VerticalAxis_12; }
	inline void set_m_VerticalAxis_12(String_t* value)
	{
		___m_VerticalAxis_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_12), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_13() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_SubmitButton_13)); }
	inline String_t* get_m_SubmitButton_13() const { return ___m_SubmitButton_13; }
	inline String_t** get_address_of_m_SubmitButton_13() { return &___m_SubmitButton_13; }
	inline void set_m_SubmitButton_13(String_t* value)
	{
		___m_SubmitButton_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_13), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_14() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_CancelButton_14)); }
	inline String_t* get_m_CancelButton_14() const { return ___m_CancelButton_14; }
	inline String_t** get_address_of_m_CancelButton_14() { return &___m_CancelButton_14; }
	inline void set_m_CancelButton_14(String_t* value)
	{
		___m_CancelButton_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_14), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_15() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_InputActionsPerSecond_15)); }
	inline float get_m_InputActionsPerSecond_15() const { return ___m_InputActionsPerSecond_15; }
	inline float* get_address_of_m_InputActionsPerSecond_15() { return &___m_InputActionsPerSecond_15; }
	inline void set_m_InputActionsPerSecond_15(float value)
	{
		___m_InputActionsPerSecond_15 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_16() { return static_cast<int32_t>(offsetof(GamePadInputModule_t1983499109, ___m_RepeatDelay_16)); }
	inline float get_m_RepeatDelay_16() const { return ___m_RepeatDelay_16; }
	inline float* get_address_of_m_RepeatDelay_16() { return &___m_RepeatDelay_16; }
	inline void set_m_RepeatDelay_16(float value)
	{
		___m_RepeatDelay_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPADINPUTMODULE_T1983499109_H
#ifndef FLOWLAYOUTGROUP_T3216283979_H
#define FLOWLAYOUTGROUP_T3216283979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FlowLayoutGroup
struct  FlowLayoutGroup_t3216283979  : public LayoutGroup_t3962498969
{
public:
	// System.Single UnityEngine.UI.Extensions.FlowLayoutGroup::SpacingX
	float ___SpacingX_10;
	// System.Single UnityEngine.UI.Extensions.FlowLayoutGroup::SpacingY
	float ___SpacingY_11;
	// System.Boolean UnityEngine.UI.Extensions.FlowLayoutGroup::ExpandHorizontalSpacing
	bool ___ExpandHorizontalSpacing_12;
	// System.Boolean UnityEngine.UI.Extensions.FlowLayoutGroup::ChildForceExpandWidth
	bool ___ChildForceExpandWidth_13;
	// System.Boolean UnityEngine.UI.Extensions.FlowLayoutGroup::ChildForceExpandHeight
	bool ___ChildForceExpandHeight_14;
	// System.Single UnityEngine.UI.Extensions.FlowLayoutGroup::_layoutHeight
	float ____layoutHeight_15;
	// System.Collections.Generic.IList`1<UnityEngine.RectTransform> UnityEngine.UI.Extensions.FlowLayoutGroup::_rowList
	RuntimeObject* ____rowList_16;

public:
	inline static int32_t get_offset_of_SpacingX_10() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t3216283979, ___SpacingX_10)); }
	inline float get_SpacingX_10() const { return ___SpacingX_10; }
	inline float* get_address_of_SpacingX_10() { return &___SpacingX_10; }
	inline void set_SpacingX_10(float value)
	{
		___SpacingX_10 = value;
	}

	inline static int32_t get_offset_of_SpacingY_11() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t3216283979, ___SpacingY_11)); }
	inline float get_SpacingY_11() const { return ___SpacingY_11; }
	inline float* get_address_of_SpacingY_11() { return &___SpacingY_11; }
	inline void set_SpacingY_11(float value)
	{
		___SpacingY_11 = value;
	}

	inline static int32_t get_offset_of_ExpandHorizontalSpacing_12() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t3216283979, ___ExpandHorizontalSpacing_12)); }
	inline bool get_ExpandHorizontalSpacing_12() const { return ___ExpandHorizontalSpacing_12; }
	inline bool* get_address_of_ExpandHorizontalSpacing_12() { return &___ExpandHorizontalSpacing_12; }
	inline void set_ExpandHorizontalSpacing_12(bool value)
	{
		___ExpandHorizontalSpacing_12 = value;
	}

	inline static int32_t get_offset_of_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t3216283979, ___ChildForceExpandWidth_13)); }
	inline bool get_ChildForceExpandWidth_13() const { return ___ChildForceExpandWidth_13; }
	inline bool* get_address_of_ChildForceExpandWidth_13() { return &___ChildForceExpandWidth_13; }
	inline void set_ChildForceExpandWidth_13(bool value)
	{
		___ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_ChildForceExpandHeight_14() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t3216283979, ___ChildForceExpandHeight_14)); }
	inline bool get_ChildForceExpandHeight_14() const { return ___ChildForceExpandHeight_14; }
	inline bool* get_address_of_ChildForceExpandHeight_14() { return &___ChildForceExpandHeight_14; }
	inline void set_ChildForceExpandHeight_14(bool value)
	{
		___ChildForceExpandHeight_14 = value;
	}

	inline static int32_t get_offset_of__layoutHeight_15() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t3216283979, ____layoutHeight_15)); }
	inline float get__layoutHeight_15() const { return ____layoutHeight_15; }
	inline float* get_address_of__layoutHeight_15() { return &____layoutHeight_15; }
	inline void set__layoutHeight_15(float value)
	{
		____layoutHeight_15 = value;
	}

	inline static int32_t get_offset_of__rowList_16() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t3216283979, ____rowList_16)); }
	inline RuntimeObject* get__rowList_16() const { return ____rowList_16; }
	inline RuntimeObject** get_address_of__rowList_16() { return &____rowList_16; }
	inline void set__rowList_16(RuntimeObject* value)
	{
		____rowList_16 = value;
		Il2CppCodeGenWriteBarrier((&____rowList_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOWLAYOUTGROUP_T3216283979_H
#ifndef CURVEDLAYOUT_T31468887_H
#define CURVEDLAYOUT_T31468887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CurvedLayout
struct  CurvedLayout_t31468887  : public LayoutGroup_t3962498969
{
public:
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.CurvedLayout::CurveOffset
	Vector3_t2243707580  ___CurveOffset_10;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.CurvedLayout::itemAxis
	Vector3_t2243707580  ___itemAxis_11;
	// System.Single UnityEngine.UI.Extensions.CurvedLayout::itemSize
	float ___itemSize_12;
	// System.Single UnityEngine.UI.Extensions.CurvedLayout::centerpoint
	float ___centerpoint_13;

public:
	inline static int32_t get_offset_of_CurveOffset_10() { return static_cast<int32_t>(offsetof(CurvedLayout_t31468887, ___CurveOffset_10)); }
	inline Vector3_t2243707580  get_CurveOffset_10() const { return ___CurveOffset_10; }
	inline Vector3_t2243707580 * get_address_of_CurveOffset_10() { return &___CurveOffset_10; }
	inline void set_CurveOffset_10(Vector3_t2243707580  value)
	{
		___CurveOffset_10 = value;
	}

	inline static int32_t get_offset_of_itemAxis_11() { return static_cast<int32_t>(offsetof(CurvedLayout_t31468887, ___itemAxis_11)); }
	inline Vector3_t2243707580  get_itemAxis_11() const { return ___itemAxis_11; }
	inline Vector3_t2243707580 * get_address_of_itemAxis_11() { return &___itemAxis_11; }
	inline void set_itemAxis_11(Vector3_t2243707580  value)
	{
		___itemAxis_11 = value;
	}

	inline static int32_t get_offset_of_itemSize_12() { return static_cast<int32_t>(offsetof(CurvedLayout_t31468887, ___itemSize_12)); }
	inline float get_itemSize_12() const { return ___itemSize_12; }
	inline float* get_address_of_itemSize_12() { return &___itemSize_12; }
	inline void set_itemSize_12(float value)
	{
		___itemSize_12 = value;
	}

	inline static int32_t get_offset_of_centerpoint_13() { return static_cast<int32_t>(offsetof(CurvedLayout_t31468887, ___centerpoint_13)); }
	inline float get_centerpoint_13() const { return ___centerpoint_13; }
	inline float* get_address_of_centerpoint_13() { return &___centerpoint_13; }
	inline void set_centerpoint_13(float value)
	{
		___centerpoint_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEDLAYOUT_T31468887_H
#ifndef PAGINATIONMANAGER_T1455408585_H
#define PAGINATIONMANAGER_T1455408585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.PaginationManager
struct  PaginationManager_t1455408585  : public ToggleGroup_t1030026315
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> UnityEngine.UI.Extensions.PaginationManager::m_PaginationChildren
	List_1_t3345875600 * ___m_PaginationChildren_6;
	// UnityEngine.UI.Extensions.ScrollSnapBase UnityEngine.UI.Extensions.PaginationManager::scrollSnap
	ScrollSnapBase_t805675194 * ___scrollSnap_7;
	// System.Boolean UnityEngine.UI.Extensions.PaginationManager::isAClick
	bool ___isAClick_8;

public:
	inline static int32_t get_offset_of_m_PaginationChildren_6() { return static_cast<int32_t>(offsetof(PaginationManager_t1455408585, ___m_PaginationChildren_6)); }
	inline List_1_t3345875600 * get_m_PaginationChildren_6() const { return ___m_PaginationChildren_6; }
	inline List_1_t3345875600 ** get_address_of_m_PaginationChildren_6() { return &___m_PaginationChildren_6; }
	inline void set_m_PaginationChildren_6(List_1_t3345875600 * value)
	{
		___m_PaginationChildren_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PaginationChildren_6), value);
	}

	inline static int32_t get_offset_of_scrollSnap_7() { return static_cast<int32_t>(offsetof(PaginationManager_t1455408585, ___scrollSnap_7)); }
	inline ScrollSnapBase_t805675194 * get_scrollSnap_7() const { return ___scrollSnap_7; }
	inline ScrollSnapBase_t805675194 ** get_address_of_scrollSnap_7() { return &___scrollSnap_7; }
	inline void set_scrollSnap_7(ScrollSnapBase_t805675194 * value)
	{
		___scrollSnap_7 = value;
		Il2CppCodeGenWriteBarrier((&___scrollSnap_7), value);
	}

	inline static int32_t get_offset_of_isAClick_8() { return static_cast<int32_t>(offsetof(PaginationManager_t1455408585, ___isAClick_8)); }
	inline bool get_isAClick_8() const { return ___isAClick_8; }
	inline bool* get_address_of_isAClick_8() { return &___isAClick_8; }
	inline void set_isAClick_8(bool value)
	{
		___isAClick_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGINATIONMANAGER_T1455408585_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef EXTENSIONSTOGGLE_T2639893278_H
#define EXTENSIONSTOGGLE_T2639893278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ExtensionsToggle
struct  ExtensionsToggle_t2639893278  : public Selectable_t1490392188
{
public:
	// System.String UnityEngine.UI.ExtensionsToggle::UniqueID
	String_t* ___UniqueID_16;
	// UnityEngine.UI.ExtensionsToggle/ToggleTransition UnityEngine.UI.ExtensionsToggle::toggleTransition
	int32_t ___toggleTransition_17;
	// UnityEngine.UI.Graphic UnityEngine.UI.ExtensionsToggle::graphic
	Graphic_t2426225576 * ___graphic_18;
	// UnityEngine.UI.ExtensionsToggleGroup UnityEngine.UI.ExtensionsToggle::m_Group
	ExtensionsToggleGroup_t1289496689 * ___m_Group_19;
	// UnityEngine.UI.ExtensionsToggle/ToggleEvent UnityEngine.UI.ExtensionsToggle::onValueChanged
	ToggleEvent_t2514518788 * ___onValueChanged_20;
	// UnityEngine.UI.ExtensionsToggle/ToggleEventObject UnityEngine.UI.ExtensionsToggle::onToggleChanged
	ToggleEventObject_t2759065715 * ___onToggleChanged_21;
	// System.Boolean UnityEngine.UI.ExtensionsToggle::m_IsOn
	bool ___m_IsOn_22;

public:
	inline static int32_t get_offset_of_UniqueID_16() { return static_cast<int32_t>(offsetof(ExtensionsToggle_t2639893278, ___UniqueID_16)); }
	inline String_t* get_UniqueID_16() const { return ___UniqueID_16; }
	inline String_t** get_address_of_UniqueID_16() { return &___UniqueID_16; }
	inline void set_UniqueID_16(String_t* value)
	{
		___UniqueID_16 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueID_16), value);
	}

	inline static int32_t get_offset_of_toggleTransition_17() { return static_cast<int32_t>(offsetof(ExtensionsToggle_t2639893278, ___toggleTransition_17)); }
	inline int32_t get_toggleTransition_17() const { return ___toggleTransition_17; }
	inline int32_t* get_address_of_toggleTransition_17() { return &___toggleTransition_17; }
	inline void set_toggleTransition_17(int32_t value)
	{
		___toggleTransition_17 = value;
	}

	inline static int32_t get_offset_of_graphic_18() { return static_cast<int32_t>(offsetof(ExtensionsToggle_t2639893278, ___graphic_18)); }
	inline Graphic_t2426225576 * get_graphic_18() const { return ___graphic_18; }
	inline Graphic_t2426225576 ** get_address_of_graphic_18() { return &___graphic_18; }
	inline void set_graphic_18(Graphic_t2426225576 * value)
	{
		___graphic_18 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_18), value);
	}

	inline static int32_t get_offset_of_m_Group_19() { return static_cast<int32_t>(offsetof(ExtensionsToggle_t2639893278, ___m_Group_19)); }
	inline ExtensionsToggleGroup_t1289496689 * get_m_Group_19() const { return ___m_Group_19; }
	inline ExtensionsToggleGroup_t1289496689 ** get_address_of_m_Group_19() { return &___m_Group_19; }
	inline void set_m_Group_19(ExtensionsToggleGroup_t1289496689 * value)
	{
		___m_Group_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Group_19), value);
	}

	inline static int32_t get_offset_of_onValueChanged_20() { return static_cast<int32_t>(offsetof(ExtensionsToggle_t2639893278, ___onValueChanged_20)); }
	inline ToggleEvent_t2514518788 * get_onValueChanged_20() const { return ___onValueChanged_20; }
	inline ToggleEvent_t2514518788 ** get_address_of_onValueChanged_20() { return &___onValueChanged_20; }
	inline void set_onValueChanged_20(ToggleEvent_t2514518788 * value)
	{
		___onValueChanged_20 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_20), value);
	}

	inline static int32_t get_offset_of_onToggleChanged_21() { return static_cast<int32_t>(offsetof(ExtensionsToggle_t2639893278, ___onToggleChanged_21)); }
	inline ToggleEventObject_t2759065715 * get_onToggleChanged_21() const { return ___onToggleChanged_21; }
	inline ToggleEventObject_t2759065715 ** get_address_of_onToggleChanged_21() { return &___onToggleChanged_21; }
	inline void set_onToggleChanged_21(ToggleEventObject_t2759065715 * value)
	{
		___onToggleChanged_21 = value;
		Il2CppCodeGenWriteBarrier((&___onToggleChanged_21), value);
	}

	inline static int32_t get_offset_of_m_IsOn_22() { return static_cast<int32_t>(offsetof(ExtensionsToggle_t2639893278, ___m_IsOn_22)); }
	inline bool get_m_IsOn_22() const { return ___m_IsOn_22; }
	inline bool* get_address_of_m_IsOn_22() { return &___m_IsOn_22; }
	inline void set_m_IsOn_22(bool value)
	{
		___m_IsOn_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONSTOGGLE_T2639893278_H
#ifndef SCROLLRECTEX_T4232992262_H
#define SCROLLRECTEX_T4232992262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectEx
struct  ScrollRectEx_t4232992262  : public ScrollRect_t1199013257
{
public:
	// System.Boolean UnityEngine.UI.Extensions.ScrollRectEx::routeToParent
	bool ___routeToParent_38;

public:
	inline static int32_t get_offset_of_routeToParent_38() { return static_cast<int32_t>(offsetof(ScrollRectEx_t4232992262, ___routeToParent_38)); }
	inline bool get_routeToParent_38() const { return ___routeToParent_38; }
	inline bool* get_address_of_routeToParent_38() { return &___routeToParent_38; }
	inline void set_routeToParent_38(bool value)
	{
		___routeToParent_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECTEX_T4232992262_H
#ifndef POINTERINPUTMODULE_T1441575871_H
#define POINTERINPUTMODULE_T1441575871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t1441575871  : public BaseInputModule_t1295781545
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t607610358 * ___m_PointerData_12;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t3572864619 * ___m_MouseState_13;

public:
	inline static int32_t get_offset_of_m_PointerData_12() { return static_cast<int32_t>(offsetof(PointerInputModule_t1441575871, ___m_PointerData_12)); }
	inline Dictionary_2_t607610358 * get_m_PointerData_12() const { return ___m_PointerData_12; }
	inline Dictionary_2_t607610358 ** get_address_of_m_PointerData_12() { return &___m_PointerData_12; }
	inline void set_m_PointerData_12(Dictionary_2_t607610358 * value)
	{
		___m_PointerData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_12), value);
	}

	inline static int32_t get_offset_of_m_MouseState_13() { return static_cast<int32_t>(offsetof(PointerInputModule_t1441575871, ___m_MouseState_13)); }
	inline MouseState_t3572864619 * get_m_MouseState_13() const { return ___m_MouseState_13; }
	inline MouseState_t3572864619 ** get_address_of_m_MouseState_13() { return &___m_MouseState_13; }
	inline void set_m_MouseState_13(MouseState_t3572864619 * value)
	{
		___m_MouseState_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T1441575871_H
#ifndef NONDRAWINGGRAPHIC_T49509067_H
#define NONDRAWINGGRAPHIC_T49509067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.NonDrawingGraphic
struct  NonDrawingGraphic_t49509067  : public MaskableGraphic_t540192618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONDRAWINGGRAPHIC_T49509067_H
#ifndef AIMERINPUTMODULE_T1926645740_H
#define AIMERINPUTMODULE_T1926645740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.Extensions.AimerInputModule
struct  AimerInputModule_t1926645740  : public PointerInputModule_t1441575871
{
public:
	// System.String UnityEngine.EventSystems.Extensions.AimerInputModule::activateAxis
	String_t* ___activateAxis_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.Extensions.AimerInputModule::aimerOffset
	Vector2_t2243707579  ___aimerOffset_15;

public:
	inline static int32_t get_offset_of_activateAxis_14() { return static_cast<int32_t>(offsetof(AimerInputModule_t1926645740, ___activateAxis_14)); }
	inline String_t* get_activateAxis_14() const { return ___activateAxis_14; }
	inline String_t** get_address_of_activateAxis_14() { return &___activateAxis_14; }
	inline void set_activateAxis_14(String_t* value)
	{
		___activateAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___activateAxis_14), value);
	}

	inline static int32_t get_offset_of_aimerOffset_15() { return static_cast<int32_t>(offsetof(AimerInputModule_t1926645740, ___aimerOffset_15)); }
	inline Vector2_t2243707579  get_aimerOffset_15() const { return ___aimerOffset_15; }
	inline Vector2_t2243707579 * get_address_of_aimerOffset_15() { return &___aimerOffset_15; }
	inline void set_aimerOffset_15(Vector2_t2243707579  value)
	{
		___aimerOffset_15 = value;
	}
};

struct AimerInputModule_t1926645740_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.Extensions.AimerInputModule::objectUnderAimer
	GameObject_t1756533147 * ___objectUnderAimer_16;

public:
	inline static int32_t get_offset_of_objectUnderAimer_16() { return static_cast<int32_t>(offsetof(AimerInputModule_t1926645740_StaticFields, ___objectUnderAimer_16)); }
	inline GameObject_t1756533147 * get_objectUnderAimer_16() const { return ___objectUnderAimer_16; }
	inline GameObject_t1756533147 ** get_address_of_objectUnderAimer_16() { return &___objectUnderAimer_16; }
	inline void set_objectUnderAimer_16(GameObject_t1756533147 * value)
	{
		___objectUnderAimer_16 = value;
		Il2CppCodeGenWriteBarrier((&___objectUnderAimer_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIMERINPUTMODULE_T1926645740_H
#ifndef SHINEEFFECT_T1749864756_H
#define SHINEEFFECT_T1749864756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ShineEffect
struct  ShineEffect_t1749864756  : public MaskableGraphic_t540192618
{
public:
	// System.Single UnityEngine.UI.Extensions.ShineEffect::yoffset
	float ___yoffset_28;
	// System.Single UnityEngine.UI.Extensions.ShineEffect::width
	float ___width_29;

public:
	inline static int32_t get_offset_of_yoffset_28() { return static_cast<int32_t>(offsetof(ShineEffect_t1749864756, ___yoffset_28)); }
	inline float get_yoffset_28() const { return ___yoffset_28; }
	inline float* get_address_of_yoffset_28() { return &___yoffset_28; }
	inline void set_yoffset_28(float value)
	{
		___yoffset_28 = value;
	}

	inline static int32_t get_offset_of_width_29() { return static_cast<int32_t>(offsetof(ShineEffect_t1749864756, ___width_29)); }
	inline float get_width_29() const { return ___width_29; }
	inline float* get_address_of_width_29() { return &___width_29; }
	inline void set_width_29(float value)
	{
		___width_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHINEEFFECT_T1749864756_H
#ifndef UIPARTICLESYSTEM_T1209606961_H
#define UIPARTICLESYSTEM_T1209606961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIParticleSystem
struct  UIParticleSystem_t1209606961  : public MaskableGraphic_t540192618
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UIParticleSystem::fixedTime
	bool ___fixedTime_28;
	// UnityEngine.Transform UnityEngine.UI.Extensions.UIParticleSystem::_transform
	Transform_t3275118058 * ____transform_29;
	// UnityEngine.ParticleSystem UnityEngine.UI.Extensions.UIParticleSystem::pSystem
	ParticleSystem_t3394631041 * ___pSystem_30;
	// UnityEngine.ParticleSystem/Particle[] UnityEngine.UI.Extensions.UIParticleSystem::particles
	ParticleU5BU5D_t574222242* ___particles_31;
	// UnityEngine.UIVertex[] UnityEngine.UI.Extensions.UIParticleSystem::_quad
	UIVertexU5BU5D_t3048644023* ____quad_32;
	// UnityEngine.Vector4 UnityEngine.UI.Extensions.UIParticleSystem::imageUV
	Vector4_t2243707581  ___imageUV_33;
	// UnityEngine.ParticleSystem/TextureSheetAnimationModule UnityEngine.UI.Extensions.UIParticleSystem::textureSheetAnimation
	TextureSheetAnimationModule_t4262561859  ___textureSheetAnimation_34;
	// System.Int32 UnityEngine.UI.Extensions.UIParticleSystem::textureSheetAnimationFrames
	int32_t ___textureSheetAnimationFrames_35;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UIParticleSystem::textureSheetAnimationFrameSize
	Vector2_t2243707579  ___textureSheetAnimationFrameSize_36;
	// UnityEngine.ParticleSystemRenderer UnityEngine.UI.Extensions.UIParticleSystem::pRenderer
	ParticleSystemRenderer_t892265570 * ___pRenderer_37;
	// UnityEngine.Material UnityEngine.UI.Extensions.UIParticleSystem::currentMaterial
	Material_t193706927 * ___currentMaterial_38;
	// UnityEngine.Texture UnityEngine.UI.Extensions.UIParticleSystem::currentTexture
	Texture_t2243626319 * ___currentTexture_39;
	// UnityEngine.ParticleSystem/MainModule UnityEngine.UI.Extensions.UIParticleSystem::mainModule
	MainModule_t6751348  ___mainModule_40;

public:
	inline static int32_t get_offset_of_fixedTime_28() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___fixedTime_28)); }
	inline bool get_fixedTime_28() const { return ___fixedTime_28; }
	inline bool* get_address_of_fixedTime_28() { return &___fixedTime_28; }
	inline void set_fixedTime_28(bool value)
	{
		___fixedTime_28 = value;
	}

	inline static int32_t get_offset_of__transform_29() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ____transform_29)); }
	inline Transform_t3275118058 * get__transform_29() const { return ____transform_29; }
	inline Transform_t3275118058 ** get_address_of__transform_29() { return &____transform_29; }
	inline void set__transform_29(Transform_t3275118058 * value)
	{
		____transform_29 = value;
		Il2CppCodeGenWriteBarrier((&____transform_29), value);
	}

	inline static int32_t get_offset_of_pSystem_30() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___pSystem_30)); }
	inline ParticleSystem_t3394631041 * get_pSystem_30() const { return ___pSystem_30; }
	inline ParticleSystem_t3394631041 ** get_address_of_pSystem_30() { return &___pSystem_30; }
	inline void set_pSystem_30(ParticleSystem_t3394631041 * value)
	{
		___pSystem_30 = value;
		Il2CppCodeGenWriteBarrier((&___pSystem_30), value);
	}

	inline static int32_t get_offset_of_particles_31() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___particles_31)); }
	inline ParticleU5BU5D_t574222242* get_particles_31() const { return ___particles_31; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_31() { return &___particles_31; }
	inline void set_particles_31(ParticleU5BU5D_t574222242* value)
	{
		___particles_31 = value;
		Il2CppCodeGenWriteBarrier((&___particles_31), value);
	}

	inline static int32_t get_offset_of__quad_32() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ____quad_32)); }
	inline UIVertexU5BU5D_t3048644023* get__quad_32() const { return ____quad_32; }
	inline UIVertexU5BU5D_t3048644023** get_address_of__quad_32() { return &____quad_32; }
	inline void set__quad_32(UIVertexU5BU5D_t3048644023* value)
	{
		____quad_32 = value;
		Il2CppCodeGenWriteBarrier((&____quad_32), value);
	}

	inline static int32_t get_offset_of_imageUV_33() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___imageUV_33)); }
	inline Vector4_t2243707581  get_imageUV_33() const { return ___imageUV_33; }
	inline Vector4_t2243707581 * get_address_of_imageUV_33() { return &___imageUV_33; }
	inline void set_imageUV_33(Vector4_t2243707581  value)
	{
		___imageUV_33 = value;
	}

	inline static int32_t get_offset_of_textureSheetAnimation_34() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___textureSheetAnimation_34)); }
	inline TextureSheetAnimationModule_t4262561859  get_textureSheetAnimation_34() const { return ___textureSheetAnimation_34; }
	inline TextureSheetAnimationModule_t4262561859 * get_address_of_textureSheetAnimation_34() { return &___textureSheetAnimation_34; }
	inline void set_textureSheetAnimation_34(TextureSheetAnimationModule_t4262561859  value)
	{
		___textureSheetAnimation_34 = value;
	}

	inline static int32_t get_offset_of_textureSheetAnimationFrames_35() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___textureSheetAnimationFrames_35)); }
	inline int32_t get_textureSheetAnimationFrames_35() const { return ___textureSheetAnimationFrames_35; }
	inline int32_t* get_address_of_textureSheetAnimationFrames_35() { return &___textureSheetAnimationFrames_35; }
	inline void set_textureSheetAnimationFrames_35(int32_t value)
	{
		___textureSheetAnimationFrames_35 = value;
	}

	inline static int32_t get_offset_of_textureSheetAnimationFrameSize_36() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___textureSheetAnimationFrameSize_36)); }
	inline Vector2_t2243707579  get_textureSheetAnimationFrameSize_36() const { return ___textureSheetAnimationFrameSize_36; }
	inline Vector2_t2243707579 * get_address_of_textureSheetAnimationFrameSize_36() { return &___textureSheetAnimationFrameSize_36; }
	inline void set_textureSheetAnimationFrameSize_36(Vector2_t2243707579  value)
	{
		___textureSheetAnimationFrameSize_36 = value;
	}

	inline static int32_t get_offset_of_pRenderer_37() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___pRenderer_37)); }
	inline ParticleSystemRenderer_t892265570 * get_pRenderer_37() const { return ___pRenderer_37; }
	inline ParticleSystemRenderer_t892265570 ** get_address_of_pRenderer_37() { return &___pRenderer_37; }
	inline void set_pRenderer_37(ParticleSystemRenderer_t892265570 * value)
	{
		___pRenderer_37 = value;
		Il2CppCodeGenWriteBarrier((&___pRenderer_37), value);
	}

	inline static int32_t get_offset_of_currentMaterial_38() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___currentMaterial_38)); }
	inline Material_t193706927 * get_currentMaterial_38() const { return ___currentMaterial_38; }
	inline Material_t193706927 ** get_address_of_currentMaterial_38() { return &___currentMaterial_38; }
	inline void set_currentMaterial_38(Material_t193706927 * value)
	{
		___currentMaterial_38 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_38), value);
	}

	inline static int32_t get_offset_of_currentTexture_39() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___currentTexture_39)); }
	inline Texture_t2243626319 * get_currentTexture_39() const { return ___currentTexture_39; }
	inline Texture_t2243626319 ** get_address_of_currentTexture_39() { return &___currentTexture_39; }
	inline void set_currentTexture_39(Texture_t2243626319 * value)
	{
		___currentTexture_39 = value;
		Il2CppCodeGenWriteBarrier((&___currentTexture_39), value);
	}

	inline static int32_t get_offset_of_mainModule_40() { return static_cast<int32_t>(offsetof(UIParticleSystem_t1209606961, ___mainModule_40)); }
	inline MainModule_t6751348  get_mainModule_40() const { return ___mainModule_40; }
	inline MainModule_t6751348 * get_address_of_mainModule_40() { return &___mainModule_40; }
	inline void set_mainModule_40(MainModule_t6751348  value)
	{
		___mainModule_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPARTICLESYSTEM_T1209606961_H
#ifndef UIPRIMITIVEBASE_T2118950086_H
#define UIPRIMITIVEBASE_T2118950086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIPrimitiveBase
struct  UIPrimitiveBase_t2118950086  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Extensions.UIPrimitiveBase::m_Sprite
	Sprite_t309593783 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.UIPrimitiveBase::m_OverrideSprite
	Sprite_t309593783 * ___m_OverrideSprite_30;
	// System.Single UnityEngine.UI.Extensions.UIPrimitiveBase::m_EventAlphaThreshold
	float ___m_EventAlphaThreshold_31;
	// UnityEngine.UI.Extensions.ResolutionMode UnityEngine.UI.Extensions.UIPrimitiveBase::m_improveResolution
	int32_t ___m_improveResolution_32;
	// System.Single UnityEngine.UI.Extensions.UIPrimitiveBase::m_Resolution
	float ___m_Resolution_33;
	// System.Boolean UnityEngine.UI.Extensions.UIPrimitiveBase::m_useNativeSize
	bool ___m_useNativeSize_34;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(UIPrimitiveBase_t2118950086, ___m_Sprite_29)); }
	inline Sprite_t309593783 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t309593783 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t309593783 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(UIPrimitiveBase_t2118950086, ___m_OverrideSprite_30)); }
	inline Sprite_t309593783 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t309593783 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t309593783 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_EventAlphaThreshold_31() { return static_cast<int32_t>(offsetof(UIPrimitiveBase_t2118950086, ___m_EventAlphaThreshold_31)); }
	inline float get_m_EventAlphaThreshold_31() const { return ___m_EventAlphaThreshold_31; }
	inline float* get_address_of_m_EventAlphaThreshold_31() { return &___m_EventAlphaThreshold_31; }
	inline void set_m_EventAlphaThreshold_31(float value)
	{
		___m_EventAlphaThreshold_31 = value;
	}

	inline static int32_t get_offset_of_m_improveResolution_32() { return static_cast<int32_t>(offsetof(UIPrimitiveBase_t2118950086, ___m_improveResolution_32)); }
	inline int32_t get_m_improveResolution_32() const { return ___m_improveResolution_32; }
	inline int32_t* get_address_of_m_improveResolution_32() { return &___m_improveResolution_32; }
	inline void set_m_improveResolution_32(int32_t value)
	{
		___m_improveResolution_32 = value;
	}

	inline static int32_t get_offset_of_m_Resolution_33() { return static_cast<int32_t>(offsetof(UIPrimitiveBase_t2118950086, ___m_Resolution_33)); }
	inline float get_m_Resolution_33() const { return ___m_Resolution_33; }
	inline float* get_address_of_m_Resolution_33() { return &___m_Resolution_33; }
	inline void set_m_Resolution_33(float value)
	{
		___m_Resolution_33 = value;
	}

	inline static int32_t get_offset_of_m_useNativeSize_34() { return static_cast<int32_t>(offsetof(UIPrimitiveBase_t2118950086, ___m_useNativeSize_34)); }
	inline bool get_m_useNativeSize_34() const { return ___m_useNativeSize_34; }
	inline bool* get_address_of_m_useNativeSize_34() { return &___m_useNativeSize_34; }
	inline void set_m_useNativeSize_34(bool value)
	{
		___m_useNativeSize_34 = value;
	}
};

struct UIPrimitiveBase_t2118950086_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Extensions.UIPrimitiveBase::s_ETC1DefaultUI
	Material_t193706927 * ___s_ETC1DefaultUI_28;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(UIPrimitiveBase_t2118950086_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t193706927 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t193706927 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t193706927 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPRIMITIVEBASE_T2118950086_H
#ifndef UICIRCLE_T2117378996_H
#define UICIRCLE_T2117378996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UICircle
struct  UICircle_t2117378996  : public UIPrimitiveBase_t2118950086
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UICircle::m_fillPercent
	int32_t ___m_fillPercent_35;
	// System.Boolean UnityEngine.UI.Extensions.UICircle::FixedToSegments
	bool ___FixedToSegments_36;
	// System.Boolean UnityEngine.UI.Extensions.UICircle::m_fill
	bool ___m_fill_37;
	// System.Single UnityEngine.UI.Extensions.UICircle::m_thickness
	float ___m_thickness_38;
	// System.Int32 UnityEngine.UI.Extensions.UICircle::m_segments
	int32_t ___m_segments_39;

public:
	inline static int32_t get_offset_of_m_fillPercent_35() { return static_cast<int32_t>(offsetof(UICircle_t2117378996, ___m_fillPercent_35)); }
	inline int32_t get_m_fillPercent_35() const { return ___m_fillPercent_35; }
	inline int32_t* get_address_of_m_fillPercent_35() { return &___m_fillPercent_35; }
	inline void set_m_fillPercent_35(int32_t value)
	{
		___m_fillPercent_35 = value;
	}

	inline static int32_t get_offset_of_FixedToSegments_36() { return static_cast<int32_t>(offsetof(UICircle_t2117378996, ___FixedToSegments_36)); }
	inline bool get_FixedToSegments_36() const { return ___FixedToSegments_36; }
	inline bool* get_address_of_FixedToSegments_36() { return &___FixedToSegments_36; }
	inline void set_FixedToSegments_36(bool value)
	{
		___FixedToSegments_36 = value;
	}

	inline static int32_t get_offset_of_m_fill_37() { return static_cast<int32_t>(offsetof(UICircle_t2117378996, ___m_fill_37)); }
	inline bool get_m_fill_37() const { return ___m_fill_37; }
	inline bool* get_address_of_m_fill_37() { return &___m_fill_37; }
	inline void set_m_fill_37(bool value)
	{
		___m_fill_37 = value;
	}

	inline static int32_t get_offset_of_m_thickness_38() { return static_cast<int32_t>(offsetof(UICircle_t2117378996, ___m_thickness_38)); }
	inline float get_m_thickness_38() const { return ___m_thickness_38; }
	inline float* get_address_of_m_thickness_38() { return &___m_thickness_38; }
	inline void set_m_thickness_38(float value)
	{
		___m_thickness_38 = value;
	}

	inline static int32_t get_offset_of_m_segments_39() { return static_cast<int32_t>(offsetof(UICircle_t2117378996, ___m_segments_39)); }
	inline int32_t get_m_segments_39() const { return ___m_segments_39; }
	inline int32_t* get_address_of_m_segments_39() { return &___m_segments_39; }
	inline void set_m_segments_39(int32_t value)
	{
		___m_segments_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICIRCLE_T2117378996_H
#ifndef UILINERENDERER_T3031355003_H
#define UILINERENDERER_T3031355003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILineRenderer
struct  UILineRenderer_t3031355003  : public UIPrimitiveBase_t2118950086
{
public:
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.UILineRenderer::m_points
	Vector2U5BU5D_t686124026* ___m_points_49;
	// System.Single UnityEngine.UI.Extensions.UILineRenderer::lineThickness
	float ___lineThickness_50;
	// System.Boolean UnityEngine.UI.Extensions.UILineRenderer::relativeSize
	bool ___relativeSize_51;
	// System.Boolean UnityEngine.UI.Extensions.UILineRenderer::lineList
	bool ___lineList_52;
	// System.Boolean UnityEngine.UI.Extensions.UILineRenderer::lineCaps
	bool ___lineCaps_53;
	// System.Int32 UnityEngine.UI.Extensions.UILineRenderer::bezierSegmentsPerCurve
	int32_t ___bezierSegmentsPerCurve_54;
	// UnityEngine.UI.Extensions.UILineRenderer/JoinType UnityEngine.UI.Extensions.UILineRenderer::LineJoins
	int32_t ___LineJoins_55;
	// UnityEngine.UI.Extensions.UILineRenderer/BezierType UnityEngine.UI.Extensions.UILineRenderer::BezierMode
	int32_t ___BezierMode_56;
	// System.Boolean UnityEngine.UI.Extensions.UILineRenderer::drivenExternally
	bool ___drivenExternally_57;

public:
	inline static int32_t get_offset_of_m_points_49() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___m_points_49)); }
	inline Vector2U5BU5D_t686124026* get_m_points_49() const { return ___m_points_49; }
	inline Vector2U5BU5D_t686124026** get_address_of_m_points_49() { return &___m_points_49; }
	inline void set_m_points_49(Vector2U5BU5D_t686124026* value)
	{
		___m_points_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_points_49), value);
	}

	inline static int32_t get_offset_of_lineThickness_50() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___lineThickness_50)); }
	inline float get_lineThickness_50() const { return ___lineThickness_50; }
	inline float* get_address_of_lineThickness_50() { return &___lineThickness_50; }
	inline void set_lineThickness_50(float value)
	{
		___lineThickness_50 = value;
	}

	inline static int32_t get_offset_of_relativeSize_51() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___relativeSize_51)); }
	inline bool get_relativeSize_51() const { return ___relativeSize_51; }
	inline bool* get_address_of_relativeSize_51() { return &___relativeSize_51; }
	inline void set_relativeSize_51(bool value)
	{
		___relativeSize_51 = value;
	}

	inline static int32_t get_offset_of_lineList_52() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___lineList_52)); }
	inline bool get_lineList_52() const { return ___lineList_52; }
	inline bool* get_address_of_lineList_52() { return &___lineList_52; }
	inline void set_lineList_52(bool value)
	{
		___lineList_52 = value;
	}

	inline static int32_t get_offset_of_lineCaps_53() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___lineCaps_53)); }
	inline bool get_lineCaps_53() const { return ___lineCaps_53; }
	inline bool* get_address_of_lineCaps_53() { return &___lineCaps_53; }
	inline void set_lineCaps_53(bool value)
	{
		___lineCaps_53 = value;
	}

	inline static int32_t get_offset_of_bezierSegmentsPerCurve_54() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___bezierSegmentsPerCurve_54)); }
	inline int32_t get_bezierSegmentsPerCurve_54() const { return ___bezierSegmentsPerCurve_54; }
	inline int32_t* get_address_of_bezierSegmentsPerCurve_54() { return &___bezierSegmentsPerCurve_54; }
	inline void set_bezierSegmentsPerCurve_54(int32_t value)
	{
		___bezierSegmentsPerCurve_54 = value;
	}

	inline static int32_t get_offset_of_LineJoins_55() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___LineJoins_55)); }
	inline int32_t get_LineJoins_55() const { return ___LineJoins_55; }
	inline int32_t* get_address_of_LineJoins_55() { return &___LineJoins_55; }
	inline void set_LineJoins_55(int32_t value)
	{
		___LineJoins_55 = value;
	}

	inline static int32_t get_offset_of_BezierMode_56() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___BezierMode_56)); }
	inline int32_t get_BezierMode_56() const { return ___BezierMode_56; }
	inline int32_t* get_address_of_BezierMode_56() { return &___BezierMode_56; }
	inline void set_BezierMode_56(int32_t value)
	{
		___BezierMode_56 = value;
	}

	inline static int32_t get_offset_of_drivenExternally_57() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003, ___drivenExternally_57)); }
	inline bool get_drivenExternally_57() const { return ___drivenExternally_57; }
	inline bool* get_address_of_drivenExternally_57() { return &___drivenExternally_57; }
	inline void set_drivenExternally_57(bool value)
	{
		___drivenExternally_57 = value;
	}
};

struct UILineRenderer_t3031355003_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_TOP_LEFT
	Vector2_t2243707579  ___UV_TOP_LEFT_37;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_BOTTOM_LEFT
	Vector2_t2243707579  ___UV_BOTTOM_LEFT_38;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_TOP_CENTER_LEFT
	Vector2_t2243707579  ___UV_TOP_CENTER_LEFT_39;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_TOP_CENTER_RIGHT
	Vector2_t2243707579  ___UV_TOP_CENTER_RIGHT_40;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_BOTTOM_CENTER_LEFT
	Vector2_t2243707579  ___UV_BOTTOM_CENTER_LEFT_41;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_BOTTOM_CENTER_RIGHT
	Vector2_t2243707579  ___UV_BOTTOM_CENTER_RIGHT_42;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_TOP_RIGHT
	Vector2_t2243707579  ___UV_TOP_RIGHT_43;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineRenderer::UV_BOTTOM_RIGHT
	Vector2_t2243707579  ___UV_BOTTOM_RIGHT_44;
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.UILineRenderer::startUvs
	Vector2U5BU5D_t686124026* ___startUvs_45;
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.UILineRenderer::middleUvs
	Vector2U5BU5D_t686124026* ___middleUvs_46;
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.UILineRenderer::endUvs
	Vector2U5BU5D_t686124026* ___endUvs_47;
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.UILineRenderer::fullUvs
	Vector2U5BU5D_t686124026* ___fullUvs_48;

public:
	inline static int32_t get_offset_of_UV_TOP_LEFT_37() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_TOP_LEFT_37)); }
	inline Vector2_t2243707579  get_UV_TOP_LEFT_37() const { return ___UV_TOP_LEFT_37; }
	inline Vector2_t2243707579 * get_address_of_UV_TOP_LEFT_37() { return &___UV_TOP_LEFT_37; }
	inline void set_UV_TOP_LEFT_37(Vector2_t2243707579  value)
	{
		___UV_TOP_LEFT_37 = value;
	}

	inline static int32_t get_offset_of_UV_BOTTOM_LEFT_38() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_BOTTOM_LEFT_38)); }
	inline Vector2_t2243707579  get_UV_BOTTOM_LEFT_38() const { return ___UV_BOTTOM_LEFT_38; }
	inline Vector2_t2243707579 * get_address_of_UV_BOTTOM_LEFT_38() { return &___UV_BOTTOM_LEFT_38; }
	inline void set_UV_BOTTOM_LEFT_38(Vector2_t2243707579  value)
	{
		___UV_BOTTOM_LEFT_38 = value;
	}

	inline static int32_t get_offset_of_UV_TOP_CENTER_LEFT_39() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_TOP_CENTER_LEFT_39)); }
	inline Vector2_t2243707579  get_UV_TOP_CENTER_LEFT_39() const { return ___UV_TOP_CENTER_LEFT_39; }
	inline Vector2_t2243707579 * get_address_of_UV_TOP_CENTER_LEFT_39() { return &___UV_TOP_CENTER_LEFT_39; }
	inline void set_UV_TOP_CENTER_LEFT_39(Vector2_t2243707579  value)
	{
		___UV_TOP_CENTER_LEFT_39 = value;
	}

	inline static int32_t get_offset_of_UV_TOP_CENTER_RIGHT_40() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_TOP_CENTER_RIGHT_40)); }
	inline Vector2_t2243707579  get_UV_TOP_CENTER_RIGHT_40() const { return ___UV_TOP_CENTER_RIGHT_40; }
	inline Vector2_t2243707579 * get_address_of_UV_TOP_CENTER_RIGHT_40() { return &___UV_TOP_CENTER_RIGHT_40; }
	inline void set_UV_TOP_CENTER_RIGHT_40(Vector2_t2243707579  value)
	{
		___UV_TOP_CENTER_RIGHT_40 = value;
	}

	inline static int32_t get_offset_of_UV_BOTTOM_CENTER_LEFT_41() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_BOTTOM_CENTER_LEFT_41)); }
	inline Vector2_t2243707579  get_UV_BOTTOM_CENTER_LEFT_41() const { return ___UV_BOTTOM_CENTER_LEFT_41; }
	inline Vector2_t2243707579 * get_address_of_UV_BOTTOM_CENTER_LEFT_41() { return &___UV_BOTTOM_CENTER_LEFT_41; }
	inline void set_UV_BOTTOM_CENTER_LEFT_41(Vector2_t2243707579  value)
	{
		___UV_BOTTOM_CENTER_LEFT_41 = value;
	}

	inline static int32_t get_offset_of_UV_BOTTOM_CENTER_RIGHT_42() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_BOTTOM_CENTER_RIGHT_42)); }
	inline Vector2_t2243707579  get_UV_BOTTOM_CENTER_RIGHT_42() const { return ___UV_BOTTOM_CENTER_RIGHT_42; }
	inline Vector2_t2243707579 * get_address_of_UV_BOTTOM_CENTER_RIGHT_42() { return &___UV_BOTTOM_CENTER_RIGHT_42; }
	inline void set_UV_BOTTOM_CENTER_RIGHT_42(Vector2_t2243707579  value)
	{
		___UV_BOTTOM_CENTER_RIGHT_42 = value;
	}

	inline static int32_t get_offset_of_UV_TOP_RIGHT_43() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_TOP_RIGHT_43)); }
	inline Vector2_t2243707579  get_UV_TOP_RIGHT_43() const { return ___UV_TOP_RIGHT_43; }
	inline Vector2_t2243707579 * get_address_of_UV_TOP_RIGHT_43() { return &___UV_TOP_RIGHT_43; }
	inline void set_UV_TOP_RIGHT_43(Vector2_t2243707579  value)
	{
		___UV_TOP_RIGHT_43 = value;
	}

	inline static int32_t get_offset_of_UV_BOTTOM_RIGHT_44() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___UV_BOTTOM_RIGHT_44)); }
	inline Vector2_t2243707579  get_UV_BOTTOM_RIGHT_44() const { return ___UV_BOTTOM_RIGHT_44; }
	inline Vector2_t2243707579 * get_address_of_UV_BOTTOM_RIGHT_44() { return &___UV_BOTTOM_RIGHT_44; }
	inline void set_UV_BOTTOM_RIGHT_44(Vector2_t2243707579  value)
	{
		___UV_BOTTOM_RIGHT_44 = value;
	}

	inline static int32_t get_offset_of_startUvs_45() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___startUvs_45)); }
	inline Vector2U5BU5D_t686124026* get_startUvs_45() const { return ___startUvs_45; }
	inline Vector2U5BU5D_t686124026** get_address_of_startUvs_45() { return &___startUvs_45; }
	inline void set_startUvs_45(Vector2U5BU5D_t686124026* value)
	{
		___startUvs_45 = value;
		Il2CppCodeGenWriteBarrier((&___startUvs_45), value);
	}

	inline static int32_t get_offset_of_middleUvs_46() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___middleUvs_46)); }
	inline Vector2U5BU5D_t686124026* get_middleUvs_46() const { return ___middleUvs_46; }
	inline Vector2U5BU5D_t686124026** get_address_of_middleUvs_46() { return &___middleUvs_46; }
	inline void set_middleUvs_46(Vector2U5BU5D_t686124026* value)
	{
		___middleUvs_46 = value;
		Il2CppCodeGenWriteBarrier((&___middleUvs_46), value);
	}

	inline static int32_t get_offset_of_endUvs_47() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___endUvs_47)); }
	inline Vector2U5BU5D_t686124026* get_endUvs_47() const { return ___endUvs_47; }
	inline Vector2U5BU5D_t686124026** get_address_of_endUvs_47() { return &___endUvs_47; }
	inline void set_endUvs_47(Vector2U5BU5D_t686124026* value)
	{
		___endUvs_47 = value;
		Il2CppCodeGenWriteBarrier((&___endUvs_47), value);
	}

	inline static int32_t get_offset_of_fullUvs_48() { return static_cast<int32_t>(offsetof(UILineRenderer_t3031355003_StaticFields, ___fullUvs_48)); }
	inline Vector2U5BU5D_t686124026* get_fullUvs_48() const { return ___fullUvs_48; }
	inline Vector2U5BU5D_t686124026** get_address_of_fullUvs_48() { return &___fullUvs_48; }
	inline void set_fullUvs_48(Vector2U5BU5D_t686124026* value)
	{
		___fullUvs_48 = value;
		Il2CppCodeGenWriteBarrier((&___fullUvs_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINERENDERER_T3031355003_H
#ifndef DIAMONDGRAPH_T80153826_H
#define DIAMONDGRAPH_T80153826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DiamondGraph
struct  DiamondGraph_t80153826  : public UIPrimitiveBase_t2118950086
{
public:
	// System.Single UnityEngine.UI.Extensions.DiamondGraph::m_a
	float ___m_a_35;
	// System.Single UnityEngine.UI.Extensions.DiamondGraph::m_b
	float ___m_b_36;
	// System.Single UnityEngine.UI.Extensions.DiamondGraph::m_c
	float ___m_c_37;
	// System.Single UnityEngine.UI.Extensions.DiamondGraph::m_d
	float ___m_d_38;

public:
	inline static int32_t get_offset_of_m_a_35() { return static_cast<int32_t>(offsetof(DiamondGraph_t80153826, ___m_a_35)); }
	inline float get_m_a_35() const { return ___m_a_35; }
	inline float* get_address_of_m_a_35() { return &___m_a_35; }
	inline void set_m_a_35(float value)
	{
		___m_a_35 = value;
	}

	inline static int32_t get_offset_of_m_b_36() { return static_cast<int32_t>(offsetof(DiamondGraph_t80153826, ___m_b_36)); }
	inline float get_m_b_36() const { return ___m_b_36; }
	inline float* get_address_of_m_b_36() { return &___m_b_36; }
	inline void set_m_b_36(float value)
	{
		___m_b_36 = value;
	}

	inline static int32_t get_offset_of_m_c_37() { return static_cast<int32_t>(offsetof(DiamondGraph_t80153826, ___m_c_37)); }
	inline float get_m_c_37() const { return ___m_c_37; }
	inline float* get_address_of_m_c_37() { return &___m_c_37; }
	inline void set_m_c_37(float value)
	{
		___m_c_37 = value;
	}

	inline static int32_t get_offset_of_m_d_38() { return static_cast<int32_t>(offsetof(DiamondGraph_t80153826, ___m_d_38)); }
	inline float get_m_d_38() const { return ___m_d_38; }
	inline float* get_address_of_m_d_38() { return &___m_d_38; }
	inline void set_m_d_38(float value)
	{
		___m_d_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIAMONDGRAPH_T80153826_H
#ifndef UILINETEXTURERENDERER_T1203126486_H
#define UILINETEXTURERENDERER_T1203126486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILineTextureRenderer
struct  UILineTextureRenderer_t1203126486  : public UIPrimitiveBase_t2118950086
{
public:
	// UnityEngine.Rect UnityEngine.UI.Extensions.UILineTextureRenderer::m_UVRect
	Rect_t3681755626  ___m_UVRect_35;
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.UILineTextureRenderer::m_points
	Vector2U5BU5D_t686124026* ___m_points_36;
	// System.Single UnityEngine.UI.Extensions.UILineTextureRenderer::LineThickness
	float ___LineThickness_37;
	// System.Boolean UnityEngine.UI.Extensions.UILineTextureRenderer::UseMargins
	bool ___UseMargins_38;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UILineTextureRenderer::Margin
	Vector2_t2243707579  ___Margin_39;
	// System.Boolean UnityEngine.UI.Extensions.UILineTextureRenderer::relativeSize
	bool ___relativeSize_40;

public:
	inline static int32_t get_offset_of_m_UVRect_35() { return static_cast<int32_t>(offsetof(UILineTextureRenderer_t1203126486, ___m_UVRect_35)); }
	inline Rect_t3681755626  get_m_UVRect_35() const { return ___m_UVRect_35; }
	inline Rect_t3681755626 * get_address_of_m_UVRect_35() { return &___m_UVRect_35; }
	inline void set_m_UVRect_35(Rect_t3681755626  value)
	{
		___m_UVRect_35 = value;
	}

	inline static int32_t get_offset_of_m_points_36() { return static_cast<int32_t>(offsetof(UILineTextureRenderer_t1203126486, ___m_points_36)); }
	inline Vector2U5BU5D_t686124026* get_m_points_36() const { return ___m_points_36; }
	inline Vector2U5BU5D_t686124026** get_address_of_m_points_36() { return &___m_points_36; }
	inline void set_m_points_36(Vector2U5BU5D_t686124026* value)
	{
		___m_points_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_points_36), value);
	}

	inline static int32_t get_offset_of_LineThickness_37() { return static_cast<int32_t>(offsetof(UILineTextureRenderer_t1203126486, ___LineThickness_37)); }
	inline float get_LineThickness_37() const { return ___LineThickness_37; }
	inline float* get_address_of_LineThickness_37() { return &___LineThickness_37; }
	inline void set_LineThickness_37(float value)
	{
		___LineThickness_37 = value;
	}

	inline static int32_t get_offset_of_UseMargins_38() { return static_cast<int32_t>(offsetof(UILineTextureRenderer_t1203126486, ___UseMargins_38)); }
	inline bool get_UseMargins_38() const { return ___UseMargins_38; }
	inline bool* get_address_of_UseMargins_38() { return &___UseMargins_38; }
	inline void set_UseMargins_38(bool value)
	{
		___UseMargins_38 = value;
	}

	inline static int32_t get_offset_of_Margin_39() { return static_cast<int32_t>(offsetof(UILineTextureRenderer_t1203126486, ___Margin_39)); }
	inline Vector2_t2243707579  get_Margin_39() const { return ___Margin_39; }
	inline Vector2_t2243707579 * get_address_of_Margin_39() { return &___Margin_39; }
	inline void set_Margin_39(Vector2_t2243707579  value)
	{
		___Margin_39 = value;
	}

	inline static int32_t get_offset_of_relativeSize_40() { return static_cast<int32_t>(offsetof(UILineTextureRenderer_t1203126486, ___relativeSize_40)); }
	inline bool get_relativeSize_40() const { return ___relativeSize_40; }
	inline bool* get_address_of_relativeSize_40() { return &___relativeSize_40; }
	inline void set_relativeSize_40(bool value)
	{
		___relativeSize_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINETEXTURERENDERER_T1203126486_H
#ifndef UICORNERCUT_T1498381979_H
#define UICORNERCUT_T1498381979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UICornerCut
struct  UICornerCut_t1498381979  : public UIPrimitiveBase_t2118950086
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UICornerCut::cornerSize
	Vector2_t2243707579  ___cornerSize_35;
	// System.Boolean UnityEngine.UI.Extensions.UICornerCut::m_cutUL
	bool ___m_cutUL_36;
	// System.Boolean UnityEngine.UI.Extensions.UICornerCut::m_cutUR
	bool ___m_cutUR_37;
	// System.Boolean UnityEngine.UI.Extensions.UICornerCut::m_cutLL
	bool ___m_cutLL_38;
	// System.Boolean UnityEngine.UI.Extensions.UICornerCut::m_cutLR
	bool ___m_cutLR_39;
	// System.Boolean UnityEngine.UI.Extensions.UICornerCut::m_makeColumns
	bool ___m_makeColumns_40;
	// System.Boolean UnityEngine.UI.Extensions.UICornerCut::m_useColorUp
	bool ___m_useColorUp_41;
	// UnityEngine.Color32 UnityEngine.UI.Extensions.UICornerCut::m_colorUp
	Color32_t874517518  ___m_colorUp_42;
	// System.Boolean UnityEngine.UI.Extensions.UICornerCut::m_useColorDown
	bool ___m_useColorDown_43;
	// UnityEngine.Color32 UnityEngine.UI.Extensions.UICornerCut::m_colorDown
	Color32_t874517518  ___m_colorDown_44;

public:
	inline static int32_t get_offset_of_cornerSize_35() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___cornerSize_35)); }
	inline Vector2_t2243707579  get_cornerSize_35() const { return ___cornerSize_35; }
	inline Vector2_t2243707579 * get_address_of_cornerSize_35() { return &___cornerSize_35; }
	inline void set_cornerSize_35(Vector2_t2243707579  value)
	{
		___cornerSize_35 = value;
	}

	inline static int32_t get_offset_of_m_cutUL_36() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_cutUL_36)); }
	inline bool get_m_cutUL_36() const { return ___m_cutUL_36; }
	inline bool* get_address_of_m_cutUL_36() { return &___m_cutUL_36; }
	inline void set_m_cutUL_36(bool value)
	{
		___m_cutUL_36 = value;
	}

	inline static int32_t get_offset_of_m_cutUR_37() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_cutUR_37)); }
	inline bool get_m_cutUR_37() const { return ___m_cutUR_37; }
	inline bool* get_address_of_m_cutUR_37() { return &___m_cutUR_37; }
	inline void set_m_cutUR_37(bool value)
	{
		___m_cutUR_37 = value;
	}

	inline static int32_t get_offset_of_m_cutLL_38() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_cutLL_38)); }
	inline bool get_m_cutLL_38() const { return ___m_cutLL_38; }
	inline bool* get_address_of_m_cutLL_38() { return &___m_cutLL_38; }
	inline void set_m_cutLL_38(bool value)
	{
		___m_cutLL_38 = value;
	}

	inline static int32_t get_offset_of_m_cutLR_39() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_cutLR_39)); }
	inline bool get_m_cutLR_39() const { return ___m_cutLR_39; }
	inline bool* get_address_of_m_cutLR_39() { return &___m_cutLR_39; }
	inline void set_m_cutLR_39(bool value)
	{
		___m_cutLR_39 = value;
	}

	inline static int32_t get_offset_of_m_makeColumns_40() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_makeColumns_40)); }
	inline bool get_m_makeColumns_40() const { return ___m_makeColumns_40; }
	inline bool* get_address_of_m_makeColumns_40() { return &___m_makeColumns_40; }
	inline void set_m_makeColumns_40(bool value)
	{
		___m_makeColumns_40 = value;
	}

	inline static int32_t get_offset_of_m_useColorUp_41() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_useColorUp_41)); }
	inline bool get_m_useColorUp_41() const { return ___m_useColorUp_41; }
	inline bool* get_address_of_m_useColorUp_41() { return &___m_useColorUp_41; }
	inline void set_m_useColorUp_41(bool value)
	{
		___m_useColorUp_41 = value;
	}

	inline static int32_t get_offset_of_m_colorUp_42() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_colorUp_42)); }
	inline Color32_t874517518  get_m_colorUp_42() const { return ___m_colorUp_42; }
	inline Color32_t874517518 * get_address_of_m_colorUp_42() { return &___m_colorUp_42; }
	inline void set_m_colorUp_42(Color32_t874517518  value)
	{
		___m_colorUp_42 = value;
	}

	inline static int32_t get_offset_of_m_useColorDown_43() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_useColorDown_43)); }
	inline bool get_m_useColorDown_43() const { return ___m_useColorDown_43; }
	inline bool* get_address_of_m_useColorDown_43() { return &___m_useColorDown_43; }
	inline void set_m_useColorDown_43(bool value)
	{
		___m_useColorDown_43 = value;
	}

	inline static int32_t get_offset_of_m_colorDown_44() { return static_cast<int32_t>(offsetof(UICornerCut_t1498381979, ___m_colorDown_44)); }
	inline Color32_t874517518  get_m_colorDown_44() const { return ___m_colorDown_44; }
	inline Color32_t874517518 * get_address_of_m_colorDown_44() { return &___m_colorDown_44; }
	inline void set_m_colorDown_44(Color32_t874517518  value)
	{
		___m_colorDown_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICORNERCUT_T1498381979_H
#ifndef UIPOLYGON_T4199244354_H
#define UIPOLYGON_T4199244354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIPolygon
struct  UIPolygon_t4199244354  : public UIPrimitiveBase_t2118950086
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UIPolygon::fill
	bool ___fill_35;
	// System.Single UnityEngine.UI.Extensions.UIPolygon::thickness
	float ___thickness_36;
	// System.Int32 UnityEngine.UI.Extensions.UIPolygon::sides
	int32_t ___sides_37;
	// System.Single UnityEngine.UI.Extensions.UIPolygon::rotation
	float ___rotation_38;
	// System.Single[] UnityEngine.UI.Extensions.UIPolygon::VerticesDistances
	SingleU5BU5D_t577127397* ___VerticesDistances_39;
	// System.Single UnityEngine.UI.Extensions.UIPolygon::size
	float ___size_40;

public:
	inline static int32_t get_offset_of_fill_35() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___fill_35)); }
	inline bool get_fill_35() const { return ___fill_35; }
	inline bool* get_address_of_fill_35() { return &___fill_35; }
	inline void set_fill_35(bool value)
	{
		___fill_35 = value;
	}

	inline static int32_t get_offset_of_thickness_36() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___thickness_36)); }
	inline float get_thickness_36() const { return ___thickness_36; }
	inline float* get_address_of_thickness_36() { return &___thickness_36; }
	inline void set_thickness_36(float value)
	{
		___thickness_36 = value;
	}

	inline static int32_t get_offset_of_sides_37() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___sides_37)); }
	inline int32_t get_sides_37() const { return ___sides_37; }
	inline int32_t* get_address_of_sides_37() { return &___sides_37; }
	inline void set_sides_37(int32_t value)
	{
		___sides_37 = value;
	}

	inline static int32_t get_offset_of_rotation_38() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___rotation_38)); }
	inline float get_rotation_38() const { return ___rotation_38; }
	inline float* get_address_of_rotation_38() { return &___rotation_38; }
	inline void set_rotation_38(float value)
	{
		___rotation_38 = value;
	}

	inline static int32_t get_offset_of_VerticesDistances_39() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___VerticesDistances_39)); }
	inline SingleU5BU5D_t577127397* get_VerticesDistances_39() const { return ___VerticesDistances_39; }
	inline SingleU5BU5D_t577127397** get_address_of_VerticesDistances_39() { return &___VerticesDistances_39; }
	inline void set_VerticesDistances_39(SingleU5BU5D_t577127397* value)
	{
		___VerticesDistances_39 = value;
		Il2CppCodeGenWriteBarrier((&___VerticesDistances_39), value);
	}

	inline static int32_t get_offset_of_size_40() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___size_40)); }
	inline float get_size_40() const { return ___size_40; }
	inline float* get_address_of_size_40() { return &___size_40; }
	inline void set_size_40(float value)
	{
		___size_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOLYGON_T4199244354_H
#ifndef UIGRIDRENDERER_T3756028469_H
#define UIGRIDRENDERER_T3756028469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIGridRenderer
struct  UIGridRenderer_t3756028469  : public UILineRenderer_t3031355003
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UIGridRenderer::m_GridColumns
	int32_t ___m_GridColumns_58;
	// System.Int32 UnityEngine.UI.Extensions.UIGridRenderer::m_GridRows
	int32_t ___m_GridRows_59;

public:
	inline static int32_t get_offset_of_m_GridColumns_58() { return static_cast<int32_t>(offsetof(UIGridRenderer_t3756028469, ___m_GridColumns_58)); }
	inline int32_t get_m_GridColumns_58() const { return ___m_GridColumns_58; }
	inline int32_t* get_address_of_m_GridColumns_58() { return &___m_GridColumns_58; }
	inline void set_m_GridColumns_58(int32_t value)
	{
		___m_GridColumns_58 = value;
	}

	inline static int32_t get_offset_of_m_GridRows_59() { return static_cast<int32_t>(offsetof(UIGridRenderer_t3756028469, ___m_GridRows_59)); }
	inline int32_t get_m_GridRows_59() const { return ___m_GridRows_59; }
	inline int32_t* get_address_of_m_GridRows_59() { return &___m_GridRows_59; }
	inline void set_m_GridRows_59(int32_t value)
	{
		___m_GridRows_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRIDRENDERER_T3756028469_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (MonoSpacing_t901932972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[5] = 
{
	MonoSpacing_t901932972::get_offset_of_m_spacing_3(),
	MonoSpacing_t901932972::get_offset_of_HalfCharWidth_4(),
	MonoSpacing_t901932972::get_offset_of_UseHalfCharWidth_5(),
	MonoSpacing_t901932972::get_offset_of_rectTransform_6(),
	MonoSpacing_t901932972::get_offset_of_text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (NicerOutline_t710191709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[3] = 
{
	NicerOutline_t710191709::get_offset_of_m_EffectColor_3(),
	NicerOutline_t710191709::get_offset_of_m_EffectDistance_4(),
	NicerOutline_t710191709::get_offset_of_m_UseGraphicAlpha_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (RaycastMask_t2005723581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[2] = 
{
	RaycastMask_t2005723581::get_offset_of__image_2(),
	RaycastMask_t2005723581::get_offset_of__sprite_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (UIAdditiveEffect_t2331962695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	UIAdditiveEffect_t2331962695::get_offset_of_mGraphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (UIImageCrop_t3014564719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[6] = 
{
	UIImageCrop_t3014564719::get_offset_of_mGraphic_2(),
	UIImageCrop_t3014564719::get_offset_of_mat_3(),
	UIImageCrop_t3014564719::get_offset_of_XCropProperty_4(),
	UIImageCrop_t3014564719::get_offset_of_YCropProperty_5(),
	UIImageCrop_t3014564719::get_offset_of_XCrop_6(),
	UIImageCrop_t3014564719::get_offset_of_YCrop_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (UILinearDodgeEffect_t1469920893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[1] = 
{
	UILinearDodgeEffect_t1469920893::get_offset_of_mGraphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (UIMultiplyEffect_t1139553371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[1] = 
{
	UIMultiplyEffect_t1139553371::get_offset_of_mGraphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (UIScreenEffect_t688938299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[1] = 
{
	UIScreenEffect_t688938299::get_offset_of_mGraphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (UISoftAdditiveEffect_t1806829255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[1] = 
{
	UISoftAdditiveEffect_t1806829255::get_offset_of_mGraphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (ShineEffect_t1749864756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[2] = 
{
	ShineEffect_t1749864756::get_offset_of_yoffset_28(),
	ShineEffect_t1749864756::get_offset_of_width_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (ShineEffector_t4197735589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[5] = 
{
	ShineEffector_t4197735589::get_offset_of_effector_2(),
	ShineEffector_t4197735589::get_offset_of_effectRoot_3(),
	ShineEffector_t4197735589::get_offset_of_yOffset_4(),
	ShineEffector_t4197735589::get_offset_of_width_5(),
	ShineEffector_t4197735589::get_offset_of_effectorRect_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (SoftMaskScript_t947627439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[13] = 
{
	SoftMaskScript_t947627439::get_offset_of_mat_2(),
	SoftMaskScript_t947627439::get_offset_of_cachedCanvas_3(),
	SoftMaskScript_t947627439::get_offset_of_cachedCanvasTransform_4(),
	SoftMaskScript_t947627439::get_offset_of_m_WorldCorners_5(),
	SoftMaskScript_t947627439::get_offset_of_m_CanvasCorners_6(),
	SoftMaskScript_t947627439::get_offset_of_MaskArea_7(),
	SoftMaskScript_t947627439::get_offset_of_AlphaMask_8(),
	SoftMaskScript_t947627439::get_offset_of_CutOff_9(),
	SoftMaskScript_t947627439::get_offset_of_HardBlend_10(),
	SoftMaskScript_t947627439::get_offset_of_FlipAlphaMask_11(),
	SoftMaskScript_t947627439::get_offset_of_DontClipMaskScalingRect_12(),
	SoftMaskScript_t947627439::get_offset_of_maskOffset_13(),
	SoftMaskScript_t947627439::get_offset_of_maskScale_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (UIFlippable_t725564421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[2] = 
{
	UIFlippable_t725564421::get_offset_of_m_Horizontal_3(),
	UIFlippable_t725564421::get_offset_of_m_Veritical_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (UIParticleSystem_t1209606961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[13] = 
{
	UIParticleSystem_t1209606961::get_offset_of_fixedTime_28(),
	UIParticleSystem_t1209606961::get_offset_of__transform_29(),
	UIParticleSystem_t1209606961::get_offset_of_pSystem_30(),
	UIParticleSystem_t1209606961::get_offset_of_particles_31(),
	UIParticleSystem_t1209606961::get_offset_of__quad_32(),
	UIParticleSystem_t1209606961::get_offset_of_imageUV_33(),
	UIParticleSystem_t1209606961::get_offset_of_textureSheetAnimation_34(),
	UIParticleSystem_t1209606961::get_offset_of_textureSheetAnimationFrames_35(),
	UIParticleSystem_t1209606961::get_offset_of_textureSheetAnimationFrameSize_36(),
	UIParticleSystem_t1209606961::get_offset_of_pRenderer_37(),
	UIParticleSystem_t1209606961::get_offset_of_currentMaterial_38(),
	UIParticleSystem_t1209606961::get_offset_of_currentTexture_39(),
	UIParticleSystem_t1209606961::get_offset_of_mainModule_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (AimerInputModule_t1926645740), -1, sizeof(AimerInputModule_t1926645740_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2114[3] = 
{
	AimerInputModule_t1926645740::get_offset_of_activateAxis_14(),
	AimerInputModule_t1926645740::get_offset_of_aimerOffset_15(),
	AimerInputModule_t1926645740_StaticFields::get_offset_of_objectUnderAimer_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (GamePadInputModule_t1983499109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[9] = 
{
	GamePadInputModule_t1983499109::get_offset_of_m_PrevActionTime_8(),
	GamePadInputModule_t1983499109::get_offset_of_m_LastMoveVector_9(),
	GamePadInputModule_t1983499109::get_offset_of_m_ConsecutiveMoveCount_10(),
	GamePadInputModule_t1983499109::get_offset_of_m_HorizontalAxis_11(),
	GamePadInputModule_t1983499109::get_offset_of_m_VerticalAxis_12(),
	GamePadInputModule_t1983499109::get_offset_of_m_SubmitButton_13(),
	GamePadInputModule_t1983499109::get_offset_of_m_CancelButton_14(),
	GamePadInputModule_t1983499109::get_offset_of_m_InputActionsPerSecond_15(),
	GamePadInputModule_t1983499109::get_offset_of_m_RepeatDelay_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (CurvedLayout_t31468887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[4] = 
{
	CurvedLayout_t31468887::get_offset_of_CurveOffset_10(),
	CurvedLayout_t31468887::get_offset_of_itemAxis_11(),
	CurvedLayout_t31468887::get_offset_of_itemSize_12(),
	CurvedLayout_t31468887::get_offset_of_centerpoint_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (FancyScrollViewNullContext_t4203696347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (FlowLayoutGroup_t3216283979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[7] = 
{
	FlowLayoutGroup_t3216283979::get_offset_of_SpacingX_10(),
	FlowLayoutGroup_t3216283979::get_offset_of_SpacingY_11(),
	FlowLayoutGroup_t3216283979::get_offset_of_ExpandHorizontalSpacing_12(),
	FlowLayoutGroup_t3216283979::get_offset_of_ChildForceExpandWidth_13(),
	FlowLayoutGroup_t3216283979::get_offset_of_ChildForceExpandHeight_14(),
	FlowLayoutGroup_t3216283979::get_offset_of__layoutHeight_15(),
	FlowLayoutGroup_t3216283979::get_offset_of__rowList_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (HorizontalScrollSnap_t3431010905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (RadialLayout_t988531717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[4] = 
{
	RadialLayout_t988531717::get_offset_of_fDistance_10(),
	RadialLayout_t988531717::get_offset_of_MinAngle_11(),
	RadialLayout_t988531717::get_offset_of_MaxAngle_12(),
	RadialLayout_t988531717::get_offset_of_StartAngle_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (ScrollPositionController_t563713522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[21] = 
{
	ScrollPositionController_t563713522::get_offset_of_viewport_2(),
	ScrollPositionController_t563713522::get_offset_of_directionOfRecognize_3(),
	ScrollPositionController_t563713522::get_offset_of_movementType_4(),
	ScrollPositionController_t563713522::get_offset_of_elasticity_5(),
	ScrollPositionController_t563713522::get_offset_of_scrollSensitivity_6(),
	ScrollPositionController_t563713522::get_offset_of_inertia_7(),
	ScrollPositionController_t563713522::get_offset_of_decelerationRate_8(),
	ScrollPositionController_t563713522::get_offset_of_snap_9(),
	ScrollPositionController_t563713522::get_offset_of_dataCount_10(),
	ScrollPositionController_t563713522::get_offset_of_OnUpdatePosition_11(),
	ScrollPositionController_t563713522::get_offset_of_OnItemSelected_12(),
	ScrollPositionController_t563713522::get_offset_of_pointerStartLocalPosition_13(),
	ScrollPositionController_t563713522::get_offset_of_dragStartScrollPosition_14(),
	ScrollPositionController_t563713522::get_offset_of_currentScrollPosition_15(),
	ScrollPositionController_t563713522::get_offset_of_dragging_16(),
	ScrollPositionController_t563713522::get_offset_of_velocity_17(),
	ScrollPositionController_t563713522::get_offset_of_prevScrollPosition_18(),
	ScrollPositionController_t563713522::get_offset_of_autoScrolling_19(),
	ScrollPositionController_t563713522::get_offset_of_autoScrollDuration_20(),
	ScrollPositionController_t563713522::get_offset_of_autoScrollStartTime_21(),
	ScrollPositionController_t563713522::get_offset_of_autoScrollPosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (UpdatePositionEvent_t4030317844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (ItemSelectedEvent_t2604723240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (Snap_t1413478282)+ sizeof (RuntimeObject), sizeof(Snap_t1413478282_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2129[3] = 
{
	Snap_t1413478282::get_offset_of_Enable_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Snap_t1413478282::get_offset_of_VelocityThreshold_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Snap_t1413478282::get_offset_of_Duration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (ScrollDirection_t1610447880)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2130[3] = 
{
	ScrollDirection_t1610447880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (MovementType_t3465211187)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2131[4] = 
{
	MovementType_t3465211187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (ScrollSnap_t2261190493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[32] = 
{
	ScrollSnap_t2261190493::get_offset_of__scroll_rect_2(),
	ScrollSnap_t2261190493::get_offset_of__scrollRectTransform_3(),
	ScrollSnap_t2261190493::get_offset_of__listContainerTransform_4(),
	ScrollSnap_t2261190493::get_offset_of__pages_5(),
	ScrollSnap_t2261190493::get_offset_of__startingPage_6(),
	ScrollSnap_t2261190493::get_offset_of__pageAnchorPositions_7(),
	ScrollSnap_t2261190493::get_offset_of__lerpTarget_8(),
	ScrollSnap_t2261190493::get_offset_of__lerp_9(),
	ScrollSnap_t2261190493::get_offset_of__listContainerMinPosition_10(),
	ScrollSnap_t2261190493::get_offset_of__listContainerMaxPosition_11(),
	ScrollSnap_t2261190493::get_offset_of__listContainerSize_12(),
	ScrollSnap_t2261190493::get_offset_of__listContainerRectTransform_13(),
	ScrollSnap_t2261190493::get_offset_of__listContainerCachedSize_14(),
	ScrollSnap_t2261190493::get_offset_of__itemSize_15(),
	ScrollSnap_t2261190493::get_offset_of__itemsCount_16(),
	ScrollSnap_t2261190493::get_offset_of__startDrag_17(),
	ScrollSnap_t2261190493::get_offset_of__positionOnDragStart_18(),
	ScrollSnap_t2261190493::get_offset_of__pageOnDragStart_19(),
	ScrollSnap_t2261190493::get_offset_of__fastSwipeTimer_20(),
	ScrollSnap_t2261190493::get_offset_of__fastSwipeCounter_21(),
	ScrollSnap_t2261190493::get_offset_of__fastSwipeTarget_22(),
	ScrollSnap_t2261190493::get_offset_of_NextButton_23(),
	ScrollSnap_t2261190493::get_offset_of_PrevButton_24(),
	ScrollSnap_t2261190493::get_offset_of_ItemsVisibleAtOnce_25(),
	ScrollSnap_t2261190493::get_offset_of_AutoLayoutItems_26(),
	ScrollSnap_t2261190493::get_offset_of_LinkScrolbarSteps_27(),
	ScrollSnap_t2261190493::get_offset_of_LinkScrolrectScrollSensitivity_28(),
	ScrollSnap_t2261190493::get_offset_of_UseFastSwipe_29(),
	ScrollSnap_t2261190493::get_offset_of_FastSwipeThreshold_30(),
	ScrollSnap_t2261190493::get_offset_of_onPageChange_31(),
	ScrollSnap_t2261190493::get_offset_of_direction_32(),
	ScrollSnap_t2261190493::get_offset_of_fastSwipe_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (ScrollDirection_t778749221)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[3] = 
{
	ScrollDirection_t778749221::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (PageSnapChange_t737912504), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (ScrollSnapBase_t805675194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[39] = 
{
	ScrollSnapBase_t805675194::get_offset_of_panelDimensions_2(),
	ScrollSnapBase_t805675194::get_offset_of__screensContainer_3(),
	ScrollSnapBase_t805675194::get_offset_of__isVertical_4(),
	ScrollSnapBase_t805675194::get_offset_of__screens_5(),
	ScrollSnapBase_t805675194::get_offset_of__scrollStartPosition_6(),
	ScrollSnapBase_t805675194::get_offset_of__childSize_7(),
	ScrollSnapBase_t805675194::get_offset_of__childPos_8(),
	ScrollSnapBase_t805675194::get_offset_of__maskSize_9(),
	ScrollSnapBase_t805675194::get_offset_of__childAnchorPoint_10(),
	ScrollSnapBase_t805675194::get_offset_of__scroll_rect_11(),
	ScrollSnapBase_t805675194::get_offset_of__lerp_target_12(),
	ScrollSnapBase_t805675194::get_offset_of__lerp_13(),
	ScrollSnapBase_t805675194::get_offset_of__pointerDown_14(),
	ScrollSnapBase_t805675194::get_offset_of__settled_15(),
	ScrollSnapBase_t805675194::get_offset_of__startPosition_16(),
	ScrollSnapBase_t805675194::get_offset_of__currentPage_17(),
	ScrollSnapBase_t805675194::get_offset_of__previousPage_18(),
	ScrollSnapBase_t805675194::get_offset_of__halfNoVisibleItems_19(),
	ScrollSnapBase_t805675194::get_offset_of__moveStarted_20(),
	ScrollSnapBase_t805675194::get_offset_of__bottomItem_21(),
	ScrollSnapBase_t805675194::get_offset_of__topItem_22(),
	ScrollSnapBase_t805675194::get_offset_of_StartingScreen_23(),
	ScrollSnapBase_t805675194::get_offset_of_PageStep_24(),
	ScrollSnapBase_t805675194::get_offset_of_Pagination_25(),
	ScrollSnapBase_t805675194::get_offset_of_PrevButton_26(),
	ScrollSnapBase_t805675194::get_offset_of_NextButton_27(),
	ScrollSnapBase_t805675194::get_offset_of_transitionSpeed_28(),
	ScrollSnapBase_t805675194::get_offset_of_UseFastSwipe_29(),
	ScrollSnapBase_t805675194::get_offset_of_FastSwipeThreshold_30(),
	ScrollSnapBase_t805675194::get_offset_of_SwipeVelocityThreshold_31(),
	ScrollSnapBase_t805675194::get_offset_of_MaskArea_32(),
	ScrollSnapBase_t805675194::get_offset_of_MaskBuffer_33(),
	ScrollSnapBase_t805675194::get_offset_of_JumpOnEnable_34(),
	ScrollSnapBase_t805675194::get_offset_of_RestartOnEnable_35(),
	ScrollSnapBase_t805675194::get_offset_of_UseParentTransform_36(),
	ScrollSnapBase_t805675194::get_offset_of_ChildObjects_37(),
	ScrollSnapBase_t805675194::get_offset_of_m_OnSelectionChangeStartEvent_38(),
	ScrollSnapBase_t805675194::get_offset_of_m_OnSelectionPageChangedEvent_39(),
	ScrollSnapBase_t805675194::get_offset_of_m_OnSelectionChangeEndEvent_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (SelectionChangeStartEvent_t1331424750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (SelectionPageChangedEvent_t3967268665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (SelectionChangeEndEvent_t3994187929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (ScrollSnapScrollbarHelper_t403895447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[1] = 
{
	ScrollSnapScrollbarHelper_t403895447::get_offset_of_ss_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (TableLayoutGroup_t2874982773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[7] = 
{
	TableLayoutGroup_t2874982773::get_offset_of_startCorner_10(),
	TableLayoutGroup_t2874982773::get_offset_of_columnWidths_11(),
	TableLayoutGroup_t2874982773::get_offset_of_minimumRowHeight_12(),
	TableLayoutGroup_t2874982773::get_offset_of_flexibleRowHeight_13(),
	TableLayoutGroup_t2874982773::get_offset_of_columnSpacing_14(),
	TableLayoutGroup_t2874982773::get_offset_of_rowSpacing_15(),
	TableLayoutGroup_t2874982773::get_offset_of_preferredRowHeights_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (Corner_t3586515314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2141[5] = 
{
	Corner_t3586515314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (TileSizeFitter_t2558755467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[4] = 
{
	TileSizeFitter_t2558755467::get_offset_of_m_Border_2(),
	TileSizeFitter_t2558755467::get_offset_of_m_TileSize_3(),
	TileSizeFitter_t2558755467::get_offset_of_m_Rect_4(),
	TileSizeFitter_t2558755467::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (UIVerticalScroller_t1840308474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[13] = 
{
	UIVerticalScroller_t1840308474::get_offset_of__scrollingPanel_2(),
	UIVerticalScroller_t1840308474::get_offset_of__arrayOfElements_3(),
	UIVerticalScroller_t1840308474::get_offset_of__center_4(),
	UIVerticalScroller_t1840308474::get_offset_of_StartingIndex_5(),
	UIVerticalScroller_t1840308474::get_offset_of_ScrollUpButton_6(),
	UIVerticalScroller_t1840308474::get_offset_of_ScrollDownButton_7(),
	UIVerticalScroller_t1840308474::get_offset_of_ButtonClicked_8(),
	UIVerticalScroller_t1840308474::get_offset_of_distReposition_9(),
	UIVerticalScroller_t1840308474::get_offset_of_distance_10(),
	UIVerticalScroller_t1840308474::get_offset_of_minElementsNum_11(),
	UIVerticalScroller_t1840308474::get_offset_of_elementLength_12(),
	UIVerticalScroller_t1840308474::get_offset_of_deltaY_13(),
	UIVerticalScroller_t1840308474::get_offset_of_result_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (U3CAddListenerU3Ec__AnonStorey0_t2102329242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[2] = 
{
	U3CAddListenerU3Ec__AnonStorey0_t2102329242::get_offset_of_index_0(),
	U3CAddListenerU3Ec__AnonStorey0_t2102329242::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (VerticalScrollSnap_t4045580255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (Menu_t1209672415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[2] = 
{
	Menu_t1209672415::get_offset_of_DestroyWhenClosed_2(),
	Menu_t1209672415::get_offset_of_DisableMenusUnderneath_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (MenuManager_t3446170766), -1, sizeof(MenuManager_t3446170766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2148[4] = 
{
	MenuManager_t3446170766::get_offset_of_MenuScreens_2(),
	MenuManager_t3446170766::get_offset_of_StartScreen_3(),
	MenuManager_t3446170766::get_offset_of_menuStack_4(),
	MenuManager_t3446170766_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (DiamondGraph_t80153826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[4] = 
{
	DiamondGraph_t80153826::get_offset_of_m_a_35(),
	DiamondGraph_t80153826::get_offset_of_m_b_36(),
	DiamondGraph_t80153826::get_offset_of_m_c_37(),
	DiamondGraph_t80153826::get_offset_of_m_d_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (UICircle_t2117378996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[5] = 
{
	UICircle_t2117378996::get_offset_of_m_fillPercent_35(),
	UICircle_t2117378996::get_offset_of_FixedToSegments_36(),
	UICircle_t2117378996::get_offset_of_m_fill_37(),
	UICircle_t2117378996::get_offset_of_m_thickness_38(),
	UICircle_t2117378996::get_offset_of_m_segments_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (UICornerCut_t1498381979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[10] = 
{
	UICornerCut_t1498381979::get_offset_of_cornerSize_35(),
	UICornerCut_t1498381979::get_offset_of_m_cutUL_36(),
	UICornerCut_t1498381979::get_offset_of_m_cutUR_37(),
	UICornerCut_t1498381979::get_offset_of_m_cutLL_38(),
	UICornerCut_t1498381979::get_offset_of_m_cutLR_39(),
	UICornerCut_t1498381979::get_offset_of_m_makeColumns_40(),
	UICornerCut_t1498381979::get_offset_of_m_useColorUp_41(),
	UICornerCut_t1498381979::get_offset_of_m_colorUp_42(),
	UICornerCut_t1498381979::get_offset_of_m_useColorDown_43(),
	UICornerCut_t1498381979::get_offset_of_m_colorDown_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (UIGridRenderer_t3756028469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[2] = 
{
	UIGridRenderer_t3756028469::get_offset_of_m_GridColumns_58(),
	UIGridRenderer_t3756028469::get_offset_of_m_GridRows_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (UILineRenderer_t3031355003), -1, sizeof(UILineRenderer_t3031355003_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2154[23] = 
{
	0,
	0,
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_TOP_LEFT_37(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_BOTTOM_LEFT_38(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_TOP_CENTER_LEFT_39(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_TOP_CENTER_RIGHT_40(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_BOTTOM_CENTER_LEFT_41(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_BOTTOM_CENTER_RIGHT_42(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_TOP_RIGHT_43(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_UV_BOTTOM_RIGHT_44(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_startUvs_45(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_middleUvs_46(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_endUvs_47(),
	UILineRenderer_t3031355003_StaticFields::get_offset_of_fullUvs_48(),
	UILineRenderer_t3031355003::get_offset_of_m_points_49(),
	UILineRenderer_t3031355003::get_offset_of_lineThickness_50(),
	UILineRenderer_t3031355003::get_offset_of_relativeSize_51(),
	UILineRenderer_t3031355003::get_offset_of_lineList_52(),
	UILineRenderer_t3031355003::get_offset_of_lineCaps_53(),
	UILineRenderer_t3031355003::get_offset_of_bezierSegmentsPerCurve_54(),
	UILineRenderer_t3031355003::get_offset_of_LineJoins_55(),
	UILineRenderer_t3031355003::get_offset_of_BezierMode_56(),
	UILineRenderer_t3031355003::get_offset_of_drivenExternally_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (SegmentType_t3639054610)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[5] = 
{
	SegmentType_t3639054610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (JoinType_t3583171457)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[3] = 
{
	JoinType_t3583171457::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (BezierType_t670022450)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2157[6] = 
{
	BezierType_t670022450::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (UILineTextureRenderer_t1203126486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[6] = 
{
	UILineTextureRenderer_t1203126486::get_offset_of_m_UVRect_35(),
	UILineTextureRenderer_t1203126486::get_offset_of_m_points_36(),
	UILineTextureRenderer_t1203126486::get_offset_of_LineThickness_37(),
	UILineTextureRenderer_t1203126486::get_offset_of_UseMargins_38(),
	UILineTextureRenderer_t1203126486::get_offset_of_Margin_39(),
	UILineTextureRenderer_t1203126486::get_offset_of_relativeSize_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (UIPolygon_t4199244354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[6] = 
{
	UIPolygon_t4199244354::get_offset_of_fill_35(),
	UIPolygon_t4199244354::get_offset_of_thickness_36(),
	UIPolygon_t4199244354::get_offset_of_sides_37(),
	UIPolygon_t4199244354::get_offset_of_rotation_38(),
	UIPolygon_t4199244354::get_offset_of_VerticesDistances_39(),
	UIPolygon_t4199244354::get_offset_of_size_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (ResolutionMode_t1915409729)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[4] = 
{
	ResolutionMode_t1915409729::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (UIPrimitiveBase_t2118950086), -1, sizeof(UIPrimitiveBase_t2118950086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[7] = 
{
	UIPrimitiveBase_t2118950086_StaticFields::get_offset_of_s_ETC1DefaultUI_28(),
	UIPrimitiveBase_t2118950086::get_offset_of_m_Sprite_29(),
	UIPrimitiveBase_t2118950086::get_offset_of_m_OverrideSprite_30(),
	UIPrimitiveBase_t2118950086::get_offset_of_m_EventAlphaThreshold_31(),
	UIPrimitiveBase_t2118950086::get_offset_of_m_improveResolution_32(),
	UIPrimitiveBase_t2118950086::get_offset_of_m_Resolution_33(),
	UIPrimitiveBase_t2118950086::get_offset_of_m_useNativeSize_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (NavigationMode_t1772877217)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[3] = 
{
	NavigationMode_t1772877217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (TabNavigationHelper_t538601999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[6] = 
{
	TabNavigationHelper_t538601999::get_offset_of__system_2(),
	TabNavigationHelper_t538601999::get_offset_of_StartingObject_3(),
	TabNavigationHelper_t538601999::get_offset_of_LastObject_4(),
	TabNavigationHelper_t538601999::get_offset_of_NavigationPath_5(),
	TabNavigationHelper_t538601999::get_offset_of_NavigationMode_6(),
	TabNavigationHelper_t538601999::get_offset_of_CircularNavigation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (BoundTooltipItem_t3449975380), -1, sizeof(BoundTooltipItem_t3449975380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[3] = 
{
	BoundTooltipItem_t3449975380::get_offset_of_TooltipText_2(),
	BoundTooltipItem_t3449975380::get_offset_of_ToolTipOffset_3(),
	BoundTooltipItem_t3449975380_StaticFields::get_offset_of_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (BoundTooltipTrigger_t709210963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[3] = 
{
	BoundTooltipTrigger_t709210963::get_offset_of_text_2(),
	BoundTooltipTrigger_t709210963::get_offset_of_useMousePosition_3(),
	BoundTooltipTrigger_t709210963::get_offset_of_offset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (HoverTooltip_t3001491549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[18] = 
{
	HoverTooltip_t3001491549::get_offset_of_horizontalPadding_2(),
	HoverTooltip_t3001491549::get_offset_of_verticalPadding_3(),
	HoverTooltip_t3001491549::get_offset_of_thisText_4(),
	HoverTooltip_t3001491549::get_offset_of_hlG_5(),
	HoverTooltip_t3001491549::get_offset_of_bgImage_6(),
	HoverTooltip_t3001491549::get_offset_of_bgImageSource_7(),
	HoverTooltip_t3001491549::get_offset_of_firstUpdate_8(),
	HoverTooltip_t3001491549::get_offset_of_inside_9(),
	HoverTooltip_t3001491549::get_offset_of_GUIMode_10(),
	HoverTooltip_t3001491549::get_offset_of_GUICamera_11(),
	HoverTooltip_t3001491549::get_offset_of_lowerLeft_12(),
	HoverTooltip_t3001491549::get_offset_of_upperRight_13(),
	HoverTooltip_t3001491549::get_offset_of_currentYScaleFactor_14(),
	HoverTooltip_t3001491549::get_offset_of_currentXScaleFactor_15(),
	HoverTooltip_t3001491549::get_offset_of_defaultYOffset_16(),
	HoverTooltip_t3001491549::get_offset_of_defaultXOffset_17(),
	HoverTooltip_t3001491549::get_offset_of_tooltipRealHeight_18(),
	HoverTooltip_t3001491549::get_offset_of_tooltipRealWidth_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (ToolTip_t1538012217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[9] = 
{
	ToolTip_t1538012217::get_offset_of__text_2(),
	ToolTip_t1538012217::get_offset_of__rectTransform_3(),
	ToolTip_t1538012217::get_offset_of__inside_4(),
	ToolTip_t1538012217::get_offset_of_width_5(),
	ToolTip_t1538012217::get_offset_of_height_6(),
	ToolTip_t1538012217::get_offset_of_YShift_7(),
	ToolTip_t1538012217::get_offset_of_xShift_8(),
	ToolTip_t1538012217::get_offset_of__guiMode_9(),
	ToolTip_t1538012217::get_offset_of__guiCamera_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (UIWindowBase_t1223857951), -1, sizeof(UIWindowBase_t1223857951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2168[7] = 
{
	UIWindowBase_t1223857951::get_offset_of_m_transform_2(),
	UIWindowBase_t1223857951::get_offset_of__isDragging_3(),
	UIWindowBase_t1223857951_StaticFields::get_offset_of_ResetCoords_4(),
	UIWindowBase_t1223857951::get_offset_of_m_originalCoods_5(),
	UIWindowBase_t1223857951::get_offset_of_m_canvas_6(),
	UIWindowBase_t1223857951::get_offset_of_m_canvasRectTransform_7(),
	UIWindowBase_t1223857951::get_offset_of_KeepWindowInCanvas_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (BezierPath_t875060952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[5] = 
{
	BezierPath_t875060952::get_offset_of_SegmentsPerCurve_0(),
	BezierPath_t875060952::get_offset_of_MINIMUM_SQR_DISTANCE_1(),
	BezierPath_t875060952::get_offset_of_DIVISION_THRESHOLD_2(),
	BezierPath_t875060952::get_offset_of_controlPoints_3(),
	BezierPath_t875060952::get_offset_of_curveCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (CableCurve_t3653995216), -1, sizeof(CableCurve_t3653995216_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2170[7] = 
{
	CableCurve_t3653995216::get_offset_of_m_start_0(),
	CableCurve_t3653995216::get_offset_of_m_end_1(),
	CableCurve_t3653995216::get_offset_of_m_slack_2(),
	CableCurve_t3653995216::get_offset_of_m_steps_3(),
	CableCurve_t3653995216::get_offset_of_m_regen_4(),
	CableCurve_t3653995216_StaticFields::get_offset_of_emptyCurve_5(),
	CableCurve_t3653995216::get_offset_of_points_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (Circle_t1341526964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[3] = 
{
	Circle_t1341526964::get_offset_of_xAxis_0(),
	Circle_t1341526964::get_offset_of_yAxis_1(),
	Circle_t1341526964::get_offset_of_steps_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (CLZF2_t824178053), -1, sizeof(CLZF2_t824178053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2172[6] = 
{
	CLZF2_t824178053_StaticFields::get_offset_of_HLOG_0(),
	CLZF2_t824178053_StaticFields::get_offset_of_HSIZE_1(),
	CLZF2_t824178053_StaticFields::get_offset_of_MAX_LIT_2(),
	CLZF2_t824178053_StaticFields::get_offset_of_MAX_OFF_3(),
	CLZF2_t824178053_StaticFields::get_offset_of_MAX_REF_4(),
	CLZF2_t824178053_StaticFields::get_offset_of_HashTable_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (DragCorrector_t1392823991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[3] = 
{
	DragCorrector_t1392823991::get_offset_of_baseTH_2(),
	DragCorrector_t1392823991::get_offset_of_basePPI_3(),
	DragCorrector_t1392823991::get_offset_of_dragTH_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (ExtensionsToggle_t2639893278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[7] = 
{
	ExtensionsToggle_t2639893278::get_offset_of_UniqueID_16(),
	ExtensionsToggle_t2639893278::get_offset_of_toggleTransition_17(),
	ExtensionsToggle_t2639893278::get_offset_of_graphic_18(),
	ExtensionsToggle_t2639893278::get_offset_of_m_Group_19(),
	ExtensionsToggle_t2639893278::get_offset_of_onValueChanged_20(),
	ExtensionsToggle_t2639893278::get_offset_of_onToggleChanged_21(),
	ExtensionsToggle_t2639893278::get_offset_of_m_IsOn_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ToggleTransition_t2798030653)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2175[3] = 
{
	ToggleTransition_t2798030653::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (ToggleEvent_t2514518788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (ToggleEventObject_t2759065715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (ExtensionsToggleGroup_t1289496689), -1, sizeof(ExtensionsToggleGroup_t1289496689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2178[7] = 
{
	ExtensionsToggleGroup_t1289496689::get_offset_of_m_AllowSwitchOff_2(),
	ExtensionsToggleGroup_t1289496689::get_offset_of_m_Toggles_3(),
	ExtensionsToggleGroup_t1289496689::get_offset_of_onToggleGroupChanged_4(),
	ExtensionsToggleGroup_t1289496689::get_offset_of_onToggleGroupToggleChanged_5(),
	ExtensionsToggleGroup_t1289496689::get_offset_of_U3CSelectedToggleU3Ek__BackingField_6(),
	ExtensionsToggleGroup_t1289496689_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	ExtensionsToggleGroup_t1289496689_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (ToggleGroupEvent_t1021581118), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (InputFieldEnterSubmit_t1520170634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[2] = 
{
	InputFieldEnterSubmit_t1520170634::get_offset_of_EnterSubmit_2(),
	InputFieldEnterSubmit_t1520170634::get_offset_of__input_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (EnterSubmitEvent_t3688949694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (NonDrawingGraphic_t49509067), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (PaginationManager_t1455408585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[3] = 
{
	PaginationManager_t1455408585::get_offset_of_m_PaginationChildren_6(),
	PaginationManager_t1455408585::get_offset_of_scrollSnap_7(),
	PaginationManager_t1455408585::get_offset_of_isAClick_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (PPIViewer_t479698991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[1] = 
{
	PPIViewer_t479698991::get_offset_of_label_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (ReadOnlyAttribute_t2117389128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (ReturnKeyTriggersButton_t1668641634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[4] = 
{
	ReturnKeyTriggersButton_t1668641634::get_offset_of__system_2(),
	ReturnKeyTriggersButton_t1668641634::get_offset_of_button_3(),
	ReturnKeyTriggersButton_t1668641634::get_offset_of_highlight_4(),
	ReturnKeyTriggersButton_t1668641634::get_offset_of_highlightDuration_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (ScrollConflictManager_t4227891694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[4] = 
{
	ScrollConflictManager_t4227891694::get_offset_of_ParentScrollRect_2(),
	ScrollConflictManager_t4227891694::get_offset_of__myScrollRect_3(),
	ScrollConflictManager_t4227891694::get_offset_of_scrollOther_4(),
	ScrollConflictManager_t4227891694::get_offset_of_scrollOtherHorizontally_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (ScrollRectEx_t4232992262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[1] = 
{
	ScrollRectEx_t4232992262::get_offset_of_routeToParent_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (U3COnInitializePotentialDragU3Ec__AnonStorey0_t4251053400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[1] = 
{
	U3COnInitializePotentialDragU3Ec__AnonStorey0_t4251053400::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (U3COnDragU3Ec__AnonStorey1_t2404555749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[1] = 
{
	U3COnDragU3Ec__AnonStorey1_t2404555749::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (U3COnBeginDragU3Ec__AnonStorey2_t3035827229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[1] = 
{
	U3COnBeginDragU3Ec__AnonStorey2_t3035827229::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (U3COnEndDragU3Ec__AnonStorey3_t2954120386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[1] = 
{
	U3COnEndDragU3Ec__AnonStorey3_t2954120386::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (U3COnScrollU3Ec__AnonStorey4_t4166373289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[1] = 
{
	U3COnScrollU3Ec__AnonStorey4_t4166373289::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (ScrollRectExtensions_t449593713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (ScrollRectLinker_t3928490044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[3] = 
{
	ScrollRectLinker_t3928490044::get_offset_of_clamp_2(),
	ScrollRectLinker_t3928490044::get_offset_of_controllingScrollRect_3(),
	ScrollRectLinker_t3928490044::get_offset_of_scrollRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ScrollRectTweener_t3873690283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[7] = 
{
	ScrollRectTweener_t3873690283::get_offset_of_scrollRect_2(),
	ScrollRectTweener_t3873690283::get_offset_of_startPos_3(),
	ScrollRectTweener_t3873690283::get_offset_of_targetPos_4(),
	ScrollRectTweener_t3873690283::get_offset_of_wasHorizontal_5(),
	ScrollRectTweener_t3873690283::get_offset_of_wasVertical_6(),
	ScrollRectTweener_t3873690283::get_offset_of_moveSpeed_7(),
	ScrollRectTweener_t3873690283::get_offset_of_disableDragWhileTweening_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (U3CDoMoveU3Ec__Iterator0_t945211788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[7] = 
{
	U3CDoMoveU3Ec__Iterator0_t945211788::get_offset_of_duration_0(),
	U3CDoMoveU3Ec__Iterator0_t945211788::get_offset_of_U3CposOffsetU3E__0_1(),
	U3CDoMoveU3Ec__Iterator0_t945211788::get_offset_of_U3CcurrentTimeU3E__0_2(),
	U3CDoMoveU3Ec__Iterator0_t945211788::get_offset_of_U24this_3(),
	U3CDoMoveU3Ec__Iterator0_t945211788::get_offset_of_U24current_4(),
	U3CDoMoveU3Ec__Iterator0_t945211788::get_offset_of_U24disposing_5(),
	U3CDoMoveU3Ec__Iterator0_t945211788::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (SelectableScaler_t1409550150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[5] = 
{
	SelectableScaler_t1409550150::get_offset_of_animCurve_2(),
	SelectableScaler_t1409550150::get_offset_of_speed_3(),
	SelectableScaler_t1409550150::get_offset_of_initScale_4(),
	SelectableScaler_t1409550150::get_offset_of_target_5(),
	SelectableScaler_t1409550150::get_offset_of_selectable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (U3CScaleINU3Ec__Iterator0_t463378026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[6] = 
{
	U3CScaleINU3Ec__Iterator0_t463378026::get_offset_of_U3CtU3E__1_0(),
	U3CScaleINU3Ec__Iterator0_t463378026::get_offset_of_U3CmaxTU3E__1_1(),
	U3CScaleINU3Ec__Iterator0_t463378026::get_offset_of_U24this_2(),
	U3CScaleINU3Ec__Iterator0_t463378026::get_offset_of_U24current_3(),
	U3CScaleINU3Ec__Iterator0_t463378026::get_offset_of_U24disposing_4(),
	U3CScaleINU3Ec__Iterator0_t463378026::get_offset_of_U24PC_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
