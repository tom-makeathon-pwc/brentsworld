﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;

extern RuntimeClass* MinMaxCurve_t122563058_il2cpp_TypeInfo_var;
extern const uint32_t EmissionModule_get_rateOverTime_m287157162_MetadataUsageId;
struct AnimationCurve_t3306541151_marshaled_pinvoke;
struct AnimationCurve_t3306541151;;
struct AnimationCurve_t3306541151_marshaled_pinvoke;;
extern RuntimeClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxCurve_t122563058_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct AnimationCurve_t3306541151_marshaled_com;
struct AnimationCurve_t3306541151_marshaled_com;;
extern const uint32_t TextureSheetAnimationModule_get_frameOverTime_m1544335400_MetadataUsageId;

struct ParticleU5BU5D_t574222242;


#ifndef U3CMODULEU3E_T3783534225_H
#define U3CMODULEU3E_T3783534225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534225 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534225_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TEXTURESHEETANIMATIONMODULE_T4262561859_H
#define TEXTURESHEETANIMATIONMODULE_T4262561859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct  TextureSheetAnimationModule_t4262561859 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/TextureSheetAnimationModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(TextureSheetAnimationModule_t4262561859, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t4262561859_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t4262561859_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // TEXTURESHEETANIMATIONMODULE_T4262561859_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef EXTERNALFORCESMODULE_T2596349650_H
#define EXTERNALFORCESMODULE_T2596349650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/ExternalForcesModule
struct  ExternalForcesModule_t2596349650 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ExternalForcesModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ExternalForcesModule_t2596349650, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ExternalForcesModule
struct ExternalForcesModule_t2596349650_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ExternalForcesModule
struct ExternalForcesModule_t2596349650_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // EXTERNALFORCESMODULE_T2596349650_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef EMISSIONMODULE_T2748003162_H
#define EMISSIONMODULE_T2748003162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/EmissionModule
struct  EmissionModule_t2748003162 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/EmissionModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_t2748003162, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t2748003162_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t2748003162_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // EMISSIONMODULE_T2748003162_H
#ifndef MAINMODULE_T6751348_H
#define MAINMODULE_T6751348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t6751348 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t6751348, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T6751348_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef PARTICLESYSTEMANIMATIONTYPE_T509305767_H
#define PARTICLESYSTEMANIMATIONTYPE_T509305767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemAnimationType
struct  ParticleSystemAnimationType_t509305767 
{
public:
	// System.Int32 UnityEngine.ParticleSystemAnimationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemAnimationType_t509305767, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMANIMATIONTYPE_T509305767_H
#ifndef ANIMATIONCURVE_T3306541151_H
#define ANIMATIONCURVE_T3306541151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3306541151  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3306541151, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3306541151_H
#ifndef PARTICLESYSTEMSTOPBEHAVIOR_T3921148531_H
#define PARTICLESYSTEMSTOPBEHAVIOR_T3921148531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemStopBehavior
struct  ParticleSystemStopBehavior_t3921148531 
{
public:
	// System.Int32 UnityEngine.ParticleSystemStopBehavior::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemStopBehavior_t3921148531, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSTOPBEHAVIOR_T3921148531_H
#ifndef PARTICLESYSTEMSIMULATIONSPACE_T3554150940_H
#define PARTICLESYSTEMSIMULATIONSPACE_T3554150940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemSimulationSpace
struct  ParticleSystemSimulationSpace_t3554150940 
{
public:
	// System.Int32 UnityEngine.ParticleSystemSimulationSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemSimulationSpace_t3554150940, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSIMULATIONSPACE_T3554150940_H
#ifndef PARTICLE_T250075699_H
#define PARTICLE_T250075699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/Particle
struct  Particle_t250075699 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t2243707580  ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t2243707580  ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t2243707580  ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t2243707580  ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t2243707580  ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t2243707580  ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t2243707580  ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t2243707580  ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t874517518  ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_12;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_13;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Position_0)); }
	inline Vector3_t2243707580  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_t2243707580 * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_t2243707580  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Velocity_1)); }
	inline Vector3_t2243707580  get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline Vector3_t2243707580 * get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(Vector3_t2243707580  value)
	{
		___m_Velocity_1 = value;
	}

	inline static int32_t get_offset_of_m_AnimatedVelocity_2() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_AnimatedVelocity_2)); }
	inline Vector3_t2243707580  get_m_AnimatedVelocity_2() const { return ___m_AnimatedVelocity_2; }
	inline Vector3_t2243707580 * get_address_of_m_AnimatedVelocity_2() { return &___m_AnimatedVelocity_2; }
	inline void set_m_AnimatedVelocity_2(Vector3_t2243707580  value)
	{
		___m_AnimatedVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_InitialVelocity_3() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_InitialVelocity_3)); }
	inline Vector3_t2243707580  get_m_InitialVelocity_3() const { return ___m_InitialVelocity_3; }
	inline Vector3_t2243707580 * get_address_of_m_InitialVelocity_3() { return &___m_InitialVelocity_3; }
	inline void set_m_InitialVelocity_3(Vector3_t2243707580  value)
	{
		___m_InitialVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotation_4() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_AxisOfRotation_4)); }
	inline Vector3_t2243707580  get_m_AxisOfRotation_4() const { return ___m_AxisOfRotation_4; }
	inline Vector3_t2243707580 * get_address_of_m_AxisOfRotation_4() { return &___m_AxisOfRotation_4; }
	inline void set_m_AxisOfRotation_4(Vector3_t2243707580  value)
	{
		___m_AxisOfRotation_4 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Rotation_5)); }
	inline Vector3_t2243707580  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Vector3_t2243707580 * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Vector3_t2243707580  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocity_6() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_AngularVelocity_6)); }
	inline Vector3_t2243707580  get_m_AngularVelocity_6() const { return ___m_AngularVelocity_6; }
	inline Vector3_t2243707580 * get_address_of_m_AngularVelocity_6() { return &___m_AngularVelocity_6; }
	inline void set_m_AngularVelocity_6(Vector3_t2243707580  value)
	{
		___m_AngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_StartSize_7() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_StartSize_7)); }
	inline Vector3_t2243707580  get_m_StartSize_7() const { return ___m_StartSize_7; }
	inline Vector3_t2243707580 * get_address_of_m_StartSize_7() { return &___m_StartSize_7; }
	inline void set_m_StartSize_7(Vector3_t2243707580  value)
	{
		___m_StartSize_7 = value;
	}

	inline static int32_t get_offset_of_m_StartColor_8() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_StartColor_8)); }
	inline Color32_t874517518  get_m_StartColor_8() const { return ___m_StartColor_8; }
	inline Color32_t874517518 * get_address_of_m_StartColor_8() { return &___m_StartColor_8; }
	inline void set_m_StartColor_8(Color32_t874517518  value)
	{
		___m_StartColor_8 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_9() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_RandomSeed_9)); }
	inline uint32_t get_m_RandomSeed_9() const { return ___m_RandomSeed_9; }
	inline uint32_t* get_address_of_m_RandomSeed_9() { return &___m_RandomSeed_9; }
	inline void set_m_RandomSeed_9(uint32_t value)
	{
		___m_RandomSeed_9 = value;
	}

	inline static int32_t get_offset_of_m_Lifetime_10() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Lifetime_10)); }
	inline float get_m_Lifetime_10() const { return ___m_Lifetime_10; }
	inline float* get_address_of_m_Lifetime_10() { return &___m_Lifetime_10; }
	inline void set_m_Lifetime_10(float value)
	{
		___m_Lifetime_10 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetime_11() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_StartLifetime_11)); }
	inline float get_m_StartLifetime_11() const { return ___m_StartLifetime_11; }
	inline float* get_address_of_m_StartLifetime_11() { return &___m_StartLifetime_11; }
	inline void set_m_StartLifetime_11(float value)
	{
		___m_StartLifetime_11 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator0_12() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_EmitAccumulator0_12)); }
	inline float get_m_EmitAccumulator0_12() const { return ___m_EmitAccumulator0_12; }
	inline float* get_address_of_m_EmitAccumulator0_12() { return &___m_EmitAccumulator0_12; }
	inline void set_m_EmitAccumulator0_12(float value)
	{
		___m_EmitAccumulator0_12 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator1_13() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_EmitAccumulator1_13)); }
	inline float get_m_EmitAccumulator1_13() const { return ___m_EmitAccumulator1_13; }
	inline float* get_address_of_m_EmitAccumulator1_13() { return &___m_EmitAccumulator1_13; }
	inline void set_m_EmitAccumulator1_13(float value)
	{
		___m_EmitAccumulator1_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLE_T250075699_H
#ifndef PARTICLESYSTEMCURVEMODE_T1659312557_H
#define PARTICLESYSTEMCURVEMODE_T1659312557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemCurveMode
struct  ParticleSystemCurveMode_t1659312557 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_t1659312557, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMCURVEMODE_T1659312557_H
#ifndef PARTICLESYSTEMSCALINGMODE_T366697231_H
#define PARTICLESYSTEMSCALINGMODE_T366697231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemScalingMode
struct  ParticleSystemScalingMode_t366697231 
{
public:
	// System.Int32 UnityEngine.ParticleSystemScalingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemScalingMode_t366697231, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSCALINGMODE_T366697231_H
#ifndef MINMAXCURVE_T122563058_H
#define MINMAXCURVE_T122563058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxCurve
struct  MinMaxCurve_t122563058 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_t3306541151 * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_t3306541151 * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_CurveMin_2)); }
	inline AnimationCurve_t3306541151 * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_t3306541151 * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMin_2), value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_CurveMax_3)); }
	inline AnimationCurve_t3306541151 * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_t3306541151 * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMax_3), value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t122563058_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3306541151_marshaled_pinvoke ___m_CurveMin_2;
	AnimationCurve_t3306541151_marshaled_pinvoke ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t122563058_marshaled_com
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3306541151_marshaled_com* ___m_CurveMin_2;
	AnimationCurve_t3306541151_marshaled_com* ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
#endif // MINMAXCURVE_T122563058_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef RENDERER_T257310565_H
#define RENDERER_T257310565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t257310565  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T257310565_H
#ifndef PARTICLESYSTEM_T3394631041_H
#define PARTICLESYSTEM_T3394631041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t3394631041  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T3394631041_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef PARTICLESYSTEMRENDERER_T892265570_H
#define PARTICLESYSTEMRENDERER_T892265570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemRenderer
struct  ParticleSystemRenderer_t892265570  : public Renderer_t257310565
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMRENDERER_T892265570_H
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Particle_t250075699  m_Items[1];

public:
	inline Particle_t250075699  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Particle_t250075699 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Particle_t250075699  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Particle_t250075699  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Particle_t250075699 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Particle_t250075699  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(const AnimationCurve_t3306541151_marshaled_pinvoke& marshaled, AnimationCurve_t3306541151& unmarshaled);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(AnimationCurve_t3306541151_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_com& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com_back(const AnimationCurve_t3306541151_marshaled_com& marshaled, AnimationCurve_t3306541151& unmarshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com_cleanup(AnimationCurve_t3306541151_marshaled_com& marshaled);


// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m724825857 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1371476487 (EmissionModule_t2748003162 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ExternalForcesModule__ctor_m829489505 (ExternalForcesModule_t2596349650 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TextureSheetAnimationModule__ctor_m1567141742 (TextureSheetAnimationModule_t4262561859 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m2348632957 (ParticleSystem_t3394631041 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2791344602 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m3858897315 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_SetRateOverTime_m2463656192 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void EmissionModule_set_rateOverTime_m2095778141 (EmissionModule_t2748003162 * __this, MinMaxCurve_t122563058  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::GetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_GetRateOverTime_m2642116204 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/EmissionModule::get_rateOverTime()
extern "C"  MinMaxCurve_t122563058  EmissionModule_get_rateOverTime_m287157162 (EmissionModule_t2748003162 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/ExternalForcesModule::GetMultiplier(UnityEngine.ParticleSystem)
extern "C"  float ExternalForcesModule_GetMultiplier_m2971538288 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/ExternalForcesModule::get_multiplier()
extern "C"  float ExternalForcesModule_get_multiplier_m1686088223 (ExternalForcesModule_t2596349650 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetLoop(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void MainModule_SetLoop_m3781113516 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_loop(System.Boolean)
extern "C"  void MainModule_set_loop_m809600489 (MainModule_t6751348 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::GetSimulationSpace(UnityEngine.ParticleSystem)
extern "C"  int32_t MainModule_GetSimulationSpace_m3763565148 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace()
extern "C"  int32_t MainModule_get_simulationSpace_m3810335659 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.ParticleSystem/MainModule::GetCustomSimulationSpace(UnityEngine.ParticleSystem)
extern "C"  Transform_t3275118058 * MainModule_GetCustomSimulationSpace_m772388167 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.ParticleSystem/MainModule::get_customSimulationSpace()
extern "C"  Transform_t3275118058 * MainModule_get_customSimulationSpace_m1066041604 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetSimulationSpeed(UnityEngine.ParticleSystem,System.Single)
extern "C"  void MainModule_SetSimulationSpeed_m149728714 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed(System.Single)
extern "C"  void MainModule_set_simulationSpeed_m878496823 (MainModule_t6751348 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::GetScalingMode(UnityEngine.ParticleSystem)
extern "C"  int32_t MainModule_GetScalingMode_m90444860 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode()
extern "C"  int32_t MainModule_get_scalingMode_m3822956887 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetScalingMode(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_SetScalingMode_m10537077 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_set_scalingMode_m595996162 (MainModule_t6751348 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/MainModule::GetMaxParticles(UnityEngine.ParticleSystem)
extern "C"  int32_t MainModule_GetMaxParticles_m4284408640 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles()
extern "C"  int32_t MainModule_get_maxParticles_m2456945979 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetMaxParticles(UnityEngine.ParticleSystem,System.Int32)
extern "C"  void MainModule_SetMaxParticles_m1680200385 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32)
extern "C"  void MainModule_set_maxParticles_m4070785150 (MainModule_t6751348 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m3808882547 (AnimationCurve_t3306541151 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m3111284754 (MinMaxCurve_t122563058 * __this, float ___constant0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::get_curveMin()
extern "C"  AnimationCurve_t3306541151 * MinMaxCurve_get_curveMin_m1884297384 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C"  float MinMaxCurve_get_constant_m2300889686 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::get_curve()
extern "C"  AnimationCurve_t3306541151 * MinMaxCurve_get_curve_m777464902 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_position()
extern "C"  Vector3_t2243707580  Particle_get_position_m2472710964 (Particle_t250075699 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m218446335 (Particle_t250075699 * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_velocity()
extern "C"  Vector3_t2243707580  Particle_get_velocity_m3773136468 (Particle_t250075699 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern "C"  void Particle_set_velocity_m607267429 (Particle_t250075699 * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/Particle::get_remainingLifetime()
extern "C"  float Particle_get_remainingLifetime_m1540248920 (Particle_t250075699 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/Particle::get_startLifetime()
extern "C"  float Particle_get_startLifetime_m2209858036 (Particle_t250075699 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1555724485 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C"  void Particle_set_startSize_m3215075767 (Particle_t250075699 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/Particle::get_rotation()
extern "C"  float Particle_get_rotation_m3905482201 (Particle_t250075699 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m2521995545 (Particle_t250075699 * __this, Color32_t874517518  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/Particle::GetCurrentSize(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&)
extern "C"  float Particle_GetCurrentSize_m583192239 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, Particle_t250075699 * ___particle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/Particle::GetCurrentSize(UnityEngine.ParticleSystem)
extern "C"  float Particle_GetCurrentSize_m213148010 (Particle_t250075699 * __this, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::GetCurrentColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&)
extern "C"  Color32_t874517518  Particle_GetCurrentColor_m3489678869 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, Particle_t250075699 * ___particle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::GetCurrentColor(UnityEngine.ParticleSystem)
extern "C"  Color32_t874517518  Particle_GetCurrentColor_m2838256498 (Particle_t250075699 * __this, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::INTERNAL_CALL_GetCurrentColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&,UnityEngine.Color32&)
extern "C"  void Particle_INTERNAL_CALL_GetCurrentColor_m2188982265 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, Particle_t250075699 * ___particle1, Color32_t874517518 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool TextureSheetAnimationModule_GetEnabled_m761037215 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_enabled()
extern "C"  bool TextureSheetAnimationModule_get_enabled_m2035044262 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetNumTilesX(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetNumTilesX_m3613880309 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_numTilesX()
extern "C"  int32_t TextureSheetAnimationModule_get_numTilesX_m2252910704 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetNumTilesY(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetNumTilesY_m1404609178 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_numTilesY()
extern "C"  int32_t TextureSheetAnimationModule_get_numTilesY_m2252910609 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetAnimationType(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetAnimationType_m229191312 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemAnimationType UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_animation()
extern "C"  int32_t TextureSheetAnimationModule_get_animation_m4275294914 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetFrameOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void TextureSheetAnimationModule_GetFrameOverTime_m703817158 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_frameOverTime()
extern "C"  MinMaxCurve_t122563058  TextureSheetAnimationModule_get_frameOverTime_m1544335400 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetCycleCount(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetCycleCount_m1989670015 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_cycleCount()
extern "C"  int32_t TextureSheetAnimationModule_get_cycleCount_m3162981126 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetRowIndex(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetRowIndex_m1059118668 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_rowIndex()
extern "C"  int32_t TextureSheetAnimationModule_get_rowIndex_m3150902337 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern "C"  bool ParticleSystem_get_isPlaying_m312053940 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isPlaying_m312053940_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_get_isPlaying_m312053940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPlaying_m312053940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPlaying()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem::set_time(System.Single)
extern "C"  void ParticleSystem_set_time_m2196832584 (ParticleSystem_t3394631041 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_time_m2196832584_ftn) (ParticleSystem_t3394631041 *, float);
	static ParticleSystem_set_time_m2196832584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_time_m2196832584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.ParticleSystem::get_particleCount()
extern "C"  int32_t ParticleSystem_get_particleCount_m2947928589 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_get_particleCount_m2947928589_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_get_particleCount_m2947928589_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_particleCount_m2947928589_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_particleCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t6751348  ParticleSystem_get_main_m2275307502 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	MainModule_t6751348  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t6751348  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m724825857((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t6751348  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t2748003162  ParticleSystem_get_emission_m3803990178 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	EmissionModule_t2748003162  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t2748003162  L_0;
		memset(&L_0, 0, sizeof(L_0));
		EmissionModule__ctor_m1371476487((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		EmissionModule_t2748003162  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ExternalForcesModule UnityEngine.ParticleSystem::get_externalForces()
extern "C"  ExternalForcesModule_t2596349650  ParticleSystem_get_externalForces_m1639565358 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	ExternalForcesModule_t2596349650  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ExternalForcesModule_t2596349650  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ExternalForcesModule__ctor_m829489505((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ExternalForcesModule_t2596349650  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/TextureSheetAnimationModule UnityEngine.ParticleSystem::get_textureSheetAnimation()
extern "C"  TextureSheetAnimationModule_t4262561859  ParticleSystem_get_textureSheetAnimation_m2668911730 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TextureSheetAnimationModule_t4262561859  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TextureSheetAnimationModule__ctor_m1567141742((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		TextureSheetAnimationModule_t4262561859  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C"  void ParticleSystem_SetParticles_m1941536642 (ParticleSystem_t3394631041 * __this, ParticleU5BU5D_t574222242* ___particles0, int32_t ___size1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetParticles_m1941536642_ftn) (ParticleSystem_t3394631041 *, ParticleU5BU5D_t574222242*, int32_t);
	static ParticleSystem_SetParticles_m1941536642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetParticles_m1941536642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)");
	_il2cpp_icall_func(__this, ___particles0, ___size1);
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
extern "C"  int32_t ParticleSystem_GetParticles_m2083743765 (ParticleSystem_t3394631041 * __this, ParticleU5BU5D_t574222242* ___particles0, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_GetParticles_m2083743765_ftn) (ParticleSystem_t3394631041 *, ParticleU5BU5D_t574222242*);
	static ParticleSystem_GetParticles_m2083743765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticles_m2083743765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])");
	int32_t retVal = _il2cpp_icall_func(__this, ___particles0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m2348632957 (ParticleSystem_t3394631041 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Simulate_m2348632957_ftn) (ParticleSystem_t3394631041 *, float, bool, bool, bool);
	static ParticleSystem_Simulate_m2348632957_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Simulate_m2348632957_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___t0, ___withChildren1, ___restart2, ___fixedTimeStep3);
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m3168180268 (ParticleSystem_t3394631041 * __this, float ___t0, bool ___withChildren1, bool ___restart2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		float L_0 = ___t0;
		bool L_1 = ___withChildren1;
		bool L_2 = ___restart2;
		bool L_3 = V_0;
		ParticleSystem_Simulate_m2348632957(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m2079214656 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Play_m2079214656_ftn) (ParticleSystem_t3394631041 *, bool);
	static ParticleSystem_Play_m2079214656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Play_m2079214656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Play(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern "C"  void ParticleSystem_Pause_m1651937768 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Pause_m1651937768_ftn) (ParticleSystem_t3394631041 *, bool);
	static ParticleSystem_Pause_m1651937768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Pause_m1651937768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Pause(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2791344602 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Stop_m2791344602_ftn) (ParticleSystem_t3394631041 *, bool, int32_t);
	static ParticleSystem_Stop_m2791344602_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Stop_m2791344602_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)");
	_il2cpp_icall_func(__this, ___withChildren0, ___stopBehavior1);
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern "C"  void ParticleSystem_Stop_m3931522166 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		bool L_0 = ___withChildren0;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m2791344602(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m3868680149 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		V_1 = (bool)1;
		bool L_0 = V_1;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m2791344602(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern "C"  void ParticleSystem_Clear_m3246852559 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Clear_m3246852559_ftn) (ParticleSystem_t3394631041 *, bool);
	static ParticleSystem_Clear_m3246852559_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Clear_m3246852559_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m3858897315 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_IsAlive_m3858897315_ftn) (ParticleSystem_t3394631041 *, bool);
	static ParticleSystem_IsAlive_m3858897315_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_IsAlive_m3858897315_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::IsAlive(System.Boolean)");
	bool retVal = _il2cpp_icall_func(__this, ___withChildren0);
	return retVal;
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive()
extern "C"  bool ParticleSystem_IsAlive_m3241193100 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		bool L_1 = ParticleSystem_IsAlive_m3858897315(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_pinvoke(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_back(const EmissionModule_t2748003162_marshaled_pinvoke& marshaled, EmissionModule_t2748003162& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_cleanup(EmissionModule_t2748003162_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_com(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t2748003162_marshal_com_back(const EmissionModule_t2748003162_marshaled_com& marshaled, EmissionModule_t2748003162& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_com_cleanup(EmissionModule_t2748003162_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1371476487 (EmissionModule_t2748003162 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void EmissionModule__ctor_m1371476487_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	EmissionModule_t2748003162 * _thisAdjusted = reinterpret_cast<EmissionModule_t2748003162 *>(__this + 1);
	EmissionModule__ctor_m1371476487(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void EmissionModule_set_rateOverTime_m2095778141 (EmissionModule_t2748003162 * __this, MinMaxCurve_t122563058  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		EmissionModule_SetRateOverTime_m2463656192(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_rateOverTime_m2095778141_AdjustorThunk (RuntimeObject * __this, MinMaxCurve_t122563058  ___value0, const RuntimeMethod* method)
{
	EmissionModule_t2748003162 * _thisAdjusted = reinterpret_cast<EmissionModule_t2748003162 *>(__this + 1);
	EmissionModule_set_rateOverTime_m2095778141(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/EmissionModule::get_rateOverTime()
extern "C"  MinMaxCurve_t122563058  EmissionModule_get_rateOverTime_m287157162 (EmissionModule_t2748003162 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmissionModule_get_rateOverTime_m287157162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MinMaxCurve_t122563058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t122563058  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (MinMaxCurve_t122563058_il2cpp_TypeInfo_var, (&V_0));
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		EmissionModule_GetRateOverTime_m2642116204(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t122563058  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t122563058  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t122563058  EmissionModule_get_rateOverTime_m287157162_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	EmissionModule_t2748003162 * _thisAdjusted = reinterpret_cast<EmissionModule_t2748003162 *>(__this + 1);
	return EmissionModule_get_rateOverTime_m287157162(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_SetRateOverTime_m2463656192 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_SetRateOverTime_m2463656192_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static EmissionModule_SetRateOverTime_m2463656192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetRateOverTime_m2463656192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::GetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_GetRateOverTime_m2642116204 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_GetRateOverTime_m2642116204_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static EmissionModule_GetRateOverTime_m2642116204_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_GetRateOverTime_m2642116204_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::GetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t2596349650_marshal_pinvoke(const ExternalForcesModule_t2596349650& unmarshaled, ExternalForcesModule_t2596349650_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ExternalForcesModule_t2596349650_marshal_pinvoke_back(const ExternalForcesModule_t2596349650_marshaled_pinvoke& marshaled, ExternalForcesModule_t2596349650& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t2596349650_marshal_pinvoke_cleanup(ExternalForcesModule_t2596349650_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t2596349650_marshal_com(const ExternalForcesModule_t2596349650& unmarshaled, ExternalForcesModule_t2596349650_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ExternalForcesModule_t2596349650_marshal_com_back(const ExternalForcesModule_t2596349650_marshaled_com& marshaled, ExternalForcesModule_t2596349650& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t2596349650_marshal_com_cleanup(ExternalForcesModule_t2596349650_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ExternalForcesModule__ctor_m829489505 (ExternalForcesModule_t2596349650 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ExternalForcesModule__ctor_m829489505_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	ExternalForcesModule_t2596349650 * _thisAdjusted = reinterpret_cast<ExternalForcesModule_t2596349650 *>(__this + 1);
	ExternalForcesModule__ctor_m829489505(_thisAdjusted, ___particleSystem0, method);
}
// System.Single UnityEngine.ParticleSystem/ExternalForcesModule::get_multiplier()
extern "C"  float ExternalForcesModule_get_multiplier_m1686088223 (ExternalForcesModule_t2596349650 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ExternalForcesModule_GetMultiplier_m2971538288(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float ExternalForcesModule_get_multiplier_m1686088223_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ExternalForcesModule_t2596349650 * _thisAdjusted = reinterpret_cast<ExternalForcesModule_t2596349650 *>(__this + 1);
	return ExternalForcesModule_get_multiplier_m1686088223(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/ExternalForcesModule::GetMultiplier(UnityEngine.ParticleSystem)
extern "C"  float ExternalForcesModule_GetMultiplier_m2971538288 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef float (*ExternalForcesModule_GetMultiplier_m2971538288_ftn) (ParticleSystem_t3394631041 *);
	static ExternalForcesModule_GetMultiplier_m2971538288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ExternalForcesModule_GetMultiplier_m2971538288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ExternalForcesModule::GetMultiplier(UnityEngine.ParticleSystem)");
	float retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_pinvoke_back(const MainModule_t6751348_marshaled_pinvoke& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke_cleanup(MainModule_t6751348_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_com_back(const MainModule_t6751348_marshaled_com& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com_cleanup(MainModule_t6751348_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m724825857 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m724825857_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule__ctor_m724825857(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_loop(System.Boolean)
extern "C"  void MainModule_set_loop_m809600489 (MainModule_t6751348 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = ___value0;
		MainModule_SetLoop_m3781113516(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_loop_m809600489_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_loop_m809600489(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace()
extern "C"  int32_t MainModule_get_simulationSpace_m3810335659 (MainModule_t6751348 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = MainModule_GetSimulationSpace_m3763565148(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t MainModule_get_simulationSpace_m3810335659_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	return MainModule_get_simulationSpace_m3810335659(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.ParticleSystem/MainModule::get_customSimulationSpace()
extern "C"  Transform_t3275118058 * MainModule_get_customSimulationSpace_m1066041604 (MainModule_t6751348 * __this, const RuntimeMethod* method)
{
	Transform_t3275118058 * V_0 = NULL;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		Transform_t3275118058 * L_1 = MainModule_GetCustomSimulationSpace_m772388167(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Transform_t3275118058 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  Transform_t3275118058 * MainModule_get_customSimulationSpace_m1066041604_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	return MainModule_get_customSimulationSpace_m1066041604(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed(System.Single)
extern "C"  void MainModule_set_simulationSpeed_m878496823 (MainModule_t6751348 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		MainModule_SetSimulationSpeed_m149728714(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_simulationSpeed_m878496823_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_simulationSpeed_m878496823(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode()
extern "C"  int32_t MainModule_get_scalingMode_m3822956887 (MainModule_t6751348 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = MainModule_GetScalingMode_m90444860(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t MainModule_get_scalingMode_m3822956887_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	return MainModule_get_scalingMode_m3822956887(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_set_scalingMode_m595996162 (MainModule_t6751348 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = ___value0;
		MainModule_SetScalingMode_m10537077(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_scalingMode_m595996162_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_scalingMode_m595996162(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles()
extern "C"  int32_t MainModule_get_maxParticles_m2456945979 (MainModule_t6751348 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = MainModule_GetMaxParticles_m4284408640(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t MainModule_get_maxParticles_m2456945979_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	return MainModule_get_maxParticles_m2456945979(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32)
extern "C"  void MainModule_set_maxParticles_m4070785150 (MainModule_t6751348 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = ___value0;
		MainModule_SetMaxParticles_m1680200385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_maxParticles_m4070785150_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_maxParticles_m4070785150(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetLoop(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void MainModule_SetLoop_m3781113516 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetLoop_m3781113516_ftn) (ParticleSystem_t3394631041 *, bool);
	static MainModule_SetLoop_m3781113516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetLoop_m3781113516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetLoop(UnityEngine.ParticleSystem,System.Boolean)");
	_il2cpp_icall_func(___system0, ___value1);
}
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::GetSimulationSpace(UnityEngine.ParticleSystem)
extern "C"  int32_t MainModule_GetSimulationSpace_m3763565148 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*MainModule_GetSimulationSpace_m3763565148_ftn) (ParticleSystem_t3394631041 *);
	static MainModule_GetSimulationSpace_m3763565148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetSimulationSpace_m3763565148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetSimulationSpace(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// UnityEngine.Transform UnityEngine.ParticleSystem/MainModule::GetCustomSimulationSpace(UnityEngine.ParticleSystem)
extern "C"  Transform_t3275118058 * MainModule_GetCustomSimulationSpace_m772388167 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef Transform_t3275118058 * (*MainModule_GetCustomSimulationSpace_m772388167_ftn) (ParticleSystem_t3394631041 *);
	static MainModule_GetCustomSimulationSpace_m772388167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetCustomSimulationSpace_m772388167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetCustomSimulationSpace(UnityEngine.ParticleSystem)");
	Transform_t3275118058 * retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetSimulationSpeed(UnityEngine.ParticleSystem,System.Single)
extern "C"  void MainModule_SetSimulationSpeed_m149728714 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetSimulationSpeed_m149728714_ftn) (ParticleSystem_t3394631041 *, float);
	static MainModule_SetSimulationSpeed_m149728714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetSimulationSpeed_m149728714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetSimulationSpeed(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetScalingMode(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_SetScalingMode_m10537077 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetScalingMode_m10537077_ftn) (ParticleSystem_t3394631041 *, int32_t);
	static MainModule_SetScalingMode_m10537077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetScalingMode_m10537077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetScalingMode(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemScalingMode)");
	_il2cpp_icall_func(___system0, ___value1);
}
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::GetScalingMode(UnityEngine.ParticleSystem)
extern "C"  int32_t MainModule_GetScalingMode_m90444860 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*MainModule_GetScalingMode_m90444860_ftn) (ParticleSystem_t3394631041 *);
	static MainModule_GetScalingMode_m90444860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetScalingMode_m90444860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetScalingMode(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetMaxParticles(UnityEngine.ParticleSystem,System.Int32)
extern "C"  void MainModule_SetMaxParticles_m1680200385 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetMaxParticles_m1680200385_ftn) (ParticleSystem_t3394631041 *, int32_t);
	static MainModule_SetMaxParticles_m1680200385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetMaxParticles_m1680200385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetMaxParticles(UnityEngine.ParticleSystem,System.Int32)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Int32 UnityEngine.ParticleSystem/MainModule::GetMaxParticles(UnityEngine.ParticleSystem)
extern "C"  int32_t MainModule_GetMaxParticles_m4284408640 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*MainModule_GetMaxParticles_m4284408640_ftn) (ParticleSystem_t3394631041 *);
	static MainModule_GetMaxParticles_m4284408640_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetMaxParticles_m4284408640_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetMaxParticles(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke(const MinMaxCurve_t122563058& unmarshaled, MinMaxCurve_t122563058_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3306541151_marshal_pinvoke(*unmarshaled.get_m_CurveMin_2(), marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3306541151_marshal_pinvoke(*unmarshaled.get_m_CurveMax_3(), marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke_back(const MinMaxCurve_t122563058_marshaled_pinvoke& marshaled, MinMaxCurve_t122563058& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_t122563058_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	unmarshaled.set_m_CurveMin_2((AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMin_2(), NULL);
	AnimationCurve_t3306541151_marshal_pinvoke_back(marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	unmarshaled.set_m_CurveMax_3((AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMax_3(), NULL);
	AnimationCurve_t3306541151_marshal_pinvoke_back(marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke_cleanup(MinMaxCurve_t122563058_marshaled_pinvoke& marshaled)
{
	AnimationCurve_t3306541151_marshal_pinvoke_cleanup(marshaled.___m_CurveMin_2);
	AnimationCurve_t3306541151_marshal_pinvoke_cleanup(marshaled.___m_CurveMax_3);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_com(const MinMaxCurve_t122563058& unmarshaled, MinMaxCurve_t122563058_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3306541151_marshal_com(*unmarshaled.get_m_CurveMin_2(), *marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3306541151_marshal_com(*unmarshaled.get_m_CurveMax_3(), *marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t122563058_marshal_com_back(const MinMaxCurve_t122563058_marshaled_com& marshaled, MinMaxCurve_t122563058& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	if (unmarshaled.get_m_CurveMin_2() != NULL)
	{
		AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMin_2(), NULL);
		AnimationCurve_t3306541151_marshal_com_back(*marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	}
	if (unmarshaled.get_m_CurveMax_3() != NULL)
	{
		AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMax_3(), NULL);
		AnimationCurve_t3306541151_marshal_com_back(*marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	}
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_com_cleanup(MinMaxCurve_t122563058_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_CurveMin_2) != NULL) AnimationCurve_t3306541151_marshal_com_cleanup(*marshaled.___m_CurveMin_2);
	if (&(*marshaled.___m_CurveMax_3) != NULL) AnimationCurve_t3306541151_marshal_com_cleanup(*marshaled.___m_CurveMax_3);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m3111284754 (MinMaxCurve_t122563058 * __this, float ___constant0, const RuntimeMethod* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_CurveMultiplier_1((0.0f));
		__this->set_m_CurveMin_2((AnimationCurve_t3306541151 *)NULL);
		__this->set_m_CurveMax_3((AnimationCurve_t3306541151 *)NULL);
		__this->set_m_ConstantMin_4((0.0f));
		float L_0 = ___constant0;
		__this->set_m_ConstantMax_5(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve__ctor_m3111284754_AdjustorThunk (RuntimeObject * __this, float ___constant0, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t122563058 *>(__this + 1);
	MinMaxCurve__ctor_m3111284754(_thisAdjusted, ___constant0, method);
}
// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::get_curveMin()
extern "C"  AnimationCurve_t3306541151 * MinMaxCurve_get_curveMin_m1884297384 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method)
{
	AnimationCurve_t3306541151 * V_0 = NULL;
	{
		AnimationCurve_t3306541151 * L_0 = __this->get_m_CurveMin_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		AnimationCurve_t3306541151 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  AnimationCurve_t3306541151 * MinMaxCurve_get_curveMin_m1884297384_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t122563058 *>(__this + 1);
	return MinMaxCurve_get_curveMin_m1884297384(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C"  float MinMaxCurve_get_constant_m2300889686 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_ConstantMax_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float MinMaxCurve_get_constant_m2300889686_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t122563058 *>(__this + 1);
	return MinMaxCurve_get_constant_m2300889686(_thisAdjusted, method);
}
// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::get_curve()
extern "C"  AnimationCurve_t3306541151 * MinMaxCurve_get_curve_m777464902 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method)
{
	AnimationCurve_t3306541151 * V_0 = NULL;
	{
		AnimationCurve_t3306541151 * L_0 = __this->get_m_CurveMax_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		AnimationCurve_t3306541151 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  AnimationCurve_t3306541151 * MinMaxCurve_get_curve_m777464902_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t122563058 *>(__this + 1);
	return MinMaxCurve_get_curve_m777464902(_thisAdjusted, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern "C"  MinMaxCurve_t122563058  MinMaxCurve_op_Implicit_m4185724230 (RuntimeObject * __this /* static, unused */, float ___constant0, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___constant0;
		MinMaxCurve_t122563058  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MinMaxCurve__ctor_m3111284754((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MinMaxCurve_t122563058  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_position()
extern "C"  Vector3_t2243707580  Particle_get_position_m2472710964 (Particle_t250075699 * __this, const RuntimeMethod* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Position_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Particle_get_position_m2472710964_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	return Particle_get_position_m2472710964(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m218446335 (Particle_t250075699 * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_m_Position_0(L_0);
		return;
	}
}
extern "C"  void Particle_set_position_m218446335_AdjustorThunk (RuntimeObject * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	Particle_set_position_m218446335(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_velocity()
extern "C"  Vector3_t2243707580  Particle_get_velocity_m3773136468 (Particle_t250075699 * __this, const RuntimeMethod* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Velocity_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Particle_get_velocity_m3773136468_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	return Particle_get_velocity_m3773136468(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern "C"  void Particle_set_velocity_m607267429 (Particle_t250075699 * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_m_Velocity_1(L_0);
		return;
	}
}
extern "C"  void Particle_set_velocity_m607267429_AdjustorThunk (RuntimeObject * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	Particle_set_velocity_m607267429(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/Particle::get_remainingLifetime()
extern "C"  float Particle_get_remainingLifetime_m1540248920 (Particle_t250075699 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Lifetime_10();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Particle_get_remainingLifetime_m1540248920_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	return Particle_get_remainingLifetime_m1540248920(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/Particle::get_startLifetime()
extern "C"  float Particle_get_startLifetime_m2209858036 (Particle_t250075699 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_StartLifetime_11();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Particle_get_startLifetime_m2209858036_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	return Particle_get_startLifetime_m2209858036(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C"  void Particle_set_startSize_m3215075767 (Particle_t250075699 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = ___value0;
		float L_2 = ___value0;
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m1555724485((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_m_StartSize_7(L_3);
		return;
	}
}
extern "C"  void Particle_set_startSize_m3215075767_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	Particle_set_startSize_m3215075767(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/Particle::get_rotation()
extern "C"  float Particle_get_rotation_m3905482201 (Particle_t250075699 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_m_Rotation_5();
		float L_1 = L_0->get_z_3();
		V_0 = ((float)((float)L_1*(float)(57.29578f)));
		goto IL_0018;
	}

IL_0018:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Particle_get_rotation_m3905482201_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	return Particle_get_rotation_m3905482201(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m2521995545 (Particle_t250075699 * __this, Color32_t874517518  ___value0, const RuntimeMethod* method)
{
	{
		Color32_t874517518  L_0 = ___value0;
		__this->set_m_StartColor_8(L_0);
		return;
	}
}
extern "C"  void Particle_set_startColor_m2521995545_AdjustorThunk (RuntimeObject * __this, Color32_t874517518  ___value0, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	Particle_set_startColor_m2521995545(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/Particle::GetCurrentSize(UnityEngine.ParticleSystem)
extern "C"  float Particle_GetCurrentSize_m213148010 (Particle_t250075699 * __this, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t3394631041 * L_0 = ___system0;
		float L_1 = Particle_GetCurrentSize_m583192239(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Particle_GetCurrentSize_m213148010_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	return Particle_GetCurrentSize_m213148010(_thisAdjusted, ___system0, method);
}
// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::GetCurrentColor(UnityEngine.ParticleSystem)
extern "C"  Color32_t874517518  Particle_GetCurrentColor_m2838256498 (Particle_t250075699 * __this, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	Color32_t874517518  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ParticleSystem_t3394631041 * L_0 = ___system0;
		Color32_t874517518  L_1 = Particle_GetCurrentColor_m3489678869(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Color32_t874517518  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Color32_t874517518  Particle_GetCurrentColor_m2838256498_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	return Particle_GetCurrentColor_m2838256498(_thisAdjusted, ___system0, method);
}
// System.Single UnityEngine.ParticleSystem/Particle::GetCurrentSize(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&)
extern "C"  float Particle_GetCurrentSize_m583192239 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, Particle_t250075699 * ___particle1, const RuntimeMethod* method)
{
	typedef float (*Particle_GetCurrentSize_m583192239_ftn) (ParticleSystem_t3394631041 *, Particle_t250075699 *);
	static Particle_GetCurrentSize_m583192239_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Particle_GetCurrentSize_m583192239_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/Particle::GetCurrentSize(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&)");
	float retVal = _il2cpp_icall_func(___system0, ___particle1);
	return retVal;
}
// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::GetCurrentColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&)
extern "C"  Color32_t874517518  Particle_GetCurrentColor_m3489678869 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, Particle_t250075699 * ___particle1, const RuntimeMethod* method)
{
	Color32_t874517518  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color32_t874517518  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ParticleSystem_t3394631041 * L_0 = ___system0;
		Particle_t250075699 * L_1 = ___particle1;
		Particle_INTERNAL_CALL_GetCurrentColor_m2188982265(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color32_t874517518  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Color32_t874517518  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.ParticleSystem/Particle::INTERNAL_CALL_GetCurrentColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&,UnityEngine.Color32&)
extern "C"  void Particle_INTERNAL_CALL_GetCurrentColor_m2188982265 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, Particle_t250075699 * ___particle1, Color32_t874517518 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Particle_INTERNAL_CALL_GetCurrentColor_m2188982265_ftn) (ParticleSystem_t3394631041 *, Particle_t250075699 *, Color32_t874517518 *);
	static Particle_INTERNAL_CALL_GetCurrentColor_m2188982265_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Particle_INTERNAL_CALL_GetCurrentColor_m2188982265_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/Particle::INTERNAL_CALL_GetCurrentColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Particle&,UnityEngine.Color32&)");
	_il2cpp_icall_func(___system0, ___particle1, ___value2);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_pinvoke(const TextureSheetAnimationModule_t4262561859& unmarshaled, TextureSheetAnimationModule_t4262561859_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_pinvoke_back(const TextureSheetAnimationModule_t4262561859_marshaled_pinvoke& marshaled, TextureSheetAnimationModule_t4262561859& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_pinvoke_cleanup(TextureSheetAnimationModule_t4262561859_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_com(const TextureSheetAnimationModule_t4262561859& unmarshaled, TextureSheetAnimationModule_t4262561859_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_com_back(const TextureSheetAnimationModule_t4262561859_marshaled_com& marshaled, TextureSheetAnimationModule_t4262561859& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_com_cleanup(TextureSheetAnimationModule_t4262561859_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TextureSheetAnimationModule__ctor_m1567141742 (TextureSheetAnimationModule_t4262561859 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void TextureSheetAnimationModule__ctor_m1567141742_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	TextureSheetAnimationModule__ctor_m1567141742(_thisAdjusted, ___particleSystem0, method);
}
// System.Boolean UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_enabled()
extern "C"  bool TextureSheetAnimationModule_get_enabled_m2035044262 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = TextureSheetAnimationModule_GetEnabled_m761037215(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool TextureSheetAnimationModule_get_enabled_m2035044262_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	return TextureSheetAnimationModule_get_enabled_m2035044262(_thisAdjusted, method);
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_numTilesX()
extern "C"  int32_t TextureSheetAnimationModule_get_numTilesX_m2252910704 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = TextureSheetAnimationModule_GetNumTilesX_m3613880309(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t TextureSheetAnimationModule_get_numTilesX_m2252910704_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	return TextureSheetAnimationModule_get_numTilesX_m2252910704(_thisAdjusted, method);
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_numTilesY()
extern "C"  int32_t TextureSheetAnimationModule_get_numTilesY_m2252910609 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = TextureSheetAnimationModule_GetNumTilesY_m1404609178(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t TextureSheetAnimationModule_get_numTilesY_m2252910609_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	return TextureSheetAnimationModule_get_numTilesY_m2252910609(_thisAdjusted, method);
}
// UnityEngine.ParticleSystemAnimationType UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_animation()
extern "C"  int32_t TextureSheetAnimationModule_get_animation_m4275294914 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = TextureSheetAnimationModule_GetAnimationType_m229191312(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t TextureSheetAnimationModule_get_animation_m4275294914_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	return TextureSheetAnimationModule_get_animation_m4275294914(_thisAdjusted, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_frameOverTime()
extern "C"  MinMaxCurve_t122563058  TextureSheetAnimationModule_get_frameOverTime_m1544335400 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextureSheetAnimationModule_get_frameOverTime_m1544335400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MinMaxCurve_t122563058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t122563058  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (MinMaxCurve_t122563058_il2cpp_TypeInfo_var, (&V_0));
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		TextureSheetAnimationModule_GetFrameOverTime_m703817158(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t122563058  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t122563058  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t122563058  TextureSheetAnimationModule_get_frameOverTime_m1544335400_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	return TextureSheetAnimationModule_get_frameOverTime_m1544335400(_thisAdjusted, method);
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_cycleCount()
extern "C"  int32_t TextureSheetAnimationModule_get_cycleCount_m3162981126 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = TextureSheetAnimationModule_GetCycleCount_m1989670015(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t TextureSheetAnimationModule_get_cycleCount_m3162981126_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	return TextureSheetAnimationModule_get_cycleCount_m3162981126(_thisAdjusted, method);
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::get_rowIndex()
extern "C"  int32_t TextureSheetAnimationModule_get_rowIndex_m3150902337 (TextureSheetAnimationModule_t4262561859 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = TextureSheetAnimationModule_GetRowIndex_m1059118668(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t TextureSheetAnimationModule_get_rowIndex_m3150902337_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t4262561859 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t4262561859 *>(__this + 1);
	return TextureSheetAnimationModule_get_rowIndex_m3150902337(_thisAdjusted, method);
}
// System.Boolean UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool TextureSheetAnimationModule_GetEnabled_m761037215 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef bool (*TextureSheetAnimationModule_GetEnabled_m761037215_ftn) (ParticleSystem_t3394631041 *);
	static TextureSheetAnimationModule_GetEnabled_m761037215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextureSheetAnimationModule_GetEnabled_m761037215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetEnabled(UnityEngine.ParticleSystem)");
	bool retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetNumTilesX(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetNumTilesX_m3613880309 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*TextureSheetAnimationModule_GetNumTilesX_m3613880309_ftn) (ParticleSystem_t3394631041 *);
	static TextureSheetAnimationModule_GetNumTilesX_m3613880309_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextureSheetAnimationModule_GetNumTilesX_m3613880309_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetNumTilesX(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetNumTilesY(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetNumTilesY_m1404609178 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*TextureSheetAnimationModule_GetNumTilesY_m1404609178_ftn) (ParticleSystem_t3394631041 *);
	static TextureSheetAnimationModule_GetNumTilesY_m1404609178_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextureSheetAnimationModule_GetNumTilesY_m1404609178_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetNumTilesY(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetAnimationType(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetAnimationType_m229191312 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*TextureSheetAnimationModule_GetAnimationType_m229191312_ftn) (ParticleSystem_t3394631041 *);
	static TextureSheetAnimationModule_GetAnimationType_m229191312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextureSheetAnimationModule_GetAnimationType_m229191312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetAnimationType(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetFrameOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void TextureSheetAnimationModule_GetFrameOverTime_m703817158 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*TextureSheetAnimationModule_GetFrameOverTime_m703817158_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static TextureSheetAnimationModule_GetFrameOverTime_m703817158_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextureSheetAnimationModule_GetFrameOverTime_m703817158_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetFrameOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetCycleCount(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetCycleCount_m1989670015 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*TextureSheetAnimationModule_GetCycleCount_m1989670015_ftn) (ParticleSystem_t3394631041 *);
	static TextureSheetAnimationModule_GetCycleCount_m1989670015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextureSheetAnimationModule_GetCycleCount_m1989670015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetCycleCount(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Int32 UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetRowIndex(UnityEngine.ParticleSystem)
extern "C"  int32_t TextureSheetAnimationModule_GetRowIndex_m1059118668 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*TextureSheetAnimationModule_GetRowIndex_m1059118668_ftn) (ParticleSystem_t3394631041 *);
	static TextureSheetAnimationModule_GetRowIndex_m1059118668_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextureSheetAnimationModule_GetRowIndex_m1059118668_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/TextureSheetAnimationModule::GetRowIndex(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
