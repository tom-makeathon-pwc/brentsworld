﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.UI.Extensions.UI_TweenScale
struct UI_TweenScale_t1720817768;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.UI.Extensions.SelectableScaler
struct SelectableScaler_t1409550150;
// MirzaBeig.ParticleSystems.PerlinNoise
struct PerlinNoise_t3938891586;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.String
struct String_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// System.Collections.Generic.List`1<UnityEngine.LineRenderer>
struct List_1_t218278803;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3948421699;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Extensions.UILineRenderer
struct UILineRenderer_t3031355003;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.UI.Extensions.UIHighlightable/InteractableChangedEvent
struct InteractableChangedEvent_t3882566494;
// System.Collections.Generic.List`1<MirzaBeig.ParticleSystems.Demos.CameraShake/Shake>
struct List_1_t2271044132;
// System.Predicate`1<MirzaBeig.ParticleSystems.Demos.CameraShake/Shake>
struct Predicate_1_t1344893115;
// System.Collections.Generic.List`1<UnityEngine.Light>
struct List_1_t4158814064;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MirzaBeig.ParticleSystems.Demos.MouseFollow
struct MouseFollow_t3233463164;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t1971649997;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t1030026315;
// MirzaBeig.ParticleSystems.Demos.LoopingParticleSystemsManager
struct LoopingParticleSystemsManager_t2507471847;
// MirzaBeig.ParticleSystems.Demos.OneshotParticleSystemsManager
struct OneshotParticleSystemsManager_t1174726079;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t3035069757;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Collections.Generic.List`1<UnityEngine.KeyCode>
struct List_1_t1652516284;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.UI.Extensions.UISelectableExtension/UIButtonEvent
struct UIButtonEvent_t1812119617;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// MirzaBeig.ParticleSystems.PerlinNoiseXYZ
struct PerlinNoiseXYZ_t2920671427;
// UnityEngine.TrailRenderer[]
struct TrailRendererU5BU5D_t182776078;
// MirzaBeig.ParticleSystems.Rotator
struct Rotator_t1284983691;
// System.Threading.Thread
struct Thread_t241561612;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1490986844;
// MirzaBeig.ParticleSystems.ParticleSystems/onParticleSystemsDeadEventHandler
struct onParticleSystemsDeadEventHandler_t222977030;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// MirzaBeig.Scripting.Effects.AttractionParticleForceField
struct AttractionParticleForceField_t2380317660;
// MirzaBeig.Scripting.Effects.VortexParticleForceField
struct VortexParticleForceField_t1112726591;
// MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel[]
struct VoxelU5BU5D_t3808224484;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t2763752173;
// UnityEngine.ParticleSystem/Particle[][]
struct ParticleU5BU5DU5BU5D_t2909307799;
// UnityEngine.ParticleSystem/MainModule[]
struct MainModuleU5BU5D_t2453288765;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// UnityEngine.UI.ContentSizeFitter
struct ContentSizeFitter_t1325211874;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t2468316403;
// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t2875670365;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t1515633077;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// System.Collections.Generic.List`1<MirzaBeig.ParticleSystems.ParticleSystems>
struct List_1_t1643199118;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t1524870173;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t621514313;




#ifndef U3CMODULEU3E_T3783534234_H
#define U3CMODULEU3E_T3783534234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534234 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534234_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef UIEXTENSIONMETHODS_T2394151223_H
#define UIEXTENSIONMETHODS_T2394151223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIExtensionMethods
struct  UIExtensionMethods_t2394151223  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEXTENSIONMETHODS_T2394151223_H
#ifndef U3CTWEENU3EC__ITERATOR0_T2180083606_H
#define U3CTWEENU3EC__ITERATOR0_T2180083606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_TweenScale/<Tween>c__Iterator0
struct  U3CTweenU3Ec__Iterator0_t2180083606  : public RuntimeObject
{
public:
	// System.Single UnityEngine.UI.Extensions.UI_TweenScale/<Tween>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single UnityEngine.UI.Extensions.UI_TweenScale/<Tween>c__Iterator0::<maxT>__0
	float ___U3CmaxTU3E__0_1;
	// UnityEngine.UI.Extensions.UI_TweenScale UnityEngine.UI.Extensions.UI_TweenScale/<Tween>c__Iterator0::$this
	UI_TweenScale_t1720817768 * ___U24this_2;
	// System.Object UnityEngine.UI.Extensions.UI_TweenScale/<Tween>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.Extensions.UI_TweenScale/<Tween>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.Extensions.UI_TweenScale/<Tween>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTweenU3Ec__Iterator0_t2180083606, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CmaxTU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTweenU3Ec__Iterator0_t2180083606, ___U3CmaxTU3E__0_1)); }
	inline float get_U3CmaxTU3E__0_1() const { return ___U3CmaxTU3E__0_1; }
	inline float* get_address_of_U3CmaxTU3E__0_1() { return &___U3CmaxTU3E__0_1; }
	inline void set_U3CmaxTU3E__0_1(float value)
	{
		___U3CmaxTU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CTweenU3Ec__Iterator0_t2180083606, ___U24this_2)); }
	inline UI_TweenScale_t1720817768 * get_U24this_2() const { return ___U24this_2; }
	inline UI_TweenScale_t1720817768 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UI_TweenScale_t1720817768 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CTweenU3Ec__Iterator0_t2180083606, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CTweenU3Ec__Iterator0_t2180083606, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CTweenU3Ec__Iterator0_t2180083606, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENU3EC__ITERATOR0_T2180083606_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef NOISE2_T3037305318_H
#define NOISE2_T3037305318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.Noise2
struct  Noise2_t3037305318  : public RuntimeObject
{
public:

public:
};

struct Noise2_t3037305318_StaticFields
{
public:
	// System.Single MirzaBeig.Scripting.Effects.Noise2::F3
	float ___F3_0;
	// System.Single MirzaBeig.Scripting.Effects.Noise2::G3
	float ___G3_1;
	// System.Byte[] MirzaBeig.Scripting.Effects.Noise2::perm
	ByteU5BU5D_t3397334013* ___perm_2;

public:
	inline static int32_t get_offset_of_F3_0() { return static_cast<int32_t>(offsetof(Noise2_t3037305318_StaticFields, ___F3_0)); }
	inline float get_F3_0() const { return ___F3_0; }
	inline float* get_address_of_F3_0() { return &___F3_0; }
	inline void set_F3_0(float value)
	{
		___F3_0 = value;
	}

	inline static int32_t get_offset_of_G3_1() { return static_cast<int32_t>(offsetof(Noise2_t3037305318_StaticFields, ___G3_1)); }
	inline float get_G3_1() const { return ___G3_1; }
	inline float* get_address_of_G3_1() { return &___G3_1; }
	inline void set_G3_1(float value)
	{
		___G3_1 = value;
	}

	inline static int32_t get_offset_of_perm_2() { return static_cast<int32_t>(offsetof(Noise2_t3037305318_StaticFields, ___perm_2)); }
	inline ByteU5BU5D_t3397334013* get_perm_2() const { return ___perm_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_perm_2() { return &___perm_2; }
	inline void set_perm_2(ByteU5BU5D_t3397334013* value)
	{
		___perm_2 = value;
		Il2CppCodeGenWriteBarrier((&___perm_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISE2_T3037305318_H
#ifndef NOISE_T2617615148_H
#define NOISE_T2617615148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.Noise
struct  Noise_t2617615148  : public RuntimeObject
{
public:

public:
};

struct Noise_t2617615148_StaticFields
{
public:
	// System.Single MirzaBeig.Scripting.Effects.Noise::F3
	float ___F3_0;
	// System.Single MirzaBeig.Scripting.Effects.Noise::G3
	float ___G3_1;
	// System.Byte[] MirzaBeig.Scripting.Effects.Noise::perm
	ByteU5BU5D_t3397334013* ___perm_2;

public:
	inline static int32_t get_offset_of_F3_0() { return static_cast<int32_t>(offsetof(Noise_t2617615148_StaticFields, ___F3_0)); }
	inline float get_F3_0() const { return ___F3_0; }
	inline float* get_address_of_F3_0() { return &___F3_0; }
	inline void set_F3_0(float value)
	{
		___F3_0 = value;
	}

	inline static int32_t get_offset_of_G3_1() { return static_cast<int32_t>(offsetof(Noise_t2617615148_StaticFields, ___G3_1)); }
	inline float get_G3_1() const { return ___G3_1; }
	inline float* get_address_of_G3_1() { return &___G3_1; }
	inline void set_G3_1(float value)
	{
		___G3_1 = value;
	}

	inline static int32_t get_offset_of_perm_2() { return static_cast<int32_t>(offsetof(Noise_t2617615148_StaticFields, ___perm_2)); }
	inline ByteU5BU5D_t3397334013* get_perm_2() const { return ___perm_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_perm_2() { return &___perm_2; }
	inline void set_perm_2(ByteU5BU5D_t3397334013* value)
	{
		___perm_2 = value;
		Il2CppCodeGenWriteBarrier((&___perm_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISE_T2617615148_H
#ifndef RECTTRANSFORMEXTENSION_T2306465163_H
#define RECTTRANSFORMEXTENSION_T2306465163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RectTransformExtension
struct  RectTransformExtension_t2306465163  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMEXTENSION_T2306465163_H
#ifndef SETPROPERTYUTILITY_T2250268447_H
#define SETPROPERTYUTILITY_T2250268447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SetPropertyUtility
struct  SetPropertyUtility_t2250268447  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROPERTYUTILITY_T2250268447_H
#ifndef U3CSCALEOUTU3EC__ITERATOR1_T2806189492_H
#define U3CSCALEOUTU3EC__ITERATOR1_T2806189492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SelectableScaler/<ScaleOUT>c__Iterator1
struct  U3CScaleOUTU3Ec__Iterator1_t2806189492  : public RuntimeObject
{
public:
	// System.Single UnityEngine.UI.Extensions.SelectableScaler/<ScaleOUT>c__Iterator1::<t>__1
	float ___U3CtU3E__1_0;
	// System.Single UnityEngine.UI.Extensions.SelectableScaler/<ScaleOUT>c__Iterator1::<maxT>__1
	float ___U3CmaxTU3E__1_1;
	// UnityEngine.UI.Extensions.SelectableScaler UnityEngine.UI.Extensions.SelectableScaler/<ScaleOUT>c__Iterator1::$this
	SelectableScaler_t1409550150 * ___U24this_2;
	// System.Object UnityEngine.UI.Extensions.SelectableScaler/<ScaleOUT>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.Extensions.SelectableScaler/<ScaleOUT>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.Extensions.SelectableScaler/<ScaleOUT>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__1_0() { return static_cast<int32_t>(offsetof(U3CScaleOUTU3Ec__Iterator1_t2806189492, ___U3CtU3E__1_0)); }
	inline float get_U3CtU3E__1_0() const { return ___U3CtU3E__1_0; }
	inline float* get_address_of_U3CtU3E__1_0() { return &___U3CtU3E__1_0; }
	inline void set_U3CtU3E__1_0(float value)
	{
		___U3CtU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CmaxTU3E__1_1() { return static_cast<int32_t>(offsetof(U3CScaleOUTU3Ec__Iterator1_t2806189492, ___U3CmaxTU3E__1_1)); }
	inline float get_U3CmaxTU3E__1_1() const { return ___U3CmaxTU3E__1_1; }
	inline float* get_address_of_U3CmaxTU3E__1_1() { return &___U3CmaxTU3E__1_1; }
	inline void set_U3CmaxTU3E__1_1(float value)
	{
		___U3CmaxTU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CScaleOUTU3Ec__Iterator1_t2806189492, ___U24this_2)); }
	inline SelectableScaler_t1409550150 * get_U24this_2() const { return ___U24this_2; }
	inline SelectableScaler_t1409550150 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SelectableScaler_t1409550150 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CScaleOUTU3Ec__Iterator1_t2806189492, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CScaleOUTU3Ec__Iterator1_t2806189492, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CScaleOUTU3Ec__Iterator1_t2806189492, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCALEOUTU3EC__ITERATOR1_T2806189492_H
#ifndef PERLINNOISEXYZ_T2920671427_H
#define PERLINNOISEXYZ_T2920671427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.PerlinNoiseXYZ
struct  PerlinNoiseXYZ_t2920671427  : public RuntimeObject
{
public:
	// MirzaBeig.ParticleSystems.PerlinNoise MirzaBeig.ParticleSystems.PerlinNoiseXYZ::x
	PerlinNoise_t3938891586 * ___x_0;
	// MirzaBeig.ParticleSystems.PerlinNoise MirzaBeig.ParticleSystems.PerlinNoiseXYZ::y
	PerlinNoise_t3938891586 * ___y_1;
	// MirzaBeig.ParticleSystems.PerlinNoise MirzaBeig.ParticleSystems.PerlinNoiseXYZ::z
	PerlinNoise_t3938891586 * ___z_2;
	// System.Boolean MirzaBeig.ParticleSystems.PerlinNoiseXYZ::unscaledTime
	bool ___unscaledTime_3;
	// System.Single MirzaBeig.ParticleSystems.PerlinNoiseXYZ::amplitudeScale
	float ___amplitudeScale_4;
	// System.Single MirzaBeig.ParticleSystems.PerlinNoiseXYZ::frequencyScale
	float ___frequencyScale_5;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(PerlinNoiseXYZ_t2920671427, ___x_0)); }
	inline PerlinNoise_t3938891586 * get_x_0() const { return ___x_0; }
	inline PerlinNoise_t3938891586 ** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(PerlinNoise_t3938891586 * value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(PerlinNoiseXYZ_t2920671427, ___y_1)); }
	inline PerlinNoise_t3938891586 * get_y_1() const { return ___y_1; }
	inline PerlinNoise_t3938891586 ** get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(PerlinNoise_t3938891586 * value)
	{
		___y_1 = value;
		Il2CppCodeGenWriteBarrier((&___y_1), value);
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(PerlinNoiseXYZ_t2920671427, ___z_2)); }
	inline PerlinNoise_t3938891586 * get_z_2() const { return ___z_2; }
	inline PerlinNoise_t3938891586 ** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(PerlinNoise_t3938891586 * value)
	{
		___z_2 = value;
		Il2CppCodeGenWriteBarrier((&___z_2), value);
	}

	inline static int32_t get_offset_of_unscaledTime_3() { return static_cast<int32_t>(offsetof(PerlinNoiseXYZ_t2920671427, ___unscaledTime_3)); }
	inline bool get_unscaledTime_3() const { return ___unscaledTime_3; }
	inline bool* get_address_of_unscaledTime_3() { return &___unscaledTime_3; }
	inline void set_unscaledTime_3(bool value)
	{
		___unscaledTime_3 = value;
	}

	inline static int32_t get_offset_of_amplitudeScale_4() { return static_cast<int32_t>(offsetof(PerlinNoiseXYZ_t2920671427, ___amplitudeScale_4)); }
	inline float get_amplitudeScale_4() const { return ___amplitudeScale_4; }
	inline float* get_address_of_amplitudeScale_4() { return &___amplitudeScale_4; }
	inline void set_amplitudeScale_4(float value)
	{
		___amplitudeScale_4 = value;
	}

	inline static int32_t get_offset_of_frequencyScale_5() { return static_cast<int32_t>(offsetof(PerlinNoiseXYZ_t2920671427, ___frequencyScale_5)); }
	inline float get_frequencyScale_5() const { return ___frequencyScale_5; }
	inline float* get_address_of_frequencyScale_5() { return &___frequencyScale_5; }
	inline void set_frequencyScale_5(float value)
	{
		___frequencyScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERLINNOISEXYZ_T2920671427_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef CREATELUT_T2634963967_H
#define CREATELUT_T2634963967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.CreateLUT
struct  CreateLUT_t2634963967  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATELUT_T2634963967_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef U24ARRAYTYPEU3D512_T740780702_H
#define U24ARRAYTYPEU3D512_T740780702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=512
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D512_t740780702 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D512_t740780702__padding[512];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D512_T740780702_H
#ifndef U24ARRAYTYPEU3D24_T762068664_H
#define U24ARRAYTYPEU3D24_T762068664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t762068664 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t762068664__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T762068664_H
#ifndef MAINMODULE_T6751348_H
#define MAINMODULE_T6751348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t6751348 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t6751348, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T6751348_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef EMISSIONMODULE_T2748003162_H
#define EMISSIONMODULE_T2748003162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/EmissionModule
struct  EmissionModule_t2748003162 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/EmissionModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_t2748003162, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t2748003162_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t2748003162_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // EMISSIONMODULE_T2748003162_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef UNITYEVENT_1_T3863924733_H
#define UNITYEVENT_1_T3863924733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t3863924733  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3863924733, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3863924733_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef UNITYEVENT_1_T3020313056_H
#define UNITYEVENT_1_T3020313056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.PointerEventData/InputButton>
struct  UnityEvent_1_t3020313056  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3020313056, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3020313056_H
#ifndef LIGHTSHADOWS_T747769755_H
#define LIGHTSHADOWS_T747769755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LightShadows
struct  LightShadows_t747769755 
{
public:
	// System.Int32 UnityEngine.LightShadows::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightShadows_t747769755, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSHADOWS_T747769755_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef PERLINNOISE_T3938891586_H
#define PERLINNOISE_T3938891586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.PerlinNoise
struct  PerlinNoise_t3938891586  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 MirzaBeig.ParticleSystems.PerlinNoise::offset
	Vector2_t2243707579  ___offset_0;
	// System.Single MirzaBeig.ParticleSystems.PerlinNoise::amplitude
	float ___amplitude_1;
	// System.Single MirzaBeig.ParticleSystems.PerlinNoise::frequency
	float ___frequency_2;
	// System.Boolean MirzaBeig.ParticleSystems.PerlinNoise::unscaledTime
	bool ___unscaledTime_3;

public:
	inline static int32_t get_offset_of_offset_0() { return static_cast<int32_t>(offsetof(PerlinNoise_t3938891586, ___offset_0)); }
	inline Vector2_t2243707579  get_offset_0() const { return ___offset_0; }
	inline Vector2_t2243707579 * get_address_of_offset_0() { return &___offset_0; }
	inline void set_offset_0(Vector2_t2243707579  value)
	{
		___offset_0 = value;
	}

	inline static int32_t get_offset_of_amplitude_1() { return static_cast<int32_t>(offsetof(PerlinNoise_t3938891586, ___amplitude_1)); }
	inline float get_amplitude_1() const { return ___amplitude_1; }
	inline float* get_address_of_amplitude_1() { return &___amplitude_1; }
	inline void set_amplitude_1(float value)
	{
		___amplitude_1 = value;
	}

	inline static int32_t get_offset_of_frequency_2() { return static_cast<int32_t>(offsetof(PerlinNoise_t3938891586, ___frequency_2)); }
	inline float get_frequency_2() const { return ___frequency_2; }
	inline float* get_address_of_frequency_2() { return &___frequency_2; }
	inline void set_frequency_2(float value)
	{
		___frequency_2 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_3() { return static_cast<int32_t>(offsetof(PerlinNoise_t3938891586, ___unscaledTime_3)); }
	inline bool get_unscaledTime_3() const { return ___unscaledTime_3; }
	inline bool* get_address_of_unscaledTime_3() { return &___unscaledTime_3; }
	inline void set_unscaledTime_3(bool value)
	{
		___unscaledTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERLINNOISE_T3938891586_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef GETFORCEPARAMETERS_T3736837055_H
#define GETFORCEPARAMETERS_T3736837055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleAffector/GetForceParameters
struct  GetForceParameters_t3736837055 
{
public:
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffector/GetForceParameters::distanceToAffectorCenterSqr
	float ___distanceToAffectorCenterSqr_0;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleAffector/GetForceParameters::scaledDirectionToAffectorCenter
	Vector3_t2243707580  ___scaledDirectionToAffectorCenter_1;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleAffector/GetForceParameters::particlePosition
	Vector3_t2243707580  ___particlePosition_2;

public:
	inline static int32_t get_offset_of_distanceToAffectorCenterSqr_0() { return static_cast<int32_t>(offsetof(GetForceParameters_t3736837055, ___distanceToAffectorCenterSqr_0)); }
	inline float get_distanceToAffectorCenterSqr_0() const { return ___distanceToAffectorCenterSqr_0; }
	inline float* get_address_of_distanceToAffectorCenterSqr_0() { return &___distanceToAffectorCenterSqr_0; }
	inline void set_distanceToAffectorCenterSqr_0(float value)
	{
		___distanceToAffectorCenterSqr_0 = value;
	}

	inline static int32_t get_offset_of_scaledDirectionToAffectorCenter_1() { return static_cast<int32_t>(offsetof(GetForceParameters_t3736837055, ___scaledDirectionToAffectorCenter_1)); }
	inline Vector3_t2243707580  get_scaledDirectionToAffectorCenter_1() const { return ___scaledDirectionToAffectorCenter_1; }
	inline Vector3_t2243707580 * get_address_of_scaledDirectionToAffectorCenter_1() { return &___scaledDirectionToAffectorCenter_1; }
	inline void set_scaledDirectionToAffectorCenter_1(Vector3_t2243707580  value)
	{
		___scaledDirectionToAffectorCenter_1 = value;
	}

	inline static int32_t get_offset_of_particlePosition_2() { return static_cast<int32_t>(offsetof(GetForceParameters_t3736837055, ___particlePosition_2)); }
	inline Vector3_t2243707580  get_particlePosition_2() const { return ___particlePosition_2; }
	inline Vector3_t2243707580 * get_address_of_particlePosition_2() { return &___particlePosition_2; }
	inline void set_particlePosition_2(Vector3_t2243707580  value)
	{
		___particlePosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFORCEPARAMETERS_T3736837055_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t762068664  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;
	// <PrivateImplementationDetails>/$ArrayType=512 <PrivateImplementationDetails>::$field-E74588860CA220F5327520E16546CBB6016903F4
	U24ArrayTypeU3D512_t740780702  ___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t762068664  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t762068664 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t762068664  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1)); }
	inline U24ArrayTypeU3D512_t740780702  get_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1() const { return ___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1; }
	inline U24ArrayTypeU3D512_t740780702 * get_address_of_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1() { return &___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1; }
	inline void set_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1(U24ArrayTypeU3D512_t740780702  value)
	{
		___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef NOISETYPE_T1763094680_H
#define NOISETYPE_T1763094680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.TurbulenceParticleForceField/NoiseType
struct  NoiseType_t1763094680 
{
public:
	// System.Int32 MirzaBeig.Scripting.Effects.TurbulenceParticleForceField/NoiseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NoiseType_t1763094680, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISETYPE_T1763094680_H
#ifndef NOISETYPE_T1186427329_H
#define NOISETYPE_T1186427329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.TurbulenceParticleAffector/NoiseType
struct  NoiseType_t1186427329 
{
public:
	// System.Int32 MirzaBeig.Scripting.Effects.TurbulenceParticleAffector/NoiseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NoiseType_t1186427329, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISETYPE_T1186427329_H
#ifndef GETFORCEPARAMETERS_T2196150906_H
#define GETFORCEPARAMETERS_T2196150906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleForceField/GetForceParameters
struct  GetForceParameters_t2196150906 
{
public:
	// System.Single MirzaBeig.Scripting.Effects.ParticleForceField/GetForceParameters::distanceToForceFieldCenterSqr
	float ___distanceToForceFieldCenterSqr_0;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleForceField/GetForceParameters::scaledDirectionToForceFieldCenter
	Vector3_t2243707580  ___scaledDirectionToForceFieldCenter_1;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleForceField/GetForceParameters::particlePosition
	Vector3_t2243707580  ___particlePosition_2;

public:
	inline static int32_t get_offset_of_distanceToForceFieldCenterSqr_0() { return static_cast<int32_t>(offsetof(GetForceParameters_t2196150906, ___distanceToForceFieldCenterSqr_0)); }
	inline float get_distanceToForceFieldCenterSqr_0() const { return ___distanceToForceFieldCenterSqr_0; }
	inline float* get_address_of_distanceToForceFieldCenterSqr_0() { return &___distanceToForceFieldCenterSqr_0; }
	inline void set_distanceToForceFieldCenterSqr_0(float value)
	{
		___distanceToForceFieldCenterSqr_0 = value;
	}

	inline static int32_t get_offset_of_scaledDirectionToForceFieldCenter_1() { return static_cast<int32_t>(offsetof(GetForceParameters_t2196150906, ___scaledDirectionToForceFieldCenter_1)); }
	inline Vector3_t2243707580  get_scaledDirectionToForceFieldCenter_1() const { return ___scaledDirectionToForceFieldCenter_1; }
	inline Vector3_t2243707580 * get_address_of_scaledDirectionToForceFieldCenter_1() { return &___scaledDirectionToForceFieldCenter_1; }
	inline void set_scaledDirectionToForceFieldCenter_1(Vector3_t2243707580  value)
	{
		___scaledDirectionToForceFieldCenter_1 = value;
	}

	inline static int32_t get_offset_of_particlePosition_2() { return static_cast<int32_t>(offsetof(GetForceParameters_t2196150906, ___particlePosition_2)); }
	inline Vector3_t2243707580  get_particlePosition_2() const { return ___particlePosition_2; }
	inline Vector3_t2243707580 * get_address_of_particlePosition_2() { return &___particlePosition_2; }
	inline void set_particlePosition_2(Vector3_t2243707580  value)
	{
		___particlePosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFORCEPARAMETERS_T2196150906_H
#ifndef UIBUTTONEVENT_T1812119617_H
#define UIBUTTONEVENT_T1812119617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UISelectableExtension/UIButtonEvent
struct  UIButtonEvent_t1812119617  : public UnityEvent_1_t3020313056
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONEVENT_T1812119617_H
#ifndef PARTICLEMODE_T3512372172_H
#define PARTICLEMODE_T3512372172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.DemoManager/ParticleMode
struct  ParticleMode_t3512372172 
{
public:
	// System.Int32 MirzaBeig.ParticleSystems.Demos.DemoManager/ParticleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleMode_t3512372172, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEMODE_T3512372172_H
#ifndef CAMERASHAKEAMPLITUDEOVERDISTANCECURVE_T2999505806_H
#define CAMERASHAKEAMPLITUDEOVERDISTANCECURVE_T2999505806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.CameraShakeAmplitudeOverDistanceCurve
struct  CameraShakeAmplitudeOverDistanceCurve_t2999505806 
{
public:
	// System.Int32 MirzaBeig.ParticleSystems.Demos.CameraShakeAmplitudeOverDistanceCurve::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraShakeAmplitudeOverDistanceCurve_t2999505806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKEAMPLITUDEOVERDISTANCECURVE_T2999505806_H
#ifndef CAMERASHAKEAMPLITUDECURVE_T2344288097_H
#define CAMERASHAKEAMPLITUDECURVE_T2344288097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.CameraShakeAmplitudeCurve
struct  CameraShakeAmplitudeCurve_t2344288097 
{
public:
	// System.Int32 MirzaBeig.ParticleSystems.Demos.CameraShakeAmplitudeCurve::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraShakeAmplitudeCurve_t2344288097, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKEAMPLITUDECURVE_T2344288097_H
#ifndef SCROLLTYPE_T1104322043_H
#define SCROLLTYPE_T1104322043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIScrollToSelection/ScrollType
struct  ScrollType_t1104322043 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UIScrollToSelection/ScrollType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollType_t1104322043, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLTYPE_T1104322043_H
#ifndef CAMERASHAKETARGET_T2752665144_H
#define CAMERASHAKETARGET_T2752665144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.CameraShakeTarget
struct  CameraShakeTarget_t2752665144 
{
public:
	// System.Int32 MirzaBeig.ParticleSystems.Demos.CameraShakeTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraShakeTarget_t2752665144, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKETARGET_T2752665144_H
#ifndef INTERACTABLECHANGEDEVENT_T3882566494_H
#define INTERACTABLECHANGEDEVENT_T3882566494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIHighlightable/InteractableChangedEvent
struct  InteractableChangedEvent_t3882566494  : public UnityEvent_1_t3863924733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTABLECHANGEDEVENT_T3882566494_H
#ifndef LEVEL_T1106640823_H
#define LEVEL_T1106640823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.DemoManager/Level
struct  Level_t1106640823 
{
public:
	// System.Int32 MirzaBeig.ParticleSystems.Demos.DemoManager/Level::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Level_t1106640823, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T1106640823_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef VOXEL_T3105992857_H
#define VOXEL_T3105992857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel
struct  Voxel_t3105992857 
{
public:
	// UnityEngine.Bounds MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel::bounds
	Bounds_t3033363703  ___bounds_0;
	// System.Int32[] MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel::particles
	Int32U5BU5D_t3030399641* ___particles_1;
	// System.Int32 MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel::particleCount
	int32_t ___particleCount_2;

public:
	inline static int32_t get_offset_of_bounds_0() { return static_cast<int32_t>(offsetof(Voxel_t3105992857, ___bounds_0)); }
	inline Bounds_t3033363703  get_bounds_0() const { return ___bounds_0; }
	inline Bounds_t3033363703 * get_address_of_bounds_0() { return &___bounds_0; }
	inline void set_bounds_0(Bounds_t3033363703  value)
	{
		___bounds_0 = value;
	}

	inline static int32_t get_offset_of_particles_1() { return static_cast<int32_t>(offsetof(Voxel_t3105992857, ___particles_1)); }
	inline Int32U5BU5D_t3030399641* get_particles_1() const { return ___particles_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_particles_1() { return &___particles_1; }
	inline void set_particles_1(Int32U5BU5D_t3030399641* value)
	{
		___particles_1 = value;
		Il2CppCodeGenWriteBarrier((&___particles_1), value);
	}

	inline static int32_t get_offset_of_particleCount_2() { return static_cast<int32_t>(offsetof(Voxel_t3105992857, ___particleCount_2)); }
	inline int32_t get_particleCount_2() const { return ___particleCount_2; }
	inline int32_t* get_address_of_particleCount_2() { return &___particleCount_2; }
	inline void set_particleCount_2(int32_t value)
	{
		___particleCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel
struct Voxel_t3105992857_marshaled_pinvoke
{
	Bounds_t3033363703  ___bounds_0;
	int32_t* ___particles_1;
	int32_t ___particleCount_2;
};
// Native definition for COM marshalling of MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel
struct Voxel_t3105992857_marshaled_com
{
	Bounds_t3033363703  ___bounds_0;
	int32_t* ___particles_1;
	int32_t ___particleCount_2;
};
#endif // VOXEL_T3105992857_H
#ifndef SHAKE_T2901923000_H
#define SHAKE_T2901923000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.CameraShake/Shake
struct  Shake_t2901923000  : public RuntimeObject
{
public:
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::amplitude
	float ___amplitude_0;
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::frequency
	float ___frequency_1;
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::duration
	float ___duration_2;
	// MirzaBeig.ParticleSystems.Demos.CameraShakeTarget MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::target
	int32_t ___target_3;
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::timeRemaining
	float ___timeRemaining_4;
	// UnityEngine.Vector2 MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::perlinNoiseX
	Vector2_t2243707579  ___perlinNoiseX_5;
	// UnityEngine.Vector2 MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::perlinNoiseY
	Vector2_t2243707579  ___perlinNoiseY_6;
	// UnityEngine.Vector2 MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::perlinNoiseZ
	Vector2_t2243707579  ___perlinNoiseZ_7;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::noise
	Vector3_t2243707580  ___noise_8;
	// UnityEngine.AnimationCurve MirzaBeig.ParticleSystems.Demos.CameraShake/Shake::amplitudeOverLifetimeCurve
	AnimationCurve_t3306541151 * ___amplitudeOverLifetimeCurve_9;

public:
	inline static int32_t get_offset_of_amplitude_0() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___amplitude_0)); }
	inline float get_amplitude_0() const { return ___amplitude_0; }
	inline float* get_address_of_amplitude_0() { return &___amplitude_0; }
	inline void set_amplitude_0(float value)
	{
		___amplitude_0 = value;
	}

	inline static int32_t get_offset_of_frequency_1() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___frequency_1)); }
	inline float get_frequency_1() const { return ___frequency_1; }
	inline float* get_address_of_frequency_1() { return &___frequency_1; }
	inline void set_frequency_1(float value)
	{
		___frequency_1 = value;
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___target_3)); }
	inline int32_t get_target_3() const { return ___target_3; }
	inline int32_t* get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(int32_t value)
	{
		___target_3 = value;
	}

	inline static int32_t get_offset_of_timeRemaining_4() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___timeRemaining_4)); }
	inline float get_timeRemaining_4() const { return ___timeRemaining_4; }
	inline float* get_address_of_timeRemaining_4() { return &___timeRemaining_4; }
	inline void set_timeRemaining_4(float value)
	{
		___timeRemaining_4 = value;
	}

	inline static int32_t get_offset_of_perlinNoiseX_5() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___perlinNoiseX_5)); }
	inline Vector2_t2243707579  get_perlinNoiseX_5() const { return ___perlinNoiseX_5; }
	inline Vector2_t2243707579 * get_address_of_perlinNoiseX_5() { return &___perlinNoiseX_5; }
	inline void set_perlinNoiseX_5(Vector2_t2243707579  value)
	{
		___perlinNoiseX_5 = value;
	}

	inline static int32_t get_offset_of_perlinNoiseY_6() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___perlinNoiseY_6)); }
	inline Vector2_t2243707579  get_perlinNoiseY_6() const { return ___perlinNoiseY_6; }
	inline Vector2_t2243707579 * get_address_of_perlinNoiseY_6() { return &___perlinNoiseY_6; }
	inline void set_perlinNoiseY_6(Vector2_t2243707579  value)
	{
		___perlinNoiseY_6 = value;
	}

	inline static int32_t get_offset_of_perlinNoiseZ_7() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___perlinNoiseZ_7)); }
	inline Vector2_t2243707579  get_perlinNoiseZ_7() const { return ___perlinNoiseZ_7; }
	inline Vector2_t2243707579 * get_address_of_perlinNoiseZ_7() { return &___perlinNoiseZ_7; }
	inline void set_perlinNoiseZ_7(Vector2_t2243707579  value)
	{
		___perlinNoiseZ_7 = value;
	}

	inline static int32_t get_offset_of_noise_8() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___noise_8)); }
	inline Vector3_t2243707580  get_noise_8() const { return ___noise_8; }
	inline Vector3_t2243707580 * get_address_of_noise_8() { return &___noise_8; }
	inline void set_noise_8(Vector3_t2243707580  value)
	{
		___noise_8 = value;
	}

	inline static int32_t get_offset_of_amplitudeOverLifetimeCurve_9() { return static_cast<int32_t>(offsetof(Shake_t2901923000, ___amplitudeOverLifetimeCurve_9)); }
	inline AnimationCurve_t3306541151 * get_amplitudeOverLifetimeCurve_9() const { return ___amplitudeOverLifetimeCurve_9; }
	inline AnimationCurve_t3306541151 ** get_address_of_amplitudeOverLifetimeCurve_9() { return &___amplitudeOverLifetimeCurve_9; }
	inline void set_amplitudeOverLifetimeCurve_9(AnimationCurve_t3306541151 * value)
	{
		___amplitudeOverLifetimeCurve_9 = value;
		Il2CppCodeGenWriteBarrier((&___amplitudeOverLifetimeCurve_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAKE_T2901923000_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ONPARTICLESYSTEMSDEADEVENTHANDLER_T222977030_H
#define ONPARTICLESYSTEMSDEADEVENTHANDLER_T222977030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.ParticleSystems/onParticleSystemsDeadEventHandler
struct  onParticleSystemsDeadEventHandler_t222977030  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPARTICLESYSTEMSDEADEVENTHANDLER_T222977030_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef PARTICLEPLEXUS_T126557311_H
#define PARTICLEPLEXUS_T126557311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticlePlexus
struct  ParticlePlexus_t126557311  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.Scripting.Effects.ParticlePlexus::maxDistance
	float ___maxDistance_2;
	// System.Int32 MirzaBeig.Scripting.Effects.ParticlePlexus::maxConnections
	int32_t ___maxConnections_3;
	// System.Int32 MirzaBeig.Scripting.Effects.ParticlePlexus::maxLineRenderers
	int32_t ___maxLineRenderers_4;
	// System.Single MirzaBeig.Scripting.Effects.ParticlePlexus::widthFromParticle
	float ___widthFromParticle_5;
	// System.Single MirzaBeig.Scripting.Effects.ParticlePlexus::colourFromParticle
	float ___colourFromParticle_6;
	// System.Single MirzaBeig.Scripting.Effects.ParticlePlexus::alphaFromParticle
	float ___alphaFromParticle_7;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticlePlexus::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_8;
	// UnityEngine.ParticleSystem/Particle[] MirzaBeig.Scripting.Effects.ParticlePlexus::particles
	ParticleU5BU5D_t574222242* ___particles_9;
	// UnityEngine.Vector3[] MirzaBeig.Scripting.Effects.ParticlePlexus::particlePositions
	Vector3U5BU5D_t1172311765* ___particlePositions_10;
	// UnityEngine.Color[] MirzaBeig.Scripting.Effects.ParticlePlexus::particleColours
	ColorU5BU5D_t672350442* ___particleColours_11;
	// System.Single[] MirzaBeig.Scripting.Effects.ParticlePlexus::particleSizes
	SingleU5BU5D_t577127397* ___particleSizes_12;
	// UnityEngine.ParticleSystem/MainModule MirzaBeig.Scripting.Effects.ParticlePlexus::particleSystemMainModule
	MainModule_t6751348  ___particleSystemMainModule_13;
	// UnityEngine.LineRenderer MirzaBeig.Scripting.Effects.ParticlePlexus::lineRendererTemplate
	LineRenderer_t849157671 * ___lineRendererTemplate_14;
	// System.Collections.Generic.List`1<UnityEngine.LineRenderer> MirzaBeig.Scripting.Effects.ParticlePlexus::lineRenderers
	List_1_t218278803 * ___lineRenderers_15;
	// UnityEngine.Transform MirzaBeig.Scripting.Effects.ParticlePlexus::_transform
	Transform_t3275118058 * ____transform_16;
	// System.Single MirzaBeig.Scripting.Effects.ParticlePlexus::delay
	float ___delay_17;
	// System.Single MirzaBeig.Scripting.Effects.ParticlePlexus::timer
	float ___timer_18;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticlePlexus::alwaysUpdate
	bool ___alwaysUpdate_19;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticlePlexus::visible
	bool ___visible_20;

public:
	inline static int32_t get_offset_of_maxDistance_2() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___maxDistance_2)); }
	inline float get_maxDistance_2() const { return ___maxDistance_2; }
	inline float* get_address_of_maxDistance_2() { return &___maxDistance_2; }
	inline void set_maxDistance_2(float value)
	{
		___maxDistance_2 = value;
	}

	inline static int32_t get_offset_of_maxConnections_3() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___maxConnections_3)); }
	inline int32_t get_maxConnections_3() const { return ___maxConnections_3; }
	inline int32_t* get_address_of_maxConnections_3() { return &___maxConnections_3; }
	inline void set_maxConnections_3(int32_t value)
	{
		___maxConnections_3 = value;
	}

	inline static int32_t get_offset_of_maxLineRenderers_4() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___maxLineRenderers_4)); }
	inline int32_t get_maxLineRenderers_4() const { return ___maxLineRenderers_4; }
	inline int32_t* get_address_of_maxLineRenderers_4() { return &___maxLineRenderers_4; }
	inline void set_maxLineRenderers_4(int32_t value)
	{
		___maxLineRenderers_4 = value;
	}

	inline static int32_t get_offset_of_widthFromParticle_5() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___widthFromParticle_5)); }
	inline float get_widthFromParticle_5() const { return ___widthFromParticle_5; }
	inline float* get_address_of_widthFromParticle_5() { return &___widthFromParticle_5; }
	inline void set_widthFromParticle_5(float value)
	{
		___widthFromParticle_5 = value;
	}

	inline static int32_t get_offset_of_colourFromParticle_6() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___colourFromParticle_6)); }
	inline float get_colourFromParticle_6() const { return ___colourFromParticle_6; }
	inline float* get_address_of_colourFromParticle_6() { return &___colourFromParticle_6; }
	inline void set_colourFromParticle_6(float value)
	{
		___colourFromParticle_6 = value;
	}

	inline static int32_t get_offset_of_alphaFromParticle_7() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___alphaFromParticle_7)); }
	inline float get_alphaFromParticle_7() const { return ___alphaFromParticle_7; }
	inline float* get_address_of_alphaFromParticle_7() { return &___alphaFromParticle_7; }
	inline void set_alphaFromParticle_7(float value)
	{
		___alphaFromParticle_7 = value;
	}

	inline static int32_t get_offset_of_particleSystem_8() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___particleSystem_8)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_8() const { return ___particleSystem_8; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_8() { return &___particleSystem_8; }
	inline void set_particleSystem_8(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_8 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_8), value);
	}

	inline static int32_t get_offset_of_particles_9() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___particles_9)); }
	inline ParticleU5BU5D_t574222242* get_particles_9() const { return ___particles_9; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_9() { return &___particles_9; }
	inline void set_particles_9(ParticleU5BU5D_t574222242* value)
	{
		___particles_9 = value;
		Il2CppCodeGenWriteBarrier((&___particles_9), value);
	}

	inline static int32_t get_offset_of_particlePositions_10() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___particlePositions_10)); }
	inline Vector3U5BU5D_t1172311765* get_particlePositions_10() const { return ___particlePositions_10; }
	inline Vector3U5BU5D_t1172311765** get_address_of_particlePositions_10() { return &___particlePositions_10; }
	inline void set_particlePositions_10(Vector3U5BU5D_t1172311765* value)
	{
		___particlePositions_10 = value;
		Il2CppCodeGenWriteBarrier((&___particlePositions_10), value);
	}

	inline static int32_t get_offset_of_particleColours_11() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___particleColours_11)); }
	inline ColorU5BU5D_t672350442* get_particleColours_11() const { return ___particleColours_11; }
	inline ColorU5BU5D_t672350442** get_address_of_particleColours_11() { return &___particleColours_11; }
	inline void set_particleColours_11(ColorU5BU5D_t672350442* value)
	{
		___particleColours_11 = value;
		Il2CppCodeGenWriteBarrier((&___particleColours_11), value);
	}

	inline static int32_t get_offset_of_particleSizes_12() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___particleSizes_12)); }
	inline SingleU5BU5D_t577127397* get_particleSizes_12() const { return ___particleSizes_12; }
	inline SingleU5BU5D_t577127397** get_address_of_particleSizes_12() { return &___particleSizes_12; }
	inline void set_particleSizes_12(SingleU5BU5D_t577127397* value)
	{
		___particleSizes_12 = value;
		Il2CppCodeGenWriteBarrier((&___particleSizes_12), value);
	}

	inline static int32_t get_offset_of_particleSystemMainModule_13() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___particleSystemMainModule_13)); }
	inline MainModule_t6751348  get_particleSystemMainModule_13() const { return ___particleSystemMainModule_13; }
	inline MainModule_t6751348 * get_address_of_particleSystemMainModule_13() { return &___particleSystemMainModule_13; }
	inline void set_particleSystemMainModule_13(MainModule_t6751348  value)
	{
		___particleSystemMainModule_13 = value;
	}

	inline static int32_t get_offset_of_lineRendererTemplate_14() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___lineRendererTemplate_14)); }
	inline LineRenderer_t849157671 * get_lineRendererTemplate_14() const { return ___lineRendererTemplate_14; }
	inline LineRenderer_t849157671 ** get_address_of_lineRendererTemplate_14() { return &___lineRendererTemplate_14; }
	inline void set_lineRendererTemplate_14(LineRenderer_t849157671 * value)
	{
		___lineRendererTemplate_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineRendererTemplate_14), value);
	}

	inline static int32_t get_offset_of_lineRenderers_15() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___lineRenderers_15)); }
	inline List_1_t218278803 * get_lineRenderers_15() const { return ___lineRenderers_15; }
	inline List_1_t218278803 ** get_address_of_lineRenderers_15() { return &___lineRenderers_15; }
	inline void set_lineRenderers_15(List_1_t218278803 * value)
	{
		___lineRenderers_15 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderers_15), value);
	}

	inline static int32_t get_offset_of__transform_16() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ____transform_16)); }
	inline Transform_t3275118058 * get__transform_16() const { return ____transform_16; }
	inline Transform_t3275118058 ** get_address_of__transform_16() { return &____transform_16; }
	inline void set__transform_16(Transform_t3275118058 * value)
	{
		____transform_16 = value;
		Il2CppCodeGenWriteBarrier((&____transform_16), value);
	}

	inline static int32_t get_offset_of_delay_17() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___delay_17)); }
	inline float get_delay_17() const { return ___delay_17; }
	inline float* get_address_of_delay_17() { return &___delay_17; }
	inline void set_delay_17(float value)
	{
		___delay_17 = value;
	}

	inline static int32_t get_offset_of_timer_18() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___timer_18)); }
	inline float get_timer_18() const { return ___timer_18; }
	inline float* get_address_of_timer_18() { return &___timer_18; }
	inline void set_timer_18(float value)
	{
		___timer_18 = value;
	}

	inline static int32_t get_offset_of_alwaysUpdate_19() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___alwaysUpdate_19)); }
	inline bool get_alwaysUpdate_19() const { return ___alwaysUpdate_19; }
	inline bool* get_address_of_alwaysUpdate_19() { return &___alwaysUpdate_19; }
	inline void set_alwaysUpdate_19(bool value)
	{
		___alwaysUpdate_19 = value;
	}

	inline static int32_t get_offset_of_visible_20() { return static_cast<int32_t>(offsetof(ParticlePlexus_t126557311, ___visible_20)); }
	inline bool get_visible_20() const { return ___visible_20; }
	inline bool* get_address_of_visible_20() { return &___visible_20; }
	inline void set_visible_20(bool value)
	{
		___visible_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPLEXUS_T126557311_H
#ifndef PARTICLEPLEXUSDEMO_CAMERARIG_T3759789884_H
#define PARTICLEPLEXUSDEMO_CAMERARIG_T3759789884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlePlexusDemo_CameraRig
struct  ParticlePlexusDemo_CameraRig_t3759789884  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform ParticlePlexusDemo_CameraRig::pivot
	Transform_t3275118058 * ___pivot_2;
	// UnityEngine.Vector3 ParticlePlexusDemo_CameraRig::targetRotation
	Vector3_t2243707580  ___targetRotation_3;
	// System.Single ParticlePlexusDemo_CameraRig::rotationLimit
	float ___rotationLimit_4;
	// System.Single ParticlePlexusDemo_CameraRig::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single ParticlePlexusDemo_CameraRig::rotationLerpSpeed
	float ___rotationLerpSpeed_6;
	// UnityEngine.Vector3 ParticlePlexusDemo_CameraRig::startRotation
	Vector3_t2243707580  ___startRotation_7;

public:
	inline static int32_t get_offset_of_pivot_2() { return static_cast<int32_t>(offsetof(ParticlePlexusDemo_CameraRig_t3759789884, ___pivot_2)); }
	inline Transform_t3275118058 * get_pivot_2() const { return ___pivot_2; }
	inline Transform_t3275118058 ** get_address_of_pivot_2() { return &___pivot_2; }
	inline void set_pivot_2(Transform_t3275118058 * value)
	{
		___pivot_2 = value;
		Il2CppCodeGenWriteBarrier((&___pivot_2), value);
	}

	inline static int32_t get_offset_of_targetRotation_3() { return static_cast<int32_t>(offsetof(ParticlePlexusDemo_CameraRig_t3759789884, ___targetRotation_3)); }
	inline Vector3_t2243707580  get_targetRotation_3() const { return ___targetRotation_3; }
	inline Vector3_t2243707580 * get_address_of_targetRotation_3() { return &___targetRotation_3; }
	inline void set_targetRotation_3(Vector3_t2243707580  value)
	{
		___targetRotation_3 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_4() { return static_cast<int32_t>(offsetof(ParticlePlexusDemo_CameraRig_t3759789884, ___rotationLimit_4)); }
	inline float get_rotationLimit_4() const { return ___rotationLimit_4; }
	inline float* get_address_of_rotationLimit_4() { return &___rotationLimit_4; }
	inline void set_rotationLimit_4(float value)
	{
		___rotationLimit_4 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(ParticlePlexusDemo_CameraRig_t3759789884, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_rotationLerpSpeed_6() { return static_cast<int32_t>(offsetof(ParticlePlexusDemo_CameraRig_t3759789884, ___rotationLerpSpeed_6)); }
	inline float get_rotationLerpSpeed_6() const { return ___rotationLerpSpeed_6; }
	inline float* get_address_of_rotationLerpSpeed_6() { return &___rotationLerpSpeed_6; }
	inline void set_rotationLerpSpeed_6(float value)
	{
		___rotationLerpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_startRotation_7() { return static_cast<int32_t>(offsetof(ParticlePlexusDemo_CameraRig_t3759789884, ___startRotation_7)); }
	inline Vector3_t2243707580  get_startRotation_7() const { return ___startRotation_7; }
	inline Vector3_t2243707580 * get_address_of_startRotation_7() { return &___startRotation_7; }
	inline void set_startRotation_7(Vector3_t2243707580  value)
	{
		___startRotation_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPLEXUSDEMO_CAMERARIG_T3759789884_H
#ifndef UILINECONNECTOR_T1781812353_H
#define UILINECONNECTOR_T1781812353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILineConnector
struct  UILineConnector_t1781812353  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform[] UnityEngine.UI.Extensions.UILineConnector::transforms
	RectTransformU5BU5D_t3948421699* ___transforms_2;
	// UnityEngine.Vector2[] UnityEngine.UI.Extensions.UILineConnector::previousPositions
	Vector2U5BU5D_t686124026* ___previousPositions_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UILineConnector::canvas
	RectTransform_t3349966182 * ___canvas_4;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UILineConnector::rt
	RectTransform_t3349966182 * ___rt_5;
	// UnityEngine.UI.Extensions.UILineRenderer UnityEngine.UI.Extensions.UILineConnector::lr
	UILineRenderer_t3031355003 * ___lr_6;

public:
	inline static int32_t get_offset_of_transforms_2() { return static_cast<int32_t>(offsetof(UILineConnector_t1781812353, ___transforms_2)); }
	inline RectTransformU5BU5D_t3948421699* get_transforms_2() const { return ___transforms_2; }
	inline RectTransformU5BU5D_t3948421699** get_address_of_transforms_2() { return &___transforms_2; }
	inline void set_transforms_2(RectTransformU5BU5D_t3948421699* value)
	{
		___transforms_2 = value;
		Il2CppCodeGenWriteBarrier((&___transforms_2), value);
	}

	inline static int32_t get_offset_of_previousPositions_3() { return static_cast<int32_t>(offsetof(UILineConnector_t1781812353, ___previousPositions_3)); }
	inline Vector2U5BU5D_t686124026* get_previousPositions_3() const { return ___previousPositions_3; }
	inline Vector2U5BU5D_t686124026** get_address_of_previousPositions_3() { return &___previousPositions_3; }
	inline void set_previousPositions_3(Vector2U5BU5D_t686124026* value)
	{
		___previousPositions_3 = value;
		Il2CppCodeGenWriteBarrier((&___previousPositions_3), value);
	}

	inline static int32_t get_offset_of_canvas_4() { return static_cast<int32_t>(offsetof(UILineConnector_t1781812353, ___canvas_4)); }
	inline RectTransform_t3349966182 * get_canvas_4() const { return ___canvas_4; }
	inline RectTransform_t3349966182 ** get_address_of_canvas_4() { return &___canvas_4; }
	inline void set_canvas_4(RectTransform_t3349966182 * value)
	{
		___canvas_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_4), value);
	}

	inline static int32_t get_offset_of_rt_5() { return static_cast<int32_t>(offsetof(UILineConnector_t1781812353, ___rt_5)); }
	inline RectTransform_t3349966182 * get_rt_5() const { return ___rt_5; }
	inline RectTransform_t3349966182 ** get_address_of_rt_5() { return &___rt_5; }
	inline void set_rt_5(RectTransform_t3349966182 * value)
	{
		___rt_5 = value;
		Il2CppCodeGenWriteBarrier((&___rt_5), value);
	}

	inline static int32_t get_offset_of_lr_6() { return static_cast<int32_t>(offsetof(UILineConnector_t1781812353, ___lr_6)); }
	inline UILineRenderer_t3031355003 * get_lr_6() const { return ___lr_6; }
	inline UILineRenderer_t3031355003 ** get_address_of_lr_6() { return &___lr_6; }
	inline void set_lr_6(UILineRenderer_t3031355003 * value)
	{
		___lr_6 = value;
		Il2CppCodeGenWriteBarrier((&___lr_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINECONNECTOR_T1781812353_H
#ifndef IEBASE_T2978259237_H
#define IEBASE_T2978259237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Shaders.ImageEffects.IEBase
struct  IEBase_t2978259237  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material MirzaBeig.Shaders.ImageEffects.IEBase::_material
	Material_t193706927 * ____material_2;
	// UnityEngine.Shader MirzaBeig.Shaders.ImageEffects.IEBase::<shader>k__BackingField
	Shader_t2430389951 * ___U3CshaderU3Ek__BackingField_3;
	// UnityEngine.Camera MirzaBeig.Shaders.ImageEffects.IEBase::_camera
	Camera_t189460977 * ____camera_4;

public:
	inline static int32_t get_offset_of__material_2() { return static_cast<int32_t>(offsetof(IEBase_t2978259237, ____material_2)); }
	inline Material_t193706927 * get__material_2() const { return ____material_2; }
	inline Material_t193706927 ** get_address_of__material_2() { return &____material_2; }
	inline void set__material_2(Material_t193706927 * value)
	{
		____material_2 = value;
		Il2CppCodeGenWriteBarrier((&____material_2), value);
	}

	inline static int32_t get_offset_of_U3CshaderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(IEBase_t2978259237, ___U3CshaderU3Ek__BackingField_3)); }
	inline Shader_t2430389951 * get_U3CshaderU3Ek__BackingField_3() const { return ___U3CshaderU3Ek__BackingField_3; }
	inline Shader_t2430389951 ** get_address_of_U3CshaderU3Ek__BackingField_3() { return &___U3CshaderU3Ek__BackingField_3; }
	inline void set_U3CshaderU3Ek__BackingField_3(Shader_t2430389951 * value)
	{
		___U3CshaderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CshaderU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of__camera_4() { return static_cast<int32_t>(offsetof(IEBase_t2978259237, ____camera_4)); }
	inline Camera_t189460977 * get__camera_4() const { return ____camera_4; }
	inline Camera_t189460977 ** get_address_of__camera_4() { return &____camera_4; }
	inline void set__camera_4(Camera_t189460977 * value)
	{
		____camera_4 = value;
		Il2CppCodeGenWriteBarrier((&____camera_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IEBASE_T2978259237_H
#ifndef UI_TWEENSCALE_T1720817768_H
#define UI_TWEENSCALE_T1720817768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_TweenScale
struct  UI_TweenScale_t1720817768  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationCurve UnityEngine.UI.Extensions.UI_TweenScale::animCurve
	AnimationCurve_t3306541151 * ___animCurve_2;
	// System.Single UnityEngine.UI.Extensions.UI_TweenScale::speed
	float ___speed_3;
	// System.Boolean UnityEngine.UI.Extensions.UI_TweenScale::isLoop
	bool ___isLoop_4;
	// System.Boolean UnityEngine.UI.Extensions.UI_TweenScale::playAtAwake
	bool ___playAtAwake_5;
	// System.Boolean UnityEngine.UI.Extensions.UI_TweenScale::isUniform
	bool ___isUniform_6;
	// UnityEngine.AnimationCurve UnityEngine.UI.Extensions.UI_TweenScale::animCurveY
	AnimationCurve_t3306541151 * ___animCurveY_7;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.UI_TweenScale::initScale
	Vector3_t2243707580  ___initScale_8;
	// UnityEngine.Transform UnityEngine.UI.Extensions.UI_TweenScale::myTransform
	Transform_t3275118058 * ___myTransform_9;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.UI_TweenScale::newScale
	Vector3_t2243707580  ___newScale_10;

public:
	inline static int32_t get_offset_of_animCurve_2() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___animCurve_2)); }
	inline AnimationCurve_t3306541151 * get_animCurve_2() const { return ___animCurve_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_animCurve_2() { return &___animCurve_2; }
	inline void set_animCurve_2(AnimationCurve_t3306541151 * value)
	{
		___animCurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___animCurve_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_isLoop_4() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___isLoop_4)); }
	inline bool get_isLoop_4() const { return ___isLoop_4; }
	inline bool* get_address_of_isLoop_4() { return &___isLoop_4; }
	inline void set_isLoop_4(bool value)
	{
		___isLoop_4 = value;
	}

	inline static int32_t get_offset_of_playAtAwake_5() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___playAtAwake_5)); }
	inline bool get_playAtAwake_5() const { return ___playAtAwake_5; }
	inline bool* get_address_of_playAtAwake_5() { return &___playAtAwake_5; }
	inline void set_playAtAwake_5(bool value)
	{
		___playAtAwake_5 = value;
	}

	inline static int32_t get_offset_of_isUniform_6() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___isUniform_6)); }
	inline bool get_isUniform_6() const { return ___isUniform_6; }
	inline bool* get_address_of_isUniform_6() { return &___isUniform_6; }
	inline void set_isUniform_6(bool value)
	{
		___isUniform_6 = value;
	}

	inline static int32_t get_offset_of_animCurveY_7() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___animCurveY_7)); }
	inline AnimationCurve_t3306541151 * get_animCurveY_7() const { return ___animCurveY_7; }
	inline AnimationCurve_t3306541151 ** get_address_of_animCurveY_7() { return &___animCurveY_7; }
	inline void set_animCurveY_7(AnimationCurve_t3306541151 * value)
	{
		___animCurveY_7 = value;
		Il2CppCodeGenWriteBarrier((&___animCurveY_7), value);
	}

	inline static int32_t get_offset_of_initScale_8() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___initScale_8)); }
	inline Vector3_t2243707580  get_initScale_8() const { return ___initScale_8; }
	inline Vector3_t2243707580 * get_address_of_initScale_8() { return &___initScale_8; }
	inline void set_initScale_8(Vector3_t2243707580  value)
	{
		___initScale_8 = value;
	}

	inline static int32_t get_offset_of_myTransform_9() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___myTransform_9)); }
	inline Transform_t3275118058 * get_myTransform_9() const { return ___myTransform_9; }
	inline Transform_t3275118058 ** get_address_of_myTransform_9() { return &___myTransform_9; }
	inline void set_myTransform_9(Transform_t3275118058 * value)
	{
		___myTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___myTransform_9), value);
	}

	inline static int32_t get_offset_of_newScale_10() { return static_cast<int32_t>(offsetof(UI_TweenScale_t1720817768, ___newScale_10)); }
	inline Vector3_t2243707580  get_newScale_10() const { return ___newScale_10; }
	inline Vector3_t2243707580 * get_address_of_newScale_10() { return &___newScale_10; }
	inline void set_newScale_10(Vector3_t2243707580  value)
	{
		___newScale_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_TWEENSCALE_T1720817768_H
#ifndef UIHIGHLIGHTABLE_T542579012_H
#define UIHIGHLIGHTABLE_T542579012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIHighlightable
struct  UIHighlightable_t542579012  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.UIHighlightable::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;
	// System.Boolean UnityEngine.UI.Extensions.UIHighlightable::m_Highlighted
	bool ___m_Highlighted_3;
	// System.Boolean UnityEngine.UI.Extensions.UIHighlightable::m_Pressed
	bool ___m_Pressed_4;
	// System.Boolean UnityEngine.UI.Extensions.UIHighlightable::m_Interactable
	bool ___m_Interactable_5;
	// System.Boolean UnityEngine.UI.Extensions.UIHighlightable::m_ClickToHold
	bool ___m_ClickToHold_6;
	// UnityEngine.Color UnityEngine.UI.Extensions.UIHighlightable::NormalColor
	Color_t2020392075  ___NormalColor_7;
	// UnityEngine.Color UnityEngine.UI.Extensions.UIHighlightable::HighlightedColor
	Color_t2020392075  ___HighlightedColor_8;
	// UnityEngine.Color UnityEngine.UI.Extensions.UIHighlightable::PressedColor
	Color_t2020392075  ___PressedColor_9;
	// UnityEngine.Color UnityEngine.UI.Extensions.UIHighlightable::DisabledColor
	Color_t2020392075  ___DisabledColor_10;
	// UnityEngine.UI.Extensions.UIHighlightable/InteractableChangedEvent UnityEngine.UI.Extensions.UIHighlightable::OnInteractableChanged
	InteractableChangedEvent_t3882566494 * ___OnInteractableChanged_11;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}

	inline static int32_t get_offset_of_m_Highlighted_3() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___m_Highlighted_3)); }
	inline bool get_m_Highlighted_3() const { return ___m_Highlighted_3; }
	inline bool* get_address_of_m_Highlighted_3() { return &___m_Highlighted_3; }
	inline void set_m_Highlighted_3(bool value)
	{
		___m_Highlighted_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}

	inline static int32_t get_offset_of_m_Interactable_5() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___m_Interactable_5)); }
	inline bool get_m_Interactable_5() const { return ___m_Interactable_5; }
	inline bool* get_address_of_m_Interactable_5() { return &___m_Interactable_5; }
	inline void set_m_Interactable_5(bool value)
	{
		___m_Interactable_5 = value;
	}

	inline static int32_t get_offset_of_m_ClickToHold_6() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___m_ClickToHold_6)); }
	inline bool get_m_ClickToHold_6() const { return ___m_ClickToHold_6; }
	inline bool* get_address_of_m_ClickToHold_6() { return &___m_ClickToHold_6; }
	inline void set_m_ClickToHold_6(bool value)
	{
		___m_ClickToHold_6 = value;
	}

	inline static int32_t get_offset_of_NormalColor_7() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___NormalColor_7)); }
	inline Color_t2020392075  get_NormalColor_7() const { return ___NormalColor_7; }
	inline Color_t2020392075 * get_address_of_NormalColor_7() { return &___NormalColor_7; }
	inline void set_NormalColor_7(Color_t2020392075  value)
	{
		___NormalColor_7 = value;
	}

	inline static int32_t get_offset_of_HighlightedColor_8() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___HighlightedColor_8)); }
	inline Color_t2020392075  get_HighlightedColor_8() const { return ___HighlightedColor_8; }
	inline Color_t2020392075 * get_address_of_HighlightedColor_8() { return &___HighlightedColor_8; }
	inline void set_HighlightedColor_8(Color_t2020392075  value)
	{
		___HighlightedColor_8 = value;
	}

	inline static int32_t get_offset_of_PressedColor_9() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___PressedColor_9)); }
	inline Color_t2020392075  get_PressedColor_9() const { return ___PressedColor_9; }
	inline Color_t2020392075 * get_address_of_PressedColor_9() { return &___PressedColor_9; }
	inline void set_PressedColor_9(Color_t2020392075  value)
	{
		___PressedColor_9 = value;
	}

	inline static int32_t get_offset_of_DisabledColor_10() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___DisabledColor_10)); }
	inline Color_t2020392075  get_DisabledColor_10() const { return ___DisabledColor_10; }
	inline Color_t2020392075 * get_address_of_DisabledColor_10() { return &___DisabledColor_10; }
	inline void set_DisabledColor_10(Color_t2020392075  value)
	{
		___DisabledColor_10 = value;
	}

	inline static int32_t get_offset_of_OnInteractableChanged_11() { return static_cast<int32_t>(offsetof(UIHighlightable_t542579012, ___OnInteractableChanged_11)); }
	inline InteractableChangedEvent_t3882566494 * get_OnInteractableChanged_11() const { return ___OnInteractableChanged_11; }
	inline InteractableChangedEvent_t3882566494 ** get_address_of_OnInteractableChanged_11() { return &___OnInteractableChanged_11; }
	inline void set_OnInteractableChanged_11(InteractableChangedEvent_t3882566494 * value)
	{
		___OnInteractableChanged_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnInteractableChanged_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIHIGHLIGHTABLE_T542579012_H
#ifndef CAMERASHAKE_T1322554423_H
#define CAMERASHAKE_T1322554423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.CameraShake
struct  CameraShake_t1322554423  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake::smoothDampTime
	float ___smoothDampTime_2;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.CameraShake::smoothDampPositionVelocity
	Vector3_t2243707580  ___smoothDampPositionVelocity_3;
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake::smoothDampRotationVelocityX
	float ___smoothDampRotationVelocityX_4;
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake::smoothDampRotationVelocityY
	float ___smoothDampRotationVelocityY_5;
	// System.Single MirzaBeig.ParticleSystems.Demos.CameraShake::smoothDampRotationVelocityZ
	float ___smoothDampRotationVelocityZ_6;
	// System.Collections.Generic.List`1<MirzaBeig.ParticleSystems.Demos.CameraShake/Shake> MirzaBeig.ParticleSystems.Demos.CameraShake::shakes
	List_1_t2271044132 * ___shakes_7;

public:
	inline static int32_t get_offset_of_smoothDampTime_2() { return static_cast<int32_t>(offsetof(CameraShake_t1322554423, ___smoothDampTime_2)); }
	inline float get_smoothDampTime_2() const { return ___smoothDampTime_2; }
	inline float* get_address_of_smoothDampTime_2() { return &___smoothDampTime_2; }
	inline void set_smoothDampTime_2(float value)
	{
		___smoothDampTime_2 = value;
	}

	inline static int32_t get_offset_of_smoothDampPositionVelocity_3() { return static_cast<int32_t>(offsetof(CameraShake_t1322554423, ___smoothDampPositionVelocity_3)); }
	inline Vector3_t2243707580  get_smoothDampPositionVelocity_3() const { return ___smoothDampPositionVelocity_3; }
	inline Vector3_t2243707580 * get_address_of_smoothDampPositionVelocity_3() { return &___smoothDampPositionVelocity_3; }
	inline void set_smoothDampPositionVelocity_3(Vector3_t2243707580  value)
	{
		___smoothDampPositionVelocity_3 = value;
	}

	inline static int32_t get_offset_of_smoothDampRotationVelocityX_4() { return static_cast<int32_t>(offsetof(CameraShake_t1322554423, ___smoothDampRotationVelocityX_4)); }
	inline float get_smoothDampRotationVelocityX_4() const { return ___smoothDampRotationVelocityX_4; }
	inline float* get_address_of_smoothDampRotationVelocityX_4() { return &___smoothDampRotationVelocityX_4; }
	inline void set_smoothDampRotationVelocityX_4(float value)
	{
		___smoothDampRotationVelocityX_4 = value;
	}

	inline static int32_t get_offset_of_smoothDampRotationVelocityY_5() { return static_cast<int32_t>(offsetof(CameraShake_t1322554423, ___smoothDampRotationVelocityY_5)); }
	inline float get_smoothDampRotationVelocityY_5() const { return ___smoothDampRotationVelocityY_5; }
	inline float* get_address_of_smoothDampRotationVelocityY_5() { return &___smoothDampRotationVelocityY_5; }
	inline void set_smoothDampRotationVelocityY_5(float value)
	{
		___smoothDampRotationVelocityY_5 = value;
	}

	inline static int32_t get_offset_of_smoothDampRotationVelocityZ_6() { return static_cast<int32_t>(offsetof(CameraShake_t1322554423, ___smoothDampRotationVelocityZ_6)); }
	inline float get_smoothDampRotationVelocityZ_6() const { return ___smoothDampRotationVelocityZ_6; }
	inline float* get_address_of_smoothDampRotationVelocityZ_6() { return &___smoothDampRotationVelocityZ_6; }
	inline void set_smoothDampRotationVelocityZ_6(float value)
	{
		___smoothDampRotationVelocityZ_6 = value;
	}

	inline static int32_t get_offset_of_shakes_7() { return static_cast<int32_t>(offsetof(CameraShake_t1322554423, ___shakes_7)); }
	inline List_1_t2271044132 * get_shakes_7() const { return ___shakes_7; }
	inline List_1_t2271044132 ** get_address_of_shakes_7() { return &___shakes_7; }
	inline void set_shakes_7(List_1_t2271044132 * value)
	{
		___shakes_7 = value;
		Il2CppCodeGenWriteBarrier((&___shakes_7), value);
	}
};

struct CameraShake_t1322554423_StaticFields
{
public:
	// System.Predicate`1<MirzaBeig.ParticleSystems.Demos.CameraShake/Shake> MirzaBeig.ParticleSystems.Demos.CameraShake::<>f__am$cache0
	Predicate_1_t1344893115 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(CameraShake_t1322554423_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Predicate_1_t1344893115 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Predicate_1_t1344893115 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Predicate_1_t1344893115 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKE_T1322554423_H
#ifndef PARTICLELIGHTS_T3626990339_H
#define PARTICLELIGHTS_T3626990339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleLights
struct  ParticleLights_t3626990339  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticleLights::ps
	ParticleSystem_t3394631041 * ___ps_2;
	// System.Collections.Generic.List`1<UnityEngine.Light> MirzaBeig.Scripting.Effects.ParticleLights::lights
	List_1_t4158814064 * ___lights_3;
	// System.Single MirzaBeig.Scripting.Effects.ParticleLights::scale
	float ___scale_4;
	// System.Single MirzaBeig.Scripting.Effects.ParticleLights::intensity
	float ___intensity_5;
	// UnityEngine.Color MirzaBeig.Scripting.Effects.ParticleLights::colour
	Color_t2020392075  ___colour_6;
	// System.Single MirzaBeig.Scripting.Effects.ParticleLights::colourFromParticle
	float ___colourFromParticle_7;
	// UnityEngine.LightShadows MirzaBeig.Scripting.Effects.ParticleLights::shadows
	int32_t ___shadows_8;
	// UnityEngine.GameObject MirzaBeig.Scripting.Effects.ParticleLights::template
	GameObject_t1756533147 * ___template_9;

public:
	inline static int32_t get_offset_of_ps_2() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___ps_2)); }
	inline ParticleSystem_t3394631041 * get_ps_2() const { return ___ps_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_ps_2() { return &___ps_2; }
	inline void set_ps_2(ParticleSystem_t3394631041 * value)
	{
		___ps_2 = value;
		Il2CppCodeGenWriteBarrier((&___ps_2), value);
	}

	inline static int32_t get_offset_of_lights_3() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___lights_3)); }
	inline List_1_t4158814064 * get_lights_3() const { return ___lights_3; }
	inline List_1_t4158814064 ** get_address_of_lights_3() { return &___lights_3; }
	inline void set_lights_3(List_1_t4158814064 * value)
	{
		___lights_3 = value;
		Il2CppCodeGenWriteBarrier((&___lights_3), value);
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___scale_4)); }
	inline float get_scale_4() const { return ___scale_4; }
	inline float* get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(float value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_intensity_5() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___intensity_5)); }
	inline float get_intensity_5() const { return ___intensity_5; }
	inline float* get_address_of_intensity_5() { return &___intensity_5; }
	inline void set_intensity_5(float value)
	{
		___intensity_5 = value;
	}

	inline static int32_t get_offset_of_colour_6() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___colour_6)); }
	inline Color_t2020392075  get_colour_6() const { return ___colour_6; }
	inline Color_t2020392075 * get_address_of_colour_6() { return &___colour_6; }
	inline void set_colour_6(Color_t2020392075  value)
	{
		___colour_6 = value;
	}

	inline static int32_t get_offset_of_colourFromParticle_7() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___colourFromParticle_7)); }
	inline float get_colourFromParticle_7() const { return ___colourFromParticle_7; }
	inline float* get_address_of_colourFromParticle_7() { return &___colourFromParticle_7; }
	inline void set_colourFromParticle_7(float value)
	{
		___colourFromParticle_7 = value;
	}

	inline static int32_t get_offset_of_shadows_8() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___shadows_8)); }
	inline int32_t get_shadows_8() const { return ___shadows_8; }
	inline int32_t* get_address_of_shadows_8() { return &___shadows_8; }
	inline void set_shadows_8(int32_t value)
	{
		___shadows_8 = value;
	}

	inline static int32_t get_offset_of_template_9() { return static_cast<int32_t>(offsetof(ParticleLights_t3626990339, ___template_9)); }
	inline GameObject_t1756533147 * get_template_9() const { return ___template_9; }
	inline GameObject_t1756533147 ** get_address_of_template_9() { return &___template_9; }
	inline void set_template_9(GameObject_t1756533147 * value)
	{
		___template_9 = value;
		Il2CppCodeGenWriteBarrier((&___template_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLELIGHTS_T3626990339_H
#ifndef GRAVITYCLOCKINTERACTIVITYUVFX_T3134117456_H
#define GRAVITYCLOCKINTERACTIVITYUVFX_T3134117456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Demos.Wallpapers.GravityClockInteractivityUVFX
struct  GravityClockInteractivityUVFX_t3134117456  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MirzaBeig.Demos.Wallpapers.GravityClockInteractivityUVFX::forceAffectors
	GameObject_t1756533147 * ___forceAffectors_2;
	// UnityEngine.GameObject MirzaBeig.Demos.Wallpapers.GravityClockInteractivityUVFX::forceAffectors2
	GameObject_t1756533147 * ___forceAffectors2_3;
	// UnityEngine.ParticleSystem MirzaBeig.Demos.Wallpapers.GravityClockInteractivityUVFX::gravityClockPrefab
	ParticleSystem_t3394631041 * ___gravityClockPrefab_4;
	// UnityEngine.ParticleSystem MirzaBeig.Demos.Wallpapers.GravityClockInteractivityUVFX::gravityClock
	ParticleSystem_t3394631041 * ___gravityClock_5;
	// System.Boolean MirzaBeig.Demos.Wallpapers.GravityClockInteractivityUVFX::enableGravityClockVisualEffects
	bool ___enableGravityClockVisualEffects_6;
	// System.Boolean MirzaBeig.Demos.Wallpapers.GravityClockInteractivityUVFX::enableGravityClockAttractionForce
	bool ___enableGravityClockAttractionForce_7;

public:
	inline static int32_t get_offset_of_forceAffectors_2() { return static_cast<int32_t>(offsetof(GravityClockInteractivityUVFX_t3134117456, ___forceAffectors_2)); }
	inline GameObject_t1756533147 * get_forceAffectors_2() const { return ___forceAffectors_2; }
	inline GameObject_t1756533147 ** get_address_of_forceAffectors_2() { return &___forceAffectors_2; }
	inline void set_forceAffectors_2(GameObject_t1756533147 * value)
	{
		___forceAffectors_2 = value;
		Il2CppCodeGenWriteBarrier((&___forceAffectors_2), value);
	}

	inline static int32_t get_offset_of_forceAffectors2_3() { return static_cast<int32_t>(offsetof(GravityClockInteractivityUVFX_t3134117456, ___forceAffectors2_3)); }
	inline GameObject_t1756533147 * get_forceAffectors2_3() const { return ___forceAffectors2_3; }
	inline GameObject_t1756533147 ** get_address_of_forceAffectors2_3() { return &___forceAffectors2_3; }
	inline void set_forceAffectors2_3(GameObject_t1756533147 * value)
	{
		___forceAffectors2_3 = value;
		Il2CppCodeGenWriteBarrier((&___forceAffectors2_3), value);
	}

	inline static int32_t get_offset_of_gravityClockPrefab_4() { return static_cast<int32_t>(offsetof(GravityClockInteractivityUVFX_t3134117456, ___gravityClockPrefab_4)); }
	inline ParticleSystem_t3394631041 * get_gravityClockPrefab_4() const { return ___gravityClockPrefab_4; }
	inline ParticleSystem_t3394631041 ** get_address_of_gravityClockPrefab_4() { return &___gravityClockPrefab_4; }
	inline void set_gravityClockPrefab_4(ParticleSystem_t3394631041 * value)
	{
		___gravityClockPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___gravityClockPrefab_4), value);
	}

	inline static int32_t get_offset_of_gravityClock_5() { return static_cast<int32_t>(offsetof(GravityClockInteractivityUVFX_t3134117456, ___gravityClock_5)); }
	inline ParticleSystem_t3394631041 * get_gravityClock_5() const { return ___gravityClock_5; }
	inline ParticleSystem_t3394631041 ** get_address_of_gravityClock_5() { return &___gravityClock_5; }
	inline void set_gravityClock_5(ParticleSystem_t3394631041 * value)
	{
		___gravityClock_5 = value;
		Il2CppCodeGenWriteBarrier((&___gravityClock_5), value);
	}

	inline static int32_t get_offset_of_enableGravityClockVisualEffects_6() { return static_cast<int32_t>(offsetof(GravityClockInteractivityUVFX_t3134117456, ___enableGravityClockVisualEffects_6)); }
	inline bool get_enableGravityClockVisualEffects_6() const { return ___enableGravityClockVisualEffects_6; }
	inline bool* get_address_of_enableGravityClockVisualEffects_6() { return &___enableGravityClockVisualEffects_6; }
	inline void set_enableGravityClockVisualEffects_6(bool value)
	{
		___enableGravityClockVisualEffects_6 = value;
	}

	inline static int32_t get_offset_of_enableGravityClockAttractionForce_7() { return static_cast<int32_t>(offsetof(GravityClockInteractivityUVFX_t3134117456, ___enableGravityClockAttractionForce_7)); }
	inline bool get_enableGravityClockAttractionForce_7() const { return ___enableGravityClockAttractionForce_7; }
	inline bool* get_address_of_enableGravityClockAttractionForce_7() { return &___enableGravityClockAttractionForce_7; }
	inline void set_enableGravityClockAttractionForce_7(bool value)
	{
		___enableGravityClockAttractionForce_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAVITYCLOCKINTERACTIVITYUVFX_T3134117456_H
#ifndef DEMOMANAGER_T3005492872_H
#define DEMOMANAGER_T3005492872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.DemoManager
struct  DemoManager_t3005492872  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MirzaBeig.ParticleSystems.Demos.DemoManager::cameraRotationTransform
	Transform_t3275118058 * ___cameraRotationTransform_2;
	// UnityEngine.Transform MirzaBeig.ParticleSystems.Demos.DemoManager::cameraTranslationTransform
	Transform_t3275118058 * ___cameraTranslationTransform_3;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::cameraLookAtPosition
	Vector3_t2243707580  ___cameraLookAtPosition_4;
	// MirzaBeig.ParticleSystems.Demos.MouseFollow MirzaBeig.ParticleSystems.Demos.DemoManager::mouse
	MouseFollow_t3233463164 * ___mouse_5;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::targetCameraPosition
	Vector3_t2243707580  ___targetCameraPosition_6;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::targetCameraRotation
	Vector3_t2243707580  ___targetCameraRotation_7;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::cameraPositionStart
	Vector3_t2243707580  ___cameraPositionStart_8;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::cameraRotationStart
	Vector3_t2243707580  ___cameraRotationStart_9;
	// UnityEngine.Vector2 MirzaBeig.ParticleSystems.Demos.DemoManager::input
	Vector2_t2243707579  ___input_10;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::cameraRotation
	Vector3_t2243707580  ___cameraRotation_11;
	// System.Single MirzaBeig.ParticleSystems.Demos.DemoManager::cameraMoveAmount
	float ___cameraMoveAmount_12;
	// System.Single MirzaBeig.ParticleSystems.Demos.DemoManager::cameraRotateAmount
	float ___cameraRotateAmount_13;
	// System.Single MirzaBeig.ParticleSystems.Demos.DemoManager::cameraMoveSpeed
	float ___cameraMoveSpeed_14;
	// System.Single MirzaBeig.ParticleSystems.Demos.DemoManager::cameraRotationSpeed
	float ___cameraRotationSpeed_15;
	// UnityEngine.Vector2 MirzaBeig.ParticleSystems.Demos.DemoManager::cameraAngleLimits
	Vector2_t2243707579  ___cameraAngleLimits_16;
	// UnityEngine.GameObject[] MirzaBeig.ParticleSystems.Demos.DemoManager::levels
	GameObjectU5BU5D_t3057952154* ___levels_17;
	// MirzaBeig.ParticleSystems.Demos.DemoManager/Level MirzaBeig.ParticleSystems.Demos.DemoManager::currentLevel
	int32_t ___currentLevel_18;
	// MirzaBeig.ParticleSystems.Demos.DemoManager/ParticleMode MirzaBeig.ParticleSystems.Demos.DemoManager::particleMode
	int32_t ___particleMode_19;
	// System.Boolean MirzaBeig.ParticleSystems.Demos.DemoManager::advancedRendering
	bool ___advancedRendering_20;
	// UnityEngine.UI.Toggle MirzaBeig.ParticleSystems.Demos.DemoManager::loopingParticleModeToggle
	Toggle_t3976754468 * ___loopingParticleModeToggle_21;
	// UnityEngine.UI.Toggle MirzaBeig.ParticleSystems.Demos.DemoManager::oneshotParticleModeToggle
	Toggle_t3976754468 * ___oneshotParticleModeToggle_22;
	// UnityEngine.UI.Toggle MirzaBeig.ParticleSystems.Demos.DemoManager::advancedRenderingToggle
	Toggle_t3976754468 * ___advancedRenderingToggle_23;
	// UnityEngine.UI.Toggle[] MirzaBeig.ParticleSystems.Demos.DemoManager::levelToggles
	ToggleU5BU5D_t1971649997* ___levelToggles_24;
	// UnityEngine.UI.ToggleGroup MirzaBeig.ParticleSystems.Demos.DemoManager::levelTogglesContainer
	ToggleGroup_t1030026315 * ___levelTogglesContainer_25;
	// MirzaBeig.ParticleSystems.Demos.LoopingParticleSystemsManager MirzaBeig.ParticleSystems.Demos.DemoManager::loopingParticleSystems
	LoopingParticleSystemsManager_t2507471847 * ___loopingParticleSystems_26;
	// MirzaBeig.ParticleSystems.Demos.OneshotParticleSystemsManager MirzaBeig.ParticleSystems.Demos.DemoManager::oneshotParticleSystems
	OneshotParticleSystemsManager_t1174726079 * ___oneshotParticleSystems_27;
	// UnityEngine.GameObject MirzaBeig.ParticleSystems.Demos.DemoManager::ui
	GameObject_t1756533147 * ___ui_28;
	// UnityEngine.UI.Text MirzaBeig.ParticleSystems.Demos.DemoManager::particleCountText
	Text_t356221433 * ___particleCountText_29;
	// UnityEngine.UI.Text MirzaBeig.ParticleSystems.Demos.DemoManager::currentParticleSystemText
	Text_t356221433 * ___currentParticleSystemText_30;
	// UnityEngine.UI.Text MirzaBeig.ParticleSystems.Demos.DemoManager::particleSpawnInstructionText
	Text_t356221433 * ___particleSpawnInstructionText_31;
	// UnityEngine.UI.Slider MirzaBeig.ParticleSystems.Demos.DemoManager::timeScaleSlider
	Slider_t297367283 * ___timeScaleSlider_32;
	// UnityEngine.UI.Text MirzaBeig.ParticleSystems.Demos.DemoManager::timeScaleSliderValueText
	Text_t356221433 * ___timeScaleSliderValueText_33;
	// UnityEngine.Camera MirzaBeig.ParticleSystems.Demos.DemoManager::mainCamera
	Camera_t189460977 * ___mainCamera_34;
	// UnityEngine.MonoBehaviour[] MirzaBeig.ParticleSystems.Demos.DemoManager::mainCameraPostEffects
	MonoBehaviourU5BU5D_t3035069757* ___mainCameraPostEffects_35;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::cameraPositionSmoothDampVelocity
	Vector3_t2243707580  ___cameraPositionSmoothDampVelocity_36;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Demos.DemoManager::cameraRotationSmoothDampVelocity
	Vector3_t2243707580  ___cameraRotationSmoothDampVelocity_37;

public:
	inline static int32_t get_offset_of_cameraRotationTransform_2() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraRotationTransform_2)); }
	inline Transform_t3275118058 * get_cameraRotationTransform_2() const { return ___cameraRotationTransform_2; }
	inline Transform_t3275118058 ** get_address_of_cameraRotationTransform_2() { return &___cameraRotationTransform_2; }
	inline void set_cameraRotationTransform_2(Transform_t3275118058 * value)
	{
		___cameraRotationTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRotationTransform_2), value);
	}

	inline static int32_t get_offset_of_cameraTranslationTransform_3() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraTranslationTransform_3)); }
	inline Transform_t3275118058 * get_cameraTranslationTransform_3() const { return ___cameraTranslationTransform_3; }
	inline Transform_t3275118058 ** get_address_of_cameraTranslationTransform_3() { return &___cameraTranslationTransform_3; }
	inline void set_cameraTranslationTransform_3(Transform_t3275118058 * value)
	{
		___cameraTranslationTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTranslationTransform_3), value);
	}

	inline static int32_t get_offset_of_cameraLookAtPosition_4() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraLookAtPosition_4)); }
	inline Vector3_t2243707580  get_cameraLookAtPosition_4() const { return ___cameraLookAtPosition_4; }
	inline Vector3_t2243707580 * get_address_of_cameraLookAtPosition_4() { return &___cameraLookAtPosition_4; }
	inline void set_cameraLookAtPosition_4(Vector3_t2243707580  value)
	{
		___cameraLookAtPosition_4 = value;
	}

	inline static int32_t get_offset_of_mouse_5() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___mouse_5)); }
	inline MouseFollow_t3233463164 * get_mouse_5() const { return ___mouse_5; }
	inline MouseFollow_t3233463164 ** get_address_of_mouse_5() { return &___mouse_5; }
	inline void set_mouse_5(MouseFollow_t3233463164 * value)
	{
		___mouse_5 = value;
		Il2CppCodeGenWriteBarrier((&___mouse_5), value);
	}

	inline static int32_t get_offset_of_targetCameraPosition_6() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___targetCameraPosition_6)); }
	inline Vector3_t2243707580  get_targetCameraPosition_6() const { return ___targetCameraPosition_6; }
	inline Vector3_t2243707580 * get_address_of_targetCameraPosition_6() { return &___targetCameraPosition_6; }
	inline void set_targetCameraPosition_6(Vector3_t2243707580  value)
	{
		___targetCameraPosition_6 = value;
	}

	inline static int32_t get_offset_of_targetCameraRotation_7() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___targetCameraRotation_7)); }
	inline Vector3_t2243707580  get_targetCameraRotation_7() const { return ___targetCameraRotation_7; }
	inline Vector3_t2243707580 * get_address_of_targetCameraRotation_7() { return &___targetCameraRotation_7; }
	inline void set_targetCameraRotation_7(Vector3_t2243707580  value)
	{
		___targetCameraRotation_7 = value;
	}

	inline static int32_t get_offset_of_cameraPositionStart_8() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraPositionStart_8)); }
	inline Vector3_t2243707580  get_cameraPositionStart_8() const { return ___cameraPositionStart_8; }
	inline Vector3_t2243707580 * get_address_of_cameraPositionStart_8() { return &___cameraPositionStart_8; }
	inline void set_cameraPositionStart_8(Vector3_t2243707580  value)
	{
		___cameraPositionStart_8 = value;
	}

	inline static int32_t get_offset_of_cameraRotationStart_9() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraRotationStart_9)); }
	inline Vector3_t2243707580  get_cameraRotationStart_9() const { return ___cameraRotationStart_9; }
	inline Vector3_t2243707580 * get_address_of_cameraRotationStart_9() { return &___cameraRotationStart_9; }
	inline void set_cameraRotationStart_9(Vector3_t2243707580  value)
	{
		___cameraRotationStart_9 = value;
	}

	inline static int32_t get_offset_of_input_10() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___input_10)); }
	inline Vector2_t2243707579  get_input_10() const { return ___input_10; }
	inline Vector2_t2243707579 * get_address_of_input_10() { return &___input_10; }
	inline void set_input_10(Vector2_t2243707579  value)
	{
		___input_10 = value;
	}

	inline static int32_t get_offset_of_cameraRotation_11() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraRotation_11)); }
	inline Vector3_t2243707580  get_cameraRotation_11() const { return ___cameraRotation_11; }
	inline Vector3_t2243707580 * get_address_of_cameraRotation_11() { return &___cameraRotation_11; }
	inline void set_cameraRotation_11(Vector3_t2243707580  value)
	{
		___cameraRotation_11 = value;
	}

	inline static int32_t get_offset_of_cameraMoveAmount_12() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraMoveAmount_12)); }
	inline float get_cameraMoveAmount_12() const { return ___cameraMoveAmount_12; }
	inline float* get_address_of_cameraMoveAmount_12() { return &___cameraMoveAmount_12; }
	inline void set_cameraMoveAmount_12(float value)
	{
		___cameraMoveAmount_12 = value;
	}

	inline static int32_t get_offset_of_cameraRotateAmount_13() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraRotateAmount_13)); }
	inline float get_cameraRotateAmount_13() const { return ___cameraRotateAmount_13; }
	inline float* get_address_of_cameraRotateAmount_13() { return &___cameraRotateAmount_13; }
	inline void set_cameraRotateAmount_13(float value)
	{
		___cameraRotateAmount_13 = value;
	}

	inline static int32_t get_offset_of_cameraMoveSpeed_14() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraMoveSpeed_14)); }
	inline float get_cameraMoveSpeed_14() const { return ___cameraMoveSpeed_14; }
	inline float* get_address_of_cameraMoveSpeed_14() { return &___cameraMoveSpeed_14; }
	inline void set_cameraMoveSpeed_14(float value)
	{
		___cameraMoveSpeed_14 = value;
	}

	inline static int32_t get_offset_of_cameraRotationSpeed_15() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraRotationSpeed_15)); }
	inline float get_cameraRotationSpeed_15() const { return ___cameraRotationSpeed_15; }
	inline float* get_address_of_cameraRotationSpeed_15() { return &___cameraRotationSpeed_15; }
	inline void set_cameraRotationSpeed_15(float value)
	{
		___cameraRotationSpeed_15 = value;
	}

	inline static int32_t get_offset_of_cameraAngleLimits_16() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraAngleLimits_16)); }
	inline Vector2_t2243707579  get_cameraAngleLimits_16() const { return ___cameraAngleLimits_16; }
	inline Vector2_t2243707579 * get_address_of_cameraAngleLimits_16() { return &___cameraAngleLimits_16; }
	inline void set_cameraAngleLimits_16(Vector2_t2243707579  value)
	{
		___cameraAngleLimits_16 = value;
	}

	inline static int32_t get_offset_of_levels_17() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___levels_17)); }
	inline GameObjectU5BU5D_t3057952154* get_levels_17() const { return ___levels_17; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_levels_17() { return &___levels_17; }
	inline void set_levels_17(GameObjectU5BU5D_t3057952154* value)
	{
		___levels_17 = value;
		Il2CppCodeGenWriteBarrier((&___levels_17), value);
	}

	inline static int32_t get_offset_of_currentLevel_18() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___currentLevel_18)); }
	inline int32_t get_currentLevel_18() const { return ___currentLevel_18; }
	inline int32_t* get_address_of_currentLevel_18() { return &___currentLevel_18; }
	inline void set_currentLevel_18(int32_t value)
	{
		___currentLevel_18 = value;
	}

	inline static int32_t get_offset_of_particleMode_19() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___particleMode_19)); }
	inline int32_t get_particleMode_19() const { return ___particleMode_19; }
	inline int32_t* get_address_of_particleMode_19() { return &___particleMode_19; }
	inline void set_particleMode_19(int32_t value)
	{
		___particleMode_19 = value;
	}

	inline static int32_t get_offset_of_advancedRendering_20() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___advancedRendering_20)); }
	inline bool get_advancedRendering_20() const { return ___advancedRendering_20; }
	inline bool* get_address_of_advancedRendering_20() { return &___advancedRendering_20; }
	inline void set_advancedRendering_20(bool value)
	{
		___advancedRendering_20 = value;
	}

	inline static int32_t get_offset_of_loopingParticleModeToggle_21() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___loopingParticleModeToggle_21)); }
	inline Toggle_t3976754468 * get_loopingParticleModeToggle_21() const { return ___loopingParticleModeToggle_21; }
	inline Toggle_t3976754468 ** get_address_of_loopingParticleModeToggle_21() { return &___loopingParticleModeToggle_21; }
	inline void set_loopingParticleModeToggle_21(Toggle_t3976754468 * value)
	{
		___loopingParticleModeToggle_21 = value;
		Il2CppCodeGenWriteBarrier((&___loopingParticleModeToggle_21), value);
	}

	inline static int32_t get_offset_of_oneshotParticleModeToggle_22() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___oneshotParticleModeToggle_22)); }
	inline Toggle_t3976754468 * get_oneshotParticleModeToggle_22() const { return ___oneshotParticleModeToggle_22; }
	inline Toggle_t3976754468 ** get_address_of_oneshotParticleModeToggle_22() { return &___oneshotParticleModeToggle_22; }
	inline void set_oneshotParticleModeToggle_22(Toggle_t3976754468 * value)
	{
		___oneshotParticleModeToggle_22 = value;
		Il2CppCodeGenWriteBarrier((&___oneshotParticleModeToggle_22), value);
	}

	inline static int32_t get_offset_of_advancedRenderingToggle_23() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___advancedRenderingToggle_23)); }
	inline Toggle_t3976754468 * get_advancedRenderingToggle_23() const { return ___advancedRenderingToggle_23; }
	inline Toggle_t3976754468 ** get_address_of_advancedRenderingToggle_23() { return &___advancedRenderingToggle_23; }
	inline void set_advancedRenderingToggle_23(Toggle_t3976754468 * value)
	{
		___advancedRenderingToggle_23 = value;
		Il2CppCodeGenWriteBarrier((&___advancedRenderingToggle_23), value);
	}

	inline static int32_t get_offset_of_levelToggles_24() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___levelToggles_24)); }
	inline ToggleU5BU5D_t1971649997* get_levelToggles_24() const { return ___levelToggles_24; }
	inline ToggleU5BU5D_t1971649997** get_address_of_levelToggles_24() { return &___levelToggles_24; }
	inline void set_levelToggles_24(ToggleU5BU5D_t1971649997* value)
	{
		___levelToggles_24 = value;
		Il2CppCodeGenWriteBarrier((&___levelToggles_24), value);
	}

	inline static int32_t get_offset_of_levelTogglesContainer_25() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___levelTogglesContainer_25)); }
	inline ToggleGroup_t1030026315 * get_levelTogglesContainer_25() const { return ___levelTogglesContainer_25; }
	inline ToggleGroup_t1030026315 ** get_address_of_levelTogglesContainer_25() { return &___levelTogglesContainer_25; }
	inline void set_levelTogglesContainer_25(ToggleGroup_t1030026315 * value)
	{
		___levelTogglesContainer_25 = value;
		Il2CppCodeGenWriteBarrier((&___levelTogglesContainer_25), value);
	}

	inline static int32_t get_offset_of_loopingParticleSystems_26() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___loopingParticleSystems_26)); }
	inline LoopingParticleSystemsManager_t2507471847 * get_loopingParticleSystems_26() const { return ___loopingParticleSystems_26; }
	inline LoopingParticleSystemsManager_t2507471847 ** get_address_of_loopingParticleSystems_26() { return &___loopingParticleSystems_26; }
	inline void set_loopingParticleSystems_26(LoopingParticleSystemsManager_t2507471847 * value)
	{
		___loopingParticleSystems_26 = value;
		Il2CppCodeGenWriteBarrier((&___loopingParticleSystems_26), value);
	}

	inline static int32_t get_offset_of_oneshotParticleSystems_27() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___oneshotParticleSystems_27)); }
	inline OneshotParticleSystemsManager_t1174726079 * get_oneshotParticleSystems_27() const { return ___oneshotParticleSystems_27; }
	inline OneshotParticleSystemsManager_t1174726079 ** get_address_of_oneshotParticleSystems_27() { return &___oneshotParticleSystems_27; }
	inline void set_oneshotParticleSystems_27(OneshotParticleSystemsManager_t1174726079 * value)
	{
		___oneshotParticleSystems_27 = value;
		Il2CppCodeGenWriteBarrier((&___oneshotParticleSystems_27), value);
	}

	inline static int32_t get_offset_of_ui_28() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___ui_28)); }
	inline GameObject_t1756533147 * get_ui_28() const { return ___ui_28; }
	inline GameObject_t1756533147 ** get_address_of_ui_28() { return &___ui_28; }
	inline void set_ui_28(GameObject_t1756533147 * value)
	{
		___ui_28 = value;
		Il2CppCodeGenWriteBarrier((&___ui_28), value);
	}

	inline static int32_t get_offset_of_particleCountText_29() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___particleCountText_29)); }
	inline Text_t356221433 * get_particleCountText_29() const { return ___particleCountText_29; }
	inline Text_t356221433 ** get_address_of_particleCountText_29() { return &___particleCountText_29; }
	inline void set_particleCountText_29(Text_t356221433 * value)
	{
		___particleCountText_29 = value;
		Il2CppCodeGenWriteBarrier((&___particleCountText_29), value);
	}

	inline static int32_t get_offset_of_currentParticleSystemText_30() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___currentParticleSystemText_30)); }
	inline Text_t356221433 * get_currentParticleSystemText_30() const { return ___currentParticleSystemText_30; }
	inline Text_t356221433 ** get_address_of_currentParticleSystemText_30() { return &___currentParticleSystemText_30; }
	inline void set_currentParticleSystemText_30(Text_t356221433 * value)
	{
		___currentParticleSystemText_30 = value;
		Il2CppCodeGenWriteBarrier((&___currentParticleSystemText_30), value);
	}

	inline static int32_t get_offset_of_particleSpawnInstructionText_31() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___particleSpawnInstructionText_31)); }
	inline Text_t356221433 * get_particleSpawnInstructionText_31() const { return ___particleSpawnInstructionText_31; }
	inline Text_t356221433 ** get_address_of_particleSpawnInstructionText_31() { return &___particleSpawnInstructionText_31; }
	inline void set_particleSpawnInstructionText_31(Text_t356221433 * value)
	{
		___particleSpawnInstructionText_31 = value;
		Il2CppCodeGenWriteBarrier((&___particleSpawnInstructionText_31), value);
	}

	inline static int32_t get_offset_of_timeScaleSlider_32() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___timeScaleSlider_32)); }
	inline Slider_t297367283 * get_timeScaleSlider_32() const { return ___timeScaleSlider_32; }
	inline Slider_t297367283 ** get_address_of_timeScaleSlider_32() { return &___timeScaleSlider_32; }
	inline void set_timeScaleSlider_32(Slider_t297367283 * value)
	{
		___timeScaleSlider_32 = value;
		Il2CppCodeGenWriteBarrier((&___timeScaleSlider_32), value);
	}

	inline static int32_t get_offset_of_timeScaleSliderValueText_33() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___timeScaleSliderValueText_33)); }
	inline Text_t356221433 * get_timeScaleSliderValueText_33() const { return ___timeScaleSliderValueText_33; }
	inline Text_t356221433 ** get_address_of_timeScaleSliderValueText_33() { return &___timeScaleSliderValueText_33; }
	inline void set_timeScaleSliderValueText_33(Text_t356221433 * value)
	{
		___timeScaleSliderValueText_33 = value;
		Il2CppCodeGenWriteBarrier((&___timeScaleSliderValueText_33), value);
	}

	inline static int32_t get_offset_of_mainCamera_34() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___mainCamera_34)); }
	inline Camera_t189460977 * get_mainCamera_34() const { return ___mainCamera_34; }
	inline Camera_t189460977 ** get_address_of_mainCamera_34() { return &___mainCamera_34; }
	inline void set_mainCamera_34(Camera_t189460977 * value)
	{
		___mainCamera_34 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_34), value);
	}

	inline static int32_t get_offset_of_mainCameraPostEffects_35() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___mainCameraPostEffects_35)); }
	inline MonoBehaviourU5BU5D_t3035069757* get_mainCameraPostEffects_35() const { return ___mainCameraPostEffects_35; }
	inline MonoBehaviourU5BU5D_t3035069757** get_address_of_mainCameraPostEffects_35() { return &___mainCameraPostEffects_35; }
	inline void set_mainCameraPostEffects_35(MonoBehaviourU5BU5D_t3035069757* value)
	{
		___mainCameraPostEffects_35 = value;
		Il2CppCodeGenWriteBarrier((&___mainCameraPostEffects_35), value);
	}

	inline static int32_t get_offset_of_cameraPositionSmoothDampVelocity_36() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraPositionSmoothDampVelocity_36)); }
	inline Vector3_t2243707580  get_cameraPositionSmoothDampVelocity_36() const { return ___cameraPositionSmoothDampVelocity_36; }
	inline Vector3_t2243707580 * get_address_of_cameraPositionSmoothDampVelocity_36() { return &___cameraPositionSmoothDampVelocity_36; }
	inline void set_cameraPositionSmoothDampVelocity_36(Vector3_t2243707580  value)
	{
		___cameraPositionSmoothDampVelocity_36 = value;
	}

	inline static int32_t get_offset_of_cameraRotationSmoothDampVelocity_37() { return static_cast<int32_t>(offsetof(DemoManager_t3005492872, ___cameraRotationSmoothDampVelocity_37)); }
	inline Vector3_t2243707580  get_cameraRotationSmoothDampVelocity_37() const { return ___cameraRotationSmoothDampVelocity_37; }
	inline Vector3_t2243707580 * get_address_of_cameraRotationSmoothDampVelocity_37() { return &___cameraRotationSmoothDampVelocity_37; }
	inline void set_cameraRotationSmoothDampVelocity_37(Vector3_t2243707580  value)
	{
		___cameraRotationSmoothDampVelocity_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOMANAGER_T3005492872_H
#ifndef BILLBOARDCAMERAPLANEUVFX_T2785089741_H
#define BILLBOARDCAMERAPLANEUVFX_T2785089741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Demos.ParticlePlayground.BillboardCameraPlaneUVFX
struct  BillboardCameraPlaneUVFX_t2785089741  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MirzaBeig.Demos.ParticlePlayground.BillboardCameraPlaneUVFX::cameraTransform
	Transform_t3275118058 * ___cameraTransform_2;

public:
	inline static int32_t get_offset_of_cameraTransform_2() { return static_cast<int32_t>(offsetof(BillboardCameraPlaneUVFX_t2785089741, ___cameraTransform_2)); }
	inline Transform_t3275118058 * get_cameraTransform_2() const { return ___cameraTransform_2; }
	inline Transform_t3275118058 ** get_address_of_cameraTransform_2() { return &___cameraTransform_2; }
	inline void set_cameraTransform_2(Transform_t3275118058 * value)
	{
		___cameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARDCAMERAPLANEUVFX_T2785089741_H
#ifndef VRCURSOR_T4007861940_H
#define VRCURSOR_T4007861940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.VRCursor
struct  VRCursor_t4007861940  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityEngine.UI.Extensions.VRCursor::xSens
	float ___xSens_2;
	// System.Single UnityEngine.UI.Extensions.VRCursor::ySens
	float ___ySens_3;
	// UnityEngine.Collider UnityEngine.UI.Extensions.VRCursor::currentCollider
	Collider_t3497673348 * ___currentCollider_4;

public:
	inline static int32_t get_offset_of_xSens_2() { return static_cast<int32_t>(offsetof(VRCursor_t4007861940, ___xSens_2)); }
	inline float get_xSens_2() const { return ___xSens_2; }
	inline float* get_address_of_xSens_2() { return &___xSens_2; }
	inline void set_xSens_2(float value)
	{
		___xSens_2 = value;
	}

	inline static int32_t get_offset_of_ySens_3() { return static_cast<int32_t>(offsetof(VRCursor_t4007861940, ___ySens_3)); }
	inline float get_ySens_3() const { return ___ySens_3; }
	inline float* get_address_of_ySens_3() { return &___ySens_3; }
	inline void set_ySens_3(float value)
	{
		___ySens_3 = value;
	}

	inline static int32_t get_offset_of_currentCollider_4() { return static_cast<int32_t>(offsetof(VRCursor_t4007861940, ___currentCollider_4)); }
	inline Collider_t3497673348 * get_currentCollider_4() const { return ___currentCollider_4; }
	inline Collider_t3497673348 ** get_address_of_currentCollider_4() { return &___currentCollider_4; }
	inline void set_currentCollider_4(Collider_t3497673348 * value)
	{
		___currentCollider_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentCollider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRCURSOR_T4007861940_H
#ifndef UISCROLLTOSELECTION_T1032879924_H
#define UISCROLLTOSELECTION_T1032879924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIScrollToSelection
struct  UIScrollToSelection_t1032879924  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.UIScrollToSelection/ScrollType UnityEngine.UI.Extensions.UIScrollToSelection::scrollDirection
	int32_t ___scrollDirection_2;
	// System.Single UnityEngine.UI.Extensions.UIScrollToSelection::scrollSpeed
	float ___scrollSpeed_3;
	// System.Boolean UnityEngine.UI.Extensions.UIScrollToSelection::cancelScrollOnInput
	bool ___cancelScrollOnInput_4;
	// System.Collections.Generic.List`1<UnityEngine.KeyCode> UnityEngine.UI.Extensions.UIScrollToSelection::cancelScrollKeycodes
	List_1_t1652516284 * ___cancelScrollKeycodes_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIScrollToSelection::<ScrollWindow>k__BackingField
	RectTransform_t3349966182 * ___U3CScrollWindowU3Ek__BackingField_6;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.UIScrollToSelection::<TargetScrollRect>k__BackingField
	ScrollRect_t1199013257 * ___U3CTargetScrollRectU3Ek__BackingField_7;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.UIScrollToSelection::<LastCheckedGameObject>k__BackingField
	GameObject_t1756533147 * ___U3CLastCheckedGameObjectU3Ek__BackingField_8;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIScrollToSelection::<CurrentTargetRectTransform>k__BackingField
	RectTransform_t3349966182 * ___U3CCurrentTargetRectTransformU3Ek__BackingField_9;
	// System.Boolean UnityEngine.UI.Extensions.UIScrollToSelection::<IsManualScrollingAvailable>k__BackingField
	bool ___U3CIsManualScrollingAvailableU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_scrollDirection_2() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___scrollDirection_2)); }
	inline int32_t get_scrollDirection_2() const { return ___scrollDirection_2; }
	inline int32_t* get_address_of_scrollDirection_2() { return &___scrollDirection_2; }
	inline void set_scrollDirection_2(int32_t value)
	{
		___scrollDirection_2 = value;
	}

	inline static int32_t get_offset_of_scrollSpeed_3() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___scrollSpeed_3)); }
	inline float get_scrollSpeed_3() const { return ___scrollSpeed_3; }
	inline float* get_address_of_scrollSpeed_3() { return &___scrollSpeed_3; }
	inline void set_scrollSpeed_3(float value)
	{
		___scrollSpeed_3 = value;
	}

	inline static int32_t get_offset_of_cancelScrollOnInput_4() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___cancelScrollOnInput_4)); }
	inline bool get_cancelScrollOnInput_4() const { return ___cancelScrollOnInput_4; }
	inline bool* get_address_of_cancelScrollOnInput_4() { return &___cancelScrollOnInput_4; }
	inline void set_cancelScrollOnInput_4(bool value)
	{
		___cancelScrollOnInput_4 = value;
	}

	inline static int32_t get_offset_of_cancelScrollKeycodes_5() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___cancelScrollKeycodes_5)); }
	inline List_1_t1652516284 * get_cancelScrollKeycodes_5() const { return ___cancelScrollKeycodes_5; }
	inline List_1_t1652516284 ** get_address_of_cancelScrollKeycodes_5() { return &___cancelScrollKeycodes_5; }
	inline void set_cancelScrollKeycodes_5(List_1_t1652516284 * value)
	{
		___cancelScrollKeycodes_5 = value;
		Il2CppCodeGenWriteBarrier((&___cancelScrollKeycodes_5), value);
	}

	inline static int32_t get_offset_of_U3CScrollWindowU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___U3CScrollWindowU3Ek__BackingField_6)); }
	inline RectTransform_t3349966182 * get_U3CScrollWindowU3Ek__BackingField_6() const { return ___U3CScrollWindowU3Ek__BackingField_6; }
	inline RectTransform_t3349966182 ** get_address_of_U3CScrollWindowU3Ek__BackingField_6() { return &___U3CScrollWindowU3Ek__BackingField_6; }
	inline void set_U3CScrollWindowU3Ek__BackingField_6(RectTransform_t3349966182 * value)
	{
		___U3CScrollWindowU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScrollWindowU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTargetScrollRectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___U3CTargetScrollRectU3Ek__BackingField_7)); }
	inline ScrollRect_t1199013257 * get_U3CTargetScrollRectU3Ek__BackingField_7() const { return ___U3CTargetScrollRectU3Ek__BackingField_7; }
	inline ScrollRect_t1199013257 ** get_address_of_U3CTargetScrollRectU3Ek__BackingField_7() { return &___U3CTargetScrollRectU3Ek__BackingField_7; }
	inline void set_U3CTargetScrollRectU3Ek__BackingField_7(ScrollRect_t1199013257 * value)
	{
		___U3CTargetScrollRectU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetScrollRectU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CLastCheckedGameObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___U3CLastCheckedGameObjectU3Ek__BackingField_8)); }
	inline GameObject_t1756533147 * get_U3CLastCheckedGameObjectU3Ek__BackingField_8() const { return ___U3CLastCheckedGameObjectU3Ek__BackingField_8; }
	inline GameObject_t1756533147 ** get_address_of_U3CLastCheckedGameObjectU3Ek__BackingField_8() { return &___U3CLastCheckedGameObjectU3Ek__BackingField_8; }
	inline void set_U3CLastCheckedGameObjectU3Ek__BackingField_8(GameObject_t1756533147 * value)
	{
		___U3CLastCheckedGameObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastCheckedGameObjectU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CCurrentTargetRectTransformU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___U3CCurrentTargetRectTransformU3Ek__BackingField_9)); }
	inline RectTransform_t3349966182 * get_U3CCurrentTargetRectTransformU3Ek__BackingField_9() const { return ___U3CCurrentTargetRectTransformU3Ek__BackingField_9; }
	inline RectTransform_t3349966182 ** get_address_of_U3CCurrentTargetRectTransformU3Ek__BackingField_9() { return &___U3CCurrentTargetRectTransformU3Ek__BackingField_9; }
	inline void set_U3CCurrentTargetRectTransformU3Ek__BackingField_9(RectTransform_t3349966182 * value)
	{
		___U3CCurrentTargetRectTransformU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentTargetRectTransformU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIsManualScrollingAvailableU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UIScrollToSelection_t1032879924, ___U3CIsManualScrollingAvailableU3Ek__BackingField_10)); }
	inline bool get_U3CIsManualScrollingAvailableU3Ek__BackingField_10() const { return ___U3CIsManualScrollingAvailableU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsManualScrollingAvailableU3Ek__BackingField_10() { return &___U3CIsManualScrollingAvailableU3Ek__BackingField_10; }
	inline void set_U3CIsManualScrollingAvailableU3Ek__BackingField_10(bool value)
	{
		___U3CIsManualScrollingAvailableU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLTOSELECTION_T1032879924_H
#ifndef FPSDISPLAY_T1753842521_H
#define FPSDISPLAY_T1753842521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.FPSDisplay
struct  FPSDisplay_t1753842521  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.ParticleSystems.Demos.FPSDisplay::timer
	float ___timer_2;
	// System.Single MirzaBeig.ParticleSystems.Demos.FPSDisplay::updateTime
	float ___updateTime_3;
	// System.Int32 MirzaBeig.ParticleSystems.Demos.FPSDisplay::frameCount
	int32_t ___frameCount_4;
	// System.Single MirzaBeig.ParticleSystems.Demos.FPSDisplay::fpsAccum
	float ___fpsAccum_5;
	// UnityEngine.UI.Text MirzaBeig.ParticleSystems.Demos.FPSDisplay::fpsText
	Text_t356221433 * ___fpsText_6;

public:
	inline static int32_t get_offset_of_timer_2() { return static_cast<int32_t>(offsetof(FPSDisplay_t1753842521, ___timer_2)); }
	inline float get_timer_2() const { return ___timer_2; }
	inline float* get_address_of_timer_2() { return &___timer_2; }
	inline void set_timer_2(float value)
	{
		___timer_2 = value;
	}

	inline static int32_t get_offset_of_updateTime_3() { return static_cast<int32_t>(offsetof(FPSDisplay_t1753842521, ___updateTime_3)); }
	inline float get_updateTime_3() const { return ___updateTime_3; }
	inline float* get_address_of_updateTime_3() { return &___updateTime_3; }
	inline void set_updateTime_3(float value)
	{
		___updateTime_3 = value;
	}

	inline static int32_t get_offset_of_frameCount_4() { return static_cast<int32_t>(offsetof(FPSDisplay_t1753842521, ___frameCount_4)); }
	inline int32_t get_frameCount_4() const { return ___frameCount_4; }
	inline int32_t* get_address_of_frameCount_4() { return &___frameCount_4; }
	inline void set_frameCount_4(int32_t value)
	{
		___frameCount_4 = value;
	}

	inline static int32_t get_offset_of_fpsAccum_5() { return static_cast<int32_t>(offsetof(FPSDisplay_t1753842521, ___fpsAccum_5)); }
	inline float get_fpsAccum_5() const { return ___fpsAccum_5; }
	inline float* get_address_of_fpsAccum_5() { return &___fpsAccum_5; }
	inline void set_fpsAccum_5(float value)
	{
		___fpsAccum_5 = value;
	}

	inline static int32_t get_offset_of_fpsText_6() { return static_cast<int32_t>(offsetof(FPSDisplay_t1753842521, ___fpsText_6)); }
	inline Text_t356221433 * get_fpsText_6() const { return ___fpsText_6; }
	inline Text_t356221433 ** get_address_of_fpsText_6() { return &___fpsText_6; }
	inline void set_fpsText_6(Text_t356221433 * value)
	{
		___fpsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___fpsText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSDISPLAY_T1753842521_H
#ifndef FPSTEST_T3154395497_H
#define FPSTEST_T3154395497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.FPSTest
struct  FPSTest_t3154395497  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 MirzaBeig.ParticleSystems.Demos.FPSTest::targetFPS1
	int32_t ___targetFPS1_2;
	// System.Int32 MirzaBeig.ParticleSystems.Demos.FPSTest::targetFPS2
	int32_t ___targetFPS2_3;
	// System.Int32 MirzaBeig.ParticleSystems.Demos.FPSTest::previousVSyncCount
	int32_t ___previousVSyncCount_4;

public:
	inline static int32_t get_offset_of_targetFPS1_2() { return static_cast<int32_t>(offsetof(FPSTest_t3154395497, ___targetFPS1_2)); }
	inline int32_t get_targetFPS1_2() const { return ___targetFPS1_2; }
	inline int32_t* get_address_of_targetFPS1_2() { return &___targetFPS1_2; }
	inline void set_targetFPS1_2(int32_t value)
	{
		___targetFPS1_2 = value;
	}

	inline static int32_t get_offset_of_targetFPS2_3() { return static_cast<int32_t>(offsetof(FPSTest_t3154395497, ___targetFPS2_3)); }
	inline int32_t get_targetFPS2_3() const { return ___targetFPS2_3; }
	inline int32_t* get_address_of_targetFPS2_3() { return &___targetFPS2_3; }
	inline void set_targetFPS2_3(int32_t value)
	{
		___targetFPS2_3 = value;
	}

	inline static int32_t get_offset_of_previousVSyncCount_4() { return static_cast<int32_t>(offsetof(FPSTest_t3154395497, ___previousVSyncCount_4)); }
	inline int32_t get_previousVSyncCount_4() const { return ___previousVSyncCount_4; }
	inline int32_t* get_address_of_previousVSyncCount_4() { return &___previousVSyncCount_4; }
	inline void set_previousVSyncCount_4(int32_t value)
	{
		___previousVSyncCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSTEST_T3154395497_H
#ifndef UISCROLLTOSELECTIONXY_T687907589_H
#define UISCROLLTOSELECTIONXY_T687907589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIScrollToSelectionXY
struct  UIScrollToSelectionXY_t687907589  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityEngine.UI.Extensions.UIScrollToSelectionXY::scrollSpeed
	float ___scrollSpeed_2;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIScrollToSelectionXY::layoutListGroup
	RectTransform_t3349966182 * ___layoutListGroup_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIScrollToSelectionXY::targetScrollObject
	RectTransform_t3349966182 * ___targetScrollObject_4;
	// System.Boolean UnityEngine.UI.Extensions.UIScrollToSelectionXY::scrollToSelection
	bool ___scrollToSelection_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIScrollToSelectionXY::scrollWindow
	RectTransform_t3349966182 * ___scrollWindow_6;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.UIScrollToSelectionXY::currentCanvas
	RectTransform_t3349966182 * ___currentCanvas_7;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.UIScrollToSelectionXY::targetScrollRect
	ScrollRect_t1199013257 * ___targetScrollRect_8;

public:
	inline static int32_t get_offset_of_scrollSpeed_2() { return static_cast<int32_t>(offsetof(UIScrollToSelectionXY_t687907589, ___scrollSpeed_2)); }
	inline float get_scrollSpeed_2() const { return ___scrollSpeed_2; }
	inline float* get_address_of_scrollSpeed_2() { return &___scrollSpeed_2; }
	inline void set_scrollSpeed_2(float value)
	{
		___scrollSpeed_2 = value;
	}

	inline static int32_t get_offset_of_layoutListGroup_3() { return static_cast<int32_t>(offsetof(UIScrollToSelectionXY_t687907589, ___layoutListGroup_3)); }
	inline RectTransform_t3349966182 * get_layoutListGroup_3() const { return ___layoutListGroup_3; }
	inline RectTransform_t3349966182 ** get_address_of_layoutListGroup_3() { return &___layoutListGroup_3; }
	inline void set_layoutListGroup_3(RectTransform_t3349966182 * value)
	{
		___layoutListGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&___layoutListGroup_3), value);
	}

	inline static int32_t get_offset_of_targetScrollObject_4() { return static_cast<int32_t>(offsetof(UIScrollToSelectionXY_t687907589, ___targetScrollObject_4)); }
	inline RectTransform_t3349966182 * get_targetScrollObject_4() const { return ___targetScrollObject_4; }
	inline RectTransform_t3349966182 ** get_address_of_targetScrollObject_4() { return &___targetScrollObject_4; }
	inline void set_targetScrollObject_4(RectTransform_t3349966182 * value)
	{
		___targetScrollObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetScrollObject_4), value);
	}

	inline static int32_t get_offset_of_scrollToSelection_5() { return static_cast<int32_t>(offsetof(UIScrollToSelectionXY_t687907589, ___scrollToSelection_5)); }
	inline bool get_scrollToSelection_5() const { return ___scrollToSelection_5; }
	inline bool* get_address_of_scrollToSelection_5() { return &___scrollToSelection_5; }
	inline void set_scrollToSelection_5(bool value)
	{
		___scrollToSelection_5 = value;
	}

	inline static int32_t get_offset_of_scrollWindow_6() { return static_cast<int32_t>(offsetof(UIScrollToSelectionXY_t687907589, ___scrollWindow_6)); }
	inline RectTransform_t3349966182 * get_scrollWindow_6() const { return ___scrollWindow_6; }
	inline RectTransform_t3349966182 ** get_address_of_scrollWindow_6() { return &___scrollWindow_6; }
	inline void set_scrollWindow_6(RectTransform_t3349966182 * value)
	{
		___scrollWindow_6 = value;
		Il2CppCodeGenWriteBarrier((&___scrollWindow_6), value);
	}

	inline static int32_t get_offset_of_currentCanvas_7() { return static_cast<int32_t>(offsetof(UIScrollToSelectionXY_t687907589, ___currentCanvas_7)); }
	inline RectTransform_t3349966182 * get_currentCanvas_7() const { return ___currentCanvas_7; }
	inline RectTransform_t3349966182 ** get_address_of_currentCanvas_7() { return &___currentCanvas_7; }
	inline void set_currentCanvas_7(RectTransform_t3349966182 * value)
	{
		___currentCanvas_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentCanvas_7), value);
	}

	inline static int32_t get_offset_of_targetScrollRect_8() { return static_cast<int32_t>(offsetof(UIScrollToSelectionXY_t687907589, ___targetScrollRect_8)); }
	inline ScrollRect_t1199013257 * get_targetScrollRect_8() const { return ___targetScrollRect_8; }
	inline ScrollRect_t1199013257 ** get_address_of_targetScrollRect_8() { return &___targetScrollRect_8; }
	inline void set_targetScrollRect_8(ScrollRect_t1199013257 * value)
	{
		___targetScrollRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetScrollRect_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLTOSELECTIONXY_T687907589_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3793803729_H
#define TIMEDOBJECTDESTRUCTOR_T3793803729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimedObjectDestructor
struct  TimedObjectDestructor_t3793803729  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TimedObjectDestructor::timeOut
	float ___timeOut_2;
	// System.Boolean TimedObjectDestructor::detachChildren
	bool ___detachChildren_3;

public:
	inline static int32_t get_offset_of_timeOut_2() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3793803729, ___timeOut_2)); }
	inline float get_timeOut_2() const { return ___timeOut_2; }
	inline float* get_address_of_timeOut_2() { return &___timeOut_2; }
	inline void set_timeOut_2(float value)
	{
		___timeOut_2 = value;
	}

	inline static int32_t get_offset_of_detachChildren_3() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3793803729, ___detachChildren_3)); }
	inline bool get_detachChildren_3() const { return ___detachChildren_3; }
	inline bool* get_address_of_detachChildren_3() { return &___detachChildren_3; }
	inline void set_detachChildren_3(bool value)
	{
		___detachChildren_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3793803729_H
#ifndef UISELECTABLEEXTENSION_T1749765265_H
#define UISELECTABLEEXTENSION_T1749765265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UISelectableExtension
struct  UISelectableExtension_t1749765265  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Extensions.UISelectableExtension/UIButtonEvent UnityEngine.UI.Extensions.UISelectableExtension::OnButtonPress
	UIButtonEvent_t1812119617 * ___OnButtonPress_2;
	// UnityEngine.UI.Extensions.UISelectableExtension/UIButtonEvent UnityEngine.UI.Extensions.UISelectableExtension::OnButtonRelease
	UIButtonEvent_t1812119617 * ___OnButtonRelease_3;
	// UnityEngine.UI.Extensions.UISelectableExtension/UIButtonEvent UnityEngine.UI.Extensions.UISelectableExtension::OnButtonHeld
	UIButtonEvent_t1812119617 * ___OnButtonHeld_4;
	// System.Boolean UnityEngine.UI.Extensions.UISelectableExtension::_pressed
	bool ____pressed_5;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.UISelectableExtension::_heldEventData
	PointerEventData_t1599784723 * ____heldEventData_6;

public:
	inline static int32_t get_offset_of_OnButtonPress_2() { return static_cast<int32_t>(offsetof(UISelectableExtension_t1749765265, ___OnButtonPress_2)); }
	inline UIButtonEvent_t1812119617 * get_OnButtonPress_2() const { return ___OnButtonPress_2; }
	inline UIButtonEvent_t1812119617 ** get_address_of_OnButtonPress_2() { return &___OnButtonPress_2; }
	inline void set_OnButtonPress_2(UIButtonEvent_t1812119617 * value)
	{
		___OnButtonPress_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonPress_2), value);
	}

	inline static int32_t get_offset_of_OnButtonRelease_3() { return static_cast<int32_t>(offsetof(UISelectableExtension_t1749765265, ___OnButtonRelease_3)); }
	inline UIButtonEvent_t1812119617 * get_OnButtonRelease_3() const { return ___OnButtonRelease_3; }
	inline UIButtonEvent_t1812119617 ** get_address_of_OnButtonRelease_3() { return &___OnButtonRelease_3; }
	inline void set_OnButtonRelease_3(UIButtonEvent_t1812119617 * value)
	{
		___OnButtonRelease_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonRelease_3), value);
	}

	inline static int32_t get_offset_of_OnButtonHeld_4() { return static_cast<int32_t>(offsetof(UISelectableExtension_t1749765265, ___OnButtonHeld_4)); }
	inline UIButtonEvent_t1812119617 * get_OnButtonHeld_4() const { return ___OnButtonHeld_4; }
	inline UIButtonEvent_t1812119617 ** get_address_of_OnButtonHeld_4() { return &___OnButtonHeld_4; }
	inline void set_OnButtonHeld_4(UIButtonEvent_t1812119617 * value)
	{
		___OnButtonHeld_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonHeld_4), value);
	}

	inline static int32_t get_offset_of__pressed_5() { return static_cast<int32_t>(offsetof(UISelectableExtension_t1749765265, ____pressed_5)); }
	inline bool get__pressed_5() const { return ____pressed_5; }
	inline bool* get_address_of__pressed_5() { return &____pressed_5; }
	inline void set__pressed_5(bool value)
	{
		____pressed_5 = value;
	}

	inline static int32_t get_offset_of__heldEventData_6() { return static_cast<int32_t>(offsetof(UISelectableExtension_t1749765265, ____heldEventData_6)); }
	inline PointerEventData_t1599784723 * get__heldEventData_6() const { return ____heldEventData_6; }
	inline PointerEventData_t1599784723 ** get_address_of__heldEventData_6() { return &____heldEventData_6; }
	inline void set__heldEventData_6(PointerEventData_t1599784723 * value)
	{
		____heldEventData_6 = value;
		Il2CppCodeGenWriteBarrier((&____heldEventData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISELECTABLEEXTENSION_T1749765265_H
#ifndef MOUSEFOLLOW_T3233463164_H
#define MOUSEFOLLOW_T3233463164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.MouseFollow
struct  MouseFollow_t3233463164  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.ParticleSystems.Demos.MouseFollow::speed
	float ___speed_2;
	// System.Single MirzaBeig.ParticleSystems.Demos.MouseFollow::distanceFromCamera
	float ___distanceFromCamera_3;
	// System.Boolean MirzaBeig.ParticleSystems.Demos.MouseFollow::ignoreTimeScale
	bool ___ignoreTimeScale_4;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(MouseFollow_t3233463164, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_distanceFromCamera_3() { return static_cast<int32_t>(offsetof(MouseFollow_t3233463164, ___distanceFromCamera_3)); }
	inline float get_distanceFromCamera_3() const { return ___distanceFromCamera_3; }
	inline float* get_address_of_distanceFromCamera_3() { return &___distanceFromCamera_3; }
	inline void set_distanceFromCamera_3(float value)
	{
		___distanceFromCamera_3 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_4() { return static_cast<int32_t>(offsetof(MouseFollow_t3233463164, ___ignoreTimeScale_4)); }
	inline bool get_ignoreTimeScale_4() const { return ___ignoreTimeScale_4; }
	inline bool* get_address_of_ignoreTimeScale_4() { return &___ignoreTimeScale_4; }
	inline void set_ignoreTimeScale_4(bool value)
	{
		___ignoreTimeScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEFOLLOW_T3233463164_H
#ifndef TRANSFORMNOISE_T2551917980_H
#define TRANSFORMNOISE_T2551917980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.TransformNoise
struct  TransformNoise_t2551917980  : public MonoBehaviour_t1158329972
{
public:
	// MirzaBeig.ParticleSystems.PerlinNoiseXYZ MirzaBeig.ParticleSystems.TransformNoise::positionNoise
	PerlinNoiseXYZ_t2920671427 * ___positionNoise_2;
	// MirzaBeig.ParticleSystems.PerlinNoiseXYZ MirzaBeig.ParticleSystems.TransformNoise::rotationNoise
	PerlinNoiseXYZ_t2920671427 * ___rotationNoise_3;
	// System.Boolean MirzaBeig.ParticleSystems.TransformNoise::unscaledTime
	bool ___unscaledTime_4;
	// System.Single MirzaBeig.ParticleSystems.TransformNoise::time
	float ___time_5;

public:
	inline static int32_t get_offset_of_positionNoise_2() { return static_cast<int32_t>(offsetof(TransformNoise_t2551917980, ___positionNoise_2)); }
	inline PerlinNoiseXYZ_t2920671427 * get_positionNoise_2() const { return ___positionNoise_2; }
	inline PerlinNoiseXYZ_t2920671427 ** get_address_of_positionNoise_2() { return &___positionNoise_2; }
	inline void set_positionNoise_2(PerlinNoiseXYZ_t2920671427 * value)
	{
		___positionNoise_2 = value;
		Il2CppCodeGenWriteBarrier((&___positionNoise_2), value);
	}

	inline static int32_t get_offset_of_rotationNoise_3() { return static_cast<int32_t>(offsetof(TransformNoise_t2551917980, ___rotationNoise_3)); }
	inline PerlinNoiseXYZ_t2920671427 * get_rotationNoise_3() const { return ___rotationNoise_3; }
	inline PerlinNoiseXYZ_t2920671427 ** get_address_of_rotationNoise_3() { return &___rotationNoise_3; }
	inline void set_rotationNoise_3(PerlinNoiseXYZ_t2920671427 * value)
	{
		___rotationNoise_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotationNoise_3), value);
	}

	inline static int32_t get_offset_of_unscaledTime_4() { return static_cast<int32_t>(offsetof(TransformNoise_t2551917980, ___unscaledTime_4)); }
	inline bool get_unscaledTime_4() const { return ___unscaledTime_4; }
	inline bool* get_address_of_unscaledTime_4() { return &___unscaledTime_4; }
	inline void set_unscaledTime_4(bool value)
	{
		___unscaledTime_4 = value;
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(TransformNoise_t2551917980, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMNOISE_T2551917980_H
#ifndef TRAILRENDERERS_T2418747850_H
#define TRAILRENDERERS_T2418747850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.TrailRenderers
struct  TrailRenderers_t2418747850  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TrailRenderer[] MirzaBeig.ParticleSystems.TrailRenderers::trailRenderers
	TrailRendererU5BU5D_t182776078* ___trailRenderers_2;

public:
	inline static int32_t get_offset_of_trailRenderers_2() { return static_cast<int32_t>(offsetof(TrailRenderers_t2418747850, ___trailRenderers_2)); }
	inline TrailRendererU5BU5D_t182776078* get_trailRenderers_2() const { return ___trailRenderers_2; }
	inline TrailRendererU5BU5D_t182776078** get_address_of_trailRenderers_2() { return &___trailRenderers_2; }
	inline void set_trailRenderers_2(TrailRendererU5BU5D_t182776078* value)
	{
		___trailRenderers_2 = value;
		Il2CppCodeGenWriteBarrier((&___trailRenderers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAILRENDERERS_T2418747850_H
#ifndef DEMOMANAGER_XPTITLES_T2047362782_H
#define DEMOMANAGER_XPTITLES_T2047362782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.DemoManager_XPTitles
struct  DemoManager_XPTitles_t2047362782  : public MonoBehaviour_t1158329972
{
public:
	// MirzaBeig.ParticleSystems.Demos.LoopingParticleSystemsManager MirzaBeig.ParticleSystems.Demos.DemoManager_XPTitles::list
	LoopingParticleSystemsManager_t2507471847 * ___list_2;
	// UnityEngine.UI.Text MirzaBeig.ParticleSystems.Demos.DemoManager_XPTitles::particleCountText
	Text_t356221433 * ___particleCountText_3;
	// UnityEngine.UI.Text MirzaBeig.ParticleSystems.Demos.DemoManager_XPTitles::currentParticleSystemText
	Text_t356221433 * ___currentParticleSystemText_4;
	// MirzaBeig.ParticleSystems.Rotator MirzaBeig.ParticleSystems.Demos.DemoManager_XPTitles::cameraRotator
	Rotator_t1284983691 * ___cameraRotator_5;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(DemoManager_XPTitles_t2047362782, ___list_2)); }
	inline LoopingParticleSystemsManager_t2507471847 * get_list_2() const { return ___list_2; }
	inline LoopingParticleSystemsManager_t2507471847 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(LoopingParticleSystemsManager_t2507471847 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of_particleCountText_3() { return static_cast<int32_t>(offsetof(DemoManager_XPTitles_t2047362782, ___particleCountText_3)); }
	inline Text_t356221433 * get_particleCountText_3() const { return ___particleCountText_3; }
	inline Text_t356221433 ** get_address_of_particleCountText_3() { return &___particleCountText_3; }
	inline void set_particleCountText_3(Text_t356221433 * value)
	{
		___particleCountText_3 = value;
		Il2CppCodeGenWriteBarrier((&___particleCountText_3), value);
	}

	inline static int32_t get_offset_of_currentParticleSystemText_4() { return static_cast<int32_t>(offsetof(DemoManager_XPTitles_t2047362782, ___currentParticleSystemText_4)); }
	inline Text_t356221433 * get_currentParticleSystemText_4() const { return ___currentParticleSystemText_4; }
	inline Text_t356221433 ** get_address_of_currentParticleSystemText_4() { return &___currentParticleSystemText_4; }
	inline void set_currentParticleSystemText_4(Text_t356221433 * value)
	{
		___currentParticleSystemText_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentParticleSystemText_4), value);
	}

	inline static int32_t get_offset_of_cameraRotator_5() { return static_cast<int32_t>(offsetof(DemoManager_XPTitles_t2047362782, ___cameraRotator_5)); }
	inline Rotator_t1284983691 * get_cameraRotator_5() const { return ___cameraRotator_5; }
	inline Rotator_t1284983691 ** get_address_of_cameraRotator_5() { return &___cameraRotator_5; }
	inline void set_cameraRotator_5(Rotator_t1284983691 * value)
	{
		___cameraRotator_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraRotator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOMANAGER_XPTITLES_T2047362782_H
#ifndef PARTICLEAFFECTORMT_T685825231_H
#define PARTICLEAFFECTORMT_T685825231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleAffectorMT
struct  ParticleAffectorMT_t685825231  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::force
	float ___force_2;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::speed
	float ___speed_3;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticleAffectorMT::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_4;
	// UnityEngine.ParticleSystem/Particle[] MirzaBeig.Scripting.Effects.ParticleAffectorMT::particles
	ParticleU5BU5D_t574222242* ___particles_5;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::randomX
	float ___randomX_6;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::randomY
	float ___randomY_7;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::randomZ
	float ___randomZ_8;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::offsetX
	float ___offsetX_9;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::offsetY
	float ___offsetY_10;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::offsetZ
	float ___offsetZ_11;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffectorMT::deltaTime
	float ___deltaTime_12;
	// System.Threading.Thread MirzaBeig.Scripting.Effects.ParticleAffectorMT::t
	Thread_t241561612 * ___t_13;
	// System.Object MirzaBeig.Scripting.Effects.ParticleAffectorMT::locker
	RuntimeObject * ___locker_14;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleAffectorMT::processing
	bool ___processing_15;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleAffectorMT::isDoneAssigning
	bool ___isDoneAssigning_16;

public:
	inline static int32_t get_offset_of_force_2() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___force_2)); }
	inline float get_force_2() const { return ___force_2; }
	inline float* get_address_of_force_2() { return &___force_2; }
	inline void set_force_2(float value)
	{
		___force_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_particleSystem_4() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___particleSystem_4)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_4() const { return ___particleSystem_4; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_4() { return &___particleSystem_4; }
	inline void set_particleSystem_4(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_4), value);
	}

	inline static int32_t get_offset_of_particles_5() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___particles_5)); }
	inline ParticleU5BU5D_t574222242* get_particles_5() const { return ___particles_5; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_5() { return &___particles_5; }
	inline void set_particles_5(ParticleU5BU5D_t574222242* value)
	{
		___particles_5 = value;
		Il2CppCodeGenWriteBarrier((&___particles_5), value);
	}

	inline static int32_t get_offset_of_randomX_6() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___randomX_6)); }
	inline float get_randomX_6() const { return ___randomX_6; }
	inline float* get_address_of_randomX_6() { return &___randomX_6; }
	inline void set_randomX_6(float value)
	{
		___randomX_6 = value;
	}

	inline static int32_t get_offset_of_randomY_7() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___randomY_7)); }
	inline float get_randomY_7() const { return ___randomY_7; }
	inline float* get_address_of_randomY_7() { return &___randomY_7; }
	inline void set_randomY_7(float value)
	{
		___randomY_7 = value;
	}

	inline static int32_t get_offset_of_randomZ_8() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___randomZ_8)); }
	inline float get_randomZ_8() const { return ___randomZ_8; }
	inline float* get_address_of_randomZ_8() { return &___randomZ_8; }
	inline void set_randomZ_8(float value)
	{
		___randomZ_8 = value;
	}

	inline static int32_t get_offset_of_offsetX_9() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___offsetX_9)); }
	inline float get_offsetX_9() const { return ___offsetX_9; }
	inline float* get_address_of_offsetX_9() { return &___offsetX_9; }
	inline void set_offsetX_9(float value)
	{
		___offsetX_9 = value;
	}

	inline static int32_t get_offset_of_offsetY_10() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___offsetY_10)); }
	inline float get_offsetY_10() const { return ___offsetY_10; }
	inline float* get_address_of_offsetY_10() { return &___offsetY_10; }
	inline void set_offsetY_10(float value)
	{
		___offsetY_10 = value;
	}

	inline static int32_t get_offset_of_offsetZ_11() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___offsetZ_11)); }
	inline float get_offsetZ_11() const { return ___offsetZ_11; }
	inline float* get_address_of_offsetZ_11() { return &___offsetZ_11; }
	inline void set_offsetZ_11(float value)
	{
		___offsetZ_11 = value;
	}

	inline static int32_t get_offset_of_deltaTime_12() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___deltaTime_12)); }
	inline float get_deltaTime_12() const { return ___deltaTime_12; }
	inline float* get_address_of_deltaTime_12() { return &___deltaTime_12; }
	inline void set_deltaTime_12(float value)
	{
		___deltaTime_12 = value;
	}

	inline static int32_t get_offset_of_t_13() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___t_13)); }
	inline Thread_t241561612 * get_t_13() const { return ___t_13; }
	inline Thread_t241561612 ** get_address_of_t_13() { return &___t_13; }
	inline void set_t_13(Thread_t241561612 * value)
	{
		___t_13 = value;
		Il2CppCodeGenWriteBarrier((&___t_13), value);
	}

	inline static int32_t get_offset_of_locker_14() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___locker_14)); }
	inline RuntimeObject * get_locker_14() const { return ___locker_14; }
	inline RuntimeObject ** get_address_of_locker_14() { return &___locker_14; }
	inline void set_locker_14(RuntimeObject * value)
	{
		___locker_14 = value;
		Il2CppCodeGenWriteBarrier((&___locker_14), value);
	}

	inline static int32_t get_offset_of_processing_15() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___processing_15)); }
	inline bool get_processing_15() const { return ___processing_15; }
	inline bool* get_address_of_processing_15() { return &___processing_15; }
	inline void set_processing_15(bool value)
	{
		___processing_15 = value;
	}

	inline static int32_t get_offset_of_isDoneAssigning_16() { return static_cast<int32_t>(offsetof(ParticleAffectorMT_t685825231, ___isDoneAssigning_16)); }
	inline bool get_isDoneAssigning_16() const { return ___isDoneAssigning_16; }
	inline bool* get_address_of_isDoneAssigning_16() { return &___isDoneAssigning_16; }
	inline void set_isDoneAssigning_16(bool value)
	{
		___isDoneAssigning_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEAFFECTORMT_T685825231_H
#ifndef BILLBOARD_T1942171335_H
#define BILLBOARD_T1942171335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Billboard
struct  Billboard_t1942171335  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARD_T1942171335_H
#ifndef ROTATOR_T1284983691_H
#define ROTATOR_T1284983691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Rotator
struct  Rotator_t1284983691  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Rotator::localRotationSpeed
	Vector3_t2243707580  ___localRotationSpeed_2;
	// UnityEngine.Vector3 MirzaBeig.ParticleSystems.Rotator::worldRotationSpeed
	Vector3_t2243707580  ___worldRotationSpeed_3;
	// System.Boolean MirzaBeig.ParticleSystems.Rotator::executeInEditMode
	bool ___executeInEditMode_4;
	// System.Boolean MirzaBeig.ParticleSystems.Rotator::unscaledTime
	bool ___unscaledTime_5;

public:
	inline static int32_t get_offset_of_localRotationSpeed_2() { return static_cast<int32_t>(offsetof(Rotator_t1284983691, ___localRotationSpeed_2)); }
	inline Vector3_t2243707580  get_localRotationSpeed_2() const { return ___localRotationSpeed_2; }
	inline Vector3_t2243707580 * get_address_of_localRotationSpeed_2() { return &___localRotationSpeed_2; }
	inline void set_localRotationSpeed_2(Vector3_t2243707580  value)
	{
		___localRotationSpeed_2 = value;
	}

	inline static int32_t get_offset_of_worldRotationSpeed_3() { return static_cast<int32_t>(offsetof(Rotator_t1284983691, ___worldRotationSpeed_3)); }
	inline Vector3_t2243707580  get_worldRotationSpeed_3() const { return ___worldRotationSpeed_3; }
	inline Vector3_t2243707580 * get_address_of_worldRotationSpeed_3() { return &___worldRotationSpeed_3; }
	inline void set_worldRotationSpeed_3(Vector3_t2243707580  value)
	{
		___worldRotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_executeInEditMode_4() { return static_cast<int32_t>(offsetof(Rotator_t1284983691, ___executeInEditMode_4)); }
	inline bool get_executeInEditMode_4() const { return ___executeInEditMode_4; }
	inline bool* get_address_of_executeInEditMode_4() { return &___executeInEditMode_4; }
	inline void set_executeInEditMode_4(bool value)
	{
		___executeInEditMode_4 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_5() { return static_cast<int32_t>(offsetof(Rotator_t1284983691, ___unscaledTime_5)); }
	inline bool get_unscaledTime_5() const { return ___unscaledTime_5; }
	inline bool* get_address_of_unscaledTime_5() { return &___unscaledTime_5; }
	inline void set_unscaledTime_5(bool value)
	{
		___unscaledTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATOR_T1284983691_H
#ifndef TESTCOMPRESSION_T2304655758_H
#define TESTCOMPRESSION_T2304655758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestCompression
struct  TestCompression_t2304655758  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTCOMPRESSION_T2304655758_H
#ifndef DESTROYAFTERTIME_T3168129069_H
#define DESTROYAFTERTIME_T3168129069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.DestroyAfterTime
struct  DestroyAfterTime_t3168129069  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.ParticleSystems.DestroyAfterTime::time
	float ___time_2;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(DestroyAfterTime_t3168129069, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYAFTERTIME_T3168129069_H
#ifndef IGNORETIMESCALE_T3989264843_H
#define IGNORETIMESCALE_T3989264843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.IgnoreTimeScale
struct  IgnoreTimeScale_t3989264843  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem MirzaBeig.ParticleSystems.IgnoreTimeScale::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_2;

public:
	inline static int32_t get_offset_of_particleSystem_2() { return static_cast<int32_t>(offsetof(IgnoreTimeScale_t3989264843, ___particleSystem_2)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_2() const { return ___particleSystem_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_2() { return &___particleSystem_2; }
	inline void set_particleSystem_2(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_2 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORETIMESCALE_T3989264843_H
#ifndef RENDERERSORTINGORDER_T4127253811_H
#define RENDERERSORTINGORDER_T4127253811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.RendererSortingOrder
struct  RendererSortingOrder_t4127253811  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 MirzaBeig.ParticleSystems.RendererSortingOrder::sortingOrder
	int32_t ___sortingOrder_2;

public:
	inline static int32_t get_offset_of_sortingOrder_2() { return static_cast<int32_t>(offsetof(RendererSortingOrder_t4127253811, ___sortingOrder_2)); }
	inline int32_t get_sortingOrder_2() const { return ___sortingOrder_2; }
	inline int32_t* get_address_of_sortingOrder_2() { return &___sortingOrder_2; }
	inline void set_sortingOrder_2(int32_t value)
	{
		___sortingOrder_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERSORTINGORDER_T4127253811_H
#ifndef PARTICLESYSTEMS_T2274077986_H
#define PARTICLESYSTEMS_T2274077986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.ParticleSystems
struct  ParticleSystems_t2274077986  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem[] MirzaBeig.ParticleSystems.ParticleSystems::<particleSystems>k__BackingField
	ParticleSystemU5BU5D_t1490986844* ___U3CparticleSystemsU3Ek__BackingField_2;
	// MirzaBeig.ParticleSystems.ParticleSystems/onParticleSystemsDeadEventHandler MirzaBeig.ParticleSystems.ParticleSystems::onParticleSystemsDeadEvent
	onParticleSystemsDeadEventHandler_t222977030 * ___onParticleSystemsDeadEvent_3;

public:
	inline static int32_t get_offset_of_U3CparticleSystemsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ParticleSystems_t2274077986, ___U3CparticleSystemsU3Ek__BackingField_2)); }
	inline ParticleSystemU5BU5D_t1490986844* get_U3CparticleSystemsU3Ek__BackingField_2() const { return ___U3CparticleSystemsU3Ek__BackingField_2; }
	inline ParticleSystemU5BU5D_t1490986844** get_address_of_U3CparticleSystemsU3Ek__BackingField_2() { return &___U3CparticleSystemsU3Ek__BackingField_2; }
	inline void set_U3CparticleSystemsU3Ek__BackingField_2(ParticleSystemU5BU5D_t1490986844* value)
	{
		___U3CparticleSystemsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparticleSystemsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_onParticleSystemsDeadEvent_3() { return static_cast<int32_t>(offsetof(ParticleSystems_t2274077986, ___onParticleSystemsDeadEvent_3)); }
	inline onParticleSystemsDeadEventHandler_t222977030 * get_onParticleSystemsDeadEvent_3() const { return ___onParticleSystemsDeadEvent_3; }
	inline onParticleSystemsDeadEventHandler_t222977030 ** get_address_of_onParticleSystemsDeadEvent_3() { return &___onParticleSystemsDeadEvent_3; }
	inline void set_onParticleSystemsDeadEvent_3(onParticleSystemsDeadEventHandler_t222977030 * value)
	{
		___onParticleSystemsDeadEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___onParticleSystemsDeadEvent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMS_T2274077986_H
#ifndef TURBULENCEPARTICLEAFFECTORMT_T1716033708_H
#define TURBULENCEPARTICLEAFFECTORMT_T1716033708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT
struct  TurbulenceParticleAffectorMT_t1716033708  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::force
	float ___force_2;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::speed
	float ___speed_3;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_4;
	// UnityEngine.ParticleSystem/Particle[] MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::particles
	ParticleU5BU5D_t574222242* ___particles_5;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::randomX
	float ___randomX_6;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::randomY
	float ___randomY_7;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::randomZ
	float ___randomZ_8;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::offsetX
	float ___offsetX_9;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::offsetY
	float ___offsetY_10;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::offsetZ
	float ___offsetZ_11;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::deltaTime
	float ___deltaTime_12;
	// System.Threading.Thread MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::t
	Thread_t241561612 * ___t_13;
	// System.Object MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::locker
	RuntimeObject * ___locker_14;
	// System.Boolean MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::processing
	bool ___processing_15;
	// System.Boolean MirzaBeig.Scripting.Effects.TurbulenceParticleAffectorMT::isDoneAssigning
	bool ___isDoneAssigning_16;

public:
	inline static int32_t get_offset_of_force_2() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___force_2)); }
	inline float get_force_2() const { return ___force_2; }
	inline float* get_address_of_force_2() { return &___force_2; }
	inline void set_force_2(float value)
	{
		___force_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_particleSystem_4() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___particleSystem_4)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_4() const { return ___particleSystem_4; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_4() { return &___particleSystem_4; }
	inline void set_particleSystem_4(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_4), value);
	}

	inline static int32_t get_offset_of_particles_5() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___particles_5)); }
	inline ParticleU5BU5D_t574222242* get_particles_5() const { return ___particles_5; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_5() { return &___particles_5; }
	inline void set_particles_5(ParticleU5BU5D_t574222242* value)
	{
		___particles_5 = value;
		Il2CppCodeGenWriteBarrier((&___particles_5), value);
	}

	inline static int32_t get_offset_of_randomX_6() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___randomX_6)); }
	inline float get_randomX_6() const { return ___randomX_6; }
	inline float* get_address_of_randomX_6() { return &___randomX_6; }
	inline void set_randomX_6(float value)
	{
		___randomX_6 = value;
	}

	inline static int32_t get_offset_of_randomY_7() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___randomY_7)); }
	inline float get_randomY_7() const { return ___randomY_7; }
	inline float* get_address_of_randomY_7() { return &___randomY_7; }
	inline void set_randomY_7(float value)
	{
		___randomY_7 = value;
	}

	inline static int32_t get_offset_of_randomZ_8() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___randomZ_8)); }
	inline float get_randomZ_8() const { return ___randomZ_8; }
	inline float* get_address_of_randomZ_8() { return &___randomZ_8; }
	inline void set_randomZ_8(float value)
	{
		___randomZ_8 = value;
	}

	inline static int32_t get_offset_of_offsetX_9() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___offsetX_9)); }
	inline float get_offsetX_9() const { return ___offsetX_9; }
	inline float* get_address_of_offsetX_9() { return &___offsetX_9; }
	inline void set_offsetX_9(float value)
	{
		___offsetX_9 = value;
	}

	inline static int32_t get_offset_of_offsetY_10() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___offsetY_10)); }
	inline float get_offsetY_10() const { return ___offsetY_10; }
	inline float* get_address_of_offsetY_10() { return &___offsetY_10; }
	inline void set_offsetY_10(float value)
	{
		___offsetY_10 = value;
	}

	inline static int32_t get_offset_of_offsetZ_11() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___offsetZ_11)); }
	inline float get_offsetZ_11() const { return ___offsetZ_11; }
	inline float* get_address_of_offsetZ_11() { return &___offsetZ_11; }
	inline void set_offsetZ_11(float value)
	{
		___offsetZ_11 = value;
	}

	inline static int32_t get_offset_of_deltaTime_12() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___deltaTime_12)); }
	inline float get_deltaTime_12() const { return ___deltaTime_12; }
	inline float* get_address_of_deltaTime_12() { return &___deltaTime_12; }
	inline void set_deltaTime_12(float value)
	{
		___deltaTime_12 = value;
	}

	inline static int32_t get_offset_of_t_13() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___t_13)); }
	inline Thread_t241561612 * get_t_13() const { return ___t_13; }
	inline Thread_t241561612 ** get_address_of_t_13() { return &___t_13; }
	inline void set_t_13(Thread_t241561612 * value)
	{
		___t_13 = value;
		Il2CppCodeGenWriteBarrier((&___t_13), value);
	}

	inline static int32_t get_offset_of_locker_14() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___locker_14)); }
	inline RuntimeObject * get_locker_14() const { return ___locker_14; }
	inline RuntimeObject ** get_address_of_locker_14() { return &___locker_14; }
	inline void set_locker_14(RuntimeObject * value)
	{
		___locker_14 = value;
		Il2CppCodeGenWriteBarrier((&___locker_14), value);
	}

	inline static int32_t get_offset_of_processing_15() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___processing_15)); }
	inline bool get_processing_15() const { return ___processing_15; }
	inline bool* get_address_of_processing_15() { return &___processing_15; }
	inline void set_processing_15(bool value)
	{
		___processing_15 = value;
	}

	inline static int32_t get_offset_of_isDoneAssigning_16() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffectorMT_t1716033708, ___isDoneAssigning_16)); }
	inline bool get_isDoneAssigning_16() const { return ___isDoneAssigning_16; }
	inline bool* get_address_of_isDoneAssigning_16() { return &___isDoneAssigning_16; }
	inline void set_isDoneAssigning_16(bool value)
	{
		___isDoneAssigning_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURBULENCEPARTICLEAFFECTORMT_T1716033708_H
#ifndef PARTICLEFORCEFIELDSDEMO_T2005012413_H
#define PARTICLEFORCEFIELDSDEMO_T2005012413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleForceFieldsDemo
struct  ParticleForceFieldsDemo_t2005012413  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ParticleForceFieldsDemo::FPSText
	Text_t356221433 * ___FPSText_2;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::particleCountText
	Text_t356221433 * ___particleCountText_3;
	// UnityEngine.UI.Toggle ParticleForceFieldsDemo::postProcessingToggle
	Toggle_t3976754468 * ___postProcessingToggle_4;
	// UnityEngine.MonoBehaviour ParticleForceFieldsDemo::postProcessing
	MonoBehaviour_t1158329972 * ___postProcessing_5;
	// UnityEngine.ParticleSystem ParticleForceFieldsDemo::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_6;
	// UnityEngine.ParticleSystem/MainModule ParticleForceFieldsDemo::particleSystemMainModule
	MainModule_t6751348  ___particleSystemMainModule_7;
	// UnityEngine.ParticleSystem/EmissionModule ParticleForceFieldsDemo::particleSystemEmissionModule
	EmissionModule_t2748003162  ___particleSystemEmissionModule_8;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::maxParticlesText
	Text_t356221433 * ___maxParticlesText_9;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::particlesPerSecondText
	Text_t356221433 * ___particlesPerSecondText_10;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::maxParticlesSlider
	Slider_t297367283 * ___maxParticlesSlider_11;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::particlesPerSecondSlider
	Slider_t297367283 * ___particlesPerSecondSlider_12;
	// MirzaBeig.Scripting.Effects.AttractionParticleForceField ParticleForceFieldsDemo::attractionParticleForceField
	AttractionParticleForceField_t2380317660 * ___attractionParticleForceField_13;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::attractionParticleForceFieldRadiusText
	Text_t356221433 * ___attractionParticleForceFieldRadiusText_14;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::attractionParticleForceFieldMaxForceText
	Text_t356221433 * ___attractionParticleForceFieldMaxForceText_15;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::attractionParticleForceFieldArrivalRadiusText
	Text_t356221433 * ___attractionParticleForceFieldArrivalRadiusText_16;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::attractionParticleForceFieldArrivedRadiusText
	Text_t356221433 * ___attractionParticleForceFieldArrivedRadiusText_17;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::attractionParticleForceFieldPositionTextX
	Text_t356221433 * ___attractionParticleForceFieldPositionTextX_18;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::attractionParticleForceFieldPositionTextY
	Text_t356221433 * ___attractionParticleForceFieldPositionTextY_19;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::attractionParticleForceFieldPositionTextZ
	Text_t356221433 * ___attractionParticleForceFieldPositionTextZ_20;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::attractionParticleForceFieldRadiusSlider
	Slider_t297367283 * ___attractionParticleForceFieldRadiusSlider_21;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::attractionParticleForceFieldMaxForceSlider
	Slider_t297367283 * ___attractionParticleForceFieldMaxForceSlider_22;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::attractionParticleForceFieldArrivalRadiusSlider
	Slider_t297367283 * ___attractionParticleForceFieldArrivalRadiusSlider_23;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::attractionParticleForceFieldArrivedRadiusSlider
	Slider_t297367283 * ___attractionParticleForceFieldArrivedRadiusSlider_24;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::attractionParticleForceFieldPositionSliderX
	Slider_t297367283 * ___attractionParticleForceFieldPositionSliderX_25;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::attractionParticleForceFieldPositionSliderY
	Slider_t297367283 * ___attractionParticleForceFieldPositionSliderY_26;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::attractionParticleForceFieldPositionSliderZ
	Slider_t297367283 * ___attractionParticleForceFieldPositionSliderZ_27;
	// MirzaBeig.Scripting.Effects.VortexParticleForceField ParticleForceFieldsDemo::vortexParticleForceField
	VortexParticleForceField_t1112726591 * ___vortexParticleForceField_28;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldRadiusText
	Text_t356221433 * ___vortexParticleForceFieldRadiusText_29;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldMaxForceText
	Text_t356221433 * ___vortexParticleForceFieldMaxForceText_30;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldRotationTextX
	Text_t356221433 * ___vortexParticleForceFieldRotationTextX_31;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldRotationTextY
	Text_t356221433 * ___vortexParticleForceFieldRotationTextY_32;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldRotationTextZ
	Text_t356221433 * ___vortexParticleForceFieldRotationTextZ_33;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldPositionTextX
	Text_t356221433 * ___vortexParticleForceFieldPositionTextX_34;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldPositionTextY
	Text_t356221433 * ___vortexParticleForceFieldPositionTextY_35;
	// UnityEngine.UI.Text ParticleForceFieldsDemo::vortexParticleForceFieldPositionTextZ
	Text_t356221433 * ___vortexParticleForceFieldPositionTextZ_36;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldRadiusSlider
	Slider_t297367283 * ___vortexParticleForceFieldRadiusSlider_37;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldMaxForceSlider
	Slider_t297367283 * ___vortexParticleForceFieldMaxForceSlider_38;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldRotationSliderX
	Slider_t297367283 * ___vortexParticleForceFieldRotationSliderX_39;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldRotationSliderY
	Slider_t297367283 * ___vortexParticleForceFieldRotationSliderY_40;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldRotationSliderZ
	Slider_t297367283 * ___vortexParticleForceFieldRotationSliderZ_41;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldPositionSliderX
	Slider_t297367283 * ___vortexParticleForceFieldPositionSliderX_42;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldPositionSliderY
	Slider_t297367283 * ___vortexParticleForceFieldPositionSliderY_43;
	// UnityEngine.UI.Slider ParticleForceFieldsDemo::vortexParticleForceFieldPositionSliderZ
	Slider_t297367283 * ___vortexParticleForceFieldPositionSliderZ_44;

public:
	inline static int32_t get_offset_of_FPSText_2() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___FPSText_2)); }
	inline Text_t356221433 * get_FPSText_2() const { return ___FPSText_2; }
	inline Text_t356221433 ** get_address_of_FPSText_2() { return &___FPSText_2; }
	inline void set_FPSText_2(Text_t356221433 * value)
	{
		___FPSText_2 = value;
		Il2CppCodeGenWriteBarrier((&___FPSText_2), value);
	}

	inline static int32_t get_offset_of_particleCountText_3() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___particleCountText_3)); }
	inline Text_t356221433 * get_particleCountText_3() const { return ___particleCountText_3; }
	inline Text_t356221433 ** get_address_of_particleCountText_3() { return &___particleCountText_3; }
	inline void set_particleCountText_3(Text_t356221433 * value)
	{
		___particleCountText_3 = value;
		Il2CppCodeGenWriteBarrier((&___particleCountText_3), value);
	}

	inline static int32_t get_offset_of_postProcessingToggle_4() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___postProcessingToggle_4)); }
	inline Toggle_t3976754468 * get_postProcessingToggle_4() const { return ___postProcessingToggle_4; }
	inline Toggle_t3976754468 ** get_address_of_postProcessingToggle_4() { return &___postProcessingToggle_4; }
	inline void set_postProcessingToggle_4(Toggle_t3976754468 * value)
	{
		___postProcessingToggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___postProcessingToggle_4), value);
	}

	inline static int32_t get_offset_of_postProcessing_5() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___postProcessing_5)); }
	inline MonoBehaviour_t1158329972 * get_postProcessing_5() const { return ___postProcessing_5; }
	inline MonoBehaviour_t1158329972 ** get_address_of_postProcessing_5() { return &___postProcessing_5; }
	inline void set_postProcessing_5(MonoBehaviour_t1158329972 * value)
	{
		___postProcessing_5 = value;
		Il2CppCodeGenWriteBarrier((&___postProcessing_5), value);
	}

	inline static int32_t get_offset_of_particleSystem_6() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___particleSystem_6)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_6() const { return ___particleSystem_6; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_6() { return &___particleSystem_6; }
	inline void set_particleSystem_6(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_6), value);
	}

	inline static int32_t get_offset_of_particleSystemMainModule_7() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___particleSystemMainModule_7)); }
	inline MainModule_t6751348  get_particleSystemMainModule_7() const { return ___particleSystemMainModule_7; }
	inline MainModule_t6751348 * get_address_of_particleSystemMainModule_7() { return &___particleSystemMainModule_7; }
	inline void set_particleSystemMainModule_7(MainModule_t6751348  value)
	{
		___particleSystemMainModule_7 = value;
	}

	inline static int32_t get_offset_of_particleSystemEmissionModule_8() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___particleSystemEmissionModule_8)); }
	inline EmissionModule_t2748003162  get_particleSystemEmissionModule_8() const { return ___particleSystemEmissionModule_8; }
	inline EmissionModule_t2748003162 * get_address_of_particleSystemEmissionModule_8() { return &___particleSystemEmissionModule_8; }
	inline void set_particleSystemEmissionModule_8(EmissionModule_t2748003162  value)
	{
		___particleSystemEmissionModule_8 = value;
	}

	inline static int32_t get_offset_of_maxParticlesText_9() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___maxParticlesText_9)); }
	inline Text_t356221433 * get_maxParticlesText_9() const { return ___maxParticlesText_9; }
	inline Text_t356221433 ** get_address_of_maxParticlesText_9() { return &___maxParticlesText_9; }
	inline void set_maxParticlesText_9(Text_t356221433 * value)
	{
		___maxParticlesText_9 = value;
		Il2CppCodeGenWriteBarrier((&___maxParticlesText_9), value);
	}

	inline static int32_t get_offset_of_particlesPerSecondText_10() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___particlesPerSecondText_10)); }
	inline Text_t356221433 * get_particlesPerSecondText_10() const { return ___particlesPerSecondText_10; }
	inline Text_t356221433 ** get_address_of_particlesPerSecondText_10() { return &___particlesPerSecondText_10; }
	inline void set_particlesPerSecondText_10(Text_t356221433 * value)
	{
		___particlesPerSecondText_10 = value;
		Il2CppCodeGenWriteBarrier((&___particlesPerSecondText_10), value);
	}

	inline static int32_t get_offset_of_maxParticlesSlider_11() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___maxParticlesSlider_11)); }
	inline Slider_t297367283 * get_maxParticlesSlider_11() const { return ___maxParticlesSlider_11; }
	inline Slider_t297367283 ** get_address_of_maxParticlesSlider_11() { return &___maxParticlesSlider_11; }
	inline void set_maxParticlesSlider_11(Slider_t297367283 * value)
	{
		___maxParticlesSlider_11 = value;
		Il2CppCodeGenWriteBarrier((&___maxParticlesSlider_11), value);
	}

	inline static int32_t get_offset_of_particlesPerSecondSlider_12() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___particlesPerSecondSlider_12)); }
	inline Slider_t297367283 * get_particlesPerSecondSlider_12() const { return ___particlesPerSecondSlider_12; }
	inline Slider_t297367283 ** get_address_of_particlesPerSecondSlider_12() { return &___particlesPerSecondSlider_12; }
	inline void set_particlesPerSecondSlider_12(Slider_t297367283 * value)
	{
		___particlesPerSecondSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___particlesPerSecondSlider_12), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceField_13() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceField_13)); }
	inline AttractionParticleForceField_t2380317660 * get_attractionParticleForceField_13() const { return ___attractionParticleForceField_13; }
	inline AttractionParticleForceField_t2380317660 ** get_address_of_attractionParticleForceField_13() { return &___attractionParticleForceField_13; }
	inline void set_attractionParticleForceField_13(AttractionParticleForceField_t2380317660 * value)
	{
		___attractionParticleForceField_13 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceField_13), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldRadiusText_14() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldRadiusText_14)); }
	inline Text_t356221433 * get_attractionParticleForceFieldRadiusText_14() const { return ___attractionParticleForceFieldRadiusText_14; }
	inline Text_t356221433 ** get_address_of_attractionParticleForceFieldRadiusText_14() { return &___attractionParticleForceFieldRadiusText_14; }
	inline void set_attractionParticleForceFieldRadiusText_14(Text_t356221433 * value)
	{
		___attractionParticleForceFieldRadiusText_14 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldRadiusText_14), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldMaxForceText_15() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldMaxForceText_15)); }
	inline Text_t356221433 * get_attractionParticleForceFieldMaxForceText_15() const { return ___attractionParticleForceFieldMaxForceText_15; }
	inline Text_t356221433 ** get_address_of_attractionParticleForceFieldMaxForceText_15() { return &___attractionParticleForceFieldMaxForceText_15; }
	inline void set_attractionParticleForceFieldMaxForceText_15(Text_t356221433 * value)
	{
		___attractionParticleForceFieldMaxForceText_15 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldMaxForceText_15), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldArrivalRadiusText_16() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldArrivalRadiusText_16)); }
	inline Text_t356221433 * get_attractionParticleForceFieldArrivalRadiusText_16() const { return ___attractionParticleForceFieldArrivalRadiusText_16; }
	inline Text_t356221433 ** get_address_of_attractionParticleForceFieldArrivalRadiusText_16() { return &___attractionParticleForceFieldArrivalRadiusText_16; }
	inline void set_attractionParticleForceFieldArrivalRadiusText_16(Text_t356221433 * value)
	{
		___attractionParticleForceFieldArrivalRadiusText_16 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldArrivalRadiusText_16), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldArrivedRadiusText_17() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldArrivedRadiusText_17)); }
	inline Text_t356221433 * get_attractionParticleForceFieldArrivedRadiusText_17() const { return ___attractionParticleForceFieldArrivedRadiusText_17; }
	inline Text_t356221433 ** get_address_of_attractionParticleForceFieldArrivedRadiusText_17() { return &___attractionParticleForceFieldArrivedRadiusText_17; }
	inline void set_attractionParticleForceFieldArrivedRadiusText_17(Text_t356221433 * value)
	{
		___attractionParticleForceFieldArrivedRadiusText_17 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldArrivedRadiusText_17), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldPositionTextX_18() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldPositionTextX_18)); }
	inline Text_t356221433 * get_attractionParticleForceFieldPositionTextX_18() const { return ___attractionParticleForceFieldPositionTextX_18; }
	inline Text_t356221433 ** get_address_of_attractionParticleForceFieldPositionTextX_18() { return &___attractionParticleForceFieldPositionTextX_18; }
	inline void set_attractionParticleForceFieldPositionTextX_18(Text_t356221433 * value)
	{
		___attractionParticleForceFieldPositionTextX_18 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldPositionTextX_18), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldPositionTextY_19() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldPositionTextY_19)); }
	inline Text_t356221433 * get_attractionParticleForceFieldPositionTextY_19() const { return ___attractionParticleForceFieldPositionTextY_19; }
	inline Text_t356221433 ** get_address_of_attractionParticleForceFieldPositionTextY_19() { return &___attractionParticleForceFieldPositionTextY_19; }
	inline void set_attractionParticleForceFieldPositionTextY_19(Text_t356221433 * value)
	{
		___attractionParticleForceFieldPositionTextY_19 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldPositionTextY_19), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldPositionTextZ_20() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldPositionTextZ_20)); }
	inline Text_t356221433 * get_attractionParticleForceFieldPositionTextZ_20() const { return ___attractionParticleForceFieldPositionTextZ_20; }
	inline Text_t356221433 ** get_address_of_attractionParticleForceFieldPositionTextZ_20() { return &___attractionParticleForceFieldPositionTextZ_20; }
	inline void set_attractionParticleForceFieldPositionTextZ_20(Text_t356221433 * value)
	{
		___attractionParticleForceFieldPositionTextZ_20 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldPositionTextZ_20), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldRadiusSlider_21() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldRadiusSlider_21)); }
	inline Slider_t297367283 * get_attractionParticleForceFieldRadiusSlider_21() const { return ___attractionParticleForceFieldRadiusSlider_21; }
	inline Slider_t297367283 ** get_address_of_attractionParticleForceFieldRadiusSlider_21() { return &___attractionParticleForceFieldRadiusSlider_21; }
	inline void set_attractionParticleForceFieldRadiusSlider_21(Slider_t297367283 * value)
	{
		___attractionParticleForceFieldRadiusSlider_21 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldRadiusSlider_21), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldMaxForceSlider_22() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldMaxForceSlider_22)); }
	inline Slider_t297367283 * get_attractionParticleForceFieldMaxForceSlider_22() const { return ___attractionParticleForceFieldMaxForceSlider_22; }
	inline Slider_t297367283 ** get_address_of_attractionParticleForceFieldMaxForceSlider_22() { return &___attractionParticleForceFieldMaxForceSlider_22; }
	inline void set_attractionParticleForceFieldMaxForceSlider_22(Slider_t297367283 * value)
	{
		___attractionParticleForceFieldMaxForceSlider_22 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldMaxForceSlider_22), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldArrivalRadiusSlider_23() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldArrivalRadiusSlider_23)); }
	inline Slider_t297367283 * get_attractionParticleForceFieldArrivalRadiusSlider_23() const { return ___attractionParticleForceFieldArrivalRadiusSlider_23; }
	inline Slider_t297367283 ** get_address_of_attractionParticleForceFieldArrivalRadiusSlider_23() { return &___attractionParticleForceFieldArrivalRadiusSlider_23; }
	inline void set_attractionParticleForceFieldArrivalRadiusSlider_23(Slider_t297367283 * value)
	{
		___attractionParticleForceFieldArrivalRadiusSlider_23 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldArrivalRadiusSlider_23), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldArrivedRadiusSlider_24() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldArrivedRadiusSlider_24)); }
	inline Slider_t297367283 * get_attractionParticleForceFieldArrivedRadiusSlider_24() const { return ___attractionParticleForceFieldArrivedRadiusSlider_24; }
	inline Slider_t297367283 ** get_address_of_attractionParticleForceFieldArrivedRadiusSlider_24() { return &___attractionParticleForceFieldArrivedRadiusSlider_24; }
	inline void set_attractionParticleForceFieldArrivedRadiusSlider_24(Slider_t297367283 * value)
	{
		___attractionParticleForceFieldArrivedRadiusSlider_24 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldArrivedRadiusSlider_24), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldPositionSliderX_25() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldPositionSliderX_25)); }
	inline Slider_t297367283 * get_attractionParticleForceFieldPositionSliderX_25() const { return ___attractionParticleForceFieldPositionSliderX_25; }
	inline Slider_t297367283 ** get_address_of_attractionParticleForceFieldPositionSliderX_25() { return &___attractionParticleForceFieldPositionSliderX_25; }
	inline void set_attractionParticleForceFieldPositionSliderX_25(Slider_t297367283 * value)
	{
		___attractionParticleForceFieldPositionSliderX_25 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldPositionSliderX_25), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldPositionSliderY_26() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldPositionSliderY_26)); }
	inline Slider_t297367283 * get_attractionParticleForceFieldPositionSliderY_26() const { return ___attractionParticleForceFieldPositionSliderY_26; }
	inline Slider_t297367283 ** get_address_of_attractionParticleForceFieldPositionSliderY_26() { return &___attractionParticleForceFieldPositionSliderY_26; }
	inline void set_attractionParticleForceFieldPositionSliderY_26(Slider_t297367283 * value)
	{
		___attractionParticleForceFieldPositionSliderY_26 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldPositionSliderY_26), value);
	}

	inline static int32_t get_offset_of_attractionParticleForceFieldPositionSliderZ_27() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___attractionParticleForceFieldPositionSliderZ_27)); }
	inline Slider_t297367283 * get_attractionParticleForceFieldPositionSliderZ_27() const { return ___attractionParticleForceFieldPositionSliderZ_27; }
	inline Slider_t297367283 ** get_address_of_attractionParticleForceFieldPositionSliderZ_27() { return &___attractionParticleForceFieldPositionSliderZ_27; }
	inline void set_attractionParticleForceFieldPositionSliderZ_27(Slider_t297367283 * value)
	{
		___attractionParticleForceFieldPositionSliderZ_27 = value;
		Il2CppCodeGenWriteBarrier((&___attractionParticleForceFieldPositionSliderZ_27), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceField_28() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceField_28)); }
	inline VortexParticleForceField_t1112726591 * get_vortexParticleForceField_28() const { return ___vortexParticleForceField_28; }
	inline VortexParticleForceField_t1112726591 ** get_address_of_vortexParticleForceField_28() { return &___vortexParticleForceField_28; }
	inline void set_vortexParticleForceField_28(VortexParticleForceField_t1112726591 * value)
	{
		___vortexParticleForceField_28 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceField_28), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRadiusText_29() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRadiusText_29)); }
	inline Text_t356221433 * get_vortexParticleForceFieldRadiusText_29() const { return ___vortexParticleForceFieldRadiusText_29; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldRadiusText_29() { return &___vortexParticleForceFieldRadiusText_29; }
	inline void set_vortexParticleForceFieldRadiusText_29(Text_t356221433 * value)
	{
		___vortexParticleForceFieldRadiusText_29 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRadiusText_29), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldMaxForceText_30() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldMaxForceText_30)); }
	inline Text_t356221433 * get_vortexParticleForceFieldMaxForceText_30() const { return ___vortexParticleForceFieldMaxForceText_30; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldMaxForceText_30() { return &___vortexParticleForceFieldMaxForceText_30; }
	inline void set_vortexParticleForceFieldMaxForceText_30(Text_t356221433 * value)
	{
		___vortexParticleForceFieldMaxForceText_30 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldMaxForceText_30), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRotationTextX_31() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRotationTextX_31)); }
	inline Text_t356221433 * get_vortexParticleForceFieldRotationTextX_31() const { return ___vortexParticleForceFieldRotationTextX_31; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldRotationTextX_31() { return &___vortexParticleForceFieldRotationTextX_31; }
	inline void set_vortexParticleForceFieldRotationTextX_31(Text_t356221433 * value)
	{
		___vortexParticleForceFieldRotationTextX_31 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRotationTextX_31), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRotationTextY_32() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRotationTextY_32)); }
	inline Text_t356221433 * get_vortexParticleForceFieldRotationTextY_32() const { return ___vortexParticleForceFieldRotationTextY_32; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldRotationTextY_32() { return &___vortexParticleForceFieldRotationTextY_32; }
	inline void set_vortexParticleForceFieldRotationTextY_32(Text_t356221433 * value)
	{
		___vortexParticleForceFieldRotationTextY_32 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRotationTextY_32), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRotationTextZ_33() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRotationTextZ_33)); }
	inline Text_t356221433 * get_vortexParticleForceFieldRotationTextZ_33() const { return ___vortexParticleForceFieldRotationTextZ_33; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldRotationTextZ_33() { return &___vortexParticleForceFieldRotationTextZ_33; }
	inline void set_vortexParticleForceFieldRotationTextZ_33(Text_t356221433 * value)
	{
		___vortexParticleForceFieldRotationTextZ_33 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRotationTextZ_33), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldPositionTextX_34() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldPositionTextX_34)); }
	inline Text_t356221433 * get_vortexParticleForceFieldPositionTextX_34() const { return ___vortexParticleForceFieldPositionTextX_34; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldPositionTextX_34() { return &___vortexParticleForceFieldPositionTextX_34; }
	inline void set_vortexParticleForceFieldPositionTextX_34(Text_t356221433 * value)
	{
		___vortexParticleForceFieldPositionTextX_34 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldPositionTextX_34), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldPositionTextY_35() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldPositionTextY_35)); }
	inline Text_t356221433 * get_vortexParticleForceFieldPositionTextY_35() const { return ___vortexParticleForceFieldPositionTextY_35; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldPositionTextY_35() { return &___vortexParticleForceFieldPositionTextY_35; }
	inline void set_vortexParticleForceFieldPositionTextY_35(Text_t356221433 * value)
	{
		___vortexParticleForceFieldPositionTextY_35 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldPositionTextY_35), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldPositionTextZ_36() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldPositionTextZ_36)); }
	inline Text_t356221433 * get_vortexParticleForceFieldPositionTextZ_36() const { return ___vortexParticleForceFieldPositionTextZ_36; }
	inline Text_t356221433 ** get_address_of_vortexParticleForceFieldPositionTextZ_36() { return &___vortexParticleForceFieldPositionTextZ_36; }
	inline void set_vortexParticleForceFieldPositionTextZ_36(Text_t356221433 * value)
	{
		___vortexParticleForceFieldPositionTextZ_36 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldPositionTextZ_36), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRadiusSlider_37() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRadiusSlider_37)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldRadiusSlider_37() const { return ___vortexParticleForceFieldRadiusSlider_37; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldRadiusSlider_37() { return &___vortexParticleForceFieldRadiusSlider_37; }
	inline void set_vortexParticleForceFieldRadiusSlider_37(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldRadiusSlider_37 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRadiusSlider_37), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldMaxForceSlider_38() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldMaxForceSlider_38)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldMaxForceSlider_38() const { return ___vortexParticleForceFieldMaxForceSlider_38; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldMaxForceSlider_38() { return &___vortexParticleForceFieldMaxForceSlider_38; }
	inline void set_vortexParticleForceFieldMaxForceSlider_38(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldMaxForceSlider_38 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldMaxForceSlider_38), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRotationSliderX_39() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRotationSliderX_39)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldRotationSliderX_39() const { return ___vortexParticleForceFieldRotationSliderX_39; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldRotationSliderX_39() { return &___vortexParticleForceFieldRotationSliderX_39; }
	inline void set_vortexParticleForceFieldRotationSliderX_39(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldRotationSliderX_39 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRotationSliderX_39), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRotationSliderY_40() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRotationSliderY_40)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldRotationSliderY_40() const { return ___vortexParticleForceFieldRotationSliderY_40; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldRotationSliderY_40() { return &___vortexParticleForceFieldRotationSliderY_40; }
	inline void set_vortexParticleForceFieldRotationSliderY_40(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldRotationSliderY_40 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRotationSliderY_40), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldRotationSliderZ_41() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldRotationSliderZ_41)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldRotationSliderZ_41() const { return ___vortexParticleForceFieldRotationSliderZ_41; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldRotationSliderZ_41() { return &___vortexParticleForceFieldRotationSliderZ_41; }
	inline void set_vortexParticleForceFieldRotationSliderZ_41(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldRotationSliderZ_41 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldRotationSliderZ_41), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldPositionSliderX_42() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldPositionSliderX_42)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldPositionSliderX_42() const { return ___vortexParticleForceFieldPositionSliderX_42; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldPositionSliderX_42() { return &___vortexParticleForceFieldPositionSliderX_42; }
	inline void set_vortexParticleForceFieldPositionSliderX_42(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldPositionSliderX_42 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldPositionSliderX_42), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldPositionSliderY_43() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldPositionSliderY_43)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldPositionSliderY_43() const { return ___vortexParticleForceFieldPositionSliderY_43; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldPositionSliderY_43() { return &___vortexParticleForceFieldPositionSliderY_43; }
	inline void set_vortexParticleForceFieldPositionSliderY_43(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldPositionSliderY_43 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldPositionSliderY_43), value);
	}

	inline static int32_t get_offset_of_vortexParticleForceFieldPositionSliderZ_44() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_t2005012413, ___vortexParticleForceFieldPositionSliderZ_44)); }
	inline Slider_t297367283 * get_vortexParticleForceFieldPositionSliderZ_44() const { return ___vortexParticleForceFieldPositionSliderZ_44; }
	inline Slider_t297367283 ** get_address_of_vortexParticleForceFieldPositionSliderZ_44() { return &___vortexParticleForceFieldPositionSliderZ_44; }
	inline void set_vortexParticleForceFieldPositionSliderZ_44(Slider_t297367283 * value)
	{
		___vortexParticleForceFieldPositionSliderZ_44 = value;
		Il2CppCodeGenWriteBarrier((&___vortexParticleForceFieldPositionSliderZ_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEFORCEFIELDSDEMO_T2005012413_H
#ifndef PARTICLEFLOCKING_T1560556585_H
#define PARTICLEFLOCKING_T1560556585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleFlocking
struct  ParticleFlocking_t1560556585  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.Scripting.Effects.ParticleFlocking::maxDistance
	float ___maxDistance_2;
	// System.Single MirzaBeig.Scripting.Effects.ParticleFlocking::cohesion
	float ___cohesion_3;
	// System.Single MirzaBeig.Scripting.Effects.ParticleFlocking::separation
	float ___separation_4;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleFlocking::useVoxels
	bool ___useVoxels_5;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleFlocking::voxelLocalCenterFromBounds
	bool ___voxelLocalCenterFromBounds_6;
	// System.Single MirzaBeig.Scripting.Effects.ParticleFlocking::voxelVolume
	float ___voxelVolume_7;
	// System.Int32 MirzaBeig.Scripting.Effects.ParticleFlocking::voxelsPerAxis
	int32_t ___voxelsPerAxis_8;
	// System.Int32 MirzaBeig.Scripting.Effects.ParticleFlocking::previousVoxelsPerAxisValue
	int32_t ___previousVoxelsPerAxisValue_9;
	// MirzaBeig.Scripting.Effects.ParticleFlocking/Voxel[] MirzaBeig.Scripting.Effects.ParticleFlocking::voxels
	VoxelU5BU5D_t3808224484* ___voxels_10;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticleFlocking::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_11;
	// UnityEngine.ParticleSystem/Particle[] MirzaBeig.Scripting.Effects.ParticleFlocking::particles
	ParticleU5BU5D_t574222242* ___particles_12;
	// UnityEngine.Vector3[] MirzaBeig.Scripting.Effects.ParticleFlocking::particlePositions
	Vector3U5BU5D_t1172311765* ___particlePositions_13;
	// UnityEngine.ParticleSystem/MainModule MirzaBeig.Scripting.Effects.ParticleFlocking::particleSystemMainModule
	MainModule_t6751348  ___particleSystemMainModule_14;
	// System.Single MirzaBeig.Scripting.Effects.ParticleFlocking::delay
	float ___delay_15;
	// System.Single MirzaBeig.Scripting.Effects.ParticleFlocking::timer
	float ___timer_16;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleFlocking::alwaysUpdate
	bool ___alwaysUpdate_17;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleFlocking::visible
	bool ___visible_18;

public:
	inline static int32_t get_offset_of_maxDistance_2() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___maxDistance_2)); }
	inline float get_maxDistance_2() const { return ___maxDistance_2; }
	inline float* get_address_of_maxDistance_2() { return &___maxDistance_2; }
	inline void set_maxDistance_2(float value)
	{
		___maxDistance_2 = value;
	}

	inline static int32_t get_offset_of_cohesion_3() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___cohesion_3)); }
	inline float get_cohesion_3() const { return ___cohesion_3; }
	inline float* get_address_of_cohesion_3() { return &___cohesion_3; }
	inline void set_cohesion_3(float value)
	{
		___cohesion_3 = value;
	}

	inline static int32_t get_offset_of_separation_4() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___separation_4)); }
	inline float get_separation_4() const { return ___separation_4; }
	inline float* get_address_of_separation_4() { return &___separation_4; }
	inline void set_separation_4(float value)
	{
		___separation_4 = value;
	}

	inline static int32_t get_offset_of_useVoxels_5() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___useVoxels_5)); }
	inline bool get_useVoxels_5() const { return ___useVoxels_5; }
	inline bool* get_address_of_useVoxels_5() { return &___useVoxels_5; }
	inline void set_useVoxels_5(bool value)
	{
		___useVoxels_5 = value;
	}

	inline static int32_t get_offset_of_voxelLocalCenterFromBounds_6() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___voxelLocalCenterFromBounds_6)); }
	inline bool get_voxelLocalCenterFromBounds_6() const { return ___voxelLocalCenterFromBounds_6; }
	inline bool* get_address_of_voxelLocalCenterFromBounds_6() { return &___voxelLocalCenterFromBounds_6; }
	inline void set_voxelLocalCenterFromBounds_6(bool value)
	{
		___voxelLocalCenterFromBounds_6 = value;
	}

	inline static int32_t get_offset_of_voxelVolume_7() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___voxelVolume_7)); }
	inline float get_voxelVolume_7() const { return ___voxelVolume_7; }
	inline float* get_address_of_voxelVolume_7() { return &___voxelVolume_7; }
	inline void set_voxelVolume_7(float value)
	{
		___voxelVolume_7 = value;
	}

	inline static int32_t get_offset_of_voxelsPerAxis_8() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___voxelsPerAxis_8)); }
	inline int32_t get_voxelsPerAxis_8() const { return ___voxelsPerAxis_8; }
	inline int32_t* get_address_of_voxelsPerAxis_8() { return &___voxelsPerAxis_8; }
	inline void set_voxelsPerAxis_8(int32_t value)
	{
		___voxelsPerAxis_8 = value;
	}

	inline static int32_t get_offset_of_previousVoxelsPerAxisValue_9() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___previousVoxelsPerAxisValue_9)); }
	inline int32_t get_previousVoxelsPerAxisValue_9() const { return ___previousVoxelsPerAxisValue_9; }
	inline int32_t* get_address_of_previousVoxelsPerAxisValue_9() { return &___previousVoxelsPerAxisValue_9; }
	inline void set_previousVoxelsPerAxisValue_9(int32_t value)
	{
		___previousVoxelsPerAxisValue_9 = value;
	}

	inline static int32_t get_offset_of_voxels_10() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___voxels_10)); }
	inline VoxelU5BU5D_t3808224484* get_voxels_10() const { return ___voxels_10; }
	inline VoxelU5BU5D_t3808224484** get_address_of_voxels_10() { return &___voxels_10; }
	inline void set_voxels_10(VoxelU5BU5D_t3808224484* value)
	{
		___voxels_10 = value;
		Il2CppCodeGenWriteBarrier((&___voxels_10), value);
	}

	inline static int32_t get_offset_of_particleSystem_11() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___particleSystem_11)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_11() const { return ___particleSystem_11; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_11() { return &___particleSystem_11; }
	inline void set_particleSystem_11(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_11 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_11), value);
	}

	inline static int32_t get_offset_of_particles_12() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___particles_12)); }
	inline ParticleU5BU5D_t574222242* get_particles_12() const { return ___particles_12; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_12() { return &___particles_12; }
	inline void set_particles_12(ParticleU5BU5D_t574222242* value)
	{
		___particles_12 = value;
		Il2CppCodeGenWriteBarrier((&___particles_12), value);
	}

	inline static int32_t get_offset_of_particlePositions_13() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___particlePositions_13)); }
	inline Vector3U5BU5D_t1172311765* get_particlePositions_13() const { return ___particlePositions_13; }
	inline Vector3U5BU5D_t1172311765** get_address_of_particlePositions_13() { return &___particlePositions_13; }
	inline void set_particlePositions_13(Vector3U5BU5D_t1172311765* value)
	{
		___particlePositions_13 = value;
		Il2CppCodeGenWriteBarrier((&___particlePositions_13), value);
	}

	inline static int32_t get_offset_of_particleSystemMainModule_14() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___particleSystemMainModule_14)); }
	inline MainModule_t6751348  get_particleSystemMainModule_14() const { return ___particleSystemMainModule_14; }
	inline MainModule_t6751348 * get_address_of_particleSystemMainModule_14() { return &___particleSystemMainModule_14; }
	inline void set_particleSystemMainModule_14(MainModule_t6751348  value)
	{
		___particleSystemMainModule_14 = value;
	}

	inline static int32_t get_offset_of_delay_15() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___delay_15)); }
	inline float get_delay_15() const { return ___delay_15; }
	inline float* get_address_of_delay_15() { return &___delay_15; }
	inline void set_delay_15(float value)
	{
		___delay_15 = value;
	}

	inline static int32_t get_offset_of_timer_16() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___timer_16)); }
	inline float get_timer_16() const { return ___timer_16; }
	inline float* get_address_of_timer_16() { return &___timer_16; }
	inline void set_timer_16(float value)
	{
		___timer_16 = value;
	}

	inline static int32_t get_offset_of_alwaysUpdate_17() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___alwaysUpdate_17)); }
	inline bool get_alwaysUpdate_17() const { return ___alwaysUpdate_17; }
	inline bool* get_address_of_alwaysUpdate_17() { return &___alwaysUpdate_17; }
	inline void set_alwaysUpdate_17(bool value)
	{
		___alwaysUpdate_17 = value;
	}

	inline static int32_t get_offset_of_visible_18() { return static_cast<int32_t>(offsetof(ParticleFlocking_t1560556585, ___visible_18)); }
	inline bool get_visible_18() const { return ___visible_18; }
	inline bool* get_address_of_visible_18() { return &___visible_18; }
	inline void set_visible_18(bool value)
	{
		___visible_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEFLOCKING_T1560556585_H
#ifndef PARTICLEFORCEFIELDSDEMO_CAMERARIG_T3138920947_H
#define PARTICLEFORCEFIELDSDEMO_CAMERARIG_T3138920947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleForceFieldsDemo_CameraRig
struct  ParticleForceFieldsDemo_CameraRig_t3138920947  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform ParticleForceFieldsDemo_CameraRig::pivot
	Transform_t3275118058 * ___pivot_2;
	// UnityEngine.Vector3 ParticleForceFieldsDemo_CameraRig::targetRotation
	Vector3_t2243707580  ___targetRotation_3;
	// System.Single ParticleForceFieldsDemo_CameraRig::rotationLimit
	float ___rotationLimit_4;
	// System.Single ParticleForceFieldsDemo_CameraRig::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single ParticleForceFieldsDemo_CameraRig::rotationLerpSpeed
	float ___rotationLerpSpeed_6;
	// UnityEngine.Vector3 ParticleForceFieldsDemo_CameraRig::startRotation
	Vector3_t2243707580  ___startRotation_7;

public:
	inline static int32_t get_offset_of_pivot_2() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_CameraRig_t3138920947, ___pivot_2)); }
	inline Transform_t3275118058 * get_pivot_2() const { return ___pivot_2; }
	inline Transform_t3275118058 ** get_address_of_pivot_2() { return &___pivot_2; }
	inline void set_pivot_2(Transform_t3275118058 * value)
	{
		___pivot_2 = value;
		Il2CppCodeGenWriteBarrier((&___pivot_2), value);
	}

	inline static int32_t get_offset_of_targetRotation_3() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_CameraRig_t3138920947, ___targetRotation_3)); }
	inline Vector3_t2243707580  get_targetRotation_3() const { return ___targetRotation_3; }
	inline Vector3_t2243707580 * get_address_of_targetRotation_3() { return &___targetRotation_3; }
	inline void set_targetRotation_3(Vector3_t2243707580  value)
	{
		___targetRotation_3 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_4() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_CameraRig_t3138920947, ___rotationLimit_4)); }
	inline float get_rotationLimit_4() const { return ___rotationLimit_4; }
	inline float* get_address_of_rotationLimit_4() { return &___rotationLimit_4; }
	inline void set_rotationLimit_4(float value)
	{
		___rotationLimit_4 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_CameraRig_t3138920947, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_rotationLerpSpeed_6() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_CameraRig_t3138920947, ___rotationLerpSpeed_6)); }
	inline float get_rotationLerpSpeed_6() const { return ___rotationLerpSpeed_6; }
	inline float* get_address_of_rotationLerpSpeed_6() { return &___rotationLerpSpeed_6; }
	inline void set_rotationLerpSpeed_6(float value)
	{
		___rotationLerpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_startRotation_7() { return static_cast<int32_t>(offsetof(ParticleForceFieldsDemo_CameraRig_t3138920947, ___startRotation_7)); }
	inline Vector3_t2243707580  get_startRotation_7() const { return ___startRotation_7; }
	inline Vector3_t2243707580 * get_address_of_startRotation_7() { return &___startRotation_7; }
	inline void set_startRotation_7(Vector3_t2243707580  value)
	{
		___startRotation_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEFORCEFIELDSDEMO_CAMERARIG_T3138920947_H
#ifndef PARTICLEFORCEFIELD_T2985254799_H
#define PARTICLEFORCEFIELD_T2985254799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleForceField
struct  ParticleForceField_t2985254799  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.Scripting.Effects.ParticleForceField::radius
	float ___radius_2;
	// System.Single MirzaBeig.Scripting.Effects.ParticleForceField::force
	float ___force_3;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleForceField::center
	Vector3_t2243707580  ___center_4;
	// System.Single MirzaBeig.Scripting.Effects.ParticleForceField::_radius
	float ____radius_5;
	// System.Single MirzaBeig.Scripting.Effects.ParticleForceField::radiusSqr
	float ___radiusSqr_6;
	// System.Single MirzaBeig.Scripting.Effects.ParticleForceField::forceDeltaTime
	float ___forceDeltaTime_7;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleForceField::transformPosition
	Vector3_t2243707580  ___transformPosition_8;
	// System.Single[] MirzaBeig.Scripting.Effects.ParticleForceField::particleSystemExternalForcesMultipliers
	SingleU5BU5D_t577127397* ___particleSystemExternalForcesMultipliers_9;
	// UnityEngine.AnimationCurve MirzaBeig.Scripting.Effects.ParticleForceField::forceOverDistance
	AnimationCurve_t3306541151 * ___forceOverDistance_10;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticleForceField::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_11;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> MirzaBeig.Scripting.Effects.ParticleForceField::_particleSystems
	List_1_t2763752173 * ____particleSystems_12;
	// System.Int32 MirzaBeig.Scripting.Effects.ParticleForceField::particleSystemsCount
	int32_t ___particleSystemsCount_13;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> MirzaBeig.Scripting.Effects.ParticleForceField::particleSystems
	List_1_t2763752173 * ___particleSystems_14;
	// UnityEngine.ParticleSystem/Particle[][] MirzaBeig.Scripting.Effects.ParticleForceField::particleSystemParticles
	ParticleU5BU5DU5BU5D_t2909307799* ___particleSystemParticles_15;
	// UnityEngine.ParticleSystem/MainModule[] MirzaBeig.Scripting.Effects.ParticleForceField::particleSystemMainModules
	MainModuleU5BU5D_t2453288765* ___particleSystemMainModules_16;
	// UnityEngine.Renderer[] MirzaBeig.Scripting.Effects.ParticleForceField::particleSystemRenderers
	RendererU5BU5D_t2810717544* ___particleSystemRenderers_17;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticleForceField::currentParticleSystem
	ParticleSystem_t3394631041 * ___currentParticleSystem_18;
	// MirzaBeig.Scripting.Effects.ParticleForceField/GetForceParameters MirzaBeig.Scripting.Effects.ParticleForceField::parameters
	GetForceParameters_t2196150906  ___parameters_19;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleForceField::alwaysUpdate
	bool ___alwaysUpdate_20;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_force_3() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___force_3)); }
	inline float get_force_3() const { return ___force_3; }
	inline float* get_address_of_force_3() { return &___force_3; }
	inline void set_force_3(float value)
	{
		___force_3 = value;
	}

	inline static int32_t get_offset_of_center_4() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___center_4)); }
	inline Vector3_t2243707580  get_center_4() const { return ___center_4; }
	inline Vector3_t2243707580 * get_address_of_center_4() { return &___center_4; }
	inline void set_center_4(Vector3_t2243707580  value)
	{
		___center_4 = value;
	}

	inline static int32_t get_offset_of__radius_5() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ____radius_5)); }
	inline float get__radius_5() const { return ____radius_5; }
	inline float* get_address_of__radius_5() { return &____radius_5; }
	inline void set__radius_5(float value)
	{
		____radius_5 = value;
	}

	inline static int32_t get_offset_of_radiusSqr_6() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___radiusSqr_6)); }
	inline float get_radiusSqr_6() const { return ___radiusSqr_6; }
	inline float* get_address_of_radiusSqr_6() { return &___radiusSqr_6; }
	inline void set_radiusSqr_6(float value)
	{
		___radiusSqr_6 = value;
	}

	inline static int32_t get_offset_of_forceDeltaTime_7() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___forceDeltaTime_7)); }
	inline float get_forceDeltaTime_7() const { return ___forceDeltaTime_7; }
	inline float* get_address_of_forceDeltaTime_7() { return &___forceDeltaTime_7; }
	inline void set_forceDeltaTime_7(float value)
	{
		___forceDeltaTime_7 = value;
	}

	inline static int32_t get_offset_of_transformPosition_8() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___transformPosition_8)); }
	inline Vector3_t2243707580  get_transformPosition_8() const { return ___transformPosition_8; }
	inline Vector3_t2243707580 * get_address_of_transformPosition_8() { return &___transformPosition_8; }
	inline void set_transformPosition_8(Vector3_t2243707580  value)
	{
		___transformPosition_8 = value;
	}

	inline static int32_t get_offset_of_particleSystemExternalForcesMultipliers_9() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___particleSystemExternalForcesMultipliers_9)); }
	inline SingleU5BU5D_t577127397* get_particleSystemExternalForcesMultipliers_9() const { return ___particleSystemExternalForcesMultipliers_9; }
	inline SingleU5BU5D_t577127397** get_address_of_particleSystemExternalForcesMultipliers_9() { return &___particleSystemExternalForcesMultipliers_9; }
	inline void set_particleSystemExternalForcesMultipliers_9(SingleU5BU5D_t577127397* value)
	{
		___particleSystemExternalForcesMultipliers_9 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemExternalForcesMultipliers_9), value);
	}

	inline static int32_t get_offset_of_forceOverDistance_10() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___forceOverDistance_10)); }
	inline AnimationCurve_t3306541151 * get_forceOverDistance_10() const { return ___forceOverDistance_10; }
	inline AnimationCurve_t3306541151 ** get_address_of_forceOverDistance_10() { return &___forceOverDistance_10; }
	inline void set_forceOverDistance_10(AnimationCurve_t3306541151 * value)
	{
		___forceOverDistance_10 = value;
		Il2CppCodeGenWriteBarrier((&___forceOverDistance_10), value);
	}

	inline static int32_t get_offset_of_particleSystem_11() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___particleSystem_11)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_11() const { return ___particleSystem_11; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_11() { return &___particleSystem_11; }
	inline void set_particleSystem_11(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_11 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_11), value);
	}

	inline static int32_t get_offset_of__particleSystems_12() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ____particleSystems_12)); }
	inline List_1_t2763752173 * get__particleSystems_12() const { return ____particleSystems_12; }
	inline List_1_t2763752173 ** get_address_of__particleSystems_12() { return &____particleSystems_12; }
	inline void set__particleSystems_12(List_1_t2763752173 * value)
	{
		____particleSystems_12 = value;
		Il2CppCodeGenWriteBarrier((&____particleSystems_12), value);
	}

	inline static int32_t get_offset_of_particleSystemsCount_13() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___particleSystemsCount_13)); }
	inline int32_t get_particleSystemsCount_13() const { return ___particleSystemsCount_13; }
	inline int32_t* get_address_of_particleSystemsCount_13() { return &___particleSystemsCount_13; }
	inline void set_particleSystemsCount_13(int32_t value)
	{
		___particleSystemsCount_13 = value;
	}

	inline static int32_t get_offset_of_particleSystems_14() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___particleSystems_14)); }
	inline List_1_t2763752173 * get_particleSystems_14() const { return ___particleSystems_14; }
	inline List_1_t2763752173 ** get_address_of_particleSystems_14() { return &___particleSystems_14; }
	inline void set_particleSystems_14(List_1_t2763752173 * value)
	{
		___particleSystems_14 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystems_14), value);
	}

	inline static int32_t get_offset_of_particleSystemParticles_15() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___particleSystemParticles_15)); }
	inline ParticleU5BU5DU5BU5D_t2909307799* get_particleSystemParticles_15() const { return ___particleSystemParticles_15; }
	inline ParticleU5BU5DU5BU5D_t2909307799** get_address_of_particleSystemParticles_15() { return &___particleSystemParticles_15; }
	inline void set_particleSystemParticles_15(ParticleU5BU5DU5BU5D_t2909307799* value)
	{
		___particleSystemParticles_15 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemParticles_15), value);
	}

	inline static int32_t get_offset_of_particleSystemMainModules_16() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___particleSystemMainModules_16)); }
	inline MainModuleU5BU5D_t2453288765* get_particleSystemMainModules_16() const { return ___particleSystemMainModules_16; }
	inline MainModuleU5BU5D_t2453288765** get_address_of_particleSystemMainModules_16() { return &___particleSystemMainModules_16; }
	inline void set_particleSystemMainModules_16(MainModuleU5BU5D_t2453288765* value)
	{
		___particleSystemMainModules_16 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemMainModules_16), value);
	}

	inline static int32_t get_offset_of_particleSystemRenderers_17() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___particleSystemRenderers_17)); }
	inline RendererU5BU5D_t2810717544* get_particleSystemRenderers_17() const { return ___particleSystemRenderers_17; }
	inline RendererU5BU5D_t2810717544** get_address_of_particleSystemRenderers_17() { return &___particleSystemRenderers_17; }
	inline void set_particleSystemRenderers_17(RendererU5BU5D_t2810717544* value)
	{
		___particleSystemRenderers_17 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemRenderers_17), value);
	}

	inline static int32_t get_offset_of_currentParticleSystem_18() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___currentParticleSystem_18)); }
	inline ParticleSystem_t3394631041 * get_currentParticleSystem_18() const { return ___currentParticleSystem_18; }
	inline ParticleSystem_t3394631041 ** get_address_of_currentParticleSystem_18() { return &___currentParticleSystem_18; }
	inline void set_currentParticleSystem_18(ParticleSystem_t3394631041 * value)
	{
		___currentParticleSystem_18 = value;
		Il2CppCodeGenWriteBarrier((&___currentParticleSystem_18), value);
	}

	inline static int32_t get_offset_of_parameters_19() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___parameters_19)); }
	inline GetForceParameters_t2196150906  get_parameters_19() const { return ___parameters_19; }
	inline GetForceParameters_t2196150906 * get_address_of_parameters_19() { return &___parameters_19; }
	inline void set_parameters_19(GetForceParameters_t2196150906  value)
	{
		___parameters_19 = value;
	}

	inline static int32_t get_offset_of_alwaysUpdate_20() { return static_cast<int32_t>(offsetof(ParticleForceField_t2985254799, ___alwaysUpdate_20)); }
	inline bool get_alwaysUpdate_20() const { return ___alwaysUpdate_20; }
	inline bool* get_address_of_alwaysUpdate_20() { return &___alwaysUpdate_20; }
	inline void set_alwaysUpdate_20(bool value)
	{
		___alwaysUpdate_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEFORCEFIELD_T2985254799_H
#ifndef MOUSEROTATECAMERA_T3577251721_H
#define MOUSEROTATECAMERA_T3577251721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Demos.MouseRotateCamera
struct  MouseRotateCamera_t3577251721  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.Demos.MouseRotateCamera::maxRotation
	float ___maxRotation_2;
	// System.Single MirzaBeig.Demos.MouseRotateCamera::speed
	float ___speed_3;
	// System.Boolean MirzaBeig.Demos.MouseRotateCamera::unscaledTime
	bool ___unscaledTime_4;

public:
	inline static int32_t get_offset_of_maxRotation_2() { return static_cast<int32_t>(offsetof(MouseRotateCamera_t3577251721, ___maxRotation_2)); }
	inline float get_maxRotation_2() const { return ___maxRotation_2; }
	inline float* get_address_of_maxRotation_2() { return &___maxRotation_2; }
	inline void set_maxRotation_2(float value)
	{
		___maxRotation_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(MouseRotateCamera_t3577251721, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_4() { return static_cast<int32_t>(offsetof(MouseRotateCamera_t3577251721, ___unscaledTime_4)); }
	inline bool get_unscaledTime_4() const { return ___unscaledTime_4; }
	inline bool* get_address_of_unscaledTime_4() { return &___unscaledTime_4; }
	inline void set_unscaledTime_4(bool value)
	{
		___unscaledTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEROTATECAMERA_T3577251721_H
#ifndef UI_SCROLLRECTOCCLUSION_T1850795053_H
#define UI_SCROLLRECTOCCLUSION_T1850795053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_ScrollRectOcclusion
struct  UI_ScrollRectOcclusion_t1850795053  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::InitByUser
	bool ___InitByUser_2;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_3;
	// UnityEngine.UI.ContentSizeFitter UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_contentSizeFitter
	ContentSizeFitter_t1325211874 * ____contentSizeFitter_4;
	// UnityEngine.UI.VerticalLayoutGroup UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_verticalLayoutGroup
	VerticalLayoutGroup_t2468316403 * ____verticalLayoutGroup_5;
	// UnityEngine.UI.HorizontalLayoutGroup UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_horizontalLayoutGroup
	HorizontalLayoutGroup_t2875670365 * ____horizontalLayoutGroup_6;
	// UnityEngine.UI.GridLayoutGroup UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_gridLayoutGroup
	GridLayoutGroup_t1515633077 * ____gridLayoutGroup_7;
	// System.Boolean UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_isVertical
	bool ____isVertical_8;
	// System.Boolean UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_isHorizontal
	bool ____isHorizontal_9;
	// System.Single UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_disableMarginX
	float ____disableMarginX_10;
	// System.Single UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::_disableMarginY
	float ____disableMarginY_11;
	// System.Boolean UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::hasDisabledGridComponents
	bool ___hasDisabledGridComponents_12;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.Extensions.UI_ScrollRectOcclusion::items
	List_1_t2719087314 * ___items_13;

public:
	inline static int32_t get_offset_of_InitByUser_2() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ___InitByUser_2)); }
	inline bool get_InitByUser_2() const { return ___InitByUser_2; }
	inline bool* get_address_of_InitByUser_2() { return &___InitByUser_2; }
	inline void set_InitByUser_2(bool value)
	{
		___InitByUser_2 = value;
	}

	inline static int32_t get_offset_of__scrollRect_3() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____scrollRect_3)); }
	inline ScrollRect_t1199013257 * get__scrollRect_3() const { return ____scrollRect_3; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_3() { return &____scrollRect_3; }
	inline void set__scrollRect_3(ScrollRect_t1199013257 * value)
	{
		____scrollRect_3 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_3), value);
	}

	inline static int32_t get_offset_of__contentSizeFitter_4() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____contentSizeFitter_4)); }
	inline ContentSizeFitter_t1325211874 * get__contentSizeFitter_4() const { return ____contentSizeFitter_4; }
	inline ContentSizeFitter_t1325211874 ** get_address_of__contentSizeFitter_4() { return &____contentSizeFitter_4; }
	inline void set__contentSizeFitter_4(ContentSizeFitter_t1325211874 * value)
	{
		____contentSizeFitter_4 = value;
		Il2CppCodeGenWriteBarrier((&____contentSizeFitter_4), value);
	}

	inline static int32_t get_offset_of__verticalLayoutGroup_5() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____verticalLayoutGroup_5)); }
	inline VerticalLayoutGroup_t2468316403 * get__verticalLayoutGroup_5() const { return ____verticalLayoutGroup_5; }
	inline VerticalLayoutGroup_t2468316403 ** get_address_of__verticalLayoutGroup_5() { return &____verticalLayoutGroup_5; }
	inline void set__verticalLayoutGroup_5(VerticalLayoutGroup_t2468316403 * value)
	{
		____verticalLayoutGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&____verticalLayoutGroup_5), value);
	}

	inline static int32_t get_offset_of__horizontalLayoutGroup_6() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____horizontalLayoutGroup_6)); }
	inline HorizontalLayoutGroup_t2875670365 * get__horizontalLayoutGroup_6() const { return ____horizontalLayoutGroup_6; }
	inline HorizontalLayoutGroup_t2875670365 ** get_address_of__horizontalLayoutGroup_6() { return &____horizontalLayoutGroup_6; }
	inline void set__horizontalLayoutGroup_6(HorizontalLayoutGroup_t2875670365 * value)
	{
		____horizontalLayoutGroup_6 = value;
		Il2CppCodeGenWriteBarrier((&____horizontalLayoutGroup_6), value);
	}

	inline static int32_t get_offset_of__gridLayoutGroup_7() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____gridLayoutGroup_7)); }
	inline GridLayoutGroup_t1515633077 * get__gridLayoutGroup_7() const { return ____gridLayoutGroup_7; }
	inline GridLayoutGroup_t1515633077 ** get_address_of__gridLayoutGroup_7() { return &____gridLayoutGroup_7; }
	inline void set__gridLayoutGroup_7(GridLayoutGroup_t1515633077 * value)
	{
		____gridLayoutGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&____gridLayoutGroup_7), value);
	}

	inline static int32_t get_offset_of__isVertical_8() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____isVertical_8)); }
	inline bool get__isVertical_8() const { return ____isVertical_8; }
	inline bool* get_address_of__isVertical_8() { return &____isVertical_8; }
	inline void set__isVertical_8(bool value)
	{
		____isVertical_8 = value;
	}

	inline static int32_t get_offset_of__isHorizontal_9() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____isHorizontal_9)); }
	inline bool get__isHorizontal_9() const { return ____isHorizontal_9; }
	inline bool* get_address_of__isHorizontal_9() { return &____isHorizontal_9; }
	inline void set__isHorizontal_9(bool value)
	{
		____isHorizontal_9 = value;
	}

	inline static int32_t get_offset_of__disableMarginX_10() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____disableMarginX_10)); }
	inline float get__disableMarginX_10() const { return ____disableMarginX_10; }
	inline float* get_address_of__disableMarginX_10() { return &____disableMarginX_10; }
	inline void set__disableMarginX_10(float value)
	{
		____disableMarginX_10 = value;
	}

	inline static int32_t get_offset_of__disableMarginY_11() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ____disableMarginY_11)); }
	inline float get__disableMarginY_11() const { return ____disableMarginY_11; }
	inline float* get_address_of__disableMarginY_11() { return &____disableMarginY_11; }
	inline void set__disableMarginY_11(float value)
	{
		____disableMarginY_11 = value;
	}

	inline static int32_t get_offset_of_hasDisabledGridComponents_12() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ___hasDisabledGridComponents_12)); }
	inline bool get_hasDisabledGridComponents_12() const { return ___hasDisabledGridComponents_12; }
	inline bool* get_address_of_hasDisabledGridComponents_12() { return &___hasDisabledGridComponents_12; }
	inline void set_hasDisabledGridComponents_12(bool value)
	{
		___hasDisabledGridComponents_12 = value;
	}

	inline static int32_t get_offset_of_items_13() { return static_cast<int32_t>(offsetof(UI_ScrollRectOcclusion_t1850795053, ___items_13)); }
	inline List_1_t2719087314 * get_items_13() const { return ___items_13; }
	inline List_1_t2719087314 ** get_address_of_items_13() { return &___items_13; }
	inline void set_items_13(List_1_t2719087314 * value)
	{
		___items_13 = value;
		Il2CppCodeGenWriteBarrier((&___items_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_SCROLLRECTOCCLUSION_T1850795053_H
#ifndef PARTICLEAFFECTOR_T2949767568_H
#define PARTICLEAFFECTOR_T2949767568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.ParticleAffector
struct  ParticleAffector_t2949767568  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffector::radius
	float ___radius_2;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffector::force
	float ___force_3;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleAffector::offset
	Vector3_t2243707580  ___offset_4;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffector::_radius
	float ____radius_5;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffector::radiusSqr
	float ___radiusSqr_6;
	// System.Single MirzaBeig.Scripting.Effects.ParticleAffector::forceDeltaTime
	float ___forceDeltaTime_7;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.ParticleAffector::transformPosition
	Vector3_t2243707580  ___transformPosition_8;
	// System.Single[] MirzaBeig.Scripting.Effects.ParticleAffector::particleSystemExternalForcesMultipliers
	SingleU5BU5D_t577127397* ___particleSystemExternalForcesMultipliers_9;
	// UnityEngine.AnimationCurve MirzaBeig.Scripting.Effects.ParticleAffector::scaleForceByDistance
	AnimationCurve_t3306541151 * ___scaleForceByDistance_10;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticleAffector::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_11;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> MirzaBeig.Scripting.Effects.ParticleAffector::_particleSystems
	List_1_t2763752173 * ____particleSystems_12;
	// System.Int32 MirzaBeig.Scripting.Effects.ParticleAffector::particleSystemsCount
	int32_t ___particleSystemsCount_13;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> MirzaBeig.Scripting.Effects.ParticleAffector::particleSystems
	List_1_t2763752173 * ___particleSystems_14;
	// UnityEngine.ParticleSystem/Particle[][] MirzaBeig.Scripting.Effects.ParticleAffector::particleSystemParticles
	ParticleU5BU5DU5BU5D_t2909307799* ___particleSystemParticles_15;
	// UnityEngine.ParticleSystem/MainModule[] MirzaBeig.Scripting.Effects.ParticleAffector::particleSystemMainModules
	MainModuleU5BU5D_t2453288765* ___particleSystemMainModules_16;
	// UnityEngine.Renderer[] MirzaBeig.Scripting.Effects.ParticleAffector::particleSystemRenderers
	RendererU5BU5D_t2810717544* ___particleSystemRenderers_17;
	// UnityEngine.ParticleSystem MirzaBeig.Scripting.Effects.ParticleAffector::currentParticleSystem
	ParticleSystem_t3394631041 * ___currentParticleSystem_18;
	// MirzaBeig.Scripting.Effects.ParticleAffector/GetForceParameters MirzaBeig.Scripting.Effects.ParticleAffector::parameters
	GetForceParameters_t3736837055  ___parameters_19;
	// System.Boolean MirzaBeig.Scripting.Effects.ParticleAffector::alwaysUpdate
	bool ___alwaysUpdate_20;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_force_3() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___force_3)); }
	inline float get_force_3() const { return ___force_3; }
	inline float* get_address_of_force_3() { return &___force_3; }
	inline void set_force_3(float value)
	{
		___force_3 = value;
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___offset_4)); }
	inline Vector3_t2243707580  get_offset_4() const { return ___offset_4; }
	inline Vector3_t2243707580 * get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(Vector3_t2243707580  value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of__radius_5() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ____radius_5)); }
	inline float get__radius_5() const { return ____radius_5; }
	inline float* get_address_of__radius_5() { return &____radius_5; }
	inline void set__radius_5(float value)
	{
		____radius_5 = value;
	}

	inline static int32_t get_offset_of_radiusSqr_6() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___radiusSqr_6)); }
	inline float get_radiusSqr_6() const { return ___radiusSqr_6; }
	inline float* get_address_of_radiusSqr_6() { return &___radiusSqr_6; }
	inline void set_radiusSqr_6(float value)
	{
		___radiusSqr_6 = value;
	}

	inline static int32_t get_offset_of_forceDeltaTime_7() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___forceDeltaTime_7)); }
	inline float get_forceDeltaTime_7() const { return ___forceDeltaTime_7; }
	inline float* get_address_of_forceDeltaTime_7() { return &___forceDeltaTime_7; }
	inline void set_forceDeltaTime_7(float value)
	{
		___forceDeltaTime_7 = value;
	}

	inline static int32_t get_offset_of_transformPosition_8() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___transformPosition_8)); }
	inline Vector3_t2243707580  get_transformPosition_8() const { return ___transformPosition_8; }
	inline Vector3_t2243707580 * get_address_of_transformPosition_8() { return &___transformPosition_8; }
	inline void set_transformPosition_8(Vector3_t2243707580  value)
	{
		___transformPosition_8 = value;
	}

	inline static int32_t get_offset_of_particleSystemExternalForcesMultipliers_9() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___particleSystemExternalForcesMultipliers_9)); }
	inline SingleU5BU5D_t577127397* get_particleSystemExternalForcesMultipliers_9() const { return ___particleSystemExternalForcesMultipliers_9; }
	inline SingleU5BU5D_t577127397** get_address_of_particleSystemExternalForcesMultipliers_9() { return &___particleSystemExternalForcesMultipliers_9; }
	inline void set_particleSystemExternalForcesMultipliers_9(SingleU5BU5D_t577127397* value)
	{
		___particleSystemExternalForcesMultipliers_9 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemExternalForcesMultipliers_9), value);
	}

	inline static int32_t get_offset_of_scaleForceByDistance_10() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___scaleForceByDistance_10)); }
	inline AnimationCurve_t3306541151 * get_scaleForceByDistance_10() const { return ___scaleForceByDistance_10; }
	inline AnimationCurve_t3306541151 ** get_address_of_scaleForceByDistance_10() { return &___scaleForceByDistance_10; }
	inline void set_scaleForceByDistance_10(AnimationCurve_t3306541151 * value)
	{
		___scaleForceByDistance_10 = value;
		Il2CppCodeGenWriteBarrier((&___scaleForceByDistance_10), value);
	}

	inline static int32_t get_offset_of_particleSystem_11() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___particleSystem_11)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_11() const { return ___particleSystem_11; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_11() { return &___particleSystem_11; }
	inline void set_particleSystem_11(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_11 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystem_11), value);
	}

	inline static int32_t get_offset_of__particleSystems_12() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ____particleSystems_12)); }
	inline List_1_t2763752173 * get__particleSystems_12() const { return ____particleSystems_12; }
	inline List_1_t2763752173 ** get_address_of__particleSystems_12() { return &____particleSystems_12; }
	inline void set__particleSystems_12(List_1_t2763752173 * value)
	{
		____particleSystems_12 = value;
		Il2CppCodeGenWriteBarrier((&____particleSystems_12), value);
	}

	inline static int32_t get_offset_of_particleSystemsCount_13() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___particleSystemsCount_13)); }
	inline int32_t get_particleSystemsCount_13() const { return ___particleSystemsCount_13; }
	inline int32_t* get_address_of_particleSystemsCount_13() { return &___particleSystemsCount_13; }
	inline void set_particleSystemsCount_13(int32_t value)
	{
		___particleSystemsCount_13 = value;
	}

	inline static int32_t get_offset_of_particleSystems_14() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___particleSystems_14)); }
	inline List_1_t2763752173 * get_particleSystems_14() const { return ___particleSystems_14; }
	inline List_1_t2763752173 ** get_address_of_particleSystems_14() { return &___particleSystems_14; }
	inline void set_particleSystems_14(List_1_t2763752173 * value)
	{
		___particleSystems_14 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystems_14), value);
	}

	inline static int32_t get_offset_of_particleSystemParticles_15() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___particleSystemParticles_15)); }
	inline ParticleU5BU5DU5BU5D_t2909307799* get_particleSystemParticles_15() const { return ___particleSystemParticles_15; }
	inline ParticleU5BU5DU5BU5D_t2909307799** get_address_of_particleSystemParticles_15() { return &___particleSystemParticles_15; }
	inline void set_particleSystemParticles_15(ParticleU5BU5DU5BU5D_t2909307799* value)
	{
		___particleSystemParticles_15 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemParticles_15), value);
	}

	inline static int32_t get_offset_of_particleSystemMainModules_16() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___particleSystemMainModules_16)); }
	inline MainModuleU5BU5D_t2453288765* get_particleSystemMainModules_16() const { return ___particleSystemMainModules_16; }
	inline MainModuleU5BU5D_t2453288765** get_address_of_particleSystemMainModules_16() { return &___particleSystemMainModules_16; }
	inline void set_particleSystemMainModules_16(MainModuleU5BU5D_t2453288765* value)
	{
		___particleSystemMainModules_16 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemMainModules_16), value);
	}

	inline static int32_t get_offset_of_particleSystemRenderers_17() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___particleSystemRenderers_17)); }
	inline RendererU5BU5D_t2810717544* get_particleSystemRenderers_17() const { return ___particleSystemRenderers_17; }
	inline RendererU5BU5D_t2810717544** get_address_of_particleSystemRenderers_17() { return &___particleSystemRenderers_17; }
	inline void set_particleSystemRenderers_17(RendererU5BU5D_t2810717544* value)
	{
		___particleSystemRenderers_17 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemRenderers_17), value);
	}

	inline static int32_t get_offset_of_currentParticleSystem_18() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___currentParticleSystem_18)); }
	inline ParticleSystem_t3394631041 * get_currentParticleSystem_18() const { return ___currentParticleSystem_18; }
	inline ParticleSystem_t3394631041 ** get_address_of_currentParticleSystem_18() { return &___currentParticleSystem_18; }
	inline void set_currentParticleSystem_18(ParticleSystem_t3394631041 * value)
	{
		___currentParticleSystem_18 = value;
		Il2CppCodeGenWriteBarrier((&___currentParticleSystem_18), value);
	}

	inline static int32_t get_offset_of_parameters_19() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___parameters_19)); }
	inline GetForceParameters_t3736837055  get_parameters_19() const { return ___parameters_19; }
	inline GetForceParameters_t3736837055 * get_address_of_parameters_19() { return &___parameters_19; }
	inline void set_parameters_19(GetForceParameters_t3736837055  value)
	{
		___parameters_19 = value;
	}

	inline static int32_t get_offset_of_alwaysUpdate_20() { return static_cast<int32_t>(offsetof(ParticleAffector_t2949767568, ___alwaysUpdate_20)); }
	inline bool get_alwaysUpdate_20() const { return ___alwaysUpdate_20; }
	inline bool* get_address_of_alwaysUpdate_20() { return &___alwaysUpdate_20; }
	inline void set_alwaysUpdate_20(bool value)
	{
		___alwaysUpdate_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEAFFECTOR_T2949767568_H
#ifndef ANIMATEDLIGHT_T36455359_H
#define ANIMATEDLIGHT_T36455359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.AnimatedLight
struct  AnimatedLight_t36455359  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Light MirzaBeig.ParticleSystems.AnimatedLight::light
	Light_t494725636 * ___light_2;
	// System.Single MirzaBeig.ParticleSystems.AnimatedLight::<time>k__BackingField
	float ___U3CtimeU3Ek__BackingField_3;
	// System.Single MirzaBeig.ParticleSystems.AnimatedLight::duration
	float ___duration_4;
	// System.Boolean MirzaBeig.ParticleSystems.AnimatedLight::evaluating
	bool ___evaluating_5;
	// UnityEngine.Gradient MirzaBeig.ParticleSystems.AnimatedLight::colourOverLifetime
	Gradient_t3600583008 * ___colourOverLifetime_6;
	// UnityEngine.AnimationCurve MirzaBeig.ParticleSystems.AnimatedLight::intensityOverLifetime
	AnimationCurve_t3306541151 * ___intensityOverLifetime_7;
	// System.Boolean MirzaBeig.ParticleSystems.AnimatedLight::loop
	bool ___loop_8;
	// System.Boolean MirzaBeig.ParticleSystems.AnimatedLight::autoDestruct
	bool ___autoDestruct_9;
	// UnityEngine.Color MirzaBeig.ParticleSystems.AnimatedLight::startColour
	Color_t2020392075  ___startColour_10;
	// System.Single MirzaBeig.ParticleSystems.AnimatedLight::startIntensity
	float ___startIntensity_11;

public:
	inline static int32_t get_offset_of_light_2() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___light_2)); }
	inline Light_t494725636 * get_light_2() const { return ___light_2; }
	inline Light_t494725636 ** get_address_of_light_2() { return &___light_2; }
	inline void set_light_2(Light_t494725636 * value)
	{
		___light_2 = value;
		Il2CppCodeGenWriteBarrier((&___light_2), value);
	}

	inline static int32_t get_offset_of_U3CtimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___U3CtimeU3Ek__BackingField_3)); }
	inline float get_U3CtimeU3Ek__BackingField_3() const { return ___U3CtimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CtimeU3Ek__BackingField_3() { return &___U3CtimeU3Ek__BackingField_3; }
	inline void set_U3CtimeU3Ek__BackingField_3(float value)
	{
		___U3CtimeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_evaluating_5() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___evaluating_5)); }
	inline bool get_evaluating_5() const { return ___evaluating_5; }
	inline bool* get_address_of_evaluating_5() { return &___evaluating_5; }
	inline void set_evaluating_5(bool value)
	{
		___evaluating_5 = value;
	}

	inline static int32_t get_offset_of_colourOverLifetime_6() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___colourOverLifetime_6)); }
	inline Gradient_t3600583008 * get_colourOverLifetime_6() const { return ___colourOverLifetime_6; }
	inline Gradient_t3600583008 ** get_address_of_colourOverLifetime_6() { return &___colourOverLifetime_6; }
	inline void set_colourOverLifetime_6(Gradient_t3600583008 * value)
	{
		___colourOverLifetime_6 = value;
		Il2CppCodeGenWriteBarrier((&___colourOverLifetime_6), value);
	}

	inline static int32_t get_offset_of_intensityOverLifetime_7() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___intensityOverLifetime_7)); }
	inline AnimationCurve_t3306541151 * get_intensityOverLifetime_7() const { return ___intensityOverLifetime_7; }
	inline AnimationCurve_t3306541151 ** get_address_of_intensityOverLifetime_7() { return &___intensityOverLifetime_7; }
	inline void set_intensityOverLifetime_7(AnimationCurve_t3306541151 * value)
	{
		___intensityOverLifetime_7 = value;
		Il2CppCodeGenWriteBarrier((&___intensityOverLifetime_7), value);
	}

	inline static int32_t get_offset_of_loop_8() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___loop_8)); }
	inline bool get_loop_8() const { return ___loop_8; }
	inline bool* get_address_of_loop_8() { return &___loop_8; }
	inline void set_loop_8(bool value)
	{
		___loop_8 = value;
	}

	inline static int32_t get_offset_of_autoDestruct_9() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___autoDestruct_9)); }
	inline bool get_autoDestruct_9() const { return ___autoDestruct_9; }
	inline bool* get_address_of_autoDestruct_9() { return &___autoDestruct_9; }
	inline void set_autoDestruct_9(bool value)
	{
		___autoDestruct_9 = value;
	}

	inline static int32_t get_offset_of_startColour_10() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___startColour_10)); }
	inline Color_t2020392075  get_startColour_10() const { return ___startColour_10; }
	inline Color_t2020392075 * get_address_of_startColour_10() { return &___startColour_10; }
	inline void set_startColour_10(Color_t2020392075  value)
	{
		___startColour_10 = value;
	}

	inline static int32_t get_offset_of_startIntensity_11() { return static_cast<int32_t>(offsetof(AnimatedLight_t36455359, ___startIntensity_11)); }
	inline float get_startIntensity_11() const { return ___startIntensity_11; }
	inline float* get_address_of_startIntensity_11() { return &___startIntensity_11; }
	inline void set_startIntensity_11(float value)
	{
		___startIntensity_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEDLIGHT_T36455359_H
#ifndef PARTICLEMANAGER_T3376067569_H
#define PARTICLEMANAGER_T3376067569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.ParticleManager
struct  ParticleManager_t3376067569  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<MirzaBeig.ParticleSystems.ParticleSystems> MirzaBeig.ParticleSystems.Demos.ParticleManager::particlePrefabs
	List_1_t1643199118 * ___particlePrefabs_2;
	// System.Int32 MirzaBeig.ParticleSystems.Demos.ParticleManager::currentParticlePrefabIndex
	int32_t ___currentParticlePrefabIndex_3;
	// System.Collections.Generic.List`1<MirzaBeig.ParticleSystems.ParticleSystems> MirzaBeig.ParticleSystems.Demos.ParticleManager::particlePrefabsAppend
	List_1_t1643199118 * ___particlePrefabsAppend_4;
	// System.Int32 MirzaBeig.ParticleSystems.Demos.ParticleManager::prefabNameUnderscoreCountCutoff
	int32_t ___prefabNameUnderscoreCountCutoff_5;
	// System.Boolean MirzaBeig.ParticleSystems.Demos.ParticleManager::disableChildrenAtStart
	bool ___disableChildrenAtStart_6;
	// System.Boolean MirzaBeig.ParticleSystems.Demos.ParticleManager::initialized
	bool ___initialized_7;

public:
	inline static int32_t get_offset_of_particlePrefabs_2() { return static_cast<int32_t>(offsetof(ParticleManager_t3376067569, ___particlePrefabs_2)); }
	inline List_1_t1643199118 * get_particlePrefabs_2() const { return ___particlePrefabs_2; }
	inline List_1_t1643199118 ** get_address_of_particlePrefabs_2() { return &___particlePrefabs_2; }
	inline void set_particlePrefabs_2(List_1_t1643199118 * value)
	{
		___particlePrefabs_2 = value;
		Il2CppCodeGenWriteBarrier((&___particlePrefabs_2), value);
	}

	inline static int32_t get_offset_of_currentParticlePrefabIndex_3() { return static_cast<int32_t>(offsetof(ParticleManager_t3376067569, ___currentParticlePrefabIndex_3)); }
	inline int32_t get_currentParticlePrefabIndex_3() const { return ___currentParticlePrefabIndex_3; }
	inline int32_t* get_address_of_currentParticlePrefabIndex_3() { return &___currentParticlePrefabIndex_3; }
	inline void set_currentParticlePrefabIndex_3(int32_t value)
	{
		___currentParticlePrefabIndex_3 = value;
	}

	inline static int32_t get_offset_of_particlePrefabsAppend_4() { return static_cast<int32_t>(offsetof(ParticleManager_t3376067569, ___particlePrefabsAppend_4)); }
	inline List_1_t1643199118 * get_particlePrefabsAppend_4() const { return ___particlePrefabsAppend_4; }
	inline List_1_t1643199118 ** get_address_of_particlePrefabsAppend_4() { return &___particlePrefabsAppend_4; }
	inline void set_particlePrefabsAppend_4(List_1_t1643199118 * value)
	{
		___particlePrefabsAppend_4 = value;
		Il2CppCodeGenWriteBarrier((&___particlePrefabsAppend_4), value);
	}

	inline static int32_t get_offset_of_prefabNameUnderscoreCountCutoff_5() { return static_cast<int32_t>(offsetof(ParticleManager_t3376067569, ___prefabNameUnderscoreCountCutoff_5)); }
	inline int32_t get_prefabNameUnderscoreCountCutoff_5() const { return ___prefabNameUnderscoreCountCutoff_5; }
	inline int32_t* get_address_of_prefabNameUnderscoreCountCutoff_5() { return &___prefabNameUnderscoreCountCutoff_5; }
	inline void set_prefabNameUnderscoreCountCutoff_5(int32_t value)
	{
		___prefabNameUnderscoreCountCutoff_5 = value;
	}

	inline static int32_t get_offset_of_disableChildrenAtStart_6() { return static_cast<int32_t>(offsetof(ParticleManager_t3376067569, ___disableChildrenAtStart_6)); }
	inline bool get_disableChildrenAtStart_6() const { return ___disableChildrenAtStart_6; }
	inline bool* get_address_of_disableChildrenAtStart_6() { return &___disableChildrenAtStart_6; }
	inline void set_disableChildrenAtStart_6(bool value)
	{
		___disableChildrenAtStart_6 = value;
	}

	inline static int32_t get_offset_of_initialized_7() { return static_cast<int32_t>(offsetof(ParticleManager_t3376067569, ___initialized_7)); }
	inline bool get_initialized_7() const { return ___initialized_7; }
	inline bool* get_address_of_initialized_7() { return &___initialized_7; }
	inline void set_initialized_7(bool value)
	{
		___initialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEMANAGER_T3376067569_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef UI_INFINITESCROLL_T1144724582_H
#define UI_INFINITESCROLL_T1144724582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_InfiniteScroll
struct  UI_InfiniteScroll_t1144724582  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UI_InfiniteScroll::InitByUser
	bool ___InitByUser_2;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.UI_InfiniteScroll::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_3;
	// UnityEngine.UI.ContentSizeFitter UnityEngine.UI.Extensions.UI_InfiniteScroll::_contentSizeFitter
	ContentSizeFitter_t1325211874 * ____contentSizeFitter_4;
	// UnityEngine.UI.VerticalLayoutGroup UnityEngine.UI.Extensions.UI_InfiniteScroll::_verticalLayoutGroup
	VerticalLayoutGroup_t2468316403 * ____verticalLayoutGroup_5;
	// UnityEngine.UI.HorizontalLayoutGroup UnityEngine.UI.Extensions.UI_InfiniteScroll::_horizontalLayoutGroup
	HorizontalLayoutGroup_t2875670365 * ____horizontalLayoutGroup_6;
	// UnityEngine.UI.GridLayoutGroup UnityEngine.UI.Extensions.UI_InfiniteScroll::_gridLayoutGroup
	GridLayoutGroup_t1515633077 * ____gridLayoutGroup_7;
	// System.Boolean UnityEngine.UI.Extensions.UI_InfiniteScroll::_isVertical
	bool ____isVertical_8;
	// System.Boolean UnityEngine.UI.Extensions.UI_InfiniteScroll::_isHorizontal
	bool ____isHorizontal_9;
	// System.Single UnityEngine.UI.Extensions.UI_InfiniteScroll::_disableMarginX
	float ____disableMarginX_10;
	// System.Single UnityEngine.UI.Extensions.UI_InfiniteScroll::_disableMarginY
	float ____disableMarginY_11;
	// System.Boolean UnityEngine.UI.Extensions.UI_InfiniteScroll::_hasDisabledGridComponents
	bool ____hasDisabledGridComponents_12;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.Extensions.UI_InfiniteScroll::items
	List_1_t2719087314 * ___items_13;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UI_InfiniteScroll::_newAnchoredPosition
	Vector2_t2243707579  ____newAnchoredPosition_14;
	// System.Single UnityEngine.UI.Extensions.UI_InfiniteScroll::_treshold
	float ____treshold_15;
	// System.Int32 UnityEngine.UI.Extensions.UI_InfiniteScroll::_itemCount
	int32_t ____itemCount_16;
	// System.Single UnityEngine.UI.Extensions.UI_InfiniteScroll::_recordOffsetX
	float ____recordOffsetX_17;
	// System.Single UnityEngine.UI.Extensions.UI_InfiniteScroll::_recordOffsetY
	float ____recordOffsetY_18;

public:
	inline static int32_t get_offset_of_InitByUser_2() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ___InitByUser_2)); }
	inline bool get_InitByUser_2() const { return ___InitByUser_2; }
	inline bool* get_address_of_InitByUser_2() { return &___InitByUser_2; }
	inline void set_InitByUser_2(bool value)
	{
		___InitByUser_2 = value;
	}

	inline static int32_t get_offset_of__scrollRect_3() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____scrollRect_3)); }
	inline ScrollRect_t1199013257 * get__scrollRect_3() const { return ____scrollRect_3; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_3() { return &____scrollRect_3; }
	inline void set__scrollRect_3(ScrollRect_t1199013257 * value)
	{
		____scrollRect_3 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_3), value);
	}

	inline static int32_t get_offset_of__contentSizeFitter_4() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____contentSizeFitter_4)); }
	inline ContentSizeFitter_t1325211874 * get__contentSizeFitter_4() const { return ____contentSizeFitter_4; }
	inline ContentSizeFitter_t1325211874 ** get_address_of__contentSizeFitter_4() { return &____contentSizeFitter_4; }
	inline void set__contentSizeFitter_4(ContentSizeFitter_t1325211874 * value)
	{
		____contentSizeFitter_4 = value;
		Il2CppCodeGenWriteBarrier((&____contentSizeFitter_4), value);
	}

	inline static int32_t get_offset_of__verticalLayoutGroup_5() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____verticalLayoutGroup_5)); }
	inline VerticalLayoutGroup_t2468316403 * get__verticalLayoutGroup_5() const { return ____verticalLayoutGroup_5; }
	inline VerticalLayoutGroup_t2468316403 ** get_address_of__verticalLayoutGroup_5() { return &____verticalLayoutGroup_5; }
	inline void set__verticalLayoutGroup_5(VerticalLayoutGroup_t2468316403 * value)
	{
		____verticalLayoutGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&____verticalLayoutGroup_5), value);
	}

	inline static int32_t get_offset_of__horizontalLayoutGroup_6() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____horizontalLayoutGroup_6)); }
	inline HorizontalLayoutGroup_t2875670365 * get__horizontalLayoutGroup_6() const { return ____horizontalLayoutGroup_6; }
	inline HorizontalLayoutGroup_t2875670365 ** get_address_of__horizontalLayoutGroup_6() { return &____horizontalLayoutGroup_6; }
	inline void set__horizontalLayoutGroup_6(HorizontalLayoutGroup_t2875670365 * value)
	{
		____horizontalLayoutGroup_6 = value;
		Il2CppCodeGenWriteBarrier((&____horizontalLayoutGroup_6), value);
	}

	inline static int32_t get_offset_of__gridLayoutGroup_7() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____gridLayoutGroup_7)); }
	inline GridLayoutGroup_t1515633077 * get__gridLayoutGroup_7() const { return ____gridLayoutGroup_7; }
	inline GridLayoutGroup_t1515633077 ** get_address_of__gridLayoutGroup_7() { return &____gridLayoutGroup_7; }
	inline void set__gridLayoutGroup_7(GridLayoutGroup_t1515633077 * value)
	{
		____gridLayoutGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&____gridLayoutGroup_7), value);
	}

	inline static int32_t get_offset_of__isVertical_8() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____isVertical_8)); }
	inline bool get__isVertical_8() const { return ____isVertical_8; }
	inline bool* get_address_of__isVertical_8() { return &____isVertical_8; }
	inline void set__isVertical_8(bool value)
	{
		____isVertical_8 = value;
	}

	inline static int32_t get_offset_of__isHorizontal_9() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____isHorizontal_9)); }
	inline bool get__isHorizontal_9() const { return ____isHorizontal_9; }
	inline bool* get_address_of__isHorizontal_9() { return &____isHorizontal_9; }
	inline void set__isHorizontal_9(bool value)
	{
		____isHorizontal_9 = value;
	}

	inline static int32_t get_offset_of__disableMarginX_10() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____disableMarginX_10)); }
	inline float get__disableMarginX_10() const { return ____disableMarginX_10; }
	inline float* get_address_of__disableMarginX_10() { return &____disableMarginX_10; }
	inline void set__disableMarginX_10(float value)
	{
		____disableMarginX_10 = value;
	}

	inline static int32_t get_offset_of__disableMarginY_11() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____disableMarginY_11)); }
	inline float get__disableMarginY_11() const { return ____disableMarginY_11; }
	inline float* get_address_of__disableMarginY_11() { return &____disableMarginY_11; }
	inline void set__disableMarginY_11(float value)
	{
		____disableMarginY_11 = value;
	}

	inline static int32_t get_offset_of__hasDisabledGridComponents_12() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____hasDisabledGridComponents_12)); }
	inline bool get__hasDisabledGridComponents_12() const { return ____hasDisabledGridComponents_12; }
	inline bool* get_address_of__hasDisabledGridComponents_12() { return &____hasDisabledGridComponents_12; }
	inline void set__hasDisabledGridComponents_12(bool value)
	{
		____hasDisabledGridComponents_12 = value;
	}

	inline static int32_t get_offset_of_items_13() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ___items_13)); }
	inline List_1_t2719087314 * get_items_13() const { return ___items_13; }
	inline List_1_t2719087314 ** get_address_of_items_13() { return &___items_13; }
	inline void set_items_13(List_1_t2719087314 * value)
	{
		___items_13 = value;
		Il2CppCodeGenWriteBarrier((&___items_13), value);
	}

	inline static int32_t get_offset_of__newAnchoredPosition_14() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____newAnchoredPosition_14)); }
	inline Vector2_t2243707579  get__newAnchoredPosition_14() const { return ____newAnchoredPosition_14; }
	inline Vector2_t2243707579 * get_address_of__newAnchoredPosition_14() { return &____newAnchoredPosition_14; }
	inline void set__newAnchoredPosition_14(Vector2_t2243707579  value)
	{
		____newAnchoredPosition_14 = value;
	}

	inline static int32_t get_offset_of__treshold_15() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____treshold_15)); }
	inline float get__treshold_15() const { return ____treshold_15; }
	inline float* get_address_of__treshold_15() { return &____treshold_15; }
	inline void set__treshold_15(float value)
	{
		____treshold_15 = value;
	}

	inline static int32_t get_offset_of__itemCount_16() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____itemCount_16)); }
	inline int32_t get__itemCount_16() const { return ____itemCount_16; }
	inline int32_t* get_address_of__itemCount_16() { return &____itemCount_16; }
	inline void set__itemCount_16(int32_t value)
	{
		____itemCount_16 = value;
	}

	inline static int32_t get_offset_of__recordOffsetX_17() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____recordOffsetX_17)); }
	inline float get__recordOffsetX_17() const { return ____recordOffsetX_17; }
	inline float* get_address_of__recordOffsetX_17() { return &____recordOffsetX_17; }
	inline void set__recordOffsetX_17(float value)
	{
		____recordOffsetX_17 = value;
	}

	inline static int32_t get_offset_of__recordOffsetY_18() { return static_cast<int32_t>(offsetof(UI_InfiniteScroll_t1144724582, ____recordOffsetY_18)); }
	inline float get__recordOffsetY_18() const { return ____recordOffsetY_18; }
	inline float* get_address_of__recordOffsetY_18() { return &____recordOffsetY_18; }
	inline void set__recordOffsetY_18(float value)
	{
		____recordOffsetY_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_INFINITESCROLL_T1144724582_H
#ifndef ATTRACTIONPARTICLEAFFECTOR_T1503046627_H
#define ATTRACTIONPARTICLEAFFECTOR_T1503046627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.AttractionParticleAffector
struct  AttractionParticleAffector_t1503046627  : public ParticleAffector_t2949767568
{
public:
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleAffector::arrivalRadius
	float ___arrivalRadius_21;
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleAffector::arrivedRadius
	float ___arrivedRadius_22;
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleAffector::arrivalRadiusSqr
	float ___arrivalRadiusSqr_23;
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleAffector::arrivedRadiusSqr
	float ___arrivedRadiusSqr_24;

public:
	inline static int32_t get_offset_of_arrivalRadius_21() { return static_cast<int32_t>(offsetof(AttractionParticleAffector_t1503046627, ___arrivalRadius_21)); }
	inline float get_arrivalRadius_21() const { return ___arrivalRadius_21; }
	inline float* get_address_of_arrivalRadius_21() { return &___arrivalRadius_21; }
	inline void set_arrivalRadius_21(float value)
	{
		___arrivalRadius_21 = value;
	}

	inline static int32_t get_offset_of_arrivedRadius_22() { return static_cast<int32_t>(offsetof(AttractionParticleAffector_t1503046627, ___arrivedRadius_22)); }
	inline float get_arrivedRadius_22() const { return ___arrivedRadius_22; }
	inline float* get_address_of_arrivedRadius_22() { return &___arrivedRadius_22; }
	inline void set_arrivedRadius_22(float value)
	{
		___arrivedRadius_22 = value;
	}

	inline static int32_t get_offset_of_arrivalRadiusSqr_23() { return static_cast<int32_t>(offsetof(AttractionParticleAffector_t1503046627, ___arrivalRadiusSqr_23)); }
	inline float get_arrivalRadiusSqr_23() const { return ___arrivalRadiusSqr_23; }
	inline float* get_address_of_arrivalRadiusSqr_23() { return &___arrivalRadiusSqr_23; }
	inline void set_arrivalRadiusSqr_23(float value)
	{
		___arrivalRadiusSqr_23 = value;
	}

	inline static int32_t get_offset_of_arrivedRadiusSqr_24() { return static_cast<int32_t>(offsetof(AttractionParticleAffector_t1503046627, ___arrivedRadiusSqr_24)); }
	inline float get_arrivedRadiusSqr_24() const { return ___arrivedRadiusSqr_24; }
	inline float* get_address_of_arrivedRadiusSqr_24() { return &___arrivedRadiusSqr_24; }
	inline void set_arrivedRadiusSqr_24(float value)
	{
		___arrivedRadiusSqr_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRACTIONPARTICLEAFFECTOR_T1503046627_H
#ifndef TURBULENCEPARTICLEAFFECTOR_T2589922227_H
#define TURBULENCEPARTICLEAFFECTOR_T2589922227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.TurbulenceParticleAffector
struct  TurbulenceParticleAffector_t2589922227  : public ParticleAffector_t2949767568
{
public:
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::speed
	float ___speed_21;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::frequency
	float ___frequency_22;
	// MirzaBeig.Scripting.Effects.TurbulenceParticleAffector/NoiseType MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::noiseType
	int32_t ___noiseType_23;
	// System.Int32 MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::octaves
	int32_t ___octaves_24;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::lacunarity
	float ___lacunarity_25;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::persistence
	float ___persistence_26;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::time
	float ___time_27;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::randomX
	float ___randomX_28;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::randomY
	float ___randomY_29;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::randomZ
	float ___randomZ_30;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::offsetX
	float ___offsetX_31;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::offsetY
	float ___offsetY_32;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleAffector::offsetZ
	float ___offsetZ_33;

public:
	inline static int32_t get_offset_of_speed_21() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___speed_21)); }
	inline float get_speed_21() const { return ___speed_21; }
	inline float* get_address_of_speed_21() { return &___speed_21; }
	inline void set_speed_21(float value)
	{
		___speed_21 = value;
	}

	inline static int32_t get_offset_of_frequency_22() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___frequency_22)); }
	inline float get_frequency_22() const { return ___frequency_22; }
	inline float* get_address_of_frequency_22() { return &___frequency_22; }
	inline void set_frequency_22(float value)
	{
		___frequency_22 = value;
	}

	inline static int32_t get_offset_of_noiseType_23() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___noiseType_23)); }
	inline int32_t get_noiseType_23() const { return ___noiseType_23; }
	inline int32_t* get_address_of_noiseType_23() { return &___noiseType_23; }
	inline void set_noiseType_23(int32_t value)
	{
		___noiseType_23 = value;
	}

	inline static int32_t get_offset_of_octaves_24() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___octaves_24)); }
	inline int32_t get_octaves_24() const { return ___octaves_24; }
	inline int32_t* get_address_of_octaves_24() { return &___octaves_24; }
	inline void set_octaves_24(int32_t value)
	{
		___octaves_24 = value;
	}

	inline static int32_t get_offset_of_lacunarity_25() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___lacunarity_25)); }
	inline float get_lacunarity_25() const { return ___lacunarity_25; }
	inline float* get_address_of_lacunarity_25() { return &___lacunarity_25; }
	inline void set_lacunarity_25(float value)
	{
		___lacunarity_25 = value;
	}

	inline static int32_t get_offset_of_persistence_26() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___persistence_26)); }
	inline float get_persistence_26() const { return ___persistence_26; }
	inline float* get_address_of_persistence_26() { return &___persistence_26; }
	inline void set_persistence_26(float value)
	{
		___persistence_26 = value;
	}

	inline static int32_t get_offset_of_time_27() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___time_27)); }
	inline float get_time_27() const { return ___time_27; }
	inline float* get_address_of_time_27() { return &___time_27; }
	inline void set_time_27(float value)
	{
		___time_27 = value;
	}

	inline static int32_t get_offset_of_randomX_28() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___randomX_28)); }
	inline float get_randomX_28() const { return ___randomX_28; }
	inline float* get_address_of_randomX_28() { return &___randomX_28; }
	inline void set_randomX_28(float value)
	{
		___randomX_28 = value;
	}

	inline static int32_t get_offset_of_randomY_29() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___randomY_29)); }
	inline float get_randomY_29() const { return ___randomY_29; }
	inline float* get_address_of_randomY_29() { return &___randomY_29; }
	inline void set_randomY_29(float value)
	{
		___randomY_29 = value;
	}

	inline static int32_t get_offset_of_randomZ_30() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___randomZ_30)); }
	inline float get_randomZ_30() const { return ___randomZ_30; }
	inline float* get_address_of_randomZ_30() { return &___randomZ_30; }
	inline void set_randomZ_30(float value)
	{
		___randomZ_30 = value;
	}

	inline static int32_t get_offset_of_offsetX_31() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___offsetX_31)); }
	inline float get_offsetX_31() const { return ___offsetX_31; }
	inline float* get_address_of_offsetX_31() { return &___offsetX_31; }
	inline void set_offsetX_31(float value)
	{
		___offsetX_31 = value;
	}

	inline static int32_t get_offset_of_offsetY_32() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___offsetY_32)); }
	inline float get_offsetY_32() const { return ___offsetY_32; }
	inline float* get_address_of_offsetY_32() { return &___offsetY_32; }
	inline void set_offsetY_32(float value)
	{
		___offsetY_32 = value;
	}

	inline static int32_t get_offset_of_offsetZ_33() { return static_cast<int32_t>(offsetof(TurbulenceParticleAffector_t2589922227, ___offsetZ_33)); }
	inline float get_offsetZ_33() const { return ___offsetZ_33; }
	inline float* get_address_of_offsetZ_33() { return &___offsetZ_33; }
	inline void set_offsetZ_33(float value)
	{
		___offsetZ_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURBULENCEPARTICLEAFFECTOR_T2589922227_H
#ifndef ONESHOTPARTICLESYSTEMSMANAGER_T1174726079_H
#define ONESHOTPARTICLESYSTEMSMANAGER_T1174726079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.OneshotParticleSystemsManager
struct  OneshotParticleSystemsManager_t1174726079  : public ParticleManager_t3376067569
{
public:
	// UnityEngine.LayerMask MirzaBeig.ParticleSystems.Demos.OneshotParticleSystemsManager::mouseRaycastLayerMask
	LayerMask_t3188175821  ___mouseRaycastLayerMask_8;
	// System.Collections.Generic.List`1<MirzaBeig.ParticleSystems.ParticleSystems> MirzaBeig.ParticleSystems.Demos.OneshotParticleSystemsManager::spawnedPrefabs
	List_1_t1643199118 * ___spawnedPrefabs_9;
	// System.Boolean MirzaBeig.ParticleSystems.Demos.OneshotParticleSystemsManager::<disableSpawn>k__BackingField
	bool ___U3CdisableSpawnU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_mouseRaycastLayerMask_8() { return static_cast<int32_t>(offsetof(OneshotParticleSystemsManager_t1174726079, ___mouseRaycastLayerMask_8)); }
	inline LayerMask_t3188175821  get_mouseRaycastLayerMask_8() const { return ___mouseRaycastLayerMask_8; }
	inline LayerMask_t3188175821 * get_address_of_mouseRaycastLayerMask_8() { return &___mouseRaycastLayerMask_8; }
	inline void set_mouseRaycastLayerMask_8(LayerMask_t3188175821  value)
	{
		___mouseRaycastLayerMask_8 = value;
	}

	inline static int32_t get_offset_of_spawnedPrefabs_9() { return static_cast<int32_t>(offsetof(OneshotParticleSystemsManager_t1174726079, ___spawnedPrefabs_9)); }
	inline List_1_t1643199118 * get_spawnedPrefabs_9() const { return ___spawnedPrefabs_9; }
	inline List_1_t1643199118 ** get_address_of_spawnedPrefabs_9() { return &___spawnedPrefabs_9; }
	inline void set_spawnedPrefabs_9(List_1_t1643199118 * value)
	{
		___spawnedPrefabs_9 = value;
		Il2CppCodeGenWriteBarrier((&___spawnedPrefabs_9), value);
	}

	inline static int32_t get_offset_of_U3CdisableSpawnU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(OneshotParticleSystemsManager_t1174726079, ___U3CdisableSpawnU3Ek__BackingField_10)); }
	inline bool get_U3CdisableSpawnU3Ek__BackingField_10() const { return ___U3CdisableSpawnU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CdisableSpawnU3Ek__BackingField_10() { return &___U3CdisableSpawnU3Ek__BackingField_10; }
	inline void set_U3CdisableSpawnU3Ek__BackingField_10(bool value)
	{
		___U3CdisableSpawnU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONESHOTPARTICLESYSTEMSMANAGER_T1174726079_H
#ifndef VORTEXPARTICLEAFFECTOR_T868510250_H
#define VORTEXPARTICLEAFFECTOR_T868510250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.VortexParticleAffector
struct  VortexParticleAffector_t868510250  : public ParticleAffector_t2949767568
{
public:
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.VortexParticleAffector::axisOfRotation
	Vector3_t2243707580  ___axisOfRotation_21;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.VortexParticleAffector::axisOfRotationOffset
	Vector3_t2243707580  ___axisOfRotationOffset_22;

public:
	inline static int32_t get_offset_of_axisOfRotation_21() { return static_cast<int32_t>(offsetof(VortexParticleAffector_t868510250, ___axisOfRotation_21)); }
	inline Vector3_t2243707580  get_axisOfRotation_21() const { return ___axisOfRotation_21; }
	inline Vector3_t2243707580 * get_address_of_axisOfRotation_21() { return &___axisOfRotation_21; }
	inline void set_axisOfRotation_21(Vector3_t2243707580  value)
	{
		___axisOfRotation_21 = value;
	}

	inline static int32_t get_offset_of_axisOfRotationOffset_22() { return static_cast<int32_t>(offsetof(VortexParticleAffector_t868510250, ___axisOfRotationOffset_22)); }
	inline Vector3_t2243707580  get_axisOfRotationOffset_22() const { return ___axisOfRotationOffset_22; }
	inline Vector3_t2243707580 * get_address_of_axisOfRotationOffset_22() { return &___axisOfRotationOffset_22; }
	inline void set_axisOfRotationOffset_22(Vector3_t2243707580  value)
	{
		___axisOfRotationOffset_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VORTEXPARTICLEAFFECTOR_T868510250_H
#ifndef DESTROYONPARTICLESDEAD_T2259132662_H
#define DESTROYONPARTICLESDEAD_T2259132662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.DestroyOnParticlesDead
struct  DestroyOnParticlesDead_t2259132662  : public ParticleSystems_t2274077986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYONPARTICLESDEAD_T2259132662_H
#ifndef DESTROYONTRAILSDESTROYED_T246466167_H
#define DESTROYONTRAILSDESTROYED_T246466167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.DestroyOnTrailsDestroyed
struct  DestroyOnTrailsDestroyed_t246466167  : public TrailRenderers_t2418747850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYONTRAILSDESTROYED_T246466167_H
#ifndef PARTICLESYSTEMSSIMULATIONSPEED_T2257522220_H
#define PARTICLESYSTEMSSIMULATIONSPEED_T2257522220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.ParticleSystemsSimulationSpeed
struct  ParticleSystemsSimulationSpeed_t2257522220  : public ParticleSystems_t2274077986
{
public:
	// System.Single MirzaBeig.ParticleSystems.ParticleSystemsSimulationSpeed::speed
	float ___speed_4;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(ParticleSystemsSimulationSpeed_t2257522220, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSSIMULATIONSPEED_T2257522220_H
#ifndef ATTRACTIONPARTICLEFORCEFIELD_T2380317660_H
#define ATTRACTIONPARTICLEFORCEFIELD_T2380317660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.AttractionParticleForceField
struct  AttractionParticleForceField_t2380317660  : public ParticleForceField_t2985254799
{
public:
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleForceField::arrivalRadius
	float ___arrivalRadius_21;
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleForceField::arrivedRadius
	float ___arrivedRadius_22;
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleForceField::arrivalRadiusSqr
	float ___arrivalRadiusSqr_23;
	// System.Single MirzaBeig.Scripting.Effects.AttractionParticleForceField::arrivedRadiusSqr
	float ___arrivedRadiusSqr_24;

public:
	inline static int32_t get_offset_of_arrivalRadius_21() { return static_cast<int32_t>(offsetof(AttractionParticleForceField_t2380317660, ___arrivalRadius_21)); }
	inline float get_arrivalRadius_21() const { return ___arrivalRadius_21; }
	inline float* get_address_of_arrivalRadius_21() { return &___arrivalRadius_21; }
	inline void set_arrivalRadius_21(float value)
	{
		___arrivalRadius_21 = value;
	}

	inline static int32_t get_offset_of_arrivedRadius_22() { return static_cast<int32_t>(offsetof(AttractionParticleForceField_t2380317660, ___arrivedRadius_22)); }
	inline float get_arrivedRadius_22() const { return ___arrivedRadius_22; }
	inline float* get_address_of_arrivedRadius_22() { return &___arrivedRadius_22; }
	inline void set_arrivedRadius_22(float value)
	{
		___arrivedRadius_22 = value;
	}

	inline static int32_t get_offset_of_arrivalRadiusSqr_23() { return static_cast<int32_t>(offsetof(AttractionParticleForceField_t2380317660, ___arrivalRadiusSqr_23)); }
	inline float get_arrivalRadiusSqr_23() const { return ___arrivalRadiusSqr_23; }
	inline float* get_address_of_arrivalRadiusSqr_23() { return &___arrivalRadiusSqr_23; }
	inline void set_arrivalRadiusSqr_23(float value)
	{
		___arrivalRadiusSqr_23 = value;
	}

	inline static int32_t get_offset_of_arrivedRadiusSqr_24() { return static_cast<int32_t>(offsetof(AttractionParticleForceField_t2380317660, ___arrivedRadiusSqr_24)); }
	inline float get_arrivedRadiusSqr_24() const { return ___arrivedRadiusSqr_24; }
	inline float* get_address_of_arrivedRadiusSqr_24() { return &___arrivedRadiusSqr_24; }
	inline void set_arrivedRadiusSqr_24(float value)
	{
		___arrivedRadiusSqr_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRACTIONPARTICLEFORCEFIELD_T2380317660_H
#ifndef SHARPEN_T3011623735_H
#define SHARPEN_T3011623735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Shaders.ImageEffects.Sharpen
struct  Sharpen_t3011623735  : public IEBase_t2978259237
{
public:
	// System.Single MirzaBeig.Shaders.ImageEffects.Sharpen::strength
	float ___strength_5;
	// System.Single MirzaBeig.Shaders.ImageEffects.Sharpen::edgeMult
	float ___edgeMult_6;

public:
	inline static int32_t get_offset_of_strength_5() { return static_cast<int32_t>(offsetof(Sharpen_t3011623735, ___strength_5)); }
	inline float get_strength_5() const { return ___strength_5; }
	inline float* get_address_of_strength_5() { return &___strength_5; }
	inline void set_strength_5(float value)
	{
		___strength_5 = value;
	}

	inline static int32_t get_offset_of_edgeMult_6() { return static_cast<int32_t>(offsetof(Sharpen_t3011623735, ___edgeMult_6)); }
	inline float get_edgeMult_6() const { return ___edgeMult_6; }
	inline float* get_address_of_edgeMult_6() { return &___edgeMult_6; }
	inline void set_edgeMult_6(float value)
	{
		___edgeMult_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARPEN_T3011623735_H
#ifndef BASEINPUTMODULE_T1295781545_H
#define BASEINPUTMODULE_T1295781545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t1295781545  : public UIBehaviour_t3960014691
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t3685274804 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t1524870173 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t3466835263 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t2681005625 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t621514313 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t621514313 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_RaycastResultCache_2)); }
	inline List_1_t3685274804 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t3685274804 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t3685274804 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_AxisEventData_3)); }
	inline AxisEventData_t1524870173 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t1524870173 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t1524870173 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_EventSystem_4)); }
	inline EventSystem_t3466835263 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t3466835263 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t3466835263 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_BaseEventData_5)); }
	inline BaseEventData_t2681005625 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t2681005625 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t2681005625 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_InputOverride_6)); }
	inline BaseInput_t621514313 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t621514313 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t621514313 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_DefaultInput_7)); }
	inline BaseInput_t621514313 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t621514313 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t621514313 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T1295781545_H
#ifndef VORTEXPARTICLEFORCEFIELD_T1112726591_H
#define VORTEXPARTICLEFORCEFIELD_T1112726591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.VortexParticleForceField
struct  VortexParticleForceField_t1112726591  : public ParticleForceField_t2985254799
{
public:
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.VortexParticleForceField::axisOfRotation
	Vector3_t2243707580  ___axisOfRotation_21;
	// UnityEngine.Vector3 MirzaBeig.Scripting.Effects.VortexParticleForceField::axisOfRotationOffset
	Vector3_t2243707580  ___axisOfRotationOffset_22;

public:
	inline static int32_t get_offset_of_axisOfRotation_21() { return static_cast<int32_t>(offsetof(VortexParticleForceField_t1112726591, ___axisOfRotation_21)); }
	inline Vector3_t2243707580  get_axisOfRotation_21() const { return ___axisOfRotation_21; }
	inline Vector3_t2243707580 * get_address_of_axisOfRotation_21() { return &___axisOfRotation_21; }
	inline void set_axisOfRotation_21(Vector3_t2243707580  value)
	{
		___axisOfRotation_21 = value;
	}

	inline static int32_t get_offset_of_axisOfRotationOffset_22() { return static_cast<int32_t>(offsetof(VortexParticleForceField_t1112726591, ___axisOfRotationOffset_22)); }
	inline Vector3_t2243707580  get_axisOfRotationOffset_22() const { return ___axisOfRotationOffset_22; }
	inline Vector3_t2243707580 * get_address_of_axisOfRotationOffset_22() { return &___axisOfRotationOffset_22; }
	inline void set_axisOfRotationOffset_22(Vector3_t2243707580  value)
	{
		___axisOfRotationOffset_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VORTEXPARTICLEFORCEFIELD_T1112726591_H
#ifndef TURBULENCEPARTICLEFORCEFIELD_T1289429996_H
#define TURBULENCEPARTICLEFORCEFIELD_T1289429996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.Scripting.Effects.TurbulenceParticleForceField
struct  TurbulenceParticleForceField_t1289429996  : public ParticleForceField_t2985254799
{
public:
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::scrollSpeed
	float ___scrollSpeed_21;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::frequency
	float ___frequency_22;
	// MirzaBeig.Scripting.Effects.TurbulenceParticleForceField/NoiseType MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::noiseType
	int32_t ___noiseType_23;
	// System.Int32 MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::octaves
	int32_t ___octaves_24;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::octaveMultiplier
	float ___octaveMultiplier_25;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::octaveScale
	float ___octaveScale_26;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::time
	float ___time_27;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::randomX
	float ___randomX_28;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::randomY
	float ___randomY_29;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::randomZ
	float ___randomZ_30;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::offsetX
	float ___offsetX_31;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::offsetY
	float ___offsetY_32;
	// System.Single MirzaBeig.Scripting.Effects.TurbulenceParticleForceField::offsetZ
	float ___offsetZ_33;

public:
	inline static int32_t get_offset_of_scrollSpeed_21() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___scrollSpeed_21)); }
	inline float get_scrollSpeed_21() const { return ___scrollSpeed_21; }
	inline float* get_address_of_scrollSpeed_21() { return &___scrollSpeed_21; }
	inline void set_scrollSpeed_21(float value)
	{
		___scrollSpeed_21 = value;
	}

	inline static int32_t get_offset_of_frequency_22() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___frequency_22)); }
	inline float get_frequency_22() const { return ___frequency_22; }
	inline float* get_address_of_frequency_22() { return &___frequency_22; }
	inline void set_frequency_22(float value)
	{
		___frequency_22 = value;
	}

	inline static int32_t get_offset_of_noiseType_23() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___noiseType_23)); }
	inline int32_t get_noiseType_23() const { return ___noiseType_23; }
	inline int32_t* get_address_of_noiseType_23() { return &___noiseType_23; }
	inline void set_noiseType_23(int32_t value)
	{
		___noiseType_23 = value;
	}

	inline static int32_t get_offset_of_octaves_24() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___octaves_24)); }
	inline int32_t get_octaves_24() const { return ___octaves_24; }
	inline int32_t* get_address_of_octaves_24() { return &___octaves_24; }
	inline void set_octaves_24(int32_t value)
	{
		___octaves_24 = value;
	}

	inline static int32_t get_offset_of_octaveMultiplier_25() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___octaveMultiplier_25)); }
	inline float get_octaveMultiplier_25() const { return ___octaveMultiplier_25; }
	inline float* get_address_of_octaveMultiplier_25() { return &___octaveMultiplier_25; }
	inline void set_octaveMultiplier_25(float value)
	{
		___octaveMultiplier_25 = value;
	}

	inline static int32_t get_offset_of_octaveScale_26() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___octaveScale_26)); }
	inline float get_octaveScale_26() const { return ___octaveScale_26; }
	inline float* get_address_of_octaveScale_26() { return &___octaveScale_26; }
	inline void set_octaveScale_26(float value)
	{
		___octaveScale_26 = value;
	}

	inline static int32_t get_offset_of_time_27() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___time_27)); }
	inline float get_time_27() const { return ___time_27; }
	inline float* get_address_of_time_27() { return &___time_27; }
	inline void set_time_27(float value)
	{
		___time_27 = value;
	}

	inline static int32_t get_offset_of_randomX_28() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___randomX_28)); }
	inline float get_randomX_28() const { return ___randomX_28; }
	inline float* get_address_of_randomX_28() { return &___randomX_28; }
	inline void set_randomX_28(float value)
	{
		___randomX_28 = value;
	}

	inline static int32_t get_offset_of_randomY_29() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___randomY_29)); }
	inline float get_randomY_29() const { return ___randomY_29; }
	inline float* get_address_of_randomY_29() { return &___randomY_29; }
	inline void set_randomY_29(float value)
	{
		___randomY_29 = value;
	}

	inline static int32_t get_offset_of_randomZ_30() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___randomZ_30)); }
	inline float get_randomZ_30() const { return ___randomZ_30; }
	inline float* get_address_of_randomZ_30() { return &___randomZ_30; }
	inline void set_randomZ_30(float value)
	{
		___randomZ_30 = value;
	}

	inline static int32_t get_offset_of_offsetX_31() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___offsetX_31)); }
	inline float get_offsetX_31() const { return ___offsetX_31; }
	inline float* get_address_of_offsetX_31() { return &___offsetX_31; }
	inline void set_offsetX_31(float value)
	{
		___offsetX_31 = value;
	}

	inline static int32_t get_offset_of_offsetY_32() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___offsetY_32)); }
	inline float get_offsetY_32() const { return ___offsetY_32; }
	inline float* get_address_of_offsetY_32() { return &___offsetY_32; }
	inline void set_offsetY_32(float value)
	{
		___offsetY_32 = value;
	}

	inline static int32_t get_offset_of_offsetZ_33() { return static_cast<int32_t>(offsetof(TurbulenceParticleForceField_t1289429996, ___offsetZ_33)); }
	inline float get_offsetZ_33() const { return ___offsetZ_33; }
	inline float* get_address_of_offsetZ_33() { return &___offsetZ_33; }
	inline void set_offsetZ_33(float value)
	{
		___offsetZ_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURBULENCEPARTICLEFORCEFIELD_T1289429996_H
#ifndef LOOPINGPARTICLESYSTEMSMANAGER_T2507471847_H
#define LOOPINGPARTICLESYSTEMSMANAGER_T2507471847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirzaBeig.ParticleSystems.Demos.LoopingParticleSystemsManager
struct  LoopingParticleSystemsManager_t2507471847  : public ParticleManager_t3376067569
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPINGPARTICLESYSTEMSMANAGER_T2507471847_H
#ifndef VRINPUTMODULE_T21405650_H
#define VRINPUTMODULE_T21405650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.VRInputModule
struct  VRInputModule_t21405650  : public BaseInputModule_t1295781545
{
public:
	// System.Int32 UnityEngine.UI.Extensions.VRInputModule::counter
	int32_t ___counter_10;

public:
	inline static int32_t get_offset_of_counter_10() { return static_cast<int32_t>(offsetof(VRInputModule_t21405650, ___counter_10)); }
	inline int32_t get_counter_10() const { return ___counter_10; }
	inline int32_t* get_address_of_counter_10() { return &___counter_10; }
	inline void set_counter_10(int32_t value)
	{
		___counter_10 = value;
	}
};

struct VRInputModule_t21405650_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.UI.Extensions.VRInputModule::targetObject
	GameObject_t1756533147 * ___targetObject_8;
	// UnityEngine.UI.Extensions.VRInputModule UnityEngine.UI.Extensions.VRInputModule::_singleton
	VRInputModule_t21405650 * ____singleton_9;
	// System.Boolean UnityEngine.UI.Extensions.VRInputModule::mouseClicked
	bool ___mouseClicked_11;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.VRInputModule::cursorPosition
	Vector3_t2243707580  ___cursorPosition_12;

public:
	inline static int32_t get_offset_of_targetObject_8() { return static_cast<int32_t>(offsetof(VRInputModule_t21405650_StaticFields, ___targetObject_8)); }
	inline GameObject_t1756533147 * get_targetObject_8() const { return ___targetObject_8; }
	inline GameObject_t1756533147 ** get_address_of_targetObject_8() { return &___targetObject_8; }
	inline void set_targetObject_8(GameObject_t1756533147 * value)
	{
		___targetObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_8), value);
	}

	inline static int32_t get_offset_of__singleton_9() { return static_cast<int32_t>(offsetof(VRInputModule_t21405650_StaticFields, ____singleton_9)); }
	inline VRInputModule_t21405650 * get__singleton_9() const { return ____singleton_9; }
	inline VRInputModule_t21405650 ** get_address_of__singleton_9() { return &____singleton_9; }
	inline void set__singleton_9(VRInputModule_t21405650 * value)
	{
		____singleton_9 = value;
		Il2CppCodeGenWriteBarrier((&____singleton_9), value);
	}

	inline static int32_t get_offset_of_mouseClicked_11() { return static_cast<int32_t>(offsetof(VRInputModule_t21405650_StaticFields, ___mouseClicked_11)); }
	inline bool get_mouseClicked_11() const { return ___mouseClicked_11; }
	inline bool* get_address_of_mouseClicked_11() { return &___mouseClicked_11; }
	inline void set_mouseClicked_11(bool value)
	{
		___mouseClicked_11 = value;
	}

	inline static int32_t get_offset_of_cursorPosition_12() { return static_cast<int32_t>(offsetof(VRInputModule_t21405650_StaticFields, ___cursorPosition_12)); }
	inline Vector3_t2243707580  get_cursorPosition_12() const { return ___cursorPosition_12; }
	inline Vector3_t2243707580 * get_address_of_cursorPosition_12() { return &___cursorPosition_12; }
	inline void set_cursorPosition_12(Vector3_t2243707580  value)
	{
		___cursorPosition_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRINPUTMODULE_T21405650_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (U3CScaleOUTU3Ec__Iterator1_t2806189492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[6] = 
{
	U3CScaleOUTU3Ec__Iterator1_t2806189492::get_offset_of_U3CtU3E__1_0(),
	U3CScaleOUTU3Ec__Iterator1_t2806189492::get_offset_of_U3CmaxTU3E__1_1(),
	U3CScaleOUTU3Ec__Iterator1_t2806189492::get_offset_of_U24this_2(),
	U3CScaleOUTU3Ec__Iterator1_t2806189492::get_offset_of_U24current_3(),
	U3CScaleOUTU3Ec__Iterator1_t2806189492::get_offset_of_U24disposing_4(),
	U3CScaleOUTU3Ec__Iterator1_t2806189492::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (SetPropertyUtility_t2250268447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (RectTransformExtension_t2306465163), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (TestCompression_t2304655758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (UI_InfiniteScroll_t1144724582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[17] = 
{
	UI_InfiniteScroll_t1144724582::get_offset_of_InitByUser_2(),
	UI_InfiniteScroll_t1144724582::get_offset_of__scrollRect_3(),
	UI_InfiniteScroll_t1144724582::get_offset_of__contentSizeFitter_4(),
	UI_InfiniteScroll_t1144724582::get_offset_of__verticalLayoutGroup_5(),
	UI_InfiniteScroll_t1144724582::get_offset_of__horizontalLayoutGroup_6(),
	UI_InfiniteScroll_t1144724582::get_offset_of__gridLayoutGroup_7(),
	UI_InfiniteScroll_t1144724582::get_offset_of__isVertical_8(),
	UI_InfiniteScroll_t1144724582::get_offset_of__isHorizontal_9(),
	UI_InfiniteScroll_t1144724582::get_offset_of__disableMarginX_10(),
	UI_InfiniteScroll_t1144724582::get_offset_of__disableMarginY_11(),
	UI_InfiniteScroll_t1144724582::get_offset_of__hasDisabledGridComponents_12(),
	UI_InfiniteScroll_t1144724582::get_offset_of_items_13(),
	UI_InfiniteScroll_t1144724582::get_offset_of__newAnchoredPosition_14(),
	UI_InfiniteScroll_t1144724582::get_offset_of__treshold_15(),
	UI_InfiniteScroll_t1144724582::get_offset_of__itemCount_16(),
	UI_InfiniteScroll_t1144724582::get_offset_of__recordOffsetX_17(),
	UI_InfiniteScroll_t1144724582::get_offset_of__recordOffsetY_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (UI_ScrollRectOcclusion_t1850795053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[12] = 
{
	UI_ScrollRectOcclusion_t1850795053::get_offset_of_InitByUser_2(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__scrollRect_3(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__contentSizeFitter_4(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__verticalLayoutGroup_5(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__horizontalLayoutGroup_6(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__gridLayoutGroup_7(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__isVertical_8(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__isHorizontal_9(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__disableMarginX_10(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of__disableMarginY_11(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of_hasDisabledGridComponents_12(),
	UI_ScrollRectOcclusion_t1850795053::get_offset_of_items_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (UI_TweenScale_t1720817768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[9] = 
{
	UI_TweenScale_t1720817768::get_offset_of_animCurve_2(),
	UI_TweenScale_t1720817768::get_offset_of_speed_3(),
	UI_TweenScale_t1720817768::get_offset_of_isLoop_4(),
	UI_TweenScale_t1720817768::get_offset_of_playAtAwake_5(),
	UI_TweenScale_t1720817768::get_offset_of_isUniform_6(),
	UI_TweenScale_t1720817768::get_offset_of_animCurveY_7(),
	UI_TweenScale_t1720817768::get_offset_of_initScale_8(),
	UI_TweenScale_t1720817768::get_offset_of_myTransform_9(),
	UI_TweenScale_t1720817768::get_offset_of_newScale_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CTweenU3Ec__Iterator0_t2180083606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[6] = 
{
	U3CTweenU3Ec__Iterator0_t2180083606::get_offset_of_U3CtU3E__0_0(),
	U3CTweenU3Ec__Iterator0_t2180083606::get_offset_of_U3CmaxTU3E__0_1(),
	U3CTweenU3Ec__Iterator0_t2180083606::get_offset_of_U24this_2(),
	U3CTweenU3Ec__Iterator0_t2180083606::get_offset_of_U24current_3(),
	U3CTweenU3Ec__Iterator0_t2180083606::get_offset_of_U24disposing_4(),
	U3CTweenU3Ec__Iterator0_t2180083606::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (UIExtensionMethods_t2394151223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (UIHighlightable_t542579012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[10] = 
{
	UIHighlightable_t542579012::get_offset_of_m_Graphic_2(),
	UIHighlightable_t542579012::get_offset_of_m_Highlighted_3(),
	UIHighlightable_t542579012::get_offset_of_m_Pressed_4(),
	UIHighlightable_t542579012::get_offset_of_m_Interactable_5(),
	UIHighlightable_t542579012::get_offset_of_m_ClickToHold_6(),
	UIHighlightable_t542579012::get_offset_of_NormalColor_7(),
	UIHighlightable_t542579012::get_offset_of_HighlightedColor_8(),
	UIHighlightable_t542579012::get_offset_of_PressedColor_9(),
	UIHighlightable_t542579012::get_offset_of_DisabledColor_10(),
	UIHighlightable_t542579012::get_offset_of_OnInteractableChanged_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (InteractableChangedEvent_t3882566494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (UILineConnector_t1781812353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[5] = 
{
	UILineConnector_t1781812353::get_offset_of_transforms_2(),
	UILineConnector_t1781812353::get_offset_of_previousPositions_3(),
	UILineConnector_t1781812353::get_offset_of_canvas_4(),
	UILineConnector_t1781812353::get_offset_of_rt_5(),
	UILineConnector_t1781812353::get_offset_of_lr_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (UIScrollToSelection_t1032879924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[9] = 
{
	UIScrollToSelection_t1032879924::get_offset_of_scrollDirection_2(),
	UIScrollToSelection_t1032879924::get_offset_of_scrollSpeed_3(),
	UIScrollToSelection_t1032879924::get_offset_of_cancelScrollOnInput_4(),
	UIScrollToSelection_t1032879924::get_offset_of_cancelScrollKeycodes_5(),
	UIScrollToSelection_t1032879924::get_offset_of_U3CScrollWindowU3Ek__BackingField_6(),
	UIScrollToSelection_t1032879924::get_offset_of_U3CTargetScrollRectU3Ek__BackingField_7(),
	UIScrollToSelection_t1032879924::get_offset_of_U3CLastCheckedGameObjectU3Ek__BackingField_8(),
	UIScrollToSelection_t1032879924::get_offset_of_U3CCurrentTargetRectTransformU3Ek__BackingField_9(),
	UIScrollToSelection_t1032879924::get_offset_of_U3CIsManualScrollingAvailableU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (ScrollType_t1104322043)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2213[4] = 
{
	ScrollType_t1104322043::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (UIScrollToSelectionXY_t687907589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[7] = 
{
	UIScrollToSelectionXY_t687907589::get_offset_of_scrollSpeed_2(),
	UIScrollToSelectionXY_t687907589::get_offset_of_layoutListGroup_3(),
	UIScrollToSelectionXY_t687907589::get_offset_of_targetScrollObject_4(),
	UIScrollToSelectionXY_t687907589::get_offset_of_scrollToSelection_5(),
	UIScrollToSelectionXY_t687907589::get_offset_of_scrollWindow_6(),
	UIScrollToSelectionXY_t687907589::get_offset_of_currentCanvas_7(),
	UIScrollToSelectionXY_t687907589::get_offset_of_targetScrollRect_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (UISelectableExtension_t1749765265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[5] = 
{
	UISelectableExtension_t1749765265::get_offset_of_OnButtonPress_2(),
	UISelectableExtension_t1749765265::get_offset_of_OnButtonRelease_3(),
	UISelectableExtension_t1749765265::get_offset_of_OnButtonHeld_4(),
	UISelectableExtension_t1749765265::get_offset_of__pressed_5(),
	UISelectableExtension_t1749765265::get_offset_of__heldEventData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (UIButtonEvent_t1812119617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (VRCursor_t4007861940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[3] = 
{
	VRCursor_t4007861940::get_offset_of_xSens_2(),
	VRCursor_t4007861940::get_offset_of_ySens_3(),
	VRCursor_t4007861940::get_offset_of_currentCollider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (VRInputModule_t21405650), -1, sizeof(VRInputModule_t21405650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2218[5] = 
{
	VRInputModule_t21405650_StaticFields::get_offset_of_targetObject_8(),
	VRInputModule_t21405650_StaticFields::get_offset_of__singleton_9(),
	VRInputModule_t21405650::get_offset_of_counter_10(),
	VRInputModule_t21405650_StaticFields::get_offset_of_mouseClicked_11(),
	VRInputModule_t21405650_StaticFields::get_offset_of_cursorPosition_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (BillboardCameraPlaneUVFX_t2785089741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[1] = 
{
	BillboardCameraPlaneUVFX_t2785089741::get_offset_of_cameraTransform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (GravityClockInteractivityUVFX_t3134117456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[6] = 
{
	GravityClockInteractivityUVFX_t3134117456::get_offset_of_forceAffectors_2(),
	GravityClockInteractivityUVFX_t3134117456::get_offset_of_forceAffectors2_3(),
	GravityClockInteractivityUVFX_t3134117456::get_offset_of_gravityClockPrefab_4(),
	GravityClockInteractivityUVFX_t3134117456::get_offset_of_gravityClock_5(),
	GravityClockInteractivityUVFX_t3134117456::get_offset_of_enableGravityClockVisualEffects_6(),
	GravityClockInteractivityUVFX_t3134117456::get_offset_of_enableGravityClockAttractionForce_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (DemoManager_t3005492872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[36] = 
{
	DemoManager_t3005492872::get_offset_of_cameraRotationTransform_2(),
	DemoManager_t3005492872::get_offset_of_cameraTranslationTransform_3(),
	DemoManager_t3005492872::get_offset_of_cameraLookAtPosition_4(),
	DemoManager_t3005492872::get_offset_of_mouse_5(),
	DemoManager_t3005492872::get_offset_of_targetCameraPosition_6(),
	DemoManager_t3005492872::get_offset_of_targetCameraRotation_7(),
	DemoManager_t3005492872::get_offset_of_cameraPositionStart_8(),
	DemoManager_t3005492872::get_offset_of_cameraRotationStart_9(),
	DemoManager_t3005492872::get_offset_of_input_10(),
	DemoManager_t3005492872::get_offset_of_cameraRotation_11(),
	DemoManager_t3005492872::get_offset_of_cameraMoveAmount_12(),
	DemoManager_t3005492872::get_offset_of_cameraRotateAmount_13(),
	DemoManager_t3005492872::get_offset_of_cameraMoveSpeed_14(),
	DemoManager_t3005492872::get_offset_of_cameraRotationSpeed_15(),
	DemoManager_t3005492872::get_offset_of_cameraAngleLimits_16(),
	DemoManager_t3005492872::get_offset_of_levels_17(),
	DemoManager_t3005492872::get_offset_of_currentLevel_18(),
	DemoManager_t3005492872::get_offset_of_particleMode_19(),
	DemoManager_t3005492872::get_offset_of_advancedRendering_20(),
	DemoManager_t3005492872::get_offset_of_loopingParticleModeToggle_21(),
	DemoManager_t3005492872::get_offset_of_oneshotParticleModeToggle_22(),
	DemoManager_t3005492872::get_offset_of_advancedRenderingToggle_23(),
	DemoManager_t3005492872::get_offset_of_levelToggles_24(),
	DemoManager_t3005492872::get_offset_of_levelTogglesContainer_25(),
	DemoManager_t3005492872::get_offset_of_loopingParticleSystems_26(),
	DemoManager_t3005492872::get_offset_of_oneshotParticleSystems_27(),
	DemoManager_t3005492872::get_offset_of_ui_28(),
	DemoManager_t3005492872::get_offset_of_particleCountText_29(),
	DemoManager_t3005492872::get_offset_of_currentParticleSystemText_30(),
	DemoManager_t3005492872::get_offset_of_particleSpawnInstructionText_31(),
	DemoManager_t3005492872::get_offset_of_timeScaleSlider_32(),
	DemoManager_t3005492872::get_offset_of_timeScaleSliderValueText_33(),
	DemoManager_t3005492872::get_offset_of_mainCamera_34(),
	DemoManager_t3005492872::get_offset_of_mainCameraPostEffects_35(),
	DemoManager_t3005492872::get_offset_of_cameraPositionSmoothDampVelocity_36(),
	DemoManager_t3005492872::get_offset_of_cameraRotationSmoothDampVelocity_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (ParticleMode_t3512372172)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2222[3] = 
{
	ParticleMode_t3512372172::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (Level_t1106640823)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2223[3] = 
{
	Level_t1106640823::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (FPSDisplay_t1753842521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[5] = 
{
	FPSDisplay_t1753842521::get_offset_of_timer_2(),
	FPSDisplay_t1753842521::get_offset_of_updateTime_3(),
	FPSDisplay_t1753842521::get_offset_of_frameCount_4(),
	FPSDisplay_t1753842521::get_offset_of_fpsAccum_5(),
	FPSDisplay_t1753842521::get_offset_of_fpsText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (FPSTest_t3154395497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[3] = 
{
	FPSTest_t3154395497::get_offset_of_targetFPS1_2(),
	FPSTest_t3154395497::get_offset_of_targetFPS2_3(),
	FPSTest_t3154395497::get_offset_of_previousVSyncCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (LoopingParticleSystemsManager_t2507471847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (MouseFollow_t3233463164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[3] = 
{
	MouseFollow_t3233463164::get_offset_of_speed_2(),
	MouseFollow_t3233463164::get_offset_of_distanceFromCamera_3(),
	MouseFollow_t3233463164::get_offset_of_ignoreTimeScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (MouseRotateCamera_t3577251721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[3] = 
{
	MouseRotateCamera_t3577251721::get_offset_of_maxRotation_2(),
	MouseRotateCamera_t3577251721::get_offset_of_speed_3(),
	MouseRotateCamera_t3577251721::get_offset_of_unscaledTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (OneshotParticleSystemsManager_t1174726079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[3] = 
{
	OneshotParticleSystemsManager_t1174726079::get_offset_of_mouseRaycastLayerMask_8(),
	OneshotParticleSystemsManager_t1174726079::get_offset_of_spawnedPrefabs_9(),
	OneshotParticleSystemsManager_t1174726079::get_offset_of_U3CdisableSpawnU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (ParticleManager_t3376067569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[6] = 
{
	ParticleManager_t3376067569::get_offset_of_particlePrefabs_2(),
	ParticleManager_t3376067569::get_offset_of_currentParticlePrefabIndex_3(),
	ParticleManager_t3376067569::get_offset_of_particlePrefabsAppend_4(),
	ParticleManager_t3376067569::get_offset_of_prefabNameUnderscoreCountCutoff_5(),
	ParticleManager_t3376067569::get_offset_of_disableChildrenAtStart_6(),
	ParticleManager_t3376067569::get_offset_of_initialized_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (AnimatedLight_t36455359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[10] = 
{
	AnimatedLight_t36455359::get_offset_of_light_2(),
	AnimatedLight_t36455359::get_offset_of_U3CtimeU3Ek__BackingField_3(),
	AnimatedLight_t36455359::get_offset_of_duration_4(),
	AnimatedLight_t36455359::get_offset_of_evaluating_5(),
	AnimatedLight_t36455359::get_offset_of_colourOverLifetime_6(),
	AnimatedLight_t36455359::get_offset_of_intensityOverLifetime_7(),
	AnimatedLight_t36455359::get_offset_of_loop_8(),
	AnimatedLight_t36455359::get_offset_of_autoDestruct_9(),
	AnimatedLight_t36455359::get_offset_of_startColour_10(),
	AnimatedLight_t36455359::get_offset_of_startIntensity_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (Billboard_t1942171335), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (CameraShakeTarget_t2752665144)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[3] = 
{
	CameraShakeTarget_t2752665144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (CameraShakeAmplitudeCurve_t2344288097)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2234[5] = 
{
	CameraShakeAmplitudeCurve_t2344288097::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (CameraShakeAmplitudeOverDistanceCurve_t2999505806)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	CameraShakeAmplitudeOverDistanceCurve_t2999505806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (CameraShake_t1322554423), -1, sizeof(CameraShake_t1322554423_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2236[7] = 
{
	CameraShake_t1322554423::get_offset_of_smoothDampTime_2(),
	CameraShake_t1322554423::get_offset_of_smoothDampPositionVelocity_3(),
	CameraShake_t1322554423::get_offset_of_smoothDampRotationVelocityX_4(),
	CameraShake_t1322554423::get_offset_of_smoothDampRotationVelocityY_5(),
	CameraShake_t1322554423::get_offset_of_smoothDampRotationVelocityZ_6(),
	CameraShake_t1322554423::get_offset_of_shakes_7(),
	CameraShake_t1322554423_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (Shake_t2901923000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[10] = 
{
	Shake_t2901923000::get_offset_of_amplitude_0(),
	Shake_t2901923000::get_offset_of_frequency_1(),
	Shake_t2901923000::get_offset_of_duration_2(),
	Shake_t2901923000::get_offset_of_target_3(),
	Shake_t2901923000::get_offset_of_timeRemaining_4(),
	Shake_t2901923000::get_offset_of_perlinNoiseX_5(),
	Shake_t2901923000::get_offset_of_perlinNoiseY_6(),
	Shake_t2901923000::get_offset_of_perlinNoiseZ_7(),
	Shake_t2901923000::get_offset_of_noise_8(),
	Shake_t2901923000::get_offset_of_amplitudeOverLifetimeCurve_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (DestroyAfterTime_t3168129069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[1] = 
{
	DestroyAfterTime_t3168129069::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (DestroyOnParticlesDead_t2259132662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (DestroyOnTrailsDestroyed_t246466167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (IgnoreTimeScale_t3989264843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[1] = 
{
	IgnoreTimeScale_t3989264843::get_offset_of_particleSystem_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (ParticleSystems_t2274077986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[2] = 
{
	ParticleSystems_t2274077986::get_offset_of_U3CparticleSystemsU3Ek__BackingField_2(),
	ParticleSystems_t2274077986::get_offset_of_onParticleSystemsDeadEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (onParticleSystemsDeadEventHandler_t222977030), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (ParticleSystemsSimulationSpeed_t2257522220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[1] = 
{
	ParticleSystemsSimulationSpeed_t2257522220::get_offset_of_speed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (PerlinNoise_t3938891586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[4] = 
{
	PerlinNoise_t3938891586::get_offset_of_offset_0(),
	PerlinNoise_t3938891586::get_offset_of_amplitude_1(),
	PerlinNoise_t3938891586::get_offset_of_frequency_2(),
	PerlinNoise_t3938891586::get_offset_of_unscaledTime_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (PerlinNoiseXYZ_t2920671427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[6] = 
{
	PerlinNoiseXYZ_t2920671427::get_offset_of_x_0(),
	PerlinNoiseXYZ_t2920671427::get_offset_of_y_1(),
	PerlinNoiseXYZ_t2920671427::get_offset_of_z_2(),
	PerlinNoiseXYZ_t2920671427::get_offset_of_unscaledTime_3(),
	PerlinNoiseXYZ_t2920671427::get_offset_of_amplitudeScale_4(),
	PerlinNoiseXYZ_t2920671427::get_offset_of_frequencyScale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (RendererSortingOrder_t4127253811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[1] = 
{
	RendererSortingOrder_t4127253811::get_offset_of_sortingOrder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (Rotator_t1284983691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[4] = 
{
	Rotator_t1284983691::get_offset_of_localRotationSpeed_2(),
	Rotator_t1284983691::get_offset_of_worldRotationSpeed_3(),
	Rotator_t1284983691::get_offset_of_executeInEditMode_4(),
	Rotator_t1284983691::get_offset_of_unscaledTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (TrailRenderers_t2418747850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[1] = 
{
	TrailRenderers_t2418747850::get_offset_of_trailRenderers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (TransformNoise_t2551917980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[4] = 
{
	TransformNoise_t2551917980::get_offset_of_positionNoise_2(),
	TransformNoise_t2551917980::get_offset_of_rotationNoise_3(),
	TransformNoise_t2551917980::get_offset_of_unscaledTime_4(),
	TransformNoise_t2551917980::get_offset_of_time_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (DemoManager_XPTitles_t2047362782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[4] = 
{
	DemoManager_XPTitles_t2047362782::get_offset_of_list_2(),
	DemoManager_XPTitles_t2047362782::get_offset_of_particleCountText_3(),
	DemoManager_XPTitles_t2047362782::get_offset_of_currentParticleSystemText_4(),
	DemoManager_XPTitles_t2047362782::get_offset_of_cameraRotator_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (CreateLUT_t2634963967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (AttractionParticleAffector_t1503046627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[4] = 
{
	AttractionParticleAffector_t1503046627::get_offset_of_arrivalRadius_21(),
	AttractionParticleAffector_t1503046627::get_offset_of_arrivedRadius_22(),
	AttractionParticleAffector_t1503046627::get_offset_of_arrivalRadiusSqr_23(),
	AttractionParticleAffector_t1503046627::get_offset_of_arrivedRadiusSqr_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (ParticleAffectorMT_t685825231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[15] = 
{
	ParticleAffectorMT_t685825231::get_offset_of_force_2(),
	ParticleAffectorMT_t685825231::get_offset_of_speed_3(),
	ParticleAffectorMT_t685825231::get_offset_of_particleSystem_4(),
	ParticleAffectorMT_t685825231::get_offset_of_particles_5(),
	ParticleAffectorMT_t685825231::get_offset_of_randomX_6(),
	ParticleAffectorMT_t685825231::get_offset_of_randomY_7(),
	ParticleAffectorMT_t685825231::get_offset_of_randomZ_8(),
	ParticleAffectorMT_t685825231::get_offset_of_offsetX_9(),
	ParticleAffectorMT_t685825231::get_offset_of_offsetY_10(),
	ParticleAffectorMT_t685825231::get_offset_of_offsetZ_11(),
	ParticleAffectorMT_t685825231::get_offset_of_deltaTime_12(),
	ParticleAffectorMT_t685825231::get_offset_of_t_13(),
	ParticleAffectorMT_t685825231::get_offset_of_locker_14(),
	ParticleAffectorMT_t685825231::get_offset_of_processing_15(),
	ParticleAffectorMT_t685825231::get_offset_of_isDoneAssigning_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (TurbulenceParticleAffectorMT_t1716033708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[15] = 
{
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_force_2(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_speed_3(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_particleSystem_4(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_particles_5(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_randomX_6(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_randomY_7(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_randomZ_8(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_offsetX_9(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_offsetY_10(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_offsetZ_11(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_deltaTime_12(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_t_13(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_locker_14(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_processing_15(),
	TurbulenceParticleAffectorMT_t1716033708::get_offset_of_isDoneAssigning_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (Noise_t2617615148), -1, sizeof(Noise_t2617615148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2256[3] = 
{
	Noise_t2617615148_StaticFields::get_offset_of_F3_0(),
	Noise_t2617615148_StaticFields::get_offset_of_G3_1(),
	Noise_t2617615148_StaticFields::get_offset_of_perm_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (ParticleAffector_t2949767568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[19] = 
{
	ParticleAffector_t2949767568::get_offset_of_radius_2(),
	ParticleAffector_t2949767568::get_offset_of_force_3(),
	ParticleAffector_t2949767568::get_offset_of_offset_4(),
	ParticleAffector_t2949767568::get_offset_of__radius_5(),
	ParticleAffector_t2949767568::get_offset_of_radiusSqr_6(),
	ParticleAffector_t2949767568::get_offset_of_forceDeltaTime_7(),
	ParticleAffector_t2949767568::get_offset_of_transformPosition_8(),
	ParticleAffector_t2949767568::get_offset_of_particleSystemExternalForcesMultipliers_9(),
	ParticleAffector_t2949767568::get_offset_of_scaleForceByDistance_10(),
	ParticleAffector_t2949767568::get_offset_of_particleSystem_11(),
	ParticleAffector_t2949767568::get_offset_of__particleSystems_12(),
	ParticleAffector_t2949767568::get_offset_of_particleSystemsCount_13(),
	ParticleAffector_t2949767568::get_offset_of_particleSystems_14(),
	ParticleAffector_t2949767568::get_offset_of_particleSystemParticles_15(),
	ParticleAffector_t2949767568::get_offset_of_particleSystemMainModules_16(),
	ParticleAffector_t2949767568::get_offset_of_particleSystemRenderers_17(),
	ParticleAffector_t2949767568::get_offset_of_currentParticleSystem_18(),
	ParticleAffector_t2949767568::get_offset_of_parameters_19(),
	ParticleAffector_t2949767568::get_offset_of_alwaysUpdate_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (GetForceParameters_t3736837055)+ sizeof (RuntimeObject), sizeof(GetForceParameters_t3736837055 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2258[3] = 
{
	GetForceParameters_t3736837055::get_offset_of_distanceToAffectorCenterSqr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GetForceParameters_t3736837055::get_offset_of_scaledDirectionToAffectorCenter_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GetForceParameters_t3736837055::get_offset_of_particlePosition_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (TurbulenceParticleAffector_t2589922227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[13] = 
{
	TurbulenceParticleAffector_t2589922227::get_offset_of_speed_21(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_frequency_22(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_noiseType_23(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_octaves_24(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_lacunarity_25(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_persistence_26(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_time_27(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_randomX_28(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_randomY_29(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_randomZ_30(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_offsetX_31(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_offsetY_32(),
	TurbulenceParticleAffector_t2589922227::get_offset_of_offsetZ_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (NoiseType_t1186427329)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2260[6] = 
{
	NoiseType_t1186427329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (VortexParticleAffector_t868510250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[2] = 
{
	VortexParticleAffector_t868510250::get_offset_of_axisOfRotation_21(),
	VortexParticleAffector_t868510250::get_offset_of_axisOfRotationOffset_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (ParticleFlocking_t1560556585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[17] = 
{
	ParticleFlocking_t1560556585::get_offset_of_maxDistance_2(),
	ParticleFlocking_t1560556585::get_offset_of_cohesion_3(),
	ParticleFlocking_t1560556585::get_offset_of_separation_4(),
	ParticleFlocking_t1560556585::get_offset_of_useVoxels_5(),
	ParticleFlocking_t1560556585::get_offset_of_voxelLocalCenterFromBounds_6(),
	ParticleFlocking_t1560556585::get_offset_of_voxelVolume_7(),
	ParticleFlocking_t1560556585::get_offset_of_voxelsPerAxis_8(),
	ParticleFlocking_t1560556585::get_offset_of_previousVoxelsPerAxisValue_9(),
	ParticleFlocking_t1560556585::get_offset_of_voxels_10(),
	ParticleFlocking_t1560556585::get_offset_of_particleSystem_11(),
	ParticleFlocking_t1560556585::get_offset_of_particles_12(),
	ParticleFlocking_t1560556585::get_offset_of_particlePositions_13(),
	ParticleFlocking_t1560556585::get_offset_of_particleSystemMainModule_14(),
	ParticleFlocking_t1560556585::get_offset_of_delay_15(),
	ParticleFlocking_t1560556585::get_offset_of_timer_16(),
	ParticleFlocking_t1560556585::get_offset_of_alwaysUpdate_17(),
	ParticleFlocking_t1560556585::get_offset_of_visible_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (Voxel_t3105992857)+ sizeof (RuntimeObject), sizeof(Voxel_t3105992857_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	Voxel_t3105992857::get_offset_of_bounds_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Voxel_t3105992857::get_offset_of_particles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Voxel_t3105992857::get_offset_of_particleCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (ParticleForceFieldsDemo_t2005012413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[43] = 
{
	ParticleForceFieldsDemo_t2005012413::get_offset_of_FPSText_2(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_particleCountText_3(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_postProcessingToggle_4(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_postProcessing_5(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_particleSystem_6(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_particleSystemMainModule_7(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_particleSystemEmissionModule_8(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_maxParticlesText_9(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_particlesPerSecondText_10(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_maxParticlesSlider_11(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_particlesPerSecondSlider_12(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceField_13(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldRadiusText_14(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldMaxForceText_15(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldArrivalRadiusText_16(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldArrivedRadiusText_17(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldPositionTextX_18(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldPositionTextY_19(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldPositionTextZ_20(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldRadiusSlider_21(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldMaxForceSlider_22(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldArrivalRadiusSlider_23(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldArrivedRadiusSlider_24(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldPositionSliderX_25(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldPositionSliderY_26(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_attractionParticleForceFieldPositionSliderZ_27(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceField_28(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRadiusText_29(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldMaxForceText_30(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRotationTextX_31(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRotationTextY_32(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRotationTextZ_33(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldPositionTextX_34(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldPositionTextY_35(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldPositionTextZ_36(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRadiusSlider_37(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldMaxForceSlider_38(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRotationSliderX_39(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRotationSliderY_40(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldRotationSliderZ_41(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldPositionSliderX_42(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldPositionSliderY_43(),
	ParticleForceFieldsDemo_t2005012413::get_offset_of_vortexParticleForceFieldPositionSliderZ_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (ParticleForceFieldsDemo_CameraRig_t3138920947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[6] = 
{
	ParticleForceFieldsDemo_CameraRig_t3138920947::get_offset_of_pivot_2(),
	ParticleForceFieldsDemo_CameraRig_t3138920947::get_offset_of_targetRotation_3(),
	ParticleForceFieldsDemo_CameraRig_t3138920947::get_offset_of_rotationLimit_4(),
	ParticleForceFieldsDemo_CameraRig_t3138920947::get_offset_of_rotationSpeed_5(),
	ParticleForceFieldsDemo_CameraRig_t3138920947::get_offset_of_rotationLerpSpeed_6(),
	ParticleForceFieldsDemo_CameraRig_t3138920947::get_offset_of_startRotation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (AttractionParticleForceField_t2380317660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[4] = 
{
	AttractionParticleForceField_t2380317660::get_offset_of_arrivalRadius_21(),
	AttractionParticleForceField_t2380317660::get_offset_of_arrivedRadius_22(),
	AttractionParticleForceField_t2380317660::get_offset_of_arrivalRadiusSqr_23(),
	AttractionParticleForceField_t2380317660::get_offset_of_arrivedRadiusSqr_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (Noise2_t3037305318), -1, sizeof(Noise2_t3037305318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2267[3] = 
{
	Noise2_t3037305318_StaticFields::get_offset_of_F3_0(),
	Noise2_t3037305318_StaticFields::get_offset_of_G3_1(),
	Noise2_t3037305318_StaticFields::get_offset_of_perm_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (ParticleForceField_t2985254799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[19] = 
{
	ParticleForceField_t2985254799::get_offset_of_radius_2(),
	ParticleForceField_t2985254799::get_offset_of_force_3(),
	ParticleForceField_t2985254799::get_offset_of_center_4(),
	ParticleForceField_t2985254799::get_offset_of__radius_5(),
	ParticleForceField_t2985254799::get_offset_of_radiusSqr_6(),
	ParticleForceField_t2985254799::get_offset_of_forceDeltaTime_7(),
	ParticleForceField_t2985254799::get_offset_of_transformPosition_8(),
	ParticleForceField_t2985254799::get_offset_of_particleSystemExternalForcesMultipliers_9(),
	ParticleForceField_t2985254799::get_offset_of_forceOverDistance_10(),
	ParticleForceField_t2985254799::get_offset_of_particleSystem_11(),
	ParticleForceField_t2985254799::get_offset_of__particleSystems_12(),
	ParticleForceField_t2985254799::get_offset_of_particleSystemsCount_13(),
	ParticleForceField_t2985254799::get_offset_of_particleSystems_14(),
	ParticleForceField_t2985254799::get_offset_of_particleSystemParticles_15(),
	ParticleForceField_t2985254799::get_offset_of_particleSystemMainModules_16(),
	ParticleForceField_t2985254799::get_offset_of_particleSystemRenderers_17(),
	ParticleForceField_t2985254799::get_offset_of_currentParticleSystem_18(),
	ParticleForceField_t2985254799::get_offset_of_parameters_19(),
	ParticleForceField_t2985254799::get_offset_of_alwaysUpdate_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (GetForceParameters_t2196150906)+ sizeof (RuntimeObject), sizeof(GetForceParameters_t2196150906 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[3] = 
{
	GetForceParameters_t2196150906::get_offset_of_distanceToForceFieldCenterSqr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GetForceParameters_t2196150906::get_offset_of_scaledDirectionToForceFieldCenter_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GetForceParameters_t2196150906::get_offset_of_particlePosition_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (TurbulenceParticleForceField_t1289429996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[13] = 
{
	TurbulenceParticleForceField_t1289429996::get_offset_of_scrollSpeed_21(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_frequency_22(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_noiseType_23(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_octaves_24(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_octaveMultiplier_25(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_octaveScale_26(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_time_27(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_randomX_28(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_randomY_29(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_randomZ_30(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_offsetX_31(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_offsetY_32(),
	TurbulenceParticleForceField_t1289429996::get_offset_of_offsetZ_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (NoiseType_t1763094680)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[6] = 
{
	NoiseType_t1763094680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (VortexParticleForceField_t1112726591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[2] = 
{
	VortexParticleForceField_t1112726591::get_offset_of_axisOfRotation_21(),
	VortexParticleForceField_t1112726591::get_offset_of_axisOfRotationOffset_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (ParticleLights_t3626990339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[8] = 
{
	ParticleLights_t3626990339::get_offset_of_ps_2(),
	ParticleLights_t3626990339::get_offset_of_lights_3(),
	ParticleLights_t3626990339::get_offset_of_scale_4(),
	ParticleLights_t3626990339::get_offset_of_intensity_5(),
	ParticleLights_t3626990339::get_offset_of_colour_6(),
	ParticleLights_t3626990339::get_offset_of_colourFromParticle_7(),
	ParticleLights_t3626990339::get_offset_of_shadows_8(),
	ParticleLights_t3626990339::get_offset_of_template_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ParticlePlexusDemo_CameraRig_t3759789884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[6] = 
{
	ParticlePlexusDemo_CameraRig_t3759789884::get_offset_of_pivot_2(),
	ParticlePlexusDemo_CameraRig_t3759789884::get_offset_of_targetRotation_3(),
	ParticlePlexusDemo_CameraRig_t3759789884::get_offset_of_rotationLimit_4(),
	ParticlePlexusDemo_CameraRig_t3759789884::get_offset_of_rotationSpeed_5(),
	ParticlePlexusDemo_CameraRig_t3759789884::get_offset_of_rotationLerpSpeed_6(),
	ParticlePlexusDemo_CameraRig_t3759789884::get_offset_of_startRotation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (ParticlePlexus_t126557311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[19] = 
{
	ParticlePlexus_t126557311::get_offset_of_maxDistance_2(),
	ParticlePlexus_t126557311::get_offset_of_maxConnections_3(),
	ParticlePlexus_t126557311::get_offset_of_maxLineRenderers_4(),
	ParticlePlexus_t126557311::get_offset_of_widthFromParticle_5(),
	ParticlePlexus_t126557311::get_offset_of_colourFromParticle_6(),
	ParticlePlexus_t126557311::get_offset_of_alphaFromParticle_7(),
	ParticlePlexus_t126557311::get_offset_of_particleSystem_8(),
	ParticlePlexus_t126557311::get_offset_of_particles_9(),
	ParticlePlexus_t126557311::get_offset_of_particlePositions_10(),
	ParticlePlexus_t126557311::get_offset_of_particleColours_11(),
	ParticlePlexus_t126557311::get_offset_of_particleSizes_12(),
	ParticlePlexus_t126557311::get_offset_of_particleSystemMainModule_13(),
	ParticlePlexus_t126557311::get_offset_of_lineRendererTemplate_14(),
	ParticlePlexus_t126557311::get_offset_of_lineRenderers_15(),
	ParticlePlexus_t126557311::get_offset_of__transform_16(),
	ParticlePlexus_t126557311::get_offset_of_delay_17(),
	ParticlePlexus_t126557311::get_offset_of_timer_18(),
	ParticlePlexus_t126557311::get_offset_of_alwaysUpdate_19(),
	ParticlePlexus_t126557311::get_offset_of_visible_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (IEBase_t2978259237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[3] = 
{
	IEBase_t2978259237::get_offset_of__material_2(),
	IEBase_t2978259237::get_offset_of_U3CshaderU3Ek__BackingField_3(),
	IEBase_t2978259237::get_offset_of__camera_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (Sharpen_t3011623735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	Sharpen_t3011623735::get_offset_of_strength_5(),
	Sharpen_t3011623735::get_offset_of_edgeMult_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2278[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U24ArrayTypeU3D512_t740780702)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D512_t740780702 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (U3CModuleU3E_t3783534234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (TimedObjectDestructor_t3793803729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[2] = 
{
	TimedObjectDestructor_t3793803729::get_offset_of_timeOut_2(),
	TimedObjectDestructor_t3793803729::get_offset_of_detachChildren_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
