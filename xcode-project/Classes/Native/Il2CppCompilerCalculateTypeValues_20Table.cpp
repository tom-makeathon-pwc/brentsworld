﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Type
struct Type_t;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// LunarConsolePluginInternal.LunarArgumentCache
struct LunarArgumentCache_t781225145;
// LunarConsolePluginInternal.ListMethodsFilter
struct ListMethodsFilter_t4134585008;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.List`1<LunarConsolePluginInternal.LunarConsoleActionCall>
struct List_1_t3930859133;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;




#ifndef U3CMODULEU3E_T3783534236_H
#define U3CMODULEU3E_T3783534236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534236 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534236_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef PLATFORMIOS_T1615185666_H
#define PLATFORMIOS_T1615185666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePlugin.LunarConsole/PlatformIOS
struct  PlatformIOS_t1615185666  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMIOS_T1615185666_H
#ifndef COLORUTILS_T1591501460_H
#define COLORUTILS_T1591501460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.ColorUtils
struct  ColorUtils_t1591501460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUTILS_T1591501460_H
#ifndef LOG_T128058318_H
#define LOG_T128058318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.Log
struct  Log_t128058318  : public RuntimeObject
{
public:

public:
};

struct Log_t128058318_StaticFields
{
public:
	// System.String LunarConsolePluginInternal.Log::TAG
	String_t* ___TAG_0;

public:
	inline static int32_t get_offset_of_TAG_0() { return static_cast<int32_t>(offsetof(Log_t128058318_StaticFields, ___TAG_0)); }
	inline String_t* get_TAG_0() const { return ___TAG_0; }
	inline String_t** get_address_of_TAG_0() { return &___TAG_0; }
	inline void set_TAG_0(String_t* value)
	{
		___TAG_0 = value;
		Il2CppCodeGenWriteBarrier((&___TAG_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_T128058318_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef REFLECTIONUTILS_T3147841162_H
#define REFLECTIONUTILS_T3147841162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.ReflectionUtils
struct  ReflectionUtils_t3147841162  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t3147841162_StaticFields
{
public:
	// System.Object[] LunarConsolePluginInternal.ReflectionUtils::EMPTY_INVOKE_ARGS
	ObjectU5BU5D_t3614634134* ___EMPTY_INVOKE_ARGS_0;

public:
	inline static int32_t get_offset_of_EMPTY_INVOKE_ARGS_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3147841162_StaticFields, ___EMPTY_INVOKE_ARGS_0)); }
	inline ObjectU5BU5D_t3614634134* get_EMPTY_INVOKE_ARGS_0() const { return ___EMPTY_INVOKE_ARGS_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_EMPTY_INVOKE_ARGS_0() { return &___EMPTY_INVOKE_ARGS_0; }
	inline void set_EMPTY_INVOKE_ARGS_0(ObjectU5BU5D_t3614634134* value)
	{
		___EMPTY_INVOKE_ARGS_0 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_INVOKE_ARGS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T3147841162_H
#ifndef U3CFINDATTRIBUTETYPESU3EC__ANONSTOREY0_T711103433_H
#define U3CFINDATTRIBUTETYPESU3EC__ANONSTOREY0_T711103433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.ReflectionUtils/<FindAttributeTypes>c__AnonStorey0
struct  U3CFindAttributeTypesU3Ec__AnonStorey0_t711103433  : public RuntimeObject
{
public:
	// System.Type LunarConsolePluginInternal.ReflectionUtils/<FindAttributeTypes>c__AnonStorey0::attributeType
	Type_t * ___attributeType_0;

public:
	inline static int32_t get_offset_of_attributeType_0() { return static_cast<int32_t>(offsetof(U3CFindAttributeTypesU3Ec__AnonStorey0_t711103433, ___attributeType_0)); }
	inline Type_t * get_attributeType_0() const { return ___attributeType_0; }
	inline Type_t ** get_address_of_attributeType_0() { return &___attributeType_0; }
	inline void set_attributeType_0(Type_t * value)
	{
		___attributeType_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDATTRIBUTETYPESU3EC__ANONSTOREY0_T711103433_H
#ifndef STRINGUTILS_T1926825696_H
#define STRINGUTILS_T1926825696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.StringUtils
struct  StringUtils_t1926825696  : public RuntimeObject
{
public:

public:
};

struct StringUtils_t1926825696_StaticFields
{
public:
	// System.Char[] LunarConsolePluginInternal.StringUtils::kSpaceSplitChars
	CharU5BU5D_t1328083999* ___kSpaceSplitChars_0;
	// System.Text.RegularExpressions.Regex LunarConsolePluginInternal.StringUtils::kRichTagRegex
	Regex_t1803876613 * ___kRichTagRegex_1;
	// System.Collections.Generic.List`1<System.String> LunarConsolePluginInternal.StringUtils::s_tempList
	List_1_t1398341365 * ___s_tempList_2;
	// System.String LunarConsolePluginInternal.StringUtils::Quote
	String_t* ___Quote_3;
	// System.String LunarConsolePluginInternal.StringUtils::SingleQuote
	String_t* ___SingleQuote_4;
	// System.String LunarConsolePluginInternal.StringUtils::EscapedQuote
	String_t* ___EscapedQuote_5;
	// System.String LunarConsolePluginInternal.StringUtils::EscapedSingleQuote
	String_t* ___EscapedSingleQuote_6;

public:
	inline static int32_t get_offset_of_kSpaceSplitChars_0() { return static_cast<int32_t>(offsetof(StringUtils_t1926825696_StaticFields, ___kSpaceSplitChars_0)); }
	inline CharU5BU5D_t1328083999* get_kSpaceSplitChars_0() const { return ___kSpaceSplitChars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_kSpaceSplitChars_0() { return &___kSpaceSplitChars_0; }
	inline void set_kSpaceSplitChars_0(CharU5BU5D_t1328083999* value)
	{
		___kSpaceSplitChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___kSpaceSplitChars_0), value);
	}

	inline static int32_t get_offset_of_kRichTagRegex_1() { return static_cast<int32_t>(offsetof(StringUtils_t1926825696_StaticFields, ___kRichTagRegex_1)); }
	inline Regex_t1803876613 * get_kRichTagRegex_1() const { return ___kRichTagRegex_1; }
	inline Regex_t1803876613 ** get_address_of_kRichTagRegex_1() { return &___kRichTagRegex_1; }
	inline void set_kRichTagRegex_1(Regex_t1803876613 * value)
	{
		___kRichTagRegex_1 = value;
		Il2CppCodeGenWriteBarrier((&___kRichTagRegex_1), value);
	}

	inline static int32_t get_offset_of_s_tempList_2() { return static_cast<int32_t>(offsetof(StringUtils_t1926825696_StaticFields, ___s_tempList_2)); }
	inline List_1_t1398341365 * get_s_tempList_2() const { return ___s_tempList_2; }
	inline List_1_t1398341365 ** get_address_of_s_tempList_2() { return &___s_tempList_2; }
	inline void set_s_tempList_2(List_1_t1398341365 * value)
	{
		___s_tempList_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_tempList_2), value);
	}

	inline static int32_t get_offset_of_Quote_3() { return static_cast<int32_t>(offsetof(StringUtils_t1926825696_StaticFields, ___Quote_3)); }
	inline String_t* get_Quote_3() const { return ___Quote_3; }
	inline String_t** get_address_of_Quote_3() { return &___Quote_3; }
	inline void set_Quote_3(String_t* value)
	{
		___Quote_3 = value;
		Il2CppCodeGenWriteBarrier((&___Quote_3), value);
	}

	inline static int32_t get_offset_of_SingleQuote_4() { return static_cast<int32_t>(offsetof(StringUtils_t1926825696_StaticFields, ___SingleQuote_4)); }
	inline String_t* get_SingleQuote_4() const { return ___SingleQuote_4; }
	inline String_t** get_address_of_SingleQuote_4() { return &___SingleQuote_4; }
	inline void set_SingleQuote_4(String_t* value)
	{
		___SingleQuote_4 = value;
		Il2CppCodeGenWriteBarrier((&___SingleQuote_4), value);
	}

	inline static int32_t get_offset_of_EscapedQuote_5() { return static_cast<int32_t>(offsetof(StringUtils_t1926825696_StaticFields, ___EscapedQuote_5)); }
	inline String_t* get_EscapedQuote_5() const { return ___EscapedQuote_5; }
	inline String_t** get_address_of_EscapedQuote_5() { return &___EscapedQuote_5; }
	inline void set_EscapedQuote_5(String_t* value)
	{
		___EscapedQuote_5 = value;
		Il2CppCodeGenWriteBarrier((&___EscapedQuote_5), value);
	}

	inline static int32_t get_offset_of_EscapedSingleQuote_6() { return static_cast<int32_t>(offsetof(StringUtils_t1926825696_StaticFields, ___EscapedSingleQuote_6)); }
	inline String_t* get_EscapedSingleQuote_6() const { return ___EscapedSingleQuote_6; }
	inline String_t** get_address_of_EscapedSingleQuote_6() { return &___EscapedSingleQuote_6; }
	inline void set_EscapedSingleQuote_6(String_t* value)
	{
		___EscapedSingleQuote_6 = value;
		Il2CppCodeGenWriteBarrier((&___EscapedSingleQuote_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T1926825696_H
#ifndef CLASSUTILS_T1742036363_H
#define CLASSUTILS_T1742036363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.ClassUtils
struct  ClassUtils_t1742036363  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSUTILS_T1742036363_H
#ifndef LUNARCONSOLEANALYTICS_T1001259039_H
#define LUNARCONSOLEANALYTICS_T1001259039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarConsoleAnalytics
struct  LunarConsoleAnalytics_t1001259039  : public RuntimeObject
{
public:

public:
};

struct LunarConsoleAnalytics_t1001259039_StaticFields
{
public:
	// System.String LunarConsolePluginInternal.LunarConsoleAnalytics::TrackingURL
	String_t* ___TrackingURL_0;
	// System.String LunarConsolePluginInternal.LunarConsoleAnalytics::DefaultPayload
	String_t* ___DefaultPayload_2;

public:
	inline static int32_t get_offset_of_TrackingURL_0() { return static_cast<int32_t>(offsetof(LunarConsoleAnalytics_t1001259039_StaticFields, ___TrackingURL_0)); }
	inline String_t* get_TrackingURL_0() const { return ___TrackingURL_0; }
	inline String_t** get_address_of_TrackingURL_0() { return &___TrackingURL_0; }
	inline void set_TrackingURL_0(String_t* value)
	{
		___TrackingURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___TrackingURL_0), value);
	}

	inline static int32_t get_offset_of_DefaultPayload_2() { return static_cast<int32_t>(offsetof(LunarConsoleAnalytics_t1001259039_StaticFields, ___DefaultPayload_2)); }
	inline String_t* get_DefaultPayload_2() const { return ___DefaultPayload_2; }
	inline String_t** get_address_of_DefaultPayload_2() { return &___DefaultPayload_2; }
	inline void set_DefaultPayload_2(String_t* value)
	{
		___DefaultPayload_2 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultPayload_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUNARCONSOLEANALYTICS_T1001259039_H
#ifndef LUNARARGUMENTCACHE_T781225145_H
#define LUNARARGUMENTCACHE_T781225145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarArgumentCache
struct  LunarArgumentCache_t781225145  : public RuntimeObject
{
public:
	// UnityEngine.Object LunarConsolePluginInternal.LunarArgumentCache::m_objectArgument
	Object_t1021602117 * ___m_objectArgument_0;
	// System.String LunarConsolePluginInternal.LunarArgumentCache::m_objectArgumentAssemblyTypeName
	String_t* ___m_objectArgumentAssemblyTypeName_1;
	// System.Int32 LunarConsolePluginInternal.LunarArgumentCache::m_intArgument
	int32_t ___m_intArgument_2;
	// System.Single LunarConsolePluginInternal.LunarArgumentCache::m_floatArgument
	float ___m_floatArgument_3;
	// System.String LunarConsolePluginInternal.LunarArgumentCache::m_stringArgument
	String_t* ___m_stringArgument_4;
	// System.Boolean LunarConsolePluginInternal.LunarArgumentCache::m_boolArgument
	bool ___m_boolArgument_5;

public:
	inline static int32_t get_offset_of_m_objectArgument_0() { return static_cast<int32_t>(offsetof(LunarArgumentCache_t781225145, ___m_objectArgument_0)); }
	inline Object_t1021602117 * get_m_objectArgument_0() const { return ___m_objectArgument_0; }
	inline Object_t1021602117 ** get_address_of_m_objectArgument_0() { return &___m_objectArgument_0; }
	inline void set_m_objectArgument_0(Object_t1021602117 * value)
	{
		___m_objectArgument_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectArgument_0), value);
	}

	inline static int32_t get_offset_of_m_objectArgumentAssemblyTypeName_1() { return static_cast<int32_t>(offsetof(LunarArgumentCache_t781225145, ___m_objectArgumentAssemblyTypeName_1)); }
	inline String_t* get_m_objectArgumentAssemblyTypeName_1() const { return ___m_objectArgumentAssemblyTypeName_1; }
	inline String_t** get_address_of_m_objectArgumentAssemblyTypeName_1() { return &___m_objectArgumentAssemblyTypeName_1; }
	inline void set_m_objectArgumentAssemblyTypeName_1(String_t* value)
	{
		___m_objectArgumentAssemblyTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectArgumentAssemblyTypeName_1), value);
	}

	inline static int32_t get_offset_of_m_intArgument_2() { return static_cast<int32_t>(offsetof(LunarArgumentCache_t781225145, ___m_intArgument_2)); }
	inline int32_t get_m_intArgument_2() const { return ___m_intArgument_2; }
	inline int32_t* get_address_of_m_intArgument_2() { return &___m_intArgument_2; }
	inline void set_m_intArgument_2(int32_t value)
	{
		___m_intArgument_2 = value;
	}

	inline static int32_t get_offset_of_m_floatArgument_3() { return static_cast<int32_t>(offsetof(LunarArgumentCache_t781225145, ___m_floatArgument_3)); }
	inline float get_m_floatArgument_3() const { return ___m_floatArgument_3; }
	inline float* get_address_of_m_floatArgument_3() { return &___m_floatArgument_3; }
	inline void set_m_floatArgument_3(float value)
	{
		___m_floatArgument_3 = value;
	}

	inline static int32_t get_offset_of_m_stringArgument_4() { return static_cast<int32_t>(offsetof(LunarArgumentCache_t781225145, ___m_stringArgument_4)); }
	inline String_t* get_m_stringArgument_4() const { return ___m_stringArgument_4; }
	inline String_t** get_address_of_m_stringArgument_4() { return &___m_stringArgument_4; }
	inline void set_m_stringArgument_4(String_t* value)
	{
		___m_stringArgument_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_stringArgument_4), value);
	}

	inline static int32_t get_offset_of_m_boolArgument_5() { return static_cast<int32_t>(offsetof(LunarArgumentCache_t781225145, ___m_boolArgument_5)); }
	inline bool get_m_boolArgument_5() const { return ___m_boolArgument_5; }
	inline bool* get_address_of_m_boolArgument_5() { return &___m_boolArgument_5; }
	inline void set_m_boolArgument_5(bool value)
	{
		___m_boolArgument_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUNARARGUMENTCACHE_T781225145_H
#ifndef LUNARCONSOLECONFIG_T2250479677_H
#define LUNARCONSOLECONFIG_T2250479677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarConsoleConfig
struct  LunarConsoleConfig_t2250479677  : public RuntimeObject
{
public:

public:
};

struct LunarConsoleConfig_t2250479677_StaticFields
{
public:
	// System.Boolean LunarConsolePluginInternal.LunarConsoleConfig::consoleEnabled
	bool ___consoleEnabled_0;
	// System.Boolean LunarConsolePluginInternal.LunarConsoleConfig::consoleSupported
	bool ___consoleSupported_1;
	// System.Boolean LunarConsolePluginInternal.LunarConsoleConfig::freeVersion
	bool ___freeVersion_2;
	// System.Boolean LunarConsolePluginInternal.LunarConsoleConfig::fullVersion
	bool ___fullVersion_3;

public:
	inline static int32_t get_offset_of_consoleEnabled_0() { return static_cast<int32_t>(offsetof(LunarConsoleConfig_t2250479677_StaticFields, ___consoleEnabled_0)); }
	inline bool get_consoleEnabled_0() const { return ___consoleEnabled_0; }
	inline bool* get_address_of_consoleEnabled_0() { return &___consoleEnabled_0; }
	inline void set_consoleEnabled_0(bool value)
	{
		___consoleEnabled_0 = value;
	}

	inline static int32_t get_offset_of_consoleSupported_1() { return static_cast<int32_t>(offsetof(LunarConsoleConfig_t2250479677_StaticFields, ___consoleSupported_1)); }
	inline bool get_consoleSupported_1() const { return ___consoleSupported_1; }
	inline bool* get_address_of_consoleSupported_1() { return &___consoleSupported_1; }
	inline void set_consoleSupported_1(bool value)
	{
		___consoleSupported_1 = value;
	}

	inline static int32_t get_offset_of_freeVersion_2() { return static_cast<int32_t>(offsetof(LunarConsoleConfig_t2250479677_StaticFields, ___freeVersion_2)); }
	inline bool get_freeVersion_2() const { return ___freeVersion_2; }
	inline bool* get_address_of_freeVersion_2() { return &___freeVersion_2; }
	inline void set_freeVersion_2(bool value)
	{
		___freeVersion_2 = value;
	}

	inline static int32_t get_offset_of_fullVersion_3() { return static_cast<int32_t>(offsetof(LunarConsoleConfig_t2250479677_StaticFields, ___fullVersion_3)); }
	inline bool get_fullVersion_3() const { return ___fullVersion_3; }
	inline bool* get_address_of_fullVersion_3() { return &___fullVersion_3; }
	inline void set_fullVersion_3(bool value)
	{
		___fullVersion_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUNARCONSOLECONFIG_T2250479677_H
#ifndef U3CRESOLVEMETHODU3EC__ANONSTOREY0_T2327983287_H
#define U3CRESOLVEMETHODU3EC__ANONSTOREY0_T2327983287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarConsoleActionCall/<ResolveMethod>c__AnonStorey0
struct  U3CResolveMethodU3Ec__AnonStorey0_t2327983287  : public RuntimeObject
{
public:
	// System.String LunarConsolePluginInternal.LunarConsoleActionCall/<ResolveMethod>c__AnonStorey0::methodName
	String_t* ___methodName_0;
	// System.Type LunarConsolePluginInternal.LunarConsoleActionCall/<ResolveMethod>c__AnonStorey0::paramType
	Type_t * ___paramType_1;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(U3CResolveMethodU3Ec__AnonStorey0_t2327983287, ___methodName_0)); }
	inline String_t* get_methodName_0() const { return ___methodName_0; }
	inline String_t** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(String_t* value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_0), value);
	}

	inline static int32_t get_offset_of_paramType_1() { return static_cast<int32_t>(offsetof(U3CResolveMethodU3Ec__AnonStorey0_t2327983287, ___paramType_1)); }
	inline Type_t * get_paramType_1() const { return ___paramType_1; }
	inline Type_t ** get_address_of_paramType_1() { return &___paramType_1; }
	inline void set_paramType_1(Type_t * value)
	{
		___paramType_1 = value;
		Il2CppCodeGenWriteBarrier((&___paramType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESOLVEMETHODU3EC__ANONSTOREY0_T2327983287_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CTRACKEVENTU3EC__ITERATOR0_T3050832747_H
#define U3CTRACKEVENTU3EC__ITERATOR0_T3050832747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0
struct  U3CTrackEventU3Ec__Iterator0_t3050832747  : public RuntimeObject
{
public:
	// System.String LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::category
	String_t* ___category_0;
	// System.String LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::action
	String_t* ___action_1;
	// System.Int32 LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::value
	int32_t ___value_2;
	// System.String LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::<payload>__0
	String_t* ___U3CpayloadU3E__0_3;
	// UnityEngine.WWW LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_4;
	// System.Object LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 LunarConsolePluginInternal.LunarConsoleAnalytics/<TrackEvent>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_category_0() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___category_0)); }
	inline String_t* get_category_0() const { return ___category_0; }
	inline String_t** get_address_of_category_0() { return &___category_0; }
	inline void set_category_0(String_t* value)
	{
		___category_0 = value;
		Il2CppCodeGenWriteBarrier((&___category_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___action_1)); }
	inline String_t* get_action_1() const { return ___action_1; }
	inline String_t** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(String_t* value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___value_2)); }
	inline int32_t get_value_2() const { return ___value_2; }
	inline int32_t* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(int32_t value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_U3CpayloadU3E__0_3() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___U3CpayloadU3E__0_3)); }
	inline String_t* get_U3CpayloadU3E__0_3() const { return ___U3CpayloadU3E__0_3; }
	inline String_t** get_address_of_U3CpayloadU3E__0_3() { return &___U3CpayloadU3E__0_3; }
	inline void set_U3CpayloadU3E__0_3(String_t* value)
	{
		___U3CpayloadU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpayloadU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_4() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___U3CwwwU3E__0_4)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_4() const { return ___U3CwwwU3E__0_4; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_4() { return &___U3CwwwU3E__0_4; }
	inline void set_U3CwwwU3E__0_4(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CTrackEventU3Ec__Iterator0_t3050832747, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRACKEVENTU3EC__ITERATOR0_T3050832747_H
#ifndef U24ARRAYTYPEU3D24_T762068664_H
#define U24ARRAYTYPEU3D24_T762068664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t762068664 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t762068664__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T762068664_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef REFLECTIONEXCEPTION_T3304445882_H
#define REFLECTIONEXCEPTION_T3304445882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.ReflectionException
struct  ReflectionException_t3304445882  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONEXCEPTION_T3304445882_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t762068664  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t762068664  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t762068664 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t762068664  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef LUNARPERSISTENTLISTENERMODE_T3221473142_H
#define LUNARPERSISTENTLISTENERMODE_T3221473142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarPersistentListenerMode
struct  LunarPersistentListenerMode_t3221473142 
{
public:
	// System.Int32 LunarConsolePluginInternal.LunarPersistentListenerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LunarPersistentListenerMode_t3221473142, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUNARPERSISTENTLISTENERMODE_T3221473142_H
#ifndef GRADIENTMODE_T1452379243_H
#define GRADIENTMODE_T1452379243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.GradientMode
struct  GradientMode_t1452379243 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.GradientMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GradientMode_t1452379243, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTMODE_T1452379243_H
#ifndef GRADIENTDIR_T2268096981_H
#define GRADIENTDIR_T2268096981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.GradientDir
struct  GradientDir_t2268096981 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.GradientDir::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GradientDir_t2268096981, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTDIR_T2268096981_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef LUNARCONSOLEACTIONCALL_T266770705_H
#define LUNARCONSOLEACTIONCALL_T266770705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarConsoleActionCall
struct  LunarConsoleActionCall_t266770705  : public RuntimeObject
{
public:
	// UnityEngine.Object LunarConsolePluginInternal.LunarConsoleActionCall::m_target
	Object_t1021602117 * ___m_target_1;
	// System.String LunarConsolePluginInternal.LunarConsoleActionCall::m_methodName
	String_t* ___m_methodName_2;
	// LunarConsolePluginInternal.LunarPersistentListenerMode LunarConsolePluginInternal.LunarConsoleActionCall::m_mode
	int32_t ___m_mode_3;
	// LunarConsolePluginInternal.LunarArgumentCache LunarConsolePluginInternal.LunarConsoleActionCall::m_arguments
	LunarArgumentCache_t781225145 * ___m_arguments_4;

public:
	inline static int32_t get_offset_of_m_target_1() { return static_cast<int32_t>(offsetof(LunarConsoleActionCall_t266770705, ___m_target_1)); }
	inline Object_t1021602117 * get_m_target_1() const { return ___m_target_1; }
	inline Object_t1021602117 ** get_address_of_m_target_1() { return &___m_target_1; }
	inline void set_m_target_1(Object_t1021602117 * value)
	{
		___m_target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_1), value);
	}

	inline static int32_t get_offset_of_m_methodName_2() { return static_cast<int32_t>(offsetof(LunarConsoleActionCall_t266770705, ___m_methodName_2)); }
	inline String_t* get_m_methodName_2() const { return ___m_methodName_2; }
	inline String_t** get_address_of_m_methodName_2() { return &___m_methodName_2; }
	inline void set_m_methodName_2(String_t* value)
	{
		___m_methodName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_methodName_2), value);
	}

	inline static int32_t get_offset_of_m_mode_3() { return static_cast<int32_t>(offsetof(LunarConsoleActionCall_t266770705, ___m_mode_3)); }
	inline int32_t get_m_mode_3() const { return ___m_mode_3; }
	inline int32_t* get_address_of_m_mode_3() { return &___m_mode_3; }
	inline void set_m_mode_3(int32_t value)
	{
		___m_mode_3 = value;
	}

	inline static int32_t get_offset_of_m_arguments_4() { return static_cast<int32_t>(offsetof(LunarConsoleActionCall_t266770705, ___m_arguments_4)); }
	inline LunarArgumentCache_t781225145 * get_m_arguments_4() const { return ___m_arguments_4; }
	inline LunarArgumentCache_t781225145 ** get_address_of_m_arguments_4() { return &___m_arguments_4; }
	inline void set_m_arguments_4(LunarArgumentCache_t781225145 * value)
	{
		___m_arguments_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_arguments_4), value);
	}
};

struct LunarConsoleActionCall_t266770705_StaticFields
{
public:
	// System.Type[] LunarConsolePluginInternal.LunarConsoleActionCall::kParamTypes
	TypeU5BU5D_t1664964607* ___kParamTypes_0;
	// LunarConsolePluginInternal.ListMethodsFilter LunarConsolePluginInternal.LunarConsoleActionCall::<>f__mg$cache0
	ListMethodsFilter_t4134585008 * ___U3CU3Ef__mgU24cache0_5;

public:
	inline static int32_t get_offset_of_kParamTypes_0() { return static_cast<int32_t>(offsetof(LunarConsoleActionCall_t266770705_StaticFields, ___kParamTypes_0)); }
	inline TypeU5BU5D_t1664964607* get_kParamTypes_0() const { return ___kParamTypes_0; }
	inline TypeU5BU5D_t1664964607** get_address_of_kParamTypes_0() { return &___kParamTypes_0; }
	inline void set_kParamTypes_0(TypeU5BU5D_t1664964607* value)
	{
		___kParamTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___kParamTypes_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(LunarConsoleActionCall_t266770705_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline ListMethodsFilter_t4134585008 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline ListMethodsFilter_t4134585008 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(ListMethodsFilter_t4134585008 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUNARCONSOLEACTIONCALL_T266770705_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef LISTMETHODSFILTER_T4134585008_H
#define LISTMETHODSFILTER_T4134585008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.ListMethodsFilter
struct  ListMethodsFilter_t4134585008  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTMETHODSFILTER_T4134585008_H
#ifndef REFLECTIONTYPEFILTER_T2560002481_H
#define REFLECTIONTYPEFILTER_T2560002481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.ReflectionTypeFilter
struct  ReflectionTypeFilter_t2560002481  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONTYPEFILTER_T2560002481_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef LUNARCONSOLEACTION_T955753361_H
#define LUNARCONSOLEACTION_T955753361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LunarConsolePluginInternal.LunarConsoleAction
struct  LunarConsoleAction_t955753361  : public MonoBehaviour_t1158329972
{
public:
	// System.String LunarConsolePluginInternal.LunarConsoleAction::m_title
	String_t* ___m_title_2;
	// System.Collections.Generic.List`1<LunarConsolePluginInternal.LunarConsoleActionCall> LunarConsolePluginInternal.LunarConsoleAction::m_calls
	List_1_t3930859133 * ___m_calls_3;

public:
	inline static int32_t get_offset_of_m_title_2() { return static_cast<int32_t>(offsetof(LunarConsoleAction_t955753361, ___m_title_2)); }
	inline String_t* get_m_title_2() const { return ___m_title_2; }
	inline String_t** get_address_of_m_title_2() { return &___m_title_2; }
	inline void set_m_title_2(String_t* value)
	{
		___m_title_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_title_2), value);
	}

	inline static int32_t get_offset_of_m_calls_3() { return static_cast<int32_t>(offsetof(LunarConsoleAction_t955753361, ___m_calls_3)); }
	inline List_1_t3930859133 * get_m_calls_3() const { return ___m_calls_3; }
	inline List_1_t3930859133 ** get_address_of_m_calls_3() { return &___m_calls_3; }
	inline void set_m_calls_3(List_1_t3930859133 * value)
	{
		___m_calls_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_calls_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUNARCONSOLEACTION_T955753361_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef ACCORDIONMONTH_T1641348928_H
#define ACCORDIONMONTH_T1641348928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccordionMonth
struct  AccordionMonth_t1641348928  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.LayoutElement AccordionMonth::_layoutElement
	LayoutElement_t2808691390 * ____layoutElement_2;
	// UnityEngine.UI.Toggle AccordionMonth::_toggle
	Toggle_t3976754468 * ____toggle_3;
	// System.Single AccordionMonth::_value
	float ____value_4;

public:
	inline static int32_t get_offset_of__layoutElement_2() { return static_cast<int32_t>(offsetof(AccordionMonth_t1641348928, ____layoutElement_2)); }
	inline LayoutElement_t2808691390 * get__layoutElement_2() const { return ____layoutElement_2; }
	inline LayoutElement_t2808691390 ** get_address_of__layoutElement_2() { return &____layoutElement_2; }
	inline void set__layoutElement_2(LayoutElement_t2808691390 * value)
	{
		____layoutElement_2 = value;
		Il2CppCodeGenWriteBarrier((&____layoutElement_2), value);
	}

	inline static int32_t get_offset_of__toggle_3() { return static_cast<int32_t>(offsetof(AccordionMonth_t1641348928, ____toggle_3)); }
	inline Toggle_t3976754468 * get__toggle_3() const { return ____toggle_3; }
	inline Toggle_t3976754468 ** get_address_of__toggle_3() { return &____toggle_3; }
	inline void set__toggle_3(Toggle_t3976754468 * value)
	{
		____toggle_3 = value;
		Il2CppCodeGenWriteBarrier((&____toggle_3), value);
	}

	inline static int32_t get_offset_of__value_4() { return static_cast<int32_t>(offsetof(AccordionMonth_t1641348928, ____value_4)); }
	inline float get__value_4() const { return ____value_4; }
	inline float* get_address_of__value_4() { return &____value_4; }
	inline void set__value_4(float value)
	{
		____value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCORDIONMONTH_T1641348928_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3793803729_H
#define TIMEDOBJECTDESTRUCTOR_T3793803729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimedObjectDestructor
struct  TimedObjectDestructor_t3793803729  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TimedObjectDestructor::timeOut
	float ___timeOut_2;
	// System.Boolean TimedObjectDestructor::detachChildren
	bool ___detachChildren_3;

public:
	inline static int32_t get_offset_of_timeOut_2() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3793803729, ___timeOut_2)); }
	inline float get_timeOut_2() const { return ___timeOut_2; }
	inline float* get_address_of_timeOut_2() { return &___timeOut_2; }
	inline void set_timeOut_2(float value)
	{
		___timeOut_2 = value;
	}

	inline static int32_t get_offset_of_detachChildren_3() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3793803729, ___detachChildren_3)); }
	inline bool get_detachChildren_3() const { return ___detachChildren_3; }
	inline bool* get_address_of_detachChildren_3() { return &___detachChildren_3; }
	inline void set_detachChildren_3(bool value)
	{
		___detachChildren_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3793803729_H
#ifndef BASEMESHEFFECT_T1728560551_H
#define BASEMESHEFFECT_T1728560551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t1728560551  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t1728560551, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T1728560551_H
#ifndef GRADIENT_T585968992_H
#define GRADIENT_T585968992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient
struct  Gradient_t585968992  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.UI.Extensions.GradientMode UnityEngine.UI.Extensions.Gradient::_gradientMode
	int32_t ____gradientMode_3;
	// UnityEngine.UI.Extensions.GradientDir UnityEngine.UI.Extensions.Gradient::_gradientDir
	int32_t ____gradientDir_4;
	// System.Boolean UnityEngine.UI.Extensions.Gradient::_overwriteAllColor
	bool ____overwriteAllColor_5;
	// UnityEngine.Color UnityEngine.UI.Extensions.Gradient::_vertex1
	Color_t2020392075  ____vertex1_6;
	// UnityEngine.Color UnityEngine.UI.Extensions.Gradient::_vertex2
	Color_t2020392075  ____vertex2_7;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.Gradient::targetGraphic
	Graphic_t2426225576 * ___targetGraphic_8;

public:
	inline static int32_t get_offset_of__gradientMode_3() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____gradientMode_3)); }
	inline int32_t get__gradientMode_3() const { return ____gradientMode_3; }
	inline int32_t* get_address_of__gradientMode_3() { return &____gradientMode_3; }
	inline void set__gradientMode_3(int32_t value)
	{
		____gradientMode_3 = value;
	}

	inline static int32_t get_offset_of__gradientDir_4() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____gradientDir_4)); }
	inline int32_t get__gradientDir_4() const { return ____gradientDir_4; }
	inline int32_t* get_address_of__gradientDir_4() { return &____gradientDir_4; }
	inline void set__gradientDir_4(int32_t value)
	{
		____gradientDir_4 = value;
	}

	inline static int32_t get_offset_of__overwriteAllColor_5() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____overwriteAllColor_5)); }
	inline bool get__overwriteAllColor_5() const { return ____overwriteAllColor_5; }
	inline bool* get_address_of__overwriteAllColor_5() { return &____overwriteAllColor_5; }
	inline void set__overwriteAllColor_5(bool value)
	{
		____overwriteAllColor_5 = value;
	}

	inline static int32_t get_offset_of__vertex1_6() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____vertex1_6)); }
	inline Color_t2020392075  get__vertex1_6() const { return ____vertex1_6; }
	inline Color_t2020392075 * get_address_of__vertex1_6() { return &____vertex1_6; }
	inline void set__vertex1_6(Color_t2020392075  value)
	{
		____vertex1_6 = value;
	}

	inline static int32_t get_offset_of__vertex2_7() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ____vertex2_7)); }
	inline Color_t2020392075  get__vertex2_7() const { return ____vertex2_7; }
	inline Color_t2020392075 * get_address_of__vertex2_7() { return &____vertex2_7; }
	inline void set__vertex2_7(Color_t2020392075  value)
	{
		____vertex2_7 = value;
	}

	inline static int32_t get_offset_of_targetGraphic_8() { return static_cast<int32_t>(offsetof(Gradient_t585968992, ___targetGraphic_8)); }
	inline Graphic_t2426225576 * get_targetGraphic_8() const { return ___targetGraphic_8; }
	inline Graphic_t2426225576 ** get_address_of_targetGraphic_8() { return &___targetGraphic_8; }
	inline void set_targetGraphic_8(Graphic_t2426225576 * value)
	{
		___targetGraphic_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetGraphic_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENT_T585968992_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (PlatformIOS_t1615185666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (LunarConsoleConfig_t2250479677), -1, sizeof(LunarConsoleConfig_t2250479677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2002[4] = 
{
	LunarConsoleConfig_t2250479677_StaticFields::get_offset_of_consoleEnabled_0(),
	LunarConsoleConfig_t2250479677_StaticFields::get_offset_of_consoleSupported_1(),
	LunarConsoleConfig_t2250479677_StaticFields::get_offset_of_freeVersion_2(),
	LunarConsoleConfig_t2250479677_StaticFields::get_offset_of_fullVersion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (LunarConsoleAnalytics_t1001259039), -1, sizeof(LunarConsoleAnalytics_t1001259039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	LunarConsoleAnalytics_t1001259039_StaticFields::get_offset_of_TrackingURL_0(),
	0,
	LunarConsoleAnalytics_t1001259039_StaticFields::get_offset_of_DefaultPayload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (U3CTrackEventU3Ec__Iterator0_t3050832747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[8] = 
{
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_category_0(),
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_action_1(),
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_value_2(),
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_U3CpayloadU3E__0_3(),
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_U3CwwwU3E__0_4(),
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_U24current_5(),
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_U24disposing_6(),
	U3CTrackEventU3Ec__Iterator0_t3050832747::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (LunarPersistentListenerMode_t3221473142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2005[7] = 
{
	LunarPersistentListenerMode_t3221473142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (LunarArgumentCache_t781225145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[6] = 
{
	LunarArgumentCache_t781225145::get_offset_of_m_objectArgument_0(),
	LunarArgumentCache_t781225145::get_offset_of_m_objectArgumentAssemblyTypeName_1(),
	LunarArgumentCache_t781225145::get_offset_of_m_intArgument_2(),
	LunarArgumentCache_t781225145::get_offset_of_m_floatArgument_3(),
	LunarArgumentCache_t781225145::get_offset_of_m_stringArgument_4(),
	LunarArgumentCache_t781225145::get_offset_of_m_boolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (LunarConsoleActionCall_t266770705), -1, sizeof(LunarConsoleActionCall_t266770705_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2007[6] = 
{
	LunarConsoleActionCall_t266770705_StaticFields::get_offset_of_kParamTypes_0(),
	LunarConsoleActionCall_t266770705::get_offset_of_m_target_1(),
	LunarConsoleActionCall_t266770705::get_offset_of_m_methodName_2(),
	LunarConsoleActionCall_t266770705::get_offset_of_m_mode_3(),
	LunarConsoleActionCall_t266770705::get_offset_of_m_arguments_4(),
	LunarConsoleActionCall_t266770705_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CResolveMethodU3Ec__AnonStorey0_t2327983287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	U3CResolveMethodU3Ec__AnonStorey0_t2327983287::get_offset_of_methodName_0(),
	U3CResolveMethodU3Ec__AnonStorey0_t2327983287::get_offset_of_paramType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (LunarConsoleAction_t955753361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[2] = 
{
	LunarConsoleAction_t955753361::get_offset_of_m_title_2(),
	LunarConsoleAction_t955753361::get_offset_of_m_calls_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (ListMethodsFilter_t4134585008), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (ClassUtils_t1742036363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (ColorUtils_t1591501460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (Log_t128058318), -1, sizeof(Log_t128058318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2015[1] = 
{
	Log_t128058318_StaticFields::get_offset_of_TAG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (ReflectionTypeFilter_t2560002481), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (ReflectionUtils_t3147841162), -1, sizeof(ReflectionUtils_t3147841162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2017[1] = 
{
	ReflectionUtils_t3147841162_StaticFields::get_offset_of_EMPTY_INVOKE_ARGS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (U3CFindAttributeTypesU3Ec__AnonStorey0_t711103433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[1] = 
{
	U3CFindAttributeTypesU3Ec__AnonStorey0_t711103433::get_offset_of_attributeType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (ReflectionException_t3304445882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (StringUtils_t1926825696), -1, sizeof(StringUtils_t1926825696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2020[7] = 
{
	StringUtils_t1926825696_StaticFields::get_offset_of_kSpaceSplitChars_0(),
	StringUtils_t1926825696_StaticFields::get_offset_of_kRichTagRegex_1(),
	StringUtils_t1926825696_StaticFields::get_offset_of_s_tempList_2(),
	StringUtils_t1926825696_StaticFields::get_offset_of_Quote_3(),
	StringUtils_t1926825696_StaticFields::get_offset_of_SingleQuote_4(),
	StringUtils_t1926825696_StaticFields::get_offset_of_EscapedQuote_5(),
	StringUtils_t1926825696_StaticFields::get_offset_of_EscapedSingleQuote_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (AccordionMonth_t1641348928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[3] = 
{
	AccordionMonth_t1641348928::get_offset_of__layoutElement_2(),
	AccordionMonth_t1641348928::get_offset_of__toggle_3(),
	AccordionMonth_t1641348928::get_offset_of__value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (Gradient_t585968992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[6] = 
{
	Gradient_t585968992::get_offset_of__gradientMode_3(),
	Gradient_t585968992::get_offset_of__gradientDir_4(),
	Gradient_t585968992::get_offset_of__overwriteAllColor_5(),
	Gradient_t585968992::get_offset_of__vertex1_6(),
	Gradient_t585968992::get_offset_of__vertex2_7(),
	Gradient_t585968992::get_offset_of_targetGraphic_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (GradientMode_t1452379243)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2023[3] = 
{
	GradientMode_t1452379243::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (GradientDir_t2268096981)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2024[5] = 
{
	GradientDir_t2268096981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2025[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (U3CModuleU3E_t3783534236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (TimedObjectDestructor_t3793803729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[2] = 
{
	TimedObjectDestructor_t3793803729::get_offset_of_timeOut_2(),
	TimedObjectDestructor_t3793803729::get_offset_of_detachChildren_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
