//
//  ViewController.swift
//  CalDemo
//
//  Created by Aydan Bedingham on 1/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }
    
    
    @IBAction func likedThis(sender: UIButton) {
        //webcal, subscribe
        //calshow, show
        
        //open(scheme: "calshow://")
        open(scheme: "webcal://p38-calendars.icloud.com/published/2/Eu89AwdW_f0JHEd8RvAUnGHszkjYD2chSwSaPb5PVExVDXbG0zmxAEHoMd-pEdrUW1Im3TXbw7aTD9ywzXLmFpr7mot9UXwh-UYvYjr4lx8")
    }
    
    


}

