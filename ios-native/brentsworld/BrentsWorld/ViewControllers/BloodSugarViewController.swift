//
//  BloodSugarViewController.swift
//  BrentsWorld
//
//  Created by Aydan Bedingham on 2/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class BloodSugarViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var bloodSugarLevelTextField: UITextField?
    
    @IBOutlet weak var timePeriodPicker: UIPickerView?
    @IBOutlet weak var datePicker: UIDatePicker?
    
    @IBOutlet weak var buttonSave: UIBarButtonItem?
    
    let timePeriodValues = ["Morning", "Afternoon"]
    var timePeriodValue : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timePeriodValue = timePeriodValues[0].uppercased()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Textfields
    
    @IBAction func btnSave(sender: UIBarButtonItem) {
        let vc = self
        
        let bloodSugarLevelStr : String = (bloodSugarLevelTextField?.text)!
        
        if let bloodSugarLevel = Float(bloodSugarLevelStr) {
            buttonSave?.isEnabled = false
            
            let bloodSugar = BloodSugar()
            bloodSugar.readingPeriod = timePeriodValue.uppercased()
            bloodSugar.sugar_reading = bloodSugarLevel
            
            NetworkService.sharedInstance.submitBloodSugar(bloodSugar: bloodSugar, completionHandler: { (error) in
                
                if let error = error {
                    print(error.localizedDescription)
                    AlertUtil.showError(msg: error.localizedDescription, viewController: vc)
                } else{
                    vc.navigationController?.popViewController(animated: true)
                }
                
                
            })
            
        }else {
            AlertUtil.showError(msg: "Blood Sugar Level must be a valid number", viewController: self)
        }
    }
    
    
    //PickerView
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return timePeriodValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return timePeriodValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        timePeriodValue = timePeriodValues[row]
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //Datepicker
    
    
}
