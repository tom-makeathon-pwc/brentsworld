//
//  ViewController.swift
//  BrentsWorld
//
//  Created by Aydan Bedingham on 1/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITabBarDelegate {

    @IBOutlet weak var dayOfWeekLabel: UILabel?
    @IBOutlet weak var dayMonthLabel: UILabel?
    
    @IBOutlet weak var stepsLabel: UILabel?
    
    @IBOutlet weak var bloodSugarMorningLabel: UILabel?
    @IBOutlet weak var bloodSugarAfternoonLabel: UILabel?
    @IBOutlet weak var buttonEnterBloodSugar: UIButton?
    
    
    //Container for steps view
    @IBOutlet weak var stepsViewContainer: UIView?
    
    //Tabbar
    @IBOutlet weak var tabBar: UITabBar?
    @IBOutlet weak var tabBarItemDashboard: UITabBarItem?
    @IBOutlet weak var tabBarItemFish: UITabBarItem?
    @IBOutlet weak var tabBarItemSchedule: UITabBarItem?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dayOfWeekLabel?.text = DateUtil.getDayOfWeekString(dayOfWeek: DateUtil.getDayOfWeek())
        dayMonthLabel?.text = DateUtil.getDayMonthString(date: Date())
        
        tabBar?.selectedItem = tabBarItemDashboard
        
        stepsViewContainer?.layer.cornerRadius = ( stepsViewContainer!.frame.size.width / 2)
        stepsViewContainer?.clipsToBounds = true
        stepsViewContainer?.backgroundColor = UIColor.white
        stepsViewContainer?.layer.masksToBounds = true
        stepsViewContainer?.layer.borderColor = UIColor(red: 87/255, green: 209/255, blue: 194/255, alpha: 1.0).cgColor
        stepsViewContainer?.layer.borderWidth = 3
        
        buttonEnterBloodSugar?.layer.cornerRadius = 25
        buttonEnterBloodSugar?.layer.borderColor = UIColor(red: 87/255, green: 209/255, blue: 194/255, alpha: 1.0).cgColor
        buttonEnterBloodSugar?.layer.borderWidth = 2
    }

    override func viewDidAppear(_ animated: Bool) {
        self.stepsLabel?.text = "-"
        self.bloodSugarMorningLabel?.text = "-"
        self.bloodSugarAfternoonLabel?.text = "-"
        
        NetworkService.sharedInstance.retrieveStepsToday { (success, error, steps) in
            if let error = error {
                AlertUtil.showError(msg: error.localizedDescription, viewController: self)
            }
            
            if let steps = steps{
                self.stepsLabel?.text = String(steps)
            }
        }
        
        
        NetworkService.sharedInstance.retrieveLatestBloodSugar{ (success, error, latestMorningBloodSugar, latestAfternoonBloodSugar) in
            if let error = error {
                AlertUtil.showError(msg: error.localizedDescription, viewController: self)
            }
            
            if let latestMorningBloodSugar = latestMorningBloodSugar{
                self.bloodSugarMorningLabel?.text = String(latestMorningBloodSugar.sugar_reading)
            }
            
            if let latestAfternoonBloodSugar = latestAfternoonBloodSugar{
                self.bloodSugarAfternoonLabel?.text = String(latestAfternoonBloodSugar.sugar_reading)
            }
        }
        
        NetworkService.sharedInstance.retrieveLatestBloodSugar { (success, error, latestMorningBloodSugar, latestAfternoonBloodSugar) in
            if let error = error {
                AlertUtil.showError(msg: error.localizedDescription, viewController: self)
            }
            
            if let latestMorningBloodSugar = latestMorningBloodSugar{
                self.bloodSugarMorningLabel?.text = String(latestMorningBloodSugar.sugar_reading)
            }
            
            if let latestAfternoonBloodSugar = latestAfternoonBloodSugar{
                self.bloodSugarAfternoonLabel?.text = String(latestAfternoonBloodSugar.sugar_reading)
            }
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
       
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem){
        
        let vc = self
        
        if (tabBar.selectedItem == tabBarItemFish){
            //Open Application Url
            
            ApplicationUtil.open(scheme: "prizePond://", completionHandler: { (success) in
                if (success==false){
                    AlertUtil.showError(msg: "Could not find Prize Pond App", viewController: vc)
                }
            })
            
        } else if (tabBar.selectedItem == tabBarItemSchedule){
            
            //Open calender in iCloud
            ApplicationUtil.open(scheme: "webcal://p72-calendars.icloud.com/published/2/1xBu6aWKqhuKJo4Otlt2XCWo3BSNM7CRtorpj13eILYCFVtk8OB1KqmK9-5GcuckllYYxKLXAy97SKF1WYnQ9X83pRaj0GjorV9uJL_Gs94", completionHandler: { (success) in
                
                if (success==false){
                    ApplicationUtil.open(scheme: "calshow://", completionHandler: { (success) in
                         //AlertUtil.showError(msg: "Could not find Calender App", viewController: vc)
                    })
                }
                
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            tabBar.selectedItem = self.tabBarItemDashboard
        }
        
        
    }
    
}

