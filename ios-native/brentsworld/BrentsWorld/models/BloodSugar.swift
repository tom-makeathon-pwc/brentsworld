//
//  BloodSugar.swift
//  BrentsWorld
//
//  Created by Aydan Bedingham on 2/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class BloodSugar: NSObject {
    
    var id : Int
    var readingPeriod : String
    var sugar_reading : Float
    var createdAt : Int
    
    override init() {
        id = 0
        readingPeriod = ""
        sugar_reading = 0
        createdAt = 0
    }
    
    class func fromJSON(json: [String: AnyObject]) -> BloodSugar {
        let obj = BloodSugar()
        if let jsonElementType = json["id"] as? Int { obj.id = jsonElementType }
        if let jsonElementType = json["readingPeriod"] as? String { obj.readingPeriod = jsonElementType }
        if let jsonElementType = json["suger_reading"] as? Float { obj.sugar_reading = jsonElementType }
        if let jsonElementType = json["createdAt"] as? Int { obj.createdAt = jsonElementType }
        return obj
    }

    class func fromJSON(json: [[String: AnyObject]]) -> [BloodSugar] {
        var objs = [BloodSugar]()
        for jsonObj in json{
            let obj = fromJSON(json: jsonObj)
            objs.append(obj)
        }
        return objs
    }
}
