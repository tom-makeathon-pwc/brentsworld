//
//  NetworkService.swift
//  BrentsWorld
//
//  Created by Aydan Bedingham on 2/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class NetworkService: NSObject {
    
    @objc static let sharedInstance = NetworkService()

    static let BASE_URL : String = "http://13.70.181.152:8080/api"
    static let BLOOD_SUGAR_LIST_URI : String = "/sugerreadings"
    static let BLOOD_SUGAR_SUBMIT_URI : String = "/sugerlevel"
    static let STEPS_READ_URI : String = "/readsteps"
    
    
    
    fileprivate static let TIMEOUT = 60.0   // Seconds
    
    fileprivate static let errorDomain = "networkService"
    fileprivate static let errorDomainDefaultCode = 1000
    
    fileprivate static var downloadSession: URLSession!
    
    override fileprivate init() {
        
        // Create shared session
        let config = URLSessionConfiguration.default
        NetworkService.downloadSession = URLSession(configuration: config)
        NetworkService.downloadSession.configuration.timeoutIntervalForRequest = NetworkService.TIMEOUT
        NetworkService.downloadSession.configuration.timeoutIntervalForResource = NetworkService.TIMEOUT
    }
    
    
    func retrieveStepsToday(completionHandler: @escaping (_ success:Bool, _ error:NSError?, _ steps:Int?) -> Void) {
        
        retrieveData(urlStr: NetworkService.BASE_URL + NetworkService.STEPS_READ_URI) { (success, error, data) in
            do {
                if (success == false){
                    DispatchQueue.main.async { completionHandler(false, error, nil) }
                    return
                }
                
                let allStepsJson = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String: AnyObject]]
                
                if (allStepsJson.count>0){
                    let lastStepsDict = allStepsJson[allStepsJson.count-1]
                    let stepValue = lastStepsDict["step_reading"]
                    DispatchQueue.main.async { completionHandler(true, nil, stepValue as? Int) }
                } else{
                     DispatchQueue.main.async { completionHandler(false, nil, nil) }
                }
                
                return
            } catch let error as NSError {
                DispatchQueue.main.async { completionHandler(false, error as NSError, nil) }
                return
            }
        }
    }
    
    
    
    
    func retrieveLatestBloodSugar(completionHandler: @escaping (_ success:Bool, _ error:NSError?, _ latestMorningBloodSugar:BloodSugar?, _ latestAfternoonBloodSugar:BloodSugar?) -> Void) {
        
        retrieveData(urlStr: NetworkService.BASE_URL + NetworkService.BLOOD_SUGAR_LIST_URI) { (success, error, data) in
            do {
                if (success == false){
                    DispatchQueue.main.async { completionHandler(false, error, nil, nil) }
                    return
                }
                
                let bloodSugarReadingsJson = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String: AnyObject]]
                let bloodSugars = BloodSugar.fromJSON(json: bloodSugarReadingsJson)
                
                
                //Determine latest morning blood sugar
                var latestMorningBloodSugar : BloodSugar? = nil
                for bloodSugar in bloodSugars {
                    if (bloodSugar.readingPeriod.uppercased()=="MORNING") &&
                        ((latestMorningBloodSugar==nil) || (bloodSugar.createdAt > latestMorningBloodSugar!.createdAt))
                    {
                        latestMorningBloodSugar = bloodSugar
                    }
                }
                
                //Determine latest afternoon blood sugar
                var latestAfternoonBloodSugar : BloodSugar? = nil
                for bloodSugar in bloodSugars {
                    if (bloodSugar.readingPeriod.uppercased()=="AFTERNOON") &&
                        ((latestAfternoonBloodSugar==nil) || (bloodSugar.createdAt > latestAfternoonBloodSugar!.createdAt))
                    {
                        latestAfternoonBloodSugar = bloodSugar
                    }
                }
                DispatchQueue.main.async { completionHandler(true, nil, latestMorningBloodSugar, latestAfternoonBloodSugar) }
                
                return
            } catch let error as NSError {
                DispatchQueue.main.async { completionHandler(false, error as NSError, nil, nil) }
                return
            }
        }
    }
    
    
    func submitBloodSugar(bloodSugar:BloodSugar, completionHandler: @escaping (_ error:NSError?) -> Void){
        
        let postDict = [
            "readingPeriod" : bloodSugar.readingPeriod,
            "suger_reading" : bloodSugar.sugar_reading,
        ] as [String : Any]
        
        postData(urlStr: NetworkService.BASE_URL + NetworkService.BLOOD_SUGAR_SUBMIT_URI, obj: postDict) { (error) in
            DispatchQueue.main.async { completionHandler(error) }
        }
    }
    
    
    private func postData(urlStr:String, obj: Any, completionHandler: @escaping (_ error:NSError?) -> Void) {
        var err = NSError(domain: NetworkService.errorDomain, code: NetworkService.errorDomainDefaultCode, userInfo: [NSLocalizedDescriptionKey : "Invalid URL"])
        let url = URL(string: urlStr)
        
        if let url = url {
            let request = NSMutableURLRequest(url: url)
            request.timeoutInterval = NetworkService.TIMEOUT
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
        
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            session.configuration.timeoutIntervalForRequest = NetworkService.TIMEOUT
            session.configuration.timeoutIntervalForResource = NetworkService.TIMEOUT
            
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: obj as Any, options: .prettyPrinted)
                request.httpBody = jsonData
                
                let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                    if let error = error {
                        DispatchQueue.main.async { completionHandler(error as NSError) }
                        return
                    }
                    
                    let httpResponse = response as! HTTPURLResponse
                    
                    if httpResponse.statusCode == 200 {
                        DispatchQueue.main.async { completionHandler(nil) }
                        return
                    } else {
                        let error = NSError(domain: NetworkService.errorDomain, code: NetworkService.errorDomainDefaultCode, userInfo: [NSLocalizedDescriptionKey : "POST call returned response code \(httpResponse.statusCode)"])
                        DispatchQueue.main.async { completionHandler(error) }
                        return
                    }
                })
                
                dataTask.resume()
                return
            } catch let error as NSError {
                err = error
            }
            
        }
        DispatchQueue.main.async { completionHandler(err) }
    }
    
    private func retrieveData(urlStr:String, completionHandler: @escaping (_ success:Bool, _ error:NSError?, _ data:Data?) -> Void) {
        guard let url = URL(string: urlStr) else {
            let error = NSError(domain: NetworkService.errorDomain, code: NetworkService.errorDomainDefaultCode, userInfo: [NSLocalizedDescriptionKey : "Invalid URL"])
            DispatchQueue.main.async { completionHandler(false, error, nil) }
            return
        }
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = NetworkService.TIMEOUT
        
        let dataTask = NetworkService.downloadSession.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let error = error {
                DispatchQueue.main.async { completionHandler(false, error as NSError, nil) }
                
                return
            }
                
            else if let data = data {
                DispatchQueue.main.async { completionHandler(true, nil, data) }
                return
            }
            
            let error = NSError(domain: NetworkService.errorDomain, code: NetworkService.errorDomainDefaultCode, userInfo: [NSLocalizedDescriptionKey : "JSON parsing failed"])
            DispatchQueue.main.async { completionHandler(false, error, nil) }
        })
        
        dataTask.resume()
    }
    
}
