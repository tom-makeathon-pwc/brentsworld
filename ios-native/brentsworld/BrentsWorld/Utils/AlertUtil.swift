//
//  AlertUtil.swift
//  BrentsWorld
//
//  Created by Aydan Bedingham on 3/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class AlertUtil: NSObject {

    static func showError(msg:String, viewController: UIViewController){
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
}
