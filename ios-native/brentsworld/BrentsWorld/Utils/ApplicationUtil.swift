//
//  ApplicationUtil.swift
//  BrentsWorld
//
//  Created by Aydan Bedingham on 3/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class ApplicationUtil: NSObject {

    
    static func open(scheme: String, completionHandler:@escaping (Bool) -> Void) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                                            completionHandler(success)
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
                completionHandler(success)
            }
        }
    }
    
}
