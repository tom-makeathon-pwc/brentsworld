//
//  DateUtil.swift
//  BrentsWorld
//
//  Created by Aydan Bedingham on 3/12/17.
//  Copyright © 2017 Aydan Bedingham. All rights reserved.
//

import UIKit

class DateUtil: NSObject {

    static func getDayOfWeek()->Int? {
        let todayDate = NSDate()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let myComponents = myCalendar?.components(.weekday, from: todayDate as Date)
        let weekDay = myComponents?.weekday
        return weekDay
    }
    
    
    static func getDayOfWeekString(dayOfWeek:Int?)->String? {
        switch dayOfWeek {
        case 1?:
            return "Sunday"
        case 2?:
            return "Monday"
        case 3?:
            return "Tuesday"
        case 4?:
            return "Wednesday"
        case 5?:
            return "Thursday"
        case 6?:
            return "Friday"
        case 7?:
            return "Saturday"
        default:
            return "Unknown"
        }
    }
    
    static func getDayMonthString(date:Date)->String{
        let dateFormat = DateFormatter()
        dateFormat.locale = NSLocale.current
        dateFormat.timeZone = NSTimeZone.local
        dateFormat.dateFormat = "dd MMMM";
        return dateFormat.string(from: date)
    }
    
    
}
