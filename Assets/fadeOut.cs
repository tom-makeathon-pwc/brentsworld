﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeOut : MonoBehaviour
{

    MeshRenderer aqRenderer;

    Material aqMaterial;
    float _value;

    // Use this for initialization
    void Start()
    {
        aqRenderer = GetComponent<MeshRenderer>();
        aqMaterial = aqRenderer.material;
    }

    // Update is called once per frame
    void Update()
    {
        print(Shader.PropertyToID("_Alpha"));
        print(aqMaterial);
        
        _value += Time.deltaTime;
        
        
        aqMaterial.SetFloat("_Alpha", Mathf.Lerp(1,0,_value));

    }
}
