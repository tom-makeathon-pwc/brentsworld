﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeEverythingOut : MonoBehaviour {
	MeshRenderer[] meshRenderers;
	float _value;
	// Use this for initialization
	void Start () {
		meshRenderers = GetComponentsInChildren<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		foreach	(var renderer in meshRenderers)
		{
	
			foreach(var mat in renderer.materials){
				_value += .5f * Time.deltaTime;
				 mat.SetFloat("_Alpha", Mathf.Lerp(1,0,_value));
			}
			
		}
	}
}
