﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AquariumHandler : MonoBehaviour
{
	public ParticleSystem ParticleSystem;
	public ParticleRenderer[] Bubbles;
	public bool IsEnabled;

	public GameObject[] FishPrefabs;

	public Transform[] FishPaths;
	public Transform[] Fishes {get; set;}
	private Dictionary<Transform, Transform> FishAssignment;

	MeshRenderer[] meshRenderers;
	float _value;
	bool _played;

	// Use this for initialization
	void Start()
	{
		Fishes = new Transform[FishPaths.Length];
			
		meshRenderers = GetComponentsInChildren<MeshRenderer>();

		foreach (var renderer in meshRenderers)
		{
			foreach (var mat in renderer.materials)
				mat.SetFloat("_Alpha", 0);
		}

		foreach (var bubble in Bubbles)
			bubble.enabled = false;
	}

	// Update is called once per frame
	void Update()
	{
		if (IsEnabled)
		{
			foreach (var renderer in meshRenderers)
			{

				foreach (var mat in renderer.materials)
				{
					_value += .5f * Time.deltaTime;
					mat.SetFloat("_Alpha", Mathf.Lerp(0, 1, _value));
				}
			}

			if (ParticleSystem && !ParticleSystem.isPlaying && !_played)
			{
				_played = true;
				ParticleSystem.Play();

				foreach (var bubble in Bubbles)
					bubble.enabled = true;
				
				CreateFish(0);
			}
		}
	}

	public void AddFish(Transform fish)
	{
		for (int i = 0; i < Fishes.Length; i++)
		{
			if (Fishes[i] == null)
			{
				Fishes[i] = fish;
				fish.SetParent(FishPaths[i]);
				fish.localPosition = Vector3.zero;
				fish.localEulerAngles = new Vector3(0,90,180);
				fish.localScale = Vector3.one;
				break;
			}
		}
	}

	public void CreateFish(int fishIndex = -1)
	{
		if (fishIndex < 0 || fishIndex > FishPrefabs.Length - 1)
			AddFish(Instantiate(FishPrefabs[Random.Range(0, FishPrefabs.Length)].transform));
		else
			AddFish(Instantiate(FishPrefabs[fishIndex].transform));
	}
}
