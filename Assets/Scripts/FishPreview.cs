﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishPreview : MonoBehaviour
{
	public GameObject[] FishPrefabs;
	public Transform TargetParent;
	public Transform PreviewFish;

	private AquariumHandler _aquariumHandler;

	// Use this for initialization
	void Start()
	{
		_aquariumHandler = FindObjectOfType<AquariumHandler>();
	}

	public void CreateFish(int fishIndex = -1)
	{
		if (fishIndex < 0 || fishIndex > FishPrefabs.Length - 1)
			PreviewFish = Instantiate(FishPrefabs[Random.Range(0, FishPrefabs.Length)].transform);
		else
			PreviewFish = Instantiate(FishPrefabs[fishIndex].transform);
		
		PreviewFish.SetParent(TargetParent);
		PreviewFish.localPosition = Vector3.zero;
		PreviewFish.localEulerAngles = Vector3.zero;
		PreviewFish.localScale = Vector3.one;
	}

	public void AssignFish()
	{
		if (PreviewFish)
		{
			_aquariumHandler.AddFish(PreviewFish);
			PreviewFish = null;
		}
	}
}
