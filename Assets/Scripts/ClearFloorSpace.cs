﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearFloorSpace : MonoBehaviour
{
	public GameObject CalibrationText;
	public GameObject PlacementButton;

	public static GameObject plane;
	private Camera _ARCamera;

	private bool _placed;

	// Use this for initialization
	void Start()
	{
		_ARCamera = Camera.main;
	}

	// Update is called once per frame
	void Update()
	{
		if (!_placed && CalibrationText && PlacementButton)
		{
			if (plane)
			{
				Vector3 screenPoint = _ARCamera.WorldToViewportPoint(plane.transform.position);
				bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;

				if (onScreen)
				{
					CalibrationText.SetActive(false);
					PlacementButton.SetActive(true);
				}
				else
				{
					CalibrationText.SetActive(true);
					PlacementButton.SetActive(false);
				}
			}
			else
			{
				CalibrationText.SetActive(true);
				PlacementButton.SetActive(false);
			}
		}
		else
		{
			CalibrationText.SetActive(false);
			PlacementButton.SetActive(false);
		}
	}

	public void Place()
	{
		_placed = true;
	}
}
