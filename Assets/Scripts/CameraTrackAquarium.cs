﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrackAquarium : MonoBehaviour
{
	public GameObject aquarium;
	private Camera _ARCamera;

	private bool _placed;
	private AquariumHandler _aquariumHandler;

	// Use this for initialization
	void Start()
	{
		_ARCamera = Camera.main;
		_aquariumHandler = FindObjectOfType<AquariumHandler>();
	}

	// Update is called once per frame
	void Update()
	{
		if (!_placed)
		{
			if (aquarium)
			{
				Vector3 screenPoint = _ARCamera.WorldToViewportPoint(aquarium.transform.position);
				bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;

				if (onScreen)
				{
					_aquariumHandler.IsEnabled = true;
					_placed = true;
				}
			}
		}
	}

	public void Place()
	{
		_placed = true;
	}
}
