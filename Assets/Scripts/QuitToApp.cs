﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitToApp : MonoBehaviour
{
	// Update is called once per frame
	void OnApplicationQuit()
	{
		Application.OpenURL("stepItUp://");
	}
}
