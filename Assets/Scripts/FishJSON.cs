using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FishJSON : MonoBehaviour
{
	public FishCollection fish;

	void Start()
	{
		StartCoroutine(GetText());
	}

	IEnumerator GetText()
	{
		using (UnityWebRequest www = UnityWebRequest.Get("http://13.70.181.152:8080/api/getalltrophies"))
		{
			www.SetRequestHeader("Content-Type", "application/json");
			
			yield return www.SendWebRequest();

			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
			}
			else
			{
				string json = string.Format("{0} \"fishes\": {1} {2}", "{", www.downloadHandler.text, "}");
				Debug.Log(json);
				// Or retrieve results as binary data
				fish = JsonUtility.FromJson<FishCollection>(json);
			}
		}
	}
}

[Serializable]
public class FishCollection
{
	public FishElement[] fishes;
}


[Serializable]
public class FishElement
{
	public int id;
	public string description;
	public string fishIndex;
	public int createdAt;
}



/* [  
   {  
      "id":1,
      "description":"Brent's First Fish",
      "fishIndex":"0",
      "createdAt":1512261048000
   },
   {  
      "id":2,
      "description":"Brent's First Fish",
      "fishIndex":"0",
      "createdAt":1512263438000
   },
   {  
      "id":3,
      "description":"Brent's First Fish",
      "fishIndex":"0",
      "createdAt":1512263440000
   }
] */
