﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccordionMonth : MonoBehaviour {
	private LayoutElement _layoutElement;
	private Toggle _toggle;

	private float _value;

	// Use this for initialization
	void Start () {
		_layoutElement = GetComponent<LayoutElement>();
		_toggle = GetComponent<Toggle>();
	}
	
	// Update is called once per frame
	void Update () {
		if (_toggle.isOn)
		{
			if (_value < 1)
			{
				_value += Time.deltaTime * 2;
			}
			else
			{
				_value = 1;
			}
		}
		else
		{
			if (_value > 0)
			{
				_value -= Time.deltaTime * 2;
			}
			else
			{
				_value = 0;
			}
		}

		_layoutElement.minHeight = Mathf.SmoothStep(80, 160, _value);
	}
}
